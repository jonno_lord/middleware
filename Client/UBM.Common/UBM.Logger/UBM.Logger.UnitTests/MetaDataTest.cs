﻿using System;
using System.Collections.Generic;
using UBM.Logger;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UBM.Logger.UnitTests
{
    
    
    /// <summary>
    ///This is a test class for MetaDataTest and is intended
    ///to contain all MetaDataTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MetaDataTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        /// <summary>
        ///A test for MetaData Constructor
        ///</summary>
        [TestMethod()]
        public void MetaDataConstructorTest()
        {
            MetaData target = new MetaData();
            Assert.IsInstanceOfType(target, typeof(MetaData));
        }

        /// <summary>
        ///A test for Append
        ///</summary>
        [TestMethod()]
        public void AppendTest()
        {
            MetaData target = new MetaData(); // TODO: Initialize to an appropriate value
            string key = "Key1";
            string value = "Value1";
            target.Append(key, value);
            string key2 = "Key2";
            string value2 = "Value2";
            target.Append(key2, value2);

            Assert.AreEqual(target.KeyValuePairs.Count, 2);
        }

        /// <summary>
        ///A test for Append
        ///</summary>
        [TestMethod()]
        public void AppendDuplicateTest()
        {
            MetaData target = new MetaData();
            string key = "Key";
            string value = "Value";
            target.Append(key, value);
            target.Append(key, value);

            Assert.AreEqual(target.DuplicateIndex, 2);
        }

        /// <summary>
        ///A test for KeyValuePairs
        ///</summary>
        [TestMethod()]
        public void KeyValuePairsTest()
        {
            MetaData target = new MetaData(); // TODO: Initialize to an appropriate value
            Dictionary<string, string> expected = new Dictionary<string, string>(); // TODO: Initialize to an appropriate value
            expected.Add("x", "y");
            Dictionary<string, string> actual;
            target.KeyValuePairs = expected;
            actual = target.KeyValuePairs;
            Assert.AreEqual(expected, actual);
        }

    }
}
