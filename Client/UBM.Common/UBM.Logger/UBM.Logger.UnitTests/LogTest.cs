﻿using System;
using UBM.Logger;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UBM.Logger.UnitTests
{
    
    
    /// <summary>
    ///This is a test class for LogTest and is intended
    ///to contain all LogTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LogTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        
        /// <summary>
        ///A test for Log Constructor
        ///</summary>
        [TestMethod()]
        public void LogConstructorTest()
        {
            Log target = new Log();
            Assert.IsInstanceOfType(target, typeof(Log));
        }
    }
}
