﻿using System;
using UBM.Logger;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UBM.Logger.UnitTests
{
    
    
    /// <summary>
    ///This is a test class for BusinessProcessTest and is intended
    ///to contain all BusinessProcessTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BusinessProcessTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        ///A test for BusinessProcess Constructor
        ///</summary>
        [TestMethod()]
        public void BusinessProcessConstructorTest()
        {
            string process = "Proc";
            string subProcess = "SubProc";
            string step = "Step";
            BusinessProcess target = new BusinessProcess(process, subProcess, step);
            Assert.IsInstanceOfType(target, typeof(BusinessProcess));
            
        }

        /// <summary>
        ///A test for Process
        ///</summary>
        [TestMethod()]
        public void ProcessTest()
        {
            string process = "Proc";
            string subProcess = "SubProc";
            string step = "Step";
            BusinessProcess target = new BusinessProcess(process, subProcess, step); // TODO: Initialize to an appropriate value
            string expected = "Proc"; // TODO: Initialize to an appropriate value
            string actual;
            target.Process = expected;
            actual = target.Process;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for Step
        ///</summary>
        [TestMethod()]
        public void StepTest()
        {
            string process = "Proc";
            string subProcess = "SubProc";
            string step = "Step";
            BusinessProcess target = new BusinessProcess(process, subProcess, step); // TODO: Initialize to an appropriate value
            string expected = "Step"; // TODO: Initialize to an appropriate value
            string actual;
            target.Step = expected;
            actual = target.Step;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SubProcess
        ///</summary>
        [TestMethod()]
        public void SubProcessTest()
        {
            string process = "Proc";
            string subProcess = "SubProc";
            string step = "Step";
            BusinessProcess target = new BusinessProcess(process, subProcess, step); // TODO: Initialize to an appropriate value
            string expected = "SubProc";
            string actual;
            target.SubProcess = expected;
            actual = target.SubProcess;
            Assert.AreEqual(expected, actual);
        }
    }
}
