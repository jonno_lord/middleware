﻿using System;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using UBM.Logger;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UBM.Logger.UnitTests
{
    
    
    /// <summary>
    ///This is a test class for HandlerTest and is intended
    ///to contain all HandlerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HandlerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        /// <summary>
        ///A test for Handler Constructor
        ///</summary>
        [TestMethod()]
        public void HandlerConstructorTest()
        {
            Handler target = new Handler();
            Assert.IsInstanceOfType(target, typeof(Handler));
        }
        

        /// <summary>
        ///A test for ConfigFileIsParsed
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void ConfigFileIsParsedTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.ConfigFileIsParsed = expected;
            actual = target.ConfigFileIsParsed;
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        ///A test for DataBase
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void DataBaseTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            string expected = "a db"; // TODO: Initialize to an appropriate value
            string actual;
            target.DataBase = expected;
            actual = target.DataBase;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for LogActive
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void LogActiveTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.LogActive = expected;
            actual = target.LogActive;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for LogFile
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void LogFileTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            string expected = "x"; // TODO: Initialize to an appropriate value
            string actual;
            target.LogFile = expected;
            actual = target.LogFile;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for LogFileDateSuffix
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void LogFileDateSuffixTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            string expected = "A date"; // TODO: Initialize to an appropriate value
            string actual;
            target.LogFileDateSuffix = expected;
            actual = target.LogFileDateSuffix;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for MatchWords
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void MatchWordsTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            Dictionary<Handler_Accessor.MatchType, Handler_Accessor.MatchParams> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<Handler_Accessor.MatchType, Handler_Accessor.MatchParams> actual;
            target.MatchWords = expected;
            actual = target.MatchWords;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for SeverityLevel
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void SeverityLevelTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            Severity_Accessor expected = Severity_Accessor.Error; // TODO: Initialize to an appropriate value
            Severity_Accessor actual;
            target.SeverityLevel = expected;
            actual = target.SeverityLevel;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for VerboseLevel
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void VerboseLevelTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            Handler_Accessor.Verbose expected = Handler_Accessor.Verbose.Off; // TODO: Initialize to an appropriate value
            Handler_Accessor.Verbose actual;


            target.VerboseLevel = expected;
            actual = target.VerboseLevel;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for WriteToDB
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void WriteToDBTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.WriteToDB = expected;
            actual = target.WriteToDB;
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for WriteToFile
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.Logger.dll")]
        public void WriteToFileTest()
        {
            Handler_Accessor target = new Handler_Accessor(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.WriteToFile = expected;
            actual = target.WriteToFile;
            Assert.AreEqual(expected, actual);
 
        }
    }
}
