﻿using System;
using UBM.Logger;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UBM.Logger.UnitTests
{
    
    
    /// <summary>
    ///This is a test class for MessageTest and is intended
    ///to contain all MessageTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MessageTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        /// <summary>
        ///A test for Message Constructor
        ///</summary>
        [TestMethod()]
        public void MessageConstructorTest()
        {
            Message target = new Message();
            Assert.IsInstanceOfType(target, typeof(Message));
        }

        /// <summary>
        ///A test for Application
        ///</summary>
        [TestMethod()]
        public void ApplicationTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            string expected = "App1"; // TODO: Initialize to an appropriate value
            string actual;
            target.ApplicationName = expected;
            actual = target.ApplicationName;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for BusinessProcess
        ///</summary>
        [TestMethod()]
        public void BusinessProcessTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            BusinessProcess expected = new BusinessProcess("Proc", "SubProc", "Step"); // TODO: Initialize to an appropriate value
            BusinessProcess actual;
            target.BusinessProcess = expected;
            actual = target.BusinessProcess;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Class
        ///</summary>
        [TestMethod()]
        public void ClassTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            string expected = "Class"; // TODO: Initialize to an appropriate value
            string actual;
            target.ClassName = expected;
            actual = target.ClassName;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FileName
        ///</summary>
        [TestMethod()]
        public void FileNameTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            string expected = "File"; // TODO: Initialize to an appropriate value
            string actual;
            target.FileName = expected;
            actual = target.FileName;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for LineNumber
        ///</summary>
        [TestMethod()]
        public void LineNumberTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            int expected = 22; // TODO: Initialize to an appropriate value
            int actual;
            target.LineNumber = expected;
            actual = target.LineNumber;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for LogText
        ///</summary>
        [TestMethod()]
        public void LogTextTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            string expected = "Message"; // TODO: Initialize to an appropriate value
            string actual;
            target.LogText = expected;
            actual = target.LogText;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for MetaData
        ///</summary>
        [TestMethod()]
        public void MetaDataTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            MetaData expected = new MetaData(); // TODO: Initialize to an appropriate value
            MetaData actual;
            target.MetaData = expected;
            actual = target.MetaData;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Method
        ///</summary>
        [TestMethod()]
        public void MethodTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            string expected = "Method"; // TODO: Initialize to an appropriate value
            string actual;
            target.MethodName = expected;
            actual = target.MethodName;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SeverityLevel
        ///</summary>
        [TestMethod()]
        public void SeverityLevelTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            Severity expected = new Severity();
            expected = Severity.Error;
            Severity actual;
            target.SeverityLevel = expected;
            actual = target.SeverityLevel;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for TimeStamp
        ///</summary>
        [TestMethod()]
        public void TimeStampTest()
        {
            Message target = new Message(); // TODO: Initialize to an appropriate value
            DateTime expected = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime actual;
            target.TimeStamp = expected;
            actual = target.TimeStamp;
            Assert.AreEqual(expected, actual);
        }
    }
}
