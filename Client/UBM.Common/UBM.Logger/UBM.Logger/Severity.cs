﻿
namespace UBM.Logger
{
    internal enum Severity
    {
        Error,
        Warning,
        Information,
        Unknown
    }
}
