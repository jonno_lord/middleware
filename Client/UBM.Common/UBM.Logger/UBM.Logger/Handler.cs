﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace UBM.Logger
{
    internal class Handler
    {
        #region Private Fields

        private const int MAX_FILE_SIZE = 15000000;
        private const string DIAGNOSTIC_FILE = "LoggerDiagnostic.txt"; // Stores errors about the logger

        private static readonly object Locker = new object();    // This is here to make file writing thread safe

        #endregion

        #region Private Properties

        private bool LogActive { get; set; }

        #endregion

        #region Private Methods

        private static string FormatLog(Message message)
        {
            // Define strings for basic verbosity - for file writing
            string formattedMessage = message.TimeStamp + " " + message.SeverityLevel.ToString().ToUpper() + " - " + message.LogText;
            string formattedDetails = " User:" + message.UserName + " File:" + message.FileName + " App:" + message.ApplicationName
                                       + " Class:" + message.ClassName + " Method:" + message.MethodName + " Line:" + message.LineNumber;

            return formattedMessage + " DETAILS:" + formattedDetails;
        }

        private static void DiagnosticWrite(string diagnosticText, Exception diagnosticException = null)
        {
#if (DEBUG)

            string diagnosticPath = AppDomain.CurrentDomain.BaseDirectory + DIAGNOSTIC_FILE;
            string diagnosticConcat = "";

            if (diagnosticText != "") diagnosticConcat += diagnosticText + " ";
            if (diagnosticException != null) diagnosticConcat += diagnosticException;

            try
            {
                FileInfo info = new FileInfo(diagnosticPath);
                if (File.Exists(diagnosticPath) && info.Length > MAX_FILE_SIZE)
                {
                    File.Delete(diagnosticPath);   // when diagnostic file gets too big it scraps it
                }

                lock (Locker)
                {
                    StreamWriter file = new StreamWriter(diagnosticPath, true);
                    file.WriteLine(DateTime.Now + " " + diagnosticConcat);
                    file.Close();
                }
                ConsoleWrite(diagnosticConcat);
            }
            catch (Exception exception)
            {
                ConsoleWrite("", exception);
            }

#endif
        }

        private static void ConsoleWrite(string consoleText, Exception consoleException = null)
        {
#if (DEBUG)
       
            string consoleConcat = DateTime.Now + " ";
            if (consoleText != "") consoleConcat += consoleText + " ";
            if (consoleException != null) consoleConcat += consoleException;

            Console.WriteLine(consoleConcat);
        
#endif
        }

        private static void LogToDb(Message message)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["LoggerConnectionString"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("usp_INS_Logs", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LogTime", message.TimeStamp);
                    command.Parameters.AddWithValue("@Severity", Enum.GetName(typeof(Severity), message.SeverityLevel));
                    command.Parameters.AddWithValue("@Message", message.LogText);
                    command.Parameters.AddWithValue("@UserName", message.UserName);
                    command.Parameters.AddWithValue("@FileName", message.FileName);
                    command.Parameters.AddWithValue("@ApplicationName", message.ApplicationName);
                    command.Parameters.AddWithValue("@ClassName", message.ClassName);
                    command.Parameters.AddWithValue("@MethodName", message.MethodName);
                    command.Parameters.AddWithValue("@LineNumber", message.LineNumber);
                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    connection.Close();
                }

            }
            catch (Exception exception)
            {
                DiagnosticWrite("Check DB Connection String in Config File", exception);
            }

        }

        private static void LogToFile(string formattedLog)
        {
            ConsoleWrite(formattedLog);


            //-- checks on logfile key exists, SP 
            if (ConfigurationManager.AppSettings["LogFile"] == null)
            {
                return;
            }

            string logFilePath = ConfigurationManager.AppSettings["LogFile"];

            //-- check log file has been specified, SP 
            if(logFilePath.Length <1)
            {
                return;
            }

            try
            {
                if(File.Exists(logFilePath))
                {
                    FileInfo info = new FileInfo(logFilePath);
                    if (info.Length > MAX_FILE_SIZE)
                    {
                        File.Delete(logFilePath);
                    }  
                }

                // Make thread-safe
                lock (Locker)
                {
                    StreamWriter fileWriter = new StreamWriter(logFilePath, true); // first to arrive
                    fileWriter.WriteLine(formattedLog);
                    fileWriter.Close();
                }
            }
            catch (Exception exception)
            {
                DiagnosticWrite(exception.ToString());
            }
        
        }

        #endregion

        #region Constructor

        public Handler()
        {
            string active = ConfigurationManager.AppSettings["Logging"];
            LogActive = active != null && active.ToUpper() == "ON";
        }

        #endregion

        #region Public Methods

        public void MessageProcess(Message message)
        {
            if (LogActive)
            {
                string formattedLog = FormatLog(message);
                LogToFile(formattedLog);
                LogToDb(message);
            }
        }

        #endregion

    }
}
