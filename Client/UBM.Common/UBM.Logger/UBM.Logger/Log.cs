﻿using System;
using System.Diagnostics;
using System.Linq;

namespace UBM.Logger
{
    /// <summary>
    /// The Log class allows the creation of Log messages which can be either written to a file or database or both.
    /// Settings are controlled by the LogConfig.xml file which should be stored in the same directory as the dll
    /// </summary>
    public static class Log
    {

        #region Private Methods

        private static void PreProcess(Severity severity, Exception exception, string additionalMessage)
        {
            Handler handler = new Handler();

            // First create an instance of the call stack   
            //StackTrace callStack = new StackTrace();
            StackTrace callStack = new StackTrace(1, true);

            // 0 : current frame for the current method   
            // 1 : Frame that called the current method     
            StackFrame frame = callStack.GetFrame(1);

            // Create instance of message and populate the properties with the stack information
            Message message = new Message
                          {
                              MethodName = frame.GetMethod().Name,
                              ClassName = frame.GetMethod().DeclaringType.Name,
                              ApplicationName = frame.GetMethod().DeclaringType.Assembly.GetName().Name,
                              LineNumber = frame.GetFileLineNumber(),
                              SeverityLevel = severity,
                              LogText = additionalMessage
                          };

            if (exception != null) message.LogText += " " + exception;
            message.LogText = message.LogText.Trim();

            string fileAddress = frame.GetFileName();
            if (fileAddress != null)
            {
                string[] fileAddressSplit = fileAddress.Split('\\');
                message.FileName = fileAddressSplit.Last();
            }
            else
            {
                message.FileName = "";
            }

            // Fires message to the handler for more processing and writing
            handler.MessageProcess(message);

        }

        #endregion

        #region Constructor/Public Methods


        /// <summary>
        /// The Error function should be called to log high severity problems where an exception is thrown
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="exception"></param>
        public static void Error(string errorMessage, Exception exception)
        {
            PreProcess(Severity.Error, exception, errorMessage);
        }

        /// <summary>
        /// The Warning function should be called to log problems such as validation issues or other non-fatal problems
        /// </summary>
        /// <param name="warningMessage"></param>
        public static void Warning(string warningMessage)
        {
            PreProcess(Severity.Warning, null, warningMessage);
        }

        /// <summary>
        /// The Information function should be used for any other useful information logging.
        /// </summary>
        /// <param name="informationMessage"></param>
        public static void Information(string informationMessage)
        {
            PreProcess(Severity.Information, null, informationMessage);
        }

        #endregion

    }
}
