﻿using System;

namespace UBM.Logger
{
    internal class Message
    {
        internal string UserName { get; set; }
        internal string ApplicationName { get; set; }
        internal string FileName { get; set; }
        internal string ClassName { get; set; }
        internal string MethodName { get; set; }
        internal int LineNumber { get; set; }
        internal string LogText { get; set; }

        internal DateTime TimeStamp { get; set; }
        internal Severity SeverityLevel { get; set; }

        internal Message()
        {
            TimeStamp = DateTime.Now;
            UserName = Environment.UserName;
        }

    }
}
