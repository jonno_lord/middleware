﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using UBM.RemoteSSISExecutor.Client.PackageRunner;
using RemoteExecutor = UBM.RemoteSSISExecutor.Client.RemoteExecutor;
using ProgressReportEventArgs = UBM.RemoteSSISExecutor.Client.ProgressReportEventArgs;
using PackageFinishedEventHandlerArgs = UBM.RemoteSSISExecutor.Client.PackageFinishedEventHandlerArgs;
using System.Configuration;
using System.Collections.Specialized;



namespace UBM.RemoteSSISExecutor.GUI
{


    public partial class MainForm : Form, IDisposable
    {

        private int rowCount;

        private RemoteExecutor remoteExecutor;

        public MainForm()
        {
            InitializeComponent();

            progressGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            
        }

        private void PackageBrowseButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = openFileDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                packagePathTextBox.Text = openFileDialog.FileName;
            }

        }

        private void LaunchButton_Click(object sender, EventArgs e)
        {
            progressGrid.Rows.Clear();

            Dictionary<string, string> connections = new Dictionary<string, string>();
            
            //connections.Add("BAI_EXTRACT", "Data Source=APJDETEST01;User ID=uat811dta;Password=UAT811DTA;Initial Catalog=BAI_EXTRACT;Provider=SQLOLEDB.1;Persist Security Info=True;Auto Translate=False;Application Name=SSIS-BAI Transfer to JDE 1-{76F85442-4327-4FB0-8EEE-DF4AD3E08132}APJDETEST01.uat811dta");

            //connections.Add("APJDETEST01.UAT811DTA", ConfigurationManager.ConnectionStrings["APJDETEST01.UAT811DTA"].ToString());

            //connections.Add("RevenueRecognition", ConfigurationManager.ConnectionStrings["RevenueRecognition"].ToString());

            connections.Add("APJDETEST01.PS_UAT.UAT811DTA", ConfigurationManager.ConnectionStrings["APJDETEST01.PS_UAT.UAT811DTA"].ToString());

            connections.Add("DEVMWR02.RevenueRecognition.MiddlewareSystem", ConfigurationManager.ConnectionStrings["DEVMWR02.RevenueRecognition.MiddlewareSystem"].ToString());



            remoteExecutor = new RemoteExecutor(packagePathTextBox.Text, connections, null);

            rowCount = 0;

            remoteExecutor.OnProgressReport += OnProgressReport;
            remoteExecutor.OnPackageFinished += OnPackageFinished;

            PackageStatus status = remoteExecutor.FirePackage();
            
        }

        private void OnProgressReport(object sender, ProgressReportEventArgs args)
        {

            progressGrid.Rows.Add(1);
            //progressGrid[0, rowCount].Value = rowCount + 1;
            progressGrid[0, rowCount].Value = args.PackageFeedback.SequenceNumber;
            progressGrid[1, rowCount].Value = args.PackageFeedback.Time;
            progressGrid[2, rowCount].Value = args.PackageFeedback.Message;

            progressGrid[3, rowCount].Value = args.PackageFeedback.PackageEvent;
            progressGrid[4, rowCount].Value = args.PackageFeedback.PackageStatus;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.WrapMode = DataGridViewTriState.True;

            progressGrid.Rows[rowCount].Cells[2].Style = style;

            rowCount++;

        }


        private void OnPackageFinished(object sender, PackageFinishedEventHandlerArgs args)
        {

            if (args.Summary.ErrorCount > 0)
            {
                MessageBox.Show("Failed");
            }
            else
            {
                MessageBox.Show("Success");
            }
        }


    }
}
