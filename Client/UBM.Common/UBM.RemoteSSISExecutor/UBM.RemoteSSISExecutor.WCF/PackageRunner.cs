﻿using System;
using System.ServiceModel;
using UBM.RemoteSSISExecutor.WCF.Contracts;

namespace UBM.RemoteSSISExecutor.WCF
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class PackageRunner : IPackageRunner
    {

        public PackageStatus FirePackage(string packagePath)
        {
            return FirePackage(packagePath, new PackageArguments());
        }


        public PackageStatus FirePackage(string packagePath, PackageArguments packageArguments)
        {
            PackageStatus status;
            FeedbackManager feedbackManager = null;

            try
            {
                IPackageRunnerCallback callback = OperationContext.Current.GetCallbackChannel<IPackageRunnerCallback>();
                feedbackManager = new FeedbackManager(callback);
                SSISPackage package = new SSISPackage(feedbackManager);
                status = package.Fire(packagePath, packageArguments.ConnectionStrings, packageArguments.PackageVariables);  
            }
            catch (Exception exception)
            {
                if (feedbackManager != null)
                {
                    feedbackManager.SendProgressReport(PackageEvent.Error, exception.ToString(), PackageStatus.Failed);
                }

                status = PackageStatus.Failed;
            }

            return status;
        }
    }
}
