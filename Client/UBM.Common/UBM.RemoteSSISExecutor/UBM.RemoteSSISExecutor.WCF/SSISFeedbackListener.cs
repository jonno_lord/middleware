﻿using System;
using System.Collections.Generic;
using UBM.RemoteSSISExecutor.WCF.Contracts;
using UBM.SSISExecutor;

namespace UBM.RemoteSSISExecutor.WCF
{
    public class SSISFeedbackListener : SSISEventListener
    {

        private readonly FeedbackManager feedbackManager;

        private readonly int numberOfSteps;
        private int numberOfErrors;
        private int stepCount;

        public SSISFeedbackListener(FeedbackManager feedbackManager, int numberOfSteps)
        {
            stepCount = 0;

            this.numberOfSteps = numberOfSteps;
            this.feedbackManager = feedbackManager;
        }

        public override void OnProgress(Microsoft.SqlServer.Dts.Runtime.TaskHost taskHost, string progressDescription, int percentComplete, int progressCountLow, int progressCountHigh, string subComponent, ref bool fireAgain)
        {
            feedbackManager.SendProgressReport(PackageEvent.Information, progressDescription);
            base.OnProgress(taskHost, progressDescription, percentComplete, progressCountLow, progressCountHigh, subComponent, ref fireAgain);
        }

       
        public override void OnWarning(Microsoft.SqlServer.Dts.Runtime.DtsObject source, int warningCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            feedbackManager.SendProgressReport(PackageEvent.Warning, description);
            base.OnWarning(source, warningCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError);
        }

        public override void OnInformation(Microsoft.SqlServer.Dts.Runtime.DtsObject source, int informationCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError, ref bool fireAgain)
        {
            feedbackManager.SendProgressReport(PackageEvent.Information, description);
            base.OnInformation(source, informationCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError, ref fireAgain);
        }

        public override bool OnError(Microsoft.SqlServer.Dts.Runtime.DtsObject source, int errorCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            numberOfErrors++;
            feedbackManager.SendProgressReport(PackageEvent.Error, description);
            return base.OnError(source, errorCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError);
        }

        
        public override void OnPostExecute(Microsoft.SqlServer.Dts.Runtime.Executable exec, ref bool fireAgain)
        {
            stepCount++;
            if (stepCount == numberOfSteps)
            {
                feedbackManager.SendSummary(numberOfErrors);
            }
        
            base.OnPostExecute(exec, ref fireAgain);
        }

    }
}
