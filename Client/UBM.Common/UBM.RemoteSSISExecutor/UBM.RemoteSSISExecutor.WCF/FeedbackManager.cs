﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.RemoteSSISExecutor.WCF.Contracts;

namespace UBM.RemoteSSISExecutor.WCF
{
    public class FeedbackManager
    {
        private readonly IPackageRunnerCallback callback;
        private readonly List<PackageFeedback> feedbackList;

        private int stepCount = 0;

        public FeedbackManager(IPackageRunnerCallback callback)
        {
            this.callback = callback;

            feedbackList = new List<PackageFeedback>();
        }

        public void SendProgressReport(PackageEvent packageEvent, string description, PackageStatus packageStatus = PackageStatus.Running)
        {
            PackageFeedback feedback = new PackageFeedback
            {
                Message = description,
                PackageEvent = packageEvent,
                PackageStatus = packageStatus,
                SequenceNumber = stepCount
            };

            feedbackList.Add(feedback);

            callback.ProgressReport(feedback);

            stepCount++;
        }

        public void SendSummary(int numberOfErrors)
        {
            SendProgressReport(PackageEvent.Information, "Complete - " + numberOfErrors + " error(s)", PackageStatus.Complete);

            callback.PackageFinished(new PackageSummary
            {
                ErrorCount = numberOfErrors,
                Feedback = feedbackList
            });
        }
    }
}
