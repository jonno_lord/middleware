﻿using System.ServiceModel;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    public interface IPackageRunnerCallback
    {
        [OperationContract(IsOneWay = true)]
        void ProgressReport(PackageFeedback feedback);

        [OperationContract(IsOneWay = true)]
        void PackageFinished(PackageSummary packageSummary);

    }
}
   