﻿using System.Runtime.Serialization;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    [DataContract]
    public enum PackageEvent
    {
        [EnumMember]
        Error,
        [EnumMember]
        Warning,
        [EnumMember]
        Information
    }
}
