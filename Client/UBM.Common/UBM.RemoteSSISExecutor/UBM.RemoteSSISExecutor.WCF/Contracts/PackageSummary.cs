﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    [DataContract]
    public class PackageSummary
    {
        [DataMember]
        public List<PackageFeedback> Feedback { get; set; }

        [DataMember]
        public int ErrorCount { get; set; }


    }
}
