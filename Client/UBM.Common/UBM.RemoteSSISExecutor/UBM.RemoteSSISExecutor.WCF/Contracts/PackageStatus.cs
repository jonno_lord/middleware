﻿using System.Runtime.Serialization;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    [DataContract]
    public enum PackageStatus
    {
        [EnumMember]
        Started,
        [EnumMember]
        Failed,
        [EnumMember]
        Running,
        [EnumMember]
        Complete,
        [EnumMember]
        DoesNotExist,
        [EnumMember]
        LoadFailed
    }
}