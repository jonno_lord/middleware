﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    [DataContract]
    public class PackageArguments
    {

        [DataMember]
        public Dictionary<string, string> ConnectionStrings { get; set; }

        
        [DataMember]
        public Dictionary<string, object> PackageVariables { get; set; }
    }
}
