﻿using System;
using System.Runtime.Serialization;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    [DataContract]
    public class PackageFeedback
    {
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public PackageStatus PackageStatus { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public DateTime Time { get; private set; }

        [DataMember]
        public PackageEvent PackageEvent { get; set; }

        public PackageFeedback()
        {
            Time = DateTime.Now;
        }
    }
}
