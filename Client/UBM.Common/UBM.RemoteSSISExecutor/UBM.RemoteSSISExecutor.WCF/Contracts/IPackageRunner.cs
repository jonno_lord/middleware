﻿using System.ServiceModel;

namespace UBM.RemoteSSISExecutor.WCF.Contracts
{
    [ServiceContract(
    CallbackContract = typeof(IPackageRunnerCallback),
    SessionMode = SessionMode.Required)]
    public interface IPackageRunner
    {
        [OperationContract]
        PackageStatus FirePackage(string packagePath);

        [OperationContract(Name = "FirePackageWithArguments")]
        PackageStatus FirePackage(string packagePath, PackageArguments packageArguments);
    }
}
