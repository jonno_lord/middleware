﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Microsoft.SqlServer.Dts.Runtime;
using UBM.RemoteSSISExecutor.WCF.Contracts;
using UBM.SSISExecutor;

namespace UBM.RemoteSSISExecutor.WCF
{
    public class SSISPackage
    {
        private readonly FeedbackManager feedbackManager;

        private void SetConnectionStrings(DTSPackage package, Dictionary<string, string> connectionStrings)
        {            
            if(connectionStrings != null && connectionStrings.Count > 0)
            {                               
                package.SetPackageConnections(new Hashtable(connectionStrings),0,0,0); 

                string connections = "Connection strings received: -";
                connectionStrings.ToList().ForEach(s => connections += "\n" + s.Key + " - " + s.Value);

                feedbackManager.SendProgressReport(PackageEvent.Information, connections);
            }
        }

        private void SetPackageVariables(DTSPackage package, Dictionary<string,object> variables)
        {
            if (variables != null && variables.Count > 0)
            {
                foreach (var variable in variables)
                {
                    package.pkg.Variables[variable.Key].Value = variable.Value;
                }

                string variablesConcat = "Variables received: -";
                variables.ToList().ForEach(s => variablesConcat += "\n" + s.Key + " - " + s.Value);

                feedbackManager.SendProgressReport(PackageEvent.Information, variablesConcat);
            }

        }

        public SSISPackage(FeedbackManager feedbackManager)
        {
            this.feedbackManager = feedbackManager;
        }


        public PackageStatus Fire(string jobName, Dictionary<string, string> connectionStrings, Dictionary<string, object> variables)
        {

           FileInfo fileInfo = new FileInfo(jobName);

            if (!fileInfo.Exists)
            {
                feedbackManager.SendProgressReport(PackageEvent.Error, "Could not find package",
                                                   PackageStatus.DoesNotExist);

                return PackageStatus.DoesNotExist;
            }

            DTSPackage package = new DTSPackage();
            package.LoadPackage(jobName);

            if (package.PackageStatus != DTSPackage.DTSPackageStatus.Loaded)
            {
                return PackageStatus.LoadFailed;
            }

            SetConnectionStrings(package, connectionStrings);
            SetPackageVariables(package, variables);

            SSISFeedbackListener eventListener = new SSISFeedbackListener(feedbackManager, package.pkg.Executables.Count);
            Logger.Log.Information("ERROR");
            package.Execute(eventListener);

            return PackageStatus.Started;
        }
    }
}
