﻿using System.ServiceProcess;

namespace UBM.RemoteSSISExecutorService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new RemoteSSISExecutorService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
