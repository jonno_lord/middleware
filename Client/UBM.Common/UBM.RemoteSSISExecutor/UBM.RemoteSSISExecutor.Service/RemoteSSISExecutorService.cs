﻿using System.ServiceModel;
using System.ServiceProcess;


namespace UBM.RemoteSSISExecutorService
{
    partial class RemoteSSISExecutorService : ServiceBase
    {
        private ServiceHost host;

        public RemoteSSISExecutorService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            host = new ServiceHost(typeof(UBM.RemoteSSISExecutor.WCF.PackageRunner));
            host.Open();
        }

        protected override void OnStop()
        {
            host.Close();
        }
    }
}
