﻿using UBM.RemoteSSISExecutor.Client.PackageRunner;

namespace UBM.RemoteSSISExecutor.Client
{
    public delegate void PackageFinishedEventHandler(object sender, PackageFinishedEventHandlerArgs args);
    public class PackageFinishedEventHandlerArgs
    {
        public PackageSummary Summary { get; set; }
    }
}