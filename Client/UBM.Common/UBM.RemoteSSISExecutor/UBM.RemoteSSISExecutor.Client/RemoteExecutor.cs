﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using UBM.RemoteSSISExecutor.Client.PackageRunner;
using UBM.RemoteSSISExecutor.WCF;



namespace UBM.RemoteSSISExecutor.Client
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
    CallbackBehavior(UseSynchronizationContext = false)]
    public class RemoteExecutor : IPackageRunnerCallback
    {

        private readonly SynchronizationContext context;
        private readonly PackageRunnerClient client;
       
        private readonly string packageName;
        private readonly PackageArguments packageArguments;

        public event ProgressReportEventHandler OnProgressReport;
        public event PackageFinishedEventHandler OnPackageFinished;
        

        public RemoteExecutor(string packageName)
        {
            client = new PackageRunnerClient(new InstanceContext(this));
            context = SynchronizationContext.Current;

            this.packageName = packageName;
        }

        public RemoteExecutor(string packageName, Dictionary<string, string> connectionStrings, Dictionary<string, object> variables)
            : this (packageName)
        {
            packageArguments = new PackageArguments
                                   {
                                       ConnectionStrings = connectionStrings,
                                       PackageVariables = variables
                                   };
        }

        public PackageStatus FirePackage()
        {
            if(packageArguments == null)
            {
                return client.FirePackage(packageName);    
            }

            return client.FirePackageWithArguments(packageName, packageArguments);

        }

        public void ProgressReport(PackageFeedback feedback)
        {
            SendOrPostCallback callback = delegate
            {
                ProgressReportEventHandler handler = OnProgressReport;
                if (handler != null)
                {
                    handler(this, new ProgressReportEventArgs
                    {
                        PackageFeedback = feedback
                    });
                }

            };
            context.Post(callback, null);
         
        }

        public void PackageFinished(PackageSummary summary)
        {
            SendOrPostCallback callback = delegate
            {
                PackageFinishedEventHandler handler = OnPackageFinished;
                if (handler != null)
                {
                    handler(this, new PackageFinishedEventHandlerArgs
                    {
                        Summary = summary
                    });
                }

                CloseClient();
            };
             
            context.Post(callback, null);
           
        }

     

        private void CloseClient()
        {

            if (client.State != CommunicationState.Faulted)
            {
                client.Close();
            }
            else
            {
                client.Abort();
            }
        }
    }

   
}
