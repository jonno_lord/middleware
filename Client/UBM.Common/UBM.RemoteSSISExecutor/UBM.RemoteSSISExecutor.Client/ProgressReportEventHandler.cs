﻿using System;
using UBM.RemoteSSISExecutor.Client.PackageRunner;

namespace UBM.RemoteSSISExecutor.Client
{
    public delegate void ProgressReportEventHandler(object sender, ProgressReportEventArgs args);

    public class ProgressReportEventArgs : EventArgs
    {
        public PackageFeedback PackageFeedback { get; set; }
    }
}