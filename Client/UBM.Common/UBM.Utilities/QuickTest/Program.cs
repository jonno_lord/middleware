﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.Utilities;
using UBM.Utilities.HtmlReportGenerator;

namespace QuickTest
{
    class Program
    {


        static void Main(string[] args)
        {
            Program p = new Program();

            //p.InvoicingReport();
            p.CustomerRejectionReport();
        }

        private void InvoicingReport()
        {
            HtmlReport report = new HtmlReport();

            report.AddHeader("Middleware Invoicing Job Report");

            report.AddParagraph("This thing went fine");

            report.AddParagraph("This thing went bad", TextStyleName.Failure);

            report.AddParagraph("This thing had some warnings", TextStyleName.Caution);

            report.AddHorizontalRule();

            Table table = HtmlReport.CreateTable(TableStyleName.MiddlewarePublicationInvoicingReport).
                AddHeader(new List<string> {"Invoice", "System", "Issue Date"}).
                AddRow(new List<string> {"Farmers Guardian", "ASOPUK", "30/09/12"}).
                AddRow(new List<string> {"Farmers Guardian", "ASOPUK", "30/09/12"}).
                AddRow(new List<string> {"Farmers Guardian", "ASOPUK", "30/09/12"}).
                AddRow(new List<string> {"Farmers Guardian", "ASOPUK", "30/09/12"});

            report.AddTable(table);

            report.AddParagraph("The referenced account has been rejected from Middleware as there is a Zero order value/balance. Bahhhhh");
        }

        private void CustomerRejectionReport()
        {
            HtmlReport report = new HtmlReport();

            report.AddHeader("Test Report");

            report.AddParagraph("This thing went fine");

            report.AddParagraph("This thing went bad", TextStyleName.Failure);

            report.AddParagraph("This thing had some warnings", TextStyleName.Caution);

            report.AddHorizontalRule();

            Table table = HtmlReport.CreateTable(TableStyleName.MiddlewareCustomerRejection).
                                                AddRow(new List<string> { "Customer Name:", "A.P.Moller-Maersk Group" }).
                                                AddRow(new List<string> { "Customer URN:", "1573580" }).
                                                AddRow(new List<string> { "Order URN:", "1625284" }).
                                                AddRow(new List<string> { "Rejected By:", "JPresly" });
            report.AddTable(table);

            report.AddParagraph("The referenced account has been rejected from Middleware as there is a Zero order value/balance. Bahhhhh");
        }
    }
}
