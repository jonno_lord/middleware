﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading;

namespace UBM.Utilities
{
    /// Useful URL's for researching LDAP queries
    /// http://www.c-sharpcorner.com/UploadFile/dhananjaycoder/activedirectoryoperations11132009113015AM/activedirectoryoperations.aspx
    /// http://www.pardesifashions.com/Softomatix/GroupMembers.aspx
    public class LdapHelper
    {
        
        #region Fields

        private readonly string applicationDomain;

        #endregion

        #region Private Methods

        private static IEnumerable<string> GetGroupMembers(string strDomain, string strGroup)
        {

            List<string> groupMembers = new List<string>();
            DirectoryEntry ent = new DirectoryEntry("LDAP://DC=" + strDomain + ",DC=com");
            DirectorySearcher srch = new DirectorySearcher("(CN=" + strGroup + ")");
            SearchResultCollection coll = srch.FindAll();
            foreach (SearchResult rs in coll)
            {
                ResultPropertyCollection resultPropColl = rs.Properties;
                foreach (object memberColl in resultPropColl["member"])
                {
                    DirectoryEntry gpMemberEntry = new DirectoryEntry("LDAP://" + memberColl);
                    System.DirectoryServices.PropertyCollection userProps = gpMemberEntry.Properties;
                    object obVal = userProps["mail"].Value;
                    if (null != obVal)
                    {
                        groupMembers.Add(obVal.ToString());
                    }
                }
            }
            return groupMembers.ToArray();
        }

        private string GetUserProperty(string accountName, string propertyName, string filterType)
        {
            // Changed by dgrenter on 23/07/2014 as it was hardcoded to + .com but since we have moved to UBM.net GAD it stopped working.
            // Path = "LDAP://" + applicationDomain + ".com;
            // applicationDomain is stored in the web.config.
            DirectoryEntry entry = new DirectoryEntry
            {
                Path = "LDAP://" + applicationDomain,
                AuthenticationType = AuthenticationTypes.Secure
            };

            DirectorySearcher search = new DirectorySearcher(entry)
            {
                Filter = "(" + filterType + "=" + accountName + ")"
            };

            search.PropertiesToLoad.Add(propertyName);

            SearchResultCollection results = search.FindAll();
            if (results.Count > 0)
            {
                return results[0].Properties[propertyName][0].ToString();
            }
            return "";  //-- no match found on this filter
        }

        private bool IsEmailAddress(string address)
        {
            return GetUserProperty(address, "mail", "mail").Length != 0;
        }

        private bool IsDisplayName(string displayname)
        {
            return GetUserProperty(displayname, "mail", "displayName").Length != 0;
        }

        private bool IsLoginName(string login)
        {
            return GetUserProperty(login, "mail", "SAMAccountName").Length != 0;
        }

        private bool IsGroup(string group)
        {
            return GetGroupMembers(applicationDomain, group).Count() > 0;
        }

        #endregion

        #region Constructor

        public LdapHelper(string applicationDomain)
        {
            this.applicationDomain = applicationDomain;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Return all the ADS groups a user is a member of
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>List of groups user is a member of</returns>
        public string[] GetUsersGroups(string userName)
        {

            DirectoryEntry entry = new DirectoryEntry();


            // Changed by dgrenter on 23/07/2014 as it was hardcoded to + .com but since we have moved to UBM.net GAD it stopped working.
            // entry.Path = "LDAP://" + applicationDomain + ".com;
            // applicationDomain is stored in the web.config.
            entry.Path = "LDAP://" + applicationDomain;
            entry.AuthenticationType = AuthenticationTypes.Secure;


            string mail = GetUserProperty(userName, "mail", "SAMAccountName");

            DirectorySearcher search = new DirectorySearcher(entry);

            search.Filter = "(" + "mail" + "=" + mail + ")";
            search.PropertiesToLoad.Add("memberOf");

            SearchResultCollection results = search.FindAll();

            ResultPropertyValueCollection groupResults = results[0].Properties["memberOf"];

            List<string> groups = new List<string>();

            foreach (string group in groupResults)
            {
                groups.Add(group.Substring(group.IndexOf('=') + 1, group.IndexOf(',') - 3));
            }

            return groups.ToArray();
        }


        /// <summary>
        /// Accepts a recipient name (e.g. Steve Parker) and attempts to resolve it to either its email
        /// equivalent - or expand the group it belongs to e.g. UBM Development
        /// -- The  routine will attempt to resolve the name through three different ways
        /// <para>
        /// -- 1. the name supplied is an email address (e.g. David.Grenter@ubm.com)
        /// -- 2. the name supplied is an Distinguished Name (.g. David Grenter)
        /// -- 3. the name supplued is a Group, and individuals from that group needs to be returned (e.g. UBM Development)
        /// -- 4. the name supplued is a SAM account name (e.g. MFUK\DGrenter)
        /// </para>
        /// </summary>
        /// <param name="name">The recipient name to resolve</param>
        /// <returns>Either a list of the resolved addresses, an empty list for no matches, or null if the function created an error.</returns>
        public string[] GetEmailAddressFromNameOrUser(string name)
        {

            //-- return value of found strings
            List<string> names = new List<string>();

            //-- if nothing was supplied return an empty listing
            if (name.Length == 0) return names.ToArray();


            try
            {
                //-- format the displayname. if for example "David Grenter" was supplied, it should be
                //-- searched for as "Grenter, David"
                string displayName = name;
                if (!name.Contains(","))
                {
                    //-- split the names based on spaces.
                    string[] displayNames = name.Split(' ');
                    displayName = "";
                    //-- on the first element add a comma, all other elements add spaces

                    string firstNames = "";
                    string surnames = "";

                    for (int i = 0; i < displayNames.Count(); i++)
                    {
                        if (i == 0 || displayNames[i].Contains("."))
                        {
                            firstNames += displayNames[i] + " ";
                        }
                        else
                        {
                            surnames += displayNames[i] + " ";
                        }
                    }

                    displayName += surnames.Trim() + ", " + firstNames;
     
                    displayName = displayName.Trim();
                }

                //-- used to look for a login account name (e.g. DGrenter). Remove domain references
                string loginName = name.ToUpper().Replace(applicationDomain.ToUpper() + "\\", "");    //-- remove domain

                //-- check to see if the supplied address is an email, a group, a displayname or a login
                //-- name. Note the order is important - as a Group (e.g. IT Department) will also be
                //-- a LoginName
                if (IsEmailAddress(name))
                {
                    names.Add(GetUserProperty(name, "mail", "mail"));
                }
                else if (IsGroup(name))
                {
                    names.AddRange(GetGroupMembers(applicationDomain, name));
                }
                else if (IsDisplayName(displayName))
                {
                    names.Add(GetUserProperty(displayName, "mail", "displayName"));
                }
                else if (IsLoginName(loginName))
                {
                    names.Add(GetUserProperty(loginName, "mail", "SAMAccountName"));
                }
            }
            catch
            {
                //-- ignored
                return null;
            }

            return names.ToArray();
        }

        #endregion

    }
}
