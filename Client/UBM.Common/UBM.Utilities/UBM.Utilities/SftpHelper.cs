﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using Rebex.Net;

namespace UBM.Utilities
{
    public class SftpHelper
    {
        #region Fields

        private Sftp sftp;

        public string LocalDirectory { get; set; }
        public string SftpServerDirectory { get; set; }

        private string sftpUser = @"";
        private string sftpPassword = @"";

        
        #endregion

        #region Constructor

        public SftpHelper(string server, string user, string password, string local)
        {
            sftpUser = user;
            sftpPassword = password;

            SftpServerDirectory = server;
            LocalDirectory = local;
        }

        #endregion

        #region Public Methods

        public void DownloadFile(string fileDirectory, string fileName)
        {
            string sftpFilePath = LocalDirectory + "/" + fileName;
            string localFilePath = fileDirectory + @"\" + fileName;

            using (sftp = new Sftp())
            {
                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                sftp.Connect(SftpServerDirectory, 22, parms);
                sftp.Login(sftpUser, sftpPassword);

                try
                {
                    sftp.GetFile(sftpFilePath, localFilePath);
                }
                catch (Exception ex)
                {
                    //Some logging here...
                }
            }
        }

        public void UploadFile(string fileDirectory, string fileName)
        {
            string sftpFilePath = LocalDirectory + "/" + fileName;
            string localFilePath = fileDirectory + @"\" + fileName;

            using (sftp = new Sftp())
            {
                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                sftp.Connect(SftpServerDirectory, 22, parms);
                sftp.Login(sftpUser, sftpPassword);

                try
                {
                    sftp.PutFile(localFilePath, sftpFilePath);
                }
                catch (Exception ex)
                {
                    //Some logging here...
                }
            }
        }

        public bool UploadFiles(string fileDirectory)
        {
            using (sftp = new Sftp())
            {
                try
                {
                    SshParameters parms = new SshParameters();
                    parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                    sftp.Connect(SftpServerDirectory, 22, parms);
                    sftp.Login(sftpUser, sftpPassword);
                    sftp.PutFiles(fileDirectory + @"\*", LocalDirectory, SftpBatchTransferOptions.Default, SftpActionOnExistingFiles.OverwriteAll);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
        }

        public void RenameRemoteFile(string oldName, string newName)
        {
            using (sftp = new Sftp())
            {
                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                sftp.Connect(SftpServerDirectory, 22, parms);
                sftp.Login(sftpUser, sftpPassword);

                try
                {
                    sftp.Rename(oldName, newName);
                }
                catch (Exception ex)
                {
                    //Some logging here...
                }
            }
        }

        public List<string> GetDirectoryListing(string append = "")
        {
            List<string> files = new List<string>();

            using (sftp = new Sftp())
            {
                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                sftp.Connect(SftpServerDirectory, 22, parms);
                sftp.Login(sftpUser, sftpPassword);

                try
                {
                    files = sftp.GetNameList(LocalDirectory + "/" + append).ToList();
                }
                catch (Exception ex)
                {
                    //Some logging here...
                }

                return files;
            }
        }

        public long GetFileSize(string fileName)
        {
            long fileSize = 0;

            using (sftp = new Sftp())
            {
                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                sftp.Connect(SftpServerDirectory, 22, parms);
                sftp.Login(sftpUser, sftpPassword);

                try
                {
                    fileSize = sftp.GetFileLength(LocalDirectory + "/" + fileName);
                }
                catch (Exception ex)
                {
                    //Some logging here...
                }

                return fileSize;
            }
        }

        public void DeleteFile(string fileDirectory)
        {
            using (sftp = new Sftp())
            {
                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.RSA;

                sftp.Connect(SftpServerDirectory, 22, parms);
                sftp.Login(sftpUser, sftpPassword);

                try
                {
                    sftp.DeleteFile(fileDirectory);
                }
                catch (Exception ex)
                {
                    //Some logging here...
                }
            }
 
        }

        #endregion
    }
}
