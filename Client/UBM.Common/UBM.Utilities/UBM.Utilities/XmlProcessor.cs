﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;


namespace UBM.Utilities
{
    /// <summary>
    /// The XmlProcessor class provides some basic functionality for Xml files
    /// </summary>
    public class XmlProcessor
    {
        private readonly string xmlDocumentPath;
        private readonly string xmlSchemaPath;
        private XmlDocument xmlDocument;
        private bool documentLoaded;

        /// <summary>
        /// Creates new instance of XmlProcessor. Schema path is optional
        /// </summary>
        /// <param name="xmlDocumentPath"></param>
        /// <param name="xmlSchemaPath"></param>
        public XmlProcessor(string xmlDocumentPath, string xmlSchemaPath = "")
        {
            if (!Path.IsPathRooted(xmlDocumentPath))
                this.xmlDocumentPath = AppDomain.CurrentDomain.BaseDirectory + "\\";
            if (!Path.IsPathRooted(xmlSchemaPath))
                this.xmlSchemaPath = AppDomain.CurrentDomain.BaseDirectory + "\\";

            this.xmlDocumentPath += xmlDocumentPath;
            if (xmlSchemaPath != null) this.xmlSchemaPath += xmlSchemaPath;
        }

        /// <summary>
        /// Creates an XmlDocument object and loads the file passed in the constructor. Throws an exception on error.
        /// </summary>
        public void Load()
        {
            xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlDocumentPath);
            documentLoaded = true;
        }

        /// <summary>
        /// Validates the Xml file against the passed Schema. Throws an exception on failure. Document does not need
        /// to be loaded for this to work.
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            bool validated = true;

            XmlReaderSettings settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};

            XmlReader validator = null;

            XmlSchemaSet schemas = new XmlSchemaSet();
            settings.Schemas = schemas;

            try
            {
                schemas.Add(null, xmlSchemaPath);
                validator = XmlReader.Create(xmlDocumentPath, settings);
                while (validator.Read())
                {
                }
            }
            catch (Exception)
            {
                validated = false;
            }
            finally
            {
                if (validator != null) validator.Close();
            }

            return validated;

        }


        public int GetNodeCount(string nodeName)
        {
            return xmlDocument.GetElementsByTagName(nodeName).Count;
        }

        public int GetChildNodeCount(string nodeName, int depth)
        {
            XmlNodeList nodelist = xmlDocument.GetElementsByTagName(nodeName);

            XmlNode parentNode = nodelist.Item(depth);
            if (parentNode != null)
            {
                //Point to Child node.
                if (parentNode.HasChildNodes)
                {
                    return parentNode.ChildNodes.Count;
                }
            }

            return 0;
        }

        public XmlNodeList GetChildNodes(string nodeName, int depth)
        {
            XmlNodeList nodelist = xmlDocument.GetElementsByTagName(nodeName);

            XmlNode parentNode = nodelist.Item(depth);
            if (parentNode != null)
            {
                //Point to Child node.
                if (parentNode.HasChildNodes)
                {
                    return parentNode.ChildNodes;
                }
            }

            return null;
        }

        //public string GetChildNodeValueFromList(XmlNodeList list string nodeName, int depth)
        //{
        //    XmlNodeList nodelist = xmlDocument.GetElementsByTagName(nodeName);

        //    XmlNode parentNode = nodelist.Item(depth);
        //    if (parentNode != null)
        //    {
        //        //Point to Child node.
        //        if (parentNode.HasChildNodes)
        //        {
        //            return parentNode.ChildNodes;
        //        }
        //    }

        //    return null;
        //}

        public string GetValueAtNodeFromList(XmlNodeList nodeList, string nodeName, int depth)
        {
            try
            {
                //Find the node by Tagname which should be exactle same as in our XML Configuration file.

                //XmlNodeList list = nodeList.GetElementsByTagName(nodeName);

                XmlNode parentNode = null;

                foreach (XmlNode node in nodeList)
                {
                    if (node.Name == nodeName)
                    {
                        parentNode = node;
                        break;
                    }
                }

                //Point to parent node from the Node List(In our example Configuration is the Parent Node.
                //parentNode = list.Item(depth);
                if (parentNode != null)
                {
                    //Point to Child node.
                    if (parentNode.HasChildNodes)
                    {
                        XmlNode childNode = parentNode.ChildNodes[0];
                        //Read the value from the child node.
                        return childNode.InnerText;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch (Exception)
            {
                return "";
            }
            return "";

        }

        /// <summary>
        /// Returns the text at a given node name. The depth defines the index of the named node.
        /// Returns "" if not valid.
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        public string GetValueAtNode(string nodeName, int depth)
        {
            string nodeValue = "";

            if (documentLoaded)
            {
                try
                {
                    //Find the node by Tagname which should be exactle same as in our XML Configuration file.
                    XmlNodeList nodelist = xmlDocument.GetElementsByTagName(nodeName);

                    //Point to parent node from the Node List(In our example Configuration is the Parent Node.
                    XmlNode parentNode = nodelist.Item(depth);
                    if (parentNode != null)
                    {
                        //Point to Child node.
                        if (parentNode.HasChildNodes)
                        {
                            XmlNode childNode = parentNode.ChildNodes[0];
                            //Read the value from the child node.
                            nodeValue = childNode.InnerText;
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                catch (Exception)
                {
                    return "";
                }
            }
            return nodeValue;
        }

        /// <summary>
        /// Returns true if the Xml document loaded successfully
        /// </summary>
        /// <returns></returns>
        public bool IsDocumentLoaded()
        {
            return documentLoaded;
        }
    }
}