﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Threading;

namespace UBM.Utilities
{
    public static class ThreadHelper
    {
        public delegate void AlertDelegate();

        public static Thread GetWorkerThread(Action action)
        {
            ThreadStart start = delegate()
            {
                DispatcherOperation operation = Dispatcher.CurrentDispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    action);

                CheckStatus(operation, null);
            };

            return new Thread(start);
        }

        public static Thread GetWorkerThread(Action action, AlertDelegate alert)
        {
            ThreadStart start = delegate()
            {
                DispatcherOperation operation = Dispatcher.CurrentDispatcher.BeginInvoke(
                    DispatcherPriority.Normal,
                    action);

                CheckStatus(operation, alert);
            };

            return new Thread(start);
        }

        private static void CheckStatus(DispatcherOperation operation, AlertDelegate alert)
        {
            DispatcherOperationStatus status = operation.Status;
            while (status != DispatcherOperationStatus.Completed)
            {
                status = operation.Wait(TimeSpan.FromMilliseconds(1000));
                if (status == DispatcherOperationStatus.Aborted)
                {
                    if (alert != null) alert();
                }
            }
        }
    }
}
