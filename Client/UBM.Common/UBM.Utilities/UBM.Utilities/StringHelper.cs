﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using System.Collections;

namespace UBM.Utilities
{
    public static class StringHelper
    {
        public static string Tidy(string untidy)
        {
            if (string.IsNullOrEmpty(untidy))
            {
                return "";
            }
            else
            {
                return untidy.Trim();
            }
        }

        public static string Truncate(string toTruncate, int length)
        {
            if (string.IsNullOrEmpty(toTruncate))
            {
                return "";
            }
            else if (toTruncate.Length <= length)
            {
                return toTruncate;
            }
            else
            {
                return toTruncate.Substring(0, length) + "... ";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueToSplit"></param>
        /// <param name="delimeter"></param>
        /// <returns></returns>
        public static IEnumerable<string> SplitValues(string valueToSplit, char delimeter)
        {
            // Splits the string up by its delimeter value that is passed to this method.
            string[] tempItems = valueToSplit.Split(delimeter);

            // Trims space of each string.
            for (int i = 0; i < tempItems.Length; i++)
            {
                tempItems[i] = tempItems[i].Trim();
            }

            return tempItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueToRemove"></param>
        /// <returns></returns>
        public static IEnumerable<string> RemoveDuplicates(IEnumerable<string> valueToRemove)
        {
            ArrayList items = new ArrayList();

            // Removes any duplicated strings.
            foreach (string temp in valueToRemove)
            {
                if (!items.Contains(temp))
                {
                    items.Add(temp);
                }
            }

            return items.OfType<string>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valuesToConcatonate"></param>
        /// <param name="includeSpace"></param>
        /// <returns></returns>
        public static string ConcatenateString(IEnumerable<string> valuesToConcatonate, bool includeSpace)
        {
            StringBuilder reformatedItems = new StringBuilder();

            // Creates a new formated string to pass back. 
            foreach (string item in valuesToConcatonate)
            {
                reformatedItems.Append(item);
                reformatedItems.Append(",");

                if (includeSpace)
                {
                    reformatedItems.Append(" ");
                }
            }

            if (reformatedItems.Length > 0)
            {
                if (includeSpace)
                {
                    // Removes the last ", " off of the string.
                    reformatedItems.Remove(reformatedItems.Length - 2, 2);
                }
                else
                {
                    // Removes the last "," off of the string. 
                    reformatedItems.Remove(reformatedItems.Length - 1, 1);
                }
            }


            return reformatedItems.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueIn"></param>
        /// <param name="length"></param>
        /// <param name="hasEllipsis"></param>
        /// <returns></returns>
        public static string TrimStringToSetLength(string valueIn, int length, bool hasEllipsis)
        {
            string valueOut;

            if (valueIn.Length > length)
            {
                // Gets the start of the value passed to this method.
                if (hasEllipsis)
                    valueOut = valueIn.Substring(0, length - 3) + "...";
                else
                    valueOut = valueIn.Substring(0, length);

                return valueOut;
            }

            return valueIn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueIn"></param>
        /// <param name="valuesToCheck"></param>
        /// <returns></returns>
        public static bool CheckValueIsUnique(string valueIn, IEnumerable<string> valuesToCheck)
        {
            bool unique = true;

            foreach (var value in valuesToCheck)
            {
                if (value == valueIn)
                    unique = false;
            }

            return unique;

        }
     }
}
