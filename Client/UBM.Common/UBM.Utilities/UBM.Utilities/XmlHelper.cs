﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace UBM.Utilities
{
    public static class XmlHelper
    {
        public static T DeserializeXml<T>(string xmlPath)
        {
            using (StreamReader stream = new StreamReader(xmlPath))
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(T), "");
                    var deserializedStream = (T)serializer.Deserialize(stream);
                    return deserializedStream;
                }
                catch (Exception ex)
                {
                    return default(T);
                }
            }
        }
    }
}
