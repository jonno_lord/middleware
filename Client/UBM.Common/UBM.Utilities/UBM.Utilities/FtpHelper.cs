﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;

namespace UBM.Utilities
{
    public class FtpHelper
    {
        #region Fields

        public string LocalDirectory { get; set; }
        public string FtpServerDirectory { get; set; }

        private string ftpUser = @"";
        private string ftpPassword = @"";

        private FtpWebResponse response;
        private FtpWebRequest request;
        private Stream responseStream;

        private const int BUFFER_LENGTH = 2048;
        private byte[] buffer = new byte[BUFFER_LENGTH];

        private bool connected;


        #endregion

        #region Private Methods

        private void Connect(string append = "")
        {
            request = WebRequest.Create(new Uri(FtpServerDirectory + append)) as FtpWebRequest;

            if (request != null)
            {
                request.Credentials = new NetworkCredential(ftpUser, ftpPassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                //WebProxy proxy = new WebProxy { Credentials = request.Credentials };
                //request.Proxy = proxy;

                connected = true;
            }
        }

        private void Disconnect()
        {
            if (connected)
            {
                responseStream.Close();
                response.Close(); //Closes the connection to the server
            }
        }

        #endregion 

        #region Constructor

        public FtpHelper(string server, string user, string password, string local)
        {
            ftpUser = user;
            ftpPassword = password;

            FtpServerDirectory = server;
            LocalDirectory = local;
        }

        #endregion

        #region Public Methods 

        public List<string> GetDirectoryListing(string suffix = "")
        {
            Connect();

            request.Method = WebRequestMethods.Ftp.ListDirectory;
            response = request.GetResponse() as FtpWebResponse;

            responseStream = response.GetResponseStream();

            List<string> files = new List<string>();
            StreamReader reader = new StreamReader(responseStream); while (!reader.EndOfStream)
            {
                string filename = reader.ReadLine();
                if (!string.IsNullOrEmpty(filename) && filename.EndsWith(suffix))
                {
                    // get only the filename
                    if(filename.Contains("/"))
                    {
                        filename = filename.Substring(filename.LastIndexOf("/") + 1);
                    }

                    files.Add(filename);
                }
                    
            }
            reader.Close();

            Disconnect();

            return files;
        }

        public void DownloadFile(string fileName)
        {
            string filePath = LocalDirectory + fileName;

            //if (!File.Exists(filePath))
            //{
                Connect();

                request.Method = WebRequestMethods.Ftp.DownloadFile;
                response = request.GetResponse() as FtpWebResponse;

                responseStream = response.GetResponseStream();

                int bytesRead = responseStream.Read(buffer, 0, BUFFER_LENGTH);
                responseStream.Write(buffer, 0, 0);

                FileStream writeStream = new FileStream(filePath, FileMode.Create);

                while (bytesRead > 0)
                {
                    writeStream.Write(buffer, 0, bytesRead);
                    bytesRead = responseStream.Read(buffer, 0, BUFFER_LENGTH);
                }
                writeStream.Close();


                Disconnect();
            //}
        }

        public void UploadFile(string fileName)
        {
            string filePath = LocalDirectory + "\\" + fileName;

            if (File.Exists(filePath))
            {
                Connect(fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;

                response = request.GetResponse() as FtpWebResponse;

                FileStream fileStream = new FileStream(filePath, FileMode.Open);

                responseStream = request.GetRequestStream();

                // Read from the file stream 2kb at a time
                int contentLen = fileStream.Read(buffer, 0, BUFFER_LENGTH);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload
                    // Stream
                    responseStream.Write(buffer, 0, contentLen);
                    contentLen = fileStream.Read(buffer, 0, BUFFER_LENGTH);
                }

                // Close the file stream and the Request Stream
                fileStream.Close();

            }

            Disconnect();
        }

        public void DeleteFile()
        {
            Connect();

            request.Method = WebRequestMethods.Ftp.DeleteFile;
            response = (FtpWebResponse)request.GetResponse();
            responseStream = response.GetResponseStream();

            Disconnect();
        }

        public void RenameRemoteFile(string oldName, string newName)
        {
            Connect();

            request.Method = WebRequestMethods.Ftp.Rename;
            request.RenameTo = newName;
            response = request.GetResponse() as FtpWebResponse;
            responseStream = response.GetResponseStream();

            Disconnect();
        }

        public long GetFileSize(string fileName)
        {
            Connect();

            request.Method = WebRequestMethods.Ftp.GetFileSize;
            response = (FtpWebResponse)request.GetResponse();
            long size = response.ContentLength;

            Disconnect();

            return size;
        }

        #endregion
    }
}
