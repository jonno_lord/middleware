using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using UBM.Utilities.Mocking.Interfaces;

namespace UBM.Utilities.Mocking
{
    /// <summary>
    /// A linq to sql wrapper class for the Datacontext object. This is the real implementation of IDataContextWrapper
    /// that works directly with a database 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataContextWrapper<T> : IDataContextWrapper where T : DataContext, new()
    {
        private readonly T db;
        private bool _disposed;

        public DataContextWrapper()
        {
            var t = typeof(T);
            db = (T) Activator.CreateInstance(t);
        }

        public DataContextWrapper(string connectionString)
        {
            var t = typeof(T);
            db = (T)Activator.CreateInstance(t, connectionString);
        }

        #region IDataContextWrapper Members

        /// <summary>
        /// Tables this instance.
        /// </summary>
        /// <typeparam name="TableName"></typeparam>
        /// <returns></returns>
        public List<TableName> Table<TableName>() where TableName : class
        {
            var table = (Table<TableName>)db.GetTable(typeof(TableName));

            return table.ToList();
        }

        public void DeleteAllOnSubmit<Entity>(IEnumerable<Entity> entities) where Entity : class
        {
            db.GetTable(typeof(Entity)).DeleteAllOnSubmit(entities);
        }

        public void DeleteOnSubmit<Entity>(Entity entity) where Entity : class
        {
            db.GetTable(typeof(Entity)).DeleteOnSubmit(entity);
        }

        public void InsertAllOnSubmit<Entity>(IEnumerable<Entity> entities) where Entity : class
        {
            db.GetTable(typeof(Entity)).InsertAllOnSubmit(entities);
        }

        public void InsertOnSubmit<Entity>(Entity entity) where Entity : class
        {
            db.GetTable(typeof(Entity)).InsertOnSubmit(entity);
        }

        public void SubmitChanges()
        {
            db.SubmitChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                db.Dispose();
            }

            _disposed = true;
        }


     
    }
}