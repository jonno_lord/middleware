using System;
using System.Collections.Generic;

namespace UBM.Utilities.Mocking.Interfaces
{
    public interface IDataContextWrapper : IDisposable
    {
        List<T> Table<T>() where T : class;
        void DeleteAllOnSubmit<T>(IEnumerable<T> entities) where T : class;
        void DeleteOnSubmit<T>(T entity) where T : class;
        void InsertAllOnSubmit<T>(IEnumerable<T> entities) where T : class;
        void InsertOnSubmit<T>(T entity) where T : class;
        void SubmitChanges();
    }
}