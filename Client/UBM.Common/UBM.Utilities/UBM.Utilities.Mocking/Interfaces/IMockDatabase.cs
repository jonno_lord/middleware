using System;
using System.Collections;
using System.Collections.Generic;

namespace UBM.Utilities.Mocking.Interfaces
{
    public interface IMockDatabase
    {
        Dictionary<Type, IList> Tables { get; set; }
    }
}