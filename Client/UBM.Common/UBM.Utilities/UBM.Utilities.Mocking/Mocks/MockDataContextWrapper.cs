using System;
using System.Collections.Generic;
using System.Reflection;
using UBM.Utilities.Mocking.Interfaces;

namespace UBM.Utilities.Mocking.Mocks
{
    /// <summary>
    /// A linq to sql wrapper class. This is a mock implementation of IDataContextWrapper
    /// that works directly with an in memory version of a database 
    /// </summary>
    public class MockDataContextWrapper : IDataContextWrapper
    {
        private readonly MockDatabase _mockDatabase;

        public MockDataContextWrapper(MockDatabase database)
        {
            _mockDatabase = database;
        }

        #region IDataContextWrapper Members

        public List<T> Table<T>() where T : class
        {
            return (List<T>)_mockDatabase.Tables[typeof(T)];
        }

        public void DeleteAllOnSubmit<T>(IEnumerable<T> entities) where T : class
        {
            foreach (var entity in entities)
            {
                Table<T>().Remove(entity);
            }
        }

        public void DeleteOnSubmit<T>(T entity) where T : class
        {
            Table<T>().Remove(entity);
        }

        public void InsertAllOnSubmit<T>(IEnumerable<T> entities) where T : class
        {

            foreach (var entity in entities)
            {
                try
                {
                    Type type = entity.GetType();
                    PropertyInfo prop = type.GetProperty("id");
                    prop.SetValue(entity, Table<T>().Count + 1, null);
                }
                catch
                {
                }

                Table<T>().Add(entity);
            }
        }

        public void InsertOnSubmit<T>(T entity) where T : class
        {
            try
            {
                Type type = entity.GetType();
                PropertyInfo prop = type.GetProperty("id");
                prop.SetValue(entity, Table<T>().Count + 1, null);
            }
            catch
            {
            }

            Table<T>().Add(entity);
        }

        public void SubmitChanges()
        {
        }

        public void Dispose()
        {
        }

        #endregion


      
    }
}