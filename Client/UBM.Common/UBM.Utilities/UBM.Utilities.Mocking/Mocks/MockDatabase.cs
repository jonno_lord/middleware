﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UBM.Utilities.Mocking.Mocks
{
    /// <summary>
    /// Abstract Template class that represents our in memory database. We can create different implementations of this class that contain different
    /// tables and data.
    /// </summary>
    public abstract class MockDatabase
    {
        protected MockDatabase()
        {
            InitializeDataBase();
        }
        
        public Dictionary<Type, IList> Tables { get; set; }

        private void InitializeDataBase()
        {
            Tables = new Dictionary<Type, IList>();
            CreateTables();
            PopulateTables();
        }

        protected abstract void CreateTables();
        protected abstract void PopulateTables();

        protected void AddTable<T>()
        {
            var table = new List<T>();
            Tables.Add(typeof (T), table);
        }

        protected List<T> GetTable<T>()
        {
            return (List<T>) Tables[typeof (T)];
        }
    }
}