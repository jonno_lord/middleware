﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UBM.Utilities.Testing
{
    public static class TestHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="duplicates"></param>
        /// <returns></returns>
        public static bool CheckForDuplicates(IEnumerable<string> duplicates)
        {
            bool hasDuplicates = false;
            string temp;
            //Checks to see if there are more than one string the same.
            foreach (string dupe in duplicates)
            {
                int numOfTimes = 0;

                foreach (string t1 in duplicates)
                {
                    temp = t1;

                    // If they are the same it increments the integer by one. 
                    if (dupe == temp)
                    {
                        numOfTimes++;
                    }
                }

                // If the number of times is greater than 1 then the userbase item
                // is in there more than once so it sets the duplicates bool to true.
                if (numOfTimes > 1)
                {
                    hasDuplicates = true;
                    break;
                }
            }

            return hasDuplicates;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalValue"></param>
        /// <param name="valuesToCompare"></param>
        /// <returns></returns>
        public static bool CheckValuesAreSame(string originalValue, IEnumerable<string> valuesToCompare)
        {
            foreach (string s in valuesToCompare)
            {
                // Checks the values if they're not the same then returns false.)
                if (originalValue != s.Trim())
                    return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalValue"></param>
        /// <param name="valuesToCompare"></param>
        /// <returns></returns>
        public static bool CheckValuesAreSame(int originalValue, IEnumerable<int> valuesToCompare)
        {
            foreach (int i in valuesToCompare)
            {
                // Checks the values if they're not the same then returns false.
                if (originalValue != i)
                    return false;
            }

            return true;
        }
    }
}
