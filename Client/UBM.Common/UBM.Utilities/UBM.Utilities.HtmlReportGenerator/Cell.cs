﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public class Cell : StyledElement
    {
        public string Content { get; set; }


        public override string Compile()
        {
            return Tags.CreateTagLine(Tags.TABLE_CELL, Content, Style);
        }
    }
}
