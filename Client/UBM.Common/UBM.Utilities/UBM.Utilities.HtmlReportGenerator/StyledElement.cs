﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public abstract class StyledElement
    {
        public string Style { get; set; }

        public abstract string Compile();
    }
}
