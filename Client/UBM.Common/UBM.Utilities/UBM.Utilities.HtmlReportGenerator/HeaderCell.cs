﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public class HeaderCell : StyledElement
    {
        public string Content { get; set; }

        public override string Compile()
        {
            return Tags.CreateTagLine(Tags.TABLE_HEADER_CELL, Content, Style);
        }
    }
}
