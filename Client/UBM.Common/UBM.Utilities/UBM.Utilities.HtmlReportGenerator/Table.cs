﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public class Table : StyledElement
    {
        private TableHeaderRow HeaderRow { get; set; }
        private List<TableRow> Rows { get; set; }


        private readonly TableStyle tableStyle;

        public Table(TableStyle style)
        {
            tableStyle = style;
            Style = tableStyle.Table;
            Rows = new List<TableRow>();
            HeaderRow = new TableHeaderRow();
        }

        public Table(TableStyleName styleName) : this(PresetStyles.TableStyleDictionary[styleName])
        {            
        }

        public Table AddHeader(List<string> content)
        {
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.Style = tableStyle.Row;

            foreach (string s in content)
            {
                HeaderCell c = new HeaderCell();
                c.Content = s;
                c.Style = tableStyle.Header;
                headerRow.Cells.Add(c);
            }
            HeaderRow = headerRow;

            return this;
        }

        public Table AddRow(List<string> content)
        {
            TableRow tableRow = new TableRow();
            tableRow.Style = tableStyle.Row;
            foreach (string s in content)
            {
                Cell c = new Cell();
                c.Content = s;
                c.Style = tableStyle.Cell;

                tableRow.Cells.Add(c);
            }
            Rows.Add(tableRow);

            return this;
        }

        public override string Compile()
        {
            string rows = "";
            
            Rows.ForEach(c=> rows += c.Compile());

            return Tags.CreateTagOpen(Tags.TABLE, Style)
                    + HeaderRow.Compile()
                    + rows
                    + Tags.CreateTagClose(Tags.TABLE);

        }
    }
}
