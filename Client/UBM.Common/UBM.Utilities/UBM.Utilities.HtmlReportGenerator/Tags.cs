﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    internal class Tags
    {
        public const string HEADER1 = "h1";
        public const string HEADER2 = "h2";
        public const string PARAGRAPH = "p";
        public const string TABLE = "table";
        public const string TABLE_CELL = "td";
        public const string TABLE_ROW = "tr";
        public const string TABLE_HEADER_CELL = "th";
        public const string HORIZONTAL_RULE = "hr";



        public static string CreateTagLine(string tag, string content, string textStyle)
        {
            return "<" + tag + " style=\"" + textStyle + "\">" + content + "</" + tag + ">";
        }

        public static string CreateTagOpen(string tag, string textStyle)
        {
            return "<" + tag + " style=\"" + textStyle + "\">";
        }

        public static string CreateTagClose(string tag)
        {
            return "</" + tag + ">";
        }

        public static string CreateTagSelfClosing(string tag)
        {
            return "<" + tag + "/>";
        }
    }
}
