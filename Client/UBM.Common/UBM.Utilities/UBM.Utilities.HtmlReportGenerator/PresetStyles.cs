﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public enum TextStyleName
    {
        Normal,
        Caution,
        Failure
    }

    public enum TableStyleName
    {
        MiddlewareCustomerRejection,
        MiddlewarePublicationInvoicingReport
    }

    internal static class PresetStyles
    {
        public static Dictionary<TextStyleName, string> StyleDictionary = new Dictionary<TextStyleName, string>
                                                                      {
                                                                          {TextStyleName.Normal, "font-family: Calibri; color: #5880C5;"},
                                                                          {TextStyleName.Caution, "font-family: Calibri; color: #FFA500;"},
                                                                          {TextStyleName.Failure, "font-family: Calibri; color: #ff0000;"}
                                                                      };

        public static Dictionary<TableStyleName, TableStyle> TableStyleDictionary = new Dictionary<TableStyleName, TableStyle>
                                                                                        {
                                                                                            {TableStyleName.MiddlewareCustomerRejection, new TableStyle
                                                                                                {
                                                                                                    Table = "width: 500px; font-size: 12pt;",                                                                                                    
                                                                                                    Cell = "width:50%; font-family: Calibri; color: #5880C5;",
                                                                                                    Row = "",
                                                                                                    Header = ""
                                                                                                }
                                                                                            },
                                                                                            {TableStyleName.MiddlewarePublicationInvoicingReport, new TableStyle
                                                                                                {
                                                                                                    Table = "font-size: 12pt; border:1px solid #000; border-spacing:5px; width:500px",                                                                                                    
                                                                                                    Cell = "width:*; font-family: Calibri; color: #5880C5;",
                                                                                                    Header = "font-family: Calibri; color: #5880C5; text-align: left;",
                                                                                                    Row = "",
                                                                                                }
                                                                                            }
                                                                                        };

    }
}
