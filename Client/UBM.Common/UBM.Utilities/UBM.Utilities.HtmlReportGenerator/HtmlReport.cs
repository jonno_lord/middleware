﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public class HtmlReport
    {
        private string report;
        private string textStyle;

        private string CreateTagLine(string tag, string content)
        {
            return Tags.CreateTagLine(tag, content, textStyle);
        }

        public HtmlReport()
        {
            report = "";
            SetTextStyle(TextStyleName.Normal);
        }

        public void SetTextStyle(string style)
        {
            textStyle = style;
        }

        public void SetTextStyle(TextStyleName style)
        {
            textStyle = PresetStyles.StyleDictionary[style];
        }

        public void AddHorizontalRule()
        {
            report += Tags.CreateTagSelfClosing(Tags.HORIZONTAL_RULE);
        }

        public void AddHeader(string headerText)
        {
            report += CreateTagLine(Tags.HEADER2, headerText);
        }

        public void AddHeader(string headerText, TextStyleName style)
        {
            string saveCurrentStyle = textStyle;
            SetTextStyle(style);
            AddHeader(headerText);
            SetTextStyle(saveCurrentStyle);
        }

        public void AddParagraph(string paragraph)
        {
            report += CreateTagLine(Tags.PARAGRAPH, paragraph);
        }

        public void AddParagraph(string paragraph, TextStyleName style)
        {
            string saveCurrentStyle = textStyle;
            SetTextStyle(style);
            AddParagraph(paragraph);
            SetTextStyle(saveCurrentStyle);
        }

        public void AddHtml(string html)
        {
            report += html;
        }

        public void AddTable(Table table)
        {
            report += table.Compile();
        }

        public static Table CreateTable(TableStyleName styleName)
        {
            Table table = new Table(styleName);
            return table;
        }

        public override string ToString()
        {
            return "<html><body>" + report + "</body></html>";
        }
    }
}
