﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public class TableStyle
    {
        public string Table { get; set; }
        public string Row { get; set; }
        public string Header { get; set; }
        public string Cell { get; set; }
    }
}
