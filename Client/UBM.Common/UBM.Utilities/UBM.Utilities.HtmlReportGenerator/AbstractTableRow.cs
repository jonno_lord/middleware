﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.Utilities.HtmlReportGenerator
{
    public class AbstractTableRow<T> : StyledElement where T : StyledElement
    {
        public List<T> Cells { get; set; }

        public AbstractTableRow()
        {
            Cells = new List<T>();
        }

        public override string Compile()
        {
            string cells = "";
            
            Cells.ForEach(c => cells += c.Compile().ToString());

            return Tags.CreateTagOpen(Tags.TABLE_ROW, Style) +
                   cells
                   + Tags.CreateTagClose(Tags.TABLE_ROW);

        }
    }
}
