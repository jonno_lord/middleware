﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Dts.Runtime;

namespace UBM.SSISExecutor
{
    /// <summary>
    /// Handles the events for listening to a packages progess.
    /// This is a direct port of the code from http://www.codeproject.com/KB/database/SampleSSIS.aspx
    /// </summary>
    public class SSISEventListener : Microsoft.SqlServer.Dts.Runtime.DefaultEvents
    {

        private System.Data.DataTable eventLogTable;
        private int eventCount;

        /// <summary>
        /// Constructor creates a data table that can be bound on for UI purposes.
        /// </summary>
        public SSISEventListener()
        {
            eventCount = 0;

            eventLogTable = new System.Data.DataTable();
            eventLogTable.Columns.Add("Status", System.Type.GetType("System.Int16"));
            eventLogTable.Columns.Add("Event Sequence", System.Type.GetType("System.Int16"));
            eventLogTable.Columns.Add("Time", System.Type.GetType("System.DateTime"));
            eventLogTable.Columns.Add("SSIS Execution Status", System.Type.GetType("System.String"));
        }


        /// <summary>
        /// determines the number of events that have been risen from the package (warnings, errors and information)
        /// </summary>
        public int EventCount
        {
            get { return eventCount; }
        }

        /// <summary>
        /// A datatable that can be used to bind to for UI purposes. Contains 4 columns
        /// Status, Event Sequence, Time, SSIS Exection Status
        /// </summary>
        public System.Data.DataTable EventLogTable
        {
            get { return eventLogTable; }
        }

        /// <summary>
        /// Rasied when an error occurs from the DTS package - used from the Base DefaultEvents in
        /// the DTS runtime library
        /// </summary>
        /// <param name="source">Where the error was raised from</param>
        /// <param name="errorCode">The error code</param>
        /// <param name="subComponent">Sub component details</param>
        /// <param name="description">The description of the error</param>
        /// <param name="helpFile">details of the help file that can be used for this fault</param>
        /// <param name="helpContext">The context of the error</param>
        /// <param name="idofInterfaceWithError">The id of the interface</param>
        /// <returns></returns>
        public override bool OnError(DtsObject source, int errorCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            eventCount++;

            string[] LogData = { "2", eventCount.ToString(), System.DateTime.Now.ToLongTimeString(), description };
            eventLogTable.Rows.Add(LogData);

            return base.OnError(source, errorCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError);
        }

        /// <summary>
        /// Raised when an information event occurs in the DTS package
        /// </summary>
        /// <param name="source">the source of the information</param>
        /// <param name="informationCode">information code</param>
        /// <param name="subComponent">the sub component</param>
        /// <param name="description">the description of the information event</param>
        /// <param name="helpFile">the help file associated</param>
        /// <param name="helpContext">help file context</param>
        /// <param name="idofInterfaceWithError">The interface id of the information</param>
        /// <param name="fireAgain">if this task should fire again.</param>
        public override void OnInformation(DtsObject source, int informationCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError, ref bool fireAgain)
        {
            eventCount++;

            string[] LogData = { "0", eventCount.ToString(), System.DateTime.Now.ToLongTimeString(), description };
            eventLogTable.Rows.Add(LogData);

            base.OnInformation(source, informationCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError, ref fireAgain);
        }

        /// <summary>
        /// Raised when a warning occurs in the DTS package
        /// </summary>
        /// <param name="source">the source of the warning</param>
        /// <param name="warningCode">the warning code</param>
        /// <param name="subComponent">the subcomponent this warning realtes to</param>
        /// <param name="description">the description of the warning</param>
        /// <param name="helpFile">help file</param>
        /// <param name="helpContext">help context</param>
        /// <param name="idofInterfaceWithError">id of the warning</param>
        public override void OnWarning(DtsObject source, int warningCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            eventCount++;

            string[] LogData = { "1", eventCount.ToString(), System.DateTime.Now.ToString(), description };
            eventLogTable.Rows.Add(LogData);

            base.OnWarning(source, warningCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError);
        }
    }
}
