﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Dts.Runtime;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareLogging;

namespace UBM.SSISExecutor
{

    /// <summary>
    /// Main class that deals with loading and execute DTSpackages
    /// </summary>
    public class DTSPackage
    {

        /// <summary>
        /// States of packages used when loading
        /// </summary>
        public enum DTSPackageStatus
        {
            Empty, LoadFailed, Loaded
        };

        private string pkgLocation;
        private DTSPackageStatus status;
        public Package pkg;
        private Microsoft.SqlServer.Dts.Runtime.Application app;


        /// <summary>
        /// The package name property
        /// </summary>
        public string PkgLocation
        {
            get { return pkgLocation; }
        }

        /// <summary>
        /// The packages status
        /// </summary>
        public DTSPackageStatus PackageStatus
        {
            get { return status; }
        }


        /// <summary>
        /// Constructor for the package
        /// </summary>
        public DTSPackage()
        {
            pkgLocation = null;
            status = DTSPackageStatus.Empty;
            pkg = null;
            app = new Microsoft.SqlServer.Dts.Runtime.Application();
        }


        /// <summary>
        /// disposing details of the class (package related)
        /// </summary>
        private void DisposePackage()
        {
            if (pkg != null)
            {
                pkg.Dispose();
                pkg = null;
            }
        }

        /// <summary>
        /// resets the packages status, and disposes of it.
        /// </summary>
        public void ClearPackage()
        {
            pkgLocation = null;
            status = DTSPackageStatus.Empty;
            DisposePackage();
        }


        /// <summary>
        /// Attempts to load the package specified. If it cannot load, it sets the package
        /// status to LoadFailed.
        /// </summary>
        /// <param name="PkgLocation"></param>
        public void LoadPackage(string PkgLocation)
        {
            pkgLocation = PkgLocation;

            if (PkgLocation != null)
            {
                DisposePackage();
                try
                {
                    pkg = app.LoadPackage(PkgLocation, null);
                    status = DTSPackageStatus.Loaded;
                }
                catch
                {
                    status = DTSPackageStatus.LoadFailed;
                }
            }
        }


        /// <summary>
        /// Used for instantiating connections for the package.
        /// </summary>
        /// <param name="ConnectionCollection">a collection of ConnectionString used for the package</param>
        public void SetPackageConnections(System.Collections.Hashtable ConnectionCollection, int jobid, int processid, int batchid)
        {
            if (status == DTSPackageStatus.Loaded)
            {
                Microsoft.SqlServer.Dts.Runtime.Connections connections = pkg.Connections;
                for (int Idex = 0; Idex < connections.Count; Idex++)
                {
                    ConnectionManager connection = connections[Idex];
                    string ConName = connection.Name;
                    
                    if (ConnectionCollection.Contains(ConName))
                    {
                        //TODO: Implement this in a Middleware Specific instance - or such likes...
                        string logDetail = "Setting: " + ConName + " to " + ConnectionCollection[ConName].ToString();
                        Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Standard, logDetail);
                        connection.ConnectionString = ConnectionCollection[ConName].ToString();
                    }
                    

                }
            }
        }


        /// <summary>
        /// Return the listing of package connections that are required to be set.
        /// </summary>
        /// <returns></returns>
        public System.Collections.Hashtable GetPackageConnections()
        {
            System.Collections.Hashtable ConnectionCollection = new System.Collections.Hashtable();

            if (status == DTSPackageStatus.Loaded)
            {
                Microsoft.SqlServer.Dts.Runtime.Connections connections = pkg.Connections;
                for (int Idex = 0; Idex < connections.Count; Idex++)
                {
                    ConnectionManager connection = connections[Idex];
                    string ConName = connection.Name;
                    string ConStr = connection.ConnectionString;

                    ConnectionCollection.Add(ConName, ConStr);
                }
            }

            return ConnectionCollection;
        }


        /// <summary>
        /// Executes the package synchronously without a Listener callback
        /// </summary>
        /// <returns></returns>
        public DTSExecResult Execute()
        {
            return pkg.Execute();
        }

        /// <summary>
        /// Execute the package Asynchronously with a listener object that has events raised against it
        /// </summary>
        /// <param name="Listener"></param>
        /// <returns></returns>
        public DTSExecResult Execute(SSISEventListener Listener)
        {
            return pkg.Execute(null, null, Listener, null, null);
        }


        /// <summary>
        /// Destructor release of the package
        /// </summary>
        ~DTSPackage()
        {
            DisposePackage();
        }
    }
}
