﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace UBM.SSISTestbench
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void app_Startup(object sender, StartupEventArgs e)
        {
            Splash SplashWnd = new Splash();
            SplashWnd.Show();

            DispatcherHelper.DoEvents();
            System.Threading.Thread.Sleep(800);

            WinMain MainWnd = new WinMain();
            MainWnd.Show();
            SplashWnd.Close();
            GC.Collect();
        }
    }
}
