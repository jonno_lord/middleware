﻿using System;
using UBM.NotificationClient.Notification;

namespace UBM.NotificationClient
{
    public class NotificationServiceClient
    {




        public static bool Email(string from, string to, string cc, string subject, string body, Priority priority = Priority.Normal, bool isHtml = false)
        {
            NotificationSoapClient client = new NotificationSoapClient("NotificationSoap");

            Message message = new Message
                                  {
                                      From = from,
                                      To = to,
                                      Cc = cc,
                                      Priority = Enum.GetName(typeof (Priority), priority),
                                      IsHtml = isHtml,
                                      Subject = subject,
                                      Body = body
                                  };

            bool sent = client.Email(message);

            client.Close();

            return sent;
        }

        /// <summary>
        /// Returns true if given username is present on ADS
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>True if successful</returns>
        public static bool AuthenticateUser(string userName)
        {
            NotificationSoapClient client = new NotificationSoapClient("NotificationSoap");

            ArrayOfString emails = client.ResolveRecipient(userName);

            client.Close();

            return emails != null && emails.ToArray().Length != 0;
        }

        /// <summary>
        /// Check function. Returns current server time.
        /// </summary>
        /// <returns>True if successful</returns>
        public static bool ServerCheck()
        {
            bool serverOnline;
            NotificationSoapClient client;
            try
            {
                client = new NotificationSoapClient("NotificationSoap");
                client.ServerCheck();
                serverOnline = true;
                client.Close();
            }
            catch
            {
                serverOnline = false;
            }

            return serverOnline;

        }

        /// <summary>
        /// Returns a list of email addresses from a give name or group
        /// </summary>
        /// <param name="name">The name or group</param>
        /// <returns>List of resolved email addresses</returns>
        public static string[] ResolveEmail(string name)
        {
            NotificationSoapClient client = new NotificationSoapClient("NotificationSoap");

            ArrayOfString email = client.ResolveRecipient(name);

            return email.ToArray();
        }

        /// <summary>
        /// Return all the ADS groups a user is a member of
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>List of groups user is a member of</returns>
        public static string[] GetUsersGroups(string userName)
        {
            NotificationSoapClient client = new NotificationSoapClient("NotificationSoap");

            ArrayOfString groups = client.GetUsersGroups(userName);

            return groups.ToArray();
        }

        public static string FireMiddlewareJob(int id)
        {
            NotificationSoapClient client = new NotificationSoapClient("NotificationSoap");

            return client.FireMiddlewareJob(id);
        }
    }

    /// <summary>
    /// Email message priority
    /// </summary>
    public enum Priority
    {
        Normal,
        High
    }

}
