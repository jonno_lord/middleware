﻿namespace UBM.NotificationService
{
    /// <summary>
    /// Message class
    /// </summary>
    public class Message
    {

        public string From { get; set; }

        /// <summary>
        /// Gets or sets the recipient.
        /// </summary>
        /// <value>The recipient.</value>
        public string To { get; set; }

        public string Cc { get; set; }

        /// <summary>
        /// Gets or sets the subject line
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body of the message
        /// </summary>
        /// <value>The message body.</value>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the priority of the message
        /// </summary>
        /// <value>The priority.</value>
        public string Priority { get; set; }

        public bool IsHtml { get; set; }
    }
}