﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using UBM.Logger;
using UBM.Utilities;


namespace UBM.NotificationService
{


    /// <summary>
    /// Class providing main functionality of the Notification Server
    /// <para>
    /// Notes:

    /// </para>
    /// </summary>
    public class NotificationLibrary
    {

        #region Private Fields

        private const string FOOTER = "";//"\r\n\r\n*** This message was sent from the UBMi Notification Server ***\r\n";
        private readonly LdapHelper ldapHelper;

        #endregion

        #region Private Properties

        private int MessageID { get; set; }       

        private string ApplicationDomain { get; set; }
        private string ServerEmail { get; set; }
        private string SmtpHost { get; set; }
        private string SmtpPort { get; set; }

        #endregion

        #region Private Methods


        private bool WriteToDB(Message message, bool sendSuccessful)
        {
            string connectionString = "";
           
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings["NotificationConnectionString"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    // Message
                    SqlCommand command = new SqlCommand("usp_INS_Messages", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@From", message.From);
                    command.Parameters.AddWithValue("@To", message.To);
                    command.Parameters.AddWithValue("@CC", message.Cc);
                    command.Parameters.AddWithValue("@Priority", message.Priority);
                    command.Parameters.AddWithValue("@Html", message.IsHtml);
                    command.Parameters.AddWithValue("@SendSuccessful", sendSuccessful);
                    command.Parameters.AddWithValue("@DateSent", DateTime.Now);
                    command.Parameters.AddWithValue("@Subject", message.Subject);
                    command.Parameters.AddWithValue("@Body", message.Body);
                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();
                    int messageID = (int)command.Parameters["@id"].Value;
                    MessageID = messageID; // set Message ID property for logging purposes
                }
                return true;
            }
            catch (Exception exception)
            {
                Log.Error("Problem writing to database. Connection string: " + connectionString, exception);
                return false;
            }
        }

        private void LoadConfigValues()
        {
            ApplicationDomain = ConfigurationManager.AppSettings["Domain"];
            ServerEmail = ConfigurationManager.AppSettings["ServerEmail"];
            SmtpHost = ConfigurationManager.AppSettings["SMTPHost"];
            SmtpPort = ConfigurationManager.AppSettings["SMTPPort"];
        }

        private string PopulateRecipients(string recipients)
        {
            string resolvedList = "";
                
            string[] splitRecipients = recipients.Split(';');

            foreach (string rec in splitRecipients)
            {
                string[] resolved = ldapHelper.GetEmailAddressFromNameOrUser(rec);

                if (resolved.Count() != 0)
                {
                    foreach (string add in resolved)
                    {
                        resolvedList += add + ",";
                    }
                }
                else
                {
                    if (rec.Contains("@")) resolvedList += rec + ",";
                }
            }

            return resolvedList.Contains(",") ? resolvedList.Substring(0, resolvedList.Count() - 1) : "";
        }

        private MailMessage PrepareEmail(Message message)
        {
            string subject = string.IsNullOrEmpty(message.Subject) ? "Notification Server Message" : message.Subject;

            string sender = PopulateRecipients(message.From);

            string from = string.IsNullOrEmpty(sender) ? ServerEmail : sender;

            MailMessage mailItem = new MailMessage
                                        {
                                            Subject = subject,
                                            Body = message.Body + FOOTER,
                                            From = new MailAddress(from, "UBMi Notification Server"),
                                            IsBodyHtml = message.IsHtml,
                                            Priority = message.Priority == "High" ? MailPriority.High : MailPriority.Normal
                                        };

            string recipients = PopulateRecipients(message.To);
            
            if(string.IsNullOrEmpty(recipients))
            {
                if(string.IsNullOrEmpty(sender))
                    throw new ArgumentException("No valid recipients or sender");
                
                recipients = sender;
                mailItem.Subject = "NOTIFICATION FAILURE: " + mailItem.Subject;

                string ammendedBody = "Unable to resolve at Notification Server: " + message.To
                                      + (mailItem.IsBodyHtml ? "<br>" : "\r\n")
                                      + mailItem.Body;

                mailItem.Body = ammendedBody;

            }
            else
            {
                string cc = PopulateRecipients(message.Cc);

                if (!string.IsNullOrEmpty(cc)) mailItem.CC.Add(cc);   
            }

            mailItem.To.Add(recipients);



            return mailItem;
        }

        private bool SendEmail(MailMessage mail)
        {
            try
            {            
                SmtpClient client = new SmtpClient(SmtpHost, int.Parse(SmtpPort));
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                return true;
            } 
            catch (Exception exception)
            {
                Log.Error("Problem sending email notification", exception);
                return false;
            }

        }

        #endregion

        #region Constructor

        public NotificationLibrary()
        {
            LoadConfigValues();
            ldapHelper = new LdapHelper(ApplicationDomain);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to make a single shot email notification
        /// </summary>
        /// <param name="message">The message.</param>
        public bool Email(Message message)
        {
            bool sent = false;

            try
            {
                MailMessage mail = PrepareEmail(message);
                      
                sent = SendEmail(mail);
              
            }
            catch (Exception exception)
            {
                Log.Error("Notification failure.", exception);
            }
            finally
            {
                bool writtenToDB = WriteToDB(message, sent);

                if (writtenToDB)
                {
                    if (sent)
                    {
                        Log.Information("Email Notification: " + MessageID + " written to DB and sent to recipients");
                    }
                    else
                    {
                        Log.Warning("Email Notification: " + MessageID + " written to DB but NOT sent to any recipients");
                    }
                }
            }

            return sent;
        }

        public string[] GetUsersGroups(string userName)
        {
            return ldapHelper.GetUsersGroups(userName);
        }

        public string[] ResolveRecipient(string name)
        {
            return ldapHelper.GetEmailAddressFromNameOrUser(name);
        }

        /// <summary>
        /// Checks server is alive.
        /// </summary>
        /// <returns>Datetime as a string</returns>
        public string ServerCheck()
        {
            return DateTime.Now.ToString();
        }

        /// <summary>
        /// Fires the middleware job.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns></returns>
        public string FireMiddlewareJob(int jobId)
        {
            MiddlewareJobService.JobServiceSoapClient client = new MiddlewareJobService.JobServiceSoapClient("JobServiceSoap");
            return client.StartJob(jobId);
        }

        #endregion

    }
}
