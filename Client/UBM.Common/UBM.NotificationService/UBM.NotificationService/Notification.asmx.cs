﻿using System.Web.Services;

namespace UBM.NotificationService
{

    /// <summary>
    /// Notification Service. Wrapper for the notification library
    /// </summary>
    [WebService(Namespace = "UBM.Notification")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Notification : WebService
    {

        /// <summary>
        /// Checks the server is alive
        /// </summary>
        /// <returns>
        /// The current date and time in string format
        /// </returns>
        [WebMethod]
        public string ServerCheck()
        {
            NotificationLibrary library = new NotificationLibrary();
            return library.ServerCheck();
        }

        /// <summary>
        /// Emails the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        [WebMethod]
        public bool Email(Message message)
        {
            NotificationLibrary library = new NotificationLibrary();
            return library.Email(message);
        }

        /// <summary>
        /// Resolves email from user name or group
        /// </summary>
        /// <param name="userName">Name of the user or group</param>
        /// <returns>Array of email address</returns>
        [WebMethod]
        public string[] ResolveRecipient(string userName)
        {
            NotificationLibrary library = new NotificationLibrary();
            return library.ResolveRecipient(userName);
        }

        /// <summary>
        /// Gets the users groups.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>List of groups user is a member of</returns>
        [WebMethod]
        public string[] GetUsersGroups(string userName)
        {
            NotificationLibrary library = new NotificationLibrary();
            return library.GetUsersGroups(userName);
        }

        /// <summary>
        /// Fires the middleware job.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns></returns>
        [WebMethod]
        public string FireMiddlewareJob(int jobId)
        {
            NotificationLibrary library = new NotificationLibrary();
            return library.FireMiddlewareJob(jobId);
        }

    }
}
