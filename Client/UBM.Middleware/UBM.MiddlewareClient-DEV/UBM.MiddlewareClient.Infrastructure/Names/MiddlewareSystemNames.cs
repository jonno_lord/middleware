﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Infrastructure.Names
{
    public class MiddlewareSystemNames
    {
        public const string ESOP = "ESOP";
        public const string JDE = "JDE";
        public const string UBMA = "UBMA";
        public const string IPSOP = "IPSOP";
        public const string ASOP_UK = "ASOPUK";
        public const string ASOP_HOLLAND = "ASOPHolland";
        public const string ASOP_DALTONS = "ASOPDaltons";
        public const string EVENTS_FORCE = "EventsForce";
        public const string CARS = "CARS";
    }
}
