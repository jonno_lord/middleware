﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    public enum MiddlewareJobType
    {
        InvoicesImmediate,
        InvoicesPrepaid,
        InvoicesPublication,
        ReceiptsCreditCard,
        ReceiptsRevenue,
        MsiCreateForm,
        MsiEventsForce
    }

}
