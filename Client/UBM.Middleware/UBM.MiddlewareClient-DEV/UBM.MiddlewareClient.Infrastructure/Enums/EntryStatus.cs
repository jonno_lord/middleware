﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{

    /// <summary>
    /// Customers status in the process of going through Middleware
    /// </summary>
    public enum EntryStatus
    {
        RejectedFromJDE,
        RejectedInScrutiny,
        SourceToMiddleware,
        AcceptedInScrutiny,
        MiddlewareToMiddlewareIn,
        MiddlewareToJDE,
        JDEToMiddleware,
        MiddlewareToMiddlewareOut,
        MiddlewareToSource,
        Unknown
    }
}