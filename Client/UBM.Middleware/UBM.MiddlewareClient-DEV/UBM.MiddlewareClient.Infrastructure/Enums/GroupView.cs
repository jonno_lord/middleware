﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    /// <summary>
    /// Subsets of customers for Dedupe views
    /// </summary>
    public enum GroupView
    {
        All,
        Scrutiny,
        PostScrutiny,
        Waiting,
        LegalHold,
        PreviouslyRejected,
        Accepted,
        Rejected,
        Search
    }
}