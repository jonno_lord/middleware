﻿
namespace UBM.MiddlewareClient.Infrastructure.Enums
{

    /// <summary>
    /// Customer state in Dedupe
    /// </summary>
    public enum QueueType 
    {
        Unassigned,
        Waiting,
        OnHold,
        PreviouslyRejected,
        Accepted,
        Rejected

    }

}
