﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Infrastructure
{
    public class ConfigurationHelper
    {
        public static string GetConnectionStringFromConfigFile(string connectionStringName)
        {
            return ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }
    }
}
