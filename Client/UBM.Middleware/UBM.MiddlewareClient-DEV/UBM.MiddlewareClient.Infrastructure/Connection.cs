﻿using System;

namespace UBM.MiddlewareClient.Infrastructure
{
    public class Connection
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }

        public Connection(string name, string connectionString)
        {
            Name = name;
            ConnectionString = connectionString;
        }

        public Connection(string name)
        {
            Name = name;
            ConnectionString = ConfigurationHelper.GetConnectionStringFromConfigFile(name);
        }
    }
}