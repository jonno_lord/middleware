﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace UBM.MiddlewareClient.Tracer
{
    public partial class TraceWindow : Form
    {
        public TraceWindow()
        {
            InitializeComponent();

        }

        private void AppendWindow(string text, Color color)
        {



            if(rtbWindow.InvokeRequired)
            {
                rtbWindow.Invoke(new ThreadStart(delegate
                                                     {
                                                         int length = rtbWindow.TextLength;  // at end of text
                                                         rtbWindow.AppendText(text + "\n");
                                                         rtbWindow.SelectionStart = length;
                                                         rtbWindow.SelectionLength = text.Length;
                                                         rtbWindow.SelectionColor = color;

                                                         rtbWindow.ScrollToCaret();
                                                     }));
            }
            else
            {
                int length = rtbWindow.TextLength;  // at end of text
                rtbWindow.AppendText(text + "\n");
                rtbWindow.SelectionStart = length;
                rtbWindow.SelectionLength = text.Length;
                rtbWindow.SelectionColor = color;

                rtbWindow.ScrollToCaret();
            }

        }


        public void ControllerTrace(string text)
        {
            if (chkControllers.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Purple);
        }

        public void InfoTrace(string text)
        {
            if (chkInfo.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Black);
        }

        public void ViewModelTrace(string text)
        {
            if (chkViewModel.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Blue);
        }

        public void ServiceTrace(string text)
        {
            if (chkService.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Firebrick);
        }

        public void DatabaseTrace(string text)
        {
            if (chkDatabase.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Green);
        }
        
        public void Error(string text)
        {
            if (chkErrors.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Red);
        }

        public void Warning(string text)
        {
            if (chkWarnings.Checked && !chkFreeze.Checked) AppendWindow(text, Color.Orange);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            rtbWindow.Clear();
        }

        
    }
}
