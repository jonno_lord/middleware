﻿using System;
using System.ComponentModel;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    /// <summary>
    ///   All View Models inherit this class. INotifyPropertyChanged interface is implemented to update WPF bindings
    /// 
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
    
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                PropertyChanged(this, e);
            }
        }

        public void Dispose()
        {
            this.OnDispose();
        }

        protected virtual void OnDispose() {}
        
    }
}