﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Input;
using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain;
using UBM.NotificationClient;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    public class InfoBarViewModel : ViewModelBase
    {

        private const int ACTION_MESSAGE_HISTORY_SIZE = 5;
        private static readonly object Locker = new object();

        private DateTime actionTime;
        private List<PageViewModel> pageViewModels;

        #region Visibility Properties

        private LightColour notificationState;
        public LightColour NotificationState
        {
            get { return notificationState; }
            set
            {
                if (value == notificationState) return;
                notificationState = value;
                OnPropertyChanged("NotificationState");
            }
        }

        private LightColour syncState;
        public LightColour SyncState
        {
            get { return syncState; }
            set
            {
                if (value == syncState) return;
                syncState = value;
                OnPropertyChanged("SyncState");
            }
        }

        private string actionMessage;
        public string ActionMessage
        {
            get { return actionMessage; }
            set
            {
                if (value == actionMessage) return;
                actionMessage = value;
                OnPropertyChanged("ActionMessage");
            }
        }

        private ObservableCollection<ActionHistory> actionList;
        public ObservableCollection<ActionHistory> ActionList
        {
            get { return actionList; }
            set
            {
                if (value == actionList) return;
                actionList = value;
                OnPropertyChanged("ActionList");
            }
        }

        private MessageType actionMessageType;
        public MessageType ActionMessageType
        {
            get { return actionMessageType; }
            set
            {
                if (value == actionMessageType) return;
                actionMessageType = value;
                OnPropertyChanged("ActionMessageType");
            }
        }

        private bool isSearching;
        public bool IsSearching
        {
            get { return isSearching; }
            set
            {
                if (value == isSearching) return;
                isSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }

        #endregion

        public ICommand ClearActionMessageCommand { get; private set; }

        private readonly IAsyncWorkerService asyncWorkerService;
        private readonly MasterSyncController syncController;

        public InfoBarViewModel(IAsyncWorkerService asyncWorkerService, MasterSyncController syncController, List<PageViewModel> pageViewModels)
        {
            ActionList = new ObservableCollection<ActionHistory>();

            this.asyncWorkerService = asyncWorkerService;
            this.syncController = syncController;
            this.pageViewModels = pageViewModels;

            InitialiseCommands();

            NotificationServerCheck();
            SyncServiceCheck();
        }

        private void InitialiseCommands()
        {         
            ClearActionMessageCommand = new DelegateCommand(ClearActionMessage);
        }

        private void ClearActionMessage(object state)
        {
            ActionMessage = "";
            ActionMessageType = MessageType.None;
            ActionList.Clear();
        }


        public void SetActionMessage(string message, MessageType type)
        {
            SendOrPostCallback callback = delegate
            {
                lock (Locker)
                {
                    if (!string.IsNullOrEmpty(ActionMessage))
                        ActionList.Add(new ActionHistory { Message = ActionMessage, MessageType = ActionMessageType, Time = actionTime });
                    if (ActionList.Count == ACTION_MESSAGE_HISTORY_SIZE) ActionList.RemoveAt(0);

                    ActionMessage = message;
                    ActionMessageType = type;
                    actionTime = DateTime.Now;
                }
            };
            asyncWorkerService.Post(callback);

        }

        public void NotificationServerCheckAsync()
        {
            asyncWorkerService.RunAsyncSystemOperation(NotificationServerCheck);
        }

        private void NotificationServerCheck()
        {
            NotificationState = LightColour.Amber;
            Thread.Sleep(100);
            NotificationState = NotificationCheck() ? LightColour.Green : LightColour.Red;
        }

        private static bool NotificationCheck()
        {
            try
            {
                return NotificationServiceClient.AuthenticateUser(Environment.UserName);
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                return false;
            }
        }

        public void SyncServiceCheckAsync()
        {
            asyncWorkerService.RunAsyncSystemOperation(SyncServiceCheck);
        }

        private void SyncServiceCheck()
        {
            SyncState = LightColour.Amber;
            Thread.Sleep(100);
            SyncState = syncController.SyncCheckAndRecover() ? LightColour.Green : LightColour.Red;
        }

        public void SetSyncLight(object sender, TrafficLightEventHandlerArgs args)
        {
            SyncState = args.Light;
        }

        

    }
}
