﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Service;
using UBM.MiddlewareClient.Services;
using UBM.MiddlewareClient.ViewModel.Child;
using UBM.MiddlewareClient.ViewModel.Pages;
using Application = System.Windows.Application;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    /// <summary>
    /// Controls all the View Models and Searching
    /// </summary>
    public partial class MasterViewModel : ViewModelBase
    {
        #region Fields

        // Services
        private IUserService userService;
        private IDedupeService dedupeService;
        private ICustomerService customerService;
        private IInvoiceService invoiceService;
        private IOrderService orderService;
        private IReceiptService receiptService;
        private ISystemService systemService;
        private IFeedRunnerService feedRunnerService;

        private INotificationService notificationService;
        private IAsyncWorkerService asyncWorkerService;

        public IMessageService MessageService { get; set; }

        private readonly SynchronizationContext uiContext = SynchronizationContext.Current;

        #endregion

        #region Public Properties

        public HomeViewModel HomeViewModel { get; set; }
        public CustomerViewModel CustomerViewModel { get; set; }
        public RejectionHistoryViewModel RejectionHistoryViewModel { get; set; }
        public OrderViewModel OrderViewModel { get; set; }
        public LegalHistoryViewModel LegalHistoryViewModel { get; set; }
        public DedupeViewModel DedupeViewModel { get; set; }
        public InvoiceViewModel InvoiceViewModel { get; set; }
        public ReceiptViewModel ReceiptViewModel { get; set; }
        public FeedRunnerViewModel FeedRunnerViewModel { get; set; }
        public DiagnosticsViewModel DiagnosticsViewModel { get; set; }

        public FiltersViewModel FiltersViewModel { get; set; }
        public InfoBarViewModel InfoBarViewModel { get; set; }
        public ICustomerDetailsViewModel CustomerDetailsViewModel { get; set; }

        public List<PageViewModel> ViewModels { get; private set; }

        public AccessController AccessController { get; set; }
        public MasterSyncController SyncController { get; set; }
        public IJobRunnerService JobRunnerService { get; set; }


        private ClientPageNumber currentPage;
        public ClientPageNumber CurrentPage
        {
            get { return currentPage; }
            set
            {
                if (value == currentPage) return;
                currentPage = value;
                OnPropertyChanged("CurrentPage");
            }
        }

        private bool busyAnimation;
        public bool BusyAnimation
        {
            get { return this.busyAnimation; }
            set
            {
                if (value == this.busyAnimation) return;
                this.busyAnimation = value;
                OnPropertyChanged("BusyAnimation");
            }
        }

        #endregion

        #region Private Methods


        private void TurnOnBusyAnimation(object sender, EventArgs args)
        {
            BusyAnimation = true;
        }

        private void TurnOffBusyAnimation(object sender, EventArgs args)
        {
            BusyAnimation = false;

        }

        private void UpdatePageCountString(ClientPageNumber page)
        {
            userService.UpdatePageCountString(page);
        }

        private void SystemFiltersChanged(object sender, EventArgs e)
        {
            if (CurrentPage == ClientPageNumber.CustomerDedupe)
            {
                DedupeViewModel.RefreshPage();       
            } 
        }

        private static void LoadConnections()
        {
            TraceHelper.Trace("Initialising Connections...");

            ConnectionManager.SetupConnectionManager(new List<Connection>
                                                         {
                                                           new Connection(ConnectionStringNames.MIDDLEWARE_COMMON),                                                             
                                                           new Connection(ConnectionStringNames.MIDDLEWARE_SYSTEM),
                                                           new Connection(ConnectionStringNames.MIDDLEWARE_ASOP_DALTONS),
                                                           new Connection(ConnectionStringNames.MIDDLEWARE_ASOP_UK),
                                                           new Connection(ConnectionStringNames.MIDDLEWARE_ASOP_HOLLAND),
                                                           new Connection(ConnectionStringNames.MIDDLEWARE_MSI),
                                                           new Connection(ConnectionStringNames.ASOP_UK),
                                                           new Connection(ConnectionStringNames.ASOP_HOLLAND),
                                                           new Connection(ConnectionStringNames.ASOP_DALTONS)
                                                         });

            TraceHelper.Trace("Connections initialised OK");
        }

        private void LoadServices()
        {
            TraceHelper.Trace("Initialising Services...");

            notificationService = new NotificationService();
            MessageService = new MessageService();
            MessageService.OnSendUserMessage += SetActionMessage;

            asyncWorkerService = new AsyncWorkerService(SynchronizationContext.Current, MessageService);
            asyncWorkerService.Started += TurnOnBusyAnimation;
            asyncWorkerService.Complete += TurnOffBusyAnimation;

            systemService = new SystemService();
            userService = new UserService(notificationService);
            AccessController = new AccessController(userService);
            AccessController.LoadPermissions();
            customerService = new CustomerService(AccessController.SearchTypes);
            orderService = new OrderService(AccessController.SearchTypes);
            dedupeService = new DedupeService(AccessController.SearchTypes);
            feedRunnerService = new FeedRunnerService();
            invoiceService = new InvoiceService();
            receiptService = new ReceiptService();

            SyncController = new MasterSyncController(uiContext, MessageService);

            JobRunnerService = new JobRunnerService(MessageService, systemService, notificationService, SyncController);


            TraceHelper.Trace("Services initialised OK");
        }

        private void LoadViewModels()
        {
            TraceHelper.Trace("Initialising ViewModels...");


            List<SalesSystem> salesSystems = systemService.GetSalesSystems();
            CustomerDetailsViewModel = new CustomerDetailsViewModel(MessageService);

            CustomerDetailsViewModel standardCustomerDetailsViewModel = new CustomerDetailsViewModel(MessageService, resendEnabled: false, mergeEnabled: false);
            CustomerDetailsViewModel resendCustomerDetailsViewModel = new CustomerDetailsViewModel(MessageService, resendEnabled: true, mergeEnabled: false);

            INotificationDialogViewModel dedupeNotificationViewModel = new NotificationViewModel(dedupeService.GetRejectionReasons(), notificationService, MessageService);
            INotificationDialogViewModel historyNotificationViewModel = new NotificationViewModel(dedupeService.GetRejectionReasons(), notificationService, MessageService);


            HomeViewModel = new HomeViewModel();
            CustomerViewModel = new CustomerViewModel(customerService,
                                                      asyncWorkerService,
                                                      MessageService,
                                                      standardCustomerDetailsViewModel);
            RejectionHistoryViewModel = new RejectionHistoryViewModel(customerService,
                                                                      historyNotificationViewModel,
                                                                      MessageService,
                                                                      resendCustomerDetailsViewModel,
                                                                      asyncWorkerService);
            OrderViewModel = new OrderViewModel(orderService,
                                                asyncWorkerService);
            LegalHistoryViewModel = new LegalHistoryViewModel(customerService,
                                                              MessageService,
                                                              asyncWorkerService,
                                                              standardCustomerDetailsViewModel);
            InvoiceViewModel = new InvoiceViewModel(invoiceService,
                                                    notificationService,
                                                    MessageService,
                                                    JobRunnerService,
                                                    SyncController,                                                    
                                                    asyncWorkerService,
                                                    AccessController.HasWriteAccess(ClientPageNumber.InvoicePublication));
            DedupeViewModel = new DedupeViewModel(dedupeService,
                                                   dedupeNotificationViewModel,
                                                   MessageService,
                                                   CustomerDetailsViewModel,                                                   
                                                   SyncController,
                                                   salesSystems,
                                                   asyncWorkerService,
                                                   AccessController.HasWriteAccess(ClientPageNumber.CustomerDedupe));
            ReceiptViewModel = new ReceiptViewModel(receiptService,
                                                    notificationService,
                                                    MessageService,
                                                    JobRunnerService,
                                                    asyncWorkerService,
                                                    AccessController.HasWriteAccess(ClientPageNumber.PrintReceipts));

            FeedRunnerViewModel = new FeedRunnerViewModel(feedRunnerService, MessageService, JobRunnerService, systemService);
            DiagnosticsViewModel = new DiagnosticsViewModel(MessageService);

            // View Models
            ViewModels = new List<PageViewModel>
                             {
                                 HomeViewModel,
                                 CustomerViewModel,
                                 RejectionHistoryViewModel,
                                 OrderViewModel,
                                 LegalHistoryViewModel,
                                 InvoiceViewModel,
                                 DedupeViewModel,
                                 ReceiptViewModel,
                                 FeedRunnerViewModel,
                                 DiagnosticsViewModel
                             };


            FiltersViewModel = new FiltersViewModel(salesSystems, ViewModels);

            FiltersViewModel.SystemFiltersChanged += SystemFiltersChanged;

            InfoBarViewModel = new InfoBarViewModel(asyncWorkerService, SyncController, ViewModels);

            TraceHelper.Trace("ViewModels initialised OK");
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterViewModel"/> class.
        /// </summary>
        public MasterViewModel()
        {

            TraceHelper.Trace("Initialising MasterViewModel");


            LoadConnections();
            LoadServices();
            LoadViewModels();

            InitialiseCommands();

            MessageService.SendUserMessage("Welcome to Middleware", MessageType.Good);
        }

        #endregion

        #region Public Methods

        public void SetActionMessage(string message, MessageType type)
        {
            InfoBarViewModel.SetActionMessage(message, type);
        }

        public void RequestClose()
        {

            MessageBoxArgs messageBoxArgs = new MessageBoxArgs
                                                {
                                                    Title = "Middleware Closing",
                                                    MessageType = MessageType.Info,
                                                };

            if (DedupeViewModel.HasActiveLocks() || InvoiceViewModel.HasActiveLocks())
            {
                messageBoxArgs.Message = "There are still active locks on one more pages. Do you wish to continue?";
            }
            else
            {
                messageBoxArgs.Message = "Are you sure you want to shutdown Middleware?";
            }

            if (MessageService.OpenMessageBoxWithResult(messageBoxArgs) == DialogResult.OK)
            {
                MessageService.CloseAnyOpenDialogs();
                Shutdown();
            }

        }

        private void Shutdown()
        {
            InvoiceViewModel.ClearLocks();
            SyncController.Disconnect();
            SaveMostUsedPages();

            MessageService.CloseDialog(DialogType.MessageBox);

            Application.Current.Shutdown();
        }

        public void SaveMostUsedPages()
        {
            userService.UpdateMostUsed();
        }

        public List<int> GetTopFivePages()
        {
            return userService.GetTopFivePages();
        }

        public void SetPage(ClientPageNumber pageNumber)
        {

            if (CurrentPage != pageNumber)
            {
                PageViewModel current = ViewModels.FirstOrDefault(c => c.PageConfiguration.Page == pageNumber);

                CurrentPage = current.PageConfiguration.Page;

                FiltersViewModel.SetFiltersProperties(current.PageConfiguration);

                current.Load();

                UpdatePageCountString(pageNumber);
            }
        }

        #endregion

        protected override void OnDispose()
        {

        }
    }
}