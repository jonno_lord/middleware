﻿using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    public abstract class PageViewModel : ViewModelBase, IDrillDown
    {
        protected bool LoadedFirstTime;
        protected bool HasWriteAccess;

        public abstract PageConfiguration PageConfiguration { get; }

        public virtual void Load()
        {
            LoadedFirstTime = true;
        }

        public virtual void Unload()
        {
            
        }

        public virtual void RefreshPage()
        {
            
        }

        #region IDrillDown

        public event DrillDownEventHandler OnDrillDown;

        public virtual void DrillDown(object sender)
        {
            DrillDownEventHandler handler = OnDrillDown;
            if (handler != null) handler(sender);
        }

        #endregion
    }
}
