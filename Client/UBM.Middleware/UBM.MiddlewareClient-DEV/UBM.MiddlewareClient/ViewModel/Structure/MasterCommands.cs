﻿using System.Linq;
using System.Windows.Input;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    public partial class MasterViewModel : ViewModelBase
    {

        public ICommand RefreshCommand { get; private set; }
        public ICommand NavigateCommand { get; private set; }


        private void InitialiseCommands()
        {
            RefreshCommand = new DelegateCommand(Refresh, CanRefresh);
            NavigateCommand = new DelegateCommand(Navigate, CanNavigate);
        }

        private void Refresh(object state)
        {
            ViewModels.FirstOrDefault(c => c.PageConfiguration.Page == CurrentPage).RefreshPage();
        }
        private bool CanRefresh(object state)
        {
            if (ViewModels != null)
            {
                PageViewModel page = ViewModels.FirstOrDefault(c => c.PageConfiguration.Page == CurrentPage);
                return page.GetType().GetMethod("RefreshPage").DeclaringType != typeof(PageViewModel);
            }
            return false;
        }

        private static void Navigate(object state)
        {
            // Dummy
        }
        private bool CanNavigate(object state)
        {
            return !MessageService.AnyDialogsOpen();
        }
 
    }
}