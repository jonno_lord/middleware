﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.ViewHelper;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    public class FiltersViewModel : ViewModelBase
    {
        #region Filter Properties

        private string searchField;
        public string SearchField
        {
            get { return searchField; }
            set
            {
                if (value == searchField) return;
                searchField = value;
                OnPropertyChanged("SearchField");
            }
        }

        private bool useDateRange;
        public bool UseDateRange
        {
            get { return useDateRange; }
            set
            {
                if (value == useDateRange) return;
                useDateRange = value;
                OnPropertyChanged("UseDateRange");
            }
        }

        private DateTime? searchStartDate;
        public DateTime? SearchStartDate
        {
            get { return searchStartDate; }
            set
            {
                if (value == searchStartDate) return;
                searchStartDate = value;
                OnPropertyChanged("SearchStartDate");
            }
        }

        private DateTime? searchEndDate;
        public DateTime? SearchEndDate
        {
            get { return searchEndDate; }
            set
            {
                if (value == searchEndDate) return;
                searchEndDate = value;
                OnPropertyChanged("SearchEndDate");
            }
        }

        private bool filtersVisible;
        public bool FiltersVisible
        {
            get { return this.filtersVisible; }
            set
            {
                if (value == this.filtersVisible) return;
                this.filtersVisible = value;
                OnPropertyChanged("FiltersVisible");
            }
        }

        private bool propertySearch;
        public bool PropertySearch
        {
            get { return this.propertySearch; }
            set
            {
                if (value == this.propertySearch) return;
                this.propertySearch = value;
                OnPropertyChanged("PropertySearch");
            }
        }

        private bool dateSearch;
        public bool DateSearch
        {
            get { return this.dateSearch; }
            set
            {
                if (value == this.dateSearch) return;
                this.dateSearch = value;
                OnPropertyChanged("DateSearch");
            }
        }

        private ObservableCollection<ComboItem<SearchMatching>> patternMatches;
        public ObservableCollection<ComboItem<SearchMatching>> PatternMatches
        {
            get { return this.patternMatches; }
            set
            {
                if (value == this.patternMatches) return;
                this.patternMatches = value;
                OnPropertyChanged("PatternMatches");
            }
        }

        private ComboItem<SearchMatching> selectedPatternMatch;
        public ComboItem<SearchMatching> SelectedPatternMatch
        {
            get { return this.selectedPatternMatch; }
            set
            {
                if (value == this.selectedPatternMatch) return;
                this.selectedPatternMatch = value;
                OnPropertyChanged("SelectedPatternMatch");
            }
        }

        private ObservableCollection<ComboItem<SearchBy>> searchProperties;
        public ObservableCollection<ComboItem<SearchBy>> SearchProperties
        {
            get { return this.searchProperties; }
            set
            {
                if (value == this.searchProperties) return;
                this.searchProperties = value;
                OnPropertyChanged("SearchProperties");
            }
        }

        private ComboItem<SearchBy> selectedSearchProperty;
        public ComboItem<SearchBy> SelectedSearchProperty
        {
            get { return this.selectedSearchProperty; }
            set
            {
                if (value == this.selectedSearchProperty) return;
                this.selectedSearchProperty = value;
                OnPropertyChanged("SelectedSearchProperty");
            }
        }

        private ObservableCollection<ComboItem<SearchOnDates>> dateFilterProperties;
        public ObservableCollection<ComboItem<SearchOnDates>> DateFilterProperties
        {
            get { return this.dateFilterProperties; }
            set
            {
                if (value == this.dateFilterProperties) return;
                this.dateFilterProperties = value;
                OnPropertyChanged("DateFilterProperties");
            }
        }

        private ComboItem<SearchOnDates> selectedDateFilterProperty;
        public ComboItem<SearchOnDates> SelectedDateFilterProperty
        {
            get { return this.selectedDateFilterProperty; }
            set
            {
                if (value == this.selectedDateFilterProperty) return;
                this.selectedDateFilterProperty = value;
                OnPropertyChanged("SelectedDateFilterProperty");
            }
        }

        private ObservableCollection<PageViewModel> searchableViewModels;
        public ObservableCollection<PageViewModel> SearchableViewModels
        {
            get
            {
                if (pages == null) return null;
                searchableViewModels = new ObservableCollection<PageViewModel>(pages.Where(c => c.PageConfiguration.FiltersVisible));
                return searchableViewModels;
            }
        }

        private PageViewModel selectedSearchableViewModel;
        public PageViewModel SelectedSearchableViewModel
        {
            get { return this.selectedSearchableViewModel; }
            set
            {
                if (value == this.selectedSearchableViewModel) return;
                this.selectedSearchableViewModel = value;
                OnPropertyChanged("SelectedSearchableViewModel");
            }
        }

        private bool systemsSelectAll;
        public bool SystemsSelectAll
        {
            get { return systemsSelectAll; }
            set
            {
                if (value == systemsSelectAll) return;
                systemsSelectAll = value;
                OnPropertyChanged("SystemsSelectAll");
            }
        }

        private List<SalesSystem> salesSystems;
        public List<SalesSystem> SalesSystems
        {
            get { return salesSystems; }
            set
            {
                if (value == salesSystems) return;
                salesSystems = value;
                OnPropertyChanged("SalesSystems");
            }
        }

        #endregion

        public ICommand SystemFilterChangedCommand { get; private set; }
        public ICommand SearchCommand { get; private set; }
        public ICommand SelectAllSystemsCommand { get; private set; }

      
        private readonly List<PageViewModel> pages;

        private const int MAX_SEARCH_SIZE = 200;

        public event EventHandler SystemFiltersChanged;

        public void OnSystemFiltersChanged(EventArgs e)
        {
            if (SystemFiltersChanged != null)
            {
                SystemFiltersChanged(this, e);
            }
        }

        public FiltersViewModel(List<SalesSystem> salesSystems, List<PageViewModel> pages)
        {
            this.SalesSystems = salesSystems;
            this.pages = pages;

            PatternMatches = new ObservableCollection<ComboItem<SearchMatching>>
            {
                new ComboItem<SearchMatching>{ Name = "Fuzzy", Value = SearchMatching.Fuzzy }, 
                new ComboItem<SearchMatching>{ Name = "Starts With", Value = SearchMatching.StartsWith },
                new ComboItem<SearchMatching>{ Name = "Exact", Value = SearchMatching.Exact }
            };
            SystemsSelectAll = true;

            InitialiseCommands();
        }

        private PageConfiguration pageConfiguration;

        public void SetFiltersProperties(PageConfiguration pageConfig)
        {
            this.pageConfiguration = pageConfig;

            FiltersVisible = pageConfiguration.FiltersVisible;
            PropertySearch = pageConfiguration.PropertySearch;
            DateSearch = pageConfiguration.DateSearch;
            SearchProperties = new ObservableCollection<ComboItem<SearchBy>>(pageConfiguration.SearchPropertiesList);
            DateFilterProperties = new ObservableCollection<ComboItem<SearchOnDates>>(pageConfiguration.DateFilterPropertiesList);

            if (PatternMatches.Count > 0) SelectedPatternMatch = PatternMatches[0];
            if (DateFilterProperties.Count > 0) SelectedDateFilterProperty = DateFilterProperties[0];
            if (SearchProperties.Count > 0) SelectedSearchProperty = SearchProperties[0];
        }

        private void InitialiseCommands()
        {
            SystemFilterChangedCommand = new DelegateCommand(SystemFilterChanged);
            SearchCommand = new DelegateCommand(Search);
            SelectAllSystemsCommand = new DelegateCommand(SelectAllSystems);
        }

        private void SystemFilterChanged(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            // Flip the state of a system
            if (!string.IsNullOrEmpty(state as string))
            {
                salesSystems.Where(c => c.Name == state.ToString())
                            .FirstOrDefault()
                            .IsSelected ^= true;
            }

            SystemsSelectAll = salesSystems.FindAll(c => c.IsSelected).Count == salesSystems.Count;


            OnSystemFiltersChanged(EventArgs.Empty);
        }

        private ISearchable GetCurrentPage()
        {
            return pages.FirstOrDefault(c => c.PageConfiguration.Page == pageConfiguration.Page) as ISearchable;
        }

        private void Search(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            ISearchable viewModel = GetCurrentPage();

            SearchInfo searchInfo = new SearchInfo
            {
                Term = SearchField ?? "",
                By = SelectedSearchProperty.Value,
                From = SearchStartDate,
                To = SearchEndDate,
                Matching = SelectedPatternMatch.Value,
                OrderBy = "",
                FilterOnDates = UseDateRange,
                DateFieldName = SelectedDateFilterProperty == null ? (SearchOnDates?)null : SelectedDateFilterProperty.Value,
                Systems = new List<string>(salesSystems.Where(c => c.IsSelected).Select(c => c.Name)),
                MaxRecords = MAX_SEARCH_SIZE
            };

            viewModel.SearchAsync(searchInfo);
        }

        private void SelectAllSystems(object state)
        {
            if (SystemsSelectAll)
            {
                salesSystems.FindAll(s => !s.IsSelected)
                            .ForEach(s => s.IsSelected = true);

                SystemFilterChanged(null);
            }

        }
    }
}
