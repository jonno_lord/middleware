﻿using System;
using System.Collections.Generic;
using System.Threading;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public class LegalHistoryViewModel : GenericPageableViewModel<Legal>, ISearchable , ISupportsCustomerDetails
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.LegalHistory,
            DisplayName = "Legal History",
            ObjectDisplayName = "Legal Hold",
            FiltersVisible = true,
            PropertySearch = true,
            DateSearch = true,
            SearchPropertiesList = new List<ComboItem<SearchBy>>
                                    {
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.CUSTOMER_NAME, Value = SearchBy.CustomerName },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.ACTIONED_BY, Value = SearchBy.Sender }
                                    },
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };

        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        public override void Load()
        {
            //@JP Users requested refresh on load each time
            //if(!LoadedFirstTime)
            {
                asyncWorkerService.RunAsyncUserOperation(LoadAsync, "Legal history load");
            }
        }

        private void LoadAsync()
        {
            this.legalHistory = customerService.SearchLegal(null);

            SendOrPostCallback callback = delegate
                                          {
                                              BeginPaging(legalHistory);
                                          };
            asyncWorkerService.Post(callback);
            
            base.Load();


        }

        public override void RefreshPage()
        {
            Search(lastSearch);
        }

        #endregion

        #region Fields

        
        private readonly ICustomerService customerService;
        private readonly IAsyncWorkerService asyncWorkerService;
        private readonly IMessageService messageService;
        private Legal selectedLegal;
        private List<Legal> legalHistory;
        private SearchInfo lastSearch;

        #endregion

        public ICustomerDetailsViewModel CustomerDetailsViewModel { get; set; }

        #region Constructor

        public LegalHistoryViewModel(ICustomerService customerService, 
                                     IMessageService messageService, 
                                     IAsyncWorkerService asyncWorkerService, 
                                     ICustomerDetailsViewModel customerDetailsViewModel)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.messageService = messageService;
            this.asyncWorkerService = asyncWorkerService;
            this.customerService = customerService;
            this.CustomerDetailsViewModel = customerDetailsViewModel;
            this.customerService.OnDrillDown += DrillDown;

            CustomerDetailsViewModel.CustomerDetailsClosed += OnCustomerDetailsClosed;

        }

        private void OnCustomerDetailsClosed(object sender, EventArgs e)
        {
            if (selectedLegal != null)
            {
                selectedLegal.IsClicked = false;
            }
        }

        #endregion

        #region ISearchable

        public void SearchAsync(SearchInfo searchInfo)
        {
            asyncWorkerService.RunAsyncUserOperation(() => Search(searchInfo), "Legal history search");            
        }

        public int Search(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            lastSearch = searchInfo;

            List<Legal> legal = customerService.SearchLegal(searchInfo);

            SendOrPostCallback callback = delegate
            {
                BeginPaging(legal);
            };
            asyncWorkerService.Post(callback);


            return legal.Count;
        }


        #endregion


        #region IDrillDown

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Legal legal = sender as Legal;
            if (legal != null)
            {
                selectedLegal = legal;
                Customer customer = customerService.GetCustomer(legal.CustomerID, legal.SystemName);
                if(customer != null)
                {
                    CustomerDetailsViewModel.SelectedCustomer = customer;
                    //base.DrillDown(CustomerDetailsViewModel.SelectedCustomer);
                    messageService.OpenDialog(DialogType.CustomerDetails);
                }
                else
                {
                    messageService.SendUserMessage("Customer not found", MessageType.Error);
                }
            }
        }

        #endregion

        #region IDisposable

        protected override void OnDispose()
        {
            customerService.OnDrillDown -= DrillDown;
            base.OnDispose();
        }

        #endregion
    }
}