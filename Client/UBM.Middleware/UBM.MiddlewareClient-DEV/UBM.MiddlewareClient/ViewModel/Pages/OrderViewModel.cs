﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    /// <summary>
    /// Controls Customer Order Collections
    /// </summary>
    public class OrderViewModel : GenericPageableViewModel<Order>, ISearchable
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.OrderViewer,
            DisplayName = "Order Viewer",
            ObjectDisplayName = "Order",
            FiltersVisible = true,
            PropertySearch = true,
            DateSearch = false,
            SearchPropertiesList = new List<ComboItem<SearchBy>>
                                    {
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.ORDER_NUMBER, Value = SearchBy.OrderNumber },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.BATCH_NUMBER, Value = SearchBy.BatchNumber },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.JDE_NUMBER, Value = SearchBy.JDENumber },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.CUSTOMER_NAME, Value = SearchBy.CustomerName }

                                    },
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };

        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        #endregion

        #region Fields

        private readonly IOrderService orderService;
        private readonly IAsyncWorkerService asyncWorkerService;
        private List<Order> orders;

        #endregion

        #region Properties

        private Order selectedOrder;
        public Order SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                if (value == selectedOrder) return;
                selectedOrder = value;
                OnPropertyChanged("SelectedOrder");
            }
        }

        private DataSet orderDetails;
        public DataSet OrderDetails
        {
            get { return orderDetails; }
            set
            {
                if (value == orderDetails) return;
                orderDetails = value;
                OnPropertyChanged("OrderDetails");
            }
        }

        #endregion

        #region Constructor

        public OrderViewModel(IOrderService orderService, IAsyncWorkerService asyncWorkerService)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.asyncWorkerService = asyncWorkerService;
            this.orderService = orderService;
            this.orders = new List<Order>();

            orderService.OnDrillDown += DrillDown;
        }

        #endregion

        #region Public Methods


        public void SearchAsync(SearchInfo searchInfo)
        {
            asyncWorkerService.RunAsyncUserOperation(() => Search(searchInfo), "Order search");            
        }


        public int Search(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            OrderDetails = null;
            orders = orderService.SearchOrders(searchInfo);

            SendOrPostCallback callback = delegate
            {
                BeginPaging(orders);
            };
            asyncWorkerService.Post(callback);

            return orders.Count;
        }

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            SelectedOrder = (Order)sender;
            OrderDetails = orderService.GetOrderDetails(SelectedOrder);

            base.DrillDown(sender);
        }

        protected override void OnDispose()
        {
            orderService.OnDrillDown -= DrillDown;
        }



        #endregion
    }
}