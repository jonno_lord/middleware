﻿using System;
using System.Collections.Generic;
using System.Threading;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    /// <summary>
    /// View model controlling the collections of past customer rejections
    /// </summary>
    public class RejectionHistoryViewModel : GenericPageableViewModel<Email>, ISearchable, ISupportsCustomerDetails //, ICustomerDetailsViewModel
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.EmailHistory,
            DisplayName = "Email History",
            ObjectDisplayName = "Email",
            FiltersVisible = true,
            PropertySearch = true,
            DateSearch = true,
            SearchPropertiesList = new List<ComboItem<SearchBy>>
                                    {
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.CUSTOMER_NAME, Value = SearchBy.CustomerName },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.SENDER, Value = SearchBy.Sender },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.REASON, Value = SearchBy.Reason }
                                    },
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };

        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        public override void Load()
        {
            //@JP Users requested refresh on load each time
            //if(!LoadedFirstTime)
            {
                asyncWorkerService.RunAsyncUserOperation(LoadAsync, "Email history load");
            }
        }

        private void LoadAsync()
        {
            this.rejectionHistory = customerService.SearchEmails(null);

            SendOrPostCallback callback = delegate
            {
                BeginPaging(rejectionHistory);
            };
            asyncWorkerService.Post(callback);

            base.Load();
        }

        public override void RefreshPage()
        {
            Search(lastSearch);
        }

        #endregion

        #region Fields


        private readonly ICustomerService customerService;
        private readonly IAsyncWorkerService asyncWorkerService;
        private readonly IMessageService messageService;
        private List<Email> rejectionHistory;
        private Email selectedEmail;

        private SearchInfo lastSearch;

        #endregion

        #region Properties 


        public INotificationDialogViewModel NotificationViewModel { get; private set; }

        public ICustomerDetailsViewModel CustomerDetailsViewModel { get; set; }

        #endregion

        #region Constructor

        public RejectionHistoryViewModel(ICustomerService customerService, 
                                         INotificationDialogViewModel notificationDialogViewModel, 
                                         IMessageService messageService, 
                                         ICustomerDetailsViewModel customerDetailsViewModel,
                                         IAsyncWorkerService asyncWorkerService)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.customerService = customerService;
            this.messageService = messageService;
            this.asyncWorkerService = asyncWorkerService;
            this.CustomerDetailsViewModel = customerDetailsViewModel;

            this.customerService.OnDrillDown += DrillDown;
            this.CustomerDetailsViewModel.CustomerDetailsClosed += OnCustomerDetailsClosed;
            this.CustomerDetailsViewModel.ResendEmail += OnPrepareResendEmail;

            this.NotificationViewModel = notificationDialogViewModel;
            this.NotificationViewModel.AcceptNotification += OnAcceptNotification;
        }

        private void OnCustomerDetailsClosed(object sender, EventArgs e)
        {
            selectedEmail.IsClicked = false;
        }

        private void OnPrepareResendEmail(object sender, EventArgs e)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email email = sender as Email;

            if (email != null)
            {
                this.NotificationViewModel.PrepareResend(CustomerDetailsViewModel.SelectedCustomer, email);
                
                messageService.OpenDialog(DialogType.RejectNotification);
            }
        }

        private void OnAcceptNotification(object sender, EventArgs e)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email resend = NotificationViewModel.Resend();

            customerService.AddRejectionEmail(resend);

            CustomerDetailsViewModel.SelectedCustomer.Emails.Add(resend);
        }

        #endregion

        #region ISearchable

        public void SearchAsync(SearchInfo searchInfo)
        {
            asyncWorkerService.RunAsyncUserOperation(() => Search(searchInfo), "Email history search");            
        }

        public int Search(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            lastSearch = searchInfo;

            List<Email> emails = customerService.SearchEmails(searchInfo);

            SendOrPostCallback callback = delegate
            {
                BeginPaging(emails);
            };
            asyncWorkerService.Post(callback);

            return emails.Count;
        }

        #endregion
    

        #region IDrillDown

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email email = sender as Email;
            if (email != null)
            {
                selectedEmail = email;
                Customer customer = customerService.GetCustomer(email.CustomerID, email.SystemName);
                if (customer != null)
                {
                    CustomerDetailsViewModel.SelectedCustomer = customer;
                    //base.DrillDown(CustomerDetailsViewModel.SelectedCustomer);
                    messageService.OpenDialog(DialogType.CustomerDetails);
                }
                else
                {
                    messageService.SendUserMessage("Customer not found", MessageType.Error);
                }
            }
        }

        #endregion

      
    }
}