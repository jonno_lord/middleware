﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Invoicing;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;
using UBM.NotificationClient;
using UBM.Utilities.HtmlReportGenerator;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public partial class InvoiceViewModel : PageViewModel
    {
        #region Page Definition
       
        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.InvoicePublication,
            DisplayName = "Invoice Publication",
            ObjectDisplayName = "Invoice",
            FiltersVisible = false,
            PropertySearch = false,
            DateSearch = false,
            SearchPropertiesList = new List<ComboItem<SearchBy>>(),
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };

        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        public override void Load()
        {
            if(!LoadedFirstTime)
            {
                invoiceSync = new InvoiceSyncController(masterSyncController, SelectedPublication.Invoices);
                ClearLocks();
                base.Load();
            }
        }

        #endregion

        #region Fields

        private readonly IInvoiceService invoiceService;
        private readonly INotificationService notificationService;
        private readonly IMessageService messageService;
        private InvoiceSyncController invoiceSync;
        private readonly IJobRunnerService jobRunnerService;

        private readonly MasterSyncController masterSyncController;
        private readonly IAsyncWorkerService asyncWorkerService;

        private HtmlReport report;

        private List<Invoice> processingInvoices;

  
        private static readonly List<ComboItem<string>> ASOPSystems = new List<ComboItem<string>>
                                        {
                                            new ComboItem<string>{ Name = "ASOPUK", Value = MiddlewareSystemNames.ASOP_UK },
                                            new ComboItem<string>{ Name = "ASOPDaltons", Value = MiddlewareSystemNames.ASOP_DALTONS },
                                            new ComboItem<string>{ Name = "ASOPHolland", Value = MiddlewareSystemNames.ASOP_HOLLAND }
                                        };
        #endregion

        #region Properties
        
        public List<ComboItem<string>> AsopSystems
        {
            get { return ASOPSystems; }
        }


        private ComboItem<string> selectedAsopSystem;
        public ComboItem<string> SelectedAsopSystem
        {
            get { return this.selectedAsopSystem; }
            set
            {
                if (value == this.selectedAsopSystem) return;
                this.selectedAsopSystem = value;
                OnPropertyChanged("SelectedAsopSystem");

                GetPublicationsAsync();

            }
        }

        private bool viewInvoicesTab;
        public bool ViewInvoicesTab
        {
            get { return this.viewInvoicesTab; }
            set
            {
                if (value == this.viewInvoicesTab) return;
                this.viewInvoicesTab = value;
                OnPropertyChanged("ViewInvoicesTab");
            }
        }

        private ObservableCollection<Invoice> selectedInvoices;
        public ObservableCollection<Invoice> SelectedInvoices
        {
            get { return selectedInvoices; }
            set
            {
                if (value == selectedInvoices) return;
                selectedInvoices = value;
                OnPropertyChanged("SelectedInvoices");
            }
        }

        private ObservableCollection<Publication> publications;
        public ObservableCollection<Publication> Publications
        {
            get { return publications; }
            set
            {
                if (value == publications) return;
                publications = value;
                OnPropertyChanged("Publications");
            }
        }

        private Publication selectedPublication;
        public Publication SelectedPublication
        {
            get { return selectedPublication; }
            set
            {
                if (value == selectedPublication) return;
                selectedPublication = value;
                OnPropertyChanged("SelectedPublication");
                OnPropertyChanged("ShowOutstandingInvoicesEnabled");
            }
        }

        private Invoice selectedInvoice;
        public Invoice SelectedInvoice
        {
            get { return selectedInvoice; }
            set
            {
                if (value == selectedInvoice) return;
                selectedInvoice = value;
                OnPropertyChanged("SelectedInvoice");
            }
        }

        private bool showOutstandingInvoices;
        public bool ShowOutstandingInvoices
        {
            get { return showOutstandingInvoices; }
            set
            {
                //if (value == showOutstandingInvoices) return;                
                showOutstandingInvoices = value;
                OnPropertyChanged("ShowOutstandingInvoices");

                GetInvoicesAsync();

            }
        }

        public bool ShowOutstandingPublicationsEnabled
        {
            get { return Publications != null && Publications.Count > 0; }
        }
         
        private bool showOutstandingPublications;
        public bool ShowOutstandingPublications
        {
            get { return showOutstandingPublications; }
            set
            {
                if (value == showOutstandingPublications) return;
                showOutstandingPublications = value;
                OnPropertyChanged("ShowOutstandingPublications");

                GetPublicationsAsync();

            }
        }

        public bool ShowOutstandingInvoicesEnabled
        {
            get { return SelectedPublication.ID != 0; }
        }

        #region Progress Bar

        private bool jobRunning;
        public bool JobRunning
        {
            get { return jobRunning; }
            set
            {
                if (value == jobRunning) return;
                jobRunning = value;
                OnPropertyChanged("JobRunning");
            }
        }

        private string jobName;
        public string JobName
        {
            get { return this.jobName; }
            set
            {
                if (value == this.jobName) return;
                this.jobName = value;
                OnPropertyChanged("JobName");
            }
        }

        private string stepName;
        public string StepName
        {
            get { return this.stepName; }
            set
            {
                if (value == this.stepName) return;
                this.stepName = value;
                OnPropertyChanged("StepName");
            }
        }

        private double percentComplete;
        public double PercentComplete
        {
            get { return this.percentComplete; }
            set
            {
                if (value == this.percentComplete) return;
                this.percentComplete = value;
                OnPropertyChanged("PercentComplete");
            }
        }

        private bool progressBarEnabled;
        public bool ProgressBarEnabled
        {
            get { return this.progressBarEnabled; }
            set
            {
                if (value == this.progressBarEnabled) return;
                this.progressBarEnabled = value;
                OnPropertyChanged("ProgressBarEnabled");
            }
        }

        #endregion 

        #endregion

        #region Private Methods

        #region Invoices/Publications

        private void GetInvoicesAsync()
        {
            TraceHelper.Trace(TraceType.ViewModel);
            asyncWorkerService.RunAsyncUserOperation(GetInvoices, "Get invoices", true);
        }

        private void GetInvoices()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            UnsubscribeInvoices();

            SelectedInvoice = null;

            lock (SelectedPublication)
            {
                List<Invoice> asyncInvoices = invoiceService.GetInvoices(selectedAsopSystem.Value, SelectedPublication, ShowOutstandingInvoices);

                SendOrPostCallback callback = delegate
                {
                    SelectedPublication.Invoices = new ObservableCollection<Invoice>(asyncInvoices);
                    SubscribeInvoices();
                    invoiceSync.SyncClientCollection(SelectedPublication.Invoices);
                };
                asyncWorkerService.Post(callback);
            }

        }

        private void GetPublicationsAsync()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            asyncWorkerService.RunAsyncUserOperation(GetPublications, "Get publications", true);
        }

        private void GetPublications()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            UnsubscribePublications();
            UnsubscribeInvoices();

            SelectedPublication = Publication.GetNullPublication();

            List<Publication> asyncPublications = invoiceService.GetPublications(selectedAsopSystem.Name, ShowOutstandingPublications);


            SendOrPostCallback callback = delegate
            {
                Publications = new ObservableCollection<Publication>(asyncPublications);
                SubscribePublications();
                Publications.ToList().ForEach(c => c.IsClicked = false);
            };

            asyncWorkerService.Post(callback);

            OnPropertyChanged("ShowOutstandingPublicationsEnabled");

            invoiceSync.SyncClientCollection(SelectedPublication.Invoices);

        }

        private void SubscribePublications()
        {
            Publications.ToList().ForEach(c=>c.OnDrillDown += DrillDown);
        }

        private void UnsubscribePublications()
        {
            Publications.ToList().ForEach(c => c.OnDrillDown -= DrillDown);
        }

        private void SubscribeInvoices()
        {
            SelectedPublication.Invoices.ToList().ForEach(c => c.OnDrillDown += DrillDown);
        }

        private void UnsubscribeInvoices()
        {
            SelectedPublication.Invoices.ToList().ForEach(c => c.OnDrillDown -= DrillDown);          
        }

        private void SelectPublication(Publication publication)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            ShowOutstandingInvoices = ShowOutstandingPublications;

            ViewInvoicesTab = true;

            SelectedPublication = publication;
            SelectedInvoice = null;

            SubscribeInvoices();

            SelectedPublication.Invoices.ToList().ForEach(c => c.IsClicked = false);

            invoiceSync.SyncClientCollection(SelectedPublication.Invoices);



        }

        private void SelectInvoice(Invoice invoice)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            SelectedInvoice = invoice;
        }

        private void AddBatchNumbers(IEnumerable<Invoice> processedInvoices)
        {

            List<Invoice> systemsRequiringBatchNumbers = processedInvoices.GroupBy(c => c.SystemName)
                                                                          .Distinct()
                                                                          .Select(c => c.FirstOrDefault())
                                                                          .ToList();

            string batchNumbersSummary = "";

            foreach (Invoice system in systemsRequiringBatchNumbers)
            {
                int batchNumber = invoiceService.GetBatchNumber(system);

                batchNumbersSummary += system.SystemName + " - Batch Number: " + batchNumber + "<br/>";
                messageService.SendUserMessage(system.SystemName + " - Batch Number: " + batchNumber, MessageType.Good);
            }

            report.AddParagraph(batchNumbersSummary);

        }

        private void CleanUpListsAfterJob()
        {
            if (processingInvoices.Count > 0)
            {
                processingInvoices.Clear();
                ClearLocks();
                SelectedInvoices.Clear();
                GetInvoicesAsync();
            }
        }

        #endregion

        #region Job Running


        private void RunJobs(List<string> jobNames)
        {
            
            jobRunnerService.JobComplete += JobComplete;
            jobRunnerService.JobProgress += JobProgress;

            ProgressBarEnabled = true;
            JobRunning = true;

            jobRunnerService.RunJobs(jobNames);
        }

        private void JobComplete(object sender, EventArgs e)
        {
            AddBatchNumbers(processingInvoices);

            notificationService.SendEmail(new EmailMessageWrapper
            {
                From = "Middleware Client",
                To = Environment.UserName,
                Subject = "Middleware Job Report",
                Body = report.ToString(),
                Priority = Priority.Normal,
                IsHtml = true
            });

            CleanUpListsAfterJob();         

            asyncWorkerService.RunAsyncSystemOperation(delegate
                                                           {
                                                               Thread.Sleep(2000);
                                                               ProgressBarEnabled = false;
                                                               JobRunning = false;
                                                               
                                                           });
            jobRunnerService.JobComplete -= JobComplete;
            jobRunnerService.JobProgress -= JobProgress;
        }


        private void JobProgress(object sender, JobProgressEventHandlerArgs args)
        {
            JobName = args.JobName;
            StepName = args.StepDetail;
            PercentComplete = args.PercentageComplete;

            if (percentComplete == 0)
            {
                report = new HtmlReport();
                report.AddHeader("Middleware Invoicing Job Report:");

                if (processingInvoices.Count > 0)
                {
                    Table table = HtmlReport.CreateTable(TableStyleName.MiddlewarePublicationInvoicingReport);
                    table.AddHeader(new List<string> { "Invoice", "System", "Issue Date" });
                    foreach (Invoice invoice in processingInvoices)
                    {
                        table.AddRow(new List<string> { invoice.Publication.Description, invoice.SystemName, invoice.IssueDate.Date.ToShortDateString() });
                    }
                    report.AddTable(table);
                }

            }

            if (args.StepDetail.Contains("Complete"))
                report.AddParagraph(args.JobName + " - " + args.StepDetail);

            if (args.StepDetail.Contains("Failed"))
                report.AddParagraph(args.JobName + " - " + args.StepDetail, TextStyleName.Failure);

        }

        #endregion

        #endregion

        #region Constructor

        public InvoiceViewModel(IInvoiceService invoiceService, INotificationService notificationService, 
                                IMessageService messageService, IJobRunnerService jobRunnerService, 
                                MasterSyncController masterSyncController, IAsyncWorkerService asyncWorkerService, bool hasWriteAccess)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.asyncWorkerService = asyncWorkerService;
            this.masterSyncController = masterSyncController;
            this.messageService = messageService;
            this.invoiceService = invoiceService;
            this.notificationService = notificationService;
            this.jobRunnerService = jobRunnerService;

            this.HasWriteAccess = hasWriteAccess;

            Publications = new ObservableCollection<Publication>();
            SelectedInvoices = new ObservableCollection<Invoice>();
            SelectedPublication = Publication.GetNullPublication();
            processingInvoices = new List<Invoice>();

            InitialiseCommands();
        }

        #endregion

        #region Public Methods

        public override void RefreshPage()
        {
            invoiceService.ClearCache();
            GetPublicationsAsync();
        }

        public bool HasActiveLocks()
        {
            if (LoadedFirstTime)
            {
                return SelectedInvoices != null && SelectedInvoices.Count > 0; 
            }
            return false;
        }
      

        public void ClearLocks()
        {
            if(LoadedFirstTime)
                invoiceSync.UnlockAll();
        }

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Publication publication = sender as Publication;
            if (publication != null) SelectPublication(publication);

            Invoice invoice = sender as Invoice;
            if (invoice != null) SelectInvoice(invoice);


            base.DrillDown(sender);
        }

        #endregion
     
    }
}