﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Receipt;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;
using UBM.NotificationClient;
using UBM.Utilities.HtmlReportGenerator;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public partial class ReceiptViewModel : GenericPageableViewModel<Receipt>, ISearchable
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.PrintReceipts,
            DisplayName = "Print Receipts",
            ObjectDisplayName = "Receipt",
            FiltersVisible = true,
            PropertySearch = true,
            DateSearch = false,
            SearchPropertiesList = new List<ComboItem<SearchBy>>
                                {
                                    new ComboItem<SearchBy>{ Name = SearchFieldNames.CUSTOMER_NAME, Value = SearchBy.CustomerName },
                                    new ComboItem<SearchBy>{ Name = SearchFieldNames.URN, Value = SearchBy.URN },
                                    new ComboItem<SearchBy>{ Name = SearchFieldNames.ORDER_NUMBER, Value = SearchBy.OrderNumber },
                                    new ComboItem<SearchBy>{ Name = SearchFieldNames.RECEIPT_NUMBER, Value = SearchBy.ReceiptNumber },
                                    new ComboItem<SearchBy>{ Name = SearchFieldNames.POSTCODE, Value = SearchBy.Postcode }
                                },
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };

        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        public override void RefreshPage()
        {
            Batches = new ObservableCollection<Batch>(receiptService.GetBatches());
        }

        public override void Load()
        {
            if(!LoadedFirstTime)
            {
                asyncWorkerService.RunAsyncUserOperation(LoadAsync);                
            }
        }

        private void LoadAsync()
        {
            ObservableCollection<Batch> asyncBatches = new ObservableCollection<Batch>(receiptService.GetBatches());

            SendOrPostCallback callback = delegate
                                              {
                                                  Batches = asyncBatches;
                                                  RefreshPage();
                                              };

            asyncWorkerService.Post(callback);

            base.Load();
        }

        #endregion

        #region Fields

        private readonly IReceiptService receiptService;
        private readonly INotificationService notificationService;
        private readonly IMessageService messageService;
        private readonly IJobRunnerService jobRunnerService;
        private readonly IAsyncWorkerService asyncWorkerService;

        private HtmlReport report;

        #endregion

        #region Properties

        private ObservableCollection<Batch> batches;
        public ObservableCollection<Batch> Batches
        {
            get { return this.batches; }
            set
            {
                if (value == this.batches) return;
                this.batches = value;
                OnPropertyChanged("Batches");
            }
        }

        private Receipt selectedReceipt;
        public Receipt SelectedReceipt
        {
            get { return this.selectedReceipt; }
            set
            {
                if (value == this.selectedReceipt) return;
                this.selectedReceipt = value;
                OnPropertyChanged("SelectedReceipt");
            }
        }

        private bool searched;
        public bool Searched
        {
            get { return this.searched; }
            set
            {
                if (value == this.searched) return;
                this.searched = value;
                OnPropertyChanged("Searched");
            }
        }
        
        #endregion

        #region Private Methods

        private void SendBatchesToJde()
        {            
            jobRunnerService.RunJobs(MiddlewareJobNames.GetAsopJobNamesOfType(MiddlewareJobType.ReceiptsRevenue));
        }

        private IEnumerable<Batch> GetSelectedBatches()
        {
            return Batches.Where(c => c.IsSelected);
        }

        private void AllocatedReceiptNumbers(IEnumerable<Batch> batchesToAllocate)
        {
            foreach (Batch batch in batchesToAllocate)
            {
                bool success = receiptService.PrepareBatchToSendToJde(batch);

                if (!success)
                {
                    messageService.SendUserMessage("Batch: " + batch.ID + " System: " + batch.SystemName + " has not been allocated a receipt number", MessageType.Bad);
                }
            }
        }

        private IEnumerable<Batch> CheckBatchesWereSentToJde(IEnumerable<Batch> batchesToCheck)
        {
            foreach (Batch batch in batchesToCheck)
            {
                bool success = receiptService.ReceiptsSentToJde(batch);

                if (!success)
                {
                    Log.Warning("Batch: " + batch.ID + " System: " + batch.SystemName + " was not sent to JDE");
                    messageService.SendUserMessage("Batch: " + batch.ID + " System: " + batch.SystemName + " was not sent to JDE", MessageType.Bad);
                }
                else
                {
                    messageService.SendUserMessage("Batch: " + batch.ID + " System: " + batch.SystemName + " was sent to JDE", MessageType.Good);
                    yield return batch;
                }
            }
        }

        private void SendToJdeAndGenerateReceipts()
        {
            IEnumerable<Batch> selectedBatches = GetSelectedBatches();

            AllocatedReceiptNumbers(selectedBatches);

            jobRunnerService.JobComplete += CheckReceiptsWereSent;

            // Fires a job asynch
            SendBatchesToJde();


        }

        private void CheckReceiptsWereSent(object sender, EventArgs e)
        {
            IEnumerable<Batch> selectedBatches = GetSelectedBatches();
            IEnumerable<Batch> successfulBatches = CheckBatchesWereSentToJde(selectedBatches);

            if (successfulBatches.Count() > 0)
            {
                messageService.SendUserMessage(successfulBatches.Count() + "/" + selectedBatches.Count()
                    + " Batches Exported Successfully. Mouse over for details.", MessageType.Info);
            }
            else
            {
                messageService.SendUserMessage("No batches were processed", MessageType.Bad);
            }

            RefreshPage();

            jobRunnerService.JobComplete -= CheckReceiptsWereSent;
        }

        private void RunJobs(IEnumerable<string> jobNames, string reportName)
        {
            jobRunnerService.JobComplete += JobComplete;
            jobRunnerService.JobProgress += JobProgress;

            //messageService.SendUserMessage("Credit card orders extraction started", MessageType.Info);
            report = new HtmlReport();
            report.AddHeader(reportName);

            jobRunnerService.RunJobs(jobNames);
        }

        private void JobComplete(object sender, EventArgs e)
        {
            jobRunnerService.JobComplete -= JobComplete;
            jobRunnerService.JobProgress -= JobProgress;
            
            notificationService.SendEmail(new EmailMessageWrapper
            {
                From = "Middleware Client",
                To = Environment.UserName,
                Subject = "Middleware Job Report",
                Body = report.ToString(),
                Priority = Priority.Normal,
                IsHtml = true
            });

            RefreshPage();
        }

        private void JobProgress(object sender, JobProgressEventHandlerArgs args)
        {
            if (args.StepDetail.Contains("Complete"))
                report.AddParagraph(args.JobName + " - " + args.StepDetail);

            if (args.StepDetail.Contains("Failed"))
                report.AddParagraph(args.JobName + " - " + args.StepDetail, TextStyleName.Failure);

        }

        #endregion

        #region Constructor

        public ReceiptViewModel(IReceiptService receiptService, 
                                INotificationService notificationService, 
                                IMessageService messageService, 
                                IJobRunnerService jobRunnerService, 
                                IAsyncWorkerService asyncWorkerService,
                                bool hasWriteAccess)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.asyncWorkerService = asyncWorkerService;
            this.receiptService = receiptService;
            this.receiptService.OnDrillDown += DrillDown;
            this.notificationService = notificationService;
            this.jobRunnerService = jobRunnerService;
            this.messageService = messageService;

            this.HasWriteAccess = hasWriteAccess;

            Batches = new ObservableCollection<Batch>();

            InitialiseCommands();


        }

        #endregion

        #region ISearchable

        public void SearchAsync(SearchInfo searchInfo)
        {
            asyncWorkerService.RunAsyncUserOperation(() => Search(searchInfo), "Receipt search");            
        }

        public  int Search(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            List<Receipt> receipts = receiptService.SearchReceipts(searchInfo);

            SendOrPostCallback callback = delegate
            {
                BeginPaging(receipts);
            };
            asyncWorkerService.Post(callback);

            Searched = true;

            return receipts.Count;
        }


        #endregion

        #region IDrillDown

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if(SelectedReceipt != null) SelectedReceipt.IsClicked = false;
            SelectedReceipt = (Receipt) sender;

            SelectedReceipt.Products = LineDetail.CreateLineDetails(SelectedReceipt.ReceiptNumber, SelectedReceipt.SystemName);

            base.DrillDown(sender);
        }

        #endregion

        #region IDisposable

        protected override void OnDispose()
        {
            this.receiptService.OnDrillDown -= DrillDown;
            base.OnDispose();
        }

        #endregion
    }
}
