﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Receipt;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public partial class ReceiptViewModel : GenericPageableViewModel<Receipt>, IReceiptViewModel
    {
        public ICommand ProcessSelectedBatchesCommand { get; private set; }
        public ICommand PrintSelectedReceiptsCommand { get; private set; }
        public ICommand ExtractionCommand { get; private set; }

        private void InitialiseCommands()
        {
            ProcessSelectedBatchesCommand = new DelegateCommand(ProcessSelectedBatches, CanProcessSelectedBatches);
            PrintSelectedReceiptsCommand = new DelegateCommand(PrintSelectedReceipts, CanPrintSelectedReceipts);
            ExtractionCommand = new DelegateCommand(RunExtraction, CanRunExtraction);
        }

        private void ProcessSelectedBatches(object state)
        {
            messageService.SendUserMessage("Processing receipts...", MessageType.Info);
            asyncWorkerService.RunAsyncUserOperation(SendToJdeAndGenerateReceipts);           
        }
        private bool CanProcessSelectedBatches(object state)
        {
            return LoadedFirstTime && HasWriteAccess && Batches.Where(c => c.IsSelected).Count() > 0;
        }
        
        private void PrintSelectedReceipts(object state)
        {
            foreach (Receipt receipt in FullModelCollection)
            {
                if(receipt.IsSelected)
                {
                    receiptService.CreateReceiptCopy(receipt);
                }
            }

            // @DG Changed so that revenue jobs don'trun for receipt copies.
            List<string> jobs = new List<string>(); 
            //= MiddlewareJobNames.GetAsopJobNamesOfType(MiddlewareJobType.ReceiptsRevenue);
            jobs.Add(MiddlewareJobNames.RECEIPT_COPIES);

            RunJobs(jobs, "Middleware Receipt Copy Report");

        }
        private bool CanPrintSelectedReceipts(object state)
        {
            return HasWriteAccess && FullModelCollection.Where(c => c.IsSelected).Count() > 0;
        }

        private void RunExtraction(object state)
        {
            RunJobs(MiddlewareJobNames.GetAsopJobNamesOfType(MiddlewareJobType.ReceiptsCreditCard), "Middleware Credit Card Extraction Report");        
        }
        private bool CanRunExtraction(object state)
        {
            return HasWriteAccess && !jobRunnerService.Running;
        }
    
        
     
    }
}
