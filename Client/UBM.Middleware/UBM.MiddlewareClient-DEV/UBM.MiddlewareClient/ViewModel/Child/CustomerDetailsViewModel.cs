﻿using System;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Child
{
    public class CustomerDetailsViewModel : ViewModelBase, ICustomerDetailsViewModel
    {

        private Customer selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return this.selectedCustomer; }
            set
            {
                if (value == this.selectedCustomer) return;
                this.selectedCustomer = value;
                OnPropertyChanged("SelectedCustomer");
            }
        }

        private readonly bool resendEnabled;
        private readonly bool mergeEnabled;

        public ICommand CloseCustomerDetailsCommand { get; private set; }
        public ICommand PrepareResendEmailCommand { get; private set; }
        public ICommand MergeCustomerCommand { get; private set; }

        public event EventHandler CustomerDetailsClosed;
        public event EventHandler ResendEmail;
        public event EventHandler MergeDedupeCustomer;

        private readonly IMessageService messageService;

        public CustomerDetailsViewModel(IMessageService messageService, bool resendEnabled = true, bool mergeEnabled = true)
        {
            this.messageService = messageService;
            this.resendEnabled = resendEnabled;
            this.mergeEnabled = mergeEnabled;

            SelectedCustomer = Customer.CreateNullCustomer();

            CloseCustomerDetailsCommand = new DelegateCommand(CloseCustomerDetails, CanCloseCustomerDetails);
            PrepareResendEmailCommand = new DelegateCommand(PrepareResendEmail, CanPrepareResendEmail);
            MergeCustomerCommand = new DelegateCommand(MergeCustomer, CanMergeCustomer);
        }

        private void OnCustomerDetailsClosed()
        {
            if(CustomerDetailsClosed != null)
            {
                CustomerDetailsClosed(this, EventArgs.Empty);
            }
        }

        private void OnResendEmail(object state)
        {
            if(ResendEmail != null)
            {
                ResendEmail(state, EventArgs.Empty);
            }
        }

        private void OnMergeDedupeCustomer(object state)
        {
            if(MergeDedupeCustomer != null)
            {
                MergeDedupeCustomer(state, EventArgs.Empty);
            }
        }

        private void CloseCustomerDetails(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            SelectedCustomer.IsClicked = false;
            messageService.CloseDialog(DialogType.CustomerDetails);
            OnCustomerDetailsClosed();
        }
        private bool CanCloseCustomerDetails(object state)
        {
            return !messageService.AnyDialogsOpenExcept(DialogType.CustomerDetails);
        }

        private void PrepareResendEmail(object email)
        {
            OnResendEmail(email);
        }

        private bool CanPrepareResendEmail(object state)
        {
            return resendEnabled;
        }

        private void MergeCustomer(object duplicate)
        {
            OnMergeDedupeCustomer(duplicate);
        }
        private bool CanMergeCustomer(object state)
        {
            return mergeEnabled && !IsCustomerLockedByAnotherUser(SelectedCustomer) && SelectedCustomer.Scrutiny.InScrutiny;
        }
        private static bool IsCustomerLockedByAnotherUser(Customer customer)
        {
            return customer.IsLocked && customer.LockedBy != Environment.UserName;
        }


        public void ShowFullBody(object row)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email rejection = (Email)row;
            rejection.ShowFullBody();
        }
        public void ShowTruncatedBody(object row)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email rejection = (Email)row;
            rejection.Body = rejection.TruncatedBody;
        }
    }
}
