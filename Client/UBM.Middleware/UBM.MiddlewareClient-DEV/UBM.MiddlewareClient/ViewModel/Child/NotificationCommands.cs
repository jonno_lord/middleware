﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Child
{
    public partial class NotificationViewModel : ViewModelBase, INotificationDialogViewModel
    {

        public ICommand AcceptNotificationDialogCommand { get; private set; }
        public ICommand CancelNotificationDialogCommand { get; private set; }
        public ICommand NextNotificationCommand { get; private set; }
        public ICommand PreviousNotificationCommand { get; private set; }
        public ICommand CCAddCommand { get; private set; }
        public ICommand CCRemoveCommand { get; private set; }
        public ICommand CCCheckCommand { get; private set; }
        public ICommand RecipientCheckCommand { get; private set; }

        private void InitialiseCommands()
        {
            AcceptNotificationDialogCommand = new DelegateCommand(AcceptNotificationDialog, CanAcceptNotificationDialog);
            CancelNotificationDialogCommand = new DelegateCommand(CancelNotificationDialog);
            NextNotificationCommand = new DelegateCommand(MoveToNextEmail, CanMoveToNextEmail);
            PreviousNotificationCommand = new DelegateCommand(MoveToPreviousEmail, CanMoveToPreviousEmail);
            CCAddCommand = new DelegateCommand(AddCC, CanAddCC);
            CCRemoveCommand = new DelegateCommand(RemoveCC, CanRemoveCC);
            CCCheckCommand = new DelegateCommand(CheckCC, CanCheckCC);
            RecipientCheckCommand = new DelegateCommand(CheckRecipient, CanCheckRecipient);
        }

        private void AcceptNotificationDialog(object state)
        {
            if (CheckAllRecipientsAreValid())
            {
                OnAcceptNotification(this, EventArgs.Empty);
                messageService.CloseDialog(DialogType.RejectNotification);
            }
        }

        private bool CanAcceptNotificationDialog(object state)
        {
            return true; //!CanMoveToNextEmail(state);
        }

        private void CancelNotificationDialog(object state)
        {
            OnCancelNotification(this, EventArgs.Empty);
            messageService.CloseDialog(DialogType.RejectNotification);
        }

        #region Commands

        private void MoveToNextEmail(object state)
        {
            if(CheckAllRecipientsAreValid())
            {
                SelectNotificationCustomer(customers[++NotificationIndex]);
            }
        }
        private bool CanMoveToNextEmail(object state)
        {
            return NotificationIndex != customers.Count - 1;
        }

        private void MoveToPreviousEmail(object state)
        {
            if (CheckAllRecipientsAreValid())
            {
                SelectNotificationCustomer(customers[--NotificationIndex]);   
            }
                
        }
        private bool CanMoveToPreviousEmail(object state)
        {
            return NotificationIndex != 0;
        }

        private void AddCC(object state)
        {
            if (!string.IsNullOrEmpty(SelectedCustomer.Email.CC.Name))
            {
                CheckEmail(SelectedCustomer.Email.CC.Name);
                SelectedCustomer.Email.CC.CCList.Add(SelectedCustomer.Email.CC.Name);
                SelectedCustomer.Email.CC.Name = "";
            }
        }
        private bool CanAddCC(object state)
        {
            return !string.IsNullOrEmpty(SelectedCustomer.Email.CC.Name);
        }


        private void RemoveCC(object state)
        {
            if (SelectedCustomer.Email.CC.CCList.Count != 0)
                SelectedCustomer.Email.CC.CCList.Remove(SelectedCustomer.Email.CC.SelectedCC);
        }
        private bool CanRemoveCC(object state)
        {
            return SelectedCustomer.Email.CC.CCList.Count() > 0;
        }

        private void CheckCC(object state)
        {
            if (!string.IsNullOrEmpty(SelectedCustomer.Email.CC.Name))
            {
                CheckEmail(SelectedCustomer.Email.CC.Name);
            }
        }
        private bool CanCheckCC(object state)
        {
            return SelectedCustomer != null ? !string.IsNullOrEmpty(SelectedCustomer.Email.CC.Name) : false;
        }

        private void CheckRecipient(object state)
        {
            List<string> addresses = notificationService.ResolveEmail(SelectedCustomer.Email.To);

            if (addresses.Count == 1)
            {
                messageService.SendUserMessage("Recipient: " + SelectedCustomer.Email.To + " is valid - " + addresses[0], MessageType.Good);
            }
            else
            {
                messageService.SendUserMessage("Recipient: " + SelectedCustomer.Email.To + " is NOT valid", MessageType.Bad);
            }
        }
        private bool CanCheckRecipient(object state)
        {
            return !string.IsNullOrEmpty(SelectedCustomer.Email.To);
        }

        #endregion



    }
}
