﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;
using UBM.NotificationClient;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Controllers
{
    /// <summary>
    ///   This class controls read/write access to various parts of the Client and controls which types of customer are visible to the user.
    /// </summary>
    public class AccessController
    {
        #region Fields

        private readonly IUserService userService;

        #endregion

        #region Properties

        ///<summary>
        /// Collection of pages and read/write access for user
        ///</summary>
        public List<PageAccess> PageAccessRights { get; set; }

        /// <summary>
        /// List of Customer Search Types user is allowed to see
        /// </summary>
        public List<string> SearchTypes { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///   Creates an instance of Access Controller
        /// </summary>
        /// <param name = "userService"></param>
        public AccessController(IUserService userService)
        {
            TraceHelper.Trace(TraceType.Controller);
            this.userService = userService;
        }

        public void LoadPermissions()
        {
            TraceHelper.Trace(TraceType.Controller);
            SearchTypes = userService.GetSearchTypes();
            PageAccessRights = userService.GetPageAccessRights();
        }

        public bool HasWriteAccess(ClientPageNumber pageNumber)
        {
            if(PageAccessRights != null)
            {
                return PageAccessRights.Exists(c => c.ClientPageNumber == pageNumber) &&
                       PageAccessRights[(int) pageNumber].CanWrite;
            }
            return false;
        }
        
        #endregion
    }
}