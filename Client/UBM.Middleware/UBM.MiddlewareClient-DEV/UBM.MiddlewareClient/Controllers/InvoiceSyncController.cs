﻿using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Invoicing;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Controllers
{

    internal class InvoiceSyncController : GenericSyncController<Invoice>
    {

        public InvoiceSyncController(MasterSyncController masterSyncController, IEnumerable<Invoice> invoices)
            : base(masterSyncController, invoices)
        {
            MasterSyncController.OnSynchroniseNotification += base.Synchronise;            
            TraceHelper.Trace(TraceType.Controller);
        }

        protected override void OnDispose()
        {
            MasterSyncController.OnSynchroniseNotification -= base.Synchronise;  
        }
    }
}
