﻿using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Controllers
{
    /// <summary>
    /// Paging manager for controlling large collections
    /// </summary>
    public class PageController
    {

        #region Fields

        #region Delegates

        ///<summary>
        /// Delegate to allow custom GetPage method
        ///</summary>
        public delegate void GetPageDelegate();

        #endregion

        private readonly int collectionNumberOfItems;
        private readonly GetPageDelegate getPage;
        private readonly int maxItemsOnPage;
        private int currentPageNumber;
        private int numberOfPages;

        #endregion

        public int NumberOfItemsInCollection
        {
            get
            {
                return collectionNumberOfItems;
            }
        }
        public int NumberOfPages
        {
            get
            {
                if (collectionNumberOfItems % maxItemsOnPage == 0)
                {
                    numberOfPages = collectionNumberOfItems / maxItemsOnPage;
                }
                else
                {
                    numberOfPages = (collectionNumberOfItems / maxItemsOnPage) + 1;
                }

                return numberOfPages;
            }
        }

        public int MaxItemsOnPage
        {
            get { return maxItemsOnPage; }
        }
        public int StartIndex
        {
            get
            {
                return currentPageNumber * maxItemsOnPage;
            }
        }
        public int EndIndex
        {
            get
            {
                int end = StartIndex + maxItemsOnPage;
                if (end >= collectionNumberOfItems) end = collectionNumberOfItems;

                return end;
            }
        }

        #region Constructor

        ///<summary>
        /// Creates new instance of page controller
        ///</summary>
        ///<param name="currentPageNumber">Page to start on</param>
        ///<param name="collectionNumberOfItems">Number of items in the collection</param>
        ///<param name="maxItemsOnPage">Maximum items per page</param>
        ///<param name="getPage">Method to execute when getting a page</param>
        public PageController(int currentPageNumber, int collectionNumberOfItems, int maxItemsOnPage,
                              GetPageDelegate getPage)
        {
            TraceHelper.Trace(TraceType.Controller);
            this.currentPageNumber = currentPageNumber;
            this.collectionNumberOfItems = collectionNumberOfItems;
            this.maxItemsOnPage = maxItemsOnPage;
            this.getPage = getPage;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Factory method. Returns PageController object
        /// </summary>
        /// <param name = "currentPageNumber">Page to start on</param>
        /// <param name = "collectionNumberOfItems">Number of items to be paged</param>
        /// <param name = "maxItemsOnPage">Number of items to a page</param>
        /// <param name = "getPage">Method to execute when getting a page</param>
        /// <returns></returns>
        public static PageController CreatePageController(int currentPageNumber, int collectionNumberOfItems,
                                                          int maxItemsOnPage, GetPageDelegate getPage)
        {
            TraceHelper.Trace(TraceType.Controller);
            return new PageController(currentPageNumber, collectionNumberOfItems, maxItemsOnPage, getPage);
        }





        /// <summary>
        ///   Calculates the maximum page for a given collection size
        /// </summary>
        /// <param name = "count"></param>
        /// <returns></returns>
        public int GetMaxPageForSetItemCount(int count)
        {
            TraceHelper.Trace(TraceType.Controller);
            if (collectionNumberOfItems % maxItemsOnPage == 0)
            {
                return count / (maxItemsOnPage - 1);
            }
            return count / maxItemsOnPage;
        }



        /// <summary>
        ///   Sets the page
        /// </summary>
        /// <param name = "pageNumber"></param>
        /// <returns></returns>
        public int SetCurrentPageAndValidate(int pageNumber)
        {
            TraceHelper.Trace(TraceType.Controller);
            currentPageNumber = pageNumber;

            if (numberOfPages == 0)
            {
                currentPageNumber = 0;
            }
            else if (pageNumber >= 0 && pageNumber < numberOfPages)
            {
                currentPageNumber = pageNumber;
            }
            else if (pageNumber > numberOfPages)
            {
                currentPageNumber = numberOfPages - 1;
            }
            else
            {
                currentPageNumber = 0;
            }

            return currentPageNumber;
        }

        /// <summary>
        ///   Goes to the next page
        /// </summary>
        /// <returns></returns>
        public int NextPage()
        {
            TraceHelper.Trace(TraceType.Controller);
            ++currentPageNumber;
            if (getPage != null) getPage();
            return currentPageNumber;
        }

        /// <summary>
        ///   Goes to the previous page
        /// </summary>
        /// <returns></returns>
        public int PreviousPage()
        {
            TraceHelper.Trace(TraceType.Controller);
            --currentPageNumber;
            if (getPage != null) getPage();
            return currentPageNumber;
        }

        #endregion
    }
}