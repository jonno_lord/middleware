﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.Controllers
{

    internal class DedupeSyncController : GenericSyncController<Customer>
    {
        private readonly DedupeViewModel dedupeViewModel;

        public DedupeSyncController(MasterSyncController masterSyncController,  IEnumerable<Customer> customers, DedupeViewModel dedupeViewModel)
            : base(masterSyncController, customers)
        {
            TraceHelper.Trace(TraceType.Controller);

            this.dedupeViewModel = dedupeViewModel;
            MasterSyncController.OnSynchroniseNotification += Synchronise;

            MasterSyncController.OnRefreshFromDatabaseNotification += RefreshFromDatabase;
        }

        
        protected override void Synchronise()
        {
            TraceHelper.Trace(TraceType.Controller);
            base.Synchronise();
            foreach (Customer customer in SyncCollection)
            {
                if (customer.Queue == QueueType.Accepted || customer.Queue == QueueType.Rejected)
                {
                    if (!customer.IsLocked) Lock(customer);
                }
            }
        }

        private void RefreshFromDatabase()
        {
            TraceHelper.Trace(TraceType.Controller);
            if (IgnoreNextRefresh)
            {
                IgnoreNextRefresh = false;
            }
            else
            {
                dedupeViewModel.RefreshPage();
            }
        }

        protected override void OnDispose()
        {
            MasterSyncController.OnSynchroniseNotification -= Synchronise;
            MasterSyncController.OnRefreshFromDatabaseNotification -= RefreshFromDatabase;
        }
    }
}
