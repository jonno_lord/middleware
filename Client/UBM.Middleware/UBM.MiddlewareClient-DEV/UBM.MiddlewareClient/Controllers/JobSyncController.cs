﻿using System;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Services;

namespace UBM.MiddlewareClient.Controllers
{
    internal class JobSyncController : GenericSyncController<DBNull>
    {

        private readonly JobRunnerService jobRunnerService;

        public JobSyncController(MasterSyncController masterSyncController, JobRunnerService jobRunnerService)
            : base(masterSyncController, null)
        {
            TraceHelper.Trace(TraceType.Controller);
            MasterSyncController.OnProgressReportNotification += ProgressReportNotification;
            this.jobRunnerService = jobRunnerService;
        }

        private void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
            TraceHelper.Trace(TraceType.Controller);
            jobRunnerService.IncomingProgressReport(jobName, stepDetail, percentageComplete);
        }

        protected override void OnDispose()
        {
            MasterSyncController.OnProgressReportNotification -= ProgressReportNotification;
        }
    }
}
