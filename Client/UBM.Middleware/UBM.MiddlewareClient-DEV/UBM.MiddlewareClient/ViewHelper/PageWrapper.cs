﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.Enums;

namespace UBM.MiddlewareClient.ViewHelper
{
    public class PageWrapper
    {
        public string Name { get; set; }

        public Page Page { get; set; }

        public object ViewModel { get; set; }

        public Type ViewModelType { get; set; }

        public ClientPageNumber ClientPageNumber { get; set; }
    }
}

