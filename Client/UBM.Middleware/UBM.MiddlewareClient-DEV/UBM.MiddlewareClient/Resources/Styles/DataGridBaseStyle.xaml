﻿<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:l="clr-namespace:UBM.MiddlewareClient">

    <!-- DataGridBaseStyle class that caters for general DataGrids which can 
         then be based on for a more specialist DataGrid.  -->

    <!-- DataGridColumnHeadersPresenter style that creates the look and feel for the column headers. -->
    <Style x:Key="DataGridColumnHeaderPresenterBaseStyle" TargetType="{x:Type DataGridColumnHeadersPresenter}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGridColumnHeadersPresenter}">
                    <Border Background="Transparent"
                            CornerRadius="14,14,0,0">
                        <Grid>
                            <!-- Need ItemsPresenter to ensure that the the different headers can be displayed. -->
                            <ItemsPresenter />
                        </Grid>
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- Style for the resize grippers to allow the user to resize the columns on the DataGrid. -->
    <Style x:Key="ColumnHeaderRightGripperStyle" TargetType="{x:Type Thumb}">
        <Setter Property="Width" Value="2" />
        <Setter Property="Background" Value="Transparent" />
        <Setter Property="Cursor" Value="SizeWE" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type Thumb}">
                    <Border Name="Border">
                        <Canvas>
                            <Line RenderOptions.EdgeMode="Aliased" Stroke="Transparent"
                                  X1="0" Y1="{Binding ElementName=Border, Path=ActualHeight}"
	                              X2="0" Y2="0" />
                        </Canvas>
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
    <Style x:Key="ColumnHeaderLeftGripperStyle"
	       BasedOn="{StaticResource ColumnHeaderRightGripperStyle}"
	       TargetType="{x:Type Thumb}">
        <Setter Property="Width" Value="2" />
        <Setter Property="Background" Value="Transparent" />
        <Setter Property="Cursor" Value="SizeWE" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type Thumb}">
                    <Border Name="Border">
                        <Canvas>
                            <Line RenderOptions.EdgeMode="Aliased" Stroke="Transparent"
                                  X1="0" Y1="{Binding ElementName=Border, Path=ActualHeight}"
	                              X2="0" Y2="0" />
                        </Canvas>
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- DataGridColumnHeader style that creates the look and feel for each individual column header. 
         Has to be placed after the gripper styles to ensure that it can find them, as they are compiled before. -->
    <Style x:Key="{x:Type DataGridColumnHeader}" TargetType="{x:Type DataGridColumnHeader}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="VerticalContentAlignment" Value="Center" />
        <Setter Property="FontFamily" Value="Calibri" />
        <Setter Property="FontSize" Value="10pt" />
        <Setter Property="FontWeight" Value="Bold" />
        <Setter Property="Foreground" Value="Black" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGridColumnHeader}">
                    <Grid>
                        <Border Name="HeaderBorder"
                                Background="{StaticResource StandardButtonBackground}"
                                CornerRadius="12,12,0,0" 
                                BorderBrush="{StaticResource StandardBorderBrush}"
                                BorderThickness="0,1,1,0">
                            <Grid>
                                <Grid.RowDefinitions>
                                    <RowDefinition Height="*" />
                                    <RowDefinition Height="2*" />
                                </Grid.RowDefinitions>

                                <Border Grid.Row="0" 
                                        CornerRadius="12,12,2,2" 
                                        Background="{StaticResource ButtonInsideBorderBrush}" />

                                <Path Name="UpArrow" 
                                      Grid.Row="0"
                                      Fill="{StaticResource GlyphBrush}"
                                      HorizontalAlignment="Center" 
                                      VerticalAlignment="Center"
                                      Margin="0,2,0,0" 
                                      Visibility="Hidden" 
                                      Opacity=".6">
                                    <Path.Data>
                                        <PathGeometry>
                                            <PathFigure StartPoint="0,0">
                                                <LineSegment Point="7,0" />
                                                <LineSegment Point="3.5,6" />
                                                <LineSegment Point="0,0" />
                                            </PathFigure>
                                        </PathGeometry>
                                    </Path.Data>
                                </Path>

                                <Path Name="DownArrow" 
                                      Grid.Row="0"
                                      Fill="{StaticResource GlyphBrush}"
                                      HorizontalAlignment="Center" 
                                      VerticalAlignment="Center" 
                                      Margin="0,2,0,0" 
                                      Visibility="Hidden" 
                                      Opacity=".6">
                                    <Path.Data>
                                        <PathGeometry>
                                            <PathFigure StartPoint="0,6">
                                                <LineSegment Point="7,6" />
                                                <LineSegment Point="3.5,0" />
                                                <LineSegment Point="0,6" />
                                            </PathFigure>
                                        </PathGeometry>
                                    </Path.Data>
                                </Path>

                                <ContentPresenter Grid.Row="1"
                                                  Opacity=".7"
                                                  Margin="5,0,5,0"
                                                  HorizontalAlignment="Center"
                                                  VerticalAlignment="Top"
                                                  Content="{TemplateBinding Content}"
                                                  ContentStringFormat="{TemplateBinding ContentStringFormat}"
                                                  ContentTemplate="{TemplateBinding ContentTemplate}" />
                            </Grid>
                        </Border>
                        <Thumb x:Name="PART_LeftHeaderGripper"
                               HorizontalAlignment="Left"
                               Style="{StaticResource ColumnHeaderLeftGripperStyle}" />

                        <Thumb x:Name="PART_RightHeaderGripper"
                               HorizontalAlignment="Right"
                               Style="{StaticResource ColumnHeaderRightGripperStyle}" />
                    </Grid>
                    <ControlTemplate.Triggers>
                        <Trigger Property="SortDirection" Value="Descending">
                            <Setter TargetName="UpArrow" Property="Visibility" Value="Hidden" />
                            <Setter TargetName="DownArrow" Property="Visibility" Value="Visible" />
                        </Trigger>
                        <Trigger Property="SortDirection" Value="Ascending">
                            <Setter TargetName="DownArrow" Property="Visibility" Value="Hidden" />
                            <Setter TargetName="UpArrow" Property="Visibility" Value="Visible" />
                        </Trigger>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="HeaderBorder" Property="Background" Value="{StaticResource StandardIsMouseOverBrush}" />
                        </Trigger>
                        <Trigger Property="IsKeyboardFocused" Value="True">
                            <Setter TargetName="HeaderBorder" Property="Background" Value="{StaticResource StandardIsMouseOverBrush}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="HeaderBorder" Property="Background" Value="{StaticResource DisabledBackgroundGradient}" />
                            <Setter TargetName="HeaderBorder" Property="BorderBrush" Value="{StaticResource DisabledBorderBrush}" />
                            <Setter Property="Foreground" Value="{StaticResource DisabledForegroundBrush}"/>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- DataGridRowHeader style that give the base DataGrid the look and feel with a small gap of 10 pixels.
         That is set by the width property of RowHeaderBorder, this will change for the more specialist DataGrids
         like CustomerDedupe that has more in the row header. -->
    <Style x:Key="DataGridRowHeaderBaseStyle" TargetType="{x:Type DataGridRowHeader}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="ClipToBounds" Value="True" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGridRowHeader}">
                    <Border Name="RowHeaderBorder" 
                            Width="0">
                        <ContentPresenter />                      
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- DataGridCellsPresenter style that is used to give the styles the look and feel. -->
    <Style x:Key="DataGridCellsPresenterBaseStyle" TargetType="{x:Type DataGridCellsPresenter}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="Background" Value="White" />
        <Setter Property="Foreground" Value="Black" />
        <Setter Property="FontFamily" Value="Calibri" />
        <Setter Property="FontSize" Value="10pt" />
        <Setter Property="FocusVisualStyle" Value="{x:Null}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGridCellsPresenter}">
                    <Border Name="CellBorder"
                            Background="Transparent"
                            CornerRadius="0"
                            BorderBrush="{StaticResource StandardBorderBrush}"
                            BorderThickness="1,0,1,0"
                            Margin="-2,0,-2,0">
                        <ItemsPresenter /> <!--VerticalAlignment="Center" />-->
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- DataGridRow style that is used to set up the row style by bringing 
         the row header and cells presenter together. -->
    <Style x:Key="DataGridRowBaseStyle" TargetType="{x:Type DataGridRow}">
        <!--<Setter Property="Height" Value="25" />-->
        <Setter Property="VerticalAlignment" Value="Top" />
        <Setter Property="Foreground" Value="Black" />
        <Setter Property="Background" Value="white" />
        <Setter Property="FontFamily" Value="Calibri" />
        <Setter Property="Opacity" Value=".9" />
        <Setter Property="BorderBrush" Value="{StaticResource StandardBorderBrush}" />
        <Setter Property="BorderThickness" Value="1" />
        <Setter Property="KeyboardNavigation.TabNavigation" Value="None" />
        <Setter Property="FontSize" Value="10pt" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGridRow}">
                    <Border x:Name="RowBorder"
                            Background="{TemplateBinding Background}"
                            BorderBrush="{TemplateBinding BorderBrush}"
                            BorderThickness="{TemplateBinding BorderThickness}">
                        <Grid>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>

                            <DataGridRowHeader Grid.Column="0" 
                                               VerticalContentAlignment="Center" 
                                               HorizontalContentAlignment="Center" 
                                               Style="{StaticResource DataGridRowHeaderBaseStyle}" />

                            <DataGridCellsPresenter Grid.Column="1"
                                                    Style="{StaticResource DataGridCellsPresenterBaseStyle}" />

                        </Grid>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="RowBorder" Property="Background" Value="{StaticResource StandardIsMouseOverBrush}" />
                        </Trigger>
                        <Trigger Property="IsKeyboardFocused" Value="True">
                            <Setter TargetName="RowBorder" Property="Background" Value="{StaticResource StandardIsMouseOverBrush}" />
                        </Trigger>
                        <Trigger Property="IsSelected" Value="True">
                            <Setter TargetName="RowBorder" Property="Background" Value="{StaticResource StandardIsMouseOverBrush}" />
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="RowBorder" Property="Background" Value="{StaticResource DisabledBackgroundGradient}" />
                            <Setter TargetName="RowBorder" Property="BorderBrush" Value="{StaticResource DisabledBorderBrush}" />
                            <Setter Property="Foreground" Value="{StaticResource DisabledForegroundBrush}"/>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- DataGridCell style that is used to set up the cell style and display its content in such away -->
    <Style x:Key="DataGridCellBaseStyle" TargetType="{x:Type DataGridCell}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="Background" Value="White" />
        <Setter Property="Foreground" Value="Black" />
        <Setter Property="FontFamily" Value="Calibri" />
        <Setter Property="FontSize" Value="10pt" />
        <Setter Property="FocusVisualStyle" Value="{x:Null}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGridCell}">
                    <Border Name="CellBorder"
                            Background="Transparent"
                            Padding="2"
                            BorderBrush="{StaticResource StandardBorderBrush}"
                            BorderThickness="1,0,1,0">
                        <ContentPresenter SnapsToDevicePixels="False" VerticalAlignment="Top" />
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- DataGrid style this is the base style that brings all the other styles together.
         As ItemsPresenter is used to display the rows the style for rows and cells is set
         the a setter just before the template, to ensure these styles are applied. -->
    <Style x:Key="DataGridBaseStyle" TargetType="{x:Type DataGrid}">
        <Setter Property="OverridesDefaultStyle" Value="True" />
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="FocusVisualStyle" Value="{x:Null}" />
        <Setter Property="AutoGenerateColumns" Value="True" />
        <Setter Property="GridLinesVisibility" Value="None" />
        <Setter Property="Cursor" Value="Hand" />
        <!-- Both the style for the rows and cells is set here. -->
        <Setter Property="RowStyle" Value="{StaticResource DataGridRowBaseStyle}" />
        <Setter Property="CellStyle" Value="{StaticResource DataGridCellBaseStyle}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type DataGrid}">
                    <l:ClippingBorder x:Name="GridBorder" 
                                      Background="White" 
                                      BorderBrush="{StaticResource StandardBorderBrush}"
                                      BorderThickness="2"
                                      CornerRadius="15">
                        <Grid>
                            <Border CornerRadius="15">
                                <Grid>
                                    <Grid.RowDefinitions>
                                        <RowDefinition Height="40" />
                                        <RowDefinition />
                                    </Grid.RowDefinitions>
                                    <Border Grid.Row="0" 
                                            Background="{StaticResource StandardBackgroundGradient}" 
                                            CornerRadius="14,14,0,0">
                                        <StackPanel Orientation="Horizontal">
                                            <!-- Need this presenter to show the column headers in the DataGrid. -->
                                            <DataGridColumnHeadersPresenter Margin="0,0,0,0" />
                                        </StackPanel>
                                    </Border>
                                    <Border Grid.Row="1"
                                            CornerRadius="0,0,15,15"
                                            Margin="0,-1,0,0">
                                        <ScrollViewer HorizontalScrollBarVisibility="Visible" VerticalScrollBarVisibility="Visible">
                                            <ItemsPresenter />
                                        </ScrollViewer>
                                    </Border>
                                </Grid>
                            </Border>
                        </Grid>
                    </l:ClippingBorder>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>


    <Style x:Key="DataGridColumnHeaderTextStyle" TargetType="{x:Type TextBlock}">
        <Setter Property="FontFamily" Value="Calibri" />
        <Setter Property="FontSize" Value="10pt" />
        <Setter Property="FontWeight" Value="Bold" />
        <Setter Property="Foreground" Value="Black" />
    </Style>

</ResourceDictionary>