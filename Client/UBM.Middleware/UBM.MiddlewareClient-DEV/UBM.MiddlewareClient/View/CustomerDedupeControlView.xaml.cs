﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for CustomerDedupeControlView.xaml
    /// </summary>
    public partial class CustomerDedupeControlView : UserControl
    {
        #region Public Properties.

        public DedupeViewModel DVM { get; set; }

        #endregion

    
        #region Constructors.

        public CustomerDedupeControlView()
        {
            InitializeComponent();            
        }

        #endregion
    }
}