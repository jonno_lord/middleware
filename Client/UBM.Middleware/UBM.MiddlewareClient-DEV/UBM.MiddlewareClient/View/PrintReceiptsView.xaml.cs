﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for PrintReceiptsView.xaml
    /// </summary>
    public partial class PrintReceiptsView : UserControl
    {
        #region Private Fields.

        private ReceiptViewModel rvm;

        #endregion

        //#region Private Control Events.

        //private void TextBoxKeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Enter)
        //    {
        //        int pagenum;
        //        if (int.TryParse(txtPage.Text, out pagenum) == false)
        //        {
        //            rvm.SendUserMessage("Invalid Page Number", MessageType.Bad);
        //        }
        //        else
        //        {
        //            rvm.GoToPage(pagenum);
        //        }
        //    }
        //}

        //#endregion

        #region Constructors. 

        public PrintReceiptsView()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods.

        /// <summary>
        /// Method that sets the ReceiptViewModel to the current DataContext 
        /// of the current view. 
        /// </summary>
        /// <param name="rvm">Sets the ViewModel of this view.</param>
        public void SetViewModel(ReceiptViewModel rvm)
        {
            this.rvm = rvm;
        }

        #endregion
    }
}
