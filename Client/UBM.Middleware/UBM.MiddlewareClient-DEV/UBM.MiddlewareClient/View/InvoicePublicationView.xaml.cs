﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for InvoicePublicationView.xaml
    /// </summary>
    public partial class InvoicePublicationView : UserControl
    {
        #region Public Properties.

        public InvoiceViewModel IVM { get; set; }

        #endregion

        #region Constructors.

        public InvoicePublicationView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
