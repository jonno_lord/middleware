﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for OrderViewerView.xaml
    /// </summary>
    public partial class OrderViewerView : UserControl
    {
        #region Public Properties.

        public OrderViewModel OVM { get; set; }

        #endregion

        #region Constructors.

        public OrderViewerView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
