﻿using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for EmailRejectionView.xaml
    /// </summary>
    public partial class EmailRejectionView : UserControl
    {
        #region Public Properties.

        public RejectionHistoryViewModel RHVM { get; set; }

        #endregion

        #region Constructors.

        public EmailRejectionView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
