﻿using System;
using System.Threading;
using System.Windows.Threading;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;

namespace UBM.MiddlewareClient.Services
{
       
    public class AsyncWorkerService : IAsyncWorkerService
    {

        public SynchronizationContext Context { get; private set; }
        
        public event EventHandler Started;
        public event EventHandler Complete;

        private readonly IMessageService messageService;

        public bool UserOperationRunning { get; private set; }

        public AsyncWorkerService(SynchronizationContext context, IMessageService messageService)
        {
            this.Context = context;
            this.messageService = messageService;
            this.UserOperationRunning = false;
        }

        private void Checker()
        {
            //Console.WriteLine("Checker!");
            
        }


        public void RunAsyncSystemOperation(Action action)
        {
            Thread worker = GetWorkerThread(() =>
            {
                try
                {                 
                    action.Invoke();               
                }
                catch (Exception exception)
                {                  
                    Log.Error(action.Method.Name + " failed", exception);
                }                
            }, Checker);

            worker.Start();
        }

        public void Post(SendOrPostCallback callback)
        {
           Context.Post(callback, null);
        }

        public void RunAsyncUserOperation(Action action, string operationName = "", bool onlyReportOnError = false)
        {
            if (UserOperationRunning)
            {
                messageService.SendUserMessage("A user operation is already running. Please wait until it completes",
                               MessageType.Busy);
                return;
            }

            

            OnStarted(EventArgs.Empty);
            Thread worker = GetWorkerThread(() =>
                                             {
                                                 try
                                                 {
                                                     if(!string.IsNullOrEmpty(operationName) && !onlyReportOnError)
                                                         messageService.SendUserMessage(operationName + " started", MessageType.Info);

                                                     action.Invoke();

                                                     if (!string.IsNullOrEmpty(operationName) && !onlyReportOnError)
                                                        messageService.SendUserMessage(operationName + " complete", MessageType.Good);
                                                 }
                                                 catch (Exception exception)
                                                 {
                                                     if (!string.IsNullOrEmpty(operationName))
                                                        messageService.SendUserMessage(operationName + " failed", MessageType.Error);
                                                     Log.Error(operationName + " failed", exception);
                                                 }
                                                 finally
                                                 {
                                                     OnComplete(EventArgs.Empty);
                                                 }
                                                 
                                             }, Checker );
            worker.Start();
        }



        private void OnComplete(EventArgs args)
        {
            UserOperationRunning = false;

            if (Complete != null) 
                Complete(this, args);
        }

        private void OnStarted(EventArgs args)
        {
            UserOperationRunning = true;

            if (Started != null) 
                Started(this, args);
        }

        public delegate void AlertDelegate();

        public static Thread GetWorkerThread(Action action)
        {
            ThreadStart start = delegate()
            {
                DispatcherOperation operation = Dispatcher.CurrentDispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    action);

                CheckStatus(operation, null);
            };

            return new Thread(start);
        }

        public static Thread GetWorkerThread(Action action, AlertDelegate alert)
        {
            ThreadStart start = delegate()
            {
                DispatcherOperation operation = Dispatcher.CurrentDispatcher.BeginInvoke(
                    DispatcherPriority.Normal,
                    action);

                CheckStatus(operation, alert);
            };

            return new Thread(start);
        }

        private static void CheckStatus(DispatcherOperation operation, AlertDelegate alert)
        {
            DispatcherOperationStatus status = operation.Status;
            while (status != DispatcherOperationStatus.Completed)
            {
                status = operation.Wait(TimeSpan.FromMilliseconds(1000));
                //if (status == DispatcherOperationStatus.Aborted)
                {
                    if (alert != null) alert();
                }
            }
        }
    }

  
}
