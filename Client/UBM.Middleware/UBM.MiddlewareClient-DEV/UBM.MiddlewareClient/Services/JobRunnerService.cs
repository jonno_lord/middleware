﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Interfaces;
using IJobRunnerService = UBM.MiddlewareClient.Interfaces.IJobRunnerService;

namespace UBM.MiddlewareClient.Services
{
    public class JobRunnerService : IJobRunnerService
    {
        private readonly IMessageService messageService;
        private readonly ISystemService systemService;
        private readonly INotificationService notificationService;

        private readonly Queue<Job> jobQueue;
        private Job currentJob;
        private int totalSteps;

        public bool Running { get; private set; }

        public event JobProgressEventHandler JobProgress;
        public event EventHandler JobComplete;


        public JobRunnerService(IMessageService messageService,
                                ISystemService systemService,
                                INotificationService notificationService,
                                MasterSyncController masterSyncController)
        {
            this.messageService = messageService;
            this.systemService = systemService;
            this.notificationService = notificationService;
            
            JobSyncController jobSyncController = new JobSyncController(masterSyncController, this);
            

            this.Running = false;
            this.jobQueue = new Queue<Job>();

        }

        public void RunJob(string jobName)
        {
            RunJobs(new List<string> {jobName});
        }

        public void RunJobs(IEnumerable<string> jobNames)
        {
            if(Running)
            {
                messageService.SendUserMessage("Another job is running. Please wait until it finishes", MessageType.Busy);
                return;                
            }

            totalSteps = 0;
            Running = true;

            List<Job> jobs = Job.CreateJobs(systemService, jobNames).ToList();
            
            foreach (Job job in jobs)
            {
                totalSteps += job.NumberOfStepsRemaining;
                jobQueue.Enqueue(job);
            }

            FireJob();

        }

        public void RunJob(int jobId)
        {
            if (Running)
            {
                messageService.SendUserMessage("Another job is running. Please wait until it finishes", MessageType.Busy);
                return;
            }

            totalSteps = 0;
            Running = true;

            List<Job> jobs = new List<Job> {Job.CreateJobById(systemService, jobId)};

            foreach (Job job in jobs)
            {
                totalSteps += job.NumberOfStepsRemaining;
                jobQueue.Enqueue(job);
            }

            FireJob();
        }

        private void FireJob()
        {
            if (jobQueue.Count > 0)
            {
                currentJob = jobQueue.Peek();
                OnJobProgress(currentJob.DisplayName, "Initialising");
                string status = notificationService.FireMiddlewareJob(currentJob.JobId);
                EvaluateStatus(status);
            }
            else
            {
                Running = false;
                OnJobComplete();
            }
        }

        private void MoveToNextJob()
        {
            jobQueue.Dequeue();
            FireJob();
        }

        private void OnJobComplete()
        {
            if(JobComplete != null)
            {
                JobComplete(this, EventArgs.Empty);
            }
        }

        public void IncomingProgressReport(string jobName,string stepDetail, double percentageComplete)
        {
  
            if(currentJob != null && jobName == currentJob.DisplayName)
            {

                if (percentageComplete == 100)
                {
                    
                    currentJob.StepComplete();                  
                }

                OnJobProgress(jobName, stepDetail);

                if (stepDetail.Contains("Failed") || currentJob.NumberOfStepsRemaining == 0)
                {
                    MoveToNextJob(); // When job is finished move to the next one)
                }

            }
        }

        private void OnJobProgress(string jobName, string stepDetail)
        {
            if (GetStepDetail(stepDetail) == "Complete")
                messageService.SendUserMessage(currentJob.DisplayName + " " + stepDetail, MessageType.Good);

            if(JobProgress != null)
            {
                JobProgress(this, new JobProgressEventHandlerArgs
                                      {
                                          JobName = jobName,
                                          StepDetail = GetStepDetail(stepDetail),
                                          PercentageComplete = CalculatePercentageComplete()
                                      });
            }
        }


        private string GetStepDetail(string stepDetail)
        {
            // multistep jobs wont report complete until final step is complete
            if(stepDetail == "Complete")
                return currentJob.NumberOfStepsRemaining == 0 ? "Complete" : "Processing";

            return stepDetail;
        }

        private double CalculatePercentageComplete()
        {
            List<Job> jobs = jobQueue.ToList();

            double stepsRemaining = 0;            

            foreach (Job job in jobs)
            {
                stepsRemaining += job.NumberOfStepsRemaining;
            }

            return ( (totalSteps - stepsRemaining) / totalSteps)*100.0;
        }

        


        private void EvaluateStatus(string status)
        {
            if (!status.Contains("Started"))
            {
                messageService.SendUserMessage(currentJob.DisplayName + " " + status, MessageType.Bad);
                MoveToNextJob(); // RECURSIVE - If job doesnt fire, move to the next one
            }
            else
            {
                messageService.SendUserMessage(currentJob.DisplayName + " started", MessageType.Good);
            }
        }



       
    }
}
