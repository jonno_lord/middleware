﻿using System.Windows.Controls;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for OrderViewer.xaml
    /// </summary>
    public partial class OrderViewer : Page
    {
        #region Constructors.

        /// <summary>
        /// OrderViewer Constructor that takes an instance of the OrderViewModel
        /// to set up the View datacontext. 
        /// </summary>
        /// <param name="mvm">
        /// Instace of the MasterViewModel used to setup the DataContext so that
        /// the binding on the View all work.
        /// </param>
        public OrderViewer(MasterViewModel mvm)
        {
            InitializeComponent();
            vwOrderViewer.DataContext = mvm.OrderViewModel;
            vwOrderViewer.OVM = mvm.OrderViewModel;
        }

        #endregion
    }
}
