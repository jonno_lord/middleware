﻿using System.Windows.Controls;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for CustomerOrderViewer.xaml
    /// </summary>
    public partial class CustomerViewer : Page
    {      
        #region Constructors.

        /// <summary>
        /// Constructor that takes a CustoemrViewModel so that it can set 
        /// the DataContext of the View.
        /// </summary>
        /// <param name="mvm">
        /// Instance of the MasterViewModwl so that all Pages and views 
        /// use the same MasterViewModwl.
        /// </param>
        public CustomerViewer(MasterViewModel mvm)
        {
            InitializeComponent();

            // Sets the DataContext to the DedupeViewModel.
            vwCustomerViewer.DataContext = mvm.CustomerViewModel;
            vwCustomerViewer.CVM = mvm.CustomerViewModel;
        }

        #endregion
    }
}
