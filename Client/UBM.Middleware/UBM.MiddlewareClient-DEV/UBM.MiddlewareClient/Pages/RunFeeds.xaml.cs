﻿using System.Windows.Controls;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for RunFeeds.xaml
    /// </summary>
    public partial class RunFeeds : Page
    {
        /// <summary>
        /// Construtor for the RunFeeds Page that sets the DataContext of the view
        /// To the correct ViewModel.
        /// </summary>
        /// <param name="mvm">
        /// MasterViewModle that holds an instance of the other ViewModels.
        /// </param>
        public RunFeeds(MasterViewModel mvm)
        {
            InitializeComponent();
            vwMSIRunFeeds.DataContext = mvm.FeedRunnerViewModel;
        }
    }
}
