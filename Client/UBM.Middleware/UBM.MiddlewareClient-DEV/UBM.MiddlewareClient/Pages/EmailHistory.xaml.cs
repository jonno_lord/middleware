﻿using System.Windows.Controls;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for EmailRejections.xaml
    /// </summary>
    public partial class EmailHistory : Page
    {
        #region Constructors.

        /// <summary>
        /// Constructor that takes HistoryViewModel as a parameter to
        /// set the DataContext of the view.
        /// </summary>
        /// <param name="mvm">
        /// Instance of the MasterViewModel that is used in this screen.
        /// </param>
        public EmailHistory(MasterViewModel mvm)
        {
            InitializeComponent();

            // Sets the DataContext to the HistoryViewModel.
            vwEmailRejections.DataContext = mvm;
            vwEmailRejections.RHVM = mvm.RejectionHistoryViewModel;
        }

        #endregion
    }
}
