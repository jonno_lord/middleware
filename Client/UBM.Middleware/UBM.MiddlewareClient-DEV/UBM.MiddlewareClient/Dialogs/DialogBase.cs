﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace UBM.MiddlewareClient.Dialogs
{
    public abstract class DialogBase : Window
    {
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if(DialogResult == null) e.Cancel = true;
            base.OnClosing(e);
        }
    }
}
