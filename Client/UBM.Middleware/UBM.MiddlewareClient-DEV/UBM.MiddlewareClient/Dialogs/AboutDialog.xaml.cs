﻿using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for AboutDialog.xaml
    /// </summary>
    public partial class AboutDialog : DialogBase
    {
        #region Private Window Control Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        #endregion

        #region Constructors.

        public AboutDialog(string titleText, string environmentText)
        {
            InitializeComponent();

            txtTitle.Text = titleText;

            txtVersion.Text = "Version: " + Assembly.GetExecutingAssembly().GetName().Version;
            txtenvironment.Text = environmentText;
        }

        #endregion
    }
}
