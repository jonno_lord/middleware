﻿using System.Windows;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for AcceptDialog.xaml
    /// </summary>
    public partial class AcceptDialog : DialogBase
    {

        #region Private Window Control Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnMinimiseClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        #endregion

        #region Constructors.

        public AcceptDialog()
        {
            InitializeComponent();
        }

        #endregion
    }
}
