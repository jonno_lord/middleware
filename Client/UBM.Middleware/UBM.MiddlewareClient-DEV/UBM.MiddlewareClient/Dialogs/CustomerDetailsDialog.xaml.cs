﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using UBM.MiddlewareClient.Interfaces;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for CustomerDetailsDialog.xaml
    /// </summary>
    public partial class CustomerDetailsDialog : DialogBase
    {
        #region Public Properties.

        public ICustomerDetailsViewModel ViewModel { get; set; }

        public MainWindow MainWindow { get; set; }

        #endregion

        #region Private Methods.

        private void ChangeScreen(string buttonClicked)
        {
            DoubleAnimation opacityAnimation = new DoubleAnimation
            {
                From = 1,
                To = 0,
                Duration = TimeSpan.FromSeconds(1)
            };

            // Sets up the opacity animation to move between screens. 
            grdScrutiny.BeginAnimation(OpacityProperty, opacityAnimation);

            // Switch case to calculate what button was pressed. 
            switch (buttonClicked)
            {
                case "btnScrutiny":
                    bdrScrutiny.Visibility = Visibility.Visible;
                    bdrEmail.Visibility = Visibility.Collapsed;
                    bdrLog.Visibility = Visibility.Collapsed;
                    bdrLegalHistory.Visibility = Visibility.Collapsed;
                    break;
                case "btnEmail":
                    bdrScrutiny.Visibility = Visibility.Collapsed;
                    bdrEmail.Visibility = Visibility.Visible;
                    bdrLog.Visibility = Visibility.Collapsed;
                    bdrLegalHistory.Visibility = Visibility.Collapsed;
                    break;
                case "btnLog":
                    bdrScrutiny.Visibility = Visibility.Collapsed;
                    bdrEmail.Visibility = Visibility.Collapsed;
                    bdrLog.Visibility = Visibility.Visible;
                    bdrLegalHistory.Visibility = Visibility.Collapsed;
                    break;
                case "btnLegalHistory":
                    bdrScrutiny.Visibility = Visibility.Collapsed;
                    bdrEmail.Visibility = Visibility.Collapsed;
                    bdrLog.Visibility = Visibility.Collapsed;
                    bdrLegalHistory.Visibility = Visibility.Visible;
                    break;
            }

            opacityAnimation = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = TimeSpan.FromSeconds(1)
            };
            grdScrutiny.BeginAnimation(OpacityProperty, opacityAnimation);
        }

        #endregion

        #region Private Control Events.

        #region Window Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnMaximiseClick(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                Point relPoint =
                    MainWindow.vwCustomerDedupeControl.TransformToAncestor(MainWindow).Transform(new Point(0, 0));


                MaxHeight = (relPoint.Y - 5);
                if (MaxHeight <= MinHeight)
                    MaxHeight = MinHeight;

                WindowState = WindowState.Maximized;
            }

        }

        private void btnMinimiseClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void TopThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Height > MinHeight)
            {
                Height -= e.VerticalChange;
                Top += e.VerticalChange;
            }
            else
            {
                Height = MinHeight + 4;
                TopThumb.ReleaseMouseCapture();
            }
        }

        private void TopRightThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Height > MinHeight || Width > MinWidth)
            {
                Height -= e.VerticalChange;
                Top += e.VerticalChange;
                Width += e.HorizontalChange;
            }
            else
            {
                Height = MinHeight + 4;
                Width = MinWidth + 4;
                TopRightThumb.ReleaseMouseCapture();
            }
        }

        private void TopLeftThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Height > MinHeight || Width > MinWidth)
            {
                Height -= e.VerticalChange;
                Top += e.VerticalChange;
                Width -= e.HorizontalChange;
                Left += e.HorizontalChange;
            }
            else
            {
                Height = MinHeight + 4;
                Width = MinWidth + 4;
                TopLeftThumb.ReleaseMouseCapture();
            }
        }

        private void BottomThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Height > MinHeight)
            {
                Height += e.VerticalChange;
            }
            else
            {
                Height = MinHeight + 4;
                BottomThumb.ReleaseMouseCapture();
            }
        }

        private void BottomRightThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Height > MinHeight || Width > MinWidth)
            {
                Height += e.VerticalChange;
                Width += e.HorizontalChange;
            }
            else
            {
                Height = MinHeight + 4;
                Width = MinWidth + 4;
                BottomRightThumb.ReleaseMouseCapture();
            }
        }

        private void BottomLeftThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Height > MinHeight || Width > MinWidth)
            {
                Height += e.VerticalChange;
                Width -= e.HorizontalChange;
                Left += e.HorizontalChange;
            }
            else
            {
                Height = MinHeight + 4;
                Width = MinWidth + 4;
                BottomLeftThumb.ReleaseMouseCapture();
            }
        }

        private void LeftThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Width > MinWidth)
            {
                Width -= e.HorizontalChange;
                Left += e.HorizontalChange;
            }
            else
            {
                Width = MinWidth + 4;
                LeftThumb.ReleaseMouseCapture();
            }
        }

        private void RightThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (Width > MinWidth)
            {
                Width += e.HorizontalChange;
            }
            else
            {
                Width = MinWidth + 4;
                RightThumb.ReleaseMouseCapture();
            }
        }

        #endregion

        private void HyperlinkRequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Hyperlink hl = (Hyperlink)sender;
            string navigateUri = hl.NavigateUri.ToString();
            // Opens the web address in the default browser. 
            Process.Start(new ProcessStartInfo(navigateUri));

            e.Handled = true;
        }

        private void ScreenButtonClick(object sender, RoutedEventArgs e)
        {
            Button btnClicked = (Button)sender;
            ChangeScreen(btnClicked.Name);
        }

        #endregion

        #region Constructors.

        public CustomerDetailsDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Properties.

        public void ShowDialog(DialogType dialogType)
        {
            switch (dialogType)
            {
                case DialogType.CustomerDetails:
                    ChangeScreen("btnScrutiny");
                    Show();
                    break;
                case DialogType.CustomerDetailsDefaultLegal:
                    ChangeScreen("btnLegalHistory");
                    Show();
                    break;
                case DialogType.CustomerDetailsDefaultEmail:
                    ChangeScreen("btnEmail");
                    Show();
                    break;
            }
        }

        #endregion
    }
}
