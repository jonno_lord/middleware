﻿using System.Windows;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for IconKeyDialog.xaml
    /// </summary>
    public partial class IconKeyDialog : DialogBase
    {
        #region Private Window Control Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void btnMinimiseClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        #endregion

        #region Constructors.

        public IconKeyDialog()
        {
            InitializeComponent();
        }

        #endregion
    }
}
