﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(string), typeof(Visibility))]
    internal class JDEAccountNumberVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == null || (string)value == "" || (string)value == "0") return Visibility.Collapsed;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}