﻿using System;
using System.Globalization;
using System.Windows.Data;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(GroupView), typeof(string))]
    internal class GroupViewConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((GroupView)value)
            {
                case GroupView.Accepted:
                    return "Accepted";
                case GroupView.Rejected:
                    return "Rejected";
                case GroupView.Waiting:
                    return "Waiting";
                case GroupView.LegalHold:
                    return "Legal Hold";
                case GroupView.PreviouslyRejected:
                    return "Previously Rejected";
                case GroupView.Scrutiny:
                    return "Scrutiny";
                case GroupView.PostScrutiny:
                    return "Post Scrutiny";
                case GroupView.Search:
                    return "Search";
                case GroupView.All:
                    return "All";
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}