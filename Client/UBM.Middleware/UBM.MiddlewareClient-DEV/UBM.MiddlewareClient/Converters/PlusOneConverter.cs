﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(int), typeof(int))]
    internal class PlusOneConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value + 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value.ToString() == "")
                {
                    return "";
                }
                else
                {
                    object o = int.Parse(value.ToString()) - 1;
                    return o;
                }
            }
            catch
            {
                return "";
            }
        }

        #endregion
    }
}