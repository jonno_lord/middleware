﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(int?), typeof(bool))]
    internal class EmailBodyLengthConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int?)value == null) return false;
            return (int)value > 30;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}