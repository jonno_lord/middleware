﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface ISupportsCustomerDetails
    {
        ICustomerDetailsViewModel CustomerDetailsViewModel { get; }
    }
}
