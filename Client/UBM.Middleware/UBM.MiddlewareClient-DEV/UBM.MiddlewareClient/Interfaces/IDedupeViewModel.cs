﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface IDedupeViewModel
    {
        ICommand PrepareLegalCommand { get; }
        ICommand AddLegalHoldCommand { get; }
        ICommand RemoveLegalHoldCommand { get; }

        ICommand PrepareVatCommand { get; }
        ICommand AddVatNumberCommand { get; }
        ICommand RemoveVatNumberCommand { get; }

        ICommand PrepareRejectCommand { get; }
        ICommand EditNotificationDialogCommand { get; }
        ICommand PrepareAcceptCommand { get; }
        ICommand AcceptCustomerCommand { get; }
        ICommand CancelAcceptDialogCommand { get; }
        ICommand ResetCustomerCommand { get; }
        ICommand CommitCustomerCommand { get; }
        ICommand ViewCustomerGroupCommand { get; }

        ICommand RemoveCustomerLockCommand { get; }
        ICommand SelectAllCustomersCommand { get; }
    }
}
