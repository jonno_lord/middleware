﻿using System;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Interfaces
{

    public enum DialogAction
    {
        Open,
        Close
    }

    public enum DialogType
    {
        MessageBox,
        Legal,
        Vat,
        RejectNotification,
        Accept,
        CustomerDetails,
        CustomerDetailsDefaultEmail,
        CustomerDetailsDefaultLegal
    }
  

    public class DialogEventArgs : EventArgs
    {   
        public DialogEventArgs(DialogAction action, DialogType type, MessageBoxArgs messageBoxArgs)
        {
            this.RequestedAction = action;
            this.Type = type;
            this.MessageBoxArgs = messageBoxArgs;
        }

        public DialogAction RequestedAction { get; set; }
        public DialogType Type { get; set; }
        public MessageBoxArgs MessageBoxArgs { get; set; }
    
    }

    public class MessageBoxArgs
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public MessageType MessageType { get; set; }

        public Response Yes { get; set; }
        public Response No { get; set; }

        public delegate void Response();

    }
}
