﻿using System.Data;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface IPageable
    {
        void GetPage();
        void GoToPage(int gotoPageNumber);
    }
}