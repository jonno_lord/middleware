using System;
using System.Threading;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface IAsyncWorkerService
    {
        SynchronizationContext Context { get; }
        bool UserOperationRunning { get; }

        event EventHandler Started;
        event EventHandler Complete;

        void RunAsyncUserOperation(Action action, string operationName = "", bool onlyReportOnError = false);
        void RunAsyncSystemOperation(Action action);

        void Post(SendOrPostCallback callback);
    }
}