﻿using UBM.MiddlewareClient.Infrastructure;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface ISearchable
    {
        void SearchAsync(SearchInfo searchInfo);
        int Search(SearchInfo searchInfo);
    }
}
