﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface IJobRunnerService
    {
        void RunJob(string jobName);
        void RunJobs(IEnumerable<string> jobNames);

        void RunJob(int jobId);

        bool Running { get; }

        event JobProgressEventHandler JobProgress;
        event EventHandler JobComplete;
    }

    public delegate void JobProgressEventHandler(object sender, JobProgressEventHandlerArgs args);

    public class JobProgressEventHandlerArgs    
    {
        public string JobName { get; set; }
        public string StepDetail { get; set; }
        public double PercentageComplete { get; set; }
    }
}
