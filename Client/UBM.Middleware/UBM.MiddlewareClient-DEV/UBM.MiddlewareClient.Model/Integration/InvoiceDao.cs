﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Invoicing;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Integration
{
    public class InvoiceDao : IInvoiceDao
    {
        private InvoiceDataContext invoiceDataContext;

        public void InsertPublicationInvoicingParameters(string titleCard, DateTime insertDate, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            using (invoiceDataContext = new InvoiceDataContext())
            {

                vwPublicationInvoicingParameter pub = new vwPublicationInvoicingParameter()
                {
                    SystemName = systemName,
                    SelectedBy = Environment.UserName,
                    TitleCard = titleCard,
                    InsertDate = insertDate
                };

                invoiceDataContext.vwPublicationInvoicingParameters.InsertOnSubmit(pub);

                invoiceDataContext.SubmitChanges();

            }

        }

        public int GetBatchNumber(Invoice invoice)
        {
            TraceHelper.Trace(TraceType.Database);

            //DataSet ds = SqlDataAccess.GetInvoiceBatchNumbers(invoice.SystemName, invoice.Publication.Code, DateTime.Now);

            //foreach (DataRow dataRow in ds.Tables[0].Rows)
            //{
            //    int i = (int) dataRow["batch_number"];
            //}

            //return 0;

            using (invoiceDataContext = InvoiceDataContext.ReadOnly)
            {

                var query = from c in invoiceDataContext.vwPublicationInvoicingParameters
                            where
                                c.InsertDate == invoice.IssueDate && c.SystemName == invoice.SystemName &&
                                c.SelectedBy == Environment.UserName
                            select c;

                //vwPublicationInvoicingParameter p = query.FirstOrDefault();

                return query.Max(c => c.BatchNumber) ?? 0;
            }
        }
    }
}
