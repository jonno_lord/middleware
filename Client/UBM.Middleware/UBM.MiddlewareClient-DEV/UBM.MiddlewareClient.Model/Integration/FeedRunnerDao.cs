﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.FeedRunner;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Integration
{
    public class FeedRunnerDao : IFeedRunnerDao
    {
        private static readonly List<string> StatusList = new List<string>
                                                 {
                                                     "FAILED",
                                                     "WAITING",
                                                     "COMPLETE",
                                                 };

        private FeedRunnerDataContext feedRunnerDataContext;


        private List<BatchFeed> GetBatches (DateTime batchDate)
        {
            List<BatchFeed> batchFeeds = new List<BatchFeed>();

            var query = from batches in feedRunnerDataContext.FileDetails
                        join category in feedRunnerDataContext.Categories on batches.Categoryid equals category.id
                        where batches.BatchDate > batchDate && batches.BatchDate < batchDate.AddDays(1)
                        orderby batches.BatchDate ascending 
                        select new {category, batches};
            
            

            foreach (var row in query)
            {
                BatchFeed batchFeed = batchFeeds.FirstOrDefault(c => c.BatchNumber == row.batches.BatchNumber.ToString());

                if (batchFeed == null)
                {
                   
                    batchFeed = new BatchFeed
                                              {
                                                  Category = row.category.CategoryName,
                                                  BatchDate = row.batches.BatchDate,
                                                  BatchNumber = row.batches.BatchNumber.ToString(),
                                                  Status = row.batches.Status,
                                                  FileList = new List<FileFeed>()
                                              };

                    batchFeeds.Add(batchFeed);
                }
                

                    FileFeed fileFeed = new FileFeed
                                            {
                                                FileId = row.batches.id,
                                                FileSize = row.batches.FileSize.ToString(),
                                                FileDate = row.batches.FileDate,
                                                Filename = row.batches.FileName,
                                                FilePath = row.batches.FilePath,
                                                FtpDate = row.batches.FTPDate,
                                                MovedFileDate = row.batches.MovedFileDate,
                                                Status = row.batches.Status,
                                                History = new List<FileHistory>()
                                            };
                    
                    batchFeed.FileList.Add(fileFeed);

                    if(fileFeed.Status != batchFeed.Status)
                    {
                        if(StatusList.IndexOf(fileFeed.Status) < StatusList.IndexOf(batchFeed.Status))
                        {
                            batchFeed.Status = fileFeed.Status;
                        }
                    }

            }

            return batchFeeds;
        }

        public List<BatchFeed> GetFeedBatches(DateTime batchDate)
        {
            TraceHelper.Trace(TraceType.Database);

            using (feedRunnerDataContext = FeedRunnerDataContext.ReadOnly)
            {
                List<BatchFeed> batches = GetBatches(batchDate);
                           
                return batches;
            }
        }

        public List<FileHistory> GetFileHistory(FileFeed file)
        {
            TraceHelper.Trace(TraceType.Database);

            using (feedRunnerDataContext = FeedRunnerDataContext.ReadOnly)
            {
                List<FileHistory> history = new List<FileHistory>();

                var query = from h in feedRunnerDataContext.FileDetailHistories
                            where h.FileDetailId == file.FileId
                            orderby h.id ascending
                            select h;


                foreach (var fileDetailHistory in query)
                {
                    history.Add(new FileHistory
                                {
                                    Dated = fileDetailHistory.Dated,
                                    Description = fileDetailHistory.Description,
                                    Error = fileDetailHistory.isError ?? false
                                });
                }

                return history;
            }
        }
    }
}
