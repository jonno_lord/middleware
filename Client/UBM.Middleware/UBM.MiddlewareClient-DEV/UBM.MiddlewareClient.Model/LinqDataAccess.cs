﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using UBM.Logger;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model
{
    /// <summary>
    /// Class to retrieve database information via Linq to Sql
    /// </summary>
    public class LinqDataAccess
    {
        #region Fields

        private readonly CustomerDao customerDao = new CustomerDao();
        private readonly UserDao userDao = new UserDao();
        private readonly SystemDao systemDao = new SystemDao();
        private readonly ReceiptDao receiptDao = new ReceiptDao();
        private readonly InvoiceDao invoiceDao = new InvoiceDao();
        private readonly OrderDao orderDao = new OrderDao();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        /// <value>The customers.</value>
        public List<vwCustomer> Customers { get; set; }
        /// <summary>
        /// Gets or sets the holds.
        /// </summary>
        /// <value>The holds.</value>
        public List<vwCustomerHold> Holds { get; set; }
        /// <summary>
        /// Gets or sets the rejections.
        /// </summary>
        /// <value>The rejections.</value>
        public List<vwCustomerRejection> Rejections { get; set; }
        /// <summary>
        /// Gets or sets the duplicates.
        /// </summary>
        /// <value>The duplicates.</value>
        public List<vwCustomerDuplicate> Duplicates { get; set; }
        /// <summary>
        /// Gets or sets the violations.
        /// </summary>
        /// <value>The violations.</value>
        public List<vwCustomerViolation> Violations { get; set; }

        /// <summary>
        /// Gets or sets the system names.
        /// </summary>
        /// <value>The system names.</value>
        public List<SystemName> SystemNames { get; set; }
        /// <summary>
        /// Gets or sets the user actions.
        /// </summary>
        /// <value>The user actions.</value>
        public List<UserAction> UserActions { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LinqDataAccess"/> class.
        /// </summary>
        public LinqDataAccess()
        {
           
        }

        #endregion

        #region Public Methods

        #region Populate Lists

        /// <summary>
        /// Populates all lists.
        /// </summary>
        /// <param name="searchTypes">The search types.</param>
        public void PopulateAllLists(List<string> searchTypes)
        {
            PopulateAllCustomers(searchTypes);
            PopulateAllDuplicates();
            PopulateAllLegal(searchTypes);
            PopulateAllViolations();
            PopulateAllRejections(searchTypes);
            PopulateAllUserActions();
        }

        /// <summary>
        /// Populates all user actions.
        /// </summary>
        public void PopulateAllUserActions()
        {
            UserActions = customerDao.GetAllUserActions();
        }

        /// <summary>
        /// Populates all violations.
        /// </summary>
        public void PopulateAllViolations()
        {
            Violations = customerDao.GetAllViolations();
        }

        /// <summary>
        /// Populates all duplicates.
        /// </summary>
        public void PopulateAllDuplicates()
        {
            Duplicates = customerDao.GetAllDuplicates();
        }

        /// <summary>
        /// Populates all rejections.
        /// </summary>
        public void PopulateAllRejections(List<string> searchTypes)
        {
            Rejections = customerDao.GetAllRejections(searchTypes);
        }

        /// <summary>
        /// Populates all legal.
        /// </summary>
        public void PopulateAllLegal(List<string> searchTypes)
        {

            Holds = customerDao.GetAllLegal(searchTypes);
        }

        /// <summary>
        /// Populates all customers.
        /// </summary>
        /// <param name="searchTypes">The search types.</param>
        public void PopulateAllCustomers(List<string> searchTypes)
        {
            Customers = customerDao.GetAllCustomers(searchTypes);
        }

        #endregion

        #region Selects

        /// <summary>
        /// Gets the most used.
        /// </summary>
        /// <returns></returns>
        public MostUsed GetMostUsed()
        {
            return userDao.GetMostUsed();
        }

        /// <summary>
        /// Gets the system names.
        /// </summary>
        /// <returns></returns>
        public List<SystemName> GetSystemNames()
        {
            return systemDao.GetSystemNames();
        }

        /// <summary>
        /// Gets the rejection reasons.
        /// </summary>
        /// <returns></returns>
        public List<RejectionReason> GetRejectionReasons()
        {
            return customerDao.GetRejectionReasons();
        }

        /// <summary>
        /// Gets the user actions for other users.
        /// </summary>
        /// <returns></returns>
        public List<UserAction> GetUserActionsForOtherUsers()
        {
            return customerDao.GetUserActionsForOtherUsers();
        }


        /// <summary>
        /// Gets the user action.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <returns></returns>
        public UserAction GetUserAction(int id, string systemName)
        {
            return customerDao.GetUserActionForCurrentUser(id, systemName);
        }

        /// <summary>
        /// Gets the violations.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <returns></returns>
        public List<vwCustomerViolation> GetViolations(int id, string systemName)
        {
            return customerDao.GetViolations(id, systemName);
        }

        /// <summary>
        /// Gets the duplicates.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <returns></returns>
        public List<vwCustomerDuplicate> GetDuplicates(int id, string systemName)
        {
            return customerDao.GetDuplicates(id, systemName);
        }

        /// <summary>
        /// Gets the legal.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <returns></returns>
        public List<vwCustomerHold> GetLegal(int id, string systemName)
        {
            return customerDao.GetLegal(id, systemName);
        }

        /// <summary>
        /// Gets the rejections.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <returns></returns>
        public List<vwCustomerRejection> GetRejections(int id, string systemName)
        {
            return customerDao.GetRejections(id, systemName);
        }

        /// <summary>
        /// Gets the country groups.
        /// </summary>
        /// <returns></returns>
        public List<CountryGroup> GetCountryGroups()
        {
            return userDao.GetCountryGroups();
        }

        /// <summary>
        /// Gets the user group access.
        /// </summary>
        /// <returns></returns>
        public List<vwUserGroupAccess> GetUserGroupAccess()
        {
            return userDao.GetUserGroupAccess();
        }

        public List<vwReceiptsAggregate> GetReceipts()
        {
            return receiptDao.GetReceipts();
        }

        public List<vwBatchReceipt> GetBatches()
        {
            return receiptDao.GetBatches();
        }

        public int GetJobIdFromString(string name)
        {
            return systemDao.GetJobIdFromString(name);
        }

        public string GetJobShortNameFromId(int id)
        {
            return systemDao.GetJobShortNameFromId(id);
        }

        public int GetIncrementedReceiptNumber(string systemName)
        {
            int? systemId = GetSystemIdFromName(systemName);

            return receiptDao.GetIncrementedReceiptNumber(systemId);
        }

        public int? GetSystemIdFromName(string systemName)
        {
            return systemDao.GetSystemIdFromName(systemName);
        }

        #endregion

        #region Inserts

        /// <summary>
        /// Commits the customer.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <param name="accept">if set to <c>true</c> [accept].</param>
        /// <param name="jdeAccountNumber">The jde account number.</param>
        public void CommitCustomer(int id, string systemName, bool accept, string jdeAccountNumber)
        {

            if (accept)
            {
                customerDao.CommitAcceptedCustomer(id, systemName);
            }
            else
            {
                customerDao.CommitRejectedCustomer(id, systemName);

                UserAction userAction = customerDao.GetUserActionForCurrentUser(id, systemName);

                customerDao.InsertRejection(id, systemName, userAction.reason, userAction.recipient, 
                                            userAction.cc, userAction.body, userAction.sendNotification ?? false);
            }

            PopulateAllUserActions();

            if (!string.IsNullOrEmpty(jdeAccountNumber))
            {
                // @DG:
                // Calls private function to fire a stored procedure to set the JDEAccountNumber.
                SqlDataAccess.UpdateJDEAccountNumber(id, systemName, jdeAccountNumber);
            }
        }


        /// <summary>
        /// Accepts the customer in user actions.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <param name="jdeAccount">The jde account.</param>
        public void AcceptCustomerInUserActions(int id, string systemName, string jdeAccount)
        {
            customerDao.AcceptCustomerInUserActions(id, systemName, jdeAccount);
            PopulateAllUserActions();
        }


        /// <summary>
        /// Rejects the customer in user actions.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <param name="salesperson">The salesperson.</param>
        public void RejectCustomerInUserActions(int id, string systemName, string salesperson)
        {
            customerDao.RejectCustomerInUserActions(id, systemName, salesperson);
            PopulateAllUserActions();
        }

        /// <summary>
        /// Edits the notification dialog.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <param name="body">The body.</param>
        /// <param name="reason">The reason.</param>
        /// <param name="recipient">The recipient.</param>
        /// <param name="cc">The cc.</param>
        /// <param name="send">if set to <c>true</c> [send].</param>
        public void EditUserAction(int id, string systemName, string body, string reason, string recipient, string cc, bool send)
        {
            customerDao.EditUserAction(id, systemName, body, reason, recipient, cc, send);
            
            PopulateAllUserActions();
        }

        /// <summary>
        /// Legals the hold.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <param name="hold">if set to <c>true</c> [hold].</param>
        /// <param name="reason">The reason.</param>
        public void LegalHold(int id, string systemName, bool hold, string reason)
        {
            customerDao.LegalHold(id, systemName, hold, reason);
        }

        /// <summary>
        /// Inserts the most used.
        /// </summary>
        public void InsertMostUsed()
        {
            userDao.InsertMostUsed();
        }

        public void InsertPublicationInvoicingParameters(string titleCard, DateTime insertDate, string systemName)
        {
            invoiceDao.InsertPublicationInvoicingParameters(titleCard, insertDate, systemName);
        }



        #endregion

        #region Updates

        public void UpdateMostUsed(string pageCount)
        {
            userDao.UpdateMostUsed(pageCount);
        }

        public void UpdateCustomerVat(int id, string systemName, string vat)
        {
            customerDao.UpdateCustomerVat(id, systemName, vat);
        }

        #endregion

        #region Deletes

        public void DeleteCustomerInUserActions(int id, string systemName)
        {
            customerDao.DeleteCustomerInUserActions(id, systemName);
            PopulateAllUserActions();
        }

        #endregion

        #endregion


      

    }
}
