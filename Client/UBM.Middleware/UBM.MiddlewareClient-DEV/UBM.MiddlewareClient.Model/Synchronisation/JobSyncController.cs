﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.Controllers
{
    internal class JobSyncController : AbstractGenericSyncController<DBNull>
    {

        private readonly JobController jobController;

        public JobSyncController(MasterViewModel master, JobController jobController) : base(master, null)
        {
            MasterSyncController.OnProgressReportNotification += ProgressReportNotification;
            this.jobController = jobController;
        }

        private void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
            jobController.IncomingProgressReport(jobName, stepDetail, percentageComplete);
        }
    }
}
