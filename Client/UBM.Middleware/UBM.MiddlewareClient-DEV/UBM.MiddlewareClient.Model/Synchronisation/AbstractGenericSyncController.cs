﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using UBM.MiddlewareClient.ClientSyncService;
//using UBM.MiddlewareClient.Model.Enums;
//using UBM.MiddlewareClient.Model.Interfaces;
//using UBM.MiddlewareClient.ViewModel;

//namespace UBM.MiddlewareClient.Controllers
//{

//    abstract class AbstractGenericSyncController<T>
//    {
//        #region Fields

//        protected ObservableCollection<T> SyncCollection;
//        protected static readonly List<Lock> LockList = new List<Lock>();
//        protected static readonly List<Viewing> ViewList = new List<Viewing>();

//        protected readonly MasterViewModel Master;
//        protected readonly MasterSyncController MasterSyncController;
    
//        #endregion

//        #region Private Methods

//        private ISyncable FindSyncObject(int id, string systemName)
//        {
//            foreach (ISyncable customer in SyncCollection)
//            {
//                if (customer.ID == id && customer.SystemName == systemName)
//                {
//                    return customer;
//                }
//            }
//            return null;
//        }

//        private void Synchronise()
//        {
//            try
//            {
//                SyncData syncData = GetSyncData();


//                if (syncData != null) // && (syncData.LockedList.GetLength(0) != 0 || syncData.ViewingList.GetLength(0) != 0))
//                {
//                    SyncViewsClientSide(syncData.ViewingList);
//                    SyncLocksClientSide(syncData.LockedList);
//                }
//            }
//            catch
//            {
//            }
//        }

//        #endregion

//        #region Protected Methods

//        protected void SyncLocksClientSide(IEnumerable<Lock> locks)
//        {
//            // Add locks
//            foreach (Lock l in locks)
//            {
//                bool addlock = true;

//                foreach (Lock mylock in LockList)
//                {
//                    if (mylock.ID == l.ID && mylock.SystemName == l.SystemName) addlock = false;
//                }

//                if (addlock)
//                {
//                    LockList.Add(l);

//                    ISyncable obj = FindSyncObject(l.ID, l.SystemName);

//                    if(obj != null)
//                    {
//                        string message = obj.Lock(l.UserName);
//                        SendUserMessage(message, MessageType.Info, l.ObjectType);
//                    }

//                }
//            }

//            // Remove locks
//            var expiredLocks = new List<Lock>();

//            foreach (Lock mylocks in LockList)
//            {
//                bool lockstilvalid = false;
//                foreach (Lock serverlocks in locks)
//                {
//                    if (mylocks.ID == serverlocks.ID && mylocks.SystemName == serverlocks.SystemName)
//                    {
//                        lockstilvalid = true;
//                        break;
//                    }
//                }

//                if (!lockstilvalid)
//                {
//                    expiredLocks.Add(mylocks);
//                }
//            }

//            foreach (Lock expired in expiredLocks)
//            {
//                LockList.Remove(expired);

//                ISyncable obj = FindSyncObject(expired.ID, expired.SystemName);

//                if(obj != null)
//                {
//                    string message = obj.Unlock(expired.UserName);
//                    SendUserMessage(message, MessageType.Info, expired.ObjectType);
//                }

//            }
//        }

//        protected void SyncViewsClientSide(IEnumerable<Viewing> views)
//        {
//            // Add views
//            foreach (Viewing v in views)
//            {
//                if (v.UserName != Environment.UserName)
//                {
//                    ISyncable syncable = FindSyncObject(v.ID, v.SystemName);

//                    if(syncable != null)
//                    {
//                        string message = syncable.AddViewer(v.UserName);
//                        SendUserMessage(message, MessageType.Info, v.ObjectType);
//                    }
//                }

//                bool addview = true;

//                foreach (Viewing clientView in ViewList)
//                {
//                    if (clientView.ID == v.ID && clientView.SystemName == v.SystemName && clientView.UserName == v.UserName)
//                        addview = false;
//                }

//                if (addview)
//                {
//                    ViewList.Add(v);
//                }
//            }

//            // Remove views
//            var expiredViewings = new List<Viewing>();

//            foreach (Viewing clientView in ViewList)
//            {
//                bool viewStillValid = false;
//                foreach (Viewing serverViews in views)
//                {
//                    if (clientView.ID == serverViews.ID && clientView.SystemName == serverViews.SystemName &&
//                        clientView.UserName == serverViews.UserName)
//                    {
//                        viewStillValid = true;
//                        break;
//                    }
//                }

//                if (!viewStillValid)
//                {
//                    expiredViewings.Add(clientView);
//                }
//            }

//            foreach (Viewing expired in expiredViewings)
//            {
//                ViewList.Remove(expired);

//                if (expired.UserName != Environment.UserName)
//                {
//                    ISyncable syncable = FindSyncObject(expired.ID, expired.SystemName);

//                    if(syncable != null)
//                    {
//                        string message = syncable.RemoveViewer(expired.UserName);
//                        SendUserMessage(message, MessageType.Info, expired.ObjectType);
//                    }
//                }
//            }
//        }

//        #endregion

//        #region Constructor

//        protected AbstractGenericSyncController(MasterViewModel master, ObservableCollection<T> syncCollection)
//        {
//            this.Master = master;
//            this.MasterSyncController = Master.SyncController;
//            this.SyncCollection = syncCollection;

//            MasterSyncController.Value.OnSynchroniseNotification += Synchronise;
//        }

//        #endregion

//        #region Public Methods

//        public void SyncClientCollection(bool download)
//        {
//            Synchronise();
//            SyncClientCollection();
//        }
        
//        public void SyncClientCollection(ObservableCollection<T> collection)
//        {
//            SyncCollection = collection;
//            SyncClientCollection();
//        }

//        public void SyncClientCollection()
//        {
            
//            ISyncable syncable;

//            foreach (Viewing viewing in ViewList)
//            {
//                syncable = FindSyncObject(viewing.ID, viewing.SystemName);
//                if (syncable != null) syncable.AddViewer(viewing.UserName);
//            }
//            foreach (Lock locked in LockList)
//            {
//                syncable = FindSyncObject(locked.ID, locked.SystemName);
//                if (syncable != null) syncable.Lock(locked.UserName);
//            }
//        }

//        public void View(ISyncable syncable)
//        {
//            MasterSyncController.Value.View(syncable);
//        }

//        public void Unview(ISyncable syncable)
//        {
//            MasterSyncController.Value.Unview(syncable);
//        }

//        public void Lock(ISyncable syncable)
//        {
//            MasterSyncController.Value.Lock(syncable);
//        }

//        public void Unlock(ISyncable syncable)
//        {
//            MasterSyncController.Value.Unlock(syncable);
//        }

//        public void UnlockAll()
//        {
//            MasterSyncController.Value.UnlockAll(GetTypeName());
//        }

//        public void RefreshClients()
//        {
//            MasterSyncController.Value.RefreshClients();
//        }

//        public SyncData GetSyncData()
//        {
//            return MasterSyncController.Value.GetSyncData(GetTypeName());
//        }

//        public string GetTypeName()
//        {
//            return typeof(T).Name;
//        }

//        #endregion

     
//    }
//}
