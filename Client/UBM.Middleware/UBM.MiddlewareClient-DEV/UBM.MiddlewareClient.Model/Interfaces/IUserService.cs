﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Domain;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IUserService
    {

        void UpdateMostUsed();

        List<string> GetSearchTypes();
        List<PageAccess> GetPageAccessRights();

        List<int> GetTopFivePages();
        void UpdatePageCountString(ClientPageNumber page);
    }
}
