﻿using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface ICustomerService
    {
        Customer GetCustomer(int id, string systemName);
        List<Customer> SearchCustomers(SearchInfo searchInfo);
        List<Email> SearchEmails(SearchInfo searchInfo);
        List<Legal> SearchLegal(SearchInfo searchInfo);
        List<Rejection> GetRejectionReasons();

        void AddRejectionEmail(Email email);

        event DrillDownEventHandler OnDrillDown;

    }
}
