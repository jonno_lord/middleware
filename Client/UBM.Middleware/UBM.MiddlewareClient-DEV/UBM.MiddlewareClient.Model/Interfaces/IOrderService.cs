﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IOrderService
    {
        List<Order> SearchOrders(SearchInfo searchInfo);
        DataSet GetOrderDetails(Order order);

        event DrillDownEventHandler OnDrillDown;
    }
}
