﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IDedupeService
    {
        GroupView CurrentGroupView { get; set; }
        void FilterOnSystems(List<SalesSystem> systems);

        List<Customer> CustomersView { get; }
        List<Customer> CustomersAll { get; }

        void ReloadCustomers();

        Customer GetSelectedCustomer();
        void DeselectCustomer();

        List<Rejection> GetRejectionReasons();
        List<Legal> GetLegalRecords(Customer customer);

        List<Email> GetRejectionEmailsSnapshot();
        List<Legal> GetLegalHistorySnapshot();

        bool UpdateEmail(Customer customer);

        bool AddLegalHold(Customer customer, string reason);
        bool RemoveLegalHold(Customer customer);

        bool SetVatNumber(Customer customer, string vatNumber);
        bool RemoveVatNumber(Customer customer);

        bool AcceptCustomer(Customer customer);
        bool RejectCustomer(Customer customer);

        void CommitCustomer(Customer customer);
        void ResetCustomer(Customer customer);

        List<QueueType> GetQueueListFromGroupView(GroupView groupView);

        void AddRejectionEmail(Email email);

        event DrillDownEventHandler OnDrillDown;
    }
}
