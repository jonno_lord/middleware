﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Receipt;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IReceiptService
    {
        List<Receipt> SearchReceipts(SearchInfo searchInfo);
        List<Batch> GetBatches();
        List<LineDetail> GetReceiptLineDetails(int receiptNumber, string systemName);

        bool PrepareBatchToSendToJde(Batch batch);
        bool ReceiptsSentToJde(Batch batch);

        void CreateReceiptCopy(Receipt receipt);

        event DrillDownEventHandler OnDrillDown;
    }
}
