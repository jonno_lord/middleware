﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IUserDao
    {

        MostUsed GetMostUsed();
        void InsertMostUsed();

        List<CountryGroup> GetCountryGroups();
        List<vwUserGroupAccess> GetUserGroupAccess();
        void UpdateMostUsed(string pageCount);


    }
}
