﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Receipt;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IReceiptDao
    {
        List<Receipt> SearchReceipts(SearchInfo searchInfo);
        List<Batch> GetBatches();
        List<LineDetail> GetReceiptLineDetails(int receiptNumber, string systemName);

        bool BatchSentToJde(int batchNumber, string systemName);

        int GetIncrementedReceiptNumber(string systemName);
        bool BatchHasReceiptNumber(int batchNumber, string systemName);
        void SelectReceiptBatch(int batchNumber, string systemName);
        List<int> GetOrdersNumbersInBatch(int batchNumber, string systemName);
        void AllocateReceiptNumberToOrder(int orderUrn, string systemName, int receiptNumber, DateTime timeStamp);

        void InsertReceiptCopy(int receiptNumber, string systemName);
    }
}
