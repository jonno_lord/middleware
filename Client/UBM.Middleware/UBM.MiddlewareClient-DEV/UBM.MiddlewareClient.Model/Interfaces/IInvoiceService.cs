﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Domain.Invoicing;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IInvoiceService
    {



        void InsertPublicationInvoicingParameters(string titleCard, DateTime insertDate, string systemName);

        int GetBatchNumber(Invoice invoice);

        void ClearCache();

        List<Publication> GetPublications(string systemName, bool includeOutstanding);
        List<Invoice> GetInvoices(string systemName, Publication publication, bool showOutstandingInvoices);
    }
}
