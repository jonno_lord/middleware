﻿
namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Duplicate represents a duplidate customer in either a SOP system or JDE
    /// </summary>
    public class Duplicate : ModelBase
    {
        #region Fields

        private string systemName;
        private string urn;
        private string source;
        private ContactDetails contactDetails;
        private string jdeAccountNumber;
        private bool accountActive;

        #endregion

        #region Properties

        public string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }
        
        public string URN
        {
            get { return this.urn; }
            set
            {
                if (value == this.urn) return;
                this.urn = value;
                OnPropertyChanged("URN");
            }
        }
         

        public string Source
        {
            get { return this.source; }
            set
            {
                if (value == this.source) return;
                this.source = value;
                OnPropertyChanged("Source");
            }
        }

        public ContactDetails ContactDetails
        {
            get { return this.contactDetails; }
            set
            {
                if (value == this.contactDetails) return;
                this.contactDetails = value;
                OnPropertyChanged("ContactDetails");
            }
        }
        
        public string JDEAccountNumber
        {
            get { return this.jdeAccountNumber; }
            set
            {
                if (value == this.jdeAccountNumber) return;
                this.jdeAccountNumber = value;
                OnPropertyChanged("JDEAccountNumber");
            }
        }
         
        public bool AccountActive
        {
            get { return this.accountActive; }
            set
            {
                if (value == this.accountActive) return;
                this.accountActive = value;
                OnPropertyChanged("AccountActive");
            }
        }

        #endregion
    }
}
