﻿
namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// ContactDetails holds contact information for a customer. Each customer has a ContactDetails object
    /// </summary>
    public class ContactDetails : ModelBase
    {
        #region Fields

        private string customerName; 
        private string addressLine1;
        private string addressLine2;
        private string addressLine3;
        private string addressLine4;
        private string city;
        private string county;
        private string country;
        private string postcode;
        private string url;
        private string email;
        private string telephone;

        #endregion

        #region Properties
        
        public string CustomerName
        {
            get { return this.customerName; }
            set
            {
                if (value == this.customerName) return;
                this.customerName = value;
                OnPropertyChanged("CustomerName");
            }
        }
         
        public string AddressLine1
        {
            get { return this.addressLine1; }
            set
            {
                if (value == this.addressLine1) return;
                this.addressLine1 = value;
                OnPropertyChanged("AddressLine1");
            }
        }

        
        public string AddressLine2
        {
            get { return this.addressLine2; }
            set
            {
                if (value == this.addressLine2) return;
                this.addressLine2 = value;
                OnPropertyChanged("AddressLine2");
            }
        }

        
        public string AddressLine3
        {
            get { return this.addressLine3; }
            set
            {
                if (value == this.addressLine3) return;
                this.addressLine3 = value;
                OnPropertyChanged("AddressLine3");
            }
        }

        
        public string AddressLine4
        {
            get { return this.addressLine4; }
            set
            {
                if (value == this.addressLine4) return;
                this.addressLine4 = value;
                OnPropertyChanged("AddressLine4");
            }
        }
        
        public string City
        {
            get { return this.city; }
            set
            {
                if (value == this.city) return;
                this.city = value;
                OnPropertyChanged("City");
            }
        }
        
        public string County
        {
            get { return this.county; }
            set
            {
                if (value == this.county) return;
                this.county = value;
                OnPropertyChanged("County");
            }
        }
        
        public string Country
        {
            get { return this.country; }
            set
            {
                if (value == this.country) return;
                this.country = value;
                OnPropertyChanged("Country");
            }
        }
         
        
        public string Postcode
        {
            get { return this.postcode; }
            set
            {
                if (value == this.postcode) return;
                this.postcode = value;
                OnPropertyChanged("Postcode");
            }
        }

        public string URL
        {
            get { return this.url; }
            set
            {
                if (value == this.url) return;
                this.url = value;
                OnPropertyChanged("URL");
            }
        }

        public string Email
        {
            get { return this.email; }
            set
            {
                if (value == this.email) return;
                this.email = value;
                OnPropertyChanged("Email");
            }
        }

        public string Telephone
        {
            get { return this.telephone; }
            set
            {
                if (value == this.telephone) return;
                this.telephone = value;
                OnPropertyChanged("Telephone");
            }
        }

        #endregion

        public static ContactDetails CreateContactDetails(string name,
                                                         string address1,
                                                         string address2,
                                                         string address3,
                                                         string address4,
                                                         string city,
                                                         string county,
                                                         string country,
                                                         string postcode,
                                                         string email,
                                                         string telephone,
                                                         string url)
        {
            return new ContactDetails()
            {
                CustomerName = name,
                AddressLine1 = address1,
                AddressLine2 = address2,
                AddressLine3 = address3,
                AddressLine4 = address4,
                City = city,
                County = county,
                Country = country,
                Email = email,
                Postcode = postcode,
                Telephone = telephone,
                URL = url
            };
        }
    }
}
