﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareGUI.Model
{
    public class State : ICloneable
    {
        public QueueType Queue { get; set; }
        public EntryStatus EntryStatus { get; set; }

        public Legal Legal { get; set; }
        public StatusLog StatusLog { get; set; }
        public Scrutiny Scrutiny { get; set; }

     

        public State(QueueType queue, EntryStatus entryStatus, Legal legal, StatusLog statusLog, Scrutiny scrutiny)
        {
            Queue = queue;
            EntryStatus = entryStatus;
            Legal = legal;
            StatusLog = statusLog;
            Scrutiny = scrutiny;

        }



        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public virtual State Clone()
        {
            // Start with a flat, memberwise copy
            State clonedState = this.MemberwiseClone() as State;

            clonedState.Scrutiny = this.Scrutiny.Clone();
            clonedState.Legal = this.Legal.Clone();
            clonedState.StatusLog = this.StatusLog.Clone();

            return clonedState;
        }
    }
}
