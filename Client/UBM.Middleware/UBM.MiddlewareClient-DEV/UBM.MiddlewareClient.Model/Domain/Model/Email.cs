﻿using System;
using System.Collections.ObjectModel;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Email represents a notification email sent to a salesperson when a customer is rejected from dedupe
    /// </summary>
    public class Email : ModelBase
    {
        #region Fields

        private string from;
        private Recipient to;
        private string cc;
        private ObservableCollection<Recipient> ccList = new ObservableCollection<Recipient>();
        private int ccIndex;
        private string reason;
        private int reasonID;
        private string description;
        private string body;
        private DateTime? dateRejected;
        private bool sent;

        #endregion

        #region Properties

        public string From
        {
            get { return this.from; }
            set
            {
                if (value == this.from) return;
                this.from = value;
                OnPropertyChanged("From");
            }
        }
        
        public Recipient To
        {
            get { return this.to; }
            set
            {
                if (value == this.to) return;
                this.to = value;
                OnPropertyChanged("To");
            }
        }
        
        public string CC
        {
            get { return this.cc; }
            set
            {
                if (value == this.cc) return;
                this.cc = value;
                OnPropertyChanged("CC");
            }
        }
        
        public ObservableCollection<Recipient> CCList
        {
            get { return this.ccList; }
            set
            {
                if (value == this.ccList) return;
                this.ccList = value;
                OnPropertyChanged("CCList");
            }
        }
        
        public int CCIndex
        {
            get { return this.ccIndex; }
            set
            {
                if (value == this.ccIndex) return;
                this.ccIndex = value;
                OnPropertyChanged("CCIndex");
            }
        }
         
        
        public string Reason
        {
            get { return this.reason; }
            set
            {
                if (value == this.reason) return;
                this.reason = value;
                OnPropertyChanged("Reason");
            }
        }
        
        public int ReasonID
        {
            get { return this.reasonID; }
            set
            {
                if (value == this.reasonID) return;
                this.reasonID = value;
                OnPropertyChanged("ReasonID");
            }
        }
        
        public string Description
        {
            get { return this.description; }
            set
            {
                if (value == this.description) return;
                this.description = value;
                OnPropertyChanged("Description");
            }
        }
         
         
        public string Body
        {
            get { return this.body; }
            set
            {
                if (value == this.body) return;
                this.body = value;
                OnPropertyChanged("Body");
            }
        }
        
        public DateTime? DateRejected
        {
            get { return this.dateRejected; }
            set
            {
                if (value == this.dateRejected) return;
                this.dateRejected = value;
                OnPropertyChanged("DateRejected");
            }
        }
        
        public bool Sent
        {
            get { return this.sent; }
            set
            {
                if (value == this.sent) return;
                this.sent = value;
                OnPropertyChanged("Sent");
            }
        }     

        #endregion

    }
}
