﻿
namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Represents a scrurtiny violation for a customer
    /// </summary>
    public class Violation : ModelBase
    {
        #region Fields

        private string name;
        private string description;

        #endregion

        #region Properties

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                OnPropertyChanged("Name");
            }
        }
        
        public string Description
        {
            get { return this.description; }
            set
            {
                if (value == this.description) return;
                this.description = value;
                OnPropertyChanged("Description");
            }
        }
        #endregion
    }
}
