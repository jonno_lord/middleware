﻿
namespace UBM.MiddlewareGUI.Model
{

    /// <summary>
    /// Rejection represents a rejection reason and description poplulated from the MiddlewareCommon database
    /// </summary>
    public class Rejection : ModelBase
    {
        #region Fields

        private int id;
        private string reason;
        private string description;

        #endregion

        #region Properties

        public int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }
        
        public string Reason
        {
            get { return this.reason; }
            set
            {
                if (value == this.reason) return;
                this.reason = value;
                OnPropertyChanged("Reason");
            }
        }
        
        public string Description
        {
            get { return this.description; }
            set
            {
                if (value == this.description) return;
                this.description = value;
                OnPropertyChanged("Description");
            }
        }

        #endregion

        #region Constructor/Public Methods

        public Rejection(int id, string reason, string description)
        {
            ID = id;
            Reason = reason;
            Description = description;
        }

        public override string ToString()
        {
            return Reason;
        }

        #endregion
    }
}
