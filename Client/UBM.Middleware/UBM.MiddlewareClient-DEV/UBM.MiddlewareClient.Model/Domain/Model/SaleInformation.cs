﻿
namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Contains sales information for a customer
    /// </summary>
    public class SaleInformation : ModelBase
    {
        #region Fields

        private string productName;
        private string tradingStyle;
        private string vatNumber;
        private string paymentSetting;
        private string taxExemptNumber;
        private string salesPerson;
        private string contractNumber;
        private string batchNumber; // ASOP 
        private string currency;
        private double? orderAmount;

        #endregion

        #region Properties

        public string ProductName
        {
            get { return this.productName; }
            set
            {
                if (value == this.productName) return;
                this.productName = value;
                OnPropertyChanged("ProductName");
            }
        }

        public string TradingStyle
        {
            get { return this.tradingStyle; }
            set
            {
                if (value == this.tradingStyle) return;
                this.tradingStyle = value;
                OnPropertyChanged("TradingStyle");
            }
        }

        public string VATNumber
        {
            get { return this.vatNumber; }
            set
            {
                if (value == this.vatNumber) return;
                this.vatNumber = value;
                OnPropertyChanged("VATNumber");
            }
        }

        public string PaymentSetting
        {
            get { return this.paymentSetting; }
            set
            {
                if (value == this.paymentSetting) return;
                this.paymentSetting = value;
                OnPropertyChanged("PaymentSetting");
            }
        }

        public string TaxExemptNumber
        {
            get { return this.taxExemptNumber; }
            set
            {
                if (value == this.taxExemptNumber) return;
                this.taxExemptNumber = value;
                OnPropertyChanged("TaxExemptNumber");
            }
        }

        public string SalesPerson
        {
            get { return this.salesPerson; }
            set
            {
                if (value == this.salesPerson) return;
                this.salesPerson = value;
                OnPropertyChanged("SalesPerson");
            }
        }


        public string ContractNumber
        {
            get { return this.contractNumber; }
            set
            {
                if (value == this.contractNumber) return;
                this.contractNumber = value;
                OnPropertyChanged("ContractNumber");
            }
        }

        public string BatchNumber
        {
            get { return this.batchNumber; }
            set
            {
                if (value == this.batchNumber) return;
                this.batchNumber = value;
                OnPropertyChanged("BatchNumber");
            }
        }
        
        public string Currency
        {
            get { return this.currency; }
            set
            {
                if (value == this.currency) return;
                this.currency = value;
                OnPropertyChanged("Currency");
            }
        }
         

        public double? OrderAmount
        {
            get { return this.orderAmount; }
            set
            {
                if (value == this.orderAmount) return;
                this.orderAmount = value;
                OnPropertyChanged("OrderAmount");
            }
        }

        #endregion


    }
}
