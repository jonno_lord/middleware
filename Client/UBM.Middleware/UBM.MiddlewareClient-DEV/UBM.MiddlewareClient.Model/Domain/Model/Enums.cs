﻿
namespace UBM.MiddlewareGUI.Model
{

    public enum Mode
    {
        Live,
        Test,
        Dev
    }

    public enum EntryStatus
    {
        RejectedFromJDE,
        RejectedInScrutiny,
        SourceToMiddleware,
        AcceptedInScrutiny,
        MiddlewareToMiddlewareIn,
        MiddlewareToJDE,
        JDEToMiddleware,
        MiddlewareToMiddlewareOut,
        MiddlewareToSource
    }

    public enum CustomerType
    {
        Company,
        Charity,
        Individual
    }


    public enum QueueType 
    {
        Unassigned,
        Waiting,
        OnHold,
        PreviouslyRejected,
        Accepted,
        Rejected

    }

    public enum SearchType
    {
        CustomerName,
        URN,
        JDEAccountNumber
    }


    public enum GroupView
    {
        All,
        Scrutiny,
        PostScrutiny,
        Waiting,
        LegalHold,
        PreviouslyRejected,
        Accepted,
        Rejected,
        Search
    }


    // Search Matching
    public enum Matching
    {
        Fuzzy,
        StartsWith,
        Exact,
    }

    public enum By
    {
        CustomerName,
        URN,
        JDENumber,
        OrderNumber,
        ActionedBy,
        Sender,
        Reason
    }

    public enum Page
    {
        Home,
        CustomerDedupe,
        CustomerDetails,
        CustomerSearch,
        OrderSearch,
        EmailHistory,
        LegalHistory
    }

    public enum MessageType
    {
        None,
        Good,
        Bad,
        Error,
        Info
    }

}
