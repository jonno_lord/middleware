﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Customer represents a customer in Middleware. This holds many other objects which contain customer details
    /// </summary>
    public class Customer : ModelBase
    {
        #region Fields

        private int id;
        private string urn;
        private string jdeAccountNumber;
        private string systemName;
        private CustomerType customerType;
        
        private EntryStatus entryStatus;
        private QueueType queue;
        private bool notify;
        private bool display;

        private bool isSelected;
        private bool isLocked;
        private bool isViewed;
        private string lockedBy;
        private ObservableCollection<string> viewers = new ObservableCollection<string>();

        // objects
        private Email email;
        private ContactDetails contactDetails;
        private Legal legal;
        private Scrutiny scrutiny;
        private SaleInformation saleInformation;
        private List<Email> emails;
        private List<Legal> holds;
        private StatusLog statusLog;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }
     
        public string URN
        {
            get { return this.urn; }
            set
            {
                if (value == this.urn) return;
                this.urn = value;
                OnPropertyChanged("URN");
            }
        }
  
        public string JDEAccountNumber
        {
            get { return this.jdeAccountNumber; }
            set
            {
                if (value == this.jdeAccountNumber) return;
                this.jdeAccountNumber = value;
                OnPropertyChanged("JDEAccountNumber");
            }
        }
        
        public string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }
         

        public QueueType Queue
        {
            get { return this.queue; }
            set
            {
                if (value == this.queue) return;
                this.queue = value;
                OnPropertyChanged("Queue");
            }
        }

        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        
        public bool IsViewed
        {
            get { return this.isViewed; }
            set
            {
                if (value == this.isViewed) return;
                this.isViewed = value;
                OnPropertyChanged("IsViewed");
            }
        }
        
        public bool IsLocked
        {
            get { return this.isLocked; }
            set
            {
                if (value == this.isLocked) return;
                this.isLocked = value;
                OnPropertyChanged("IsLocked");
            }
        }
        
        public string LockedBy
        {
            get { return this.lockedBy; }
            set
            {
                if (value == this.lockedBy) return;
                this.lockedBy = value;
                OnPropertyChanged("LockedBy");
            }
        }
        
        public ObservableCollection<string> Viewers
        {
            get { return this.viewers; }
            set
            {
                if (value == this.viewers) return;
                this.viewers = value;
                OnPropertyChanged("Viewers");
            }
        }
         

        public bool Notify
        {
            get { return this.notify; }
            set
            {
                if (value == this.notify) return;
                this.notify = value;
                OnPropertyChanged("Notify");
            }
        }
        
        public bool Display
        {
            get { return this.display; }
            set
            {
                if (value == this.display) return;
                this.display = value;
                OnPropertyChanged("Display");
            }
        }
         
         
        public CustomerType CustomerType
        {
            get { return this.customerType; }
            set
            {
                if (value == this.customerType) return;
                this.customerType = value;
                OnPropertyChanged("CustomerType");
            }
        }
        
        public EntryStatus EntryStatus
        {
            get { return this.entryStatus; }
            set
            {
                if (value == this.entryStatus) return;
                this.entryStatus = value;
                OnPropertyChanged("EntryStatus");
            }
        }

        public ContactDetails ContactDetails
        {
            get { return this.contactDetails; }
            set
            {
                if (value == this.contactDetails) return;
                this.contactDetails = value;
                OnPropertyChanged("ContactDetails");
            }
        }

        public Legal Legal
        {
            get { return this.legal; }
            set
            {
                if (value == this.legal) return;
                this.legal = value;
                OnPropertyChanged("Legal");
            }
        }

        public Scrutiny Scrutiny
        {
            get { return this.scrutiny; }
            set
            {
                if (value == this.scrutiny) return;
                this.scrutiny = value;
                OnPropertyChanged("Scrutiny");
            }
        }

        public SaleInformation SaleInformation
        {
            get { return this.saleInformation; }
            set
            {
                if (value == this.saleInformation) return;
                this.saleInformation = value;
                OnPropertyChanged("SaleInformation");
            }
        }
        
        public List<Email> Emails
        {
            get { return this.emails; }
            set
            {
                if (value == this.emails) return;
                this.emails = value;
                OnPropertyChanged("Emails");
            }
        }
        
        public List<Legal> Holds
        {
            get { return this.holds; }
            set
            {
                if (value == this.holds) return;
                this.holds = value;
                OnPropertyChanged("Holds");
            }
        }
         
        public Email Email
        {
            get { return this.email; }
            set
            {
                if (value == this.email) return;
                this.email = value;
                OnPropertyChanged("Email");
            }
        }
        
        public StatusLog StatusLog
        {
            get { return this.statusLog; }
            set
            {
                if (value == this.statusLog) return;
                this.statusLog = value;
                OnPropertyChanged("StatusLog");
            }
        }

        #endregion

        public static Customer CreateCustomer(int id, string urn, string jdeAccountNumber, CustomerType customerType)
        {
            return new Customer() { ID = id, URN = urn, JDEAccountNumber = jdeAccountNumber, CustomerType = customerType };
        }
    }
}