﻿using System;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Model.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionHistory
    {
        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>The time.</value>
        public DateTime Time { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the type of the message.
        /// </summary>
        /// <value>The type of the message.</value>
        public MessageType MessageType { get; set; }
         
         
    }
}
