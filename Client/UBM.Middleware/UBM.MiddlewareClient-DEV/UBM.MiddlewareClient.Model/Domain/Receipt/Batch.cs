﻿using System;
using System.Collections.ObjectModel;
using UBM.MiddlewareClient.Model.Orm;
using UBM.MiddlewareClient.Model.Service;

namespace UBM.MiddlewareClient.Model.Domain.Receipt
{
    public class Batch : AbstractSyncableModel
    {
        #region Fields

        private int id;
        private string systemName;

        private DateTime? extracted;
        private int linesCount;
        private int receiptCount;
        private string currency;
        private double amount;
        private bool isSelected;

        #endregion

        #region Properties

        public override int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }
        
        public override string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged(" string SystemName");
            }
        }

        public DateTime? Extracted
        {
            get { return this.extracted; }
            set
            {
                if (value == this.extracted) return;
                this.extracted = value;
                OnPropertyChanged("Extracted");
            }
        }

        public int LinesCount
        {
            get { return this.linesCount; }
            set
            {
                if (value == this.linesCount) return;
                this.linesCount = value;
                OnPropertyChanged("LinesCount");
            }
        }
        
        public int ReceiptCount
        {
            get { return this.receiptCount; }
            set
            {
                if (value == this.receiptCount) return;
                this.receiptCount = value;
                OnPropertyChanged("ReceiptCount");
            }
        }
        
        public string Currency
        {
            get { return this.currency; }
            set
            {
                if (value == this.currency) return;
                this.currency = value;
                OnPropertyChanged("Currency");
            }
        }
        
        public double Amount
        {
            get { return this.amount; }
            set
            {
                if (value == this.amount) return;
                this.amount = value;
                OnPropertyChanged("Amount");
            }
        }

        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
         

        #endregion




        internal override string GetIdentifier()
        {
            throw new NotImplementedException();
        }
        
        //public static ObservableCollection<Batch> CreateBatchList(ReceiptRepository dataAccess)
        //{
        //    ObservableCollection<Batch> batches = new ObservableCollection<Batch>();

        //    foreach (vwBatchReceipt batch in dataAccess.GetBatches())
        //    {
        //        batches.Add(new Batch
        //                        {
        //                            ID = batch.ID,
        //                            SystemName = batch.SystemName,
        //                            IsSelected = false,
        //                            Extracted = batch.Extracted,
        //                            ReceiptCount = batch.ReceiptCount ?? 0,
        //                            LinesCount = batch.LinesTotal ?? 0,
        //                            Currency = batch.Currency,
        //                            Amount = (double)(batch.Amount ?? 0)
                                    
        //                        });
        //    }

        //    return batches;
        //}
                  
        
  
    }
}
