﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Domain
{
    public abstract class AbstractSyncableModel : ModelBase, ISyncable
    {
        #region Properties

        public abstract int ID { get; set; }
        public abstract string SystemName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is viewed.
        /// </summary>
        /// <value><c>true</c> if this instance is viewed; otherwise, <c>false</c>.</value>
        private bool isViewed;
        public bool IsViewed
        {
            get { return this.isViewed; }
            set
            {
                if (value == this.isViewed) return;
                this.isViewed = value;
                OnPropertyChanged("IsViewed");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is locked.
        /// </summary>
        /// <value><c>true</c> if this instance is locked; otherwise, <c>false</c>.</value>
        private bool isLocked;
        public bool IsLocked
        {
            get { return this.isLocked; }
            set
            {
                if (value == this.isLocked) return;
                this.isLocked = value;
                OnPropertyChanged("IsLocked");
            }
        }

        /// <summary>
        /// Gets or sets the locked by.
        /// </summary>
        /// <value>The locked by.</value>
        private string lockedBy;
        public string LockedBy
        {
            get { return this.lockedBy; }
            set
            {
                if (value == this.lockedBy) return;
                this.lockedBy = value;
                OnPropertyChanged("LockedBy");
            }
        }

        /// <summary>
        /// Gets or sets the lock time.
        /// </summary>
        /// <value>The lock time.</value>
        private DateTime lockTime;
        public DateTime LockTime
        {
            get { return this.lockTime; }
            set
            {
                if (value == this.lockTime) return;
                this.lockTime = value;
                OnPropertyChanged("LockTime");
            }
        }


        /// <summary>
        /// Gets or sets the viewers.
        /// </summary>
        /// <value>The viewers.</value>
        private ObservableCollection<ViewerWrapper> viewers = new ObservableCollection<ViewerWrapper>();
        public ObservableCollection<ViewerWrapper> Viewers
        {
            get
            {
                return this.viewers;
            }
            set
            {
                if (value == this.viewers) return;
                this.viewers = value;
                OnPropertyChanged("Viewers");
            }
        }



        #endregion

        #region Public Methods

        public string Lock(string userName)
        {
            IsLocked = true;
            LockedBy = userName;
            LockTime = DateTime.Now;

            return GetIdentifier() + " Locked By " + userName;
        }

        public string Unlock(string userName)
        {
            IsLocked = false;
            LockedBy = "";

            return GetIdentifier() + " Unlocked By " + userName;
        }

        public string AddViewer(string userName)
        {
            if (Viewers.Where(c=>c.Username == userName).Count() == 0) 
                Viewers.Add(new ViewerWrapper{ Username = userName });
            
            //OnPropertyChanged("Viewers");
            IsViewed = true;

            return GetIdentifier() + " Viewed by " + userName;
        }

        public string RemoveViewer(string userName)
        {
            if (Viewers.Where(c => c.Username == userName).Count() == 1)
                Viewers.Remove(Viewers.Where(c => c.Username == userName).FirstOrDefault());
            
            //OnPropertyChanged("Viewers");)))
            if (Viewers.Count == 0) IsViewed = false;

            return GetIdentifier() + " No Longer Viewed by " + userName;
        }

        public string GetTypeName()
        {
            return GetType().Name;
        }


        internal abstract string GetIdentifier();

        #endregion
    }
}
