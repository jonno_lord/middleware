﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Model.Domain.FeedRunner
{
    public class BatchFeed : ModelBase
    {
        public string BatchNumber { get; set; }
        public string Category { get; set; }
        public DateTime? BatchDate { get; set; }
        public string Status { get; set; }

        public List<FileFeed> FileList { get; set; }
    }
}
