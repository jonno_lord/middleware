﻿using System.Data;
using UBM.MiddlewareClient.Model.Orm;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// ContactDetails holds contact information for a customer. Each customer has a ContactDetails object
    /// </summary>
    public class ContactDetails : ModelBase
    {

        #region Properties

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>The name of the customer.</value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the address line1.
        /// </summary>
        /// <value>The address line1.</value>
        public string AddressLine1 { get; set; }


        /// <summary>
        /// Gets or sets the address line2.
        /// </summary>
        /// <value>The address line2.</value>
        public string AddressLine2 { get; set; }


        /// <summary>
        /// Gets or sets the address line3.
        /// </summary>
        /// <value>The address line3.</value>
        public string AddressLine3 { get; set; }


        /// <summary>
        /// Gets or sets the address line4.
        /// </summary>
        /// <value>The address line4.</value>
        public string AddressLine4 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        /// <value>The county.</value>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the postcode.
        /// </summary>
        /// <value>The postcode.</value>
        public string Postcode { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string URL { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the telephone.
        /// </summary>
        /// <value>The telephone.</value>
        public string Telephone { get; set; }

        #endregion

        /// <summary>
        /// Creates the contact details.
        /// </summary>
        /// <param name="linqCustomer">The linq customer.</param>
        /// <returns></returns>
        public static ContactDetails CreateContactDetails(vwCustomer linqCustomer)
        {
            return new ContactDetails
            {
                CustomerName = (linqCustomer.CustomerName ?? "").Trim(),
                AddressLine1 = (linqCustomer.AddressLine1 ?? "").Trim(),
                AddressLine2 = (linqCustomer.AddressLine2 ?? "").Trim(),
                AddressLine3 = (linqCustomer.AddressLine3 ?? "").Trim(),
                AddressLine4 = (linqCustomer.AddressLine4 ?? "").Trim(),
                City = (linqCustomer.City ?? "").Trim(),
                County = (linqCustomer.County ?? "").Trim(),
                Country = (linqCustomer.Country ?? "").Trim(),
                Email = (linqCustomer.Email ?? "").Trim(),
                Postcode = (linqCustomer.PostCode ?? "").Trim(),
                Telephone = (linqCustomer.Phone ?? "").Trim(),
                URL = (linqCustomer.URL ?? "").Trim()
            };

        }


    }
}
