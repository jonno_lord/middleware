﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    public class CC : ModelBase
    {

        private string name;
        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                OnPropertyChanged("Name");
            }
        }



        public string FullCCListConcatenated 
        { 
            get
            {
                string concat = "";

                CCList.ToList().ForEach(c => concat += c + ";");
                return concat.Length > 0 ? concat.Remove(concat.LastIndexOf(";")) : "";
            }
        }

        public string TruncatedCCListConcatenated
        {
            get
            {
                return !string.IsNullOrEmpty(FullCCListConcatenated) && FullCCListConcatenated.Length > 30 ? StringHelper.Truncate(FullCCListConcatenated, 30) ?? "" : FullCCListConcatenated;
            }
        }

        private string ccListConcatenated; 
        public string CCListConcatenated
        {
            get { return this.ccListConcatenated; }
            set
            {
                if (value == this.ccListConcatenated) return;
                this.ccListConcatenated = value;
                OnPropertyChanged("CCListConcatenated");
            }
        }
         
        
        private ObservableCollection<string> ccList;
        public ObservableCollection<string> CCList
        {
            get { return this.ccList; }
            set
            {
                if (value == this.ccList) return;
                this.ccList = value;
                OnPropertyChanged("CCList");
            }
        }

        private string selectedCC;
        public string SelectedCC
        {
            get { return this.selectedCC; }
            set
            {
                if (value == this.selectedCC) return;
                this.selectedCC = value;
                OnPropertyChanged("SelectedCC");
            }
        }

        public CC()
        {
            CCList = new ObservableCollection<string>();
        }

        public static ObservableCollection<string> CreateCCList(string ccString)
        {
            ObservableCollection<string> ccList = new ObservableCollection<string>();

            if (!string.IsNullOrEmpty(ccString))
            {
                foreach (string address in ccString.Split(';'))
                {
                    if (!string.IsNullOrEmpty(address) && !string.IsNullOrWhiteSpace(address))
                        ccList.Add(address);
                }
            }

            return ccList;
        }

    }
}
