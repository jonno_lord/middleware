﻿using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Legal contains legal hold information about a customer. 
    /// </summary>
    public class Legal : ModelBase
    {



        #region Properties


        /// <summary>
        /// Gets or sets the customer ID.
        /// </summary>
        /// <value>The customer ID.</value>
        private int customerId;        
        public int CustomerID
        {
            get { return this.customerId; }
            set
            {
                if (value == this.customerId) return;
                this.customerId = value;
                OnPropertyChanged("CustomerID");
            }
        }
         

        /// <summary>
        /// Gets or sets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        private string systemName;        
        public string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }
         

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>The name of the customer.</value>
        private string customerName;
        public string CustomerName
        {
            get { return this.customerName; }
            set
            {
                if (value == this.customerName) return;
                this.customerName = value;
                OnPropertyChanged("CustomerName");
            }
        }
         

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Legal"/> is hold.
        /// </summary>
        /// <value><c>true</c> if hold; otherwise, <c>false</c>.</value>       
        private bool hold;
        public bool Hold
        {
            get { return this.hold; }
            set
            {
                if (value == this.hold) return;
                this.hold = value;
                OnPropertyChanged("Hold");
            }
        }


        /// <summary>
        /// Gets or sets the hold date.
        /// </summary>
        /// <value>The hold date.</value>
        private DateTime? holdDate;        
        public DateTime? HoldDate
        {
            get { return this.holdDate; }
            set
            {
                if (value == this.holdDate) return;
                this.holdDate = value;
                OnPropertyChanged("HoldDate");
            }
        }
         

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>The reason.</value>
        private string reason;        
        public string Reason
        {
            get { return this.reason; }
            set
            {
                if (value == this.reason) return;
                this.reason = value;
                OnPropertyChanged("Reason");
            }
        }
         

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        private string userName;
        public string UserName
        {
            get { return this.userName; }
            set
            {
                if (value == this.userName) return;
                this.userName = value;
                OnPropertyChanged("UserName");
            }
        }
         

        /// <summary>
        /// Gets or sets the entity number.
        /// </summary>
        /// <value>The entity number.</value>
        private string entityNumber;
        public string EntityNumber
        {
            get { return this.entityNumber; }
            set
            {
                if (value == this.entityNumber) return;
                this.entityNumber = value;
                OnPropertyChanged("EntityNumber");
            }
        }
         
         
        #endregion           


        public static List<Legal> CreateLegalHistory(List<vwCustomerHold> customerHolds)
        {
            var legal = new List<Legal>();

            

            foreach (vwCustomerHold hold in customerHolds)
            {
                legal.Add(new Legal
                {
                    CustomerID = hold.ID,
                    SystemName = hold.SystemName,
                    CustomerName = hold.CustomerName,
                    Hold = hold.Hold ?? false,
                    HoldDate = hold.HoldDate,
                    Reason = hold.Reason,
                    UserName = hold.UserName,
                });
            }

            return legal;
        }

         ///<summary>
        /// Creates the legal history.
        /// </summary>
        /// <param name="customerHold">The customer hold.</param>
        /// <returns></returns>
        public static Legal CreateLegalHistory(vwCustomerHold customerHold)
        {
            return new Legal
            {
                CustomerID = customerHold.ID,
                CustomerName = customerHold.CustomerName,
                Hold = customerHold.Hold ?? false,
                HoldDate = customerHold.HoldDate,
                Reason = customerHold.Reason,
                SystemName = customerHold.SystemName,
                UserName = customerHold.UserName

            };
        }

        /// <summary>
        /// Creates the legal.
        /// </summary>
        /// <param name="legalHistory">The legal history.</param>
        /// <returns></returns>
        public static Legal CreateLegal(List<Legal> legalHistory)
        {
            if (legalHistory.Count > 0) return legalHistory[0];
            return new Legal();
        }


    }
}
