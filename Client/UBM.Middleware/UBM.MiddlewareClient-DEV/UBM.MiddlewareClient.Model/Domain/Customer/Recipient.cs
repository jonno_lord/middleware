﻿using System.Collections.ObjectModel;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Recipient represents someone receiving a rejection email. This is used in conjunction with ADS to resolve email addresses
    /// </summary>
    public class Recipient : ModelBase
    {
        #region Fields

        private string name;
        private ObservableCollection<string> resolvedAddresses = new ObservableCollection<string>();
        private bool isGroup;
        private bool isResolved;
        private bool isSelected;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets or sets the resolved addresses.
        /// </summary>
        /// <value>The resolved addresses.</value>
        public ObservableCollection<string> ResolvedAddresses
        {
            get { return this.resolvedAddresses; }
            set
            {
                if (value == this.resolvedAddresses) return;
                this.resolvedAddresses = value;
                OnPropertyChanged("ResolvedAddresses");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is group.
        /// </summary>
        /// <value><c>true</c> if this instance is group; otherwise, <c>false</c>.</value>
        public bool IsGroup
        {
            get { return this.isGroup; }
            set
            {
                if (value == this.isGroup) return;
                this.isGroup = value;
                OnPropertyChanged("IsGroup");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is resolved.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is resolved; otherwise, <c>false</c>.
        /// </value>
        public bool IsResolved
        {
            get { return this.isResolved; }
            set
            {
                if (value == this.isResolved) return;
                this.isResolved = value;
                OnPropertyChanged("IsResolved");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion
    }
}
