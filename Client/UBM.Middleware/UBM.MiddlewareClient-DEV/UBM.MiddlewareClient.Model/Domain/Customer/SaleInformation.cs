﻿using System;
using System.ComponentModel;
using System.Data;
using UBM.MiddlewareClient.Model.Orm;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Contains sales information for a customer
    /// </summary>
    public class SaleInformation : ModelBase, IDataErrorInfo
    {
        private string vatNumber;

        #region Properties

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>The name of the product.</value>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the trading style.
        /// </summary>
        /// <value>The trading style.</value>
        public string TradingStyle { get; set; }

        /// <summary>
        /// Gets or sets the VAT number.
        /// </summary>
        /// <value>The VAT number.</value>
        public string VATNumber
        {
            get { return this.vatNumber; }
            set
            {
                if (value == this.vatNumber) return;
                this.vatNumber = value;
                OnPropertyChanged("VATNumber");
            }
        }
         

        /// <summary>
        /// Gets or sets the payment setting.
        /// </summary>
        /// <value>The payment setting.</value>
        public string PaymentSetting { get; set; }

        /// <summary>
        /// Gets or sets the tax exempt number.
        /// </summary>
        /// <value>The tax exempt number.</value>
        public string TaxExemptNumber { get; set; }

        /// <summary>
        /// Gets or sets the sales person.
        /// </summary>
        /// <value>The sales person.</value>
        public string SalesPerson { get; set; }


        /// <summary>
        /// Gets or sets the contract number.
        /// </summary>
        /// <value>The contract number.</value>
        public string ContractNumber { get; set; }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        /// <value>The batch number.</value>
        public string BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the currency.
        /// </summary>
        /// <value>The currency.</value>
        public string Currency { get; set; }
        /// <summary>
        /// Gets or sets the name of the event.
        /// </summary>
        /// <value>The name of the event.</value>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the event start date.
        /// </summary>
        /// <value>The event start date.</value>
        public DateTime? EventStartDate { get; set; }


        /// <summary>
        /// Gets or sets the order amount.
        /// </summary>
        /// <value>The order amount.</value>
        public double? OrderAmount { get; set; }

        #endregion


        /// <summary>
        /// Creates the sale information.
        /// </summary>
        /// <param name="linqCustomer">The linq customer.</param>
        /// <returns></returns>
        public static SaleInformation CreateSaleInformation(vwCustomer linqCustomer)
        {
            var saleInformation = new SaleInformation
            {
                BatchNumber = StringHelper.Tidy(linqCustomer.BatchNumber),
                ContractNumber = StringHelper.Tidy(linqCustomer.ContractNumber),
                OrderAmount = linqCustomer.OrderAmount,
                PaymentSetting = StringHelper.Tidy(linqCustomer.PaymentSetting),
                ProductName = StringHelper.Tidy(linqCustomer.ProductName),
                SalesPerson = StringHelper.Tidy(linqCustomer.SalesPerson),
                TaxExemptNumber = StringHelper.Tidy(linqCustomer.TaxExemptNumber),
                TradingStyle = StringHelper.Tidy(linqCustomer.TradingStyle),
                VATNumber = StringHelper.Tidy(linqCustomer.VATNumber),
                Currency = StringHelper.Tidy(linqCustomer.Currency),
                EventStartDate = linqCustomer.EventStartDate
            };

            return saleInformation;
        }

        /// <summary>
        /// Creates the sale information.
        /// </summary>
        /// <param name="customerRow">The customer row.</param>
        /// <returns></returns>
        public static SaleInformation CreateSaleInformation(DataRow customerRow)
        {

            object value = customerRow["OrderAmount"];
            double orderValue = 0;

            if (value != null)
            {
                double.TryParse(value.ToString(), out orderValue);
            }

            return new SaleInformation
            {
                BatchNumber = StringHelper.Tidy(customerRow["BatchNumber"].ToString()),
                ContractNumber = StringHelper.Tidy(customerRow["ContractNumber"].ToString()),
                Currency = customerRow["Currency"].ToString(),
                OrderAmount = orderValue,
                PaymentSetting = customerRow["PaymentSetting"].ToString(),
                ProductName = customerRow["ProductName"].ToString(),
                SalesPerson = customerRow["SalesPerson"].ToString(),
                TaxExemptNumber = StringHelper.Tidy(customerRow["TaxExemptNumber"].ToString()),
                TradingStyle = customerRow["TradingStyle"].ToString(),
                VATNumber = StringHelper.Tidy(customerRow["VATNumber"].ToString())
            };
        }

        public string this[string columnName]
        {
            get
            {
                string validationResult = null;
                switch (columnName)
                {
                    case "VATNumber":
                        validationResult = ValidateVat();
                        break;
                    default:
                        throw new ApplicationException("Unknown Property being validated on Product.");
                }
                return validationResult;
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        private string ValidateVat()
        {
            if(this.VATNumber.Length > 20)
            {
                return "VAT number must be less than 20 characters long.";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
