﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Domain.Invoicing
{
    public class Publication : ModelBase
    {
        #region Fields

        private int id;
        private string systemName;
        private string code;
        private string description;
        private ObservableCollection<Invoice> invoices;
        private bool outstandingFlag;

        private static IInvoiceDao invoiceDao;

        #endregion

        #region Properties

        public int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }
        public string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }
        public string Code
        {
            get { return this.code; }
            set
            {
                if (value == this.code) return;
                this.code = value;
                OnPropertyChanged("Code");
            }
        }
        public string Description
        {
            get { return this.description; }
            set
            {
                if (value == this.description) return;
                this.description = value;
                OnPropertyChanged("Description");
            }
        }
        public ObservableCollection<Invoice> Invoices
        {
            get { return this.invoices; }
            set
            {
                if (value == this.invoices) return;
                this.invoices = value;
                OnPropertyChanged("Invoices");
            }
        }
        public bool OutstandingFlag
        {
            get { return this.outstandingFlag; }
            set
            {
                if (value == this.outstandingFlag) return;
                this.outstandingFlag = value;
                OnPropertyChanged("OutstandingFlag");
            }
        }
         
        #endregion

        public static void SetDataAccess(IInvoiceDao dataAccess)
        {
            invoiceDao = dataAccess;
        }
        
        public static Publication CreatePublication(string systemName, Publication publication, List<Warning> warnings)
        {
            DataSet pubData = SqlDataAccess.GetPublicationInvoices(systemName, publication.Description);
            List<Warning> warningsForInvoiceDate;

            Publication newPub = new Publication
                                     {
                                         ID = publication.ID,
                                         Code = publication.Code,
                                         Description = publication.Description,
                                         SystemName = systemName,
                                         Invoices = new ObservableCollection<Invoice>()
                                     };


            foreach (DataRow row in pubData.Tables[0].Rows)
            {
                Invoice invoice = Invoice.CreateInvoice(systemName, row);

                warningsForInvoiceDate = warnings.FindAll(c => c.InvoiceID == invoice.ID).GroupBy(x => x.URN).Select(x => x.First()).ToList();


                if (warningsForInvoiceDate.Count != 0)
                {
                    if (invoice.Warnings == null) invoice.Warnings = new ObservableCollection<Warning>();

                    invoice.Status = "Warnings";

                    foreach (Warning warning in warningsForInvoiceDate)
                    {
                        invoice.Warnings.Add(warning);
                    }
                }

                newPub.Invoices.Add(invoice);

                if (invoice.OutstandingFlag) newPub.OutstandingFlag = true;
            }
           


            return newPub;
        }

        public static ObservableCollection<Publication> CreatePublications(string systemName, List<Warning> warnings, bool recent = true)
        {
            ObservableCollection<Publication> publications = new ObservableCollection<Publication>();
            List<Publication> pubList = new List<Publication>();


            DataSet pubData = SqlDataAccess.GetPublicationInvoices(systemName,recent);    

            

            List<Warning> warningsForInvoiceDate;

            foreach (DataRow row in pubData.Tables[0].Rows)
            {

                Publication publication = pubList.Find(c => c.ID == (int) row["Title_ID"]);

                if(publication == null)
                {
                    publication = new Publication
                    {
                        ID = (int)row["Title_ID"],
                        Code = row["Short_name"].ToString(),
                        Description = row["Long_name"].ToString(),
                        Invoices = new ObservableCollection<Invoice>(),
                        SystemName = systemName
                    };
                    pubList.Add(publication);
                }

                Invoice invoice = Invoice.CreateInvoice(systemName, row);

                warningsForInvoiceDate = warnings.FindAll(c => c.InvoiceID == invoice.ID).GroupBy(x => x.URN).Select(x => x.First()).ToList();


                if(warningsForInvoiceDate.Count != 0)
                {
                    if (invoice.Warnings == null) invoice.Warnings = new ObservableCollection<Warning>();

                    invoice.Status = "Warnings";

                    foreach (Warning warning in warningsForInvoiceDate)
                    {
                        invoice.Warnings.Add(warning);
                    }
                }



                publication.Invoices.Add(invoice);

                if (invoice.OutstandingFlag) publication.OutstandingFlag = true;
               
            }


            foreach (Publication publication in pubList)
            {

                List<Invoice> invoices = publication.Invoices.OrderBy(c => c.IssueDate).ToList();

                publication.Invoices = new ObservableCollection<Invoice>(invoices);

                publications.Add(publication);
            }


            return publications;
        }
        
        public static Publication GetNullPublication()
        {
            return new Publication()
                       {
                           Invoices = new ObservableCollection<Invoice>()
                       };
        }
    }
}
