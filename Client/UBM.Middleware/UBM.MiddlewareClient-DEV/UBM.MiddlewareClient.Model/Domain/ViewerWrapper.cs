﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Model.Domain
{
    public class ViewerWrapper : ModelBase
    {
        private string username;
        public string Username
        {
            get { return this.username; }
            set
            {
                if (value == this.username) return;
                this.username = value;
                OnPropertyChanged("Username");
            }
        }
         
    }
}
