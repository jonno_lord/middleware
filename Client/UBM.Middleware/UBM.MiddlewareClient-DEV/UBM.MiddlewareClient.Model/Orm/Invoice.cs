using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class InvoiceDataContext
    {

        public InvoiceDataContext() : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON))
        {
            
        }

        public static InvoiceDataContext ReadOnly
        {
            get
            {
                return new InvoiceDataContext { ObjectTrackingEnabled = false };
            }
        }  

    }
}
