using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class UserDataContext
    {
        public UserDataContext()
            : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON))
        {
        
        }

        public static UserDataContext ReadOnly
        {
            get
            {
                return new UserDataContext { ObjectTrackingEnabled = false };
            }
        }   
    }
}
