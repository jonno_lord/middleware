using System;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class CustomerDataContext
    {

        public CustomerDataContext() : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON))
        {
            
        }

        public static CustomerDataContext ReadOnly
        {
            get
            {
                return new CustomerDataContext { ObjectTrackingEnabled = false };
            }
        }   

    }
}
