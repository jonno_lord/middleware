﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Service
{
    public class DedupeService : IDedupeService, IDisposable
    {
        #region Fields

        private const int SNAPSHOT_QUANTITY = 3;

        private readonly ICustomerDao customerDao;
        private readonly List<Rejection> reasonsList;

        private List<string> systems;
        private Customer selectedCustomer;

        #endregion

        public List<Customer> CustomersView
        {
            get { return GetCustomersView(CurrentGroupView); } 
        }

        private List<Customer> customersAll;
        public List<Customer> CustomersAll
        {
            get { return customersAll; }
        }

        public GroupView CurrentGroupView { get; set; }

        private List<Customer> GetCustomersView(GroupView group)
        {
  
            List<QueueType> queueTypes = GetQueueListFromGroupView(group);

            customersAll.ForEach(c => c.OnDrillDown -= DrillDown);

            List<Customer> viewingCustomers = customersAll.FindAll(c => queueTypes.Contains(c.Queue) && 
                                                                        (systems.Contains(c.SystemName) || GetQueueListFromGroupView(GroupView.PostScrutiny).Contains(c.Queue) ));

            viewingCustomers.ForEach(c => c.OnDrillDown += DrillDown);

            return viewingCustomers;
        }

        public DedupeService(List<string> searchTypes) : this(new CustomerDao(searchTypes))
        {
            
        }

        public DedupeService(ICustomerDao customerDao)
        {
            TraceHelper.Trace(TraceType.Service);
             
            this.customerDao = customerDao;
            this.reasonsList = customerDao.GetRejectionReasons();
            this.systems = new List<string>();
            this.customersAll = new List<Customer>();         
        }



        public void ReloadCustomers()
        {
            TraceHelper.Trace(TraceType.Service);

            List<Customer> ticked = new List<Customer>(customersAll.FindAll(c=>c.IsSelected));
            customersAll = customerDao.GetDedupeCustomers();
            if(selectedCustomer != null)
            {
                selectedCustomer.OnDrillDown -= DrillDown;
                selectedCustomer = customersAll.FirstOrDefault(c => c.ID == selectedCustomer.ID && c.SystemName == selectedCustomer.SystemName);
                if(selectedCustomer != null)
                {                  
                    selectedCustomer.OnDrillDown += DrillDown;
                    selectedCustomer.IsClicked = true;
                }

            }
            if (ticked.Count > 0)
            {
                foreach (Customer customer in ticked)
                {
                    Customer cust = customersAll.FirstOrDefault(c => c.ID == customer.ID && c.SystemName == customer.SystemName);
                    if (cust != null) cust.IsSelected = true;
                }
            }


        }

        public void FilterOnSystems(List<SalesSystem> systems)
        {
            TraceHelper.Trace(TraceType.Service);

            this.systems = systems.FindAll(c =>c.IsSelected).Select(c => c.Name).ToList();
        }

        public Customer GetSelectedCustomer()
        {
            TraceHelper.Trace(TraceType.Service);

            return selectedCustomer;
        }

        public void DeselectCustomer()
        {
            TraceHelper.Trace(TraceType.Service);

            selectedCustomer.OnDrillDown -= DrillDown;
            selectedCustomer = null;
        }


        public List<QueueType> GetQueueListFromGroupView(GroupView groupView)
        {
            //LogHelper.Trace(TraceType.Service);

            List<QueueType> queueTypes = new List<QueueType>();

            switch (groupView)
            {
                case GroupView.Waiting:
                    queueTypes.Add(QueueType.Waiting);
                    break;
                case GroupView.LegalHold:
                    queueTypes.Add(QueueType.OnHold);
                    break;
                case GroupView.PreviouslyRejected:
                    queueTypes.Add(QueueType.PreviouslyRejected);
                    break;
                case GroupView.Scrutiny:
                    queueTypes.Add(QueueType.Waiting);
                    queueTypes.Add(QueueType.OnHold);
                    queueTypes.Add(QueueType.PreviouslyRejected);
                    break;
                case GroupView.Accepted:
                    queueTypes.Add(QueueType.Accepted);
                    break;
                case GroupView.Rejected:
                    queueTypes.Add(QueueType.Rejected);
                    break;
                case GroupView.PostScrutiny:
                    queueTypes.Add(QueueType.Accepted);
                    queueTypes.Add(QueueType.Rejected);
                    break;
            }

            return queueTypes;
        }

      

        public List<Rejection> GetRejectionReasons()
        {
            TraceHelper.Trace(TraceType.Service);

            return customerDao.GetRejectionReasons();
        }

        public List<Legal> GetLegalRecords(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            return customerDao.GetLegalRecords(customer);
        }

        public bool UpdateEmail(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            customerDao.EditUserAction(customer.ID,customer.SystemName,customer.Email.Body ?? "",customer.Email.Reason,customer.Email.To ,customer.Email.CC.FullCCListConcatenated, customer.Email.Sent);
            return true;
        }


        public bool AddLegalHold(Customer customer, string reason)
        {
            TraceHelper.Trace(TraceType.Service);

            customerDao.LegalHold(customer.ID, customer.SystemName, true, reason);

            customer.Legal.Reason = reason;
            customer.Legal.Hold = true;

            customer.Holds = new ObservableCollection<Legal>(customerDao.GetLegalRecords(customer));
            customer.Queue = QueueType.OnHold;

            customer.Unlock(Environment.UserName);

            return true;
        }

        public bool RemoveLegalHold(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            customerDao.LegalHold(customer.ID, customer.SystemName, false, "");

            customer.Legal.Reason = "";
            customer.Legal.Hold = false;

            customer.Holds = new ObservableCollection<Legal>(customerDao.GetLegalRecords(customer));

            if(customer.Queue != QueueType.Accepted && customer.Queue != QueueType.Rejected)
                customer.Queue = customer.Emails.Count > 0 ? QueueType.PreviouslyRejected : QueueType.Waiting;

            customer.Unlock(Environment.UserName);

            return true;
        }

        public bool SetVatNumber(Customer customer, string vatNumber)
        {
            TraceHelper.Trace(TraceType.Service);

            customer.SaleInformation.VATNumber = vatNumber;
            customerDao.UpdateCustomerVat(customer.ID, customer.SystemName, customer.SaleInformation.VATNumber);

            return true;
        }

        public bool RemoveVatNumber(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            customer.SaleInformation.VATNumber = "";
            customerDao.UpdateCustomerVat(customer.ID, customer.SystemName, "");

            return true;
        }

        public bool AcceptCustomer(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            customer.Scrutiny.InScrutiny = false;

            

            customerDao.AcceptCustomerInUserActions(customer.ID, customer.SystemName, customer.JDEAccountNumber);
            customer.StatusLog.AcceptedInDedupe = DateTime.Now;
            customer.Queue = QueueType.Accepted;

            return true;
        }

        public bool RejectCustomer(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            customer.Scrutiny.InScrutiny = false;
            customerDao.RejectCustomerInUserActions(customer.ID, customer.SystemName,
                                                   customer.SaleInformation.SalesPerson);

            customer.StatusLog.RejectedFromDedupe = DateTime.Now;
            customer.Queue = QueueType.Rejected;

            return true;
        }

        public void CommitCustomer(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            if (customer.Queue == QueueType.Accepted)
            {
                customerDao.CommitAcceptedCustomer(customer.ID, customer.SystemName);
            }
            else
            {
                customerDao.CommitRejectedCustomer(customer.ID, customer.SystemName);

                UserAction userAction = customerDao.GetUserActionForCurrentUser(customer.ID, customer.SystemName);

                customerDao.InsertRejection(customer.ID, customer.SystemName, userAction.reason, userAction.recipient,
                                            userAction.cc, userAction.body, userAction.sendNotification ?? false);

                if (customer.Email.Sent)
                {
                    Email.Send(customer);
                }
            }

            if (!string.IsNullOrEmpty(customer.JDEAccountNumber))
            {
                // @DG:
                // Calls private function to fire a stored procedure to set the JDEAccountNumber.
                SqlDataAccess.UpdateJDEAccountNumber(customer.ID, customer.SystemName, customer.JDEAccountNumber);
            }

            customersAll.Remove(customer);

        }

        public void ResetCustomer(Customer customer)
        {
            TraceHelper.Trace(TraceType.Service);

            customerDao.DeleteCustomerInUserActions(customer.ID, customer.SystemName);

            customer.JDEAccountNumber = "";
            customer.Scrutiny.InScrutiny = true;

            Customer.SetCustomerQueue(customer, customerDao.GetAllUserActions(), reasonsList);

        }


        public List<Email> GetRejectionEmailsSnapshot()
        {
            TraceHelper.Trace(TraceType.Service);

            return customerDao.GetRejectionEmails(SNAPSHOT_QUANTITY);
        }

        public List<Legal> GetLegalHistorySnapshot()
        {
            TraceHelper.Trace(TraceType.Service);

            return customerDao.GetLegalHistory(SNAPSHOT_QUANTITY);
        }


        public void AddRejectionEmail(Email email)
        {
            TraceHelper.Trace(TraceType.Service);

            customerDao.InsertRejection(email.CustomerID, email.SystemName, email.Reason, email.To, email.CC.FullCCListConcatenated, email.Body, email.Sent);
        }

        public event DrillDownEventHandler OnDrillDown;

        public void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.Service);

            selectedCustomer = (Customer) sender;

            DrillDownEventHandler handler = OnDrillDown;
            if (handler != null) handler(sender);

        }

        public void Dispose()
        {
            selectedCustomer.OnDrillDown -= DrillDown;
            customersAll.ForEach(c => c.OnDrillDown -= DrillDown);
        }
    }
}
