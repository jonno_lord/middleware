﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Service
{
    public class OrderService : IOrderService, IDisposable
    {
        private readonly OrderDao orderDao;

        private List<Order> orders;

        public OrderService(List<string> searchTypes) : this(new OrderDao(searchTypes))
        {
            
        }

        public OrderService(IOrderDao orderDao)
        {
            TraceHelper.Trace(TraceType.Service);

            this.orderDao = (OrderDao)orderDao;
            this.orders = new List<Order>();
        }

        public List<Order> SearchOrders(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Service);

            orders.ForEach(c => c.OnDrillDown -= OnDrillDown);

            orders = orderDao.SearchOrders(searchInfo);

            orders.ForEach(c=>c.OnDrillDown += OnDrillDown);

            return orders;
        }

        public DataSet GetOrderDetails(Order order)
        {
            TraceHelper.Trace(TraceType.Service);

            return orderDao.GetOrderDetails(order);
        }

        public event DrillDownEventHandler OnDrillDown;


        public void Dispose()
        {
            orders.ForEach(c => c.OnDrillDown -= OnDrillDown);
        }
    }
}
