﻿using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Receipt;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Service
{
    public class ReceiptService : IReceiptService, IDisposable
    {
        private readonly IReceiptDao receiptDao;

        private List<Receipt> receipts;



        public event DrillDownEventHandler OnDrillDown;

        public ReceiptService() : this(new ReceiptDao())
        {
        }

        public ReceiptService(IReceiptDao receiptDao)
        {
            TraceHelper.Trace(TraceType.Service);
            
            this.receiptDao = receiptDao;
            this.receipts = new List<Receipt>();
        }

        public List<Receipt> SearchReceipts(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Service);

            receipts.ForEach(c => c.OnDrillDown -= OnDrillDown);

            receipts = receiptDao.SearchReceipts(searchInfo);

            receipts.ForEach(c => c.OnDrillDown += OnDrillDown);

            return receipts;
        }

        public List<Batch> GetBatches()
        {
            TraceHelper.Trace(TraceType.Service);

            return receiptDao.GetBatches();
        }

        public List<LineDetail> GetReceiptLineDetails(int receiptNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Service);

            return receiptDao.GetReceiptLineDetails(receiptNumber, systemName);
        }

        public bool PrepareBatchToSendToJde(Batch batch)
        {
            TraceHelper.Trace(TraceType.Service);

            GenerateReceiptNumbersForBatch(batch);

            bool receiptNumberAllocated = receiptDao.BatchHasReceiptNumber(batch.ID, batch.SystemName);

            if (receiptNumberAllocated)
            {
                receiptDao.SelectReceiptBatch(batch.ID, batch.SystemName);
            }
            else
            {
                return false;
            }

            return true;
        }

        public bool ReceiptsSentToJde(Batch batch)
        {
            TraceHelper.Trace(TraceType.Service);

            bool sentToJde = receiptDao.BatchSentToJde(batch.ID, batch.SystemName);

            return sentToJde;
        }


        private void GenerateReceiptNumbersForBatch(Batch batch)
        {
            TraceHelper.Trace(TraceType.Service);

            List<int> orderUrns = receiptDao.GetOrdersNumbersInBatch(batch.ID, batch.SystemName);

            foreach (int orderUrn in orderUrns)
            {
                receiptDao.AllocateReceiptNumberToOrder(orderUrn, batch.SystemName, receiptDao.GetIncrementedReceiptNumber(batch.SystemName), DateTime.Now);
            }

        }


        public void CreateReceiptCopy(Receipt receipt)
        {
            receiptDao.InsertReceiptCopy(int.Parse(receipt.ReceiptNumber), receipt.SystemName);
        }

        public void Dispose()
        {
            receipts.ForEach(c => c.OnDrillDown -= OnDrillDown);
        }
    }
}
