﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Service
{
    public class SystemService : ISystemService
    {
        private readonly SystemDao systemDao;

        public SystemService() : this(new SystemDao())
        {
            
        }

        public SystemService(ISystemDao systemDao)
        {
            TraceHelper.Trace(TraceType.Service);
             
            this.systemDao = (SystemDao)systemDao;
        }

        public List<SalesSystem> GetSalesSystems()
        {
            TraceHelper.Trace(TraceType.Service);

            return systemDao.GetSalesSystems();
        }

        public int GetJobIdFromString(string name)
        {
            TraceHelper.Trace(TraceType.Service);

            return systemDao.GetJobIdFromString(name);
        }

        public int GetJobIdFromJobName(string name)
        {
            TraceHelper.Trace(TraceType.Service);

            return systemDao.GetJobIdFromJobName(name);
        }

        public string GetJobShortNameFromId(int id)
        {
            TraceHelper.Trace(TraceType.Service);

            return systemDao.GetJobShortNameFromId(id);
        }


        public int GetJobStepsCount(int jobId)
        {
            TraceHelper.Trace(TraceType.Service);

            return systemDao.GetJobStepsCount(jobId);
        }
    }
}
