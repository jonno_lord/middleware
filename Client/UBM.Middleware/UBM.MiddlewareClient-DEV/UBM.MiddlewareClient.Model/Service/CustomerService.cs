﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Service
{
    public class CustomerService : ICustomerService, IDisposable
    {
        private readonly ICustomerDao customerDao;

        private List<Customer> customers;
        private List<Email> emails;
        private List<Legal> legal;

        public CustomerService(List<string> searchTypes) : this(new CustomerDao(searchTypes))
        {
            TraceHelper.Trace(TraceType.Service);

            emails = new List<Email>();
            legal = new List<Legal>();
            customers = new List<Customer>();
        }

        public CustomerService(ICustomerDao customerDao)
        {
            this.customerDao = customerDao;
        }

        public Customer GetCustomer(int id, string systemName)
        {
            TraceHelper.Trace(TraceType.Service);

            return customerDao.GetCustomer(id, systemName);
        }

        public List<Customer> SearchCustomers(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Service);

            customers.ForEach(c => c.OnDrillDown -= OnDrillDown);

            customers = customerDao.SearchCustomers(searchInfo);

            customers.ForEach(c => c.OnDrillDown += OnDrillDown);

            return customers;
        }

        public List<Email> SearchEmails(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Service);

            emails.ForEach(c => c.OnDrillDown -= OnDrillDown);

            emails = customerDao.SearchEmails(searchInfo);

            emails.ForEach(c => c.OnDrillDown += OnDrillDown);

            return emails;
        }

        public List<Legal> SearchLegal(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Service);

            legal.ForEach(c => c.OnDrillDown -= OnDrillDown);

            legal = customerDao.SearchLegal(searchInfo);

            legal.ForEach(c => c.OnDrillDown += OnDrillDown);

            return legal;
        }

        public List<Rejection> GetRejectionReasons()
        {
            TraceHelper.Trace(TraceType.Service);

            return customerDao.GetRejectionReasons();
        }

        public void AddRejectionEmail(Email email)
        {
            TraceHelper.Trace(TraceType.Service);

            customerDao.InsertRejection(email.CustomerID, email.SystemName, email.Reason, email.To, email.CC.FullCCListConcatenated, email.Body, email.Sent);
        }


        public event DrillDownEventHandler OnDrillDown;

        public void Dispose()
        {
            legal.ForEach(c => c.OnDrillDown -= OnDrillDown);
            emails.ForEach(c => c.OnDrillDown -= OnDrillDown);
            customers.ForEach(c => c.OnDrillDown -= OnDrillDown);
        }
    }
}
