﻿using System;
using System.Collections.Generic;
using System.Linq;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Invoicing;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Service
{
    public class InvoiceService : IInvoiceService
    {
        private readonly InvoiceDao invoiceDao;
        

        public const string ASOP_UK = "ASOPUK";
        public const string ASOP_DALTONS = "ASOPDaltons";
        public const string ASOP_HOLLAND = "ASOPHolland";

        private readonly Dictionary<string, List<Publication>> cache;

        private void AddToCache(string systemName, List<Publication> publications, bool includeOutstanding)
        {
            string suffix = GetKeySuffix(includeOutstanding);
            if (!cache.ContainsKey(systemName + suffix))
                cache.Add(systemName + GetKeySuffix(includeOutstanding), publications);
        }

        private List<Publication> GetFromCache(string systemName, bool includeOutstanding)
        {
            string suffix = GetKeySuffix(includeOutstanding);
            if (!cache.ContainsKey(systemName + suffix))
                return null;
            
            return cache[systemName + suffix];
        }

        private static string GetKeySuffix(bool includeOutstanding)
        {
            return includeOutstanding ? "-outstanding" : "-recent";
        }

        public InvoiceService() : this(new InvoiceDao())
        {
            
        }

        public InvoiceService(IInvoiceDao invoiceDao)
        {
            TraceHelper.Trace(TraceType.Service);

            cache = new Dictionary<string, List<Publication>>();
            this.invoiceDao = (InvoiceDao)invoiceDao;

            Publication.SetDataAccess(invoiceDao);
        }

        public void InsertPublicationInvoicingParameters(string titleCard, DateTime insertDate, string systemName)
        {
            TraceHelper.Trace(TraceType.Service);
             
            invoiceDao.InsertPublicationInvoicingParameters(titleCard, insertDate, systemName);
        }

        public int GetBatchNumber(Invoice invoice)
        {
            TraceHelper.Trace(TraceType.Service);

            return invoiceDao.GetBatchNumber(invoice);
        }

        public void ClearCache()
        {
            cache.Clear();
        }

        public List<Publication> GetPublications(string systemName, bool includeOutstanding)
        {
            List<Publication> cached = GetFromCache(systemName, includeOutstanding);

            if (cached != null)
                return cached;
                     
            List<Warning> warnings = Warning.CreateWarnings(systemName, !includeOutstanding);
            List<Publication> publications = Publication.CreatePublications(systemName, warnings, !includeOutstanding).ToList();

            AddToCache(systemName, publications, includeOutstanding);

            return publications;
        }

        public List<Invoice> GetInvoices(string systemName, Publication publication, bool showOutstandingInvoices)
        {
            List<Warning> warnings = Warning.CreateWarningsForSinglePublication(systemName, publication.Description);
            List<Invoice> invoices = Publication.CreatePublication(systemName, publication, warnings).Invoices.ToList();

            if(!showOutstandingInvoices)
            {
                List<Invoice> recent = invoices.Where(c => !c.OutstandingFlag && !c.FutureFlag).ToList();
                return recent;
            }

            return invoices;
        }


    }
}
