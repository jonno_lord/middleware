﻿using UBM.MiddlewareScheduler;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace SchedulerTestProject
{
    
    
    /// <summary>
    ///This is a test class for ScheduleTest and is intended
    ///to contain all ScheduleTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ScheduleTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Schedule Constructor
        ///</summary>
        [TestMethod()]
        public void ScheduleConstructorTest()
        {
            string jobid = "1";             
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = string.Empty; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday);
            Assert.IsTrue(target != null);
        }

        /// <summary>
        ///A test for DayOfWeekIsToday
        ///</summary>
        [TestMethod()]
        public void DayOfWeekIsTodayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = string.Empty; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.DayOfWeekIsToday();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FrequencyIsValid
        ///</summary>
        [TestMethod()]
        public void FrequencyIsValidTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = string.Empty; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            target.LastExecuted = DateTime.Now.AddSeconds(-300);
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.FrequencyIsValid();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for TimeIsValid
        ///</summary>
        [TestMethod()]
        public void TimeIsValidTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.TimeIsValid();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Description
        ///</summary>

        /// <summary>
        ///A test for Disabled
        ///</summary>
        [TestMethod()]
        public void DisabledTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Disabled = expected;
            actual = target.Disabled;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EndDate
        ///</summary>
        [TestMethod()]
        public void EndDateTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = "2010-01-01"; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            Nullable<DateTime> expected = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> actual;
            target.EndDate = expected;
            actual = target.EndDate;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EndTime
        ///</summary>
        [TestMethod()]
        public void EndTimeTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.EndTime = expected;
            actual = target.EndTime;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Frequency
        ///</summary>
        [TestMethod()]
        public void FrequencyTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            int expected = 300; // TODO: Initialize to an appropriate value
            int actual;
            target.Frequency = expected;
            actual = target.Frequency;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Friday
        ///</summary>
        [TestMethod()]
        public void FridayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Friday = expected;
            actual = target.Friday;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for JobId
        ///</summary>
        [TestMethod()]
        public void JobIdTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            int expected = 1; // TODO: Initialize to an appropriate value
            int actual;
            target.JobId = expected;
            actual = target.JobId;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for LastExecuted
        ///</summary>
        [TestMethod()]
        public void LastExecutedTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            Nullable<DateTime> expected = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> actual;
            expected = DateTime.Parse("2010-01-01");
            target.LastExecuted = DateTime.Parse("2010-01-01");
            target.LastExecuted = expected;
            actual = target.LastExecuted;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Monday
        ///</summary>
        [TestMethod()]
        public void MondayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Monday = expected;
            actual = target.Monday;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Saturday
        ///</summary>
        [TestMethod()]
        public void SaturdayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Saturday = expected;
            actual = target.Saturday;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ScheduleId
        ///</summary>
        [TestMethod()]
        public void ScheduleIdTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.ScheduleId = expected;
            actual = target.ScheduleId;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for StartDate
        ///</summary>
        [TestMethod()]
        public void StartDateTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = "2010-01-01"; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            Nullable<DateTime> expected = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> actual;
            expected = DateTime.Parse("2010-01-01");
            target.StartDate = expected;
            actual = target.StartDate;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for StartTime
        ///</summary>
        [TestMethod()]
        public void StartTimeTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            string expected = "00:00:00"; // TODO: Initialize to an appropriate value
            string actual;
            target.StartTime = expected;
            actual = target.StartTime;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Sunday
        ///</summary>
        [TestMethod()]
        public void SundayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Sunday = expected;
            actual = target.Sunday;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Thursday
        ///</summary>
        [TestMethod()]
        public void ThursdayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Thursday = expected;
            actual = target.Thursday;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Tuesday
        ///</summary>
        [TestMethod()]
        public void TuesdayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Tuesday = expected;
            actual = target.Tuesday;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Wednesday
        ///</summary>
        [TestMethod()]
        public void WednesdayTest()
        {
            string jobid = "1";
            string scheduleid = "3"; // TODO: Initialize to an appropriate value
            string description = "Test description"; // TODO: Initialize to an appropriate value
            string startdate = string.Empty; // TODO: Initialize to an appropriate value
            string starttime = "00:00:00"; // TODO: Initialize to an appropriate value
            string enddate = string.Empty; // TODO: Initialize to an appropriate value
            string endtime = "23:59:00"; // TODO: Initialize to an appropriate value
            string disabled = "False"; // TODO: Initialize to an appropriate value
            string frequency = "300"; // TODO: Initialize to an appropriate value
            string monday = "False"; // TODO: Initialize to an appropriate value
            string tuesday = "False"; // TODO: Initialize to an appropriate value
            string wednesday = "False"; // TODO: Initialize to an appropriate value
            string thursday = "False"; // TODO: Initialize to an appropriate value
            string friday = "False"; // TODO: Initialize to an appropriate value
            string saturday = "False"; // TODO: Initialize to an appropriate value
            string sunday = "False"; // TODO: Initialize to an appropriate value
            Schedule target = new Schedule(jobid, scheduleid, description, startdate, starttime, enddate, endtime, disabled, frequency, monday, tuesday, wednesday, thursday, friday, saturday, sunday); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Wednesday = expected;
            actual = target.Wednesday;
            Assert.AreEqual(expected, actual);
        }
    }
}
