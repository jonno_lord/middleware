﻿using UBM.MiddlewareScheduler;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SchedulerTestProject
{
    
    
    /// <summary>
    ///This is a test class for SchedulerProcessTest and is intended
    ///to contain all SchedulerProcessTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SchedulerProcessTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SchedulerProcess Constructor
        ///</summary>
        [TestMethod()]
        public void SchedulerProcessConstructorTest()
        {
            SchedulerProcess target = new SchedulerProcess();
            Assert.IsTrue(target != null);
        }

        /// <summary>
        ///A test for ExecuteJob
        ///</summary>
        [TestMethod()]
        [DeploymentItem("MiddlewareScheduler.exe")]
        public void ExecuteJobTest()
        {
            SchedulerProcess_Accessor target = new SchedulerProcess_Accessor(); // TODO: Initialize to an appropriate value
            int jobid = 12; // TODO: Initialize to an appropriate value
            target.ExecuteJob(jobid);
            Assert.IsTrue(true, "This method will fail if the the website is not available");
        }

        /// <summary>
        ///A test for ExecuteSchedules
        ///</summary>
        [TestMethod()]
        public void ExecuteSchedulesTest()
        {
            SchedulerProcess target = new SchedulerProcess(); // TODO: Initialize to an appropriate value
            target.ExecuteSchedules();
            Assert.IsTrue(true, "This method will fail if the the website is not available");
        }

        /// <summary>
        ///A test for GetSchedulesFromXML
        ///</summary>
        [TestMethod()]
        [DeploymentItem("MiddlewareScheduler.exe")]
        public void GetSchedulesFromXMLTest()
        {
            SchedulerProcess_Accessor target = new SchedulerProcess_Accessor(); // TODO: Initialize to an appropriate value
            IList<Schedule> actual;
            actual = target.GetSchedulesFromXML();
            Assert.IsTrue(actual.Count > 0, "actual loaded in at least 1 schedule");
        }

        /// <summary>
        ///A test for LoadSchedules
        ///</summary>
        [TestMethod()]
        public void LoadSchedulesTest()
        {
            SchedulerProcess target = new SchedulerProcess(); // TODO: Initialize to an appropriate value
            target.LoadSchedules();
            Assert.IsTrue(true, "Load Schedules will raise an exception if XML is invalid");
        }

        /// <summary>
        ///A test for SaveExecutionsToXML
        ///</summary>
        [TestMethod()]
        [DeploymentItem("MiddlewareScheduler.exe")]
        public void SaveExecutionsToXMLTest()
        {
            SchedulerProcess_Accessor target = new SchedulerProcess_Accessor(); // TODO: Initialize to an appropriate value
            target.SaveExecutionsToXML();
            Assert.IsTrue(true, "SaveExecutions will raise an exception if unable to write.");
        }

        /// <summary>
        ///A test for SetLastExecutionsFromXML
        ///</summary>
        [TestMethod()]
        [DeploymentItem("MiddlewareScheduler.exe")]
        public void SetLastExecutionsFromXMLTest()
        {
            SchedulerProcess_Accessor target = new SchedulerProcess_Accessor(); // TODO: Initialize to an appropriate value
            IList<Schedule> schedulelisting = null; // TODO: Initialize to an appropriate value
            IList<Schedule> expected = null; // TODO: Initialize to an appropriate value
            IList<Schedule> actual;
            actual = target.SetLastExecutionsFromXML(schedulelisting);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Schedules
        ///</summary>
        [TestMethod()]
        [DeploymentItem("MiddlewareScheduler.exe")]
        public void SchedulesTest()
        {
            SchedulerProcess_Accessor target = new SchedulerProcess_Accessor(); // TODO: Initialize to an appropriate value
            IList<Schedule> expected = new List<Schedule>(); // TODO: Initialize to an appropriate value
            expected.Add(new Schedule("0", "1", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
            IList<Schedule> actual;
            target.Schedules = expected;
            actual = target.Schedules;
            Assert.AreEqual(expected, actual);
        }
    }
}
