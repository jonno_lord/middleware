﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UBM.MiddlewareScheduler;

namespace MiddlewareSchedulerTestBench
{
    public partial class TestbenchMain : Form
    {
        public TestbenchMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SchedulerProcess sp = new SchedulerProcess();
            sp.LoadSchedules();
        }
    }
}
