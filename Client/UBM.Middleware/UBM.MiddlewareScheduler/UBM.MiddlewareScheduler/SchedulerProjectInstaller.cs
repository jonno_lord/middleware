﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace UBM.MiddlewareScheduler
{
    [RunInstaller(true)]
    public partial class SchedulerProjectInstaller : System.Configuration.Install.Installer
    {

        /// <summary>
        /// Installer
        /// </summary>
        public SchedulerProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
