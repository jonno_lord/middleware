﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UBM;
// using UBM.Logger; 
// Currently removed to reduce complexity of deployment.
using System.Diagnostics;

namespace UBM.MiddlewareScheduler
{
    /// <summary>
    /// deals with processing the XML and determining which jobs should
    /// be executed. Also deal with saving to another XML file the last execution time 
    /// of a specific schedule.
    /// </summary>
    public class SchedulerProcess
    {

        #region Privates Members
        // private property to store the list of schedules.
        private IList<Schedule> Schedules { get; set; }
        //private Log log = new Log();

        //private BusinessProcess businessprocess = new BusinessProcess("Middleware Scheduler", "SchedulerProcess", "");
        #endregion

        #region Private Methods

        // Write out the XML with the Last Execution status for each schedule
        private void SaveExecutionsToXML()
        {

            //-- check to see if there are no schedules loaded
            if (Schedules == null) return;

            //businessprocess.Step = "determining schedule validity";
            //log.Information("Analysing Last executed schedules", businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Analysing Last executed schedules", EventLogEntryType.Information);

            //-- create a new Xml document, and write out all the schedule settings
            XDocument xml = new XDocument();
            XElement xschedule = new XElement("executions");

            foreach (Schedule s in Schedules)
            {
                // only save schedules that have been executed to the XML document.
                if (s.LastExecuted != null)
                {
                    xschedule.Add(new XElement("execution",
                            new XElement("scheduleid", s.ScheduleId.ToString()),
                            new XElement("description", s.Description.ToString()),
                            new XElement("starttime", s.StartTime.ToString()),
                            new XElement("endtime", s.EndTime.ToString()),
                            new XElement("frequency", s.Frequency.ToString()),
                            new XElement("jobid", s.JobId.ToString()),
                            new XElement("lastexecution", s.LastExecuted.ToString())));
                }
            }
            xml.Add(xschedule);


            //businessprocess.Step = "Saving schedule last execution";
            //log.Information("Saving Last executed schedules", businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Saving Last executed schedules", EventLogEntryType.Information);


            //TODO: read this from config
            xml.Save(SchedulerCommonFunctions.LastExecutedFileNameAndPath);
        }

        // set the last executed date and time for each schedule
        private IList<Schedule> SetLastExecutionsFromXML(IList<Schedule> schedulelisting)
        {

            if (schedulelisting == null) return null;

            //businessprocess.Step = "Reading last execution status";
            //log.Information("Reading Last executed schedules", businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Reading Last executed schedules", EventLogEntryType.Information);

            //TODO: read this from config
            if (!System.IO.File.Exists(SchedulerCommonFunctions.LastExecutedFileNameAndPath))
                return schedulelisting;

            //TODO: read this from config
            XDocument feedXML = XDocument.Load(SchedulerCommonFunctions.LastExecutedFileNameAndPath);
            var xmlexecutions = from execution in feedXML.Descendants("execution")
                                select new
                                {
                                    scheduleid = int.Parse(execution.Element("scheduleid").Value),
                                    lastexecution = execution.Element("lastexecution").Value
                                };

            // now we have read in all last execution details from the XML, match with the
            // schedules, so each one knows when it last executed. (Note: This is not
            // elegant - would like to do this using Linq).
            foreach (var execution in xmlexecutions)
            {


                foreach (Schedule schedule in schedulelisting)
                {
                    // if the schedule is the one in the LastExecution xml config
                    // then set the lastExecuted property to the one stored in the XML
                    if (schedule.ScheduleId == execution.scheduleid)
                    {
                        //businessprocess.Step = "setting schedule last execution";
                        //log.Information("Found last execution of schedule " + schedule.Description, businessprocess, null);
                        SchedulerCommonFunctions.LogEvent("Found last execution of schedule " + schedule.Description, EventLogEntryType.Information);

                        schedule.LastExecuted = DateTime.Parse(execution.lastexecution);
                    }
                }
            }

            return schedulelisting;

        }

        // load in the XML schedule file
        private IList<Schedule> GetSchedulesFromXML()
        {

            //businessprocess.Step = "reading schedule execution";
            //log.Information("Reading Schedule configuration", businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Reading Schedule configuration", EventLogEntryType.Information);

            //TODO: Read this from Config
            if (!System.IO.File.Exists(SchedulerCommonFunctions.SchedulerFileNameAndPath))
            {
                SchedulerCommonFunctions.LogEvent("Scheduler file " + SchedulerCommonFunctions.SchedulerFileNameAndPath + " does not exist", EventLogEntryType.Warning);
                return null;
            }

            //TODO: Read this from Config
            XDocument feedXML = XDocument.Load(SchedulerCommonFunctions.SchedulerFileNameAndPath);

            // Read the XML and create new Schedule objects. There must be a 
            // prettier way to this rather than use a huge constructor - 
            // however at least because this is .net 4 we can use named
            // parameters
            var xmlschedules = from feed in feedXML.Descendants("schedule")
                               select new Schedule(
                                       jobid: feed.Element("jobid").Value,
                                       scheduleid: feed.Element("scheduleid").Value,
                                       description: feed.Element("description").Value,
                                       startdate: feed.Element("startdate").Value,
                                       starttime: feed.Element("starttime").Value,
                                       enddate: feed.Element("enddate").Value,
                                       endtime: feed.Element("endtime").Value,
                                       disabled: feed.Element("disabled").Value,
                                       frequency: feed.Element("frequency").Value,
                                       monday: feed.Element("monday").Value,
                                       tuesday: feed.Element("tuesday").Value,
                                       wednesday: feed.Element("wednesday").Value,
                                       thursday: feed.Element("thursday").Value,
                                       friday: feed.Element("friday").Value,
                                       saturday: feed.Element("saturday").Value,
                                       sunday: feed.Element("sunday").Value
                                       );

            //businessprocess.Step = "read schedule execution";
            //log.Information("Read in " + xmlschedules.Count<Schedule>().ToString() + " schedules",
            //                businessprocess, null);

            if (xmlschedules != null)
            {
                SchedulerCommonFunctions.LogEvent("Read in " + xmlschedules.Count<Schedule>().ToString() + " schedules", xmlschedules.Count<Schedule>() == 0 ? EventLogEntryType.Warning : EventLogEntryType.Information);
            }
            else
            {
                SchedulerCommonFunctions.LogEvent("No Schedules read in. ", EventLogEntryType.Warning);
            }

            return xmlschedules.ToList<Schedule>();

        }

        // fire a specific job
        private void ExecuteJob(int jobid)
        {
            // old fashioned webservice call. this should be done as a service reference,
            // but this way of executing the job is much simplier (as its two lines of code). 

            //businessprocess.Step = "execution of job";
            //log.Information("Executing Schedule " + jobid.ToString() + " at " + DateTime.Now.ToShortDateString(), businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Executing Schedule " + jobid.ToString() + " at " + DateTime.Now.ToShortDateString(), EventLogEntryType.Information);

            // execute the job
            try
            {
                UBM.Middleware.JobsService.JobService jobservice = new UBM.Middleware.JobsService.JobService();
                jobservice.StartJob(jobid);
            }
            catch (Exception ex)
            {
                SchedulerCommonFunctions.LogEvent(ex.Message, EventLogEntryType.Error);
                //log.Error("", ex, businessprocess);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sets up the scheduler process object (by loading the schedule in the schedules
        /// IList property. (see privates region in this class)
        /// </summary>
        public SchedulerProcess()
        {
        }

        /// <summary>
        /// Loads in the Schedules from the XML file into a IList structure of Schedule
        /// objects. Once the last has been loaded, it then loads the last execution time
        /// for each of the schedules - otherwise if the service stopped and started 
        /// it would not be able to calculate which job recently executed
        /// </summary>
        public void LoadSchedules()
        {

            //businessprocess.Step = "Loading Schedules";
            //log.Information("Creating schedule information", businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Creating schedule information", EventLogEntryType.Information);

            // read the schedules structure from the XML file
            Schedules = GetSchedulesFromXML();
            // after schedules have been read in, cross reference them
            // with the LastExecuted XML file, so then we know for each
            // schedule when it was last executed.
            Schedules = SetLastExecutionsFromXML(Schedules);

        }

        /// <summary>
        /// iterate through the schedules and execute ones that meet the criteria. This
        /// means respecting the days of the week, disabled flag, start and end dates
        /// and start and end times. These are set specifically per Schedule through the
        /// Schedule.XML file - loaded at the instantiation (or constructor) part of this
        /// class.
        /// </summary>
        public void ExecuteSchedules()
        {
            // if there are no schedules - do not process.
            if (Schedules == null)
            {
                SchedulerCommonFunctions.LogEvent("No Schedules Exist.", EventLogEntryType.Information);
                return;
            }

            // determine valid schedules through a mixture of Linq queries and methods
            // in the schedule class (DayOfWeekIsToday, TimeIsValid and FrequencyIsValid)
            // businessprocess.Step = "determining valid schedules";
            //log.Information("Executing Linq schedule information", businessprocess, null);
            SchedulerCommonFunctions.LogEvent("Executing Linq schedule information", EventLogEntryType.Information);

            var validSchedules = from schedule in Schedules
                                 where
                                     schedule.Disabled == false &&
                                     schedule.DayOfWeekIsToday() &&
                                     DateTime.Now >= schedule.StartDate &&
                                     (schedule.EndDate >= DateTime.Now || schedule.EndDate == null)
                                     && schedule.TimeIsValid() &&
                                     schedule.FrequencyIsValid()
                                 select schedule;

            // iterate through each valid schedule, execute it and Save its last execution
            // date and time to the LastExecution XML file.
            foreach (var validschedule in validSchedules)
            {
                //businessprocess.Step = "executing valid schedules";
                //log.Information("Found valid schedule" + validschedule.Description, businessprocess, null);
                SchedulerCommonFunctions.LogEvent("Found valid schedule" + validschedule.Description, EventLogEntryType.Information);

                validschedule.LastExecuted = DateTime.Now;
                this.ExecuteJob(validschedule.JobId);       // call the webservice
                this.SaveExecutionsToXML();                 // write all lastexecuted schedules to xml
            }
        }

        #endregion

    }
}
