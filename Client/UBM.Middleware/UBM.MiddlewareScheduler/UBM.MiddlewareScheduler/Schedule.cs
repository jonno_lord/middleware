﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareScheduler
{
    /// <summary>
    /// </summary>
    public class Schedule
    {
        #region Public properties

        /// <summary>
        /// Unique Id for the schedule
        /// </summary>
        public int ScheduleId { get; set; }
        /// <summary>
        /// Job that the schedule relates to
        /// </summary>
        public int JobId { get; set; }
        /// <summary>
        /// Full description of the schedule (e.g. Weekend Schedule)
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Starting date of the Schedule
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Starting time of the schedule
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// EndDate of the schedule
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// The last time and date this shcedule was executed
        /// </summary>
        public DateTime? LastExecuted { get; set; }

        /// <summary>
        /// The last date this schedule will activate Jobs until,
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// Whether this schedule is disabled or not
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// The frequency (in S) that the schedule should execute at
        /// </summary>
        public int Frequency { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Monday
        /// </summary>
        public bool Monday { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Tuesday
        /// </summary>
        public bool Tuesday { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Wednesday
        /// </summary>
        public bool Wednesday { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Thursday
        /// </summary>
        public bool Thursday { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Friday
        /// </summary>
        public bool Friday { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Saturday
        /// </summary>
        public bool Saturday { get; set; }

        /// <summary>
        /// Whether this job should exeute on a Sunday
        /// </summary>
        public bool Sunday { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Checks to see if this job is valid to execute, as the amount of time since it last
        /// executed is now less than the current time.
        /// </summary>
        /// <returns>whether the last execution time + the frequency is less than the current time</returns>
        public bool FrequencyIsValid()
        {

            if (LastExecuted == null)
                return true;

            DateTime lastExec = DateTime.Parse(this.LastExecuted.ToString());

            if (lastExec.AddSeconds(this.Frequency) < DateTime.Now)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks to see if the current time falls within the schedules Starting and End times 
        /// therefore making it a valid schedule
        /// </summary>
        /// <returns>if the current time falls within the starting and ending times of the schedule</returns>
        public bool TimeIsValid()
        {
            //-- if no start time and no end time is specified, its valid.
            if (this.StartTime.Length == 0 && this.EndTime.Length == 0)
                return true;

            //-- check for start time specified, but no end
            if (this.StartTime.Length > 0 && this.EndTime.Length == 0)
            {
                if (DateTime.Now.TimeOfDay >= TimeSpan.Parse(this.StartTime))
                    return true;
                else
                    return false;
            }

            //-- check for end time, but no start specified
            if (this.StartTime.Length == 0 && this.EndTime.Length > 0)
            {
                if (DateTime.Now.TimeOfDay <= TimeSpan.Parse(this.EndTime))
                    return true;
                else
                    return false;
            }

            //-- check for both start and end times specified
            if (DateTime.Now.TimeOfDay >= TimeSpan.Parse(this.StartTime) &&
                DateTime.Now.TimeOfDay <= TimeSpan.Parse(this.EndTime))
                return true;
            else
                return false;

        }

        /// <summary>
        /// Checks to see if the day of the week the schedule is set to, matches the current day
        /// of the week
        /// </summary>
        /// <returns>whether the schedule is Valid based on which day of the week it is set to execute on</returns>
        public bool DayOfWeekIsToday()
        {
            DayOfWeek today = DateTime.Now.DayOfWeek;

            // determine if the day of the week this schedule matches the day of the week
            // specified through the XML.
            switch (today)
            {
                case DayOfWeek.Monday:
                    if (this.Monday) return true;
                    break;
                case DayOfWeek.Tuesday:
                    if (this.Tuesday) return true;
                    break;
                case DayOfWeek.Wednesday:
                    if (this.Wednesday) return true;
                    break;
                case DayOfWeek.Thursday:
                    if (this.Thursday) return true;
                    break;
                case DayOfWeek.Friday:
                    if (this.Friday) return true;
                    break;
                case DayOfWeek.Saturday:
                    if (this.Saturday) return true;
                    break;
                case DayOfWeek.Sunday:
                    if (this.Sunday) return true;
                    break;
            }

            return false;
        }

        #endregion

        #region Public constructor


        /// <summary>
        /// Huge constructor used to create schedule objects. It is done this way so that I can
        /// use it easily in Linq (see LoadXML method in the SchedulerProcess). Using named
        /// parameters on the call helps the maintainibility of this method - but there must
        /// be a better way of doing this..
        /// </summary>
        /// <param name="jobid">the Id of the job to execute</param>
        /// <param name="scheduleid">the unique reference of the schedule item</param>
        /// <param name="description">the description (e.g. Afternoon Backfeed)</param>
        /// <param name="startdate">starting date of the schedule</param>
        /// <param name="starttime">starting time of the schedule</param>
        /// <param name="enddate">ending date of the schedule</param>
        /// <param name="endtime">ending time of the schedule</param>
        /// <param name="disabled">whether this schedule is disabled</param>
        /// <param name="frequency">the frequency in Seconds of the schedule</param>
        /// <param name="monday">if this job should execute on Mondays</param>
        /// <param name="tuesday">if this job should execute on Tuesdays</param>
        /// <param name="wednesday">if this job should execute on Wednesdays</param>
        /// <param name="thursday">if this job should execute on Thursdays</param>
        /// <param name="friday">if this job should execute on Fridays</param>
        /// <param name="saturday">if this job should execute on Saturdays</param>
        /// <param name="sunday">if this job should execute on Subndays</param>
        public Schedule(string jobid, string scheduleid, string description,
                        string startdate, string starttime,
                        string enddate, string endtime,
                        string disabled, string frequency,
                        string monday, string tuesday,
                        string wednesday, string thursday,
                        string friday, string saturday,
                        string sunday)
        {

            // these are all strings because they have come from XML
            this.JobId = int.Parse(jobid);
            this.Description = description;
            this.ScheduleId = int.Parse(scheduleid);
            this.StartTime = starttime;
            this.EndTime = endtime;

            if (disabled.Length  == 0) disabled = "false";            
            this.Disabled = bool.Parse(disabled);

            if (frequency.Length == 0) frequency = "500";            
            this.Frequency = int.Parse(frequency);

            if (monday.Length == 0) monday = "false";
            this.Monday = bool.Parse(monday);

            if (tuesday.Length == 0) tuesday = "false";
            this.Tuesday = bool.Parse(tuesday);

            if (wednesday.Length == 0) wednesday= "false";
            this.Wednesday = bool.Parse(wednesday);

            if(thursday.Length ==0) thursday = "false";
            this.Thursday = bool.Parse(thursday);

            if (friday.Length == 0) friday = "false";
            this.Friday = bool.Parse(friday);

            if (saturday.Length == 0) saturday = "false";
            this.Saturday = bool.Parse(saturday);

            if (sunday.Length == 0) sunday = "false";
            this.Sunday = bool.Parse(sunday);

            // start and end dates are nullable - therefore test for them here as you
            // cannot parse a null date.
            if (startdate.Length == 0)
                StartDate = null;
            else
                StartDate = DateTime.Parse(startdate);

            if (enddate.Length == 0)
                this.EndDate = null;
            else
                this.EndDate = DateTime.Parse(enddate);

        }

        #endregion

    }
}
