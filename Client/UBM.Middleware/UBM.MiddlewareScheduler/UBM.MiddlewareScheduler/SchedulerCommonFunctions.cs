﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Diagnostics;

namespace UBM.MiddlewareScheduler
{
    /// <summary>
    /// Common functionality relating to the Scheduler service. This includes
    /// Filenames and Paths for scheduler files
    /// </summary>
    public static class SchedulerCommonFunctions
    {
        private const string EVENT_LOG_NAME = "Middleware Scheduler";


        #region Public Properties

        /// <summary>
        /// stores a logs an event into the Event Log. used because as this is deployed as service
        /// it has issues with writing to text files through the logger.
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="EntryType"></param>
        public static void LogEvent(string Message, EventLogEntryType EntryType)
        {

            Properties.Settings settings = new Properties.Settings();
            string eventLogLevel = "";

            try
            {
                eventLogLevel = settings.EventLogLevel.ToLower().Trim();
            }
            catch (Exception ex)
            {
                //TODO: Handle when there is no log
                Console.WriteLine(ex.Message.ToString());
            }

            //-- if none, then do not perform any logging.
            if (eventLogLevel == "none") return;

            if (EntryType == EventLogEntryType.Error ||
                EntryType == EventLogEntryType.Warning ||
                eventLogLevel == "verbose" ||
                eventLogLevel == "all")
            {

                if (!EventLog.SourceExists(EVENT_LOG_NAME))
                    EventLog.CreateEventSource(EVENT_LOG_NAME, "Application");

                EventLog eventDetailLog = new EventLog();
                eventDetailLog.Source = EVENT_LOG_NAME;
                eventDetailLog.WriteEntry(Message, EntryType);
            }
            
        }


        /// <summary>
        /// Return the schedulers full file name and path from the App.Config file
        /// </summary>
        public static string SchedulerFileNameAndPath
        {
            get 
            {
                Properties.Settings settings = new Properties.Settings();
                return settings.SchedulerXMLConfigurationFileAndPath;
            }
        }

        /// <summary>
        /// return only the Scheduler.XML filename
        /// </summary>
        public static string SchedulerFileName
        {
            get 
            {

                string fileName = SchedulerCommonFunctions.SchedulerFileNameAndPath;
                if (fileName.LastIndexOf(@"\") > 0)
                    return fileName.Substring(fileName.LastIndexOf(@"\"));
                else
                    return "";
            }

        }

        /// <summary>
        /// Return the filePath component of the file for use with Directory watching
        /// </summary>
        public static string SchedulerFilePath
        {
            get 
            {
                string fileName = SchedulerCommonFunctions.SchedulerFileNameAndPath;
                if (fileName.LastIndexOf(@"\") > 0)
                    return fileName.Substring(0, fileName.LastIndexOf(@"\"));
                else
                    return "";
            }
        }

        /// <summary>
        /// return the last executed XML file name from the App.config ( as key SchedulerXMLLastExecutedFileAndPath)
        /// </summary>
        public static string LastExecutedFileNameAndPath
        {
            get 
            {

                Properties.Settings settings = new Properties.Settings();
                return settings.SchedulerXMLLastExecutedFileAndPath;
            }
        }

        #endregion
    }
}
