﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
//using UBM.Logger;
using System.IO;
using System.Timers;


namespace UBM.MiddlewareScheduler
{
    public partial class SchedulerService : ServiceBase
    {

        private const int INTERVAL_PERIOD = 1000; //-- check the schedule every second.

        private SchedulerProcess Scheduler { get; set; }
        //private Log log;
        private Timer timer;
        //private BusinessProcess businessprocess;
        FileSystemWatcher incoming;

        /// <summary>
        /// Constructor of the Scheduler Service Component
        /// </summary>
        public SchedulerService()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Start the Process (on Service start up)
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            
            Initialise();
            SchedulerCommonFunctions.LogEvent("Starting", EventLogEntryType.Information);
        }


        /// <summary>
        /// Initialise the process by creating a new business set up procedure
        /// </summary>
        public void Initialise()
        {
            //log = new Log();
            //businessprocess = new BusinessProcess("Scheduler Start up", "Start", "");
            //businessprocess.Step = "Initialising";
            SchedulerCommonFunctions.LogEvent("Initialising", EventLogEntryType.Information);

            try
            {
                //-- create the base parts of the process
                Scheduler = new SchedulerProcess();

                SchedulerCommonFunctions.LogEvent("Loading Schedules", EventLogEntryType.Information);
                Scheduler.LoadSchedules();

                timer = new Timer();
                //businessprocess = new BusinessProcess("Middleware Scheduler", "Scheduler Service", "");


                SchedulerCommonFunctions.LogEvent("Creating File System Watcher", EventLogEntryType.Information);
                //-- start the watcher, this will reload the schedule if someone changes the file.
                incoming = new FileSystemWatcher();
                incoming.Path = SchedulerCommonFunctions.SchedulerFilePath;

                incoming.NotifyFilter = NotifyFilters.LastAccess |
                                        NotifyFilters.LastWrite |
                                        NotifyFilters.FileName |
                                        NotifyFilters.DirectoryName;
                incoming.Filter = "Schedule.xml";

                SchedulerCommonFunctions.LogEvent("Assigning event handling to Creating File System Watcher", EventLogEntryType.Information);
                incoming.Changed += new FileSystemEventHandler(OnSchedulerChanged);
                incoming.EnableRaisingEvents = true;

                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                timer.Interval = INTERVAL_PERIOD;
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                SchedulerCommonFunctions.LogEvent(ex.Message, EventLogEntryType.Error);
                //log.Error("Initialising", ex, businessprocess);
            }
            SchedulerCommonFunctions.LogEvent("Initialise has completed", EventLogEntryType.Information);
        }
        

        /// <summary>
        /// This event fires when an update has occurred in the XML configuration file
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnSchedulerChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                //businessprocess.Step = "XML config file changed contents";
                SchedulerCommonFunctions.LogEvent("XML config file has changed", EventLogEntryType.Information);
                Scheduler.LoadSchedules();
            }
            catch (Exception ex)
            {
                SchedulerCommonFunctions.LogEvent(ex.Message, EventLogEntryType.Error);
                //log.Error("", ex, businessprocess);
            }
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                timer.Enabled = false;
                //businessprocess.Step = "executing waiting schedules";
                SchedulerCommonFunctions.LogEvent("Executing schedules", EventLogEntryType.Information);
                Scheduler.ExecuteSchedules();
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                SchedulerCommonFunctions.LogEvent(ex.Message, EventLogEntryType.Error);
                //log.Error("", ex, businessprocess);
            }
        }

        /// <summary>
        /// Actions to perform when the process completes.
        /// </summary>
        protected override void OnStop()
        {
            SchedulerCommonFunctions.LogEvent("Stop", EventLogEntryType.Information);
        }
    }
}
