﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Windows.Forms;

namespace UBM.MiddlewareScheduler
{

    /// <summary>
    /// Used for testing
    /// </summary>
    public partial class TestBench : Form
    {
        /// <summary>
        /// Test bench
        /// </summary>
        public TestBench()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SchedulerService ss = new SchedulerService();
            ss.Initialise();
        }

    }
}
