﻿namespace UBM.MiddlewareClient.Tracer
{
    partial class TraceWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbWindow = new System.Windows.Forms.RichTextBox();
            this.chkViewModel = new System.Windows.Forms.CheckBox();
            this.chkService = new System.Windows.Forms.CheckBox();
            this.chkDatabase = new System.Windows.Forms.CheckBox();
            this.chkErrors = new System.Windows.Forms.CheckBox();
            this.chkWarnings = new System.Windows.Forms.CheckBox();
            this.chkFreeze = new System.Windows.Forms.CheckBox();
            this.chkInfo = new System.Windows.Forms.CheckBox();
            this.chkControllers = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbWindow
            // 
            this.rtbWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbWindow.Location = new System.Drawing.Point(12, 12);
            this.rtbWindow.Name = "rtbWindow";
            this.rtbWindow.Size = new System.Drawing.Size(842, 659);
            this.rtbWindow.TabIndex = 0;
            this.rtbWindow.Text = "";
            // 
            // chkViewModel
            // 
            this.chkViewModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkViewModel.AutoSize = true;
            this.chkViewModel.Checked = true;
            this.chkViewModel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkViewModel.ForeColor = System.Drawing.Color.Blue;
            this.chkViewModel.Location = new System.Drawing.Point(12, 696);
            this.chkViewModel.Name = "chkViewModel";
            this.chkViewModel.Size = new System.Drawing.Size(81, 17);
            this.chkViewModel.TabIndex = 1;
            this.chkViewModel.Text = "View Model";
            this.chkViewModel.UseVisualStyleBackColor = true;
            // 
            // chkService
            // 
            this.chkService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkService.AutoSize = true;
            this.chkService.Checked = true;
            this.chkService.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkService.ForeColor = System.Drawing.Color.Firebrick;
            this.chkService.Location = new System.Drawing.Point(99, 696);
            this.chkService.Name = "chkService";
            this.chkService.Size = new System.Drawing.Size(62, 17);
            this.chkService.TabIndex = 2;
            this.chkService.Text = "Service";
            this.chkService.UseVisualStyleBackColor = true;
            // 
            // chkDatabase
            // 
            this.chkDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkDatabase.AutoSize = true;
            this.chkDatabase.Checked = true;
            this.chkDatabase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDatabase.ForeColor = System.Drawing.Color.Green;
            this.chkDatabase.Location = new System.Drawing.Point(167, 696);
            this.chkDatabase.Name = "chkDatabase";
            this.chkDatabase.Size = new System.Drawing.Size(72, 17);
            this.chkDatabase.TabIndex = 3;
            this.chkDatabase.Text = "Database";
            this.chkDatabase.UseVisualStyleBackColor = true;
            // 
            // chkErrors
            // 
            this.chkErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkErrors.AutoSize = true;
            this.chkErrors.Checked = true;
            this.chkErrors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkErrors.ForeColor = System.Drawing.Color.Red;
            this.chkErrors.Location = new System.Drawing.Point(569, 696);
            this.chkErrors.Name = "chkErrors";
            this.chkErrors.Size = new System.Drawing.Size(53, 17);
            this.chkErrors.TabIndex = 4;
            this.chkErrors.Text = "Errors";
            this.chkErrors.UseVisualStyleBackColor = true;
            // 
            // chkWarnings
            // 
            this.chkWarnings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkWarnings.AutoSize = true;
            this.chkWarnings.Checked = true;
            this.chkWarnings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWarnings.ForeColor = System.Drawing.Color.Orange;
            this.chkWarnings.Location = new System.Drawing.Point(628, 696);
            this.chkWarnings.Name = "chkWarnings";
            this.chkWarnings.Size = new System.Drawing.Size(71, 17);
            this.chkWarnings.TabIndex = 5;
            this.chkWarnings.Text = "Warnings";
            this.chkWarnings.UseVisualStyleBackColor = true;
            // 
            // chkFreeze
            // 
            this.chkFreeze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFreeze.AutoSize = true;
            this.chkFreeze.Location = new System.Drawing.Point(792, 696);
            this.chkFreeze.Name = "chkFreeze";
            this.chkFreeze.Size = new System.Drawing.Size(58, 17);
            this.chkFreeze.TabIndex = 6;
            this.chkFreeze.Text = "Freeze";
            this.chkFreeze.UseVisualStyleBackColor = true;
            // 
            // chkInfo
            // 
            this.chkInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkInfo.AutoSize = true;
            this.chkInfo.Checked = true;
            this.chkInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInfo.Location = new System.Drawing.Point(696, 696);
            this.chkInfo.Name = "chkInfo";
            this.chkInfo.Size = new System.Drawing.Size(78, 17);
            this.chkInfo.TabIndex = 7;
            this.chkInfo.Text = "Information";
            this.chkInfo.UseVisualStyleBackColor = true;
            // 
            // chkControllers
            // 
            this.chkControllers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkControllers.AutoSize = true;
            this.chkControllers.ForeColor = System.Drawing.Color.Purple;
            this.chkControllers.Location = new System.Drawing.Point(245, 696);
            this.chkControllers.Name = "chkControllers";
            this.chkControllers.Size = new System.Drawing.Size(75, 17);
            this.chkControllers.TabIndex = 8;
            this.chkControllers.Text = "Controllers";
            this.chkControllers.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClear.Location = new System.Drawing.Point(408, 690);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // TraceWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 725);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.chkControllers);
            this.Controls.Add(this.chkInfo);
            this.Controls.Add(this.chkFreeze);
            this.Controls.Add(this.chkWarnings);
            this.Controls.Add(this.chkErrors);
            this.Controls.Add(this.chkDatabase);
            this.Controls.Add(this.chkService);
            this.Controls.Add(this.chkViewModel);
            this.Controls.Add(this.rtbWindow);
            this.Name = "TraceWindow";
            this.Text = "Middleware Client Tracer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbWindow;
        private System.Windows.Forms.CheckBox chkViewModel;
        private System.Windows.Forms.CheckBox chkService;
        private System.Windows.Forms.CheckBox chkDatabase;
        private System.Windows.Forms.CheckBox chkErrors;
        private System.Windows.Forms.CheckBox chkWarnings;
        private System.Windows.Forms.CheckBox chkFreeze;
        private System.Windows.Forms.CheckBox chkInfo;
        private System.Windows.Forms.CheckBox chkControllers;
        private System.Windows.Forms.Button btnClear;

    }
}

