﻿using System.Collections.ObjectModel;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Model.Domain.Invoicing;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.Controllers
{

    internal class InvoiceSyncController : AbstractGenericSyncController<Invoice>
    {

        public InvoiceSyncController(MasterViewModel master, ObservableCollection<Invoice> invoices)
            : base(master,  invoices)
        {
            //MasterSyncController.OnProgressReportNotification += ProgressReportNotification;
            
        }

        private void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
            //Master.InvoiceViewModel.SetProgressBar(jobName, stepDetail, percentageComplete);
            //SendUserMessage(jobName + ": " + stepDetail + " " + percentageComplete + "% complete", MessageType.Info, "Invoice");
        }
    
    }
}
