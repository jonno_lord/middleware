﻿using System.Collections.ObjectModel;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Enums;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.Controllers
{

    internal class DedupeSyncController : AbstractGenericSyncController<Customer>
    {

        public DedupeSyncController(MasterViewModel master,  ObservableCollection<Customer> customers)
            : base(master,  customers)
        {
            MasterSyncController.OnSynchroniseNotification += Synchronise;
            MasterSyncController.OnRefreshFromDatabaseNotification += RefreshFromDatabase;
        }

        
        private void Synchronise()
        {
            foreach (Customer customer in SyncCollection)
            {
                if (customer.Queue == QueueType.Accepted || customer.Queue == QueueType.Rejected)
                {
                    if (!customer.IsLocked) Lock(customer);
                }
            }
        }

        private void RefreshFromDatabase()
        {
            Master.DedupeViewModel.RefreshDedupe();
        }

     
    }
}
