﻿using System;
using System.ServiceModel;
using System.Threading;
using UBM.Logger;
using UBM.MiddlewareClient.ClientSyncService;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Enums;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.Controllers
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
   CallbackBehavior(UseSynchronizationContext = false)]
    public sealed class MasterSyncController : ISynchroniseCallback
    {
        #region Fields

        private SynchroniseClient syncClient;
        private bool syncOnline;

        private delegate bool SyncDelegate(ISyncable syncable);

        private static MasterViewModel Master { get; set; }


        #endregion

        #region Private Methods

        private bool SyncRecover(SyncDelegate tryAgain, ISyncable syncable)
        {
            syncClient.Abort();
            Master.SyncState = LightColour.Amber;
            Thread.Sleep(1000);
            try
            {
                syncClient = new SynchroniseClient(new InstanceContext(this));
                syncClient.Open();

            }
            catch(Exception exception)
            {
                syncClient.Abort();
                Master.SyncState = LightColour.Red;
                syncOnline = false;
                Log.Error("Failed to initialise Master Sync Controller", exception);
                return false;
            }

            if (!string.IsNullOrEmpty(syncable.SystemName))
            {
                return Connect() && tryAgain(syncable);
            }
            return Connect();
          
        }

        private bool RequestView(ISyncable syncable)
        {
            try
            {
                return syncClient.View(new Viewing
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch
            {
                return SyncRecover(RequestView, syncable);
            }
        }

        private bool RequestUnview(ISyncable syncable)
        {
            try
            {
                return syncClient.Unview(new Viewing
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch 
            {
                return SyncRecover(RequestUnview, syncable);
            }
        }

        private bool RequestLock(ISyncable syncable)
        {
            try
            {
                return syncClient.Lock(new Lock
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    LockTime = DateTime.Now,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch 
            {
                return SyncRecover(RequestLock, syncable);
            }
        }

        private bool RequestUnlock(ISyncable syncable)
        {
            try
            {
                return syncClient.Unlock(new Lock
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    LockTime = DateTime.Now,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch 
            {
                return SyncRecover(RequestUnlock, syncable);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates new instance of Sync Controller
        /// </summary>
        /// <param name="master">The master.</param>
        /// <param name="syncObjectType">Type of the sync object.</param>
        public MasterSyncController(MasterViewModel master) // , SyncObjectType syncObjectType)
        {

            if (syncClient == null)
            {
                Master = master;

                syncClient = new SynchroniseClient(new InstanceContext(this));
                syncClient.Open();

                Connect();
            }
        }

        #endregion

        #region Internal Methods

        internal SyncData GetSyncData(string syncObjectType)
        {
            return syncOnline ? syncClient.GetSyncData(syncObjectType) : null;
        }

        /// <summary>
        /// Connects to sync service
        /// </summary>
        /// <returns></returns>
        internal bool Connect()
        {
            try
            {
                syncClient.Connect(Environment.UserName);
                SynchroniseNotification();
                Master.SyncState = LightColour.Green;
                syncOnline = true;
                return true;
            }
            catch
            {
                Master.SetActionMessage("Could not connect to Synchronisation Service", MessageType.Error);
                Master.SyncState = LightColour.Red;
                syncOnline = false;
                return false;
            }
        }

        /// <summary>
        /// Disconnects from sync service
        /// </summary>
        internal void Disconnect()
        {
            try
            {
                syncClient.Disconnect(Environment.UserName);
                syncClient.Close();
            }
            catch
            {
                syncClient.Abort();
            }
        }

        /// <summary>
        /// Checks if still connected to sync service and reconnects if its faulted
        /// </summary>
        /// <returns></returns>
        internal bool SyncCheckAndRecover()
        {
            if (syncClient.State != CommunicationState.Opened)
            {
                return SyncRecover(null, null);
            }
            return true;
        }

        /// <summary>
        ///   Signals other connected clients to refresh from database
        /// </summary>
        internal void RefreshClients()
        {
            try
            {
                syncClient.RefreshClients();
            }
            catch
            {
                if (SyncRecover(null, null)) syncClient.RefreshClients();
            }
        }

        /// <summary>
        ///   Adds this user as a viewer of a syncable on sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void View(ISyncable syncable)
        {
            if (syncOnline && RequestView(syncable))
            {
                syncable.AddViewer(Environment.UserName);
            }
        }

        /// <summary>
        ///   Removes this user as a viewer of a syncable on sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void Unview(ISyncable syncable)
        {
            if (syncOnline && RequestUnview(syncable))
            {
                syncable.RemoveViewer(Environment.UserName);
            }
        }

        /// <summary>
        ///   Locks a syncable on the sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void Lock(ISyncable syncable)
        {
            if (syncOnline && RequestLock(syncable))
            {
                syncable.Lock(Environment.UserName);
            }
        }

        /// <summary>
        ///   Unlocks a syncable on sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void Unlock(ISyncable syncable)
        {
            if (syncOnline && RequestUnlock(syncable))
            {
                syncable.Unlock(Environment.UserName);
            }
        }

        internal void UnlockAll(string syncObjectType)
        {
            syncClient.UnlockAllForUser(Environment.UserName, syncObjectType);
        }


        #endregion

        #region Callbacks

        public delegate void SynchroniseHandler();
        public delegate void RefreshFromDatabase();
        public delegate void ProgressReport(string jobName, string stepDetail, double percentageComplete);

        public event SynchroniseHandler OnSynchroniseNotification;
        public event RefreshFromDatabase OnRefreshFromDatabaseNotification;
        public event ProgressReport OnProgressReportNotification;

        public void SynchroniseNotification()
        {
            SendOrPostCallback callback = delegate
            {
                SynchroniseHandler handler = OnSynchroniseNotification;
                if (handler != null) handler();
            };

            Master.UIContext.Post(callback, null);
        }

        public void RefreshFromDatabaseNotification()
        {
            SendOrPostCallback callback = delegate
            {
                RefreshFromDatabase handler = OnRefreshFromDatabaseNotification;
                if (handler != null) handler();
            };

            Master.UIContext.Post(callback, null);
        }

        public void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
            SendOrPostCallback callback = delegate
            {
                ProgressReport handler = OnProgressReportNotification;
                if (handler != null) handler(jobName, stepDetail, percentageComplete);
            };

            Master.UIContext.Post(callback, null);
        }

        public void DebugNotification(string message)
        {

        }

        #endregion

      
    }

}
