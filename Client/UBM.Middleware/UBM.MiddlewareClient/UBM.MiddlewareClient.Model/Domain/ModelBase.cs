﻿using System;
using System.ComponentModel;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Domain
{
    /// <summary>
    /// All Model classes inherit from model base. It implements the INotifyPropertyChanged interface to update WPF bindings
    /// </summary>
    public class ModelBase : INotifyPropertyChanged, IDrillDown
    {
        private bool isClicked;

        
        public bool IsClicked
        {
            get { return this.isClicked; }
            set
            {
                if (value == this.isClicked) return;
                this.isClicked = value;
                OnPropertyChanged("IsClicked");
                if(isClicked) DrillDown(this);
            }
        }
         

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public event DrillDownEventHandler OnDrillDown;

        public void DrillDown(object sender)
        {
            DrillDownEventHandler handler = OnDrillDown;
            if (handler != null) handler(sender);
        }
    }
}
