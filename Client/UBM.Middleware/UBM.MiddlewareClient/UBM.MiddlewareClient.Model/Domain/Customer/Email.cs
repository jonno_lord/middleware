﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;
using UBM.NotificationClient;
using UBM.Utilities;
using UBM.Utilities.HtmlReportGenerator;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Email represents a notification email sent to a salesperson when a customer is rejected from dedupe
    /// </summary>
    public class Email : ModelBase
    {

        #region Properties

        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string SystemName { get; set; }

        private DateTime? dateRejected;
        public DateTime? DateRejected
        {
            get { return this.dateRejected; }
            set
            {
                if (value == this.dateRejected) return;
                this.dateRejected = value;
                OnPropertyChanged("DateRejected");
            }
        }
         

        private string from;
        public string From
        {
            get { return this.from; }
            set
            {
                if (value == this.from) return;
                this.from = value;
                OnPropertyChanged("From");
            }
        }

        private string to;
        public string To
        {
            get { return this.to; }
            set
            {
                if (value == this.to) return;
                this.to = value;
                OnPropertyChanged("To");
            }
        }

        private CC cc;
        public CC CC
        {
            get { return this.cc; }
            set
            {
                if (value == this.cc) return;
                this.cc = value;
                OnPropertyChanged("CC");
            }
        }

        private Rejection selectedReason;
        public Rejection SelectedReason
        {
            get { return this.selectedReason; }
            set
            {
                if (value == null || value == this.selectedReason) return;
                this.selectedReason = value;
                OnPropertyChanged("SelectedReason");

                Body = selectedReason.Description;
                Reason = selectedReason.Reason;
            }
        }

        private string reason;
        public string Reason
        {
            get { return this.reason; }
            set
            {
                if (value == this.reason) return;
                this.reason = value;
                OnPropertyChanged("Reason");
            }
        }

        public string TruncatedBody
        {
            get
            {
                return !string.IsNullOrEmpty(body) && body.Length > 30 ? StringHelper.Truncate(body, 30) ?? "" : body;
            }
        }

        private string body;
        public string Body
        {
            get { return (BodyState == MAXIMISED) ? this.body : TruncatedBody; }
            set
            {
                if (value == this.body) return;
                this.body = value;
                OnPropertyChanged("Body");
            }
        }

        private bool sent;
        public bool Sent
        {
            get { return this.sent; }
            set
            {
                if (value == this.sent) return;
                this.sent = value;
                OnPropertyChanged("Sent");
            }
        }     



        #endregion


        private const string TRUNCATED = "More>>";
        private const string MAXIMISED = "<<Less";

        private string bodyState;
        public string BodyState
        {
            get { return this.bodyState; }
            set
            {
                if (value == this.bodyState) return;
                this.bodyState = value;
                OnPropertyChanged("BodyState");
                OnPropertyChanged("Body");
            }
        }

        private string ccState;
        public string CCState
        {
            get { return this.ccState; }
            set
            {
                if (value == this.ccState) return;
                this.ccState = value;
                OnPropertyChanged("CCState");
            }
        }
         
        private DelegateCommand toggleCCComand;
        public ICommand ToggleCCCommand
        {
            get
            {
                if (toggleCCComand == null) toggleCCComand = new DelegateCommand(ToggleCC);
                return toggleCCComand;
            }
        }
        public void ToggleCC(object state)
        {
            if (CCState == MAXIMISED)
            {
                this.CC.CCListConcatenated = this.CC.TruncatedCCListConcatenated;
                CCState = TRUNCATED;
            }
            else
            {
                this.CC.CCListConcatenated = this.CC.FullCCListConcatenated;
                CCState = MAXIMISED;
            }
        }

        private DelegateCommand toggleBodyCommand;
        public ICommand ToggleBodyCommand
        {
            get
            {
                if (toggleBodyCommand == null) toggleBodyCommand = new DelegateCommand(ToggleBody);
                return toggleBodyCommand;
            }
        }
        public void ToggleBody(object state)
        {
            if(BodyState == MAXIMISED)
            {
                BodyState = TRUNCATED;
            }
            else
            {
                BodyState = MAXIMISED;
            }
        }


 
        #region Public Methods

        public static List<Email> CreateEmailHistory(List<vwCustomerRejection> rejections)
        {
            var emails = new List<Email>();

            foreach (vwCustomerRejection rejection in rejections)
            {


                Email email = new Email
                                  {
                                      CustomerID = rejection.ID,
                                      CustomerName = rejection.CustomerName,
                                      SystemName = rejection.SystemName,
                                      DateRejected =rejection.DateRejected,
                                      Body = rejection.Body,
                                      Reason = StringHelper.Tidy(rejection.ReasonRejected),
                                      From = StringHelper.Tidy(rejection.Sender),
                                      To = StringHelper.Tidy(rejection.Recipient),
                                      Sent = rejection.NotificationSent ?? false,
                                      CC = new CC
                                      {
                                          CCList = CC.CreateCCList(rejection.CC),
                                      }
                                  };

                email.BodyState = TRUNCATED;

                email.CC.CCListConcatenated = email.CC.TruncatedCCListConcatenated;
                email.CCState = TRUNCATED;

                emails.Add(email);
            }

            return emails;
        }


        public static Email CreateBlankEmail()
        {
            return new Email
                       {
                           Sent = true,
                           CC = new CC
                                    {
                                        CCList = new ObservableCollection<string>()
                                    }
                       };
        }

        /// <summary>
        /// Creates the email.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        public static Email RetrieveUnsentEmail(UserAction action, List<Rejection> rejectionReasonList)
        {

            Email email = new Email
                              {
                                  
                                  From = action.userName,
                                  To = action.recipient,
                                  Reason = action.reason,
                                  Sent = action.sendNotification ?? false,
                                  CC = new CC
                                  {
                                      CCList = CC.CreateCCList(action.cc),
                                  }
                              };

            email.SelectedReason = rejectionReasonList.Find(c => c.Reason == action.reason);
            if (email.SelectedReason == null) email.SelectedReason = rejectionReasonList[0];

            email.Body = action.body;
            email.BodyState = MAXIMISED;

            email.CC.CCListConcatenated = email.CC.FullCCListConcatenated;
            email.CCState = MAXIMISED;

            return email;
        }

     

        public static void Send(Customer customer)
        {
            if (customer.Email.Sent)
            {
                NotificationServiceClient.Email(Environment.UserName,
                                                customer.Email.To,
                                                customer.Email.CC.FullCCListConcatenated,
                                                "Customer Rejection Notification",
                                                CreateHtmlEmailBody(customer),
                                                Priority.Normal, true);
            }
        }

        public void ShowFullBody()
        {
            if (BodyState != MAXIMISED)
            {
                ToggleBody(null);
            }
        }
        public void ShowTruncatedBody()
        {
            if (BodyState != TRUNCATED)
            {
                ToggleBody(null);
            }
        }

        private static string CreateHtmlEmailBody(Customer customer)
        {

            string customerIdentifier, orderIdentifier;

            switch (customer.SystemName)
            {
                case "ESOP":
                    customerIdentifier = "Prospect Number";
                    orderIdentifier = "Contract Number";
                    break;
                case "ASOPUK":
                    customerIdentifier = "Customer URN";
                    orderIdentifier = "Order URN";
                    break;
                case "ASOPDaltons":
                    customerIdentifier = "Customer URN";
                    orderIdentifier = "Order URN";
                    break;
                case "IPSOP":
                    customerIdentifier = "Company Text ID";
                    orderIdentifier = "Subscription No.";
                    break;
                default:
                    customerIdentifier = "Customer URN";
                    orderIdentifier = "Order Number";
                    break;
            }

            HtmlReport report = new HtmlReport();

            report.AddHeader(customer.SystemName + " Customer Rejected");

            Table table = HtmlReport.CreateTable(TableStyleName.MiddlewareCustomerRejection).
                                    AddRow(new List<string> {"Customer Name:", customer.ContactDetails.CustomerName}).
                                    AddRow(new List<string> {customerIdentifier + ":", customer.URN});

            if (!string.IsNullOrEmpty(customer.SaleInformation.ContractNumber))
            {
                table.AddRow(new List<string> { orderIdentifier + ":", customer.SaleInformation.ContractNumber });
            }

            table.AddRow(new List<string> { "Rejected By:", Environment.UserName });

            report.AddTable(table);

            report.AddParagraph(customer.Email.Body);

            return report.ToString();
        }




        #endregion



    }
}
