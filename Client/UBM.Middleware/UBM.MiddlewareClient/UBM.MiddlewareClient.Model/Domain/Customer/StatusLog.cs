﻿using System;
using System.Data;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Contains a log of dates for the different stages of processing in Middleware for a customer
    /// </summary>
    public class StatusLog : ModelBase
    {
        #region Fields


        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the JDE to middleware.
        /// </summary>
        /// <value>The JDE to middleware.</value>
        public DateTime? JDEToMiddleware { get; set; }

        /// <summary>
        /// Gets or sets the source to middleware.
        /// </summary>
        /// <value>The source to middleware.</value>
        public DateTime? SourceToMiddleware { get; set; }

        /// <summary>
        /// Gets or sets the middleware to middleware in.
        /// </summary>
        /// <value>The middleware to middleware in.</value>
        public DateTime? MiddlewareToMiddlewareIn { get; set; }

        /// <summary>
        /// Gets or sets the middleware to middleware out.
        /// </summary>
        /// <value>The middleware to middleware out.</value>
        public DateTime? MiddlewareToMiddlewareOut { get; set; }

        /// <summary>
        /// Gets or sets the middleware to JDE.
        /// </summary>
        /// <value>The middleware to JDE.</value>
        public DateTime? MiddlewareToJDE { get; set; }

        /// <summary>
        /// Gets or sets the middleware to source.
        /// </summary>
        /// <value>The middleware to source.</value>
        public DateTime? MiddlewareToSource { get; set; }

        /// <summary>
        /// Gets or sets the rejected from JDE.
        /// </summary>
        /// <value>The rejected from JDE.</value>
        public DateTime? RejectedFromJDE { get; set; }

        /// <summary>
        /// Gets or sets the cancelled from JDE.
        /// </summary>
        /// <value>The cancelled from JDE.</value>
        public DateTime? CancelledFromJDE { get; set; }

        /// <summary>
        /// Gets or sets the rejected from dedupe.
        /// </summary>
        /// <value>The rejected from dedupe.</value>
        public DateTime? RejectedFromDedupe { get; set; }

        /// <summary>
        /// Gets or sets the accepted in dedupe.
        /// </summary>
        /// <value>The accepted in dedupe.</value>
        public DateTime? AcceptedInDedupe { get; set; }


        /// <summary>
        /// Gets or sets the last affected.
        /// </summary>
        /// <value>The last affected.</value>
        public DateTime? LastAffected { get; set; }

        #endregion


        /// <summary>
        /// Creates the status log.
        /// </summary>
        /// <param name="linqCustomer">The linq customer.</param>
        /// <returns></returns>
        public static StatusLog CreateStatusLog(vwCustomer linqCustomer)
        {
            return new StatusLog
            {
                AcceptedInDedupe = linqCustomer.AcceptedInScrutiny,
                RejectedFromDedupe = linqCustomer.RejectedInScrutiny,
                MiddlewareToMiddlewareIn = linqCustomer.MiddlewaretoMiddlewareIn,
                MiddlewareToMiddlewareOut = linqCustomer.MiddlewareToMiddlewareOut,
                MiddlewareToSource = linqCustomer.MiddlewareToSource,
                MiddlewareToJDE = linqCustomer.MiddlewareToJDE,
                SourceToMiddleware = linqCustomer.SourceToMiddleware,
                RejectedFromJDE = linqCustomer.RejectedFromJDE,
                JDEToMiddleware = linqCustomer.JDEToMiddleware
            };

        }

        /// <summary>
        /// Creates the status log.
        /// </summary>
        /// <param name="customerRow">The customer row.</param>
        /// <returns></returns>
        public static StatusLog CreateStatusLog(DataRow customerRow)
        {
            return new StatusLog
            {
                AcceptedInDedupe = customerRow["AcceptedInScrutiny"] as DateTime?,
                RejectedFromDedupe = customerRow["RejectedInScrutiny"] as DateTime?,
                MiddlewareToMiddlewareIn = customerRow["MiddlewaretoMiddlewareIn"] as DateTime?,
                MiddlewareToMiddlewareOut = customerRow["MiddlewareToMiddlewareOut"] as DateTime?,
                MiddlewareToSource = customerRow["MiddlewareToSource"] as DateTime?,
                MiddlewareToJDE = customerRow["MiddlewareToJDE"] as DateTime?,
                SourceToMiddleware = customerRow["SourceToMiddleware"] as DateTime?,
                RejectedFromJDE = customerRow["RejectedFromJDE"] as DateTime?,
                JDEToMiddleware = customerRow["JDEToMiddleware"] as DateTime?
            };

        }
    }
}
