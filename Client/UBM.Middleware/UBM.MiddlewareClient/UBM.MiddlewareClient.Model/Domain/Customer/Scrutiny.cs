﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Scrutiny contains information regarding violations and duplicates for a dedupe customer
    /// </summary>
    public class Scrutiny : ModelBase
    {
        #region Fields

        private bool inScrutiny;
        private bool previouslyRejected;
        private string reasonRejected;
        private List<Violation> violations;
        private List<Duplicate> duplicates;
        private List<Duplicate> sopDuplicates;
        private bool flag;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [in scrutiny].
        /// </summary>
        /// <value><c>true</c> if [in scrutiny]; otherwise, <c>false</c>.</value>
        public bool InScrutiny
        {
            get { return this.inScrutiny; }
            set
            {
                if (value == this.inScrutiny) return;
                this.inScrutiny = value;
                OnPropertyChanged("InScrutiny");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [previously rejected].
        /// </summary>
        /// <value><c>true</c> if [previously rejected]; otherwise, <c>false</c>.</value>
        public bool PreviouslyRejected
        {
            get { return this.previouslyRejected; }
            set
            {
                if (value == this.previouslyRejected) return;
                this.previouslyRejected = value;
                OnPropertyChanged("PreviouslyRejected");
            }
        }

        /// <summary>
        /// Gets or sets the reason rejected.
        /// </summary>
        /// <value>The reason rejected.</value>
        public string ReasonRejected
        {
            get { return this.reasonRejected; }
            set
            {
                if (value == this.reasonRejected) return;
                this.reasonRejected = value;
                OnPropertyChanged("ReasonRejected");
            }
        }



        /// <summary>
        /// Gets or sets the violations.
        /// </summary>
        /// <value>The violations.</value>
        public List<Violation> Violations
        {
            get { return this.violations; }
            set
            {
                if (value == this.violations) return;
                this.violations = value;
                OnPropertyChanged("Violations");
            }
        }

        /// <summary>
        /// Gets or sets the duplicates.
        /// </summary>
        /// <value>The duplicates.</value>
        public List<Duplicate> Duplicates
        {
            get { return this.duplicates; }
            set
            {
                if (value == this.duplicates) return;
                this.duplicates = value;
                OnPropertyChanged("Duplicates");
            }
        }

        /// <summary>
        /// Gets or sets the SOP duplicates.
        /// </summary>
        /// <value>The SOP duplicates.</value>
        public List<Duplicate> SOPDuplicates
        {
            get { return this.sopDuplicates; }
            set
            {
                if (value == this.sopDuplicates) return;
                this.sopDuplicates = value;
                OnPropertyChanged("SOPDuplicates");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Scrutiny"/> is flag.
        /// </summary>
        /// <value><c>true</c> if flag; otherwise, <c>false</c>.</value>
        public bool Flag
        {
            get { return this.flag; }
            set
            {
                if (value == this.flag) return;
                this.flag = value;
                OnPropertyChanged("Flag");
            }
        }
         
         
                
      

        #endregion

        /// <summary>
        /// Creates the scrutiny.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="duplicates">The duplicates.</param>
        /// <param name="violations">The violations.</param>
        /// <param name="rejections">The rejections.</param>
        /// <returns></returns>
        public static Scrutiny CreateScrutiny(Customer customer, List<Duplicate> duplicates, List<Violation> violations, List<Email> rejections)
        {

            bool inScrutiny = false;
            bool flagCustomer = false;

            if ((duplicates.Count + violations.Count) > 0)
            {
                flagCustomer = true;
            }
            if (customer.EntryStatus == EntryStatus.SourceToMiddleware) inScrutiny = true;

  

            bool previouslyRejected = false;
            string reasonRejected = "";

            if (rejections.Count > 0)
            {
                previouslyRejected = true;
                reasonRejected = rejections[0].Reason;
            }

            Scrutiny scrutiny = new Scrutiny
            {
                InScrutiny = inScrutiny,
                Duplicates = duplicates,
                SOPDuplicates = new List<Duplicate>(),
                Violations = violations,
                PreviouslyRejected = previouslyRejected,
                ReasonRejected = StringHelper.Tidy(reasonRejected),
                Flag = flagCustomer
            };

            return scrutiny;
        }
    }
}
