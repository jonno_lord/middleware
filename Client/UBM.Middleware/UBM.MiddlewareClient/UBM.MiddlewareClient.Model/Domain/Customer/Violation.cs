﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Represents a scrurtiny violation for a customer
    /// </summary>
    public class Violation : ModelBase
    {
        #region Fields

        private string name;
        private string description;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return this.description; }
            set
            {
                if (value == this.description) return;
                this.description = value;
                OnPropertyChanged("Description");
            }
        }
        #endregion

        public static List<Violation> CreateViolations(List<vwCustomerViolation> customerViolations)
        {
            var violations = new List<Violation>();

            foreach (vwCustomerViolation violation in customerViolations)
            {
                violations.Add(new Violation
                {
                    Name = violation.Violation,
                    Description = violation.Description
                });
            }

            return violations;
        }

        ///// <summary>
        ///// Creates the violations.
        ///// </summary>
        ///// <param name="customer">The customer.</param>
        ///// <param name="dataAccess">The data access.</param>
        ///// <returns></returns>
        //public static List<Violation> CreateViolations(Customer customer, LinqDataAccess dataAccess)
        //{
        //    var violations = new List<Violation>();

        //    List<vwCustomerViolation> customerViolations = dataAccess.Violations.FindAll(c => c.ID == customer.ID && c.SystemName == customer.SystemName);

        //    foreach (vwCustomerViolation violation in customerViolations)
        //    {
        //        violations.Add(new Violation
        //        {
        //            Name = violation.Violation,
        //            Description = violation.Description
        //        });
        //    }

        //    return violations;
        //}
    }
}
