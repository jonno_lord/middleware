﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Model.Orm;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Duplicate represents a duplidate customer in either a SOP system or JDE
    /// </summary>
    public class Duplicate : ModelBase
    {

        #region Properties

        /// <summary>
        /// Gets or sets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        public string SystemName { get; set; }

        /// <summary>
        /// Gets or sets the URN.
        /// </summary>
        /// <value>The URN.</value>
        public string URN { get; set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the contact details.
        /// </summary>
        /// <value>The contact details.</value>
        public ContactDetails ContactDetails { get; set; }
        /// <summary>
        /// Gets or sets the JDE account number.
        /// </summary>
        /// <value>The JDE account number.</value>
        public string JDEAccountNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [account expired].
        /// </summary>
        /// <value><c>true</c> if [account expired]; otherwise, <c>false</c>.</value>
        public bool AccountExpired { get; set; }

        #endregion

        public static List<Duplicate> CreateDuplicates(List<vwCustomerDuplicate> duplicates)
        {
            var dupes = new List<Duplicate>();

            foreach (vwCustomerDuplicate dupe in duplicates)
            {
                dupes.Add(new Duplicate
                {
                    URN = dupe.URN,
                    SystemName = dupe.SystemName,
                    Source = dupe.DuplicateSource,
                    JDEAccountNumber = dupe.JDEAccountNumber,
                    AccountExpired = dupe.JDEAccountExpired ?? false,

                    ContactDetails = new ContactDetails
                                         {
                                             CustomerName = dupe.CustomerName.Trim(),
                                             AddressLine1 =  dupe.AddressLine1.Trim(),
                                             AddressLine2 = dupe.AddressLine2.Trim(),
                                             AddressLine3 = dupe.AddressLine3.Trim(),
                                             AddressLine4 = dupe.AddressLine4.Trim(),
                                             City = dupe.City.Trim(),
                                             County = dupe.County.Trim(),
                                             Country = dupe.Country.Trim(),
                                             Postcode = dupe.Postcode.Trim(),
                                             Email = dupe.Email.Trim(),
                                             Telephone = dupe.Telephone.Trim(),
                                             URL = dupe.URL.Trim()
                                         }
                });
            }

            return dupes;
        }

      
    }
}
