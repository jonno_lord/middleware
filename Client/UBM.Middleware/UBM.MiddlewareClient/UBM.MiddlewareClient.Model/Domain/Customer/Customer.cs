﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Domain.Customer
{
    /// <summary>
    /// Customer represents a syncable in Middleware. This holds many other objects which contain syncable details
    /// </summary>
    public class Customer : AbstractSyncableModel
    {
        #region Fields

        private string jdeAccountNumber;
        private EntryStatus entryStatus;
        private QueueType queue;
        private bool notify;
        private bool display;
        private bool isSelected;

        // objects
        private Email email;
        private Legal legal;
        private Scrutiny scrutiny;
        private SaleInformation saleInformation;
        private ObservableCollection<Email> emails;
        private ObservableCollection<Legal> holds;
        private StatusLog statusLog;


        #endregion

        #region Public Properties


        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public override int ID { get; set; }

        /// <summary>
        /// Gets or sets the URN.
        /// </summary>
        /// <value>The URN.</value>
        public string URN { get; set; }

        /// <summary>
        /// Gets or sets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        public override string SystemName { get; set; }


        public string SearchType { get; set; }
        

        /// <summary>
        /// Gets or sets the JDE account number.
        /// </summary>
        /// <value>The JDE account number.</value>
        public string JDEAccountNumber
        {
            get { return this.jdeAccountNumber; }
            set
            {
                if (value == this.jdeAccountNumber) return;
                this.jdeAccountNumber = value;
                OnPropertyChanged("JDEAccountNumber");
            }
        }

        /// <summary>
        /// Gets or sets the queue.
        /// </summary>
        /// <value>The queue.</value>
        public QueueType Queue
        {
            get { return this.queue; }
            set
            {
                if (value == this.queue) return;
                this.queue = value;
                OnPropertyChanged("Queue");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

         
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Customer"/> is notify.
        /// </summary>
        /// <value><c>true</c> if notify; otherwise, <c>false</c>.</value>
        public bool Notify
        {
            get { return this.notify; }
            set
            {
                if (value == this.notify) return;
                this.notify = value;
                OnPropertyChanged("Notify");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Customer"/> is display.
        /// </summary>
        /// <value><c>true</c> if display; otherwise, <c>false</c>.</value>
        public bool Display
        {
            get { return this.display; }
            set
            {
                if (value == this.display) return;
                this.display = value;
                OnPropertyChanged("Display");
            }
        }


        /// <summary>
        /// Gets or sets the type of the syncable.
        /// </summary>
        /// <value>The type of the syncable.</value>
        public string CustomerType { get; set; }

        /// <summary>
        /// Gets or sets the entry status.
        /// </summary>
        /// <value>The entry status.</value>
        public EntryStatus EntryStatus
        {
            get { return this.entryStatus; }
            set
            {
                if (value == this.entryStatus) return;
                this.entryStatus = value;
                OnPropertyChanged("EntryStatus");
            }
        }

        /// <summary>
        /// Gets or sets the contact details.
        /// </summary>
        /// <value>The contact details.</value>
        public ContactDetails ContactDetails { get; set; }

        /// <summary>
        /// Gets or sets the legal.
        /// </summary>
        /// <value>The legal.</value>
        public Legal Legal
        {
            get { return this.legal; }
            set
            {
                if (value == this.legal) return;
                this.legal = value;
                OnPropertyChanged("Legal");
            }
        }

        /// <summary>
        /// Gets or sets the scrutiny.
        /// </summary>
        /// <value>The scrutiny.</value>
        public Scrutiny Scrutiny
        {
            get { return this.scrutiny; }
            set
            {
                if (value == this.scrutiny) return;
                this.scrutiny = value;
                OnPropertyChanged("Scrutiny");
            }
        }

        /// <summary>
        /// Gets or sets the sale information.
        /// </summary>
        /// <value>The sale information.</value>
        public SaleInformation SaleInformation
        {
            get { return this.saleInformation; }
            set
            {
                if (value == this.saleInformation) return;
                this.saleInformation = value;
                OnPropertyChanged("SaleInformation");
            }
        }

        /// <summary>
        /// Gets or sets the emails.
        /// </summary>
        /// <value>The emails.</value>
        public ObservableCollection<Email> Emails
        {
            get { return this.emails; }
            set
            {
                if (value == this.emails) return;
                this.emails = value;
                OnPropertyChanged("Emails");
            }
        }

        /// <summary>
        /// Gets or sets the holds.
        /// </summary>
        /// <value>The holds.</value>
        public ObservableCollection<Legal> Holds
        {
            get { return this.holds; }
            set
            {
                if (value == this.holds) return;
                this.holds = value;
                OnPropertyChanged("Holds");
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public Email Email
        {
            get { return this.email; }
            set
            {
                if (value == this.email) return;
                this.email = value;
                OnPropertyChanged("Email");
            }
        }

        /// <summary>
        /// Gets or sets the status log.
        /// </summary>
        /// <value>The status log.</value>
        public StatusLog StatusLog
        {
            get { return this.statusLog; }
            set
            {
                if (value == this.statusLog) return;
                this.statusLog = value;
                OnPropertyChanged("StatusLog");
            }
        }

    

        #endregion


        public static Customer CreateCustomer(vwCustomer linqCustomer, List<vwCustomerDuplicate> duplicates, List<vwCustomerHold> holds, List<vwCustomerRejection> rejections, List<vwCustomerViolation> violations, List<UserAction> userActions, List<Rejection> rejectionReasonsList)
        {
            Stopwatch s = new Stopwatch();
            s.Start();



            Customer customer = new Customer
            {
                ID = linqCustomer.ID,
                URN = linqCustomer.URN,
                JDEAccountNumber = linqCustomer.JDEAccountNumber,
                CustomerType = linqCustomer.CustomerType,
                EntryStatus = (EntryStatus)Enum.Parse(typeof(EntryStatus), linqCustomer.EntryStatus.Replace(" ", ""), true),
                Display = true,
                SystemName = linqCustomer.SystemName,
                ContactDetails = ContactDetails.CreateContactDetails(linqCustomer),
                Email = Email.CreateBlankEmail(),
                SaleInformation = SaleInformation.CreateSaleInformation(linqCustomer),
                StatusLog = StatusLog.CreateStatusLog(linqCustomer),
                SearchType = linqCustomer.SearchType
            };

            customer.Emails = new ObservableCollection<Email>(Email.CreateEmailHistory(rejections));
            customer.Holds = new ObservableCollection<Legal>(Legal.CreateLegalHistory(holds));
            customer.Legal = Legal.CreateLegal(customer.Holds.ToList());

            customer.Scrutiny = Scrutiny.CreateScrutiny(customer,
                                                        Duplicate.CreateDuplicates(duplicates),
                                                        Violation.CreateViolations(violations),
                                                        customer.Emails.ToList());
            s.Stop();
            //Console.WriteLine("Create: " + s.ElapsedMilliseconds.ToString());

            return SetCustomerQueue(customer, userActions, rejectionReasonsList);

        }

        public static Customer SetCustomerQueue(Customer customer, List<UserAction> userActions, List<Rejection> rejectionReasonsList)
        {
            QueueType destination = QueueType.Unassigned;

            UserAction action = userActions.FirstOrDefault(c => c.customerId == customer.ID && c.systemName == customer.SystemName);


            if (action != null)
            {
                if (action.accepted == true)
                {
                    destination = QueueType.Accepted;
                    customer.Scrutiny.InScrutiny = false;
                    customer.JDEAccountNumber = action.jdeAccountNumber;
                }
                else if (action.rejected == true)
                {
                    destination = QueueType.Rejected;
                    customer.Scrutiny.InScrutiny = false;
                    customer.Email = Email.RetrieveUnsentEmail(action, rejectionReasonsList);
                }
            }
            else if (customer.Scrutiny.InScrutiny)
            {
                if (customer.Legal.Hold)
                {
                    destination = QueueType.OnHold;
                }
                else if (customer.Scrutiny.PreviouslyRejected)
                {
                    destination = QueueType.PreviouslyRejected;
                }
                else
                {
                    destination = QueueType.Waiting;
                }
            }
            else if (customer.EntryStatus >= EntryStatus.AcceptedInScrutiny)
            {
                destination = QueueType.Accepted;
            }

            if (destination != QueueType.Unassigned)
            {
                customer.Queue = destination;
            }


            return customer;
        }

        public static Customer CreateNullCustomer()
        {
            return new Customer
                       {
                           ContactDetails = new ContactDetails(),
                           Email = new Email
                                       {
                                           CC = new CC
                                                    {
                                                        CCList = new ObservableCollection<string>()
                                                    }                                            
                                       }
                       };
        }

        internal override string GetIdentifier()
        {
            return this.ContactDetails.CustomerName;
        }

    }
}