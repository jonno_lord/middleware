﻿using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Model.Domain
{
    public class PageAccess : ModelBase
    {
        public ClientPageNumber ClientPageNumber { get; set; }
        public bool CanRead { get; set; }
        public bool CanWrite { get; set; }

        public PageAccess()
        {
        }

        public PageAccess(ClientPageNumber pageNumber, bool canRead, bool canWrite)
        {
            ClientPageNumber = pageNumber;
            CanRead = canRead;
            CanWrite = canWrite;
        }

        public override bool Equals(object obj)
        {
            PageAccess compare = obj as PageAccess;

            if(compare != null)
            {
                return this.ClientPageNumber == compare.ClientPageNumber
                       && this.CanRead == compare.CanRead
                       && this.CanWrite == compare.CanWrite;
            }

            return false;
        }
    }
}
