﻿using System.Data;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Domain
{
    /// <summary>
    /// Order contains information about a customer order
    /// </summary>
    public class Order : ModelBase
    {
        #region Fields

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        public string SystemName { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The number.</value>
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        public double? Amount { get; set; }

        /// <summary>
        /// Gets or sets the currency.
        /// </summary>
        /// <value>The currency.</value>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the lines.
        /// </summary>
        /// <value>The lines.</value>
        public string Lines { get; set; }



        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        /// <value>The batch number.</value>
        public string BatchNumber { get; set; }


        public string JDEAccountNumber { get; set; }

        public string CustomerName { get; set; }
       
        #endregion


    }
}
