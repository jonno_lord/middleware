﻿using System;
using System.Collections.Generic;

namespace UBM.MiddlewareClient.Model.Domain.FeedRunner
{
    public class FileFeed
    {
        public int FileId { get; set; }
        public string FileSize { get; set; }
        public string Filename { get; set; }
        public string FilePath { get; set; }
        public DateTime? FileDate { get; set; }
        public DateTime? FtpDate { get; set; }
        public DateTime? MovedFileDate { get; set; }
        public string Status { get; set; }


        public List<FileHistory> History { get; set; }
    }
}
