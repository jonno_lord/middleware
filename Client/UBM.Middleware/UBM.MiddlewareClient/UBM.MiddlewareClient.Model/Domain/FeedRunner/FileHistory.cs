﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Model.Domain.FeedRunner
{
    public class FileHistory
    {
        public string Description { get; set; }
        public DateTime? Dated { get; set; }
        public bool Error { get; set; }
    }
}
