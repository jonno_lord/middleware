﻿
namespace UBM.MiddlewareClient.Model.Domain
{
    /// <summary>
    /// SalesSystem is populated from the MiddlewareSystems database. An object is created for each SOP system in Middleware.
    /// </summary>
    public class SalesSystem : ModelBase
    {

        public string Name { get; set; }

        private bool isSelected;
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

    }
}
