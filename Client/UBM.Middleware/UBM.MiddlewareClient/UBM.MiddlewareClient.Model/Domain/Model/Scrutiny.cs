﻿using System.Collections.Generic;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Scrutiny contains information regarding violations and duplicates for a dedupe customer
    /// </summary>
    public class Scrutiny : ModelBase
    {
        #region Fields

        private bool inScrutiny;
        private bool previouslyRejected;
        private string reasonRejected;
        private List<Violation> violations;
        private List<Duplicate> duplicates;
        private List<Duplicate> sopDuplicates;
        private bool flag;

        #endregion

        #region Properties

        public bool InScrutiny
        {
            get { return this.inScrutiny; }
            set
            {
                if (value == this.inScrutiny) return;
                this.inScrutiny = value;
                OnPropertyChanged("InScrutiny");
            }
        }
        
        public bool PreviouslyRejected
        {
            get { return this.previouslyRejected; }
            set
            {
                if (value == this.previouslyRejected) return;
                this.previouslyRejected = value;
                OnPropertyChanged("PreviouslyRejected");
            }
        }
        
        public string ReasonRejected
        {
            get { return this.reasonRejected; }
            set
            {
                if (value == this.reasonRejected) return;
                this.reasonRejected = value;
                OnPropertyChanged("ReasonRejected");
            }
        }
         
         
   
        public List<Violation> Violations
        {
            get { return this.violations; }
            set
            {
                if (value == this.violations) return;
                this.violations = value;
                OnPropertyChanged("Violations");
            }
        }
        
        public List<Duplicate> Duplicates
        {
            get { return this.duplicates; }
            set
            {
                if (value == this.duplicates) return;
                this.duplicates = value;
                OnPropertyChanged("Duplicates");
            }
        }
        
        public List<Duplicate> SOPDuplicates
        {
            get { return this.sopDuplicates; }
            set
            {
                if (value == this.sopDuplicates) return;
                this.sopDuplicates = value;
                OnPropertyChanged("SOPDuplicates");
            }
        }
         
        public bool Flag
        {
            get { return this.flag; }
            set
            {
                if (value == this.flag) return;
                this.flag = value;
                OnPropertyChanged("Flag");
            }
        }
         
         
                
      

        #endregion
    }
}
