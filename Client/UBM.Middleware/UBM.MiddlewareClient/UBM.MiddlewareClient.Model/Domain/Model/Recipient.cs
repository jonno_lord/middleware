﻿using System.Collections.ObjectModel;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Recipient represents someone receiving a rejection email. This is used in conjunction with ADS to resolve email addresses
    /// </summary>
    public class Recipient : ModelBase
    {
        #region Fields

        private string name;
        private ObservableCollection<string> resolvedAddresses = new ObservableCollection<string>();
        private bool isGroup;
        private bool isResolved;
        private bool isSelected;

        #endregion

        #region Properties

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                OnPropertyChanged("Name");
            }
        }
        
        public ObservableCollection<string> ResolvedAddresses
        {
            get { return this.resolvedAddresses; }
            set
            {
                if (value == this.resolvedAddresses) return;
                this.resolvedAddresses = value;
                OnPropertyChanged("ResolvedAddresses");
            }
        }
        
        public bool IsGroup
        {
            get { return this.isGroup; }
            set
            {
                if (value == this.isGroup) return;
                this.isGroup = value;
                OnPropertyChanged("IsGroup");
            }
        }
        
        public bool IsResolved
        {
            get { return this.isResolved; }
            set
            {
                if (value == this.isResolved) return;
                this.isResolved = value;
                OnPropertyChanged("IsResolved");
            }
        }
        
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion
    }
}
