﻿
namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Order contains information about a customer order
    /// </summary>
    public class Order : ModelBase
    {
        #region Fields

        private int id;
        private string number;
        private double? amount;
        private string currency;
        private string lines;
        private string systemName;

        #endregion

        #region Properties

        public int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }
         
        public string Number
        {
            get { return this.number; }
            set
            {
                if (value == this.number) return;
                this.number = value;
                OnPropertyChanged("Number");
            }
        }
        
        public double? Amount
        {
            get { return this.amount; }
            set
            {
                if (value == this.amount) return;
                this.amount = value;
                OnPropertyChanged("Amount");
            }
        }
        
        public string Currency
        {
            get { return this.currency; }
            set
            {
                if (value == this.currency) return;
                this.currency = value;
                OnPropertyChanged("Currency");
            }
        }
        
        public string Lines
        {
            get { return this.lines; }
            set
            {
                if (value == this.lines) return;
                this.lines = value;
                OnPropertyChanged("Lines");
            }
        }
        
        public string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }

        #endregion
    }
}
