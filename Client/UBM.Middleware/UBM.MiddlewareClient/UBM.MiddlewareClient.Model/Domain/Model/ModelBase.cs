﻿using System.ComponentModel;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// All Model classes inherit from model base. It implements the INotifyPropertyChanged interface to update WPF bindings
    /// </summary>
    public class ModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
