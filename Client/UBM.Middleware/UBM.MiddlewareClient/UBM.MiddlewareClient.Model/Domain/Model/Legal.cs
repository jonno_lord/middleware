﻿using System;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Legal contains legal hold information about a customer. 
    /// </summary>
    public class Legal : ModelBase
    {
        #region Fields

        private bool hold;
        private DateTime? holdDate;
        private string reason;
        private string userName;
        private string entityNumber;

        #endregion

        #region Properties

        public bool Hold
        {
            get { return this.hold; }
            set
            {
                if (value == this.hold) return;
                this.hold = value;
                OnPropertyChanged("Hold");
            }
        }
 
        public DateTime? HoldDate
        {
            get { return this.holdDate; }
            set
            {
                if (value == this.holdDate) return;
                this.holdDate = value;
                OnPropertyChanged("HoldDate");
            }
        }
         
        public string Reason
        {
            get { return this.reason; }
            set
            {
                if (value == this.reason) return;
                this.reason = value;
                OnPropertyChanged("Reason");
            }
        }
        
        public string UserName
        {
            get { return this.userName; }
            set
            {
                if (value == this.userName) return;
                this.userName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string EntityNumber
        {
            get { return this.entityNumber; }
            set
            {
                if (value == this.entityNumber) return;
                this.entityNumber = value;
                OnPropertyChanged("EntityNumber");
            }
        }
         
        #endregion           

    }
}
