﻿using System;

namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// Contains a log of dates for the different stages of processing in Middleware for a customer
    /// </summary>
    public class StatusLog : ModelBase
    {
        #region Fields

        private DateTime? jdeToMiddleware;
        private DateTime? sourceToMiddleware;
        private DateTime? middlewareToMiddlewareIn;
        private DateTime? middlewareToMiddlewareOut;
        private DateTime? middlewareToJDE;
        private DateTime? middlewareToSource;
        private DateTime? rejectedFromJDE;
        private DateTime? cancelledFromJDE;

        private DateTime? rejectedFromDedupe;
        private DateTime? acceptedInDedupe;
        private DateTime? lastAffected;

        #endregion

        #region Properties

        public DateTime? JDEToMiddleware
        {
            get { return this.jdeToMiddleware; }
            set
            {
                if (value == this.jdeToMiddleware) return;
                this.jdeToMiddleware = value;
                OnPropertyChanged("JDEToMiddleware");
            }
        }

        public DateTime? SourceToMiddleware
        {
            get { return this.sourceToMiddleware; }
            set
            {
                if (value == this.sourceToMiddleware) return;
                this.sourceToMiddleware = value;
                OnPropertyChanged("SourceToMiddleware");
            }
        }

        public DateTime? MiddlewareToMiddlewareIn
        {
            get { return this.middlewareToMiddlewareIn; }
            set
            {
                if (value == this.middlewareToMiddlewareIn) return;
                this.middlewareToMiddlewareIn = value;
                OnPropertyChanged("MiddlewareToMiddlewareIn");
            }
        }
        
        public DateTime? MiddlewareToMiddlewareOut
        {
            get { return this.middlewareToMiddlewareOut; }
            set
            {
                if (value == this.middlewareToMiddlewareOut) return;
                this.middlewareToMiddlewareOut = value;
                OnPropertyChanged("MiddlewareToMiddlewareOut");
            }
        }
        
        public DateTime? MiddlewareToJDE
        {
            get { return this.middlewareToJDE; }
            set
            {
                if (value == this.middlewareToJDE) return;
                this.middlewareToJDE = value;
                OnPropertyChanged("MiddlewareToJDE");
            }
        }
        
        public DateTime? MiddlewareToSource
        {
            get { return this.middlewareToSource; }
            set
            {
                if (value == this.middlewareToSource) return;
                this.middlewareToSource = value;
                OnPropertyChanged("MiddlewareToSource");
            }
        }
        
        public DateTime? RejectedFromJDE
        {
            get { return this.rejectedFromJDE; }
            set
            {
                if (value == this.rejectedFromJDE) return;
                this.rejectedFromJDE = value;
                OnPropertyChanged("RejectedFromJDE");
            }
        }
        
        public DateTime? CancelledFromJDE
        {
            get { return this.cancelledFromJDE; }
            set
            {
                if (value == this.cancelledFromJDE) return;
                this.cancelledFromJDE = value;
                OnPropertyChanged("CancelledFromJDE");
            }
        }

        public DateTime? RejectedFromDedupe
        {
            get { return this.rejectedFromDedupe; }
            set
            {
                if (value == this.rejectedFromDedupe) return;
                this.rejectedFromDedupe = value;
                OnPropertyChanged("RejectedFromDedupe");
            }
        }
        
        public DateTime? AcceptedInDedupe
        {
            get { return this.acceptedInDedupe; }
            set
            {
                if (value == this.acceptedInDedupe) return;
                this.acceptedInDedupe = value;
                OnPropertyChanged("AcceptedInDedupe");
            }
        }
         
        public DateTime? LastAffected
        {
            get { return this.lastAffected; }
            set
            {
                if (value == this.lastAffected) return;
                this.lastAffected = value;
                OnPropertyChanged("LastAffected");
            }
        }

        #endregion
    }
}
