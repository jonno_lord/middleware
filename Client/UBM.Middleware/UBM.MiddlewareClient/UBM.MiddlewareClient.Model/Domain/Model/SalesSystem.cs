﻿
namespace UBM.MiddlewareGUI.Model
{
    /// <summary>
    /// SalesSystem is populated from the MiddlewareSystems database. An object is created for each SOP system in Middleware.
    /// </summary>
    public class SalesSystem : ModelBase
    {
        #region Fields

        private string name;
        private bool isSelected;

        #endregion

        #region Properties

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                OnPropertyChanged("Name");
            }
        }
        
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion

    }
}
