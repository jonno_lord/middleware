﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Domain.Receipt
{
    public class Receipt : ModelBase
    {
        #region Fields

        private int id;
        private string systemName;

        private string receiptNumber;
        private string orderNumber;

        private DateTime? processedDate; 
        private string urn;

        private string customerName;
        private string currency;
        private double amount;

        private bool isSelected;
        
        private ContactDetails contactDetails;
        private ObservableCollection<LineDetail> products;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }

        /// <summary>
        /// Gets or sets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        public string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }

       
        public string ReceiptNumber
        {
            get { return this.receiptNumber; }
            set
            {
                if (value == this.receiptNumber) return;
                this.receiptNumber = value;
                OnPropertyChanged("ReceiptNumber");
            }
        }
        
        public string OrderNumber
        {
            get { return this.orderNumber; }
            set
            {
                if (value == this.orderNumber) return;
                this.orderNumber = value;
                OnPropertyChanged("OrderNumber");
            }
        }
        
        public DateTime? ProcessedDate
        {
            get { return this.processedDate; }
            set
            {
                if (value == this.processedDate) return;
                this.processedDate = value;
                OnPropertyChanged("ProcessedDate");
            }
        }
         
        
        public string URN
        {
            get { return this.urn; }
            set
            {
                if (value == this.urn) return;
                this.urn = value;
                OnPropertyChanged("URN");
            }
        }
        
        public string CustomerName
        {
            get { return this.customerName; }
            set
            {
                if (value == this.customerName) return;
                this.customerName = value;
                OnPropertyChanged("CustomerName");
            }
        }
        
        public string Currency
        {
            get { return this.currency; }
            set
            {
                if (value == this.currency) return;
                this.currency = value;
                OnPropertyChanged("Currency");
            }
        }
        
        public double Amount
        {
            get { return this.amount; }
            set
            {
                if (value == this.amount) return;
                this.amount = value;
                OnPropertyChanged("Amount");
            }
        }
        
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
               
        public ContactDetails ContactDetails
        {
            get { return this.contactDetails; }
            set
            {
                if (value == this.contactDetails) return;
                this.contactDetails = value;
                OnPropertyChanged("ContactDetails");
            }
        }

        
        public ObservableCollection<LineDetail> Products
        {
            get { return this.products; }
            set
            {
                if (value == this.products) return;
                this.products = value;
                OnPropertyChanged("Products");
            }
        }

        #endregion

        #region Public Methods

        //private static 

        //public static Receipt CreateReceipt(vwReceiptsAggregate receipt)
        //{
        //    if (receipt.Address == null) receipt.Address = "";

        //    string[] split = receipt.Address.Replace("\r\n", ";").Split(';');
        //    string address1 = split.Length > 0 ? split[0] : null;
        //    string address2 = split.Length > 1 ? split[1] : null;
        //    string address3 = split.Length > 2 ? split[2] : null;

        //    return new Receipt
        //    {
        //        ID = (int) receipt.ReceiptNumber,
        //        SystemName = receipt.SystemName,
        //        Amount = (double) (receipt.Amount ?? 0),
        //        Currency = receipt.Currency,
        //        CustomerName = receipt.CustomerName,
        //        URN = receipt.URN.ToString(),
        //        ReceiptNumber = receipt.ReceiptNumber.ToString(),
        //        OrderNumber = receipt.OrderNumber.ToString(),
        //        ProcessedDate = receipt.Processed,
        //        //Products = SqlDataAccess.GetReceiptLines(receipt.ReceiptNumber.ToString(), receipt.SystemName),
        //        ContactDetails = new ContactDetails
        //        {
        //            AddressLine1 = address1,
        //            AddressLine2 = address2,
        //            AddressLine3 = address3,
        //            City = receipt.Town,
        //            County = receipt.County,
        //            Country = receipt.Country,
        //            Postcode = receipt.Postcode
        //        }
        //    };

        //}

        //public static Receipt CreateReceipt(DataRow row)
        //{

        //    string[] split = row["Address"].ToString().Replace("\r\n", ";").Split(';');
        //    string address1 = split.Length > 0 ? split[0] : null;
        //    string address2 = split.Length > 1 ? split[1] : null;
        //    string address3 = split.Length > 2 ? split[2] : null;

        //    return new Receipt
        //    {
        //        ID = int.Parse(row["ReceiptNumber"].ToString()),
        //        SystemName = row["SystemName"].ToString(),
        //        Amount = double.Parse(row["Amount"].ToString()),
        //        Currency = row["Currency"].ToString(),
        //        CustomerName = row["CustomerName"].ToString(),
        //        URN = row["URN"].ToString(),
        //        ReceiptNumber = row["ReceiptNumber"].ToString(),
        //        OrderNumber = row["OrderNumber"].ToString(),
        //        ProcessedDate = row["Processed"] == DBNull.Value ? null : (DateTime?)row["Processed"],
        //        //Products = SqlDataAccess.GetReceiptLines(row["ReceiptNumber"].ToString(), row["SystemName"].ToString()),
        //        ContactDetails = new ContactDetails
        //        {
        //            AddressLine1 = address1,
        //            AddressLine2 = address2,
        //            AddressLine3 = address3,
        //            City = row["Town"].ToString(),
        //            County = row["County"].ToString(),
        //            Country = row["Country"].ToString(),
        //            Postcode = row["Postcode"].ToString()
        //        }
        //    };
        //}

        internal string GetIdentifier()
        {
            return "Receipt: " + ReceiptNumber + " - " + ContactDetails.CustomerName;
        }

        #endregion

    }
}
