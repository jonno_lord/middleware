﻿using System;
using System.Collections.ObjectModel;
using System.Data;

namespace UBM.MiddlewareClient.Model.Domain.Receipt
{
    public class LineDetail : ModelBase
    {

        #region Fields

        private string title;
        private string classification;
        private string size;
        private string colour;
        private DateTime issueDate;
        private string vatRate;
        private float netAmount;
        private float vatAmount;
        private float totalAmount;

        #endregion

        #region Properties

        public string Title
        {
            get { return this.title; }
            set
            {
                if (value == this.title) return;
                this.title = value;
                OnPropertyChanged("Title");
            }
        }

        public string Classification
        {
            get { return this.classification; }
            set
            {
                if (value == this.classification) return;
                this.classification = value;
                OnPropertyChanged("Classification");
            }
        }

        public string Size
        {
            get { return this.size; }
            set
            {
                if (value == this.size) return;
                this.size = value;
                OnPropertyChanged("Size");
            }
        }

        public string Colour
        {
            get { return this.colour; }
            set
            {
                if (value == this.colour) return;
                this.colour = value;
                OnPropertyChanged("Colour");
            }
        }

        public DateTime IssueDate
        {
            get { return this.issueDate; }
            set
            {
                if (value == this.issueDate) return;
                this.issueDate = value;
                OnPropertyChanged("IssueDate");
            }
        }

        public string VATRate
        {
            get { return this.vatRate; }
            set
            {
                if (value == this.vatRate) return;
                this.vatRate = value;
                OnPropertyChanged("VATRate");
            }
        }

        public float NetAmount
        {
            get { return this.netAmount; }
            set
            {
                if (value == this.netAmount) return;
                this.netAmount = value;
                OnPropertyChanged("netAmount");
            }
        }

        public float VATAmount
        {
            get { return this.vatAmount; }
            set
            {
                if (value == this.vatAmount) return;
                this.vatAmount = value;
                OnPropertyChanged("VATAmount");
            }
        }

        public float TotalAmount
        {
            get { return this.totalAmount; }
            set
            {
                if (value == this.totalAmount) return;
                this.totalAmount = value;
                OnPropertyChanged("TotalAmount");
            }
        }


        #endregion


        public static ObservableCollection<LineDetail> CreateLineDetails(string receiptNumber, string systemName)
        {

            ObservableCollection<LineDetail> details = new ObservableCollection<LineDetail>();

            DataSet dataSet = SqlDataAccess.GetReceiptLines(receiptNumber, systemName);

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {

                details.Add(new LineDetail
                                         {
                                             Title = row["Publication"].ToString(),
                                             Size = row["Size"].ToString(),
                                             Colour = row["Colour"].ToString(),
                                             Classification = row["Classification"].ToString(),
                                             IssueDate = (DateTime)row["Issue_date"],
                                             VATRate = row["Tax_Rate_Description"].ToString(),
                                             NetAmount = float.Parse(row["Output_net_price"].ToString()),
                                             VATAmount = float.Parse(row["Output_VAT_amount"].ToString()),
                                             TotalAmount = float.Parse(row["Output_gross_price"].ToString())
                                         });
            }

            return details;
        }

    }
}
