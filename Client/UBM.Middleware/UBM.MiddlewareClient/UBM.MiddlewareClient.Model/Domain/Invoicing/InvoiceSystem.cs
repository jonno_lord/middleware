﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UBM.MiddlewareClient.Model.Domain.Invoicing
{
    public class InvoiceSystem : ModelBase
    {
        public string SystemName { get; set; }

        public bool CurrentPublicationsPopulated { get; set; }
        public bool AllPublicationsPopulated { get; set; }

        private ObservableCollection<Publication> currentPublications;
        public ObservableCollection<Publication> CurrentPublications
        {
            get { return this.currentPublications; }
            set
            {
                if (value == this.currentPublications) return;
                this.currentPublications = value;
                OnPropertyChanged("CurrentPublications");
            }
        }

        private ObservableCollection<Publication> allPublications;
        public ObservableCollection<Publication> AllPublications
        {
            get { return this.allPublications; }
            set
            {
                if (value == this.allPublications) return;
                this.allPublications = value;
                OnPropertyChanged("AllPublications");
            }
        }

        private List<Warning> allWarnings;
        public List<Warning> AllWarnings
        {
            get { return this.allWarnings; }
            set
            {
                if (value == this.allWarnings) return;
                this.allWarnings = value;
                OnPropertyChanged("AllWarnings");
            }
        }

        private List<Warning> currentWarnings;
        public List<Warning> CurrentWarnings
        {
            get { return this.currentWarnings; }
            set
            {
                if (value == this.currentWarnings) return;
                this.currentWarnings = value;
                OnPropertyChanged("CurrentWarnings");
            }
        }

        private bool outstandingInvoices;
        public bool OutstandingInvoices
        {
            get { return this.outstandingInvoices; }
            set
            {
                if (value == this.outstandingInvoices) return;
                this.outstandingInvoices = value;
                OnPropertyChanged("OutstandingInvoices");
            }
        }

        private bool outstandingPublications;
        public bool OutstandingPublications
        {
            get { return this.outstandingPublications; }
            set
            {
                if (value == this.outstandingPublications) return;
                this.outstandingPublications = value;
                OnPropertyChanged("OustandingPublications");
            }
        }

        public InvoiceSystem(string systemName)
        {
            SystemName = systemName;
            CurrentPublications = new ObservableCollection<Publication>();
            AllPublications = new ObservableCollection<Publication>();
        }
    }
}
