﻿using System;
using System.Collections.ObjectModel;
using System.Data;

namespace UBM.MiddlewareClient.Model.Domain.Invoicing
{
    public class Invoice : AbstractSyncableModel
    {
        private int id;
        private string systemName;


        private Publication publication;
        private DateTime issueDate;
        private string status;
        private ObservableCollection<Warning> warnings;
        private bool isSelected;
        private bool ignoreWarnings;
        private bool outstandingFlag;

        #region Properties


        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public override int ID
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                OnPropertyChanged("ID");
            }
        }

        /// <summary>
        /// Gets or sets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        public override string SystemName
        {
            get { return this.systemName; }
            set
            {
                if (value == this.systemName) return;
                this.systemName = value;
                OnPropertyChanged("SystemName");
            }
        }


        /// <summary>
        /// Gets or sets the publication.
        /// </summary>
        /// <value>The publication.</value>
        public Publication Publication
        {
            get { return this.publication; }
            set
            {
                if (value == this.publication) return;
                this.publication = value;
                OnPropertyChanged("Publication");
            }
        }

        /// <summary>
        /// Gets or sets the issue date.
        /// </summary>
        /// <value>The issue date.</value>
        public DateTime IssueDate
        {
            get { return this.issueDate; }
            set
            {
                if (value == this.issueDate) return;
                this.issueDate = value;
                OnPropertyChanged("IssueDate");
            }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status
        {
            get { return this.status; }
            set
            {
                if (value == this.status) return;
                this.status = value;
                OnPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Gets or sets the warnings.
        /// </summary>
        /// <value>The warnings.</value>
        public ObservableCollection<Warning> Warnings
        {
            get { return this.warnings; }
            set
            {
                if (value == this.warnings) return;
                this.warnings = value;
                OnPropertyChanged("Warnings");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value == this.isSelected) return;
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [ignore warnings].
        /// </summary>
        /// <value><c>true</c> if [ignore warnings]; otherwise, <c>false</c>.</value>
        public bool IgnoreWarnings
        {
            get { return this.ignoreWarnings; }
            set
            {
                if (value == this.ignoreWarnings) return;
                this.ignoreWarnings = value;
                OnPropertyChanged("IgnoreWarnings");
            }
        }

        public bool OutstandingFlag
        {
            get { return this.outstandingFlag; }
            set
            {
                if (value == this.outstandingFlag) return;
                this.outstandingFlag = value;
                OnPropertyChanged("OutstandingFlag");
            }
        }

        public bool FutureFlag { get; set; }


         
        #endregion


        public static Invoice CreateInvoice(string systemName, DataRow row)
        {
            DateTime issueDate = (DateTime) row["Insert_Date"];
            bool outstanding = false;
            bool future = false;

            if (DateTime.Compare(issueDate, DateTime.Now.Subtract(new TimeSpan(31,0,0,0))) < 0)
            {
                outstanding = true;
            }

            if(DateTime.Compare(issueDate, DateTime.Now.AddDays(31.0)) > 0)
            {
                future = true;
            }

            return new Invoice
                       {
                           ID = CreateInvoiceId(row["Short_name"].ToString(), issueDate),
                           SystemName = systemName,
                           // @JP Sort this out
                           Publication = new Publication
                                             {
                                                 Code = row["Short_name"].ToString(),
                                                 Description = row["Long_name"].ToString()
                                             },
                           IssueDate = issueDate,
                           Status = "Accepted For Invoicing",
                           Warnings = new ObservableCollection<Warning>(),
                           OutstandingFlag = outstanding,
                           FutureFlag = future
                       };
        }


        public static int CreateInvoiceId(string code, DateTime dateTime)
        {
            return (code + dateTime).GetHashCode();
        }

        internal override string GetIdentifier()
        {
            return this.Publication.Code + "-" + this.IssueDate.ToShortDateString() + " " + this.Publication.Description;
        }

    }
}
