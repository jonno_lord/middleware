﻿using System;
using System.Collections.Generic;
using System.Data;

namespace UBM.MiddlewareClient.Model.Domain.Invoicing
{
    public class Warning : ModelBase
    {
        private int invoiceId;
        private string description;
        private string urn;
        private string customerName;


        
        public int InvoiceID
        {
            get { return this.invoiceId; }
            set
            {
                if (value == this.invoiceId) return;
                this.invoiceId = value;
                OnPropertyChanged("InvoiceID");
            }
        }
         

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return this.description; }
            set
            {
                if (value == this.description) return;
                this.description = value;
                OnPropertyChanged("Description");
            }
        }

        /// <summary>
        /// Gets or sets the URN.
        /// </summary>
        /// <value>The URN.</value>
        public string URN
        {
            get { return this.urn; }
            set
            {
                if (value == this.urn) return;
                this.urn = value;
                OnPropertyChanged("URN");
            }
        }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>The name of the customer.</value>
        public string CustomerName
        {
            get { return this.customerName; }
            set
            {
                if (value == this.customerName) return;
                this.customerName = value;
                OnPropertyChanged("CustomerName");
            }
        }


        private static List<Warning> CreateWarnings(DataSet dataSet)
        {
            List<Warning> warnings = new List<Warning>();

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                string description;

                if (string.IsNullOrEmpty(row["account_no"].ToString()) && string.IsNullOrEmpty(row["Scrutiny_Required_Id"].ToString()))
                {
                    description = "No JDE Account Number";
                }
                else
                {
                    description = "In Scrutiny";
                }

                warnings.Add(new Warning
                {
                    InvoiceID = Invoice.CreateInvoiceId(row["Short_name"].ToString(), (DateTime)row["Insert_date"]),
                    CustomerName = row["customer_name"].ToString(),
                    URN = row["urn_number"].ToString(),
                    Description = description
                });
            }

            return warnings;
        }

        public static List<Warning> CreateWarningsForSinglePublication(string systemName, string publicationName)
        {
            DataSet dataSet = SqlDataAccess.GetInvoiceWarnings(systemName, publicationName);
            return CreateWarnings(dataSet);
        }

        public static List<Warning> CreateWarnings(string systemName, bool recent = true)
        {
            DataSet dataSet = SqlDataAccess.GetInvoiceWarnings(systemName, recent);
            return CreateWarnings(dataSet);
        }
    }
}
