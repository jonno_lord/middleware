﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model
{
    public static class SqlDataAccess
    {



        // @DG:
        // Private method to update JDEAccountNumber.
        public static void UpdateJDEAccountNumber(int id, string systemName, string jdeAccountNumber)
        {
            using (SqlConnection conCommon = new SqlConnection(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON)))
            {
                conCommon.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "usp_SET_SystemAccountNumber";
                    cmd.Connection = conCommon;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("CustomerId", id);
                    cmd.Parameters.AddWithValue("SystemName", systemName);
                    cmd.Parameters.AddWithValue("JdeAccountNumber", jdeAccountNumber);

                    cmd.ExecuteNonQuery();

                }
                conCommon.Close();
            }
        }

        public static int GetJobStepsCount(int jobId)
        {
            DataSet dataSet = GetDataSetFromQuery("SELECT id"
                                            + " FROM dbo.Processes"
                                            + " WHERE jobid = " + jobId
                                            , ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_SYSTEM));

            return dataSet.Tables[0].Rows.Count;
        }

        public static void SelectReceiptBatch(int batchNumber, string systemName)
        {
            string connectionString = GetBatchReceiptConnectionString(systemName);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string query = "UPDATE dbo.ASOP_Credit_Card_Orders " +
                               "SET Batch_Selected = 1 " +
                               "WHERE [Batch_number] = " + batchNumber;

                SqlCommand command = new SqlCommand(query, connection);

                command.ExecuteNonQuery();
            }
        }

        public static List<int> GetOrdersNumbersInBatch(int batchNumber, string systemName)
        {
            List<int> orderUrns = new List<int>();

            string connectionString = GetBatchReceiptConnectionString(systemName);

            string query = "SELECT DISTINCT Order_Urn FROM dbo.ASOP_Credit_Card_Orders " +
                               "WHERE Batch_number = " + batchNumber +
                               " AND Receipt_Number IS NULL ";

            DataSet dataSet = GetDataSetFromQuery(query, connectionString);

            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                int? urn = (int?) row["Order_Urn"];
                
                if(urn != null)
                {
                    orderUrns.Add(urn.Value);    
                }
            }

            return orderUrns;

        }

        public static bool BatchSentToJde(int batchNumber, string systemName)
        {
            string connectionString = GetBatchReceiptConnectionString(systemName);

            string query = "SELECT MW_to_JDE FROM dbo.vw_Credit_Card_Orders " +
                               "WHERE Batch_number = " + batchNumber + " AND MW_to_JDE IS NULL";

            DataSet dataSet = GetDataSetFromQuery(query, connectionString);

            return dataSet.Tables[0].Rows.Count == 0 ? true : false;
        }

        public static void AllocateReceiptNumberToOrder(int orderUrn, string systemName, int receiptNumber, DateTime timeStamp)
        {
            string connectionString = GetBatchReceiptConnectionString(systemName);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlDateTime sqlDateTime = new SqlDateTime(timeStamp);

                string query = "UPDATE dbo.ASOP_Credit_Card_Orders " +
                               "SET Receipt_Number = " + receiptNumber +
                               ", Receipt_Date = '" + timeStamp.ToString("yyyy-MM-dd") +
                               "' WHERE [Order_urn] = " + orderUrn;

                SqlCommand command = new SqlCommand(query, connection);

                command.ExecuteNonQuery();
            }
        }

        public static bool BatchHasReceiptNumber(int batchNumber, string systemName)
        {
            string connectionString = GetBatchReceiptConnectionString(systemName);

            string query = "SELECT Receipt_Number FROM dbo.ASOP_Credit_Card_Orders " +
                               "WHERE Batch_number = " + batchNumber + " AND Receipt_Number IS NOT NULL";

            DataSet dataSet = GetDataSetFromQuery(query, connectionString);



            return dataSet.Tables[0].Rows.Count > 0 ? true : false;
        }

        public static string GetBatchReceiptConnectionString(string systemName)
        {
            switch (systemName)
            {
                case "ASOPUK":
                    return ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_ASOP_UK);
                case "ASOPDaltons":
                    return ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_ASOP_DALTONS);
                case "ASOPHolland":
                    return ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_ASOP_HOLLAND);
                default:
                    return "";
            }
        }

        // Invoices

        public static string GetPublicationInvoiceConnectionString(string systemName)
        {
            switch (systemName)
            {
                case "ASOPUK":
                    return ConnectionManager.GetConnectionString(ConnectionStringNames.ASOP_UK);
                case "ASOPDaltons":
                    return ConnectionManager.GetConnectionString(ConnectionStringNames.ASOP_DALTONS);
                case "ASOPHolland":
                    return ConnectionManager.GetConnectionString(ConnectionStringNames.ASOP_HOLLAND);
                default:
                    return "";
            }
        }

        public static DataSet GetPublicationInvoices(string systemName, bool recent)
        {
            string connectionString = GetPublicationInvoiceConnectionString(systemName);
            string storedProc;

            if(recent)
            {
                storedProc = "usp_SEL_PublicationInvoices";
            }
            else
            {
                storedProc = "usp_SEL_PublicationInvoicesAll";
            }

            return SelectStoredProcedure(connectionString, storedProc);
        }

        public static DataSet GetInvoiceWarnings(string systemName, bool recent)
        {
            string connectionString = GetPublicationInvoiceConnectionString(systemName);
            string storedProc;

            if (recent)
            {
                storedProc = "usp_SEL_PreInvoiceWarnings";
            }
            else
            {
                storedProc = "usp_SEL_PreInvoiceWarningsAll";
            }

            return SelectStoredProcedure(connectionString, storedProc);
        }


        public static DataSet GetInvoiceWarnings(string systemName, string publicationName)
        {
            DataSet dataSet = new DataSet();
            string connectionString = GetPublicationInvoiceConnectionString(systemName);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_PreInvoiceWarningsForSinglePublication", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@publication", publicationName);
                command.CommandTimeout = 120;
                command.Prepare();

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(dataSet);
            }

            return dataSet;
        }

        public static DataSet GetPublicationInvoices(string systemName, string publicationName)
        {
            DataSet dataSet = new DataSet();
            string connectionString = GetPublicationInvoiceConnectionString(systemName);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_PublicationInvoicesForSinglePublication", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@publication", publicationName);
                command.CommandTimeout = 120;
                command.Prepare();

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(dataSet);
            }

            return dataSet;
        }

        public static DataSet SelectStoredProcedure(string connectionString, string procName)
        {
            if (string.IsNullOrEmpty(connectionString) || string.IsNullOrEmpty(procName)) return null;

            DataSet dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(procName, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 600;

                command.Prepare();

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(dataSet);
            }

            return dataSet;
        }
        

        public static DataSet GetOrderDetails(string orderNumber, string batchNumber, string systemName)
        {
            string query = "SELECT * FROM dbo.vwOrders" + systemName + " WHERE [Order Number] = " + orderNumber;

            if(!string.IsNullOrEmpty(batchNumber) && batchNumber != "0")
            {
                query += " AND [Batch Number] = " + batchNumber;
            }

            return GetDataSetFromQuery(query, ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON));
        }

        public static DataSet GetReceiptLines(string receiptNumber, string systemName)
        {
            return GetDataSetFromQuery("SELECT Publication,Size,Colour,Classification,Issue_date,Tax_Rate_Description,Output_net_price,Output_VAT_amount,Output_gross_price" 
                                            + " FROM dbo.vwReceipts"
                                            + " WHERE [ReceiptNumber] = " + receiptNumber + " AND [SystemName] = '" + systemName + "'"
                                            , ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON));
        }

        public static DataSet GetInvoiceBatchNumbers(string systemName, string titleCard, DateTime rundate)
        {
            return GetDataSetFromQuery("SELECT batch_number FROM dbo.Orders " 
                                        + " WHERE Title_Card_short LIKE '" + titleCard + "'"
                                        + " AND SourceToMiddleware > '"  + rundate.Date + "'", GetBatchReceiptConnectionString(systemName));
        }

        public static DataSet GetDataSetFromQuery(string query, string connectionString)
        {
            DataSet dataset = new DataSet();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                adapter.Fill(dataset);
            }
            return dataset;
        }


    }
}
