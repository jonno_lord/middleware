﻿using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface ICustomerDao
    {
        Customer GetCustomer(int id, string systemName);
        List<Customer> SearchCustomers(SearchInfo searchInfo);
        List<Email> SearchEmails(SearchInfo searchInfo);
        List<Legal> SearchLegal(SearchInfo searchInfo);

        List<Customer> GetDedupeCustomers();
        List<Legal> GetLegalRecords(Customer customer);
        List<Rejection> GetRejectionReasons();


        void LegalHold(int id, string systemName, bool hold, string reason);
        void UpdateCustomerVat(int id, string systemName, string vat);



        void CommitAcceptedCustomer(int id, string systemName);
        void CommitRejectedCustomer(int id, string systemName);

        void InsertRejection(int id, string systemName, string reason, string recipient, string cc,
                             string body, bool sendNotification);


        List<UserAction> GetAllUserActions();
        List<UserAction> GetUserActionsForOtherUsers();
        UserAction GetUserActionForCurrentUser(int id, string systemName);

        void DeleteCustomerInUserActions(int id, string systemName);
        void EditUserAction(int id, string systemName, string body, string reason, string recipient, string cc, bool send);

        void AcceptCustomerInUserActions(int id, string systemName, string jdeAccount);
        void RejectCustomerInUserActions(int id, string systemName, string salesperson);

        List<Email> GetRejectionEmails(int howMany = 0);
        List<Legal> GetLegalHistory(int howMany = 0);

    }
}
