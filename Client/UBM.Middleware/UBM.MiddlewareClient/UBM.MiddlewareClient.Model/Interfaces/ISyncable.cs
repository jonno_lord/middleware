﻿using System;
using System.Collections.ObjectModel;
using UBM.MiddlewareClient.Model.Domain;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface ISyncable
    {
        int ID { get; set; }
        string SystemName { get; set; }

        bool IsLocked { get; set; }
        string LockedBy { get; set; }
        DateTime LockTime { get; set; }

        bool IsViewed { get; set; }
        ObservableCollection<ViewerWrapper> Viewers { get; set; }

        string Lock(string userName);
        string Unlock(string userName);

        string AddViewer(string userName);
        string RemoveViewer(string userName);

        string GetTypeName();
    }
}
