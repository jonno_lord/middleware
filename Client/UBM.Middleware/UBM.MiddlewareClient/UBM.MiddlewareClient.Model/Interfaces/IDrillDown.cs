﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public delegate void DrillDownEventHandler(object sender);

    public interface IDrillDown
    {
        event DrillDownEventHandler OnDrillDown;
        void DrillDown(object sender);
    }
}
