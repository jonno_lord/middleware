﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Helper;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface INotificationService
    {
        List<string> GetUsersGroups(string userName);

        bool AuthenticateUser(string userName);

        string FireMiddlewareJob(int jobId);

        bool SendEmail(EmailMessageWrapper messageWrapper);

        List<string> ResolveEmail(string name);
    }
}
