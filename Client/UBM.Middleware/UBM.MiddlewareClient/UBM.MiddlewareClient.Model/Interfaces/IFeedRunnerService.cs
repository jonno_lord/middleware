﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.FeedRunner;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface IFeedRunnerService
    {
        List<BatchFeed> GetFeedBatches(DateTime batchDate);
        List<FileHistory> GetFileHistory(FileFeed file);

        event DrillDownEventHandler OnDrillDown;
    }
}
