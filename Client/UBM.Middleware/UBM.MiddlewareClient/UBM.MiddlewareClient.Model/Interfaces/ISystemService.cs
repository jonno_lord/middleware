﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Interfaces
{
    public interface ISystemService
    {
        List<SalesSystem> GetSalesSystems();
        int GetJobIdFromString(string name);
        int GetJobIdFromJobName(string name);
        string GetJobShortNameFromId(int id);
        int GetJobStepsCount(int jobId);

    }
}
