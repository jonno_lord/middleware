﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;
using UBM.NotificationClient;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Service
{
    public class UserService : IUserService
    {
        private readonly IUserDao userDao;
        private readonly INotificationService notificationService;

        private string pageCount;

        private readonly List<string> userGroupMembership;

        private  List<string> GetGroupsForThisUser()
        {
            return notificationService.GetUsersGroups(Environment.UserName);
            
        }

        public UserService(INotificationService notificationService) : this(new UserDao(), notificationService)
        {

        }

        public UserService(IUserDao userDao, INotificationService notificationService)
        {
            TraceHelper.Trace(TraceType.Service);

            this.userDao = userDao;
            this.notificationService = notificationService;
            this.pageCount = "";

            userGroupMembership = GetGroupsForThisUser();
        }

        public void UpdateMostUsed()
        {
           
            userDao.UpdateMostUsed(pageCount);
        }

        public List<PageAccess> GetPageAccessRights()
        {
            TraceHelper.Trace(TraceType.Service);

            List<vwUserGroupAccess> userGroupAccess = userDao.GetUserGroupAccess();

            List<string> accessStrings = new List<string>();

            // find out if user is in any of the groups
            foreach (vwUserGroupAccess userGroup in userGroupAccess)
            {
                if (userGroupMembership.Contains(userGroup.GroupName))
                {
                    accessStrings.Add(userGroup.AccessString);
                }
            }

            AccessStringParser parser = new AccessStringParser(accessStrings);

            return parser.Parse();
        }

        public List<string> GetSearchTypes()
        {
            TraceHelper.Trace(TraceType.Service);

            List<string> searchTypes = new List<string>();
            CountryGroup country;

            List<CountryGroup> countryGroups = userDao.GetCountryGroups();

            foreach (string group in userGroupMembership)
            {
                country = countryGroups.FirstOrDefault(c => c.GroupName == group);
                if (country != null)
                {
                    string[] splitTypes = StringHelper.Tidy(country.SearchType).Split(',');
                    searchTypes.AddRange(splitTypes);
                }
            }

            return StringHelper.RemoveDuplicates(searchTypes).ToList();
        }

        public void UpdatePageCountString(ClientPageNumber page)
        {
            //LogHelper.Trace(TraceType.Service);

            if (page != ClientPageNumber.HomePage && page != ClientPageNumber.CustomerDetails)
            {
                // generate new one

                if (pageCount != null && pageCount.Contains("p" + (int)page))
                {
                    string[] split = pageCount.Split(',');

                    foreach (string token in split)
                    {
                        if (token.StartsWith("p" + (int)page))
                        {
                            string[] splitToken = token.Split('=');
                            int count = int.Parse(splitToken[1]);
                            count++;

                            string finishedToken = splitToken[0] + "=" + count;

                            pageCount = pageCount.Replace(token, finishedToken);

                            break;
                        }
                    }
                }
                else
                {
                    pageCount += "p" + (int)page + "=" + "1,";
                }

                UpdateMostUsed();
            }
        }

        public List<int> GetTopFivePages()
        {
            TraceHelper.Trace(TraceType.Service);

            MostUsed most = userDao.GetMostUsed();

            if (most == null)
            {
                userDao.InsertMostUsed();
            }
            else
            {
                pageCount = most.PageCountString;
            }


            int[] topPages = new int[5];

            if (!string.IsNullOrEmpty(pageCount))
            {
                string[] split = pageCount.Split(',');

                Dictionary<int, int> pageDictionary = new Dictionary<int, int>();

                foreach (string token in split)
                {
                    if (token != "")
                    {
                        string[] splitToken = token.Split('=');
                        pageDictionary.Add(int.Parse(splitToken[0].Substring(1)), int.Parse(splitToken[1]));
                    }
                }

                IEnumerable<KeyValuePair<int, int>> sortedDict =
                    from entry in pageDictionary orderby entry.Value descending select entry;
                sortedDict = sortedDict.Take(5);

                int counter = 0;
                foreach (KeyValuePair<int, int> item in sortedDict)
                {
                    topPages[counter] = item.Key;
                    counter++;
                }
            }

            return topPages.ToList();
        }
      
    }
}
