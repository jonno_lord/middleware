﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.NotificationClient;

namespace UBM.MiddlewareClient.Model.Service
{
    public class NotificationService : INotificationService
    {
        public List<string> GetUsersGroups(string userName)
        {
            return NotificationServiceClient.GetUsersGroups(userName).ToList();
        }

        public bool AuthenticateUser(string userName)
        {
            return NotificationServiceClient.AuthenticateUser(userName);
        }

        public string FireMiddlewareJob(int jobId)
        {
            return NotificationServiceClient.FireMiddlewareJob(jobId);
        }

        public bool SendEmail(EmailMessageWrapper messageWrapper)
        {
            return NotificationServiceClient.Email(messageWrapper.From ?? "",
                                                    messageWrapper.To,
                                                    messageWrapper.CC ?? "",
                                                    messageWrapper.Subject,
                                                    messageWrapper.Body,
                                                    messageWrapper.Priority,
                                                    messageWrapper.IsHtml);
        }

        public List<string> ResolveEmail(string name)
        {

            string[] addresses = null;

            try
            {
                addresses = NotificationServiceClient.ResolveEmail(name);
            }
            catch (Exception exception)
            {
                // throws an exception for Aviation addresses
            }

            if(addresses == null || addresses.Count() == 0)
            {
                return new List<string>();
            }

            return addresses.ToList();
        }
    }
}
