﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.FeedRunner;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Integration;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Model.Service
{
    public class FeedRunnerService : IFeedRunnerService, IDisposable
    {
        private readonly IFeedRunnerDao feedRunnerDao;

        private List<BatchFeed> batches;

        public FeedRunnerService() : this(new FeedRunnerDao())
        {
            
        }

        public FeedRunnerService(IFeedRunnerDao feedRunnerDao)
        {
            TraceHelper.Trace(TraceType.Service);
             
            this.feedRunnerDao = feedRunnerDao;
            this.batches = new List<BatchFeed>();
        }


        public List<BatchFeed> GetFeedBatches(DateTime batchDate)
        {
            TraceHelper.Trace(TraceType.Service);

            batches.ForEach(c=>c.OnDrillDown -= OnDrillDown);

            batches = feedRunnerDao.GetFeedBatches(batchDate);

            batches.ForEach(c => c.OnDrillDown += OnDrillDown);

            return batches;
        }

        public List<FileHistory> GetFileHistory(FileFeed file)
        {
            TraceHelper.Trace(TraceType.Service);

            return feedRunnerDao.GetFileHistory(file);
        }

        public event DrillDownEventHandler OnDrillDown;


        public void Dispose()
        {
            batches.ForEach(c => c.OnDrillDown -= OnDrillDown);
        }
    }
}
