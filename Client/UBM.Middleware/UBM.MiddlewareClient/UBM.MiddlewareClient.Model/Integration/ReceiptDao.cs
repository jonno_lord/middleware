﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LinqKit;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Domain.Receipt;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Integration
{
    internal class ReceiptDao : IReceiptDao 
    {
        ReceiptDataContext receiptDataContext;

        private IQueryable<vwReceiptsAggregate> ReceiptsSearchDefinition(SearchInfo searchInfo)
        {
            var predicate = PredicateBuilder.True<vwReceiptsAggregate>();

            IQueryable<vwReceiptsAggregate> queryable = receiptDataContext.vwReceiptsAggregates;

            queryable = queryable.Where(SearchHelper.MakeFilter<vwReceiptsAggregate>(searchInfo.By, searchInfo.Term, searchInfo.Matching));

            queryable = queryable.Where(predicate).OrderBy(c => c.CustomerName).Take(searchInfo.MaxRecords);
            queryable = queryable.Where(c => searchInfo.Systems.Contains(c.SystemName));

            return (IQueryable<vwReceiptsAggregate>) queryable;
        }

        private List<Receipt> GetReceiptsWithFilterApplied(IQueryable<vwReceiptsAggregate> filtered)
        {
            List<Receipt> receipts = new List<Receipt>();

            foreach (var receipt in filtered)
            {
                if (receipt.Address == null) receipt.Address = "";

                string[] split = receipt.Address.Replace("\r\n", ";").Split(';');
                string address1 = split.Length > 0 ? split[0] : null;
                string address2 = split.Length > 1 ? split[1] : null;
                string address3 = split.Length > 2 ? split[2] : null;


                receipts.Add(new Receipt
                {
                    ID = int.Parse(receipt.ReceiptNumber ?? "0"),
                    SystemName = receipt.SystemName,
                    Amount = (double)(receipt.Amount ?? 0),
                    Currency = receipt.Currency,
                    CustomerName = receipt.CustomerName,
                    URN = receipt.URN.ToString(),
                    ReceiptNumber = receipt.ReceiptNumber.ToString(),
                    OrderNumber = receipt.OrderNumber.ToString(),
                    ProcessedDate = receipt.Processed,
                    //Products = SqlDataAccess.GetReceiptLines(receipt.ReceiptNumber.ToString(), receipt.SystemName),
                    ContactDetails = new ContactDetails
                    {
                        AddressLine1 = address1,
                        AddressLine2 = address2,
                        AddressLine3 = address3,
                        City = receipt.Town,
                        County = receipt.County,
                        Country = receipt.Country,
                        Postcode = receipt.Postcode
                    }
                });
            }

            return receipts;
        }


        public List<Receipt> SearchReceipts(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Database);

            using(receiptDataContext = ReceiptDataContext.ReadOnly)
            {
                var filter = ReceiptsSearchDefinition(searchInfo);

                return GetReceiptsWithFilterApplied(filter);
            }
        }

        public List<Batch> GetBatches()
        {
            TraceHelper.Trace(TraceType.Database);

            var query = from c in ReceiptDataContext.ReadOnly.vwBatchReceipts select c;


            List<Batch> batches = new List<Batch>();

            foreach (vwBatchReceipt batch in query)
            {

                batches.Add(new Batch
                {
                    ID = batch.ID,
                    SystemName = batch.SystemName,
                    IsSelected = false,
                    Extracted = batch.Extracted,
                    ReceiptCount = batch.ReceiptCount ?? 0,
                    LinesCount = batch.LinesTotal ?? 0,
                    Currency = batch.Currency,
                    Amount = (double)(batch.Amount ?? 0)

                });
            }

            return batches;
        }

        public int GetIncrementedReceiptNumber(string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            int? newReceiptNumber;

            using (receiptDataContext = new ReceiptDataContext())
            {
                ReceiptNumber receiptNumber = receiptDataContext.ReceiptNumbers.Single(c => c.SystemName == systemName);
                receiptNumber.Count++;
                newReceiptNumber = receiptNumber.Count;
                receiptDataContext.SubmitChanges();
            }


            if (newReceiptNumber != null)
            {
                return (int)newReceiptNumber;
            }
            else
            {
                throw new NoNullAllowedException("Receipt number is null");
            }



        }

        public List<LineDetail> GetReceiptLineDetails(int receiptNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            List<LineDetail> lines = new List<LineDetail>();

            var query = from c in ReceiptDataContext.ReadOnly.vwReceipts
                        where c.ReceiptNumber == receiptNumber && c.SystemName == systemName
                        select c;

            foreach (var line in query)
            {
                lines.Add(new LineDetail
                              {
                                  Title = line.Publication,
                                  Size = line.Size,
                                  Colour = line.Colour,
                                  Classification = line.Classification,
                                  IssueDate = (DateTime)line.Issue_date,
                                  VATAmount =(float)(line.Output_VAT_amount ?? 0),
                                  VATRate = line.Tax_Rate_Description,
                                  NetAmount = (float) (line.Net_price ?? 0),
                                  TotalAmount = (float)(line.Output_gross_price ?? 0)

                              });
            }

            return null;

        }

        public bool BatchSentToJde(int batchNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            return SqlDataAccess.BatchSentToJde(batchNumber, systemName);
        }

        public bool BatchHasReceiptNumber(int batchNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            return SqlDataAccess.BatchHasReceiptNumber(batchNumber, systemName);
        }

        public void SelectReceiptBatch(int batchNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            SqlDataAccess.SelectReceiptBatch(batchNumber, systemName);
        }

        public List<int> GetOrdersNumbersInBatch(int batchNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            return SqlDataAccess.GetOrdersNumbersInBatch(batchNumber, systemName);
        }

        public void AllocateReceiptNumberToOrder(int orderUrn, string systemName, int receiptNumber, DateTime timeStamp)
        {
            TraceHelper.Trace(TraceType.Database);

            SqlDataAccess.AllocateReceiptNumberToOrder(orderUrn, systemName, receiptNumber, timeStamp);
        }

        public void InsertReceiptCopy(int receiptNumber, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            using (receiptDataContext = new ReceiptDataContext())
            {
                ReceiptCopy receiptCopy = new ReceiptCopy
                                              {
                                                  ReceiptNumber = receiptNumber,
                                                  SystemName = systemName,
                                                  RequestedDate = DateTime.Now
                                              };

                receiptDataContext.ReceiptCopies.InsertOnSubmit(receiptCopy);
                receiptDataContext.SubmitChanges();
            }
        }
    }
}
