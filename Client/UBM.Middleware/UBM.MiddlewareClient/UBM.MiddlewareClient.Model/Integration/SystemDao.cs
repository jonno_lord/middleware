﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Integration
{
    internal class SystemDao : ISystemDao 
    {

        public List<SalesSystem> GetSalesSystems()
        {
            TraceHelper.Trace(TraceType.Database);

            List<SalesSystem> salesSystems = new List<SalesSystem>();
            
            var systems = from s in SystemDataContext.ReadOnly.SystemNames 
                          select s;

            foreach (var systemName in systems)
            {
                salesSystems.Add(new SalesSystem
                                     {
                                         Name = systemName.SystemName1,
                                         IsSelected = true
                                     });
            }

            return salesSystems;
        }

        public int GetJobIdFromString(string name)
        {
            TraceHelper.Trace(TraceType.Database);

            var jobId = from c in SystemDataContext.ReadOnly.JobOrders 
                        where c.JobDescription == name 
                        select c.JobId;

            return jobId.First() ?? 0;
        }

        public int GetJobIdFromJobName(string name)
        {
            TraceHelper.Trace(TraceType.Database);

            var jobId = from c in SystemDataContext.ReadOnly.Jobs
                        where c.ShortName == name
                        select c.id;

            return jobId.First();
        }

        public string GetJobShortNameFromId(int id)
        {
            TraceHelper.Trace(TraceType.Database);

            var shortname = from c in SystemDataContext.ReadOnly.Jobs 
                            where c.id == id 
                            select c.ShortName;

            return shortname.FirstOrDefault();

        }

        public int GetJobStepsCount(int jobId)
        {
            TraceHelper.Trace(TraceType.Database);

            var rows = from c in SystemDataContext.ReadOnly.Processes
                        where c.Jobid == jobId
                        select c;

            return rows.Count();

        }
    }
}
