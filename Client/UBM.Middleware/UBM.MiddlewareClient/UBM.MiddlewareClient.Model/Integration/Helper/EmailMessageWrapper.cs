﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.NotificationClient;

namespace UBM.MiddlewareClient.Model.Helper
{
    public class EmailMessageWrapper
    {
        public string From { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Priority Priority { get; set; }
        public bool IsHtml { get; set; }
    }
}
