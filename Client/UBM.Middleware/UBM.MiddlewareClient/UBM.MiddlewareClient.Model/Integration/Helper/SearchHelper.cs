﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UBM.MiddlewareClient.Model.Enums;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Helper
{
    public class SearchHelper
    {

        public static string SearchByConverter(SearchBy by)
        {
            switch (by)
            {
                case SearchBy.CustomerName:
                    return "CustomerName";
                case SearchBy.URN:
                    return "URN";
                case SearchBy.Reason:
                    return "ReasonRejected";
                case SearchBy.JDENumber:
                    return "JDEAccountNumber";
                case SearchBy.ActionedBy:
                    return "UserName";
                case SearchBy.BatchNumber:
                    return "BatchNumber";
                case SearchBy.OrderNumber:
                    return "OrderNumber";
                case SearchBy.Postcode:
                    return "Postcode";
                case SearchBy.ReceiptNumber:
                    return "ReceiptNumber";
                case SearchBy.Sender:
                    return "Sender";
                default:
                    throw new Exception("SearchBy not recognised");

            }
        }

        /// <summary>
        /// Filter for linq properties which are non-nullable and not a string
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> MakeFilter<T>(SearchBy by, object value)
        {
            return MakeFilter<T>(by, value, null, SearchMatching.Exact);
        }

        /// <summary>
        /// Filter for linq properties which are strings
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="matching"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> MakeFilter<T>(SearchBy by, object value, SearchMatching matching)
        {
            return MakeFilter<T>(by, value, typeof(string), matching);
        }

        /// <summary>
        /// Filter for linq properties which are nullable
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="nullableType"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> MakeFilter<T>(SearchBy by, object value, Type nullableType)
        {
            return MakeFilter<T>(by, value, nullableType, SearchMatching.Exact);
        }

        //private static Expression<Func<T, bool>> MakeFilter<T>(DateTime dateField, object value)
        //{
            
        //}

        private static Expression<Func<T, bool>> MakeFilter<T>(SearchBy by, object value, Type nullableType, SearchMatching matching)
        {

            string propertyName = SearchByConverter(by);

            var type = typeof(T);

            var property = type.GetProperty(propertyName);
            var parameter = Expression.Parameter(type, "p");

            if (matching == SearchMatching.Exact || property.PropertyType != typeof(System.String))
            {

                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                Expression constantValue = Expression.Constant(value);

                if (IsNullable(value))
                {
                    constantValue = Expression.Convert(constantValue, nullableType);
                }

                var equality = Expression.Equal(propertyAccess, constantValue);
                return Expression.Lambda<Func<T, bool>>(equality, parameter);

            }
            else
            {
                MethodCallExpression equality = null;
                switch (matching)
                {
                    case SearchMatching.Fuzzy:
                        equality = Expression.Call(Expression.Call(Expression.Property(parameter, typeof(T).GetProperty(propertyName)), "ToString", null, null), typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), Expression.Constant(value));
                        break;
                    case SearchMatching.StartsWith:
                        equality = Expression.Call(Expression.Call(Expression.Property(parameter, typeof(T).GetProperty(propertyName)), "ToString", null, null), typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) }), Expression.Constant(value));
                        break;
                }
                

                return Expression.Lambda<Func<T, bool>>(equality, parameter);
            }
        }


        //private static Expression<Func<T, bool>> MakeFilter<T>(SearchInfo searchInfo)
        //{

        //    string propertyName = SearchByConverter(searchInfo.By);

        //    var type = typeof(T);

        //    var property = type.GetProperty(propertyName);
        //    var parameter = Expression.Parameter(type, "p");

        //    if (searchInfo.Matching == SearchMatching.Exact || property.PropertyType != typeof(System.String))
        //    {

        //        var propertyAccess = Expression.MakeMemberAccess(parameter, property);
        //        Expression constantValue = Expression.Constant(searchInfo.Term);

        //        if (IsNullable(searchInfo.Term))
        //        {
        //            constantValue = Expression.Convert(constantValue, nullableType);
        //        }

        //        var equality = Expression.Equal(propertyAccess, constantValue);
        //        return Expression.Lambda<Func<T, bool>>(equality, parameter);

        //    }
        //    else
        //    {
        //        MethodCallExpression equality = null;
        //        switch (searchInfo.Matching)
        //        {
        //            case SearchMatching.Fuzzy:
        //                equality = Expression.Call(Expression.Call(Expression.Property(parameter, typeof(T).GetProperty(propertyName)), "ToString", null, null), typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), Expression.Constant(value));
        //                break;
        //            case SearchMatching.StartsWith:
        //                equality = Expression.Call(Expression.Call(Expression.Property(parameter, typeof(T).GetProperty(propertyName)), "ToString", null, null), typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), Expression.Constant(value));
        //                break;
        //        }

                
        //        //Expression e = Expression.Lambda<Func<T, bool>>(equality, parameter);
                

        //        return Expression.Lambda<Func<T, bool>>(equality, parameter);
        //    }
        //}

        private static bool IsNullable<TProperty>(TProperty obj)
        {
            if (obj == null) return true; // obvious
            Type type = typeof(TProperty);
            if (!type.IsValueType) return true; // ref-type
            if (Nullable.GetUnderlyingType(type) != null) return true; // Nullable<T>
            return false; // value-type
        }


        
    }
}
