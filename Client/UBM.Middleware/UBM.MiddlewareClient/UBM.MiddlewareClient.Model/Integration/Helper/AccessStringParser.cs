﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Enums;

namespace UBM.MiddlewareClient.Model.Helper
{
    internal class AccessStringParser
    {
        private const string READ_ONLY = "r";
        private const string READ_WRITE = "rw";

        private const char PAGE_SEPERATOR = ',';
        private const char TOKEN_SEPERATOR = '=';

        private readonly List<string> accessStrings;

        
        public AccessStringParser(List<string> accessStrings)
        {
            this.accessStrings = accessStrings;
        }

        public List<PageAccess> Parse()
        {
            List<PageAccess> pageAccess = CreateDefaultList();

            foreach (string accessString in accessStrings)
            {
                List<PageAccess> newAccess = ParseAccessString(accessString);
                pageAccess = CombineLists(pageAccess, newAccess);
            }

            return pageAccess;
        }

        private static List<PageAccess> CombineLists(List<PageAccess> first, List<PageAccess> second)
        {
            List<PageAccess> combined = new List<PageAccess>();

            foreach (PageAccess pageAccess in first)
            {
                PageAccess secondPageAccess = second.FirstOrDefault(c => c.ClientPageNumber == pageAccess.ClientPageNumber);

                if(secondPageAccess != null)
                {
                    combined.Add(new PageAccess
                                     {
                                         ClientPageNumber = pageAccess.ClientPageNumber,
                                         CanRead = pageAccess.CanRead || secondPageAccess.CanRead,
                                         CanWrite = pageAccess.CanWrite || secondPageAccess.CanWrite
                                     });
                }
                else
                {
                    combined.Add(pageAccess);
                }
            }

            return combined;
        }

        private static List<PageAccess> CreateDefaultList()
        {
            List<PageAccess> pageAccessRights = new List<PageAccess>();
            foreach (ClientPageNumber page in Enum.GetValues(typeof(ClientPageNumber)))
            {
                pageAccessRights.Add(new PageAccess
                {
                    ClientPageNumber = page,
                    CanRead = false,
                    CanWrite = false
                });
            }
            return pageAccessRights;
        }

        private static List<PageAccess> ParseAccessString(string accessString)
        {
            List<PageAccess> parsed = new List<PageAccess>();

            string[] tokens = accessString.Split(PAGE_SEPERATOR);

            foreach (string token in tokens)
            {
                string[] splitToken = token.Split(TOKEN_SEPERATOR);

                parsed.Add(CreatePageAccessObject(splitToken));

            }

            return parsed;
        }

        private static void ValidateToken(string[] splitToken)
        {
            bool valid = true;
            string message = "";

            string pageString = splitToken[0];
            string accessSpecifier = splitToken[1];

            if(splitToken.Count() != 2)
            {
                valid = false;
                message = "Access token is incorrect length";
            }

            if (accessSpecifier != READ_ONLY && accessSpecifier != READ_WRITE && !string.IsNullOrEmpty(accessSpecifier))
            {
                valid = false;
                message = "Access specifier must be 'r' for 'ReadOnly' or 'rw' for 'ReadWrite'";
            }

            int pageNumber;
            if (!int.TryParse(pageString, out pageNumber))
            {
                valid = false;
                message = "Page number must be numeric";
            }

            bool inRange = false;
            foreach (ClientPageNumber page in Enum.GetValues(typeof(ClientPageNumber)))
            {
                if (page == (ClientPageNumber)pageNumber)
                {
                    inRange = true;
                    break;
                }
            }

            if (!inRange)
            {
                valid = false;
                message = "Client page number is not in range";
            }

            if (!valid)
            {
                throw new FormatException(message);
            }
        }

        private static PageAccess CreatePageAccessObject(string[] splitToken)
        {
            ValidateToken(splitToken);

            string pageString = splitToken[0];
            string accessSpecifier = splitToken[1];

            PageAccess pageAccess = new PageAccess();

            pageAccess.ClientPageNumber = (ClientPageNumber)Enum.Parse(typeof(ClientPageNumber), int.Parse(pageString).ToString());

            if (accessSpecifier == READ_ONLY)
            {
                pageAccess.CanRead = true;
            }
            else if (accessSpecifier == READ_WRITE)
            {
                pageAccess.CanRead = true;
                pageAccess.CanWrite = true;
            }

            return pageAccess;
        }

    }
}
