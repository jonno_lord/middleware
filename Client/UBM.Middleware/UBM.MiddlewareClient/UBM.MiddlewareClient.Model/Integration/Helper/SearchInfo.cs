﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UBM.MiddlewareClient.Model.Enums;

namespace UBM.MiddlewareClient.Model.Helper
{
    public class SearchInfo
    {
        public string Term { get; set; }
        public SearchMatching Matching { get; set; }
        public SearchBy By { get; set; }
        public string OrderBy { get; set; }

        public bool FilterOnDates { get; set; }
        public SearchOnDates? DateFieldName { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public List<string> Systems { get; set; }
        public int MaxRecords { get; set; }
    }

}
