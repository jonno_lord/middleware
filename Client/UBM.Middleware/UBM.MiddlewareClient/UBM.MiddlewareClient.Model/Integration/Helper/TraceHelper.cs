﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using UBM.Logger;
using UBM.MiddlewareClient.Tracer;
using UBM.Utilities;

namespace UBM.MiddlewareClient.Model.Helper
{
    public enum TraceType
    {
        ViewModel,
        Service,
        Database,
        Error,
        Warning,
        Information,
        Controller
    }

    public class TraceHelper
    {

        private static TraceWindow trace;

        private static void OpenTraceIfClosed()
        {
            if (trace == null)
            {                         
                trace = new TraceWindow();
                trace.Show();
            }
        }


        public static string CommandFailure(Exception exception)
        {
            Trace(TraceType.Error,"",1,exception);           
            return "Command Failed: " + exception.Message;
        }

        private static string GetTraceString(int extralevel = 0)
        {
            StackTrace stackTrace = new StackTrace();
            MethodBase method = stackTrace.GetFrame(2 + extralevel).GetMethod();

            string methodName = method.Name;
            string className = method.DeclaringType.Name;

            ParameterInfo[] pi = method.GetParameters();

            string summary = className + " - " + methodName;

            if (pi.Count() > 0) summary += ". Params: ";

            pi.ToList().ForEach(c => summary += c.Name + " - "); //+ c.ToString() + ",") ;

            if (pi.Count() > 0) summary = summary.Substring(0, summary.Length - 3);

            return summary;
        }

        public static void Trace(Exception exception)
        {
            #if TRACE
            Trace(TraceType.Error, "", 1, exception);

            #endif
        }

        public static void Trace(string information)
        {
            #if TRACE
            Trace(TraceType.Information, information, 1);

            #endif
        }

        public static void Trace(TraceType traceType, string info = "", int extralevel = 0, Exception exception = null)
        {            
            #if TRACE
            OpenTraceIfClosed();

            switch (traceType)
            {
                case TraceType.ViewModel:
                    trace.ViewModelTrace(GetTraceString(extralevel));
                    break;
                case TraceType.Service:
                    trace.ServiceTrace(GetTraceString(extralevel));
                    break;
                case TraceType.Database:
                    trace.DatabaseTrace(GetTraceString(extralevel));
                    break;
                case TraceType.Error:
                    string e = "";
                    if(exception != null) e = " - " + exception;
                    trace.Error(GetTraceString(extralevel) + e);
                    break;
                case TraceType.Warning:
                    trace.Warning(GetTraceString(extralevel));
                    break;
                case TraceType.Information:
                    trace.InfoTrace(GetTraceString(extralevel) + " - " + info);
                    break;
                case TraceType.Controller:
                    trace.ControllerTrace(GetTraceString(extralevel));
                    break;
            }
            #endif
        }

    }
}
