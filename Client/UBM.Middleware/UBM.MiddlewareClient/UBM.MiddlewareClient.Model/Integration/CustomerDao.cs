﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LinqKit;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;
using UBM.Utilities;


namespace UBM.MiddlewareClient.Model.Integration
{
    public class CustomerDao : Interfaces.ICustomerDao
    {
        #region Fields

        private CustomerDataContext customerDataContext;
        private readonly List<Rejection> rejectionReasonsList;
        private readonly List<string> searchTypes;

        #endregion

        #region Private Methods

        private IQueryable<vwCustomer> CustomerSearchDefinition(SearchInfo searchInfo)
        {
            var predicate = PredicateBuilder.True<vwCustomer>();

            if (searchInfo.DateFieldName == SearchOnDates.LastActioned)
            {
                if(searchInfo.To != null)
                {
                    predicate = predicate.And(c => (c.AcceptedInScrutiny.HasValue ? c.AcceptedInScrutiny.Value <= searchInfo.To : false) || (c.RejectedInScrutiny.HasValue ? c.RejectedInScrutiny.Value <= searchInfo.To : false));    
                }
                if(searchInfo.From != null)
                {
                    predicate = predicate.And(c => (c.AcceptedInScrutiny.HasValue ? c.AcceptedInScrutiny.Value >= searchInfo.From : false) || (c.RejectedInScrutiny.HasValue ? c.RejectedInScrutiny.Value >= searchInfo.From : false));                 
                } 
            }
            else if (searchInfo.DateFieldName == SearchOnDates.LoadedIntoMiddleware)
            {
                predicate = predicate.And(c => searchInfo.To.HasValue && c.SourceToMiddleware.HasValue ? c.SourceToMiddleware.Value <= searchInfo.To : false);
                predicate = predicate.And(c => searchInfo.From.HasValue && c.SourceToMiddleware.HasValue ? c.SourceToMiddleware.Value >= searchInfo.From : false);
            }


            IQueryable<vwCustomer> queryable = customerDataContext.vwCustomers;

            queryable = queryable.Where(SearchHelper.MakeFilter<vwCustomer>(searchInfo.By, searchInfo.Term, searchInfo.Matching));

            queryable =  queryable.Where(predicate);
            queryable = queryable.OrderBy(c => c.CustomerName);


            return queryable;
        }

        private List<Customer> GetCustomersWithFilterApplied(IQueryable<vwCustomer> filtered)
        {
            List<Customer> customers = new List<Customer>();

            var query = from customer in filtered
                        join duplicate in customerDataContext.vwCustomerDuplicates 
                        on new { customer.ID, Source = customer.SystemName } equals new { duplicate.ID, Source = duplicate.DuplicateSource} into duplicates
                        join hold in customerDataContext.vwCustomerHolds on new { customer.ID, customer.SystemName } equals new { hold.ID, hold.SystemName } into holds
                        join rejection in customerDataContext.vwCustomerRejections on new { customer.ID, customer.SystemName } equals new { rejection.ID, rejection.SystemName } into rejections
                        join violation in customerDataContext.vwCustomerViolations on new { customer.ID, customer.SystemName } equals new { violation.ID, violation.SystemName } into violations

                        join action in customerDataContext.UserActions 
                        on new { customer.ID, customer.SystemName, Environment.UserName } equals new { ID = action.customerId, SystemName = action.systemName, UserName = action.userName } into actions

                        where searchTypes.Contains(customer.SearchType)
                        select new { customer, duplicates, holds, rejections, violations, actions };


                        

            foreach (var row in query)
            {
                customers.Add(Customer.CreateCustomer(row.customer, 
                                                      row.duplicates.ToList(), 
                                                      row.holds.OrderByDescending(c=>c.HoldDate).ToList(), 
                                                      row.rejections.OrderByDescending(c=>c.DateRejected).ToList(), 
                                                      row.violations.ToList(), 
                                                      row.actions.ToList(), 
                                                      rejectionReasonsList));
            }



            return customers;

        }

        private IQueryable<vwCustomerRejection> EmailSearchDefinition(SearchInfo searchInfo)
        {
            var predicate = PredicateBuilder.True<vwCustomerRejection>();

            if(searchInfo.FilterOnDates)
            {
                if(searchInfo.To.HasValue) predicate = predicate.And(c => c.DateRejected.HasValue ? c.DateRejected.Value <= searchInfo.To : false);
                if (searchInfo.From.HasValue) predicate = predicate.And(c =>  c.DateRejected.HasValue ? c.DateRejected.Value >= searchInfo.From : false);
            }


            IQueryable<vwCustomerRejection> queryable = customerDataContext.vwCustomerRejections;

            queryable = queryable.Where(SearchHelper.MakeFilter<vwCustomerRejection>(searchInfo.By, searchInfo.Term, searchInfo.Matching));

            queryable = queryable.Where(predicate);

            queryable = queryable.OrderByDescending(c => c.DateRejected).Take(searchInfo.MaxRecords);

            return queryable;
        }

        private List<Email> GetEmailsWithFilterApplied(IQueryable<vwCustomerRejection> filtered)
        {
            List<Email> rejections = new List<Email>();

            rejections.AddRange(Email.CreateEmailHistory(filtered.ToList()));

            return rejections;
        }

        private IQueryable<vwCustomerHold> LegalSearchDefinition(SearchInfo searchInfo)
        {
            var predicate = PredicateBuilder.True<vwCustomerHold>();

            if (searchInfo.FilterOnDates)
            {
                if (searchInfo.To.HasValue) predicate = predicate.And(c => c.HoldDate.HasValue ? c.HoldDate.Value <= searchInfo.To : false);
                if (searchInfo.From.HasValue) predicate = predicate.And(c => c.HoldDate.HasValue ? c.HoldDate.Value >= searchInfo.From : false);
            }

            IQueryable<vwCustomerHold> queryable = customerDataContext.vwCustomerHolds;

            queryable = queryable.Where(SearchHelper.MakeFilter<vwCustomerHold>(searchInfo.By, searchInfo.Term, searchInfo.Matching));

            queryable = queryable.Where(predicate).OrderByDescending(c => c.HoldDate);

            return queryable;

        }

        private List<Legal> GetLegalWithFilterApplied(IQueryable<vwCustomerHold> filtered)
        {
            List<Legal> legal = new List<Legal>();

            foreach (var vwCustomerHold in filtered)
            {
                legal.Add(new Legal
                {
                    CustomerID = vwCustomerHold.ID,
                    SystemName = vwCustomerHold.SystemName,
                    CustomerName = vwCustomerHold.CustomerName,
                    Hold = vwCustomerHold.Hold ?? false,
                    HoldDate = vwCustomerHold.HoldDate,
                    Reason = vwCustomerHold.Reason,
                    UserName = vwCustomerHold.UserName
                });
            }

            return legal;
        }

        private List<Rejection> PopulateRejectionReasons()
        {
            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                
   
                List<Rejection> rejectionList = new List<Rejection>();

                var rejections = from s in customerDataContext.RejectionReasons
                                 select s;

                foreach (RejectionReason rejectionReason in rejections)
                {
                    rejectionList.Add(new Rejection
                    {
                        ID = rejectionReason.id,
                        Reason = rejectionReason.reason,
                        Description = rejectionReason.description
                    });
                }

                return rejectionList;
            }
        }

        #endregion

        #region Constructor

        public CustomerDao(List<string> searchTypes)
        {
            TraceHelper.Trace(TraceType.Database);
            this.searchTypes = searchTypes;
            rejectionReasonsList = PopulateRejectionReasons();
        }

        #endregion

        #region Public Methods

        public Customer GetCustomer(int id, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                var query = from c in customerDataContext.vwCustomers
                            where c.ID == id && c.SystemName == systemName
                            select c;

                return GetCustomersWithFilterApplied(query).FirstOrDefault();
            }
        }

        public List<Customer> SearchCustomers(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                var filter = CustomerSearchDefinition(searchInfo);

                filter = filter.Where(c => searchInfo.Systems.Contains(c.SystemName)).Take(searchInfo.MaxRecords);

                return GetCustomersWithFilterApplied(filter);
            }
        }

        public List<Email> SearchEmails(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                if (searchInfo != null)
                {
                    var filter = EmailSearchDefinition(searchInfo);

                    filter = filter.Where(c => searchInfo.Systems.Contains(c.SystemName)).Take(searchInfo.MaxRecords);

                    return GetEmailsWithFilterApplied(filter);
                }

                return GetRejectionEmails(200);
            }
        }

        public List<Legal> SearchLegal(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                if (searchInfo != null)
                {
                    var filter = LegalSearchDefinition(searchInfo);

                    filter = filter.Where(c => searchInfo.Systems.Contains(c.SystemName)).Take(searchInfo.MaxRecords);

                    return GetLegalWithFilterApplied(filter);
                }

                return GetLegalHistory(200);
            }
        }


        public List<Customer> GetDedupeCustomers()
        {
            TraceHelper.Trace(TraceType.Database);

            using(customerDataContext = CustomerDataContext.ReadOnly)
            {                   
                var filter = from c in customerDataContext.vwCustomers
                               where c.Stage == 1 && c.Rejected == null && c.Accepted == null 
                               orderby c.CustomerName
                               select c;

                return GetCustomersWithFilterApplied(filter);
            }
        }


        public List<Email> GetRejectionEmails(int howMany = 0)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                IQueryable<vwCustomerRejection> query = from c in customerDataContext.vwCustomerRejections
                                                        orderby c.DateRejected descending 
                                                        select c;

                if (howMany > 0) query = query.Take(howMany);

                return GetEmailsWithFilterApplied(query);
            }
        }

        public List<Legal> GetLegalHistory(int howMany = 0)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = CustomerDataContext.ReadOnly)
            {
                List<Legal> legal = new List<Legal>();

                IQueryable<vwCustomerHold> query = from c in customerDataContext.vwCustomerHolds
                            orderby c.HoldDate descending 
                            select c;

                if (howMany > 0) query = query.Take(howMany);

                return GetLegalWithFilterApplied(query);
            }
        }

        public List<Rejection> GetRejectionReasons()
        {
            TraceHelper.Trace(TraceType.Database);

            return rejectionReasonsList;
        }

     

        public List<Legal> GetLegalRecords(Customer customer)
        {
            TraceHelper.Trace(TraceType.Database);

            List<Legal> legal = new List<Legal>();

            var query = from p in CustomerDataContext.ReadOnly.vwCustomerHolds
                        where p.ID == customer.ID && p.SystemName ==  customer.SystemName
                        orderby p.HoldDate descending
                        select p;

            foreach (vwCustomerHold hold in query)
            {
                legal.Add(new Legal
                {
                    Hold = hold.Hold ?? false,
                    HoldDate = hold.HoldDate,
                    Reason = hold.Reason,
                    UserName = hold.UserName,
                });
            }

            return legal;
        }



        public void LegalHold(int id, string systemName, bool hold, string reason)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {

                vwCustomerHold newHold = new vwCustomerHold
                {
                    ID = id,
                    Hold = hold,
                    HoldDate = DateTime.Now,
                    Reason = reason,
                    UserName = Environment.UserName,
                    SystemName = systemName,
                };


                customerDataContext.vwCustomerHolds.InsertOnSubmit(newHold);

                customerDataContext.SubmitChanges();
            }

        }

        public void UpdateCustomerVat(int id, string systemName, string vat)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {
                vwCustomer vwCust = customerDataContext.vwCustomers.Single(u => u.ID == id && u.SystemName == systemName);
                vwCust.VATNumber = vat;

                customerDataContext.SubmitChanges();
            }
        }

        public void CommitAcceptedCustomer(int id, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {
                vwCustomer customer = customerDataContext.vwCustomers.FirstOrDefault(c => c.ID == id && c.SystemName == systemName);

                customer.Accepted = true;
                customer.AcceptedInScrutiny = DateTime.Now;

                customerDataContext.SubmitChanges();
            }
        }

        public void CommitRejectedCustomer(int id, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {
                vwCustomer customer = customerDataContext.vwCustomers.FirstOrDefault(c => c.ID == id && c.SystemName == systemName);

                customer.Rejected = true;
                customer.RejectedInScrutiny = DateTime.Now;

                customerDataContext.SubmitChanges();
            }
        }

        public void InsertRejection(int id, string systemName, string reason, string recipient, string cc, string body, bool sendNotification)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {
                vwCustomerRejection rejection = new vwCustomerRejection
                {
                    ID = id,
                    DateRejected = DateTime.Now,
                    ReasonRejected = reason,
                    Recipient = recipient,
                    Sender = Environment.UserName,
                    SystemName = systemName,
                    CC = cc,
                    Body = body,
                    NotificationSent = sendNotification
                };

                customerDataContext.vwCustomerRejections.InsertOnSubmit(rejection);

                customerDataContext.SubmitChanges();
            }
        }

        public void AcceptCustomerInUserActions(int id, string systemName, string jdeAccount)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {

                UserAction ua = new UserAction
                {
                    userName = Environment.UserName,
                    customerId = id,
                    systemName = systemName,
                    accepted = true,
                    jdeAccountNumber = jdeAccount
                };

                customerDataContext.UserActions.InsertOnSubmit(ua);
                customerDataContext.SubmitChanges();

            }

        }

        public void RejectCustomerInUserActions(int id, string systemName, string salesperson)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {

                UserAction ua = new UserAction
                {
                    userName = Environment.UserName,
                    customerId = id,
                    systemName = systemName,
                    recipient = salesperson,
                    rejected = true,
                    sendNotification = true

                };

                customerDataContext.UserActions.InsertOnSubmit(ua);
                customerDataContext.SubmitChanges();
            }

        }

        public void EditUserAction(int id, string systemName, string body, string reason, string recipient, string cc, bool send)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {

                UserAction ua = customerDataContext.UserActions.Single(u => u.customerId == id && u.systemName == systemName && u.userName == Environment.UserName);
                ua.body = body;
                ua.reason = reason;
                ua.recipient = recipient;
                ua.cc = cc;
                ua.sendNotification = send;

                customerDataContext.SubmitChanges();

            }

        }

        public void DeleteCustomerInUserActions(int id, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            using (customerDataContext = new CustomerDataContext())
            {
                var query = from p in customerDataContext.UserActions
                             where p.customerId == id && p.systemName == systemName
                             select p;


                if(query.Count() > 0)
                {
                    customerDataContext.UserActions.DeleteAllOnSubmit(query);
                    customerDataContext.SubmitChanges();
                }
                
            }
        }

        public List<UserAction> GetAllUserActions()
        {
            TraceHelper.Trace(TraceType.Database);

            var query = from p in CustomerDataContext.ReadOnly.UserActions
                        where p.userName == Environment.UserName
                        select p;

            return new List<UserAction>(query);
        }

        public List<UserAction> GetUserActionsForOtherUsers()
        {
            TraceHelper.Trace(TraceType.Database);

            var actions =
                from c in CustomerDataContext.ReadOnly.UserActions
                where c.userName != Environment.UserName
                select c;

            return new List<UserAction>(actions);
        }

        public UserAction GetUserActionForCurrentUser(int id, string systemName)
        {
            TraceHelper.Trace(TraceType.Database);

            var query = from p in CustomerDataContext.ReadOnly.UserActions
                        where p.customerId == id && p.systemName == systemName && p.userName == Environment.UserName
                        select p;

            List<UserAction> actions = new List<UserAction>(query);

            return actions.Count > 0 ? actions[0] : null;
        }

        #endregion

    }
}
