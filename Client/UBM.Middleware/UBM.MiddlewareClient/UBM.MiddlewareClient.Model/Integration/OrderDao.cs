﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using LinqKit;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Integration
{
    public class OrderDao : IOrderDao
    {
        private OrderDataContext orderDataContext;
        private readonly List<string> searchTypes;

        private IQueryable<vwOrdersAggregate> OrderSearchDefinition(SearchInfo searchInfo)
        {
            var predicate = PredicateBuilder.True<vwOrdersAggregate>();

            IQueryable<vwOrdersAggregate> queryable = orderDataContext.vwOrdersAggregates.Where(c =>searchTypes.Contains(c.SearchType));

            queryable = queryable.Where(SearchHelper.MakeFilter<vwOrdersAggregate>(searchInfo.By, searchInfo.Term, searchInfo.Matching));

            queryable = queryable.Where(c => searchInfo.Systems.Contains(c.SystemName));


            queryable = queryable.Where(predicate).OrderBy(c => c.OrderNumber).Take(searchInfo.MaxRecords);

            return queryable;
        }

        private static List<Order> GetOrdersWithFilterApplied(IQueryable<vwOrdersAggregate> filter)
        {
            List<Order> orders = new List<Order>();

            foreach (var vwOrder in filter)
            {
             orders.Add(new Order
                            {
                                ID = Int32.Parse(vwOrder.OrderNumber),
                                SystemName = vwOrder.SystemName,
                                Amount = vwOrder.Amount,
                                BatchNumber =  vwOrder.BatchNumber,
                                Currency = vwOrder.Currency,
                                Number = vwOrder.OrderNumber,
                                Lines = vwOrder.Lines.ToString(),
                                CustomerName = vwOrder.CustomerName,
                                JDEAccountNumber = vwOrder.JDEAccountNumber
                            });
            }

            return orders;
        }

        public OrderDao(List<string> searchTypes)
        {
            TraceHelper.Trace(TraceType.Database);

            this.searchTypes = searchTypes;
        }

        public List<Order> SearchOrders(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.Database);

            using(orderDataContext = OrderDataContext.ReadOnly)
            {
                var filter = OrderSearchDefinition(searchInfo);

                return GetOrdersWithFilterApplied(filter);
            }
        }

        public DataSet GetOrderDetails(Order order)
        {
            TraceHelper.Trace(TraceType.Database);

            return SqlDataAccess.GetOrderDetails(order.ID.ToString(), order.BatchNumber, order.SystemName);
        }
    }
}
