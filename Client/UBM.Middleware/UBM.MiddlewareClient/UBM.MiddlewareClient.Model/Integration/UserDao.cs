﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.Model.Orm;

namespace UBM.MiddlewareClient.Model.Integration
{
    internal class UserDao : IUserDao
    {
        private UserDataContext userDataContext;

        public void InsertMostUsed()
        {
            TraceHelper.Trace(TraceType.Database);

            using (userDataContext = new UserDataContext())
            {

                MostUsed most = new MostUsed
                {
                    UserName = Environment.UserName
                };

                userDataContext.MostUseds.InsertOnSubmit(most);

                userDataContext.SubmitChanges();
            }
        }

        public void UpdateMostUsed(string pageCount)
        {
            TraceHelper.Trace(TraceType.Database);

            using (userDataContext = new UserDataContext())
            {
                MostUsed most = userDataContext.MostUseds.Single(u => u.UserName == Environment.UserName);
                most.PageCountString = pageCount;

                userDataContext.SubmitChanges();
            }
        }

        public MostUsed GetMostUsed()
        {
            TraceHelper.Trace(TraceType.Database);

            try
            {
                var pages = from s in UserDataContext.ReadOnly.MostUseds where s.UserName == Environment.UserName select s;
                List<MostUsed> mostUsed = new List<MostUsed>(pages);

                return mostUsed.Count != 0 ? mostUsed[0] : null;
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(TraceType.Error);
                Log.Error("", exception);
                return null;
            }
        }

        public List<CountryGroup> GetCountryGroups()
        {
            TraceHelper.Trace(TraceType.Database);

            var groups = from c in UserDataContext.ReadOnly.CountryGroups
                         select c;

            return new List<CountryGroup>(groups);
        }

        public List<vwUserGroupAccess> GetUserGroupAccess()
        {
            TraceHelper.Trace(TraceType.Database);

            var groupAccess = from c in UserDataContext.ReadOnly.vwUserGroupAccesses select c;

            return new List<vwUserGroupAccess>(groupAccess);
        }


    }
}
