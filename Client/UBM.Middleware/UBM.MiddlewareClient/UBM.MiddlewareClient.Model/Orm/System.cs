using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class SystemDataContext
    {
        public SystemDataContext()
            : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_SYSTEM))
        {
            
        }

        public static SystemDataContext ReadOnly
        {
            get
            {
                return new SystemDataContext { ObjectTrackingEnabled = false };
            }
        } 
    }
}
