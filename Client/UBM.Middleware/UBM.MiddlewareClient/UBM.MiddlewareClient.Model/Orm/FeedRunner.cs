using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class FeedRunnerDataContext
    {

        public FeedRunnerDataContext() : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_MSI))
        {
            
        }

        public static FeedRunnerDataContext ReadOnly
        {
            get
            {
                return new FeedRunnerDataContext { ObjectTrackingEnabled = false };
            }
        }  
    }
}
