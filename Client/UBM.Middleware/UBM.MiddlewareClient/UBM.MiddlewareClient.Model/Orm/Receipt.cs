using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class ReceiptDataContext
    {
        public ReceiptDataContext()
            : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON))
        {
        
        }

        public static ReceiptDataContext ReadOnly
        {
            get
            {
                return new ReceiptDataContext { ObjectTrackingEnabled = false };
            }
        }   
    }
}
