using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Names;

namespace UBM.MiddlewareClient.Model.Orm
{
    partial class OrderDataContext
    {
        public OrderDataContext()
            : base(ConnectionManager.GetConnectionString(ConnectionStringNames.MIDDLEWARE_COMMON))
        {
            
        }

        public static OrderDataContext ReadOnly
        {
            get
            {
                OrderDataContext orderDataContext = new OrderDataContext {ObjectTrackingEnabled = false};
                orderDataContext.OnCreated();

                return orderDataContext;
            }
        }  

        partial void OnCreated()
        {
            this.CommandTimeout = 3600;
        }
    }
}
