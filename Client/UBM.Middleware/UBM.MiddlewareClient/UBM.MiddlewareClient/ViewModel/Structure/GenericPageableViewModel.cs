﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Interfaces;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TModelType">The type of the model type.</typeparam>
    public abstract class GenericPageableViewModel<TModelType> : PageViewModel, IPageable
    {
        #region Fields

        private const int MAX_ITEMS_ON_PAGE = 25;

        private readonly SynchronizationContext context;
        private int collectionCount;
        private int generatePageAtIndex;


        private int numberOfPages;
        private int pageNumber;

        private ObservableCollection<TModelType> singlePage;

        public List<TModelType> FullModelCollection { get; set; }
        public PageController PageController { get; set; }

        #endregion



        #region Observable Properties

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>The page number.</value>
        public int PageNumber
        {
            get { return pageNumber; }
            set
            {
                if (value == pageNumber) return;
                pageNumber = value;
                OnPropertyChanged("PageNumber");
            }
        }

        /// <summary>
        /// Gets or sets the number of pages.
        /// </summary>
        /// <value>The number of pages.</value>
        public int NumberOfPages
        {
            get { return numberOfPages; }
            set
            {
                if (value == numberOfPages) return;
                numberOfPages = value;
                OnPropertyChanged("NumberOfPages");
            }
        }

        /// <summary>
        /// Gets or sets the single page.
        /// </summary>
        /// <value>The single page.</value>
        public ObservableCollection<TModelType> SinglePage
        {
            get { return singlePage; }
            set
            {
                if (value == singlePage) return;
                singlePage = value;
                OnPropertyChanged("SinglePage");
            }
        }

        #endregion

        #region Commands

        private DelegateCommand nextPageCommand;
        public ICommand NextPageCommand
        {
            get
            {
                if (nextPageCommand == null) nextPageCommand = new DelegateCommand(GotoNextPage, CanGotoNextPage);
                return nextPageCommand;
            }
        }
        private void GotoNextPage(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);
            PageNumber = PageController.NextPage();
        }
        private bool CanGotoNextPage(object state)
        {
            return PageNumber != numberOfPages - 1;
        }

        private DelegateCommand previousPageCommand;
        public ICommand PreviousPageCommand
        {
            get
            {
                if (previousPageCommand == null)
                    previousPageCommand = new DelegateCommand(GotoPreviousPage, CanGotoPreviousPage);
                return previousPageCommand;
            }
        }
        private void GotoPreviousPage(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);
            PageNumber = PageController.PreviousPage();
        }
        private bool CanGotoPreviousPage(object state)
        {
            return PageNumber != 0;
        }

        private DelegateCommand goToPageCommand;
        public ICommand GoToPageCommand
        {
            get
            {
                if (goToPageCommand == null) goToPageCommand = new DelegateCommand(GoToPage);
                return goToPageCommand;
            }
        }

        public void GoToPage(object state)
        {
            int pageNum;
            if (int.TryParse(state.ToString(), out pageNum))
            {
                GoToPage(pageNum);       
            }
        }
         

        #endregion

        #region Private Methods

        private void SetupController(int count, int maxItemsOnPage)
        {
            PageNumber = 0;
            FullModelCollection.Clear();


            PageController = PageController.CreatePageController(PageNumber, count, maxItemsOnPage, GetPage);
            NumberOfPages = PageController.NumberOfPages;
            generatePageAtIndex = PageController.EndIndex - 1;
            collectionCount = PageController.NumberOfItemsInCollection;

            if(collectionCount == 0)
            {
                SendOrPostCallback callback = delegate
                {
                    SinglePage.Clear();
                };

                context.Send(callback, null);
            }
        }

        private void SetupCollection(IList<TModelType> data)
        {
            for (int i = 0; i < collectionCount; i++)
            {
                FullModelCollection.Add(data[i]);

                if (i == generatePageAtIndex)
                {
                    GetPage();
                }
            }
        }
      

        #endregion

        #region Public Methods

        protected GenericPageableViewModel()
        {
            TraceHelper.Trace(TraceType.ViewModel);
            SinglePage = new ObservableCollection<TModelType>();
            FullModelCollection = new List<TModelType>();
            context = SynchronizationContext.Current;
        }


        /// <summary>
        /// Begins the paging.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="maxItemsOnPage">The max items on page.</param>
        /// <param name="stayOnPage"></param>
        public void BeginPaging(IList<TModelType> data, int maxItemsOnPage = MAX_ITEMS_ON_PAGE, bool stayOnPage = true)
        {
            int goToPage = 0;
            if (stayOnPage)
                goToPage = pageNumber + 1;

            TraceHelper.Trace(TraceType.ViewModel);
            SetupController(data.Count, maxItemsOnPage);
            SetupCollection(data);

            if(goToPage > 1)
                GoToPage(goToPage);
        }

    

        /// <summary>
        /// Populates the page collection with the current page of items
        /// </summary>
        public void GetPage()
        {
            TraceHelper.Trace(TraceType.ViewModel);
            SinglePage.Clear();

            int startIndex = PageController.StartIndex;
            int endIndex = PageController.EndIndex;

            for (int i = startIndex; i < endIndex; i++)
            {
                if (FullModelCollection.Count() > i)
                {
                    SinglePage.Add(FullModelCollection[i]);
                }
                else
                {
                    GoToPage(PageController.GetMaxPageForSetItemCount(FullModelCollection.Count()));
                }
            }
        }

        /// <summary>
        /// Populates the page collection with items from a specific page number
        /// </summary>
        /// <param name="gotoPageNumber">The goto page number.</param>
        public void GoToPage(int gotoPageNumber)
        {
            TraceHelper.Trace(TraceType.ViewModel);
            // This is called from View code behind, pageNumber is decremented because pages start at 1 in the view and 0 here.
            gotoPageNumber--;

            PageNumber = PageController.SetCurrentPageAndValidate(gotoPageNumber);
            GetPage();
        }



        #endregion
    }
}