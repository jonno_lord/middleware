﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.ViewHelper;

namespace UBM.MiddlewareClient.ViewModel.Structure
{
    public class PageConfiguration
    {
        public ClientPageNumber Page { get; set; }
        public string DisplayName { get; set; }
        public string ObjectDisplayName { get; set; }

        public bool FiltersVisible { get; set; }
        public bool PropertySearch { get; set; }
        public bool DateSearch { get; set; }

        public List<ComboItem<SearchBy>> SearchPropertiesList { get; set; }
        public List<ComboItem<SearchOnDates>> DateFilterPropertiesList { get; set; }
    }
}
