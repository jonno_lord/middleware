﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public partial class DedupeViewModel : GenericPageableViewModel<Customer>, IDedupeViewModel
    {
        public ICommand PrepareLegalCommand { get; private set; }
        public ICommand AddLegalHoldCommand { get; private set; }
        public ICommand RemoveLegalHoldCommand { get; private set; }
        public ICommand PrepareVatCommand { get; private set; }
        public ICommand AddVatNumberCommand { get; private set; }
        public ICommand RemoveVatNumberCommand { get; private set; }
        public ICommand PrepareRejectCommand { get; private set; }
        public ICommand EditNotificationDialogCommand { get; private set; }
        public ICommand PrepareAcceptCommand { get; private set; }
        public ICommand AcceptCustomerCommand { get; private set; }
        public ICommand CancelAcceptDialogCommand { get; private set; }
        public ICommand ResetCustomerCommand { get; private set; }
        public ICommand CommitCustomerCommand { get; private set; }
        public ICommand ViewCustomerGroupCommand { get; private set; }
        public ICommand RemoveCustomerLockCommand { get; private set; }
        public ICommand SelectAllCustomersCommand { get; private set; }

        private void InitialiseCommands()
        {
            PrepareLegalCommand = new DelegateCommand(PrepareLegal, CanPrepareLegal);
            AddLegalHoldCommand = new DelegateCommand(AddLegalHold, CanAddLegalHold);
            RemoveLegalHoldCommand = new DelegateCommand(RemoveLegalHold, CanRemoveLegalHold);
            PrepareVatCommand = new DelegateCommand(PrepareVat, CanPrepareVat);
            AddVatNumberCommand = new DelegateCommand(AddVatNumber, IsCustomerLockedByMe);
            RemoveVatNumberCommand = new DelegateCommand(RemoveVatNumber, CanRemoveVat);
            PrepareRejectCommand = new DelegateCommand(PrepareRejectCustomer, CanRejectCustomer);
            EditNotificationDialogCommand = new DelegateCommand(EditNotificationDialog);
            PrepareAcceptCommand = new DelegateCommand(PrepareAccept, CanPrepareAccept);
            AcceptCustomerCommand = new DelegateCommand(AcceptCustomer, CanAcceptCustomer);
            CancelAcceptDialogCommand = new DelegateCommand(CancelAcceptDialog);
            ResetCustomerCommand = new DelegateCommand(ResetCustomer, CanResetCustomer);
            CommitCustomerCommand = new DelegateCommand(CommitCustomer, CanCommitCustomer);
            ViewCustomerGroupCommand = new DelegateCommand(ViewCustomerGroup);
            RemoveCustomerLockCommand = new DelegateCommand(RemoveCustomerLock);
            SelectAllCustomersCommand = new DelegateCommand(SelectAllCustomers);
        }

        #region Legal Commands

        private void PrepareLegal(object state)
        {

            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                syncController.Lock(customer);
                LegalReason = customer.Legal.Reason;
            }

            messageService.OpenDialog(DialogType.Legal);
        }

        private void AddLegalHold(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                dedupeService.AddLegalHold(customer, LegalReason);

                LegalSnapshot = new ObservableCollection<Legal>(dedupeService.GetLegalHistorySnapshot());

                syncController.Unlock(customer);
                syncController.RefreshClients();

                UpdateCustomerView();
                break;
            }

            messageService.CloseDialog(DialogType.Legal);
        }

        private void RemoveLegalHold(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                dedupeService.RemoveLegalHold(customer);

                LegalSnapshot = new ObservableCollection<Legal>(dedupeService.GetLegalHistorySnapshot());

                syncController.Unlock(customer);

                syncController.RefreshClients();

                UpdateCustomerView();

                break;
            }

            messageService.CloseDialog(DialogType.Legal);
        }

        #endregion

        #region VAT Commands

        private void PrepareVat(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                syncController.Lock(customer);
                VatNumber = customer.SaleInformation.VATNumber;
            }

            messageService.OpenDialog(DialogType.Vat);

        }

        private void AddVatNumber(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                SetVat(customer, true);
                syncController.Unlock(customer);
                syncController.RefreshClients();
                break;
            }

            messageService.CloseDialog(DialogType.Vat);

        }

        private void RemoveVatNumber(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                SetVat(customer, false);
                syncController.Unlock(customer);
                syncController.RefreshClients();
                break;
            }

            messageService.CloseDialog(DialogType.Vat);

        }


        #endregion

        #region Reject Commands

        private void PrepareRejectCustomer(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            NotificationViewModel.ClearCustomers();

            foreach (Customer customer in GetSelectedCustomers())
            {
                if (!customer.IsLocked)
                {
                    syncController.Lock(customer);
                    NotificationViewModel.AddCustomer(customer, true);
                }
            }

            messageService.OpenDialog(DialogType.RejectNotification);
        }

        private void EditNotificationDialog(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Customer rejectedCustomer = state as Customer;

            if (rejectedCustomer != null)
            {

                NotificationViewModel.ClearCustomers();
                if (NotificationViewModel.IsEmpty)
                {
                    foreach (Customer customer in CustomersView.Where(c => c.Queue == QueueType.Rejected))
                    {
                        NotificationViewModel.AddCustomer(customer, false);
                    }
                }
                NotificationViewModel.SelectNotificationCustomer(rejectedCustomer);

                messageService.OpenDialog(DialogType.RejectNotification);
            }

        }

        #endregion

        #region Accept Commands

        public void PrepareAccept(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if (DoSelectedCustomersHaveViolationsOrDuplicates())
            {
                messageService.OpenDialog(DialogType.Accept);
            }
            else
            {
                AcceptCustomer(null);
            }
        }

        private void AcceptCustomer(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                if (!IsCustomerLockedByAnotherUser(customer))
                {
                    syncController.Lock(customer);
                    dedupeService.AcceptCustomer(customer);
                    UpdateCustomerView();
                }
            }

            OnPropertyChanged("PostScrutinyCount");

            messageService.CloseDialog(DialogType.Accept);
            CloseCustomerDetails();
        }

        private void CancelAcceptDialog(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            messageService.CloseDialog(DialogType.Accept);
        }

        #endregion

        #region Commit/Reset Commands


        private void ResetCustomer(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                Reset(customer);
                UpdateCustomerView();
            }

            CloseCustomerDetails();
        }

        private void CommitCustomer(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if (CheckIfCustomersAreOnLegalHold(GetSelectedCustomers()))
            {

                int commitCount = 0;
                foreach (Customer customer in GetSelectedCustomers())
                {
                    if (customer.Legal.Hold)
                    {
                        dedupeService.RemoveLegalHold(customer);
                        LegalSnapshot = new ObservableCollection<Legal>(dedupeService.GetLegalHistorySnapshot());
                    }

                    CommitCustomer(customer);

                    UpdateCustomerView();
                    commitCount++;
                }


                EmailSnapshot = new ObservableCollection<Email>(dedupeService.GetRejectionEmailsSnapshot());


                if (commitCount == 1)
                {
                    messageService.SendUserMessage(commitCount + " Customer Committed", MessageType.Info);
                }
                else
                {
                    messageService.SendUserMessage(commitCount + " Customers Committed", MessageType.Info);
                }

                DeselectCustomers();
                syncController.RefreshClients();

                OnPropertyChanged("PostScrutinyCount");

                CloseCustomerDetails();
            }


        }


        private bool CheckIfCustomersAreOnLegalHold(IEnumerable<Customer> customers)
        {

            bool outstandingHolds = customers.Any(c => c.Legal.Hold);

            if (!outstandingHolds)
            {
                return true;
            }

            MessageBoxArgs messageBoxArgs = new MessageBoxArgs
                                                {
                                                    Title = "Legal Hold Warning",
                                                    Message =
                                                        "You are attempting to commit one or more customers with outstanding legal holds. Do you wish to continue and remove the holds?",
                                                    MessageType = MessageType.Bad,
                                                };

            return messageService.OpenMessageBoxWithResult(messageBoxArgs) == DialogResult.OK;
        }



        #endregion

        #region Page Commands

        private void ViewCustomerGroup(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            SetCustomersView((GroupView)state);
        }

        private void RemoveCustomerLock(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in GetSelectedCustomers())
            {
                syncController.Unlock(customer);
            }

            messageService.CloseAnyOpenDialogsExcept(DialogType.CustomerDetails);
        }

        private void SelectAllCustomers(object state)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            bool flag = CustomersView.ToList().FindAll(c => c.IsSelected).Count != CustomersView.Count;
            CustomersView.ToList().ForEach(c => c.IsSelected = flag);
        }


        private void MergeCustomer(object sender, EventArgs args)
        {

            TraceHelper.Trace(TraceType.ViewModel);

            Duplicate duplicate = sender as Duplicate;

            if (duplicate != null)
            {
                syncController.Lock(SelectedCustomer);                
                SelectedCustomer.JDEAccountNumber = duplicate.JDEAccountNumber;

                AcceptCustomer(null);
            }
        }


        #endregion



        //@JP Refactor

        #region CanExecutes

        private static bool IsCustomerLockedByAnotherUser(Customer customer)
        {
            return customer.IsLocked && customer.LockedBy != Environment.UserName;
        }

        private static bool IsCustomerViewedByMe(Customer customer)
        {
            return customer.Viewers.Where(c => c.Username == Environment.UserName).Count() > 0;
        }

        private bool DoSelectedCustomersHaveViolationsOrDuplicates()
        {
            foreach (Customer customer in GetSelectedCustomers())
            {
                if (customer.Scrutiny.Duplicates.Count > 0 || customer.Scrutiny.Violations.Count > 0)
                    return true;
            }

            return false;
        }

        private bool NoDialogsOpenButDetails()
        {
            return !messageService.AnyDialogsOpenExcept(DialogType.CustomerDetails);
        }

        private bool CanPerformMultipleCustomerOperation()
        {
            return HasWriteAccess && IsCustomerSelected() && InScrutiny() && NoDialogsOpenButDetails();
        }

        private bool CanPerformSingleCustomerOperation()
        {
            return HasWriteAccess && IsSingleCustomerSelected() && InScrutiny() && NoDialogsOpenButDetails();
        }

        private bool CanAcceptCustomer(object state)
        {
            return HasWriteAccess && IsCustomerSelected() && InScrutiny();
        }

        private bool CanPrepareAccept(object state)
        {
            return CanPerformMultipleCustomerOperation();
        }

        private bool CanRejectCustomer(object state)
        {
            return CanPerformMultipleCustomerOperation();
        }

        private bool CanPrepareLegal(object state)
        {
            return CanPerformSingleCustomerOperation();
        }

        private bool CanPrepareVat(object state)
        {
            return CanPerformSingleCustomerOperation();
        }

        private bool CanCommitCustomer(object state)
        {
            if (HasWriteAccess && IsCustomerSelected() && NoDialogsOpenButDetails() && IsCustomerLockedByMe(null))
            {
                foreach (Customer customer in GetSelectedCustomers())
                {


                    if ((customer.Queue == QueueType.Accepted || customer.Queue == QueueType.Rejected) &&
                        (CurrentGroupView == GroupView.PostScrutiny || CurrentGroupView == GroupView.Accepted ||
                         CurrentGroupView == GroupView.Rejected)) return true;

                    return false;
                }
            }
            return false;
        }

        private bool CanResetCustomer(object state)
        {
            if (HasWriteAccess && IsCustomerSelected() && NoDialogsOpenButDetails() && IsCustomerLockedByMe(null))
            {
                foreach (Customer customer in GetSelectedCustomers())
                {
                    return postScrutinyQueues.Contains(customer.Queue);
                }
            }
            return false;
        }

        private bool InScrutiny()
        {

            foreach (Customer customer in GetSelectedCustomers())
            {
                if (IsCustomerLockedByAnotherUser(customer))
                {
                    return false;
                }

                return scrutinyQueues.Contains(customer.Queue);
            }

            return false;
        }

        private bool IsSingleCustomerSelected()
        {
            return GetSelectedCustomers().Count == 1;
        }
        private bool IsCustomerSelected()
        {
            return GetSelectedCustomers().Count > 0;
        }
        private bool CanAddLegalHold(object state)
        {
            foreach (Customer customer in GetSelectedCustomers())
            {
                if (customer.LockedBy == Environment.UserName) return true;
            }
            return false;
        }
        private bool CanRemoveLegalHold(object state)
        {
            foreach (Customer customer in GetSelectedCustomers())
            {
                if (customer.LockedBy == Environment.UserName && customer.Legal.Hold) return true;
            }
            return false;
        }
        private bool CanRemoveVat(object state)
        {
            if (IsCustomerLockedByMe(state))
            {
                if (!string.IsNullOrEmpty(VatNumber))
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsCustomerLockedByMe(object state)
        {
            foreach (Customer customer in GetSelectedCustomers())
            {
                if (customer.IsLocked && customer.LockedBy == Environment.UserName) return true;
            }
            return false;
        }


        #endregion

    }
}