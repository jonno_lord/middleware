﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.FeedRunner;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public class FeedRunnerViewModel : PageViewModel
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.RunFeeds,
            DisplayName = "MSI Feed Runner",
            ObjectDisplayName = "Feed",
            FiltersVisible = false,
            PropertySearch = false,
            DateSearch = false,
            SearchPropertiesList = new List<ComboItem<SearchBy>>(),
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };
        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        #endregion

        #region Fields

        private readonly IFeedRunnerService feedRunnerService;
        private readonly IMessageService messageService;
        private readonly IJobRunnerService jobRunnerService;
        private readonly ISystemService systemService;

        private string job;

        #endregion

        #region Properties

        private ObservableCollection<BatchFeed> batches;
        public ObservableCollection<BatchFeed> Batches
        {
            get { return this.batches; }
            set
            {
                if (value == this.batches) return;
                this.batches = value;
                OnPropertyChanged("Batches");
            }
        }

        private BatchFeed selectedBatch;        
        public BatchFeed SelectedBatch
        {
            get { return this.selectedBatch; }
            set
            {
                if (value == this.selectedBatch) return;
                this.selectedBatch = value;
                OnPropertyChanged("SelectedBatch");
            }
        }

        private ObservableCollection<FileHistory> fileHistory;
        public ObservableCollection<FileHistory> FileHistory
        {
            get { return this.fileHistory; }
            set
            {
                if (value == this.fileHistory) return;
                this.fileHistory = value;
                OnPropertyChanged("FileHistory");
            }
        }

        private DateTime? batchDate;
        public DateTime? BatchDate
        {
            get { return this.batchDate; }
            set
            {
                if (value == this.batchDate) return;
                this.batchDate = value;
                OnPropertyChanged("BatchDate");
            }
        }
         
        #endregion

        #region Commands

        public ICommand PopulateBatchesCommand { get; set; }


        private void PopulateBatches(object state)
        {
            Batches = new ObservableCollection<BatchFeed>(feedRunnerService.GetFeedBatches((DateTime)BatchDate));
            SelectedBatch = null;
        }
        private bool CanPopulateBatches(object state)
        {
            return BatchDate != null;
        }

        public ICommand RunJobCommand { get; set; }

        private void RunJob(object state)
        {

            FireJob(SelectedBatch.Category);
        }
        private bool CanRunJob(object state)
        {
            return SelectedBatch != null;
        }
         
        #endregion

        private void FireJob(string category)
        {

            job = category;

            if (!string.IsNullOrEmpty(category))
            {
                jobRunnerService.RunJob(systemService.GetJobIdFromJobName(category));
            }

      
        }

        private void InitialiseCommands()
        {
            PopulateBatchesCommand = new DelegateCommand(PopulateBatches, CanPopulateBatches);
            RunJobCommand = new DelegateCommand(RunJob, CanRunJob);
        }

        #region Constructor

        public FeedRunnerViewModel(IFeedRunnerService feedRunnerService, IMessageService messageService, IJobRunnerService jobRunnerService, ISystemService systemService)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.messageService = messageService;
            this.feedRunnerService = feedRunnerService;
            this.feedRunnerService.OnDrillDown += DrillDown;
            this.jobRunnerService = jobRunnerService;
            this.systemService = systemService;



            BatchDate = DateTime.Now.Date;

            PopulateBatches(null);

            InitialiseCommands();
        }


        #endregion

        protected override void OnDispose()
        {
            feedRunnerService.OnDrillDown -= DrillDown;
            base.OnDispose();
        }

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            BatchFeed batch = sender as BatchFeed;
            if(batch != null)
            {
                SelectedBatch = batch;
            }
            base.DrillDown(sender);
        }

    }
}
