﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Windows.Input;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;
using UBM.NotificationClient;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public class DiagnosticsViewModel : PageViewModel
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.Diagnostics,
            DisplayName = "Diagnostics",
            ObjectDisplayName = "",
            FiltersVisible = false,
            PropertySearch = false,
            DateSearch = false,
            SearchPropertiesList = new List<ComboItem<SearchBy>>(),
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };
        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        #endregion

        private readonly IMessageService messageService;

        public DiagnosticsViewModel(IMessageService messageService)
        {
            this.messageService = messageService;
        }

        private DelegateCommand loggerTest;
        public ICommand LoggerTestCommand
        {
            get
            {
                if (loggerTest == null) loggerTest = new DelegateCommand(LoggerTest);
                return loggerTest;
            }
        }
        public void LoggerTest(object state)
        {
            Log.Information("Logger Test");
            messageService.SendUserMessage("Test log entry has been written. Check logs to confirm", MessageType.Info);
        }

        private DelegateCommand notificationTest;
        public ICommand NotificationTestCommand
        {
            get
            {
                if (notificationTest == null) notificationTest = new DelegateCommand(NotificationTest);
                return notificationTest;
            }
        }
        public void NotificationTest(object state)
        {
            NotificationServiceClient.Email("", Environment.UserName, "", "Test Email", "Test Body", Priority.High, false);
            messageService.SendUserMessage("Test email sent to " + Environment.UserName + ".", MessageType.Info);
        }

        private DelegateCommand exceptionNotificationTest;
        public ICommand ExceptionNotificationTestCommand
        {
            get
            {
                if (exceptionNotificationTest == null) exceptionNotificationTest = new DelegateCommand(ExceptionNotificationTest);
                return exceptionNotificationTest;
            }
        }
        public void ExceptionNotificationTest(object state)
        {
            throw new ApplicationException("This is a test exception");            
        }

        private DelegateCommand showConfigFile;
        public ICommand ShowConfigFileCommand
        {
            get { return showConfigFile ?? (showConfigFile = new DelegateCommand(ShowConfigFile)); }
        }
        private void ShowConfigFile(object state)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            StreamReader sr =new StreamReader(configuration.FilePath);
            Config = sr.ReadToEnd();

            sr.Close();
        }

        private string config;
        public string Config
        {
            get { return config; }
            set
            {
                if (value == config) return;
                config = value;
                OnPropertyChanged("Config");
            }
        }
			
			

    }
}
