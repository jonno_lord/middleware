﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using UBM.Logger;
using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public partial class DedupeViewModel : GenericPageableViewModel<Customer>, ISupportsCustomerDetails
    {
        #region Page Definition


        private static PageConfiguration pageConfiguration = new PageConfiguration
                                                                 {
                                                                     Page = ClientPageNumber.CustomerDedupe,
                                                                     DisplayName = "Customer Dedupe",
                                                                     ObjectDisplayName = "Customer",
                                                                     FiltersVisible = true, 
                                                                     PropertySearch = false,
                                                                     DateSearch = false,
                                                                     SearchPropertiesList = new List<ComboItem<SearchBy>>(),
                                                                     DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
                                                                 };
        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        public override void Load()
        {
            if (!LoadedFirstTime)
            {
                asyncWorkerService.RunAsyncUserOperation(LoadAsync, "Customer Dedupe load");
            }
        }

        private void LoadAsync()
        {
            dedupeService.ReloadCustomers();

            scrutinyQueues = dedupeService.GetQueueListFromGroupView(GroupView.Scrutiny);
            postScrutinyQueues = dedupeService.GetQueueListFromGroupView(GroupView.PostScrutiny);

            

            ObservableCollection<Legal> legal = new ObservableCollection<Legal>(dedupeService.GetLegalHistorySnapshot());
            ObservableCollection<Email> email = new ObservableCollection<Email>(dedupeService.GetRejectionEmailsSnapshot());


            try
            {
                syncController = new DedupeSyncController(masterSyncController, CustomersAll, this);
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                messageService.SendUserMessage("Error initialising Synchronisation Service. Please check logs", MessageType.Error);
                Log.Error("Problem initialising Synchronisation Service", exception);

            }


            SendOrPostCallback callback = delegate
                                              {
                                                  LegalSnapshot = legal;
                                                  EmailSnapshot = email;
                                                  InitialiseDedupe();
                                                  syncController.SyncClientCollection(true);
                                              };
            asyncWorkerService.Post(callback);


            base.Load();

        }

        public override void RefreshPage()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            try
            {
                dedupeService.ReloadCustomers();
                GetDedupeCustomers();

                SetCustomersView(CurrentGroupView, false);

                syncController.SyncClientCollection(CustomersAll);
                syncController.SyncClientCollection(true);

            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                Log.Error("Problem Refreshing Dedupe", exception);
                messageService.SendUserMessage("Problem Synchronising Client. Please report this error.", MessageType.Error);

            }
        }

        #endregion

        #region Fields

        private readonly IDedupeService dedupeService;
        private readonly IMessageService messageService;


        private DedupeSyncController syncController;
        private readonly IAsyncWorkerService asyncWorkerService;

        private List<QueueType> scrutinyQueues;
        private List<QueueType> postScrutinyQueues;
        private readonly MasterSyncController masterSyncController;

        private readonly List<SalesSystem> salesSystems;

        #endregion

        #region Properties

        
        public INotificationDialogViewModel NotificationViewModel { get; private set; }
        public ICustomerDetailsViewModel CustomerDetailsViewModel { get; private set; }

        public List<Customer> CustomersView
        {
            get
            {
                if(dedupeService != null)
                {
                    return dedupeService.CustomersView;
                }
                return null;
            }
        }
        public List<Customer> CustomersAll
        {
            get
            {
                if(dedupeService != null)
                {
                    return dedupeService.CustomersAll;
                }
                return null;
            }
        }

        private ObservableCollection<Email> emailSnapshot;
        public ObservableCollection<Email> EmailSnapshot
        {
            get { return this.emailSnapshot; }
            set
            {
                if (value == this.emailSnapshot) return;
                this.emailSnapshot = value;
                OnPropertyChanged("EmailSnapshot");
            }
        }

        private ObservableCollection<Legal> legalSnapshot;
        public ObservableCollection<Legal> LegalSnapshot
        {
            get { return this.legalSnapshot; }
            set
            {
                if (value == this.legalSnapshot) return;
                this.legalSnapshot = value;
                OnPropertyChanged("LegalSnapshot");
            }
        }

        private List<Customer> selectedCustomers;
        public List<Customer> SelectedCustomers
        {
            get { return selectedCustomers; }
            set
            {
                if (value == selectedCustomers) return;
                selectedCustomers = value;
                OnPropertyChanged("SelectedCustomers");
            }
        }

        private GroupView currentGroupView;
        public GroupView CurrentGroupView
        {
            get { return currentGroupView; }
            set
            {
                if (value == currentGroupView) return;
                currentGroupView = value;
                OnPropertyChanged("CurrentGroupView");
            }
        }

        private string legalReason;
        public string LegalReason
        {
            get { return legalReason; }
            set
            {
                if (value == legalReason) return;
                legalReason = value;
                OnPropertyChanged("LegalReason");
            }
        }

        private string vatNumber;
        public string VatNumber
        {
            get { return vatNumber; }
            set
            {
                if (value == vatNumber) return;
                vatNumber = value;
                OnPropertyChanged("VatNumber");
            }
        }

        public List<Rejection> RejectionReasonsList
        {
            get
            {
                if (dedupeService == null) return null;
                return dedupeService.GetRejectionReasons();
            }
        }

        public int PostScrutinyCount
        {
            get { return CustomersAll.Where(c => c.Queue == QueueType.Accepted || c.Queue == QueueType.Rejected).Count(); }
        }
        
        #endregion

        #region Private Methods

        private void GetDedupeCustomers()
        {
            dedupeService.FilterOnSystems(salesSystems);
        }
        

        private void CommitCustomer(Customer customer)
        {
            try
            {
                dedupeService.CommitCustomer(customer);
                Reset(customer);
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                Log.Error("Problem comitting customer", exception);
                messageService.SendUserMessage("Problem comitting customer", MessageType.Error);
                throw;
            }
        }

        private void UpdateCustomerView()
        {
            if(dedupeService != null)
            {
                syncController.SyncClientCollection(CustomersAll);

                BeginPaging(CustomersView);
            }
        }

        private void SetCustomersView(GroupView view, bool deselect = true)
        {
            CurrentGroupView = view;

            dedupeService.CurrentGroupView = CurrentGroupView;

            if(deselect) DeselectCustomers();

            if(SelectedCustomer != null)
            {
                SelectedCustomer = dedupeService.GetSelectedCustomer();
                if (SelectedCustomer == null) messageService.CloseDialog(DialogType.CustomerDetails);
            }


            UpdateCustomerView();
        }

        private void DeselectCustomers()
        {
            foreach (Customer customer in CustomersAll)
            {
                if (customer.IsSelected && !IsCustomerViewedByMe(customer))
                {
                    customer.IsClicked = false;
                    customer.IsSelected = false;
                }
            }
        }

        private void Reset(Customer customer)
        {
            dedupeService.ResetCustomer(customer);

            syncController.Unlock(customer);

            OnPropertyChanged("PostScrutinyCount");
        }

        private void SetVat(Customer customer, bool add)
        {
            if (!add) VatNumber = "";

            if (VatNumber.Length < 20)
            {
                dedupeService.SetVatNumber(customer, VatNumber);
            }
            else
            {
                messageService.SendUserMessage("VAT Number must be less than 20 characters", MessageType.Bad);
            }
        }

        private List<Customer> GetSelectedCustomers()
        {
            if(SelectedCustomer != null)
            {
                CustomersView.ToList().ForEach(c=>c.IsSelected=false);
                return new List<Customer>{SelectedCustomer};
            }
            return CustomersView.Where(c => c.IsSelected).ToList();
        }

        private void SetSelectedCustomerToEmpty()
        {
            if (SelectedCustomer != null)
            {
                syncController.Unview(SelectedCustomer);
                SelectedCustomer.IsClicked = false;
                SelectedCustomer = null;
                dedupeService.DeselectCustomer();
            }
        }

        private void SetSelectedCustomer(Customer customer)
        {
            if(SelectedCustomer != null)
            {
                syncController.Unview(SelectedCustomer);
                SelectedCustomer.IsClicked = false;
                SelectedCustomer = null;
            }
            if(customer != null)
            {
                syncController.View(customer);
                SelectedCustomer = customer;
                CustomerDetailsViewModel.SelectedCustomer = customer;
            }
        }

        private void OnCustomerDetailsClosed(object sender, EventArgs args)
        {
            SetSelectedCustomerToEmpty();
        }

        #endregion

        #region Constructor

        public DedupeViewModel(IDedupeService dedupeService, INotificationDialogViewModel notificationDialogViewModel, 
                               IMessageService messageService, ICustomerDetailsViewModel customerDetailsViewModel,
                               MasterSyncController masterSyncController, List<SalesSystem> salesSystems, 
                               IAsyncWorkerService asyncWorkerService, bool hasWriteAccess)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.salesSystems = salesSystems;
            this.masterSyncController = masterSyncController;
            this.asyncWorkerService = asyncWorkerService;
            this.messageService = messageService;
            this.dedupeService = dedupeService;
            this.CustomerDetailsViewModel = customerDetailsViewModel;
            this.HasWriteAccess = hasWriteAccess;

            NotificationViewModel = notificationDialogViewModel;
            NotificationViewModel.AcceptNotification += AcceptNotificationDialog;
            NotificationViewModel.CancelNotification += CancelNotificationDialog;
            CustomerDetailsViewModel.CustomerDetailsClosed += OnCustomerDetailsClosed;
            CustomerDetailsViewModel.MergeDedupeCustomer += MergeCustomer;
            CustomerDetailsViewModel.ResendEmail += PrepareResendEmail;


            InitialiseCommands();
        }

        #endregion

        #region Public Methods

        public void InitialiseDedupe()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            dedupeService.OnDrillDown += DrillDown;

            GetDedupeCustomers();
            OnPropertyChanged("PostScrutinyCount");
            CurrentGroupView = GroupView.Scrutiny;
            SetCustomersView(CurrentGroupView, deselect: false);
        }

        public void RefreshCustomerView()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            SetCustomersView(CurrentGroupView);
        }

        public bool HasActiveLocks()
        {
            return PostScrutinyCount > 0;
        }

        #endregion


        #region ICustomerDetailsViewModel

        private Customer selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                if (value == selectedCustomer) return;
                selectedCustomer = value;
                OnPropertyChanged("SelectedCustomer");
            }
        }


        private void CloseCustomerDetails()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            messageService.CloseDialog(DialogType.CustomerDetails);
            SetSelectedCustomerToEmpty();
        }


        private void PrepareResendEmail(object sender, EventArgs args)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email email = sender as Email;

            if (email != null)
            {
                NotificationViewModel.PrepareResend(SelectedCustomer, email);

                messageService.OpenDialog(DialogType.RejectNotification);
            }
        }



        private void AcceptNotificationDialog(object sender, EventArgs args)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            

           // if (NotificationViewModel.CheckAllRecipientsAreValid())
           // {               

                if(NotificationViewModel.ResendPrepared)
                {
                    Email resend = NotificationViewModel.Resend();
                    dedupeService.AddRejectionEmail(resend);
                    SelectedCustomer.Emails.Add(resend);

                    messageService.CloseDialog(DialogType.RejectNotification);
                }
                else
                {
                    foreach (Customer customer in NotificationViewModel.GetNotificationCustomers())
                    {

                        if (customer.Queue != QueueType.Rejected)
                        {
                            dedupeService.RejectCustomer(customer);


                            UpdateCustomerView();

                        }

                        dedupeService.UpdateEmail(customer);
                    }

                    messageService.CloseDialog(DialogType.RejectNotification);
                    CloseCustomerDetails();
                }
            //}

            OnPropertyChanged("PostScrutinyCount");

        }
        private bool CanAcceptNotificationDialog(object state)
        {
            return (CurrentGroupView == GroupView.PostScrutiny || CurrentGroupView == GroupView.Rejected) || NotificationViewModel.AtLastEmail;
        }
       

        private void CancelNotificationDialog(object sender, EventArgs args)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if(NotificationViewModel.ResendPrepared)
                NotificationViewModel.AbortResend();

            //if (CurrentGroupView != GroupView.PostScrutiny && CurrentGroupView != GroupView.Rejected)
            {
                List<Customer> customers = NotificationViewModel.GetNotificationCustomers();

                customers.ForEach(syncController.Unlock);

                NotificationViewModel.ClearCustomers();
            }

            messageService.CloseDialog(DialogType.RejectNotification);
        }
        private bool CanCancelNotificationDialog(object state)
        {
            return !messageService.IsDialogOpen(DialogType.MessageBox);
        }


        #endregion

        #region IDrillDown

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            SetSelectedCustomer((Customer) sender);

            messageService.OpenDialog(DialogType.CustomerDetails);
            base.DrillDown(sender);
        }

        #endregion

        #region IDisposable

        protected override void OnDispose()
        {
            SetSelectedCustomerToEmpty();
            dedupeService.OnDrillDown -= DrillDown;
            CustomerDetailsViewModel.CustomerDetailsClosed -= OnCustomerDetailsClosed;


            //NotificationViewModel.Dispose();
            base.OnDispose();
        }

        #endregion

       
    }
}