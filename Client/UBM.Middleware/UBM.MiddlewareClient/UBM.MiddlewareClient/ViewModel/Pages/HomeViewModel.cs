﻿using System.Collections.Generic;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public class HomeViewModel : PageViewModel
    {
        #region Page Definition

        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.HomePage,
            DisplayName = "Home",
            ObjectDisplayName = "",
            FiltersVisible = false,
            PropertySearch = false,
            DateSearch = false,
            SearchPropertiesList = new List<ComboItem<SearchBy>>(),
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>()
        };
        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        #endregion
    }
}
