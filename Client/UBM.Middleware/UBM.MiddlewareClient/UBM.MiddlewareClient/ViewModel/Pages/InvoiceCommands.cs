﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Invoicing;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    public partial class InvoiceViewModel : PageViewModel,  IInvoiceViewModel 
    {

        public ICommand AddInvoiceCommand { get; private set; }
        public ICommand RemoveInvoiceCommand { get; private set; }
        public ICommand ClearInvoicesCommand { get; private set; }
        public ICommand IgnoreWarningsCommand { get; private set; }
        public ICommand ExtractionCommand { get; private set; }
        public ICommand RunAllExtractionCommand { get; private set; }
        public ICommand SelectedExtractionCommand { get; private set; }


        void InitialiseCommands()
        {
            AddInvoiceCommand = new DelegateCommand(AddInvoice, CanEditInvoices);
            RemoveInvoiceCommand = new DelegateCommand(RemoveInvoice, CanEditInvoices);
            ClearInvoicesCommand = new DelegateCommand(ClearInvoices, CanEditInvoices);
            IgnoreWarningsCommand = new DelegateCommand(IgnoreWarnings, CanIgnoreWarnings);
            ExtractionCommand = new DelegateCommand(Extraction, CanRunJob);
            RunAllExtractionCommand = new DelegateCommand(RunAllExtraction, CanRunJob);
            SelectedExtractionCommand = new DelegateCommand(RunSelectedExtraction, CanRunSelectedExtraction);
        }

        #region Commands

        #region Invoices

        private void AddInvoice(object state)
        {
            int addedCount = 0;
            int alreadyAdded = 0;
            int hasWarnings = 0;
            int isLocked = 0;

            foreach (Invoice invoice in SelectedPublication.Invoices)
            {
                if (invoice.IsSelected)
                {
                    if (SelectedInvoices.Contains(invoice))
                    {
                        alreadyAdded++;
                    }
                    else if (invoice.Warnings != null && invoice.Warnings.Count > 0 && !invoice.IgnoreWarnings)
                    {
                        hasWarnings++;
                    }
                    else if (invoice.IsLocked)
                    {
                        isLocked++;
                    }
                    else
                    {
                        SelectedInvoices.Add(invoice);
                        invoiceSync.Lock(invoice);
                        addedCount++;
                        invoice.IsSelected = false;
                    }
                }
            }

            string summary = "";
            if (addedCount > 0) summary += addedCount + " Added To Selected Invoices.";
            if (alreadyAdded > 0) summary += " " + alreadyAdded + " Not Added - Already In Selected Invoices List.";
            if (hasWarnings > 0) summary += " " + hasWarnings + " Not Added - Check Warnings.";
            if (isLocked > 0) summary += " " + isLocked + " Not Added - Locked By Another User";

            if (summary.Length > 0) messageService.SendUserMessage(summary, MessageType.Info);
        }

        private void RemoveInvoice(object state)
        {
            int removed = 0;
            for (int i = SelectedInvoices.Count - 1; i >= 0; i--)
            {
                if (SelectedInvoices[i].IsSelected)
                {
                    invoiceSync.Unlock(SelectedInvoices[i]);
                    SelectedInvoices.RemoveAt(i);
                    removed++;
                }
            }

            string message;
            if (removed > 1)
            {
                message = removed + " Invoice Removed.";
            }
            else
            {
                message = removed + " Invoices Removed.";
            }

            messageService.SendUserMessage(message, MessageType.Info);
        }

        private void ClearInvoices(object state)
        {
            int count = SelectedInvoices.Count();
            foreach (Invoice invoice in SelectedInvoices)
            {
                invoiceSync.Unlock(invoice);
            }
            SelectedInvoices.Clear();
            messageService.SendUserMessage("Selected Invoices Cleared. " + count + " Removed.", MessageType.Info);
        }

        private void IgnoreWarnings(object state)
        {
            SelectedInvoice.IgnoreWarnings = !SelectedInvoice.IgnoreWarnings;

            SelectedInvoice.Status = SelectedInvoice.IgnoreWarnings ? "Warnings Ignored" : "Warnings";
        }

        private bool CanIgnoreWarnings(object state)
        {
            return SelectedInvoice != null;
        }

        private bool CanEditInvoices(object state)
        {
            return HasWriteAccess;
        }

        #endregion

        #region Job Running

        public void Extraction(object state)
        {
            List<string> jobNames = MiddlewareJobNames.GetAsopJobNamesOfType((MiddlewareJobType)state);

            RunJobs(jobNames);
        }
         
        private void RunAllExtraction(object state)
        {
            List<string> jobNames = MiddlewareJobNames.GetAsopJobNamesOfType(MiddlewareJobType.InvoicesImmediate);
            jobNames = jobNames.Union(MiddlewareJobNames.GetAsopJobNamesOfType(MiddlewareJobType.InvoicesPrepaid)).ToList();

            RunJobs(jobNames);
        }

        private void RunSelectedExtraction(object state)
        {

            foreach (Invoice invoice in SelectedInvoices)
            {
                invoiceService.InsertPublicationInvoicingParameters(invoice.Publication.Code, invoice.IssueDate, invoice.SystemName);
                processingInvoices.Add(invoice);
            }

            IEnumerable<string> systems = SelectedInvoices.Select(c => c.SystemName)
                                                    .Distinct()
                                                    .ToList();
            
            List<string> jobNames = new List<string>();

            foreach (string system in systems)
            {
                jobNames.AddRange((MiddlewareJobNames.GetAsopJobNamesOfType(MiddlewareJobType.InvoicesPublication, system)));
            }

            RunJobs(jobNames);
            
          
        }

     


        private bool CanRunSelectedExtraction(object state)
        {
            return HasWriteAccess && SelectedInvoices.Count > 0;
        }

        private bool CanRunJob(object state)
        {
            return HasWriteAccess && !jobRunnerService.Running;
        }

        #endregion 

        #endregion

   
    }
}