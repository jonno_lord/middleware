﻿using System.Collections.Generic;
using System.Threading;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Infrastructure.Names;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Pages
{
    /// <summary>
    /// Controls Customer Collections
    /// </summary>
    public class CustomerViewModel : GenericPageableViewModel<Customer>,  ISearchable, ISupportsCustomerDetails
    {
        #region Page Definition
        
        private static PageConfiguration pageConfiguration = new PageConfiguration
        {
            Page = ClientPageNumber.CustomerViewer,
            DisplayName = "Customer Viewer",
            ObjectDisplayName = "Customer",
            FiltersVisible = true,
            PropertySearch = true,
            DateSearch = true,
            SearchPropertiesList = new List<ComboItem<SearchBy>>
                                    {
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.CUSTOMER_NAME, Value = SearchBy.CustomerName },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.URN, Value = SearchBy.URN },
                                        new ComboItem<SearchBy>{ Name = SearchFieldNames.JDE_NUMBER, Value = SearchBy.JDENumber }
                                    },
            DateFilterPropertiesList = new List<ComboItem<SearchOnDates>>
                                    {
                                        new ComboItem<SearchOnDates>{ Name = DateSearchFieldNames.LAST_ACTIONED, Value = SearchOnDates.LastActioned },
                                        new ComboItem<SearchOnDates>{ Name = DateSearchFieldNames.LOADED_INTO_MIDDLEWARE, Value = SearchOnDates.LoadedIntoMiddleware}
                                    },
        };

        public override PageConfiguration PageConfiguration
        {
            get { return pageConfiguration; }
        }

        public override void RefreshPage()
        {
            if(lastSearch != null)
            {
                SearchAsync(lastSearch);
            }
        }

        #endregion

        #region Fields

        private readonly ICustomerService customerService;
        private readonly IAsyncWorkerService asyncWorkerService;
        private readonly IMessageService messageService;
        private SearchInfo lastSearch;

        #endregion

        public ICustomerDetailsViewModel CustomerDetailsViewModel { get; set; }

        #region Constructor

        public CustomerViewModel(ICustomerService customerService, 
                                 IAsyncWorkerService asyncWorkerService, 
                                 IMessageService messageService,
                                 ICustomerDetailsViewModel customerDetailsViewModel) 
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.asyncWorkerService = asyncWorkerService;
            this.messageService = messageService;
            this.customerService = customerService;
            this.CustomerDetailsViewModel = customerDetailsViewModel;
            this.customerService.OnDrillDown += DrillDown;
        }

        #endregion


        #region ISearchable

        public void SearchAsync(SearchInfo searchInfo)
        {
            asyncWorkerService.RunAsyncUserOperation(() => Search(searchInfo), "Customer search");
        }

        public int Search(SearchInfo searchInfo)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            lastSearch = searchInfo;
            List<Customer> customers = customerService.SearchCustomers(searchInfo);

            SendOrPostCallback callback = delegate
            {
                BeginPaging(customers);
            };
            asyncWorkerService.Post(callback);

            return customers.Count;
        }

        #endregion

        #region IDrillDown

        public override void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Customer customer = sender as Customer;
            if(customer != null)
            {
                CustomerDetailsViewModel.SelectedCustomer = customer;
                messageService.OpenDialog(DialogType.CustomerDetails);
            }
            base.DrillDown(sender);
        }

        #endregion

        #region IDispose

        protected override void OnDispose()
        {
            customerService.OnDrillDown -= DrillDown;
        }

        #endregion

        
    }
}