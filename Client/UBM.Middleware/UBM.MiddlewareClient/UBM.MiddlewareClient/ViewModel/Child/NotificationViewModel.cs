﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.Model.Interfaces;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.ViewModel.Child
{
    public partial class NotificationViewModel : ViewModelBase, INotificationDialogViewModel
    {
        private readonly INotificationService notificationService;
        private readonly IMessageService messageService;

        #region Properties

        public List<Rejection> RejectionReasonsList { get; set; }

        public bool IsEmpty
        {
            get { return customers.Count == 0; }
        }
        public bool AtLastEmail
        {
            get { return !CanMoveToNextEmail(null); }
        }

        public bool ResendPrepared { get; private set; }
        
        private Rejection selectedRejectionReason;
        public Rejection SelectedRejectionReason
        {
            get { return this.selectedRejectionReason; }
            set
            {
                if (value == this.selectedRejectionReason) return;
                this.selectedRejectionReason = value;
                OnPropertyChanged("SelectedRejectionReason");

                if (SelectedCustomer.Email.SelectedReason != selectedRejectionReason)
                    SelectedCustomer.Email.SelectedReason = selectedRejectionReason;

                //UpdateReasonFields(SelectedCustomer);

            }
        }

        private Customer selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return this.selectedCustomer; }
            set
            {
                if (value == this.selectedCustomer) return;
                this.selectedCustomer = value;
                OnPropertyChanged("SelectedCustomer");
            }
        }

        private int notificationIndex;
        public int NotificationIndex
        {
            get { return notificationIndex; }
            set
            {
                if (value == notificationIndex) return;
                notificationIndex = value;
                OnPropertyChanged("NotificationIndex");
            }
        }

        public int NotificationCount
        {
            get { return customers.Count; }
        }

        private List<Customer> customers;
        public List<Customer> Customers
        {
            get { return customers; }
            set
            {
                if (value == customers) return;
                customers = value;
                OnPropertyChanged("NotificationCustomers");
            }
        }

        #endregion

        #region Private Methods

        private void SetupEmail(Customer customer)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if (string.IsNullOrEmpty(customer.Email.From)) customer.Email.From = Environment.UserName;
            if (string.IsNullOrEmpty(customer.Email.To)) customer.Email.To = customer.SaleInformation.SalesPerson;

            // @JP DIRTY HACK!
            if(customer.SearchType == "CH")
            {
                customer.Email.CC.CCList.Add(ConfigurationManager.AppSettings["RejectionEmailCC"]);
            }

            customer.Email.SelectedReason = RejectionReasonsList[0];
        }

        private void PopulateSOPDuplicates(Customer customer)
        {
            customer.Scrutiny.SOPDuplicates = customer.Scrutiny.Duplicates.Where(c => c.SystemName != "JDE").ToList();
            customer.Scrutiny.SOPDuplicates.ForEach(c=>c.OnDrillDown += DrillDown);
        }

        private void CheckEmail(string name)
        {

            List<string> addresses = notificationService.ResolveEmail(name);

            if (addresses.Count == 1)
            {
                messageService.SendUserMessage("Recipient: " + name + " is valid - " + addresses[0], MessageType.Good);
            }
            else
            {
                messageService.SendUserMessage("Recipient: " + name + " is NOT valid", MessageType.Bad);
            }
        }

      

        #endregion

        #region Constructor

        public NotificationViewModel(List<Rejection> rejectionReasonsList, INotificationService notificationService, IMessageService messageService)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            this.customers = new List<Customer>();
            this.RejectionReasonsList = rejectionReasonsList;
            this.notificationService = notificationService;
            this.messageService = messageService;
            
            SelectedCustomer = Customer.CreateNullCustomer();
            ResendPrepared = false;

            InitialiseCommands();
        }

        #endregion

        #region Public Methods

        private bool CheckAllRecipientsAreValid()
        {
            List<string> addresses = notificationService.ResolveEmail(SelectedCustomer.Email.To);

            if(addresses.Count == 0)
            {
                MessageBoxArgs messageBoxArgs = new MessageBoxArgs
                {
                    Title = "Recipient Not Found",
                    Message = "The recipient " + SelectedCustomer.Email.To + " could not be resolved. If this is a UBM user it may be incorrect. Do you wish to continue?",
                    MessageType = MessageType.Error,
                };

                if(messageService.OpenMessageBoxWithResult(messageBoxArgs) != DialogResult.OK)
                {
                    return false;
                }                
            }

            return true;

        }


        public void AddCustomer(Customer customer, bool newEmail)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if(newEmail) SetupEmail(customer);
            PopulateSOPDuplicates(customer);
            
            customers.Add(customer);

            if(customers.Count == 1) SelectNotificationCustomer(customer);

            OnPropertyChanged("NotificationCount");
        }

        public event EventHandler AcceptNotification;
        public event EventHandler CancelNotification;

        private void OnAcceptNotification(object sender, EventArgs args)
        {
            if(AcceptNotification != null)
            {
                AcceptNotification(sender, args);
            }
        }

        private void OnCancelNotification(object sender, EventArgs args)
        {
            if(CancelNotification != null)
            {
                CancelNotification(sender, args);
            }
        }

        public void PrepareResend(Customer customer, Email email)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            ClearCustomers();
            AddCustomer(customer, false);

            email.ShowFullBody();

            SelectedCustomer.Email = new Email
            {
                
                To = email.To,

                CustomerID = email.CustomerID,
                CustomerName = email.CustomerName,
                DateRejected = DateTime.Now,
                From = email.From,
                Reason = email.Reason,
                Sent = true,
                SystemName = email.SystemName,
                CC = new CC
                         {
                             CCList = email.CC.CCList,                          
                         }

            };

            SelectedCustomer.Email.CC.CCListConcatenated = SelectedCustomer.Email.CC.FullCCListConcatenated;

            PopulateSOPDuplicates(SelectedCustomer);

            Rejection rejection = RejectionReasonsList.Find(c => c.Reason == email.Reason);
            if (rejection != null)
            {
                SelectedCustomer.Email.SelectedReason = rejection;
            }
            else
            {
                SelectedCustomer.Email.SelectedReason = RejectionReasonsList[0];
            }
            SelectedRejectionReason = email.SelectedReason;

            SelectedCustomer.Email.Body = email.Body;

            SelectNotificationCustomer(customer);

            ResendPrepared = true;
        }

        public void AbortResend()
        {
            TraceHelper.Trace(TraceType.ViewModel);
            ResendPrepared = false;
        }

        public Email Resend()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Email.Send(SelectedCustomer);
            ResendPrepared = false;
            return SelectedCustomer.Email;
        }

        public void ClearCustomers()
        {
            TraceHelper.Trace(TraceType.ViewModel);

            foreach (Customer customer in Customers)
            {
                customer.Scrutiny.SOPDuplicates.ForEach(c=>c.OnDrillDown -= DrillDown);
            }
            customers.Clear();
            OnPropertyChanged("NotificationCount");
            NotificationIndex = 0;
        }

        public List<Customer> GetNotificationCustomers()
        {

            return customers;
        }


        public void SelectNotificationCustomer(Customer customer)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            if (customer != null)
            {
                if(customers.Contains(customer))
                {
                    customer.Email.ShowFullBody();
                    SelectedCustomer = customer;
                    SelectedRejectionReason = customer.Email.SelectedReason;
                    NotificationIndex = customers.IndexOf(customer);
                }
                else
                {
                    throw new ArgumentException("Customer not found");
                }
            }
        }

        #endregion

        #region IDrillDown

        public void DrillDown(object sender)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            Duplicate duplicate = sender as Duplicate;

            if(duplicate != null)
            {
                SelectedCustomer.Email.Body = "This customer is a duplicate of another " + SelectedCustomer.SystemName +
                                " customer with URN/Prospect No.: " + duplicate.URN;
            }
            else
            {
                messageService.SendUserMessage("Error retriveing duplicate", MessageType.Error);
            }
        }

        #endregion

        #region IDisposable

        protected override void OnDispose()
        {
            ClearCustomers();
            base.OnDispose();
        }

        #endregion

    }
}
