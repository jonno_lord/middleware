﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using UBM.MiddlewareClient.Dialogs;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;

namespace UBM.MiddlewareClient.Services
{
    public class MessageService: IMessageService
    {
        public event SendUserMessageEventHandler OnSendUserMessage;

        public void SendUserMessage(string message, MessageType messageType)
        {
            if(OnSendUserMessage != null)
            {
                OnSendUserMessage(message, messageType);
            }
        }

        public event DialogRequestEventHandler OnDialogRequest;

        private void DialogRequest(DialogEventArgs eventArgs)
        {
            if(OnDialogRequest != null)
            {
                OnDialogRequest(eventArgs);
            }
        }

        public MessageService()
        {
            if (DialogStates.Count == 0)
            {
                foreach (DialogType dialog in Enum.GetValues(typeof(DialogType)))
                {
                    DialogStates.Add(dialog, false);
                }
            }
        }

        #region Dialogs

        private static readonly Dictionary<DialogType, bool> DialogStates = new Dictionary<DialogType, bool>();

        public void OpenMessageBox(MessageBoxArgs messageBoxArgs)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            DialogRequest(new DialogEventArgs(DialogAction.Open, DialogType.MessageBox, messageBoxArgs));
            DialogStates[DialogType.MessageBox] = true;
        }

        public DialogResult OpenMessageBoxWithResult(MessageBoxArgs messageBoxArgs)
        {
            MessageDialog messageDialog = new MessageDialog();
            messageDialog.SetUpDialog(messageBoxArgs);

            messageDialog.ShowDialog();

            return messageDialog.DialogResult.HasValue && messageDialog.DialogResult.Value
                       ? DialogResult.OK
                       : DialogResult.Cancel;
        }


        public void OpenDialog(DialogType dialogType)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            DialogRequest(new DialogEventArgs(DialogAction.Open, dialogType, null));
            DialogStates[dialogType] = true;
        }

        public void CloseDialog(DialogType dialogType)
        {
            TraceHelper.Trace(TraceType.ViewModel);

            DialogRequest(new DialogEventArgs(DialogAction.Close, dialogType, null));
            DialogStates[dialogType] = false;
        }

        public void CloseAnyOpenDialogs()
        {
            CloseAnyOpenDialogsExcept(null);
        }

        public void CloseAnyOpenDialogsExcept(DialogType? exception)
        {
            List<DialogType> toClose = new List<DialogType>();
            foreach (KeyValuePair<DialogType, bool> keyValuePair in DialogStates)
            {
                if (keyValuePair.Value && keyValuePair.Key != exception) toClose.Add(keyValuePair.Key);
            }
            toClose.ForEach(CloseDialog);
        }

        public bool AnyDialogsOpen()
        {
            return AnyDialogsOpenExcept(null);
        }

        public bool AnyDialogsOpenExcept(DialogType? exception)
        {
            foreach (KeyValuePair<DialogType, bool> keyValuePair in DialogStates)
            {
                if (keyValuePair.Value && keyValuePair.Key != exception) return true;
            }
            return false;
        }

        public bool IsDialogOpen(DialogType dialogType)
        {
            return DialogStates[dialogType];
        }

        #endregion


    }
}
