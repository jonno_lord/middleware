﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareClient.ViewHelper
{
    public class ComboItem<T>
    {
        public string Name { get; set; }

        public T Value { get; set; }
    }
}
