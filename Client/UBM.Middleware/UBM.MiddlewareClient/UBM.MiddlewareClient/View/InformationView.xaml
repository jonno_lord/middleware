﻿<UserControl x:Class="UBM.MiddlewareClient.View.InformationView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:uc="clr-namespace:UBM.MiddlewareClient.UserControls"
             mc:Ignorable="d" 
             d:DesignHeight="300" d:DesignWidth="300" Name="ucInformation">
    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition />
            <RowDefinition />
        </Grid.RowDefinitions>
        
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto" />
            <ColumnDefinition Width="Auto" />
            <ColumnDefinition Width="*" />
        </Grid.ColumnDefinitions>    
        
        <!-- Status User Control for the Synchronisation service. -->
        <uc:StatusControl x:Name="ucsSync" Grid.RowSpan="2" Grid.Column="0"
                          Width="75" Height="75" Status="{Binding SyncState}"
                          RedSource="../Resources/Images/Red.png"
                          AmberSource="../Resources/Images/Amber.png"
                          GreenSource="../Resources/Images/Green.png"
                          StatusBackground="{StaticResource StandardBackgroundGradient}"
                          VerticalAlignment="Center"
                          MouseDown="ucsSyncMouseDown">
            <uc:StatusControl.Style>
                <Style TargetType="{x:Type uc:StatusControl}">
                    <Style.Triggers>
                        <DataTrigger Binding="{Binding ElementName=ucInformation, Path=Height}" Value="25">
                            <Setter Property="Visibility" Value="Hidden" />
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </uc:StatusControl.Style>
            <uc:StatusControl.ToolTip>
                <!-- ToolTip for this user control. -->
                <ToolTip>
                    <Grid>
                        <Grid.RowDefinitions>
                            <RowDefinition />
                            <RowDefinition />
                            <RowDefinition />
                            <RowDefinition />
                            <RowDefinition />
                        </Grid.RowDefinitions>
                        
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>

                        <!-- Top section of the ToolTip for the synchronisation. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0,0.5"
                                Grid.Row="0" Grid.ColumnSpan="2">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Displays the current status of the Synchronisation Service." />
                        </Border>

                        <!-- Holds the Red Image. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0.5,0.5"
                                Grid.Row="1" Grid.Column="0">
                            <Image Width="25" Height="25"
                                   Source="../Resources/Images/Red.png"/>
                        </Border>

                        <!-- Holds the Text for the Red Image. -->
                        <Border BorderBrush="White" BorderThickness="0.5,0.5,0,0.5"
                                Grid.Row="1" Grid.Column="1">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Not Connected" />
                        </Border>

                        <!-- Holds the Amber Image. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0.5,0.5"
                                Grid.Row="2" Grid.Column="0">
                            <Image Width="25" Height="25"
                                   Source="../Resources/Images/Amber.png"/>
                        </Border>

                        <!-- Holds the Text for the Amber Image. -->
                        <Border BorderBrush="White" BorderThickness="0.5,0.5,0,0.5"
                                Grid.Row="2" Grid.Column="1">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Checking" />
                        </Border>

                        <!-- Holds the Green Image. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0.5,0.5"
                                Grid.Row="3" Grid.Column="0">
                            <Image Width="25" Height="25"
                                   Source="../Resources/Images/Green.png"/>
                        </Border>

                        <!-- Holds the Text for the Green Image. -->
                        <Border BorderBrush="White" BorderThickness="0.5,0.5,0,0.5"
                                Grid.Row="3" Grid.Column="1">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Connected" />
                        </Border>

                        <Border BorderBrush="White" BorderThickness="0,0.5,0,0.5"
                                Grid.Row="4" Grid.ColumnSpan="2">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Click to re-check the service is running." />
                        </Border>
                    </Grid>
                </ToolTip>
            </uc:StatusControl.ToolTip>
        </uc:StatusControl>
        
        <!-- Status User Control for the Notification service. -->
        <uc:StatusControl x:Name="ucsNotify" Grid.RowSpan="2" Grid.Column="1"
                          Width="75" Height="75" Status="{Binding NotificationState}"
                          RedSource="../Resources/Images/Red.png"
                          AmberSource="..//Resources/Images/Amber.png"
                          GreenSource="../Resources/Images/Green.png"
                          StatusBackground="{StaticResource StandardBackgroundGradient}"
                          VerticalAlignment="Center"
                          MouseDown="ucsNotifyMouseDown">
            <uc:StatusControl.Style>
                <Style TargetType="{x:Type uc:StatusControl}">
                    <Style.Triggers>
                        <DataTrigger Binding="{Binding ElementName=ucInformation, Path=Height}" Value="25">
                            <Setter Property="Visibility" Value="Hidden" />
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </uc:StatusControl.Style>
            <uc:StatusControl.ToolTip>
                <ToolTip>
                    <Grid>
                        <Grid.RowDefinitions>
                            <RowDefinition />
                            <RowDefinition />
                            <RowDefinition />
                            <RowDefinition />
                            <RowDefinition />
                        </Grid.RowDefinitions>

                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>

                        <!-- Top section of the ToolTip for the Notification. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0,0.5"
                                Grid.Row="0" Grid.ColumnSpan="2">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Displays the current status of the Notification Server." />
                        </Border>

                        <!-- Holds the Red Image. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0.5,0.5"
                                Grid.Row="1" Grid.Column="0">
                            <Image Width="25" Height="25"
                                   Source="../Resources/Images/Red.png"/>
                        </Border>

                        <!-- Holds the Text for the Red Image. -->
                        <Border BorderBrush="White" BorderThickness="0.5,0.5,0,0.5"
                                Grid.Row="1" Grid.Column="1">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Not Connected" />
                        </Border>

                        <!-- Holds the Amber Image. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0.5,0.5"
                                Grid.Row="2" Grid.Column="0">
                            <Image Width="25" Height="25"
                                   Source="../Resources/Images/Amber.png"/>
                        </Border>

                        <!-- Holds the Text for the Amber Image. -->
                        <Border BorderBrush="White" BorderThickness="0.5,0.5,0,0.5"
                                Grid.Row="2" Grid.Column="1">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Checking" />
                        </Border>

                        <!-- Holds the Green Image. -->
                        <Border BorderBrush="White" BorderThickness="0,0.5,0.5,0.5"
                                Grid.Row="3" Grid.Column="0">
                            <Image Width="25" Height="25"
                                   Source="../Resources/Images/Green.png"/>
                        </Border>

                        <!-- Holds the Text for the Green Image. -->
                        <Border BorderBrush="White" BorderThickness="0.5,0.5,0,0.5"
                                Grid.Row="3" Grid.Column="1">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Connected" />
                        </Border>

                        <Border BorderBrush="White" BorderThickness="0,0.5,0,0.5"
                                Grid.Row="4" Grid.ColumnSpan="2">
                            <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}"
                                       Text="Click to re-check the server is running." />
                        </Border>
                    </Grid>
                </ToolTip>
            </uc:StatusControl.ToolTip>
        </uc:StatusControl>

        <!-- StackPanel to hold the message icon and text that is displayed to the user. -->
        <StackPanel Orientation="Horizontal" HorizontalAlignment="Center" Grid.RowSpan="2" Grid.Column="2">
            <StackPanel.ToolTip>
                <ToolTip Width="500" MaxHeight="400" Name="messageListToolTip">
                    <ToolTip.Style>
                        <Style BasedOn="{StaticResource ToolTipBaseStyle}" TargetType="{x:Type ToolTip}">
                            <Style.Triggers>
                                <DataTrigger Binding="{Binding ActionList.Count}" Value="0">
                                    <Setter Property="Visibility" Value="Collapsed" />
                                </DataTrigger>
                            </Style.Triggers>
                        </Style>
                    </ToolTip.Style>
                    <ItemsControl ItemsSource="{Binding ActionList}">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <StackPanel Orientation="Horizontal">
                                    <Image Name="icon" Width="25" Height="25" VerticalAlignment="Center">
                                        <Image.Style>
                                            <Style TargetType="{x:Type Image}">
                                                <Style.Triggers>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Error">
                                                        <Setter Property="Source" Value="../Resources/Images/Error.png" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Info">
                                                        <Setter Property="Source" Value="../Resources/Images/Info.png" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Good">
                                                        <Setter Property="Source" Value="../Resources/Images/Good.png" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Bad">
                                                        <Setter Property="Source" Value="../Resources/Images/Bad.png" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Busy">
                                                        <Setter Property="Source" Value="../Resources/Images/Busy.png" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </Image.Style>
                                    </Image>
                                    <TextBlock Text="{Binding Message}" 
                                                HorizontalAlignment="Left"
                                                Width="300"
                                                TextWrapping="Wrap">
                                        <TextBlock.Style>
                                            <Style TargetType="{x:Type TextBlock}" BasedOn="{StaticResource ActionMessageTextBlockStyle}">
                                                <Style.Triggers>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Error">
                                                        <Setter Property="Foreground" Value="DarkRed" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Info">
                                                        <Setter Property="Foreground" Value="Black" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Good">
                                                        <Setter Property="Foreground" Value="DarkGreen" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Bad">
                                                        <Setter Property="Foreground" Value="DarkRed" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Busy">
                                                        <Setter Property="Foreground" Value="Black" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </TextBlock.Style>
                                    </TextBlock>
                                    <TextBlock Text="{Binding Path=Time, StringFormat='{}{0:dd/MM hh:mm:ss}'}"
                                               Width="100"
                                               TextWrapping="Wrap"
                                               VerticalAlignment="Top">
                                            <TextBlock.Style>
                                            <Style TargetType="{x:Type TextBlock}" BasedOn="{StaticResource ActionMessageTextBlockStyle}">
                                                <Style.Triggers>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Error">
                                                        <Setter Property="Foreground" Value="DarkRed" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Info">
                                                        <Setter Property="Foreground" Value="Black" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Good">
                                                        <Setter Property="Foreground" Value="DarkGreen" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Bad">
                                                        <Setter Property="Foreground" Value="DarkRed" />
                                                    </DataTrigger>
                                                    <DataTrigger Binding="{Binding MessageType}" Value="Busy">
                                                        <Setter Property="Foreground" Value="Black" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </TextBlock.Style>
                                    </TextBlock>
                                </StackPanel>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>
                </ToolTip>
            </StackPanel.ToolTip>
            <!-- Image control that displays an icon for each message type. -->
            <Image Name="icon" Width="20" Height="20">
                <Image.Style>
                    <Style TargetType="{x:Type Image}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Error">
                                <Setter Property="Source" Value="../Resources/Images/Error.png" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Info">
                                <Setter Property="Source" Value="../Resources/Images/Info.png" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Good">
                                <Setter Property="Source" Value="../Resources/Images/Good.png" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Bad">
                                <Setter Property="Source" Value="../Resources/Images/Bad.png" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Busy">
                                <Setter Property="Source" Value="../Resources/Images/Busy.png" />
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </Image.Style>
            </Image>
            
            <!-- TextBlock to show the different action Messages. -->
            <TextBlock Name="txtMessage"
                       Padding="5"
                       Margin="5"
                       Text="{Binding ActionMessage}" 
                       HorizontalAlignment="Center">
                <TextBlock.Style>
                    <Style TargetType="{x:Type TextBlock}" BasedOn="{StaticResource ActionMessageTextBlockStyle}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Error">
                                <Setter Property="Foreground" Value="Red" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Info">
                                <Setter Property="Foreground" Value="Black" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Good">
                                <Setter Property="Foreground" Value="Green" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Bad">
                                <Setter Property="Foreground" Value="Red" />
                            </DataTrigger>
                            <DataTrigger Binding="{Binding ActionMessageType}" Value="Busy">
                                <Setter Property="Foreground" Value="Black" />
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </TextBlock.Style> 
            </TextBlock>

            <!-- Button to clear the action box. -->
            <Button Width="25" Height="25" Margin="0"
                    Command="{Binding ClearActionMessageCommand}"
                    HorizontalAlignment="Left" VerticalAlignment="Center"
                    Style="{StaticResource ButtonBaseStyle}">
                <Button.ToolTip>
                    <TextBlock Style="{StaticResource GeneralToolTipTextBlockStyle}">
                        Click to clear the action message.
                    </TextBlock>
                </Button.ToolTip>
                <Image Source="../Resources/Images/Delete.png" Width="15" />
            </Button>
        </StackPanel>
    </Grid>
</UserControl>
