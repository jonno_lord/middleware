﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for CustomerFiltersView.xaml
    /// </summary>
    public partial class SearchFiltersView : UserControl
    {
        #region Public Properties.

        public MainWindow MainWindow { get; set; }

        #endregion

        #region Constructors.

        public SearchFiltersView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
