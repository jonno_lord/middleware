﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for LegalHoldView.xaml
    /// </summary>
    public partial class LegalHoldView : UserControl
    {
        #region Public Properties.

        public LegalHistoryViewModel LHVM { get; set; }

        #endregion

        #region Constructors.

        public LegalHoldView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
