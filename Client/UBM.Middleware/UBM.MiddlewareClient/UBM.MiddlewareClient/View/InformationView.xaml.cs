﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for InformationView.xaml
    /// </summary>
    public partial class InformationView : UserControl
    {
        #region Public Properties.

        public InfoBarViewModel ViewModel { get; set; }

        #endregion

        #region Private Control Events.

        private void ucsSyncMouseDown(object sender, MouseButtonEventArgs e)
        {
            ViewModel.SyncServiceCheckAsync();
        }

        private void ucsNotifyMouseDown(object sender, MouseButtonEventArgs e)
        {
            ViewModel.NotificationServerCheckAsync();
        }

        #endregion

        #region Constructors.

        public InformationView()
        {
            InitializeComponent();

            //mvm = (MasterViewModel)DataContext;
        }

        #endregion
    }
}
