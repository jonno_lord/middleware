﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for CustomerDedupeView.xaml
    /// </summary>
    public partial class CustomerDedupeView : UserControl
    {

        #region Public Properties.

        public MainWindow MainWindow { get; set; }

        #endregion 

        #region Private Control Events.

        private void ViewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MainWindow.DialogOpen)
            {
                FrameworkElement fe = (FrameworkElement) sender;
                MainWindow.NavigateToPage((ClientPageNumber) fe.Tag);
            }
        }

        #endregion

        #region Constructors. 

        public CustomerDedupeView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
