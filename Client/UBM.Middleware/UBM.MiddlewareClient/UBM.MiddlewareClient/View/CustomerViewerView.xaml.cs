﻿using System.Windows.Controls;
using System.Windows.Input;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Pages;

namespace UBM.MiddlewareClient.View
{
    /// <summary>
    /// Interaction logic for CustomerSearchView.xaml
    /// </summary>
    public partial class CustomerViewerView : UserControl
    {
        #region Pulbic Propertiess.

        public CustomerViewModel CVM;

        #endregion

        #region Constructors.

        public CustomerViewerView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
