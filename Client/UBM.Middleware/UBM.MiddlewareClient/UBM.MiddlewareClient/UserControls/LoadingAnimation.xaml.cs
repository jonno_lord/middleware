﻿using System.Timers;
using System.Windows;
using System.Windows.Controls;

namespace UBM.MiddlewareClient.UserControls
{
    /// <summary>
    /// Interaction logic for LoadingAnimation.xaml
    /// </summary>
    public partial class LoadingAnimation : UserControl
    {
        #region Delegate Items.

        private delegate void VoidDelegete();

        #endregion

        #region Private Fields.

        private Timer timer;

        #endregion

        #region Private Window Control Events.

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            timer = new Timer(40);
            timer.Elapsed += OnTimerElapsed;
            timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            rotationCanvas.Dispatcher.Invoke
                (
                    new VoidDelegete(delegate
                    {
                        SpinnerRotate.Angle += 30;
                        if (SpinnerRotate.Angle == 360)
                        {
                            SpinnerRotate.Angle = 0;
                        }
                    }
                        ), null

                );
        }

        #endregion

        #region Constructors.

        public LoadingAnimation()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        #endregion
    }
}
