﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using UBM.MiddlewareClient.Enums;

namespace UBM.MiddlewareClient.UserControls
{
    /// <summary>
    /// Interaction logic for StatusControl.xaml
    /// </summary>
    public partial class StatusControl : UserControl
    {
        // DependencyProperty for the status of the control.
        public static DependencyProperty StatusProperty;

        public static DependencyProperty StatusBackgroundProperty;

        // Dependency Properties for the different Image sources. 
        public static DependencyProperty RedImageSourceProperty;
        public static DependencyProperty AmberImageSourceProperty;
        public static DependencyProperty GreenImageSourceProperty;
        
        #region Public Properties.

        public LightColour Status
        {
            get { return (LightColour)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }

        public Brush StatusBackground
        {
            get { return (Brush)GetValue(StatusBackgroundProperty); }
            set { SetValue(StatusBackgroundProperty, value); }
        }

        public Uri RedSource
        {
            get { return (Uri)GetValue(RedImageSourceProperty); }
            set { SetValue(RedImageSourceProperty, value); }
        }

        public Uri AmberSource
        {
            get { return (Uri)GetValue(AmberImageSourceProperty); }
            set { SetValue(AmberImageSourceProperty, value); }
        }

        public Uri GreenSource
        {
            get { return (Uri)GetValue(GreenImageSourceProperty); }
            set { SetValue(GreenImageSourceProperty, value); }
        }

        #endregion


        #region Private Events

        private static void OnStatusChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            StatusControl statusControl = (StatusControl)sender;

            switch (statusControl.Status)
            {
                case LightColour.Red:
                    statusControl.elpRedShade.Visibility = Visibility.Collapsed;
                    statusControl.elpAmberShade.Visibility = Visibility.Visible;
                    statusControl.elpGreenShade.Visibility = Visibility.Visible;
                    break;
                case LightColour.Amber:
                    statusControl.elpRedShade.Visibility = Visibility.Visible;
                    statusControl.elpAmberShade.Visibility = Visibility.Collapsed;
                    statusControl.elpGreenShade.Visibility = Visibility.Visible;
                    break;
                case LightColour.Green:
                    statusControl.elpRedShade.Visibility = Visibility.Visible;
                    statusControl.elpAmberShade.Visibility = Visibility.Visible;
                    statusControl.elpGreenShade.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private static void OnStatusBackgroundChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //Not Implemented.
        }

        private static void OnRedImageSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            StatusControl statusControl = (StatusControl)sender;

            statusControl.imgRedSource.Visibility = Visibility.Visible;
        }

        private static void OnAmberImageSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            StatusControl statusControl = (StatusControl)sender;

            statusControl.imgAmberSource.Visibility = Visibility.Visible;
        }

        private static void OnGreenImageSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            StatusControl statusControl = (StatusControl)sender;

            statusControl.imgGreenSource.Visibility = Visibility.Visible;
        }

        #endregion

        #region Constructors.

        #region Constructors.

        public StatusControl()
        {
            InitializeComponent();
            //this.Status = LightColour.Amber;
        }

        // Static constructor to register the Dependency Properties.
        static StatusControl()
        {
            StatusProperty = DependencyProperty.Register("Status", typeof(LightColour), typeof(StatusControl),
                                                         new FrameworkPropertyMetadata(new
                                                         PropertyChangedCallback(OnStatusChanged)));

            StatusBackgroundProperty = DependencyProperty.Register("StatusBackground", typeof(Brush), typeof(StatusControl),
                                                                   new FrameworkPropertyMetadata(new
                                                                   PropertyChangedCallback(OnStatusBackgroundChanged)));

            RedImageSourceProperty = DependencyProperty.Register("RedSource", typeof(Uri), typeof(StatusControl),
                                                                 new FrameworkPropertyMetadata(new
                                                                 PropertyChangedCallback(OnRedImageSourceChanged)));

            AmberImageSourceProperty = DependencyProperty.Register("AmberSource", typeof(Uri), typeof(StatusControl),
                                                     new FrameworkPropertyMetadata(new
                                                     PropertyChangedCallback(OnAmberImageSourceChanged)));

            GreenImageSourceProperty = DependencyProperty.Register("GreenSource", typeof(Uri), typeof(StatusControl),
                                                     new FrameworkPropertyMetadata(new
                                                     PropertyChangedCallback(OnGreenImageSourceChanged)));

            
        }

        #endregion

        #endregion
    }
}
