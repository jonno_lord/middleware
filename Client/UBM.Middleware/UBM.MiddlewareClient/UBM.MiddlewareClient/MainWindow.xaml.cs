﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using System.Security.Authentication;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;
using UBM.Logger;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.ViewModel.Pages;
using UBM.MiddlewareClient.ViewModel.Structure;
using UBM.Utilities;
using UBM.MiddlewareClient.Dialogs;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Pages;
using UBM.MiddlewareClient.ViewHelper;
using UBM.MiddlewareClient.ViewModel;
using UBM.NotificationClient;

namespace UBM.MiddlewareClient
{
    #region Enums

    // Enums used in the animation. 
    public enum Animate
    {
        ActionMessage
    }

    public enum Dialog
    {
        Accept,
        Reject,
        VAT,
        Legal,
        About,
        IconKey,
        Close
    }

    // Enum used to work out what environment type we are in. 
    public enum EnvironmentType
    {
        UAT,
        Development,
        Live
    }

    #endregion

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        #region Private Fields

        private readonly MasterViewModel mvm;

        private static readonly Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        private readonly bool filtersPinned;

        private LegalHoldDialog legalHoldDialog;
        private VATDialog vatDialog;
        private RejectNotificationDialog rejectNotificationDialog;
        private AcceptDialog acceptDialog;
        private CustomerDetailsDialog customerDetailsDialog;
        private MessageDialog messageDialog;
        private IconKeyDialog iconKeyDialog;
        private AboutDialog aboutDialog;

        private string environmentText;

        #endregion

        #region Public Properties.

        public bool DialogOpen { get; set; }

        public bool Initialised { get; set; }
        
        public List<PageWrapper> PageWrappers { get; set; }
        public PageWrapper CurrentPage { get; set; }

        #endregion

        #region Private Methods.

        private void OnDialogRequest(DialogEventArgs eventArgs)
        {
            if (eventArgs.RequestedAction == DialogAction.Open)
            {
                PageWrapper pageWrapper = PageWrappers.Find(c => c.ClientPageNumber == mvm.CurrentPage);
                ISupportsCustomerDetails viewModel;
                DialogOpen = true;
                switch (eventArgs.Type)
                {
                    case DialogType.Accept:
                        acceptDialog.DataContext = pageWrapper.ViewModel;
                        acceptDialog.Show();
                        break;
                    case DialogType.RejectNotification:
                        rejectNotificationDialog.Owner = this;
                        rejectNotificationDialog.DataContext = pageWrapper.ViewModel;
                        rejectNotificationDialog.Show();
                        break;
                    case DialogType.Legal:
                        legalHoldDialog.Owner = this;
                        legalHoldDialog.DataContext = pageWrapper.ViewModel;
                        legalHoldDialog.Show();
                        break;
                    case DialogType.Vat:
                        vatDialog.Owner = this;
                        vatDialog.DataContext = pageWrapper.ViewModel;
                        vatDialog.Show();
                        break;
                    case DialogType.CustomerDetails:
                        viewModel = (ISupportsCustomerDetails)pageWrapper.ViewModel;
                        customerDetailsDialog.Owner = this;
                        customerDetailsDialog.DataContext = viewModel.CustomerDetailsViewModel;
                        customerDetailsDialog.MainWindow = this;
                        customerDetailsDialog.ShowDialog(DialogType.CustomerDetails);
                        break;
                    case DialogType.CustomerDetailsDefaultLegal:
                        viewModel = (ISupportsCustomerDetails)pageWrapper.ViewModel;
                        customerDetailsDialog.Owner = this;
                        customerDetailsDialog.DataContext = viewModel.CustomerDetailsViewModel;
                        customerDetailsDialog.MainWindow = this;
                        customerDetailsDialog.ShowDialog(DialogType.CustomerDetailsDefaultLegal);
                        break;
                    case DialogType.CustomerDetailsDefaultEmail:
                        viewModel = (ISupportsCustomerDetails)pageWrapper.ViewModel;
                        customerDetailsDialog.Owner = this;
                        customerDetailsDialog.DataContext = viewModel.CustomerDetailsViewModel;
                        customerDetailsDialog.MainWindow = this;
                        customerDetailsDialog.ShowDialog(DialogType.CustomerDetailsDefaultEmail);
                        break;
                    case DialogType.MessageBox:
                        messageDialog.MainWindow = this;
                        messageDialog.SetUpDialog(eventArgs.MessageBoxArgs);
                        messageDialog.Show();
                        break;
                }
            }
            else
            {
                DialogOpen = false;
                switch (eventArgs.Type)
                {
                    case DialogType.Accept:
                        acceptDialog.Hide();
                        break;
                    case DialogType.RejectNotification:
                        rejectNotificationDialog.Hide();
                        break;
                    case DialogType.Legal:
                        legalHoldDialog.Hide();
                        break;
                    case DialogType.Vat:
                        vatDialog.Hide();
                        break;
                    case DialogType.CustomerDetails:
                        customerDetailsDialog.Hide();
                        break;
                    case DialogType.CustomerDetailsDefaultLegal:
                        customerDetailsDialog.Hide();
                        break;
                    case DialogType.CustomerDetailsDefaultEmail:
                        customerDetailsDialog.Hide();
                        break;
                    case DialogType.MessageBox:
                        messageDialog.Hide();
                        break;
                }
            }
        }

        private void ConfigureScreenOnStartUp()
        {
            try
            {
                EnvironmentType environment = (EnvironmentType)Enum.Parse(typeof(EnvironmentType), ConfigurationManager.AppSettings.Get("CurrentEnvironment"));
                // Determines the environment and then sets the appropriate colours and title bar.
                switch (environment)
                {
                    case EnvironmentType.UAT:
                        // Configures the screen title.
                        bdrDetails.Background = Brushes.PaleGoldenrod;
                        Middlware2011.Title = "Middleware 2011 - UAT Version: " + Assembly.GetExecutingAssembly().GetName().Version;
                        bdrMain.Margin = new Thickness(15, 15, 15, 7.5);
                        environmentText = "UAT";
                        break;
                    case EnvironmentType.Development:
                        // Configures the screen title.
                        bdrDetails.Background = Brushes.PaleTurquoise;
                        Middlware2011.Title = "Middleware 2011 - Development Version: " + Assembly.GetExecutingAssembly().GetName().Version;
                        bdrMain.Margin = new Thickness(15, 7.5, 15, 7.5);
                        environmentText = "Development";
                        break;
                    case EnvironmentType.Live:
                        // Configures the screen title.
                        bdrDetails.Background = Brushes.White;
                        Middlware2011.Title = "Middleware 2011 Version: " + Assembly.GetExecutingAssembly().GetName().Version;
                        bdrMain.Margin = new Thickness(15, 7.5, 15, 7.5);
                        environmentText = "Live";
                        break;
                }
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                Log.Error("Could Parse xmlCurrentEnvironment Key to Enum", exception);
            }
        }

        private void InitializeDateTime()
        {
            DispatcherTimer dtDateTime = new DispatcherTimer
            {
                Interval = new TimeSpan(0, 0, 0, 0, 500)
            };
            dtDateTime.Tick += dtDateTimeTick;
            dtDateTime.Start();
        }

        private void DisplayDate()
        {
            string timeFormat = DateTime.Now.ToString("HH:mm:ss");
            txtDateTime.Text = DateTime.Now.ToString("dddd, dd MMMM yyyy") + ' ' + timeFormat;
        }

        private void AnimateBorder()
        {
            // Checks to see if the pin has been set and then animates the border width. 
            if (!filtersPinned)
            {
                DoubleAnimation widthAnimation = new DoubleAnimation();
                if (bdrFilters.Width == 200)
                {
                    widthAnimation.To = 0;
                    widthAnimation.Duration = TimeSpan.FromSeconds(0.2);
                    bdrFilters.BeginAnimation(WidthProperty, widthAnimation);
                    txtFilterToolTip.Text = "Click To Expand the Filters Border";
                }
                else
                {
                    widthAnimation.To = 200;
                    widthAnimation.Duration = TimeSpan.FromSeconds(0.2);
                    bdrFilters.BeginAnimation(WidthProperty, widthAnimation);
                    txtFilterToolTip.Text = "Click To Collapse the Filters Border";
                }
            }
        }

        private void CreatePageWrappers()
        {
            PageWrappers = new List<PageWrapper>();

            PageWrapper pageWrapper = new PageWrapper
                                            {
                                                ClientPageNumber = ClientPageNumber.CustomerDedupe,
                                                Name = "CustomerDedupe",
                                                Page = new CustomerDedupe(mvm, this),
                                                ViewModel = mvm.DedupeViewModel,
                                                ViewModelType = typeof(DedupeViewModel)
                                            };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.CustomerViewer,
                                  Name = "CustomerViewer",
                                  Page = new CustomerViewer(mvm),
                                  ViewModel = mvm.CustomerViewModel,
                                  ViewModelType = typeof(CustomerViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.OrderViewer,
                                  Name = "OrderViewer",
                                  Page = new OrderViewer(mvm),
                                  ViewModel = mvm.OrderViewModel,
                                  ViewModelType = typeof(OrderViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.EmailHistory,
                                  Name = "EmailHistory",
                                  Page = new EmailHistory(mvm),
                                  ViewModel = mvm.RejectionHistoryViewModel,
                                  ViewModelType = typeof(RejectionHistoryViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.LegalHistory,
                                  Name = "LegalHistory",
                                  Page = new LegalHistory(mvm),
                                  ViewModel = mvm.LegalHistoryViewModel,
                                  ViewModelType = typeof(LegalHistoryViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.InvoicePublication,
                                  Name = "InvoicePublication",
                                  Page = new InvoicePublication(mvm),
                                  ViewModel = mvm.InvoiceViewModel,
                                  ViewModelType = typeof(InvoiceViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.PrintReceipts,
                                  Name = "PrintReceipts",
                                  Page = new PrintReceipts(mvm),
                                  ViewModel = mvm.ReceiptViewModel,
                                  ViewModelType = typeof(ReceiptViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.RunFeeds,
                                  Name = "RunFeeds",
                                  Page = new RunFeeds(mvm),
                                  ViewModel = mvm.FeedRunnerViewModel,
                                  ViewModelType = typeof(FeedRunnerViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.HomePage,
                                  Name = "Home",
                                  Page = new Home(mvm, this),
                                  ViewModel = mvm.HomeViewModel,
                                  ViewModelType = typeof (HomeViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.Admin,
                                  Name = "Admin",
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.RunFeeds,
                                  Name = "RunFeeds",
                                  Page = new RunFeeds(mvm),
                                  ViewModel = mvm.FeedRunnerViewModel,
                                  ViewModelType = typeof(FeedRunnerViewModel)
                              };

            PageWrappers.Add(pageWrapper);

            pageWrapper = new PageWrapper
                              {
                                  ClientPageNumber = ClientPageNumber.Diagnostics,
                                  Name = "Diagnostics",
                                  Page = new Diagnostics(mvm),
                                  ViewModel = mvm.DiagnosticsViewModel,
                                  ViewModelType = typeof(DiagnosticsViewModel)
                              };

            PageWrappers.Add(pageWrapper);
        }

        private void InitialiseViews()
        {
            vwCustomerDedupeControl.DataContext = mvm.DedupeViewModel;
            vwCustomerDedupeControl.DVM = mvm.DedupeViewModel;

            vwSearchFilter.DataContext = mvm.FiltersViewModel;
            //vwSearchFilter.MVM = mvm;
            vwSearchFilter.MainWindow = this;

            vwInformation.DataContext = mvm.InfoBarViewModel;
            vwInformation.ViewModel = mvm.InfoBarViewModel;
        }

        private void SubscribeToViewModelEvents()
        {
            // Events from the MasterViewModel to handle dialog operations.
            mvm.MessageService.OnDialogRequest += OnDialogRequest; 
        }

        private void InitialiseDialogs()
        {
            legalHoldDialog = new LegalHoldDialog();
            vatDialog = new VATDialog();
            rejectNotificationDialog = new RejectNotificationDialog();
            acceptDialog = new AcceptDialog();
            
            customerDetailsDialog = new CustomerDetailsDialog();

            
            messageDialog = new MessageDialog();

            aboutDialog = new AboutDialog(Title, environmentText);
            iconKeyDialog = new IconKeyDialog();
        }

        private static void NotifyDevelopers(Exception exception)
        {
            try
            {
                NotificationServiceClient.Email("", "JPresly; DGrenter", "", "Unhandled Middleware Exception",
                                "Username: " + Environment.UserName + "\n" + exception, Priority.High);
            }
            catch 
            {

            }
        }

        private void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            #if DEBUG
            #else
            ThreadHelper.GetWorkerThread(() => NotifyDevelopers(e.Exception)).Start();
            #endif


            if (mvm != null)
            {
                Log.Error("Command failed ", e.Exception);
                mvm.SetActionMessage(TraceHelper.CommandFailure(e.Exception), MessageType.Error);
                e.Handled = true;
            }
            else
            {
                MessageBox.Show("Unhandled Middleware Error:\n\n" + e.Exception +
                                "\n\n\nPlease report this to the Development Team");
                Log.Error("Unhandled exception", e.Exception);
                e.Handled = false;
            }


        }

        #endregion

        #region Private Window Control Events.

        private void WindowPreviewKeyDown(object sender, KeyEventArgs e)
        {
            // Creates an instance of the double animation object which is used for animation.
            DoubleAnimation heightAnimation = new DoubleAnimation();

            // Checks to see if the alt or system key was pressed.
            if (e.Key == Key.System)
            {
                // Checks to see if the menu is already visible.
                if (mnuMenuBar.Height == 0)
                {
                    // Sets the height to 27 so that the menu will slide down.
                    heightAnimation.To = 25;
                    // Sets the time span of 4 milliseconds.
                    heightAnimation.Duration = TimeSpan.FromSeconds(0.1);

                    mnuMenuBar.BeginAnimation(HeightProperty, heightAnimation);
                }
                else
                {
                    // Sets the height to 0 so that the menu will slide up.
                    heightAnimation.To = 0;
                    heightAnimation.Duration = TimeSpan.FromSeconds(0.1);
                    // Begins the animation setting the property that will be changed and the animation object.
                    mnuMenuBar.BeginAnimation(HeightProperty, heightAnimation);
                }
            }

        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if(mvm != null)
            {
                mvm.RequestClose();
                e.Cancel = true;     
            }
            else
            {
                Application.Current.Shutdown();
            }
        }
        
        private void btnBackClick(object sender, RoutedEventArgs e)
        {
            if (!DialogOpen)
            {
                // Checks to see if the frame can navigate back.
                if (frmContent.NavigationService.CanGoBack)
                {
                    // Navigates back in the content frame.
                    frmContent.GoBack();

                    foreach (JournalEntry journal in frmContent.BackStack)
                    {
                        

                        PageWrapper pageWrapper = PageWrappers.Find(c => c.Page.Title == journal.Name);
                        mvm.SetPage(pageWrapper.ClientPageNumber);
                        break;
                    }
                }
            }
        }
        
        private void btnForwardClick(object sender, RoutedEventArgs e)
        {
            if (!DialogOpen)
            {
                // Checks to see if the frame can navigate forward.
                if (frmContent.NavigationService.CanGoForward)
                {
                    // Navigates forward to the next item that has previously been viewed in the content frame.
                    frmContent.GoForward();

                    foreach (JournalEntry journal in frmContent.ForwardStack)
                    {
                        PageWrapper pageWrapper = PageWrappers.Find(c => c.Page.Title == journal.Name);
                        mvm.SetPage(pageWrapper.ClientPageNumber);
                        
                        break;
                    }
                }
            }
        }

        private void btnRefreshClick(object sender, RoutedEventArgs e)
        {
            frmContent.Refresh();
        }

        private void btnHomeClick(object sender, RoutedEventArgs e)
        {
            if (!DialogOpen)
            {
                NavigateToPage(ClientPageNumber.HomePage);
            }
        }

        private void btnFiltersClick(object sender, RoutedEventArgs e)
        {
            if (!DialogOpen)
                AnimateBorder();
            else
                mvm.SetActionMessage("Close any open dialogs first", MessageType.Bad);
        }

        private void MenuItemClick(object sender, RoutedEventArgs e)
        {
            if (!DialogOpen)
            {
                MenuItem mnuClicked = (MenuItem) sender;

                if (mnuClicked.Name == "mnuAbout")
                {
                    aboutDialog.Show();
                }
                else if (mnuClicked.Name == "mnuIconKey")
                {
                    iconKeyDialog.Show();
                }
                else if (mnuClicked.Name == "mnuClose")
                {
                    Close();
                }
                else
                {
                    if(mnuClicked.Tag != null)
                        NavigateToPage((ClientPageNumber) mnuClicked.Tag);
                }
            }
        }

        private void dtDateTimeTick(object sender, EventArgs e)
        {
            DisplayDate();
        }

        //private void bdrDetailsMouse(object sender, MouseEventArgs e)
        //{
        //    AnimateContent(Animate.ActionMessage);
        //}

        #endregion

        #region Constructors.
        
        public MainWindow()
        {


            Application.Current.DispatcherUnhandledException += AppDispatcherUnhandledException;

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                SplashScreen splashScreen = new SplashScreen("Splash.png");
                splashScreen.Show(false);

                try
                {

                    Stopwatch s = new Stopwatch();
                    s.Start();

                   //if(!MasterViewModel.AuthenticateUser()) 
                   //    throw new AuthenticationException(Environment.UserName + " did not authenticate");

                    mvm = new MasterViewModel();


                    splashScreen.Close(TimeSpan.Zero);
                    // Sets the dialog property to false to state that no dialogs are open.
                    DialogOpen = false;

                    // Sets the MainWindow DataContext to the MasterViewModel.
                    DataContext = mvm;
                    filtersPinned = false;

                    Initialised = false;

                    CreatePageWrappers();

                    InitializeComponent();

                    NavigateToPage(ClientPageNumber.HomePage);

                    ConfigureScreenOnStartUp();

                    InitializeDateTime();

                    InitialiseViews();

                    SubscribeToViewModelEvents();

                    InitialiseDialogs();

                    mutex.ReleaseMutex();

                    s.Stop();
                    string time = s.ElapsedMilliseconds.ToString();

                    //mvm.SetActionMessage("Loaded time: " + time + "ms", MessageType.Info);

                }
                catch(AuthenticationException authenticationException)
                {
                    MessageBox.Show(authenticationException.Message);
                    Application.Current.Shutdown();
                }
                catch (Exception exception)
                {
                    Log.Error("Middleware Client Failed To Start", exception);
                    MessageBox.Show("Middleware Client Failed To Start: " + exception);
                    Application.Current.Shutdown();
                }
            }
            else
            {
                MessageBox.Show("Middleware 2011 is already running");
                Application.Current.Shutdown();
            }
        }

        #endregion

        #region Public Methods.

        ///// <summary>
        ///// Is called to Animate different items on the page, currently
        ///// only used to animate the ActionMessage Border on mouse hover.
        ///// </summary>
        ///// <param name="animation">
        ///// Takes a parameter of type Animation Enum, that states
        ///// what animation to carry out. 
        ///// </param>
        //public void AnimateContent(Animate animation)
        //{
        //    if (!DialogOpen)
        //    {
        //        switch (animation)
        //        {
        //            case Animate.ActionMessage:
        //                if (bdrDetails.Height == 70)
        //                {
        //                    bdrDetails.Height = 30;
        //                    vwInformation.Height = 25;
        //                }
        //                else
        //                {
        //                    bdrDetails.Height = 70;
        //                    vwInformation.Height = 65;
        //                }
        //                break;
        //        }
        //    }
        //}

        /// <summary>
        /// Navigate to Page that takes a string and gets the ClientPageNumebr enum from the string.
        /// </summary>
        /// <param name="name">String of page to navigate to.</param>
        public void NavigateToPage(string name)
        {
            try
            {
                NavigateToPage(PageWrappers.Find(c => c.Name == name).ClientPageNumber);
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                Log.Error("Problem Navigating to Page" + name, exception);
                mvm.SetActionMessage("Problem navigating to the " + name + " page.", MessageType.Error);
                NavigateToPage(ClientPageNumber.HomePage);
            }
        }

        /// <summary>
        /// Navigates to the correct Page depending on what ClientPageNumber Enum that is passed to it.
        /// </summary>
        /// <param name="clientPageNumber">Enum of what page to navigate to.</param>
        public void NavigateToPage(ClientPageNumber clientPageNumber)
        {
            try
            {
                if (clientPageNumber == ClientPageNumber.Admin)
                {
                    string navigateUri = ConfigurationManager.AppSettings.Get("AdminScreens");
                    // Opens the web address in the default browser. 
                    Process.Start(new ProcessStartInfo(navigateUri));
                }
                else
                {
                    PageWrapper pageWrapper = PageWrappers.Find(c => c.ClientPageNumber == clientPageNumber);
                    mvm.SetPage(pageWrapper.ClientPageNumber);
                    frmContent.Navigate(pageWrapper.Page);
                }
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                Log.Error("Problem Navigating to Page" + clientPageNumber, exception);
                mvm.SetActionMessage("Problem navigating to the page.", MessageType.Error);
                PageWrapper pageWrapper = PageWrappers.Find(c => c.ClientPageNumber == ClientPageNumber.HomePage);
                mvm.SetPage(pageWrapper.ClientPageNumber);
                frmContent.Navigate(pageWrapper.Page);
            }
        }

        /// <summary>
        /// Disposes any events from the MasterViewModel.
        /// </summary>
        public void Dispose()
        {
            mvm.MessageService.OnDialogRequest -= OnDialogRequest; 
            Application.Current.DispatcherUnhandledException -= AppDispatcherUnhandledException;
        }

        #endregion
    }
}
