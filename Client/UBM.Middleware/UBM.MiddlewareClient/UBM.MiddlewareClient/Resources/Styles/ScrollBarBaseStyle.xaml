﻿<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml">

    <!-- Base style for the ScrollBar control that are used with in ScrollViewers.
         It is the new default style and overrides the original ScrollBar style. -->
    
    <!-- Style for the RepeatButton that is at either end of the ScrollBar. -->
    <Style x:Key="ScrollBarLineButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="True" />
        <Setter Property="Focusable" Value="False" />
        <Setter Property="Cursor" Value="Hand" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <Border Name="RepeatBorder"
                            CornerRadius="15"
                            Background="{StaticResource StandardBackgroundGradient}"
                            BorderBrush="{StaticResource StandardBorderBrush}"
                            BorderThickness="1">
                        <Path HorizontalAlignment="Center"
                              VerticalAlignment="Center"
                              Fill="{StaticResource GlyphBrush}"
                              Data="{Binding Path=Content, RelativeSource={RelativeSource TemplatedParent}}" />
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="RepeatBorder" Property="Background" Value="{StaticResource StandardIsMouseOverBrush}" />
                        </Trigger>
                        <Trigger Property="IsPressed" Value="True">
                            <Setter TargetName="RepeatBorder" Property="RenderTransform" >
                                <Setter.Value>
                                    <TranslateTransform Y="1.0" X="1.0" />
                                </Setter.Value>
                            </Setter>
                            <Setter TargetName="RepeatBorder" Property="Effect">
                                <Setter.Value>
                                    <DropShadowEffect ShadowDepth="0"  />
                                </Setter.Value>
                            </Setter>
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="False">
                            <Setter TargetName="RepeatBorder" Property="Background" Value="{StaticResource DisabledBackgroundGradient}" />
                            <Setter TargetName="RepeatBorder" Property="BorderBrush" Value="{StaticResource DisabledBorderBrush}" />
                            <Setter Property="Foreground" Value="{StaticResource DisabledForegroundBrush}"/>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- Style for the page button which is either side of the Thumb on the ScrollBar. -->
    <Style x:Key="ScrollBarPageButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="OverridesDefaultStyle" Value="True"/>
        <Setter Property="IsTabStop" Value="False"/>
        <Setter Property="Focusable" Value="False"/>
        <Setter Property="Cursor" Value="Hand" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <Border Background="Transparent"/>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- Style for the Thumb of the ScrollBar. -->
    <Style x:Key="ScrollBarThumb" TargetType="{x:Type Thumb}">
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="OverridesDefaultStyle" Value="True"/>
        <Setter Property="IsTabStop" Value="False"/>
        <Setter Property="Focusable" Value="False"/>
        <Setter Property="Cursor" Value="Hand" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type Thumb}">
                    <Border CornerRadius="2"
                            Background="{TemplateBinding Background}"
                            BorderBrush="{TemplateBinding BorderBrush}"
                            BorderThickness="1"/>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- ControlTemplate for a VerticalScrollBar. -->
    <ControlTemplate x:Key="VerticalScrollBar" TargetType="{x:Type ScrollBar}">
        <Grid Background="Transparent">
            <Grid.RowDefinitions>
                <RowDefinition MaxHeight="18" />
                <RowDefinition Height="0.00001*" />
                <RowDefinition MaxHeight="18" />
            </Grid.RowDefinitions>

            <Border Grid.RowSpan="3"
                    Margin="0,9,0,9"
                    Background="{StaticResource StandardVerticalBackgroundGradient}"/>

            <RepeatButton Grid.Row="0"
                          Style="{StaticResource ScrollBarLineButton}"
                          Height="18"
                          Command="ScrollBar.LineUpCommand"
                          Content="M 0 4 L 8 4 L 4 0 Z" />

            <Track Name="PART_Track"
                   Grid.Row="1"
                   IsDirectionReversed="True">
                <Track.DecreaseRepeatButton>
                    <RepeatButton Style="{StaticResource ScrollBarPageButton}"
                                  Command="ScrollBar.PageUpCommand" />
                </Track.DecreaseRepeatButton>
                <Track.Thumb>
                    <Thumb Style="{StaticResource ScrollBarThumb}"
                           Margin="1,0,1,0"
                           Background="{StaticResource StandardVerticalBackgroundGradient}"
                           BorderBrush="{StaticResource StandardBorderBrush}"/>
                </Track.Thumb>
                <Track.IncreaseRepeatButton>
                    <RepeatButton Style="{StaticResource ScrollBarPageButton}"
                                  Command="ScrollBar.PageDownCommand"/>
                </Track.IncreaseRepeatButton>
            </Track>

            <RepeatButton Grid.Row="3"
                          Style="{StaticResource ScrollBarLineButton}"
                          Height="18"
                          Command="ScrollBar.LineDownCommand"
                          Content="M 0 0 L 4 4 L 8 0 Z"/>
        </Grid>
    </ControlTemplate>

    <!-- ControlTemplate for a HorizontalScrollBar. -->
    <ControlTemplate x:Key="HorizontalScrollBar" TargetType="{x:Type ScrollBar}">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition MaxWidth="18" />
                <ColumnDefinition Width="0.00001*" />
                <ColumnDefinition MaxWidth="18" />
            </Grid.ColumnDefinitions>

            <Border Grid.ColumnSpan="3"
                    CornerRadius="15"
                    Background="{StaticResource StandardBackgroundGradient}"/>

            <RepeatButton Grid.Column="0"
                          Style="{StaticResource ScrollBarLineButton}"
                          Height="18"
                          Command="ScrollBar.LineLeftCommand"
                          Content="M 4 0 L 4 8 L 0 4 Z" />

            <Track Name="PART_Track"
                   Grid.Column="1"
                   IsDirectionReversed="False">
                <Track.DecreaseRepeatButton>
                    <RepeatButton Style="{StaticResource ScrollBarPageButton}"
                                  Command="ScrollBar.PageLeftCommand" />
                </Track.DecreaseRepeatButton>
                <Track.Thumb>
                    <Thumb Style="{StaticResource ScrollBarThumb}"
                           Margin="0,1,0,1"
                           Background="{StaticResource StandardBackgroundGradient}"
                           BorderBrush="{StaticResource StandardBorderBrush}"/>
                </Track.Thumb>
                <Track.IncreaseRepeatButton>
                    <RepeatButton Style="{StaticResource ScrollBarPageButton}"
                                  Command="ScrollBar.PageRightCommand"/>
                </Track.IncreaseRepeatButton>
            </Track>

            <RepeatButton Grid.Column="3"
                          Style="{StaticResource ScrollBarLineButton}"
                          Height="18"
                          Command="ScrollBar.PageRightCommand"
                          Content="M 0 0 L 4 4 L 0 8 Z"/>
        </Grid>
    </ControlTemplate>

    <!-- ScrollBar style that incorporates the two ScrollBar ControlTemplate's
         depending on the orientation value that is set. -->
    <Style x:Key="{x:Type ScrollBar}" TargetType="{x:Type ScrollBar}">
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="OverridesDefaultStyle" Value="True"/>
        <Style.Triggers>
            <Trigger Property="Orientation" Value="Vertical">
                <Setter Property="Width" Value="18"/>
                <Setter Property="Height" Value="Auto"/>
                <Setter Property="Template" Value="{StaticResource VerticalScrollBar}"/>
            </Trigger>
            <Trigger Property="Orientation" Value="Horizontal">
                <Setter Property="Width" Value="Auto"/>
                <Setter Property="Height" Value="18"/>
                <Setter Property="Template" Value="{StaticResource HorizontalScrollBar}"/>
            </Trigger>
        </Style.Triggers>
    </Style>
</ResourceDictionary>