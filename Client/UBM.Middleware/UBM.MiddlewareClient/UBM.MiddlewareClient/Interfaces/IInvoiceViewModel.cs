﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Interfaces
{
    interface IInvoiceViewModel
    {
        ICommand AddInvoiceCommand { get; }
        ICommand RemoveInvoiceCommand { get; }
        ICommand ClearInvoicesCommand { get; }
        ICommand IgnoreWarningsCommand { get; }
        ICommand ExtractionCommand { get; }
        ICommand RunAllExtractionCommand { get; }
        ICommand SelectedExtractionCommand { get; }
    }
}
