﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Domain.Customer;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface ICustomerDetailsViewModel
    {
        Customer SelectedCustomer { get; set; }
        ICommand CloseCustomerDetailsCommand { get; }
        ICommand PrepareResendEmailCommand { get; }
        ICommand MergeCustomerCommand { get; }


        void ShowFullBody(object row);
        void ShowTruncatedBody(object row);

        event EventHandler CustomerDetailsClosed;
        event EventHandler ResendEmail;
        event EventHandler MergeDedupeCustomer;

    }
}
