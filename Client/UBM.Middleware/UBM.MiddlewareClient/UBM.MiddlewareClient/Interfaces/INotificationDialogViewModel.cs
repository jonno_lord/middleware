﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using UBM.MiddlewareClient.Model.Domain.Customer;
using UBM.MiddlewareClient.ViewModel;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface INotificationDialogViewModel
    {
        ICommand AcceptNotificationDialogCommand { get; }
        ICommand CancelNotificationDialogCommand { get; }
        ICommand NextNotificationCommand { get; }
        ICommand PreviousNotificationCommand { get; }
        ICommand CCAddCommand { get; }
        ICommand CCRemoveCommand { get; }
        ICommand CCCheckCommand { get; }
        ICommand RecipientCheckCommand { get; }

        event EventHandler AcceptNotification;
        event EventHandler CancelNotification;

        void PrepareResend(Customer customer, Email email);
        void AbortResend();
        void AddCustomer(Customer customer, bool newEmail);
        void ClearCustomers();
        Email Resend();
        List<Customer> GetNotificationCustomers();
        void SelectNotificationCustomer(Customer customer);

        bool IsEmpty { get; }
        bool AtLastEmail { get; }
        bool ResendPrepared { get; }
        //bool CheckAllRecipientsAreValid();

    }
}
