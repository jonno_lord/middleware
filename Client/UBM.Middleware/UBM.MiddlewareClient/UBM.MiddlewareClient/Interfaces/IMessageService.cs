﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Interfaces
{
    public delegate void SendUserMessageEventHandler(string message, MessageType messageType);
    public delegate void DialogRequestEventHandler(DialogEventArgs eventArgs);

    public interface IMessageService
    {
        event DialogRequestEventHandler OnDialogRequest;
        event SendUserMessageEventHandler OnSendUserMessage;

        void SendUserMessage(string message, MessageType messageType);
        DialogResult OpenMessageBoxWithResult(MessageBoxArgs messageBoxArgs);
 

        void OpenDialog(DialogType dialogType);
        void CloseDialog(DialogType dialogType);
        void CloseAnyOpenDialogs();
        void CloseAnyOpenDialogsExcept(DialogType? exception);
        bool AnyDialogsOpen();
        bool AnyDialogsOpenExcept(DialogType? exception);
        bool IsDialogOpen(DialogType dialogType);
    }
}
