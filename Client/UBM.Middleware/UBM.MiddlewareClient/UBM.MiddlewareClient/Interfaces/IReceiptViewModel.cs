﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Interfaces
{
    public interface IReceiptViewModel
    {
        ICommand ProcessSelectedBatchesCommand { get; }
        ICommand PrintSelectedReceiptsCommand { get; }
        ICommand ExtractionCommand { get; }
    }
}
