﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for Diagnostics.xaml
    /// </summary>
    public partial class Diagnostics : Page
    {
        public Diagnostics(MasterViewModel mvm)
        {
            InitializeComponent();
            vwDiagnostics.DataContext = mvm.DiagnosticsViewModel;
        }
    }
}
