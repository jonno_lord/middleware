﻿using System.Windows.Controls;
using System.Windows.Navigation;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for CustomerDedupe.xaml
    /// </summary>
    public partial class CustomerDedupe : Page
    {
        #region Constructors.

        /// <summary>
        /// Constructor that takes two parameters so that this Page can set the 
        /// DataContexts of the View and has one instance of the DedupeViewModel
        /// throughout the applicaiton. 
        /// </summary>
        /// <param name="mvm">
        /// Instance of MasterViewModel that is passed to this Page,
        /// so that all pages have the same instance of the MasterViewModel.
        /// </param>
        /// <param name="mainWindow">
        /// Instance of MainWindow so that this Page can call public methods on 
        /// the MainWindow to carry out animation and so on.
        /// </param>
        public CustomerDedupe(MasterViewModel mvm, MainWindow mainWindow)
        {
            InitializeComponent();

            // Sets the DataContext of the view to the DedupeViewModel so that the Bindings can work.
            vwCustomerDedupe.DataContext = mvm;
            vwCustomerDedupe.MainWindow = mainWindow;
        }

        #endregion
    }
}
