﻿using System.Windows.Controls;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for InvoicePublication.xaml
    /// </summary>
    public partial class InvoicePublication : Page
    {
        #region Constructors.

        /// <summary>
        /// Constructor that takes InvoiceViewModel as a parameter to
        /// set the DataContext of the view.
        /// </summary>
        /// <param name="mvm">
        /// Instance of the MasterViewModel that is used in this screen.
        /// </param>
        public InvoicePublication(MasterViewModel mvm)
        {
            InitializeComponent();

            // Sets the DataContext to the InvoiceViewModel.
            vwPublication.DataContext = mvm.InvoiceViewModel;
            vwPublication.IVM = mvm.InvoiceViewModel;
        }

        #endregion
    }
}
