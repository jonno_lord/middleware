﻿using System.Windows.Controls;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for PrintReceipts.xaml
    /// </summary>
    public partial class PrintReceipts : Page
    {
        #region Constructors.

        /// <summary>
        /// Constructor to create and instance of this page and set the DataContext of the View
        /// to the correct ViewModel.
        /// </summary>
        /// <param name="mvm">MasterViewModel that is passed to this Page.</param>
        public PrintReceipts(MasterViewModel mvm)
        {
            InitializeComponent();
            vwPrintReceipts.DataContext = mvm.ReceiptViewModel;
            vwPrintReceipts.SetViewModel(mvm.ReceiptViewModel);
        }

        #endregion
    }
}
