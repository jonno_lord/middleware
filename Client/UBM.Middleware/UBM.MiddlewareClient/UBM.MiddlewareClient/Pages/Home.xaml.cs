﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        #region Private Fields

        private readonly MasterViewModel mvm;

        private readonly MainWindow mainWindow;

        #endregion

        #region Private Methods

        private void CreateButton(ClientPageNumber pageNumber)
        {
            // Creates the Button properties for the Most Used Buttons. 
            Button btn = new Button
                             {
                                 Name = pageNumber.ToString(),
                                 Width = 75,
                                 Height = 75,
                                 Margin = new Thickness(10),
                             };
            btn.Click += ButtonClick;

            Image imgIcon = new Image();
            ImageSource source;
            ToolTip tool = new ToolTip();
            Binding enabledBinding = new Binding
                                         {
                                             Mode = BindingMode.TwoWay, 
                                             Path = new PropertyPath("IsEnabled")
                                         };

            // Sets the correct Image and ToolTip for each of the Buttons. 
            switch (pageNumber)
            {
                case ClientPageNumber.CustomerDedupe:
                    source = new BitmapImage(new Uri("../Resources/Images/CustomerDedupe.gif", UriKind.Relative));
                    tool = ttpCustomerDedupe;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnCustomerDedupe;
                    btn.Style = btnCustomerDedupe.Style;
                    break;
                case ClientPageNumber.CustomerViewer:
                    source = new BitmapImage(new Uri("../Resources/Images/CustomerViewer.png", UriKind.Relative));
                    imgIcon.Source = source;
                    tool = ttpCustomerViewer;
                    enabledBinding.Source = btnCustomerViewer;
                    btn.Style = btnCustomerViewer.Style;
                    break;
                case ClientPageNumber.OrderViewer:
                    source = new BitmapImage(new Uri("../Resources/Images/OrderViewer.png", UriKind.Relative));
                    imgIcon.Source = source;
                    tool = ttpOrderViewer;
                    enabledBinding.Source = btnOrderViewer;
                    btn.Style = btnOrderViewer.Style;
                    break;
                case ClientPageNumber.EmailHistory:
                    source = new BitmapImage(new Uri("../Resources/Images/EmailHistory.png", UriKind.Relative));
                    imgIcon.Source = source;
                    tool = ttpEmailHistory;
                    enabledBinding.Source = btnEmailHistory;
                    btn.Style = btnEmailHistory.Style;
                    break;
                case ClientPageNumber.LegalHistory:
                    source = new BitmapImage(new Uri("../Resources/Images/LegalHistory.png", UriKind.Relative));
                    tool = ttpLegalHistory;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnLegalHistory;
                    btn.Style = btnLegalHistory.Style;
                    break;
                case ClientPageNumber.InvoicePublication:
                    source = new BitmapImage(new Uri("../Resources/Images/Publication.png", UriKind.Relative));
                    tool = ttpInvoicePublication;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnInvoicePublication;
                    btn.Style = btnInvoicePublication.Style;
                    break;
                case ClientPageNumber.PrintReceipts:
                    source = new BitmapImage(new Uri("../Resources/Images/PrintReceipt.png", UriKind.Relative));
                    tool = ttpPrintReceipts;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnPrintReceipts;
                    btn.Style = btnPrintReceipts.Style;
                    break;
                case ClientPageNumber.Admin:
                    source = new BitmapImage(new Uri("../Resources/Images/AdminScreen.png", UriKind.Relative));
                    tool = ttpAdmin;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnAdmin;
                    btn.Style = btnAdmin.Style;
                    break;
                case ClientPageNumber.RunFeeds:
                    source = new BitmapImage(new Uri("../Resources/Images/RunFeeds.png", UriKind.Relative));
                    tool = ttpRunFeeds;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnRunFeeds;
                    btn.Style = btnRunFeeds.Style;
                    break;
                case ClientPageNumber.Diagnostics:
                    source = new BitmapImage(new Uri("../Resources/Images/DiagnosticsScreen.png", UriKind.Relative));
                    tool = ttpDiagnostics;
                    imgIcon.Source = source;
                    enabledBinding.Source = btnDiagnostics;
                    btn.Style = btnDiagnostics.Style;
                    break;  
            }

            // Adds the Image and ToolTip to the Button.
            btn.Content = imgIcon;
            btn.ToolTip = tool;
            btn.Tag = pageNumber;
            btn.SetBinding(IsEnabledProperty, enabledBinding);

            // Adds the Butto nto the Most Used WrapPanel. 
            wrpMostUsed.Children.Add(btn);

        }

        #endregion

        #region Private Control Events

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Button btnClicked = (Button)sender;
            mainWindow.NavigateToPage((ClientPageNumber)btnClicked.Tag);
        }

        #endregion

        #region Constructors.

        /// <summary>
        /// Constructor thats takes two parameters to set up the home Page
        /// and other pages that connect to it. 
        /// </summary>
        /// <param name="mvm">
        /// Instance of the MasterViewModel that is used on this Page,
        /// and used to pass the other ViewModels to the correct Page.
        /// </param>
        /// <param name="mainWindow">
        /// Instance of MainWindow so that the Home Page can pass this to
        /// the other Pages so that method calls in the MainWindow can be called.
        /// </param>
        public Home(MasterViewModel mvm, MainWindow mainWindow)
        {
            InitializeComponent();
            this.mvm = mvm;
            this.mainWindow = mainWindow;
            DataContext = mvm;

            // Sets the Page in the MasterViewModel to the Home Page. 
            this.mvm.SetPage(ClientPageNumber.HomePage);

            if (!mainWindow.Initialised)
            {
                List<int> topPages = mvm.GetTopFivePages();
                
                foreach (int i in topPages)
                {
                    if (i > 0)
                    {
                        CreateButton((ClientPageNumber)Enum.Parse(typeof(ClientPageNumber), i.ToString(), true));
                    }
                }

                mainWindow.Initialised = true;
            }
        }

        #endregion
    }
}
