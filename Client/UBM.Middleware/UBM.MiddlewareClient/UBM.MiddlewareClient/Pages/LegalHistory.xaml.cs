﻿using System.Windows.Controls;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.ViewModel;
using UBM.MiddlewareClient.ViewModel.Structure;

namespace UBM.MiddlewareClient.Pages
{
    /// <summary>
    /// Interaction logic for LegalHolds.xaml
    /// </summary>
    public partial class LegalHistory : Page
    {
        #region Constructors.

        /// <summary>
        /// LegalHold Constructor that takes an instance of the HistoryViewModel
        /// to set up the View datacontext. 
        /// </summary>
        /// <param name="mvm">
        /// Instace of the MasterViewModel used to setup the DataContext so that
        /// the binding on the View all work.
        /// </param>
        public LegalHistory(MasterViewModel mvm)
        {
            InitializeComponent();
            vwLegalHold.DataContext = mvm.LegalHistoryViewModel;
            vwLegalHold.LHVM = mvm.LegalHistoryViewModel;
        }

        #endregion
    }
}
