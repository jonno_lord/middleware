﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace UBM.MiddlewareClient
{
    /// <summary>
    /// Simple class that implements the Border control to create a border that clips 
    /// its child elements to the bounds of the border including on the rounded corners.
    /// </summary>
    public class ClippingBorder : Border
    {
        // Private Properties.
        private readonly RectangleGeometry clipRectangle = new RectangleGeometry();
        private object oldClip;

        // Public Properties.
        public override UIElement Child
        {
            get
            {
                return base.Child;
            }
            set
            {
                if (Child != value)
                {
                    if (Child != null)
                    {
                        // Restores original clipping.
                        Child.SetValue(ClipProperty, oldClip);
                    }

                    if (value != null)
                    {
                        oldClip = value.ReadLocalValue(ClipProperty);
                    }
                    else
                    {
                        oldClip = null;
                    }

                    base.Child = value;
                }
            }
        }

        // Private Methods - None.

        // Protected Methods.
        #region Clipping Border Protected Methods

        protected override void OnRender(DrawingContext dc)
        {
            OnApplyChildClip();
            base.OnRender(dc);
        }

        protected virtual void OnApplyChildClip()
        {
            UIElement child = Child;

            if (child != null)
            {
                clipRectangle.RadiusX = clipRectangle.RadiusY = Math.Max(0.0, CornerRadius.TopLeft - (BorderThickness.Left * 0.5));
                Rect rect = new Rect(RenderSize);
                rect.Height -= BorderThickness.Top + BorderThickness.Bottom;
                rect.Width -= BorderThickness.Left + BorderThickness.Right;
                clipRectangle.Rect = rect;
                child.Clip = clipRectangle;
            }
        }

        #endregion
    }
}
