﻿using System.Windows;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for RejectNotificationDialog.xaml
    /// </summary>
    public partial class RejectNotificationDialog : DialogBase
    {
        #region Private Window Control Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnMinimiseClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        #endregion

        #region Constructors. 

        public RejectNotificationDialog()
        {
            InitializeComponent();
        }

        #endregion
    }
}
