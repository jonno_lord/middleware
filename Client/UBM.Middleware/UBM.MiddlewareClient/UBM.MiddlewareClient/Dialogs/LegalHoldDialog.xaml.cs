﻿using System.Windows;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for LegalHoldDialog.xaml
    /// </summary>
    public partial class LegalHoldDialog : DialogBase
    {
        #region Private Window Control Event.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnMinimiseClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        #endregion

        #region Constructors.

        public LegalHoldDialog()
        {
            InitializeComponent();
        }

        #endregion
    }
}
