﻿using System.Windows;
using System.Windows.Input;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for VATDialog.xaml
    /// </summary>
    public partial class VATDialog : DialogBase
    {
        #region Private Window Control Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnMinimiseClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        #endregion

        #region Constructors.

        public VATDialog()
        {
            InitializeComponent();
        }

        #endregion
    }
}
