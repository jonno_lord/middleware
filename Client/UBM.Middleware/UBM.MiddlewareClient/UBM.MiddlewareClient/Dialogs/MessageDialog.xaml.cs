﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;

namespace UBM.MiddlewareClient.Dialogs
{
    /// <summary>
    /// Interaction logic for MessageDialog.xaml
    /// </summary>
    public partial class MessageDialog : DialogBase
    {
        private MessageBoxArgs messageBoxArgs;

        #region Public Properties.

        public MainWindow MainWindow { get; set; }

        #endregion

        #region Private Window Control Events.

        private void DialogMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
        
        private void btnOkClick(object sender, RoutedEventArgs e)
        {            
            if(messageBoxArgs != null && messageBoxArgs.Yes != null)
                messageBoxArgs.Yes();
            DialogResult = true;
        }

        private void btnCancelClick(object sender, RoutedEventArgs e)
        {
            if(messageBoxArgs != null && messageBoxArgs.No != null)
                messageBoxArgs.No();
            DialogResult = false;
        }
        
        #endregion

        #region Constructors.

        public MessageDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods.

        /// <summary>
        /// Sets up the customiseable sections of the dialog. Like thie title and the message that is displayed.
        /// </summary>
        /// <param name="messageBoxArgs">Arguments for the Message box, Title, Type, Message, etc</param>
        public void SetUpDialog(MessageBoxArgs messageBoxArgs)
        {
            this.messageBoxArgs = messageBoxArgs;
            txtTitle.Text = messageBoxArgs.Title;
            txtMessage.Text = messageBoxArgs.Message;
            this.Title = messageBoxArgs.Title;

            imgIcon.Visibility = Visibility.Visible;

            ImageSource source;

            switch (messageBoxArgs.MessageType)
            {
                case MessageType.None:
                    source = null;
                    imgIcon.Visibility = Visibility.Collapsed;
                    break;
                case MessageType.Good:
                    source = new BitmapImage(new Uri("../Resources/Images/Good.png", UriKind.Relative));
                    break;
                case MessageType.Bad:
                    source = new BitmapImage(new Uri("../Resources/Images/Bad.png", UriKind.Relative));
                    break;
                case MessageType.Error:
                    source = new BitmapImage(new Uri("../Resources/Images/Error.png", UriKind.Relative));
                    break;
                case MessageType.Info:
                    source = new BitmapImage(new Uri("../Resources/Images/Info.png", UriKind.Relative));
                    break;
                default:
                    source = null;
                    imgIcon.Visibility = Visibility.Collapsed;
                    break;
            }

            imgIcon.Source = source;
        }

        #endregion
    }
}
