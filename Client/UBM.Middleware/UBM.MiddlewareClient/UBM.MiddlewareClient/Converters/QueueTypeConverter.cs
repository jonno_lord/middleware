﻿using System;
using System.Globalization;
using System.Windows.Data;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(QueueType), typeof(string))]
    internal class QueueTypeConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((QueueType)value)
            {
                case QueueType.Waiting:
                    return "Waiting";
                case QueueType.OnHold:
                    return "Legal Hold";
                case QueueType.PreviouslyRejected:
                    return "Previously Rejected";
                case QueueType.Accepted:
                    return "Accepted";
                case QueueType.Rejected:
                    return "Rejected";
                case QueueType.Unassigned:
                    return "Unassigned";
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}