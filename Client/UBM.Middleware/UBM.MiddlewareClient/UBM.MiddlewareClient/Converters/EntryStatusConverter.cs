﻿using System;
using System.Globalization;
using System.Windows.Data;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(EntryStatus), typeof(string))]
    internal class EntryStatusConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((EntryStatus)value)
            {
                case EntryStatus.AcceptedInScrutiny:
                    return "Accepted In Scrutiny";
                case EntryStatus.JDEToMiddleware:
                    return "JDE To Middleware";
                case EntryStatus.MiddlewareToJDE:
                    return "Middleware To JDE";
                case EntryStatus.MiddlewareToMiddlewareIn:
                    return "Middleware To Middleware In";
                case EntryStatus.MiddlewareToMiddlewareOut:
                    return "Middleware To Middleware Out";
                case EntryStatus.MiddlewareToSource:
                    return "Middleware To Source";
                case EntryStatus.RejectedFromJDE:
                    return "Rejected From JDE";
                case EntryStatus.RejectedInScrutiny:
                    return "Rejected In Scrutiny";
                case EntryStatus.SourceToMiddleware:
                    return "Source To Middleware";
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}