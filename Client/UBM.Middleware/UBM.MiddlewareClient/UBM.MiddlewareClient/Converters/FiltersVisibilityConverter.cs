﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(ClientPageNumber), typeof(Visibility))]
    internal class FiltersVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((ClientPageNumber)value == ClientPageNumber.CustomerViewer || (ClientPageNumber)value == ClientPageNumber.CustomerDedupe ||
                (ClientPageNumber)value == ClientPageNumber.OrderViewer || (ClientPageNumber)value == ClientPageNumber.LegalHistory ||
                (ClientPageNumber)value == ClientPageNumber.EmailHistory || (ClientPageNumber)value == ClientPageNumber.PrintReceipts)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}