﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(string), typeof(Visibility))]
    internal class JDEVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string)value == "JDE" ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}