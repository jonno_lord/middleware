﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UBM.MiddlewareClient.Converters
{
    /// <summary>
    /// 
    /// </summary>
    internal class CustomerSelectedConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)values[0];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
                                    CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}