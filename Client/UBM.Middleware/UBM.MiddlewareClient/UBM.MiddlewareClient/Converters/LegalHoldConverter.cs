﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(bool), typeof(string))]
    internal class LegalHoldConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((bool?)value)
            {
                case true:
                    return "On";
                case false:
                    return "Off";
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}