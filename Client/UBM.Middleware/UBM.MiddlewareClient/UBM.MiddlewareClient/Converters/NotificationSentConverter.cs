﻿using System;
using System.Globalization;
using System.Windows.Data;
using UBM.MiddlewareClient.Enums;

namespace UBM.MiddlewareClient.Converters
{
    [ValueConversion(typeof(bool?), typeof(string))]
    internal class NotificationSentConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((bool?)value)
            {
                case true:
                    return "Yes";
                case false:
                    return "No";
                case null:
                    return "No";
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}