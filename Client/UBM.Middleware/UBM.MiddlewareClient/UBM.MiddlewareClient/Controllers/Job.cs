﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Controllers
{
    public class Job
    {
        public string Name { get; private set; }
        public string DisplayName { get; private set; }        
        public int JobId { get; private set; }
        public int NumberOfStepsRemaining
        {
           get
           {
               return numberOfSteps - stepsCompleted;   
           }
        }

        private int numberOfSteps;
        private int stepsCompleted;

        private readonly ISystemService systemService;

        public Job(ISystemService systemService, string jobName)
        {
            this.systemService = systemService;
            this.Name = jobName;

            Initialise();
        }

        private Job(ISystemService systemService)
        {
            this.systemService = systemService;
        }

        public static IEnumerable<Job> CreateJobs(ISystemService systemService, IEnumerable<string> jobNames)
        {
            foreach (string jobName in jobNames)
            {
                yield return new Job(systemService, jobName);
            }
        }

        public static Job CreateJobById(ISystemService systemService, int jobId)
        {
            Job job = new Job(systemService)
                          {
                              Name = new Random().NextDouble().ToString(),
                              JobId = jobId,
                              DisplayName = systemService.GetJobShortNameFromId(jobId),
                              numberOfSteps = systemService.GetJobStepsCount(jobId)
                          };

            return job;
        }

        public void StepComplete()
        {
            stepsCompleted++;
        }

        private void Initialise()
        {
            JobId = systemService.GetJobIdFromString(Name);
            DisplayName = systemService.GetJobShortNameFromId(JobId);
            numberOfSteps = systemService.GetJobStepsCount(JobId);
        }


    }
}
