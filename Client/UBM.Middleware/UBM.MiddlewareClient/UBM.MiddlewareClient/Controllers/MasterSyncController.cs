﻿using System;
using System.ServiceModel;
using System.Threading;
using UBM.Logger;
using UBM.MiddlewareClient.ClientSyncService;
using UBM.MiddlewareClient.Enums;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Controllers
{

    public delegate void TrafficLightEventHandler(object sender, TrafficLightEventHandlerArgs args);

    public class TrafficLightEventHandlerArgs
    {
        public LightColour Light { get; set; }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
    CallbackBehavior(UseSynchronizationContext = false)]
    public sealed class MasterSyncController : ISynchroniseCallback
    {
        #region Fields

        private SynchroniseClient syncClient;
        private bool syncOnline;

        private delegate bool SyncDelegate(ISyncable syncable);

        private SynchronizationContext uiContext;
        private IMessageService messageService;


        #endregion

        public event TrafficLightEventHandler SyncLight;

        private void SetTrafficLight(LightColour colour)
        {
            OnSyncLight(new TrafficLightEventHandlerArgs
                            {
                                Light = colour
                            });
        }

        public void OnSyncLight(TrafficLightEventHandlerArgs args)
        {
            TrafficLightEventHandler handler = SyncLight;
            if (handler != null) handler(this, args);
        }

        #region Private Methods

        private bool SyncRecover(SyncDelegate tryAgain, ISyncable syncable)
        {
            TraceHelper.Trace("Sync recovering from error state");
            syncClient.Abort();
            SetTrafficLight(LightColour.Amber);
            Thread.Sleep(1000);
            try
            {
                syncClient = new SynchroniseClient(new InstanceContext(this));
                syncClient.Open();

            }
            catch(Exception exception)
            {
                TraceHelper.Trace(exception);
                syncClient.Abort();
                SetTrafficLight(LightColour.Red);
                syncOnline = false;
                TraceHelper.Trace(exception);
                Log.Error("Failed to initialise Master Sync Controller", exception);
                return false;
            }

            if (!string.IsNullOrEmpty(syncable.SystemName))
            {
                return Connect() && tryAgain(syncable);
            }
            return Connect();
          
        }

        private bool RequestView(ISyncable syncable)
        {
            try
            {
                return syncClient.View(new Viewing
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                return SyncRecover(RequestView, syncable);
            }
        }

        private bool RequestUnview(ISyncable syncable)
        {
            try
            {
                return syncClient.Unview(new Viewing
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                return SyncRecover(RequestUnview, syncable);
            }
        }

        private bool RequestLock(ISyncable syncable)
        {
            try
            {
                return syncClient.Lock(new Lock
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    LockTime = DateTime.Now,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                return SyncRecover(RequestLock, syncable);
            }
        }

        private bool RequestUnlock(ISyncable syncable)
        {
            try
            {
                return syncClient.Unlock(new Lock
                {
                    ID = syncable.ID,
                    SystemName = syncable.SystemName,
                    UserName = Environment.UserName,
                    LockTime = DateTime.Now,
                    ObjectType = syncable.GetTypeName()
                });
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                return SyncRecover(RequestUnlock, syncable);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates new instance of Sync Controller
        /// </summary>
        /// <param name="master">The master.</param>
        /// <param name="syncObjectType">Type of the sync object.</param>
        public MasterSyncController(SynchronizationContext uiContext, IMessageService messageService) // , SyncObjectType syncObjectType)
        {
            
            if (syncClient == null)
            {
                this.messageService = messageService;
                this.uiContext = uiContext;
                syncClient = new SynchroniseClient(new InstanceContext(this));
                syncClient.Open();

                Connect();
            }
        }

        #endregion

        #region Internal Methods

        internal SyncData GetSyncData(string syncObjectType)
        {
            return syncOnline ? syncClient.GetSyncData(syncObjectType) : null;
        }

        /// <summary>
        /// Connects to sync service
        /// </summary>
        /// <returns></returns>
        internal bool Connect()
        {
            try
            {
                syncClient.Connect(Environment.UserName);
                SynchroniseNotification();
                SetTrafficLight(LightColour.Green);
                syncOnline = true;
                return true;
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                messageService.SendUserMessage("Could not connect to Synchronisation Service", MessageType.Error); 
                SetTrafficLight(LightColour.Red);
                syncOnline = false;
                return false;
            }
        }

        /// <summary>
        /// Disconnects from sync service
        /// </summary>
        internal void Disconnect()
        {
            try
            {
                syncClient.Disconnect(Environment.UserName);
                syncClient.Close();
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                syncClient.Abort();
            }
        }

        /// <summary>
        /// Checks if still connected to sync service and reconnects if its faulted
        /// </summary>
        /// <returns></returns>
        internal bool SyncCheckAndRecover()
        {
            if (syncClient.State != CommunicationState.Opened)
            {
                return SyncRecover(null, null);
            }
            return true;
        }

        /// <summary>
        ///   Signals other connected clients to refresh from database
        /// </summary>
        internal void RefreshClients()
        {
            try
            {
                syncClient.RefreshClients();
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                if (SyncRecover(null, null)) syncClient.RefreshClients();
            }
        }

        /// <summary>
        ///   Adds this user as a viewer of a syncable on sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void View(ISyncable syncable)
        {
            if (syncOnline && RequestView(syncable))
            {
                syncable.AddViewer(Environment.UserName);
            }
        }

        /// <summary>
        ///   Removes this user as a viewer of a syncable on sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void Unview(ISyncable syncable)
        {
            if (syncOnline && RequestUnview(syncable))
            {
                syncable.RemoveViewer(Environment.UserName);
            }
        }

        /// <summary>
        ///   Locks a syncable on the sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void Lock(ISyncable syncable)
        {
            if (syncOnline && RequestLock(syncable))
            {
                syncable.Lock(Environment.UserName);
            }
        }

        /// <summary>
        ///   Unlocks a syncable on sync service
        /// </summary>
        /// <param name = "syncable"></param>
        internal void Unlock(ISyncable syncable)
        {
            if (syncOnline && RequestUnlock(syncable))
            {
                syncable.Unlock(Environment.UserName);
            }
        }

        internal void UnlockAll(string syncObjectType)
        {
            syncClient.UnlockAllForUser(Environment.UserName, syncObjectType);            
        }


        #endregion

        #region Callbacks

        public delegate void SynchroniseHandler();
        public delegate void RefreshFromDatabase();
        public delegate void ProgressReport(string jobName, string stepDetail, double percentageComplete);

        public event SynchroniseHandler OnSynchroniseNotification;
        public event RefreshFromDatabase OnRefreshFromDatabaseNotification;
        public event ProgressReport OnProgressReportNotification;

        public void SynchroniseNotification()
        {
            SendOrPostCallback callback = delegate
            {
                if (OnSynchroniseNotification != null)
                {
                    OnSynchroniseNotification();
                }
            };

            uiContext.Post(callback, null);
        }

        public void RefreshFromDatabaseNotification()
        {
            SendOrPostCallback callback = delegate
            {
                if (OnRefreshFromDatabaseNotification != null)
                {
                    OnRefreshFromDatabaseNotification();
                }
            };

            uiContext.Post(callback, null);
        }

        public void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
            SendOrPostCallback callback = delegate
            {
                if (OnProgressReportNotification != null)
                {
                    OnProgressReportNotification(jobName, stepDetail, percentageComplete);
                }
            };

            uiContext.Post(callback, null);
        }

        public void DebugNotification(string message)
        {
            Console.WriteLine(message);
        }

        #endregion

      
    }

  
}
