﻿using System;
using System.Collections.Generic;
using UBM.Logger;
using UBM.MiddlewareClient.ClientSyncService;
using UBM.MiddlewareClient.Infrastructure;
using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Interfaces;
using UBM.MiddlewareClient.Model.Helper;
using UBM.MiddlewareClient.Model.Interfaces;

namespace UBM.MiddlewareClient.Controllers
{

    abstract class GenericSyncController<T> : IDisposable
    {
        #region Fields

        protected IEnumerable<T> SyncCollection;
        protected static readonly List<Lock> LockList = new List<Lock>();
        protected static readonly List<Viewing> ViewList = new List<Viewing>();

        protected readonly MasterSyncController MasterSyncController;

        protected bool IgnoreNextRefresh;
    
        #endregion

        #region Private Methods


        private SyncData GetSyncData()
        {
            return MasterSyncController.GetSyncData(GetTypeName());
        }

        private ISyncable FindSyncObject(int id, string systemName)
        {
            foreach (ISyncable customer in SyncCollection)
            {
                if (customer.ID == id && customer.SystemName == systemName)
                {
                    return customer;
                }
            }
            return null;
        }

       

        #endregion

        #region Protected Methods

        protected virtual void Synchronise()
        {
            try
            {
                TraceHelper.Trace("Synchronise: " + GetTypeName());
                SyncData syncData = GetSyncData();


                if (syncData != null) // && (syncData.LockedList.GetLength(0) != 0 || syncData.ViewingList.GetLength(0) != 0))
                {
                    SyncViewsClientSide(syncData.ViewingList);
                    SyncLocksClientSide(syncData.LockedList);
                }
            }
            catch (Exception exception)
            {
                TraceHelper.Trace(exception);
                Log.Error("Sync Problem", exception);
            }
        }

        protected void SyncLocksClientSide(IEnumerable<Lock> locks)
        {
            //LogHelper.Trace(TraceType.Controller);
            // Add locks
            foreach (Lock l in locks)
            {
                bool addlock = true;

                foreach (Lock mylock in LockList)
                {
                    if (mylock.ID == l.ID && mylock.SystemName == l.SystemName) addlock = false;
                }

                if (addlock)
                {
                    LockList.Add(l);

                    ISyncable obj = FindSyncObject(l.ID, l.SystemName);

                    if(obj != null)
                    {
                        string message = obj.Lock(l.UserName);
                        //SendUserMessage(message, MessageType.Info, l.ObjectType); @JP
                    }

                }
            }

            // Remove locks
            var expiredLocks = new List<Lock>();

            foreach (Lock mylocks in LockList)
            {
                bool lockstilvalid = false;
                foreach (Lock serverlocks in locks)
                {
                    if (mylocks.ID == serverlocks.ID && mylocks.SystemName == serverlocks.SystemName)
                    {
                        lockstilvalid = true;
                        break;
                    }
                }

                if (!lockstilvalid)
                {
                    expiredLocks.Add(mylocks);
                }
            }

            foreach (Lock expired in expiredLocks)
            {
                LockList.Remove(expired);

                ISyncable obj = FindSyncObject(expired.ID, expired.SystemName);

                if(obj != null)
                {
                    string message = obj.Unlock(expired.UserName);
                    //SendUserMessage(message, MessageType.Info, expired.ObjectType); @JP
                }

            }
        }

        protected void SyncViewsClientSide(IEnumerable<Viewing> views)
        {
            //LogHelper.Trace(TraceType.Controller);
            // Add views
            foreach (Viewing v in views)
            {
                if (v.UserName != Environment.UserName)
                {
                    ISyncable syncable = FindSyncObject(v.ID, v.SystemName);

                    if(syncable != null)
                    {
                        string message = syncable.AddViewer(v.UserName);
                        //SendUserMessage(message, MessageType.Info, v.ObjectType); @JP
                    }
                }

                bool addview = true;

                foreach (Viewing clientView in ViewList)
                {
                    if (clientView.ID == v.ID && clientView.SystemName == v.SystemName && clientView.UserName == v.UserName)
                        addview = false;
                }

                if (addview)
                {
                    ViewList.Add(v);
                }
            }

            // Remove views
            var expiredViewings = new List<Viewing>();

            foreach (Viewing clientView in ViewList)
            {
                bool viewStillValid = false;
                foreach (Viewing serverViews in views)
                {
                    if (clientView.ID == serverViews.ID && clientView.SystemName == serverViews.SystemName &&
                        clientView.UserName == serverViews.UserName)
                    {
                        viewStillValid = true;
                        break;
                    }
                }

                if (!viewStillValid)
                {
                    expiredViewings.Add(clientView);
                }
            }

            foreach (Viewing expired in expiredViewings)
            {
                ViewList.Remove(expired);

                if (expired.UserName != Environment.UserName)
                {
                    ISyncable syncable = FindSyncObject(expired.ID, expired.SystemName);

                    if(syncable != null)
                    {
                        string message = syncable.RemoveViewer(expired.UserName);
                        //SendUserMessage(message, MessageType.Info, expired.ObjectType); @JP
                    }
                }
            }
        }

        #endregion

        #region Constructor

        protected GenericSyncController(MasterSyncController masterSyncController, IEnumerable<T> syncCollection)
        {
            //LogHelper.Trace(TraceType.Controller);

            this.MasterSyncController = masterSyncController;
            this.SyncCollection = syncCollection;
            this.IgnoreNextRefresh = false;

            //MasterSyncController.Value.OnSynchroniseNotification += Synchronise;
        }

        #endregion

        #region Public Methods

        public void SyncClientCollection(bool download)
        {
            TraceHelper.Trace(TraceType.Controller);
            Synchronise();
            SyncClientCollection();
        }
        
        public void SyncClientCollection(IEnumerable<T> collection)
        {
            TraceHelper.Trace(TraceType.Controller);
            SyncCollection = collection;
            SyncClientCollection();
        }

        public void SyncClientCollection(IEnumerable<T> collection, bool download)
        {
            TraceHelper.Trace(TraceType.Controller);
            SyncCollection = collection;
            if (download) Synchronise();
            SyncClientCollection();
        }

        public void SyncClientCollection()
        {
            TraceHelper.Trace(TraceType.Controller);
            ISyncable syncable;

            foreach (Viewing viewing in ViewList)
            {
                syncable = FindSyncObject(viewing.ID, viewing.SystemName);
                if (syncable != null) syncable.AddViewer(viewing.UserName);
            }
            foreach (Lock locked in LockList)
            {
                syncable = FindSyncObject(locked.ID, locked.SystemName);
                if (syncable != null) syncable.Lock(locked.UserName);
            }
        }

        public void View(ISyncable syncable)
        {
            TraceHelper.Trace(TraceType.Controller);
            MasterSyncController.View(syncable);
        }

        public void Unview(ISyncable syncable)
        {
            TraceHelper.Trace(TraceType.Controller);
            MasterSyncController.Unview(syncable);
        }

        public void Lock(ISyncable syncable)
        {
            TraceHelper.Trace(TraceType.Controller);
            MasterSyncController.Lock(syncable);
        }

        public void Unlock(ISyncable syncable)
        {
            TraceHelper.Trace(TraceType.Controller);
            MasterSyncController.Unlock(syncable);
        }

        public void UnlockAll()
        {
            TraceHelper.Trace(TraceType.Controller);
            MasterSyncController.UnlockAll(GetTypeName());
        }

        public void RefreshClients()
        {
            TraceHelper.Trace(TraceType.Controller);
            IgnoreNextRefresh = true;
            MasterSyncController.RefreshClients();
        }

        public bool HasActiveLocks()
        {

            return LockList.Count > 0;
        }

        public string GetTypeName()
        {
            return typeof(T).Name;
        }

        #endregion

        public void Dispose()
        {
            this.OnDispose();
        }

        protected virtual void OnDispose()
        {
            string msg = string.Format("{0} ({1}) Finalized", this.GetType().Name, this.GetHashCode());
            Console.WriteLine(msg);
        }

        ~GenericSyncController()
        {
            string msg = string.Format("{0} ({1}) Finalized", this.GetType().Name, this.GetHashCode());
            Console.WriteLine(msg);

        }


    }
}
