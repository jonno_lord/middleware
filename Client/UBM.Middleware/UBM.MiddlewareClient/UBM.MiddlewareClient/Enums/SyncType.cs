﻿namespace UBM.MiddlewareClient.Enums
{
    /// <summary>
    /// Customers states for synching Middleware clients
    /// </summary>
    public enum SyncType
    {
        Lock,
        Unlock,
        View,
        Unview
    }
}