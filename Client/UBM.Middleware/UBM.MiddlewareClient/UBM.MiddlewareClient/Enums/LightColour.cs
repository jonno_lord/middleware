﻿namespace UBM.MiddlewareClient.Enums
{
    /// <summary>
    /// Colours for traffic lights
    /// </summary>
    public enum LightColour
    {
        Amber,
        Red,
        Green
    }
}