﻿using UBM.MiddlewareClient.Infrastructure.Enums;
using UBM.MiddlewareClient.Model.Orm;
using UBM.MiddlewareClient.Model.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Model.Domain;
using UBM.MiddlewareClient.Model.Interfaces;
using Moq;

namespace UBM.MiddlewareClient.Model.Tests
{
    
    
    /// <summary>
    ///This is a test class for UserServiceTest and is intended
    ///to contain all UserServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UserServiceTest
    {
        private static IUserService userService;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        #region PageAccess


        [TestMethod()]
        public void CheckPageAccessForSingleGroup()
        {
            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetUserGroupAccess()).Returns(new List<vwUserGroupAccess>
                                                               {
                                                                   new vwUserGroupAccess
                                                                       {
                                                                           GroupName = "CTX-MW-Invoicing",
                                                                           AccessString = "0=rw,1=r,2=rw,3=rw,4=rw,5=,6=,7=rw,8=rw,9="
                                                                       }
                                                               });

            var notification = new Mock<INotificationService>();
            notification.Setup(s => s.GetUsersGroups(Environment.UserName)).Returns(new List<string>()
                                                                                        {
                                                                                           "CTX-MW-Invoicing"
                                                                                        });

            userService = new UserService(dao.Object, notification.Object);

            List<PageAccess> expected = new List<PageAccess>
                                            {
                                                new PageAccess(ClientPageNumber.HomePage, true, true),
                                                new PageAccess(ClientPageNumber.CustomerDedupe, true, false),
                                                new PageAccess(ClientPageNumber.CustomerDetails, true,true),
                                                new PageAccess(ClientPageNumber.CustomerViewer, true, true),
                                                new PageAccess(ClientPageNumber.OrderViewer, true, true),
                                                new PageAccess(ClientPageNumber.EmailHistory, false, false),
                                                new PageAccess(ClientPageNumber.LegalHistory, false, false),
                                                new PageAccess(ClientPageNumber.InvoicePublication, true, true),
                                                new PageAccess(ClientPageNumber.PrintReceipts, true, true),
                                                new PageAccess(ClientPageNumber.Admin, false, false),
                                                new PageAccess(ClientPageNumber.RunFeeds, false, false),
                                                new PageAccess(ClientPageNumber.Diagnostics, false, false)
                                            };
            List<PageAccess> actual = userService.GetPageAccessRights();

            for (int i = 0; i < actual.Count - 1; i++)
            {
                Assert.AreEqual(expected[i].ClientPageNumber, actual[i].ClientPageNumber);
                Assert.AreEqual(expected[i].CanRead, actual[i].CanRead);
                Assert.AreEqual(expected[i].CanWrite, actual[i].CanWrite);
            }

          
        }

        [TestMethod()]
        public void CheckPermissionsForUsersInMultipleGroupsCombineCorrectly()
        {
            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetUserGroupAccess()).Returns(new List<vwUserGroupAccess>
                                                               {
                                                                   new vwUserGroupAccess
                                                                       {
                                                                           GroupName = "CTX-MW-Invoicing",
                                                                           AccessString = "0=rw,1=r,2=rw,3=rw,4=rw,5=,6=,7=rw,8=rw,9="
                                                                       },
                                                                   new vwUserGroupAccess
                                                                       {
                                                                           GroupName = "TestGroup",
                                                                           AccessString = "5=rw"
                                                                       }
                                                               });

            var notification = new Mock<INotificationService>();
            notification.Setup(s => s.GetUsersGroups(Environment.UserName)).Returns(new List<string>()
                                                                                        {
                                                                                           "CTX-MW-Invoicing",
                                                                                           "TestGroup"
                                                                                        });

            userService = new UserService(dao.Object, notification.Object);

            List<PageAccess> expected = new List<PageAccess>
                                            {
                                                new PageAccess(ClientPageNumber.HomePage, true, true),
                                                new PageAccess(ClientPageNumber.CustomerDedupe, true, false),
                                                //new PageAccess(ClientPageNumber.CustomerDedupe, false, false),
                                                new PageAccess(ClientPageNumber.CustomerDetails, true,true),
                                                new PageAccess(ClientPageNumber.CustomerViewer, true, true),
                                                new PageAccess(ClientPageNumber.OrderViewer, true, true),
                                                new PageAccess(ClientPageNumber.EmailHistory, true, true), //
                                                new PageAccess(ClientPageNumber.LegalHistory, false, false),
                                                new PageAccess(ClientPageNumber.InvoicePublication, true, true),
                                                new PageAccess(ClientPageNumber.PrintReceipts, true, true),
                                                new PageAccess(ClientPageNumber.Admin, false, false),
                                                new PageAccess(ClientPageNumber.RunFeeds, false, false),
                                                new PageAccess(ClientPageNumber.Diagnostics, false, false)
                                            };
            List<PageAccess> actual = userService.GetPageAccessRights();

            for (int i = 0; i < actual.Count - 1; i++)
            {
                Assert.AreEqual(expected[i].ClientPageNumber, actual[i].ClientPageNumber);
                Assert.AreEqual(expected[i].CanRead, actual[i].CanRead);
                Assert.AreEqual(expected[i].CanWrite, actual[i].CanWrite);
            }


        }

        [TestMethod()]
        public void CheckUnauthorizedUserHasNoAccess()
        {
            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetUserGroupAccess()).Returns(new List<vwUserGroupAccess>
                                                               {
                                                                   new vwUserGroupAccess
                                                                       {
                                                                           GroupName = "CTX-MW-Invoicing",
                                                                           AccessString = "0=rw,1=r,2=rw,3=rw,4=rw,5=,6=,7=rw,8=rw,9="
                                                                       }
                                                               });

            var notification = new Mock<INotificationService>();
            notification.Setup(s => s.GetUsersGroups(Environment.UserName)).Returns(new List<string>(){""});

            userService = new UserService(dao.Object, notification.Object);


            List<PageAccess> actual = userService.GetPageAccessRights();

            for (int i = 0; i < actual.Count - 1; i++)
            {                
                Assert.AreEqual(false, actual[i].CanRead);
                Assert.AreEqual(false, actual[i].CanWrite);
            }


        }

        [TestMethod()]
        [ExpectedException(typeof(FormatException))]
        public void CheckFaultyAccessStringThrowsAnException()
        {
            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetUserGroupAccess()).Returns(new List<vwUserGroupAccess>
                                                               {
                                                                   new vwUserGroupAccess
                                                                       {
                                                                           GroupName = "CTX-MW-Invoicing",
                                                                           AccessString = "abc=defg"
                                                                       }
                                                               });

            var notification = new Mock<INotificationService>();
            notification.Setup(s => s.GetUsersGroups(Environment.UserName)).Returns(new List<string>() { "CTX-MW-Invoicing" });

            userService = new UserService(dao.Object, notification.Object);


            List<PageAccess> actual = userService.GetPageAccessRights();

            Assert.Fail("Should have thrown an exception");
        }

        [TestMethod()]
        [ExpectedException(typeof(FormatException))]
        public void CheckAnotherFaultyAccessStringThrowsAnException()
        {

            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetUserGroupAccess()).Returns(new List<vwUserGroupAccess>
                                                               {
                                                                   new vwUserGroupAccess
                                                                       {
                                                                           GroupName = "CTX-MW-Invoicing",
                                                                           AccessString = "1=rw2=rw,3=rw"
                                                                       }
                                                               });

            var notification = new Mock<INotificationService>();
            notification.Setup(s => s.GetUsersGroups(Environment.UserName)).Returns(new List<string>() { "CTX-MW-Invoicing" });

            userService = new UserService(dao.Object, notification.Object);


            List<PageAccess> actual = userService.GetPageAccessRights();

            Assert.Fail("Should have thrown an exception");
        }

        #endregion

        [TestMethod()]
        public void CheckGetTopFivePages()
        {
            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetMostUsed()).Returns(new MostUsed()
                                                        {
                                                            PageCountString = "p1=1,p2=2,p3=3,p4=4,p5=5"
                                                        });

            var notification = new Mock<INotificationService>();

            userService = new UserService(dao.Object, notification.Object);

            List<int> top5 = userService.GetTopFivePages();

            List<int> expected = new List<int>{5,4,3,2,1};

            for (int i = 0; i < top5.Count; i++ )
            {
                Assert.AreEqual(expected[i], top5[i]);
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(FormatException))]
        public void CheckGetTopFivePagesWithFaultyString()
        {
            var dao = new Mock<IUserDao>();
            dao.Setup(s => s.GetMostUsed()).Returns(new MostUsed()
            {
                PageCountString = "p1=1,p2=2,p3=3,pdsgsdg4=4,sdfgsdgfsdfgp5=5"
            });

            var notification = new Mock<INotificationService>();

            userService = new UserService(dao.Object, notification.Object);

            List<int> top5 = userService.GetTopFivePages();

            List<int> expected = new List<int> { 5, 4, 3, 2, 1 };

            for (int i = 0; i < top5.Count; i++)
            {
                Assert.AreEqual(expected[i], top5[i]);
            }
        }

    }
}
