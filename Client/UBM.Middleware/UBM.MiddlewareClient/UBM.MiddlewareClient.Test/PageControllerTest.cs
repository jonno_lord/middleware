﻿using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UBM.MiddlewareClient.Test
{
    
    
    /// <summary>
    ///This is a test class for PageControllerTest and is intended
    ///to contain all PageControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PageControllerTest
    {


        private TestContext testContextInstance;

        int currentPageNumber = 0;
        int collectionNumberOfItems = 50;
        int maxItemsOnPage = 10;

        private PageController pageController;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            PageController.GetPageDelegate getPage = null;
            pageController = PageController.CreatePageController(currentPageNumber, collectionNumberOfItems, maxItemsOnPage, getPage);
        }
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for PageController Constructor
        ///</summary>
        [TestMethod()]
        public void PageControllerConstructorTest()
        {
            PageController.GetPageDelegate getPage = null; // TODO: Initialize to an appropriate value
            PageController target = new PageController(currentPageNumber, collectionNumberOfItems, maxItemsOnPage, getPage);
            Assert.IsInstanceOfType(target, typeof(PageController));
        }

        /// <summary>
        ///A test for CreatePageController
        ///</summary>
        [TestMethod()]
        public void CreatePageControllerTest()
        {
            PageController.GetPageDelegate getPage = null; // TODO: Initialize to an appropriate value
            PageController target = PageController.CreatePageController(currentPageNumber, collectionNumberOfItems, maxItemsOnPage, getPage);
            Assert.IsInstanceOfType(target, typeof(PageController));
        }

        /// <summary>
        ///A test for GetCollectionNumberOfItems
        ///</summary>
        [TestMethod()]
        public void GetCollectionNumberOfItemsTest()
        {
            PageController target = pageController;
            int expected = 50;
            int actual = target.GetCollectionNumberOfItems();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetEndIndex
        ///</summary>
        [TestMethod()]
        public void GetEndIndexTest()
        {
            PageController target = pageController;
            int expected = 10;
            int actual = target.GetEndIndex();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetMaxItemsOnPage
        ///</summary>
        [TestMethod()]
        public void GetMaxItemsOnPageTest()
        {
            PageController target = pageController;
            int expected = 10;
            int actual = target.GetMaxItemsOnPage();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetMaxPageForSetItemCount
        ///</summary>
        [TestMethod()]
        public void GetMaxPageForSetItemCountTest()
        {
            PageController target = pageController;
            int count = 55; // TODO: Initialize to an appropriate value
            int expected = 6; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetMaxPageForSetItemCount(count);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetNumberOfPages
        ///</summary>
        [TestMethod()]
        public void GetNumberOfPagesTest()
        {
            PageController target = pageController;
            int expected = 5;
            int actual;
            actual = target.GetNumberOfPages();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetStartIndex
        ///</summary>
        [TestMethod()]
        public void GetStartIndexTest()
        {
            PageController target = pageController;
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetStartIndex();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for NextPage
        ///</summary>
        [TestMethod()]
        public void NextPageTest()
        {
            PageController target = pageController;
            int expected = 1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.NextPage();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PreviousPage
        ///</summary>
        [TestMethod()]
        public void PreviousPageTest()
        {
            PageController target = pageController;
            target.NextPage();
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.PreviousPage();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SetCurrentPageAndValidate
        ///</summary>
        [TestMethod()]
        public void SetCurrentPageAndValidateTest()
        {
            PageController target = pageController;
            int pageNumber = -2; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.SetCurrentPageAndValidate(pageNumber);
            Assert.AreEqual(expected, actual);
        }
    }
}
