﻿using UBM.MiddlewareClient.Controllers;
using UBM.MiddlewareClient.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UBM.MiddlewareClient.Model;
using System.Collections.ObjectModel;

namespace UBM.MiddlewareClient.Test
{
    
    
    /// <summary>
    ///This is a test class for AccessControllerTest and is intended
    ///to contain all AccessControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AccessControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AccessController Constructor
        ///</summary>
        [TestMethod()]
        public void AccessControllerConstructorTest()
        {
            AccessController target = new AccessController(new LinqDataAccess());
            Assert.IsInstanceOfType(target, typeof(AccessController));
        }

        /// <summary>
        ///A test for GetGroupsForThisUser
        ///</summary>
        [TestMethod()]
        [DeploymentItem("UBM.MiddlewareClient.exe")]
        public void GetGroupsForThisUserTest()
        {
            List<string> actual = AccessController_Accessor.GetGroupsForThisUser();
            Assert.IsTrue(actual.Contains("Developers"));
        }


        /// <summary>
        ///A test for PageAccessRights
        ///</summary>
        [TestMethod()]
        public void PageAccessRightsTest()
        {
            AccessController target = new AccessController(new LinqDataAccess()); 
            ObservableCollection<PageAccess> actual = target.PageAccessRights;
            Assert.IsTrue(actual[0].CanRead && actual[0].CanWrite);
        }

        /// <summary>
        ///A test for SearchTypes
        ///</summary>
        [TestMethod()]
        public void SearchTypesTest()
        {
            AccessController target = new AccessController(new LinqDataAccess());
            Assert.IsTrue(target.SearchTypes.Contains("CE"));
        }
    }
}
