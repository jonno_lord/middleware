﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    public enum SearchOnDates
    {
        DateRejected,
        HoldDate,
        LastActioned,
        LoadedIntoMiddleware
    }
}
