﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    /// <summary>
    /// Action message types
    /// </summary>
    public enum MessageType
    {
        None,
        Good,
        Bad,
        Error,
        Info,
        Busy
    }
}