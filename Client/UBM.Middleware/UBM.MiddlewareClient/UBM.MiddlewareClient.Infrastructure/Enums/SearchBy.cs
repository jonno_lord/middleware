﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    /// <summary>
    /// Field to search by
    /// </summary>
    public enum SearchBy
    {
        CustomerName,
        URN,
        JDENumber,
        OrderNumber,
        ActionedBy,
        Sender,
        Reason,
        BatchNumber,
        ReceiptNumber,
        Postcode
    }
}