﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    /// <summary>
    ///   Definition of GUI Page Numbers
    /// </summary>
    public enum ClientPageNumber
    {
        HomePage = 0,
        CustomerDedupe = 1,
        CustomerDetails = 2,
        CustomerViewer = 3,
        OrderViewer = 4,
        EmailHistory = 5,
        LegalHistory = 6,
        InvoicePublication = 7,
        PrintReceipts = 8,
        Admin = 9,
        RunFeeds = 10,
        Diagnostics = 11
    }

}