﻿namespace UBM.MiddlewareClient.Infrastructure.Enums
{
    /// <summary>
    /// Search string matching type
    /// </summary>
    public enum SearchMatching
    {
        Fuzzy,
        StartsWith,
        Exact,
    }
}