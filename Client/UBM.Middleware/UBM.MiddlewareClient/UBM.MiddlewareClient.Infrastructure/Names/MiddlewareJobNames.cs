﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Infrastructure.Names
{
    public class MiddlewareJobNames
    {
        private const string ASOP_UK = "ASOPUK";
        private const string ASOP_DALTONS = "ASOPDLT";
        private const string ASOP_HOLLAND = "ASOPHOL";

        private static readonly Dictionary<string, string> AsopSystemNameToShortPrefixMapping = new Dictionary<string, string>
        {
            { MiddlewareSystemNames.ASOP_UK, ASOP_UK },
            { MiddlewareSystemNames.ASOP_DALTONS, ASOP_DALTONS },
            { MiddlewareSystemNames.ASOP_HOLLAND, ASOP_HOLLAND}
        };

        private const string CREDIT_CARD_ORDERS = "-CCD";
        private const string IMMEDIATE_INVOICES = "-IMM";
        private const string PUBLICATION_INVOICES = "-PUB";
        private const string PREPAID_INVOICES = "-PRE";
        private const string REVENUE = "-REV";

        private static readonly Dictionary<MiddlewareJobType, string> InvoiceExtractionToJobPrefixMapping = new Dictionary<MiddlewareJobType, string>
        {
            { MiddlewareJobType.ReceiptsCreditCard, CREDIT_CARD_ORDERS },
            { MiddlewareJobType.InvoicesImmediate, IMMEDIATE_INVOICES },
            { MiddlewareJobType.InvoicesPrepaid, PREPAID_INVOICES },
            { MiddlewareJobType.InvoicesPublication, PUBLICATION_INVOICES },
            { MiddlewareJobType.ReceiptsRevenue, REVENUE }
        };

        public const string ASOP_UK_CREDIT_CARD_ORDERS = ASOP_UK + CREDIT_CARD_ORDERS;
        public const string ASOP_UK_IMMEDIATE_INVOICES = ASOP_UK + IMMEDIATE_INVOICES;
        public const string ASOP_UK_PREPAID_INVOICES = ASOP_UK + PREPAID_INVOICES;
        public const string ASOP_UK_PUBLICATION_INVOICES = ASOP_UK + PUBLICATION_INVOICES;
        public const string ASOP_UK_REVENUE = ASOP_UK + REVENUE;

        public const string ASOP_DALTONS_CREDIT_CARD_ORDERS = ASOP_DALTONS + CREDIT_CARD_ORDERS;
        public const string ASOP_DALTONS_IMMEDIATE_INVOICES = ASOP_DALTONS + IMMEDIATE_INVOICES;
        public const string ASOP_DALTONS_PREPAID_INVOICES = ASOP_DALTONS + PREPAID_INVOICES;
        public const string ASOP_DALTONS_PUBLICATION_INVOICES = ASOP_DALTONS + PUBLICATION_INVOICES;
        public const string ASOP_DALTONS_REVENUE = ASOP_DALTONS + REVENUE;

        public const string ASOP_HOLLAND_IMMEDIATE_INVOICES = ASOP_HOLLAND + IMMEDIATE_INVOICES;
        public const string ASOP_HOLLAND_PREPAID_INVOICES = ASOP_HOLLAND + PREPAID_INVOICES;
        public const string ASOP_HOLLAND_PUBLICATION_INVOICES = ASOP_HOLLAND + PUBLICATION_INVOICES;

        public const string RECEIPT_COPIES = "COMMON-RECEIPTS";

        /*
        private const string MSI = "MSI";
        private const string ASPAC = "-ASPAC";
        private const string INVOICE = "-INVOICE";
        private const string DIARYNOTES = "-DIARYNOTES";
        private const string PAYMENTS = "-PAYMENTS";
        private const string CREATEFORM = "-CREATEFORM";
        private const string EVENTSFORCE = "-EVENTSFORCE";
        private const string ADVERTISING = "-ADVERTISING";
        private const string CREDITSTATUS = "-CREDITSTATUS";

        public const string MSI_INVOICES_EXPORT = MSI + INVOICE;
        public const string MSI_DIARY_NOTES_IMPORT = MSI + DIARYNOTES;
        public const string MSI_PAYMENTS_IMPORT = MSI + PAYMENTS;
        public const string MSI_CREATE_FORM = MSI + CREATEFORM;
        public const string MSI_EVENTSFORCE_EXPORT = MSI + EVENTSFORCE;
        public const string MSI_ADVERTISING_EXPORT = MSI + ADVERTISING;
        public const string MSI_CREDIT_STATUS_FORWARD_FEED = MSI + CREDITSTATUS;

        public const string MSI_ASPAC_CREATE_FORM = MSI + ASPAC + CREATEFORM;
        public const string MSI_ASPAC_INVOICE_EXPORT = MSI + ASPAC + INVOICE;
        public const string MSI_ASPAC_CREDIT_STATUS_FORWARD_FEED = MSI + ASPAC + CREDITSTATUS;
        public const string MSI_ASPAC_DIARY_NOTES_IMPORT = MSI + ASPAC + DIARYNOTES;
        public const string MSI_ASPAC_PAYMENTS = MSI + ASPAC + PAYMENTS;
        */


        public static List<string> MiddlewareAsopJobNames = new List<string>
        {
            ASOP_UK_CREDIT_CARD_ORDERS,
            ASOP_UK_IMMEDIATE_INVOICES,
            ASOP_UK_PREPAID_INVOICES,
            ASOP_UK_PUBLICATION_INVOICES,
            ASOP_UK_REVENUE,
            ASOP_DALTONS_CREDIT_CARD_ORDERS,
            ASOP_DALTONS_IMMEDIATE_INVOICES,
            ASOP_DALTONS_PREPAID_INVOICES,
            ASOP_DALTONS_PUBLICATION_INVOICES,
            ASOP_DALTONS_REVENUE,
            ASOP_HOLLAND_IMMEDIATE_INVOICES,
            ASOP_HOLLAND_PREPAID_INVOICES,
            ASOP_HOLLAND_PUBLICATION_INVOICES
        };
        
        public static List<string> GetAsopJobNamesContaining(string key)
        {
            return MiddlewareAsopJobNames.Where(c => c.Contains(key)).ToList();
        }

        public static List<string> GetAsopJobNamesContaining(string key, string systemName)
        {
            List<string> jobNamesUnfiltered = GetAsopJobNamesContaining(key);
            string systemShortName = AsopSystemNameToShortPrefixMapping[systemName];

            return jobNamesUnfiltered.Where(c => c.Contains(systemShortName)).ToList();
        }

        public static List<string> GetAsopJobNamesOfType(MiddlewareJobType jobType)
        {
            string prefix = InvoiceExtractionToJobPrefixMapping[jobType];
            return GetAsopJobNamesContaining(prefix);
        }

        public static List<string> GetAsopJobNamesOfType(MiddlewareJobType jobType, string systemName)
        {
            List<string> jobNamesUnfiltered = GetAsopJobNamesOfType(jobType);
            string systemShortName = AsopSystemNameToShortPrefixMapping[systemName];

            return jobNamesUnfiltered.Where(c => c.Contains(systemShortName)).ToList();

        } 
    }
}
