﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareClient.Infrastructure.Enums;

namespace UBM.MiddlewareClient.Infrastructure.Names
{
    public class SearchFieldNames
    {
        public const string CUSTOMER_NAME = "Customer Name";
        public const string URN = "URN/Prospect No.";
        public const string JDE_NUMBER = "JDE Number";
        public const string ACTIONED_BY = "Actioned By";
        public const string ORDER_NUMBER = "Contract/Order Number";
        public const string BATCH_NUMBER = "Batch Number";
        public const string RECEIPT_NUMBER = "Receipt Number";
        public const string POSTCODE = "Postcode";
        public const string SENDER = "Sender";
        public const string REASON = "Reason";


        //private static readonly Dictionary<SearchBy, string> Mapping = new Dictionary<SearchBy, string>
        //                                                          {
        //                                                            {SearchBy.CustomerName, CUSTOMER_NAME} ,
        //                                                            {SearchBy.URN, URN},
        //                                                            {SearchBy.JDENumber, JDE_NUMBER},
        //                                                            {SearchBy.ActionedBy, ACTIONED_BY},
        //                                                            {SearchBy.OrderNumber, ORDER_NUMBER},
        //                                                            {SearchBy.BatchNumber, BATCH_NUMBER},
        //                                                            {SearchBy.ReceiptNumber, RECEIPT_NUMBER},
        //                                                            {SearchBy.Postcode, POSTCODE},
        //                                                            {SearchBy.Sender, SENDER},
        //                                                            {SearchBy.Reason, REASON}                                                                    
        //                                                          };

        //public static string GetSearchFieldNameFromEnum(SearchBy searchBy)
        //{
        //    return Mapping[searchBy];
        //}
    }
}
