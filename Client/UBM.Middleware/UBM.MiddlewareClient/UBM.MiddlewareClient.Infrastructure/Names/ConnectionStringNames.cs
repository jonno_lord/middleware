﻿namespace UBM.MiddlewareClient.Infrastructure.Names
{
    public static class ConnectionStringNames
    {
        public const string MIDDLEWARE_COMMON = "MiddlewareCommonConnectionString";
        public const string MIDDLEWARE_SYSTEM = "MiddlewareSystemConnectionString";
        public const string MIDDLEWARE_ASOP_DALTONS = "MiddlewareAsopDaltonsConnectionString";
        public const string MIDDLEWARE_ASOP_UK = "MiddlewareAsopUKConnectionString";
        public const string MIDDLEWARE_ASOP_HOLLAND = "MiddlewareAsopHollandConnectionString";
        public const string MIDDLEWARE_MSI = "MiddlewareMsiConnectionString";
        public const string ASOP_DALTONS = "AsopDaltonsConnectionString";
        public const string ASOP_UK = "AsopUKConnectionString";
        public const string ASOP_HOLLAND = "AsopHollandConnectionString";
    }
}
