﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace UBM.MiddlewareClient.Infrastructure
{
    public static class ConnectionManager
    {
        private static string ValidateConnectionString(string connectionString)
        {
            if(!string.IsNullOrEmpty(connectionString))
            {
                return connectionString;
            }

            throw new ArgumentNullException("connectionString");
        }

        private static List<Connection> connectionsList;

        public static void SetupConnectionManager(IEnumerable<Connection> connections)
        {
            connectionsList = connections.ToList();
            connectionsList.ForEach(c => ValidateConnectionString(c.ConnectionString));
        }

        public static string GetConnectionString(string name)
        {
            Connection connection = connectionsList.Where(c => c.Name == name).FirstOrDefault();
           
            if(connection == null)
                throw new KeyNotFoundException(name + " not found");

            return connection.ConnectionString;
        }

    }
}
