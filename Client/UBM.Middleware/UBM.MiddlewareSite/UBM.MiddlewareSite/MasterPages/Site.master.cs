﻿using System;
using WebsiteFunctions;
using System.Configuration;

namespace MasterPagesSite
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {

        private void SetWebsiteName()
        {
            string value = SetWebsiteProductName.GetWebsiteProductName;

            //set logo text for website product name
            switch (value)
            {
                case "atom":

                    imgMiddlewareLogoText.ImageUrl = "~/Images/atomLogoText.png";
                    imgMiddlewareLogoText.ToolTip = "Atom";

                    break;

                case "middleware":

                    imgMiddlewareLogoText.ImageUrl = "~/Images/MiddlewareLogoText.png";

                    break;
            }

            //set mavigation menu for website product name
            Navigation.navigationProduct = SetWebsiteProductName.GetWebsiteProductName;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if(ConfigurationManager.AppSettings["LoginRequired"] != "No")
            {
                if (Session["username"] == null && !Request.Url.AbsolutePath.ToLower().Contains("login.aspx"))
                    Response.Redirect("/Login/Login.aspx");
            }

            SundryItemsDisplay();

            SetWebsiteProductName setWebsiteProductName = new SetWebsiteProductName();

            //sets product name
            setWebsiteProductName.SetProductName();
            SetWebsiteName();

            /*
            if (IsPostBack)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: summaryDisplayNoAnimation(); ", true);

            }*/


            GetMiddlewareDetails getMiddlewareDetails = new GetMiddlewareDetails();

            imgDetails.ToolTip = getMiddlewareDetails.LoadTooltip();

        }

        /// <summary>
        ///   Displays Date, times and other simple data items.
        /// </summary>
        protected void SundryItemsDisplay()
        {
            litDate.Text = DateTime.Now.ToString("ddddd dd MMMMM yyyy");
        }
    
    }
}
