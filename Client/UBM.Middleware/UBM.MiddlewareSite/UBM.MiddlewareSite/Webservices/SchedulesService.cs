﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Xml.Linq;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareLogging;
using UBM.MiddlewareSystem;

/// <summary>
///   Summary description for SchedulesService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class SchedulesService : WebService
{
    [WebMethod]
    public string CreateScheduleXML()
    {

        try
        {
            List<Schedule> schedules = SystemAccess.getSchedules();

            XDocument xml = new XDocument();
            XElement xschedule = new XElement("schedules");
            foreach (Schedule s in schedules)
            {
                xschedule.Add(new XElement("schedule",
                        new XElement("scheduleid", s.id.ToString()),
                        new XElement("jobid", s.Jobid.ToString()),
                        new XElement("description", s.Description),
                        new XElement("startdate", s.StartDate.ToString()),
                        new XElement("starttime", s.StartTime),
                        new XElement("enddate", s.EndDate.ToString()),
                        new XElement("endtime", s.EndTime),
                        new XElement("monday", s.Monday.ToString()),
                        new XElement("tuesday", s.Tuesday.ToString()),
                        new XElement("wednesday", s.Wednesday.ToString()),
                        new XElement("thursday", s.Thursday.ToString()),
                        new XElement("friday", s.Friday.ToString()),
                        new XElement("saturday", s.Saturday.ToString()),
                        new XElement("sunday", s.Sunday.ToString()),
                        new XElement("frequency", s.Frequency.ToString()),
                        new XElement("disabled", s.Disabled.ToString())));
            }
            xml.Add(xschedule);
            xml.Save("c:\\inetpub\\wwwroot\\middleware\\schedule\\Schedule.xml");
        }
        catch (Exception ex)
        {
            Logger.ErrorLog(ex);
            return "failed to extract:" + ex.Message;
        }

        return "created extraction";

    }

}

