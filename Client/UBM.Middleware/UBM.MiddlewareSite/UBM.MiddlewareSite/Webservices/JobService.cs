﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareLogging;
using UBM.MiddlewareSystem;
using UBM.NotificationClient;

/// <summary>
///   Summary description for Jobs
/// </summary>
[WebService(Namespace = "http://middleware.ubm/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[ScriptService]
public class JobService : WebService
{
    readonly List<Thread> assignedthreads;

    public JobService()
    {
        try
        {
            assignedthreads = (List<Thread>)Application["Threads"];

            // remove all dead threads
            assignedthreads.RemoveAll(x => x.IsAlive == false);

        }
        catch (Exception)
        {
            //-- ultimate sin.
        }
    }

    protected override void Dispose(bool disposing)
    {
        Application["Threads"] = assignedthreads;
        base.Dispose(disposing);
    }

    //[WebMethod]
    //public XmlDocument ConfigurationSetup()
    //{
    //    XmlDocument xdoc = new XmlDocument();
    //    string xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><configuration>";

    //    xmlDoc += "<logginglevel>" + ConfigurationManager.AppSettings["LoggingLevel"] + "</logginglevel>";

    //    foreach (ConnectionStringSettings cn in ConfigurationManager.ConnectionStrings)
    //    {
    //        xmlDoc += "<connection>";
    //        xmlDoc += "<name>" + cn.Name + "</name>";
    //        xmlDoc += "<string>" + cn.ConnectionString + "</string>";
    //        xmlDoc += "</connection>";
    //    }


    //    xmlDoc += "</configuration>";
    //    xdoc.LoadXml(xmlDoc);

    //    return xdoc;

    //}

    [WebMethod]
    public string KillJob(int jobId)
    {
        if (assignedthreads.SingleOrDefault(x => x.Name == "Job_" + jobId.ToString()) == null)
        {
            return "Job not processing";
        }

        try
        {
            Thread jobThread = assignedthreads.SingleOrDefault(x => x.Name == "Job_" + jobId);

            //-- kill the job
            jobThread.Abort();

            //-- make the job as Failed
            ProcessAccess.SetJobToFailed(jobId);

        }
        catch (Exception ex)
        {
            return "Execption occurred:" + ex.Message;
        }

        return "Job " + jobId + " aborted";
    }

    /// <summary>
    ///   Returns a status on the status of Middleware, this is either:
    ///   Red    - There are Failed Jobs
    ///   Orange - Jobs have failed in the past 1 hour
    ///   Green  - Jobs are processing
    ///   White  - No Jobs are processing
    /// </summary>
    /// <returns>xml document containing job status and number of jobs running</returns>
    [WebMethod]
    public XmlDocument GetMiddlewareStatus()
    {

        XmlDocument xdoc = new XmlDocument();

        try
        {
            string status = ProcessAccess.GetMiddlewareStatus();

            int jobsProcessing = 0;

            if (status == "GREEN")
            {
                jobsProcessing = assignedthreads.Count();
            }

            string xmlData = "<middleware>";
            xmlData += "<status>" + status + "</status>";
            xmlData += "<count>" + jobsProcessing + "</count>";
            xmlData += "</middleware>";

            xdoc.LoadXml(xmlData);

        }
        catch (Exception ex)
        {
            ProcessAccess.insertErrorLog(ex.Message, null, "GetMiddlewareStatus", ex.StackTrace);
        }

        return xdoc;

    }

    [WebMethod]
    public XmlDocument JobErrorList()
    {

        XmlDocument xdoc = new XmlDocument();
        string xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><jobs>";

        List<job> failedJobs = SystemAccess.getFailedJobs(DateTime.Today);
        foreach (job jobentry in failedJobs)
        {
            xmlDoc += "<job>";
            xmlDoc += "<id>" + jobentry.id + "</id>";
            xmlDoc += "<description>" + jobentry.ShortName + "</description>";

            system systemEntry = SystemAccess.getSystem(jobentry.Systemid);

            xmlDoc += "<system>" + systemEntry.ShortName + "</system>";
            xmlDoc += "<dated>" + jobentry.LastExecuted + "</dated>";
            xmlDoc += "</job>";
        }

        xmlDoc += "</jobs>";
        xdoc.LoadXml(xmlDoc);

        return xdoc;

    }

    [WebMethod]
    public XmlDocument JobList()
    {
        List<job> jobs = new List<job>();
        jobs = SystemAccess.getJobs();

        XmlDocument xdoc = new XmlDocument();
        string xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><jobs>";
        string format = "dd/MM/yyyy hh:mm:ss";

        foreach (var jobentry in jobs)
        {
            xmlDoc += "<job>";
            xmlDoc += "<id>" + jobentry.id + "</id>";
            xmlDoc += "<shortname>" + jobentry.ShortName + "</shortname>";
            if(jobentry.Disabled)
                xmlDoc += "<status>DISABLED</status>";
            else
                xmlDoc += "<status>" + jobentry.Status + "</status>";
            xmlDoc += "<lastexecuted>" + jobentry.LastExecuted + "</lastexecuted>";
            xmlDoc += "</job>";
  
        }

        xmlDoc += "</jobs>";
        xdoc.LoadXml(xmlDoc);
        return xdoc;
    }


    [WebMethod]
    public XmlDocument JobProcessingList()
    {
        List<Thread> assignedthreads = (List<Thread>)Application["Threads"];
        List<job> jobs = new List<job>();

        XmlDocument xdoc = new XmlDocument();
        string xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><jobs>";

        foreach (Thread tr in assignedthreads)
        {
            xmlDoc += "<job>";
            int jobid = int.Parse(tr.Name.Substring(4));
            job jobentry = SystemAccess.getJob(jobid);
            xmlDoc += "<id>" + jobentry.id + "</id>";
            xmlDoc += "<description>" + jobentry.ShortName + "</description>";
            xmlDoc += "<processid></processid>";
            xmlDoc += "<processdescription></processdescription>";
            xmlDoc += "<system></system>";
            xmlDoc += "<dated>" + DateTime.Now.ToLongDateString() + "</dated>";
            xmlDoc += "</job>";
        }

        xmlDoc += "</jobs>";
        xdoc.LoadXml(xmlDoc);

        return xdoc;

    }

    [WebMethod]
    //-- This Method Starts a Job, but doesn't return back to the caller, until its complete.
    public XmlDocument RunJob(int jobId)
    {

        string xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
        xmlDoc += "<job>";

        StartJob(jobId);
        int counter = 0;
        Thread jobthread = assignedthreads.SingleOrDefault(x => x.Name == "Job_" + jobId.ToString());

        while (jobthread != null)
        {
            if (jobthread.IsAlive == false) break;

            Thread.Sleep(500);
            counter++;
            //-- after 10 minutes, give up
            if (counter > 1200) break;

            jobthread = assignedthreads.SingleOrDefault(x => x.Name == "Job_" + jobId.ToString());

        }

        job jobentry = SystemAccess.getJob(jobId);
        xmlDoc += "<shortname>" + jobentry.ShortName + "</shortname>";
        xmlDoc += "<status>" + jobentry.Status + "</status>";
        xmlDoc += "<id>" + jobentry.id + "</id>";
        xmlDoc += "<lastexecuted>" + jobentry.LastExecuted + "</lastexecuted>";
        xmlDoc += "</job>";

        XmlDocument xdoc= new XmlDocument();
        xdoc.LoadXml(xmlDoc);
        return (xdoc);

    }


    [WebMethod]
    public string StartJob(int jobId)
    {


        try
        {
            if (assignedthreads.SingleOrDefault(x => x.Name == "Job_" + jobId.ToString()) != null)
            {
                return "Job already processing";
            }

            JobThread jobThread = new JobThread(jobId, Server);
            ThreadStart entryStartPoint = jobThread.Execute;

            Thread thread = new Thread(entryStartPoint);
            thread.Name = "Job_" + jobId;
            thread.Start();
            assignedthreads.Add(thread);

            return "Started " + jobId;
        }
        catch (Exception ex)
        {
            if (Application["Threads"] == null)
                Application["Threads"] = new List<Thread>();
            return ex.Message.ToString();
        }

    }

    [WebMethod]
    public string ListTasks()
    {
        string listing = "";
        List<Thread> assignedthreads = (List<Thread>)Application["Threads"];

        foreach (Thread tr in assignedthreads)
        {
            listing += tr.Name + "\r";
        }
        return listing;
    }

    [WebMethod]
    public XmlDocument JobFailedBatches()
    {
        //27/08/2010
        //Get the last 10 failed Batches
        string xmlDoc = "";
        XmlDocument xdoc = new XmlDocument();

        using (SqlConnection connection = new SqlConnection(Connections.GetMiddlewareConnectionString()))
        {

            connection.Open();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_SEL_LastFailedBatches";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = connection;

                xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
                xmlDoc += "<batches>";

                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    xmlDoc += "<batch>";
                    xmlDoc += "<id>" + sdr["id"] + "</id>";
                    xmlDoc += "<shortname>" + Server.HtmlEncode(sdr["ShortName"].ToString()) + "</shortname>";
                    xmlDoc += "<system>" + Server.HtmlEncode(sdr["SystemShortName"].ToString()) + "</system>";
                    xmlDoc += "</batch>";
                }

                xmlDoc += "</batches>";
            }

            xdoc.LoadXml(xmlDoc);
            return xdoc;

        }


    }

    [WebMethod]
    public XmlDocument JobsLastBatches()
    {
        //27/08/2010
        string xmlDoc = "";
        XmlDocument xdoc = new XmlDocument();

        using (SqlConnection connection = new SqlConnection(Connections.GetMiddlewareConnectionString()))
        {

            connection.Open();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_SEL_LastBatches";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = connection;

                xmlDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
                xmlDoc += "<batches>";

                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    xmlDoc += "<batch>";
                    xmlDoc += "<id>" + sdr["id"] + "</id>";
                    xmlDoc += "<shortname>" + Server.HtmlEncode(sdr["ShortName"].ToString()) + "</shortname>";
                    xmlDoc += "<system>" + Server.HtmlEncode(sdr["SystemShortName"].ToString()) + "</system>";
                    xmlDoc += "<status>" + Server.HtmlEncode(sdr["Status"].ToString()) + "</status>";
                    xmlDoc += "</batch>";
                }

                xmlDoc += "</batches>";
            }

            xdoc.LoadXml(xmlDoc);
            return xdoc;

        }

    }


    [WebMethod]
    public void EmailSystemTableDetails(string recipient, string subject, string sql, string parameters)
    {

        using (SqlConnection connection = new SqlConnection(Connections.GetMiddlewareConnectionString()))
        {
            connection.Open();
            using (SqlCommand cmd = new SqlCommand())
            {

                //-- attempt to fill a dataset with the results of the query.
                try
                {
                    cmd.Connection = connection;
                    cmd.CommandText = (sql + " " + parameters).Trim();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);

                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    string htmlBody = DataTableToHTMLTable(ds.Tables[0]);
                    NotificationServiceClient.Email("", recipient, "", subject, htmlBody, Priority.Normal, true);

                }
                catch (Exception ex)
                {
                    Logger.ErrorLog(ex);
                }


            }

        }
    }

    private string DataTableToHTMLTable(DataTable dt)
    {
        StringBuilder sbTop = new StringBuilder();
        sbTop.Append("<table>");
        string bottom = "</table>";
        StringBuilder sb = new StringBuilder();
        //Header        
        sb.Append("<tr>");
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            sb.Append("<td>" + dt.Columns[i].ColumnName + "</td>");
        }
        sb.Append("</tr>");
        //Items        
        for (int x = 0; x < dt.Rows.Count; x++)
        {
            sb.Append("<tr>");
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append("<td>" + dt.Rows[x][i] + "</td>");
            }
            sb.Append("</tr>");
        }
        string sTable = sbTop + sb.ToString() + bottom;
        return sTable;
    }





}

