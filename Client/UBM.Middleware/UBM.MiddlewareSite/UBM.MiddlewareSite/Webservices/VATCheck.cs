﻿using System.Web.Services;
using System.Data.SqlClient;
using UBM.MiddlewareDataAccess;
using System;
using System.Configuration;

/// <summary>
///   Summary description for VATCheck
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class VATCheck : WebService
{
    [WebMethod]
    public string VatCheck(string vatNumber, string countryCode)
    {
        //string result = "service not implemented.";
        // http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl
        UBM.MiddlewareSite.eu.europa.ec.checkVatService cvs = new UBM.MiddlewareSite.eu.europa.ec.checkVatService();
        //eu.europa.ec

        //TODO: check these are populated.
        string proxyServer = ConfigurationManager.AppSettings["ProxyServer"];
        string proxyPort = ConfigurationManager.AppSettings["ProxyPort"];
        string proxyUsername = ConfigurationManager.AppSettings["ProxyUsername"];
        string proxyPassword = ConfigurationManager.AppSettings["ProxyPassword"];


        // set up the proxy server - 
        cvs.Proxy = new System.Net.WebProxy(proxyServer, int.Parse(proxyPort));
        cvs.Proxy.Credentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword);

        bool valid = false;
        string name = "";
        string address = "";
        string result = "";

        try
        {

            int rowCount = 0; //-- returns the number of rows.

            try
            {
                SqlParameter[] parms = new SqlParameter[2];
                parms[0] = new SqlParameter("CountryCode", countryCode);
                parms[1] = new SqlParameter("VatNumber", vatNumber);

                //-- work out if this has already been validated and is ok
                rowCount = GenericCommands.ExecuteScalarStoredProcedure(parms,
                        Connections.GetMiddlewareCommonString(), "usp_GET_ValidVatNumber");
            }
            catch (Exception ex)
            {
                //-- something went wrong trying to query SQL.
                UBM.MiddlewareLogging.Logger.ErrorLog(ex);
            }

            if (rowCount > 0)
            {
                result = "";    //-- This has already been verified, and is valid. no need
                //-- to check the WebService....
            }
            else
            {
                countryCode = countryCode.ToUpper();
                cvs.checkVat(ref countryCode, ref vatNumber, out valid, out name, out address);
                if (!valid)
                    result = "invalid VAT number";
                else
                {
                    SqlParameter[] parms = new SqlParameter[2];
                    parms[0] = new SqlParameter("CountryCode", countryCode);
                    parms[1] = new SqlParameter("VatNumber", vatNumber);

                    //-- store the valid vat number
                    GenericCommands.ExecuteScalarStoredProcedure(parms,
                        Connections.GetMiddlewareCommonString(), "usp_INS_ValidVatNumber");
                }
            }
        }
        catch (Exception ex)
        {
            //-- store the fault.
            string fault = ex.Message;
            if (ex.Message.Contains("INVALID_INPUT")) fault = "Vat Number was in an invalid format (should be CCxxxxxx)";
            if (ex.Message.Contains("MS_UNAVAILABLE")) fault = "VAT validation service for this country is offline";

            result = "Could not validate VAT Number:" + fault;
        }

        return result;

    }

}
