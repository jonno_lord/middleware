﻿Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
function EndRequestHandler(sender, args) {
    if (args.get_error() == undefined) {
        RebindGrid();
        RebindDatePicker();
        RebindToolTips();
    }
}

function RebindGrid() {
    $(document).ready(function () {
        $('#dgrGrid').dataTable({
            "bStateSave": true,
             "bRetrieve": true
        });

    });

    $(document).ready(function () {
        $('#dgrGridGrouped').dataTable({
            "bStateSave": true,
            "bRetrieve": true
        });
    });
}

RebindGrid();

function RebindDatePicker() {
    $(document).ready(function () {
        $("#txtCPUStartDate").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtCPUEndDate").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtDriveStartDate").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtDriveEndDate").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtDate").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtBatchDate").datepicker({ dateFormat: 'yy-mm-dd' });
    });
}

RebindDatePicker();

function RebindToolTips() {

    $(".StatusTooltip[title]").tooltip({

        // tweak the position
        offset: [140, 0],

        // use the "slide" effect
        effect: 'slide'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({ bottom: { direction: 'down', bounce: true} });
}

RebindToolTips();