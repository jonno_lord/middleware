﻿var DisplayedJobs = new Array;
var DisplayedFailedJobs = new Array;
var DisplayedLast10Batches = new Array;
var DisplayedFailedBatches = new Array;


function AjaxJobsRunningSucceeded(data) {

    // remove any entry on the screen that is not in the XML.
    for (var i = 0; i < DisplayedJobs.length; i++) {

        var foundMatch = false;
        var jobid = DisplayedJobs[i];

        $(data).find('job').each(function () {
            if ($(this).find('id').text() == jobid) foundMatch = true;
        });

        if (!foundMatch) {

            DisplayedJobs.splice(i, 1);
            $("#jobRun" + jobid).fadeOut("1000", function () {
                $(this).remove();
            });
        }

    }

    $(data).find('job').each(function () {
        var $job = $(this);

        var id = $job.find('id').text()
        var description = $job.find('description').text()
        var processid = $job.find('processid').text()
        var processdescription = $job.find('processdescription').text()
        var system = $job.find('system').text()
        var duration = $job.find('duration').text()
        var dated = $job.find('dated').text();

        //if id is found in array index greater than -1
        if ($.inArray(id, DisplayedJobs) > -1) {

        }
        else {
            //add id to array
            DisplayedJobs.push(id);
            var jobTitle = (system + " " + description);
            var jobHTML = "<li id='jobRun" + id + "'>" + jobTitle + "</li>";
            $("#jobsRunningOutput ul").prepend(jobHTML);
            $("#jobRun" + id).hide().fadeIn("1000");
        }
    })


}

function AjaxJobsRunningFailed(request, status, error) {
    $('#jobsRunningOutput').html(request.statusText);
}

function AjaxJobsErrorSucceeded(data) {

    // remove any entry on the screen that is not in the XML.
    for (var i = 0; i < DisplayedFailedJobs.length; i++) {

        var foundMatch = false;
        var jobid = DisplayedFailedJobs[i];
        $(data).find('job').each(function () {
            if ($(this).find('id').text() == jobid) foundMatch = true;
        });

        if (!foundMatch) {

            DisplayedFailedJobs.splice(i, 1);
            //get it to fadeout
            $("#jobFail" + jobid).fadeOut("1000", function () {
                $(this).remove();
            });
        }

    }


    $(data).find('job').each(function () {
        var $job = $(this);

        var id = $job.find('id').text()
        var description = $job.find('description').text()
        var system = $job.find('system').text()
        var dated = $job.find('dated').text();

        //if id is found in array index greater than -1
        if ($.inArray(id, DisplayedFailedJobs) > -1) {

        }
        else {
            //add id to array
            DisplayedFailedJobs.push(id);
            var jobTitle = (system + " " + description);
            var jobHTML = "<li id='jobFail" + id + "'>" + jobTitle + "</li>";
            $("#jobsFailedOutput ul").prepend(jobHTML);
            $("#jobFail" + id).hide().fadeIn("1000");
        }
    })


}

function AjaxJobsErrorFailed(request, status, error) {
    $('#jobsFailedOutput').html(request.statusText);
}

function AjaxLast10Succeeded(data) {

    // remove any entry on the screen that is not in the XML.
    for (var i = 0; i < DisplayedLast10Batches.length; i++) {

        var foundMatch = false;
        var batchid = DisplayedLast10Batches[i];

        $(data).find('batch').each(function () {
            if ($(this).find('id').text() == batchid) foundMatch = true;
        });

        if (!foundMatch) {

            DisplayedLast10Batches.splice(i, 1);
            $("#batchLast10" + batchid).fadeOut("1000", function () {
                $(this).remove();
            });
        }

    }


    $(data).find('batch').each(function () {
        var $batch = $(this);

        var id = $batch.find('id').text()
        var shortName = $batch.find('shortname').text()
        var system = $batch.find('system').text()
        var status = $batch.find('status').text()

        //if id is found in array index greater than -1
        if ($.inArray(id, DisplayedLast10Batches) > -1) {

        }
        else {
            //add id to array
            DisplayedLast10Batches.push(id);
            var batchTitle = (id + " " + shortName);
            var batchHTML = "<li id='batchLast10" + id + "'><a href='batchdetails.aspx?id=" + id + "'>" + batchTitle + "</a></li>";
            $("#jobsLastBatchesOutput ul").prepend(batchHTML);
            $("#batchLast10" + id).hide().fadeIn("1000");
        }
    })


}

function AjaxLast10Failed(request, status, error) {
    $('#jobsLastBatchesOutput').html(request.statusText);
}

function AjaxBatchesSucceeded(data) {

    // remove any entry on the screen that is not in the XML.
    for (var i = 0; i < DisplayedFailedBatches.length; i++) {

        var foundMatch = false;
        var batchid = DisplayedFailedBatches[i];
        //REWRITE THIS to find ids
        $(data).find('batch').each(function () {
            if ($(this).find('id').text() == batchid) foundMatch = true;
        });

        if (!foundMatch) {

            DisplayedFailedBatches.splice(i, 1);
            //get it to fadeout
            $("#batches" + batchid).fadeOut("1000", function () {
                $(this).remove();
            });
        }

    }

    $(data).find('batch').each(function () {
        var $batch = $(this);

        var id = $batch.find('id').text()
        var jobName = $batch.find('shortname').text()
        var system = $batch.find('system').text()

        //if id is found in array index greater than -1
        if ($.inArray(id, DisplayedFailedBatches) > -1) {

        }
        else {
            //add id to array
            DisplayedFailedBatches.push(id);
            var batchTitle = (id + " " + jobName);
            var batchHTML = "<li id='batches" + id + "'><a href='batchdetails.aspx?id=" + id + "'>" + batchTitle + "</a></li>";
            $("#batchesLast10Output ul").prepend(batchHTML);
            $("#batches" + id).hide().fadeIn("1000");
        }
    })

}

function AjaxBatchesFailed(request, status, error) {
    $('#batchesLast10Output').html(request.statusText);
}

$(document).ready(function () {
    var webRunningMethod = '/Webservices/JobsService.asmx/JobProcessingList'

    setInterval(function () {
        $.ajax({
            type: "GET",
            url: webRunningMethod,
            contentType: "text/xml; charset=utf-8",
            dataType: "xml", 
            success: AjaxJobsRunningSucceeded,
            failure: AjaxJobsRunningFailed
        });
    }, 1000);


    var webFailedMethod = '/Webservices/JobsService.asmx/JobErrorList'

    setInterval(function () {
        $.ajax({
            type: "GET",
            url: webFailedMethod,
            contentType: "text/xml; charset=utf-8",
            dataType: "xml", 
            success: AjaxJobsErrorSucceeded,
            failure: AjaxJobsErrorFailed
        });
    }, 1000);

    var webLast10Method = '/Webservices/JobsService.asmx/JobsLastBatches'

    setInterval(function () {
        $.ajax({
            type: "GET",
            url: webLast10Method,
            contentType: "text/xml; charset=utf-8",
            dataType: "xml", 
            success: AjaxLast10Succeeded,
            failure: AjaxLast10Failed
        });
    }, 1000);

    var webBatchFailMethod = '/Webservices/JobsService.asmx/JobFailedBatches'

    setInterval(function () {
        $.ajax({
            type: "GET",
            url: webBatchFailMethod,
            contentType: "text/xml; charset=utf-8",
            dataType: "xml", 
            success: AjaxBatchesSucceeded,
            failure: AjaxBatchesFailed
        });
    }, 1000);

});
