﻿currentlyExecutingJob = '';

function updateGrid() {
    var WebSvurl = '/Webservices/JobsService.asmx/JobList'

    $.ajax({
        type: "GET",
        url: WebSvurl,
        contentType: "text/xml; charset=utf-8",
        dataType: "xml",
        success: function (data) {
            $(data).find('job').each(function () {
                var $job = $(this);
                displayJob($job);
            })
        }
    });

}

function RunJob(id) {

    var WebSvurl = '/Webservices/JobsService.asmx/RunJob'
    var JobId = '?Jobid=' + id

    currentlyExecutingJob = id;

    displayProcessing(id);

    $.ajax({
        type: "GET",
        url: WebSvurl + JobId,
        contentType: "text/xml; charset=utf-8",
        dataType: "xml",
        success: function (data) {
            currentlyExecutingJob = '';
            displayJob(data);
        }
    });

}


function displayJob(data) {
    var Status = $(data).find('status').text();
    var lastExecuted = $(data).find('lastexecuted').text();
    var id = $(data).find('id').text();

    if (id == currentlyExecutingJob) return;

    $("#imgStatus_" + id).attr('src', '/images/jobStatus/' + Status + '.png');
    $("#lblJobstatus_" + id).html(Status);
    $("#lblLastExecuted_" + id).html(lastExecuted);

    if (Status != 'PROCESSING') {
        $("#btnRunJob_" + id).css('display', 'block');
        $("#imgWait_" + id).css('display', 'none');
    }
    else {
        displayProcessing(id);
    }
}

function displayProcessing(id) {

    $("#btnRunJob_" + id).css('display', 'none');
    $("#imgWait_" + id).css('display', 'block');
    $("#lblJobstatus_" + id).html('PROCESSING');
    $("#imgStatus_" + id).attr('src', '/images/jobStatus/processing.png');
}

$(document).ready(function () {


    var webRunningMethod = '/Webservices/JobsService.asmx/GetMiddlewareStatus'

    getStatus();

    setInterval(getStatus, 1000);

    function getStatus() {
        $.ajax({
            type: "GET",
            url: webRunningMethod,
            contentType: "text/xml; charset=utf-8",
            dataType: "xml", //for Firefox change this to "jsonp" 
            success: StatusRecieved

        });
    }

    //Get the Status and the job count
    function StatusRecieved(data) {

        $(data).find('middleware').each(function () {

            var $middleware = $(this);
            var Status = $middleware.find('status').text()
            var Count = $middleware.find('count').text()

            $('.MiddlewareJobStatus').html(Count);
            $('.MiddlewareJobStatus').css("background-image", "url(/Images/Alert" + Status + ".png)");
            $('.MiddlewareJobStatus').css("background-repeat", "no-repeat");

            $('.MiddlewareDashboardStatus').css("background-image", "url(/Images/Alert" + Status + "Small.png)");
            $('.MiddlewareDashboardStatus').css("background-repeat", "no-repeat");

            $('.MiddlewareStatusText').html(Status);
            $('.JobCount').html(Count);
        });
    }

});