﻿//whether summary is expanded or not
var expanded = false;
var fExpanded = false;
var printPage = false;

//On Hover Over
function megaHoverOver() {
    $(this).find(".sub").stop().fadeTo('fast', 1).show(); //Find sub and fade it in
}

//On Hover Out
function megaHoverOut() {
    $(this).find(".sub").stop().fadeTo('fast', 0, function () { //Fade to 0 opactiy
        $(this).hide();  //after fading, hide it
    });
}

function summaryDisplayNoAnimation() {

    $(".toggle img").attr("src", "../../../images/DropUp.png");
    $('#summaryon').show();

    //prevents toggle causing repeat action
    expanded = true;
}

//removes standard stylesheet and puts printer style in place
function printerView() {

    $('link[rel=stylesheet]').remove();
    $('head').append('<link rel="stylesheet" href="../../../Styles/PrintSheet.css" type="text/css" />');
    printPage = true;

}

//gets current time
function clock() {

    var now = new Date();

    var hours = now.getHours();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();

    //appends 0 to front of string if less than 10 to behave like standard format
    if (now.getHours() < 10) {
        
         hours = '0' + now.getHours()
     }

     if (now.getMinutes() < 10) {

         minutes = '0' + now.getMinutes()
     }

     if (now.getSeconds() < 10) {

         seconds = '0' + now.getSeconds()
     }

    var timeString = hours + ':' + minutes + ':' + seconds;
    document.getElementById('time').innerHTML = timeString;
    setTimeout('clock()', 1000);

}

$(document).ready(function () {

    clock();

    var config = {
        sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)
        interval: 100, // number = milliseconds for onMouseOver polling interval
        over: megaHoverOver, // function = onMouseOver callback (REQUIRED)
        timeout: 500, // number = milliseconds delay before onMouseOut
        out: megaHoverOut // function = onMouseOut callback (REQUIRED)
    };

    $("ul#topnav li .sub").css({ 'opacity': '0' }); //Fade sub nav to 0 opacity on default 
    $("ul#topnav li").hoverIntent(config); //Trigger Hover intent with custom configurations

    $(".StatusTooltip[title]").tooltip({

        // tweak the position
        offset: [140, 0],

        // use the "slide" effect
        effect: 'slide'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({ bottom: { direction: 'down', bounce: true} });


    $("#imgDetails[title]").tooltip({

        // tweak the position
        offset: [140, 50],

        // use the "slide" effect
        effect: 'slide'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({ bottom: { direction: 'down', bounce: true} });


     $('#dgrGrid').dataTable(
     {
         "bStateSave": true,
         "bRetrieve": true
     });

    $('#dgrGridGrouped').dataTable(
    {
        "bStateSave": true,
        "bRetrieve": true
    });

    $('.toggle').click(function () {

        if (!expanded) {
            $(".toggle img").attr("src", "../../../images/DropUp.png");
            $('#summaryon').slideDown();
        }
        else {
            $(".toggle img").attr("src", "../../../images/DropDown.png");
            $("#summaryon").css('height', '170px');
            $("#summaryon").slideUp();
        }
        expanded = !expanded;
    });


    $('.toggleFilter').click(function () {

        if (!fExpanded) {
            $(".toggleFilter img").attr("src", "../../../images/filter_iconout.png");
            $('#outerFilter').slideDown();
        }
        else {
            $(".toggleFilter img").attr("src", "../../../images/filter_icon.png");
            $("#outerFilter").css('height', '35px');
            $("#outerFilter").slideUp();
        }
        fExpanded = !fExpanded;
    });



});