﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="UBM.MiddlewareSite.Login.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" runat="server">
    <div style="padding-bottom:20px">&nbsp;</div>
<div class="break"></div>
<div style="width:400px;margin-left:200px;margin-bottom:120px;background:#eee;border:1px solid #ccc;padding:50px">
<p style="width:100px;float:left;margin:0px">User Name</p><span style="float:left;width:200px">
    <asp:TextBox ID="txtUserName" runat="server" Width="200px"></asp:TextBox></span>
<div style="clear:left;height:8px;">&nbsp;</div>
<p style="width:100px;float:left;margin:0px">Password</p><span style="float:left;width:200px">
    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="200px"></asp:TextBox></span>
<div class="break"></div>
<div style="height:20px;"></div>
<p style="width:100px;float:left"></p><span style="float:left;width:200px">
    <asp:Button ID="btnLogin" runat="server" text="Login to Middleware" 
        onclick="btnLogin_Click"></asp:Button></span>
    <div style="clear:left;height:8px;">&nbsp;</div>
    <asp:Label id="lblMessage" runat="server" CssClass="boldLabel"></asp:Label>
</div>

<div style="padding-bottom:10px">&nbsp;</div>

</asp:Content>
