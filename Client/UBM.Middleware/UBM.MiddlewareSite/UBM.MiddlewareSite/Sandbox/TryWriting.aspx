﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TryWriting.aspx.cs" Inherits="UBM.MiddlewareSite.Sandbox.TryWriting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtFile" runat="server" Width="400px" Text="\\DEVMWR01\MoretonSmith\Credit_Status\Unprocessed\test.txt" ></asp:TextBox>
        <asp:Button ID="btnWrite" runat="server" Text="Write to text file" onclick="btnWrite_Click" />
        <asp:Literal ID="litResults" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
