﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="UBM.MiddlewareSite.Sandbox.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="background:#aaa;width:640px;padding:20px">
    <h2>ASOP UK</h2>
        <asp:Button ID="btnReceipts" runat="server" Text="ASOP UK Receipts" 
            onclick="btnAsopUKReceipts_Click" />
        <asp:Button ID="btnReceiptCopies" runat="server" Text="Receipts Copies" 
            onclick="btnReceiptCopies_Click" />
        <asp:Button ID="btnReceiptsDaltons" runat="server" Text="ASOP Daltons Receipts" 
            onclick="btnReceiptsDaltons_Click" />
         <asp:Button ID="btnEventsforce" runat="server" Text="Eventsforce" 
            onclick="btnEventsforce_Click" />
    </div>
    <div style="background:#ccc;width:640px;padding:20px">
    <h2>Moreton Smith</h2>
        <asp:Button ID="CreateForm" runat="server" Text="CreateForm Export" 
            Width="198px" onclick="CreateForm_Click" />
        <asp:Button ID="DiaryNotes" runat="server" Text="Diary Notes Import" 
            Width="198px" onclick="DiaryNotes_Click" />
        <asp:Button ID="invoiceFolder" runat="server" Text="Invoice Export" 
            Width="198px" onclick="invoiceFolder_Click" />
        <br />
        <asp:Button ID="paymentsFolder" runat="server" Text="Payments Import" 
            Width="198px" onclick="paymentsFolder_Click" />
        <asp:Button ID="EventsForce" runat="server" style="margin-bottom: 0px" 
            Text="EventsForce PDF Export" Width="198px" onclick="EventsForce_Click" />
        <asp:Button ID="AdvertisingFolder" runat="server" Text="Advertising Extract" 
            Width="198px" onclick="AdvertisingFolder_Click" />
        <asp:Button ID="creditStatus" runat="server" Text="Credit Status Feed" 
            Width="198px" onclick="creditStatus_Click" />
         <asp:Button ID="btnOBIA" runat="server" Text="OBIA Import" 
            Width="198px" onclick="btnOBIA_Click" />
        <asp:Button ID="btnPromisesDisputes" runat="server" Text="Promises/Disputes Import" 
            Width="198px" onclick="btnPromisesDisputes_Click" />
        <br />
        <asp:Button ID="btnDailyAllocations" runat="server" Text="DailyAllocations" 
            Width="198px" onclick="btnDailyAllocations_Click" />
        <br />
        <br />
        <asp:Button ID="ASPACmsiCreateForm" runat="server" Text="ASPAC CreateForm Export" 
            Width="198px" onclick="ASPACmsiCreateForm_Click" />
        <asp:Button ID="ASPACmsiDiaryNotes" runat="server" Text="ASPAC Diary Notes Import" 
            Width="198px" onclick="ASPACmsiDiaryNotes_Click" />
        <asp:Button ID="ASPACinvoiceExport" runat="server" Text="ASPAC Invoice Export" 
            Width="198px" onclick="ASPACinvoiceExport_Click" />
        <br />
        <asp:Button ID="ASPACpaymentsFolder" runat="server" Text="ASPAC Payments Import" 
            Width="198px" onclick="ASPACpaymentsFolder_Click" />
        <asp:Button ID="ASPACCreditStatusFeed" runat="server" style="margin-bottom: 0px" 
            Text="ASPAC Credit Status Feed" Width="198px" 
            onclick="ASPACCreditStatusFeed_Click" />

        <asp:Button ID="btnPayments" runat="server" style="margin-bottom: 0px" 
            Text="Integrity Check" Width="198px" onclick="btnPayments_Click" 
            />

        <br />
        <br />

        <asp:Button ID="btnSendReport" runat="server" style="margin-bottom: 0px" 
            Text="Send Report" Width="198px" onclick="btnSendReport_Click" 
            />

        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
            Text="Post Contact to N200" Width="197px" />
        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
            Text="Get Events from N200" Width="197px" />

        <asp:Button ID="btnESOPWebfeed" runat="server" OnClick="btnESOPWebfeed_Click" Text="ESOP Webfeed" Width="198px" />

        <asp:Button ID="btnExhibitorkit" runat="server" OnClick="btnExhibitorkit_Click" Text="Exhibiorkit extract" Width="198px" />

        <asp:Button ID="btnImportPDF" runat="server" OnClick="btnImportPDF_Click" Text="Import PDFs" Width="197px" />

        <br />
        <br />

        <asp:Button ID="EventsforceCustomerFF" runat="server" style="margin-bottom: 0px" 
            Text="Eventsforce Customer FF" Width="198px" OnClick="EventsforceCustomerFF_Click" />
        <asp:Button ID="EventsforceInvoiceFF" runat="server" style="margin-bottom: 0px" 
            Text="Eventsforce Invoice FF" Width="198px" OnClick="EventsforceInvoiceFF_Click" />
        <asp:Button ID="EventsforcePaymentBF" runat="server" style="margin-bottom: 0px"
            Text="Eventsforce Payment BF" Width="199px" OnClick="EventsforcePaymentBF_Click" />

    </div>
    </form>
</body>
</html>
