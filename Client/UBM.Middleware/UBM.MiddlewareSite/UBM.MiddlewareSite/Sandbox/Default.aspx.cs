﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UBM.MiddlewareASOPUK.AR;
using UBM.MiddlewareASOPDaltons.AR;
using UBM.MiddlewareReceipts;
using UBM.MiddlewareMoretonSmith;
using UBM.MiddlewareMoretonSmith.Feeds;
using UBM.MiddlewareN200.Feeds;

namespace UBM.MiddlewareSite.Sandbox
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAsopUKReceipts_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareASOPUK.AR.MiddlewareToMiddlewareIn(), jobid: 34, processid: 60);
        }

        protected void CreateForm_Click(object sender, EventArgs e)
        {
            FireJob(new EventsForceExport(), jobid: 104, processid: 198);

            FireJob(new CARSExport(), jobid: 104, processid: 203);
        }

        private void FireJob(UBM.MiddlewareProcess.ProcessTask process, int jobid, int processid)
        {
            process.Jobid = jobid;
            process.Processid = processid;
            process.Server = Server;
            process.ParseConfiguration();
            process.Execute();
        }

        protected void DiaryNotes_Click(object sender, EventArgs e)
        {
            FireJob(new DiaryNotesImport(), jobid: 103, processid: 231);
        }

        protected void invoiceFolder_Click(object sender, EventArgs e)
        {
            FireJob(new InvoicesExport(), jobid: 101, processid: 195);
        }

        protected void paymentsFolder_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Payments.PaymentsImport(), jobid: 102, processid: 196);
            //FireJob(new UBM.MiddlewareMoretonSmith.Payments.MiddlewareToMiddlewareIn(), jobid: 148, processid: 201);
        }

        protected void EventsForce_Click(object sender, EventArgs e)
        {
            //FireJob(new UBM.MiddlewareMoretonSmith.Feeds.EventsForceImport(), jobid: 104, processid: 202);

            FireJob(new EventsForceExport(), jobid: 104, processid: 198);

            FireJob(new CARSExport(), jobid: 104, processid: 203);
        }

        protected void AdvertisingFolder_Click(object sender, EventArgs e)
        {
            FireJob(new AdvertisingExport(), jobid: 150, processid: 193);
        }

        protected void creditStatus_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.CreditStatus.CreditStatusImport(), jobid: 109, processid: 205);   
        }

        protected void ASPACmsiCreateForm_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Feeds.ASPAC.CreateFormExport(), jobid: 152, processid: 196);
        }

        protected void ASPACmsiDiaryNotes_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Feeds.ASPAC.DiaryNotesImport(), jobid: 106, processid: 200);
        }

        protected void ASPACinvoiceExport_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Feeds.ASPAC.InvoicesExport(), jobid: 153, processid: 197);
        }

        protected void ASPACpaymentsFolder_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Payments.ASPAC.SourceToMiddleware(), jobid: 156, processid: 204);
        }

        protected void ASPACCreditStatusFeed_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.CreditStatus.ASPAC.SourceToMiddleware(), jobid: 154, processid: 198);
        }

        protected void btnReceiptCopies_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareReceipts.Receipts.PrintCopy(), jobid: 145, processid: 207);
        }

        protected void btnReceiptsDaltons_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareASOPDaltons.AR.MiddlewareToMiddlewareIn(), jobid: 107, processid: 106);
        }

        protected void btnPayments_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Integrity.CheckConfiguration(), jobid: 90, processid: 178);
        }

        protected void btnSendReport_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Feeds.ReportSender(), jobid: 102, processid: 230);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareN200.Feeds.MiddlewareToN200(), jobid: 98, processid: 192  );
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareN200.Feeds.N200ToMiddleware(), jobid: 99, processid: 193);
        }

        protected void btnESOPWebfeed_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareESOP.WebFeed.MiddlewareToSource(), jobid: 100, processid: 194);
        }

        protected void btnOBIA_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.OBIA.OBIAImport(), jobid: 108, processid: 204);
        }

        protected void btnPromisesDisputes_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Feeds.PromisesDisputes(), jobid: 107, processid: 201);
        }

        protected void btnDailyAllocations_Click(object sender, EventArgs e)
        {
            FireJob(new UBM.MiddlewareMoretonSmith.Feeds.GenericExport(), jobid: 117, processid: 255);
        }

        protected void btnExhibitorkit_Click(object sender, EventArgs e)
        {
            FireJob(new MiddlewareN200.Feeds.ExhibitorkitToMiddleware(), jobid: 98, processid: 256);
        }

        protected void btnEventsforce_Click(object sender, EventArgs e)
        {
            FireJob(new MiddlewareEventsForce.AR.SourceToMiddleware(), jobid: 55, processid: 110);
        }

        protected void btnImportPDF_Click(object sender, EventArgs e)
        {
            FireJob(new MiddlewareMoretonSmith.Feeds.InvoicePDFImport(), jobid: 118, processid: 257);
        }

        protected void EventsforceCustomerFF_Click(object sender, EventArgs e)
        {
            FireJob(new MiddlewareEventsForce.Customer.SourceToMiddleware(), jobid: 53, processid: 104);
        }

        protected void EventsforceInvoiceFF_Click(object sender, EventArgs e)
        {
            FireJob(new MiddlewareEventsForce.AR.SourceToMiddleware(), jobid: 55, processid: 110);
        }

        protected void EventsforcePaymentBF_Click(object sender, EventArgs e)
        {
            FireJob(new MiddlewareEventsForce.Payments.MiddlewareToSource(), jobid:4, processid: 211);
        }
    }
}