﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UBM.MiddlewareSite.Sandbox
{
    public partial class TryWriting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnWrite_Click(object sender, EventArgs e)
        {
            string path = txtFile.Text;

            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path);
                file.WriteLine("test data");
                file.Close();
            }
            catch (Exception ex)
            {
                litResults.Text = ex.Message.ToString();
            }

            litResults.Text = "Complete";

        }
    }
}