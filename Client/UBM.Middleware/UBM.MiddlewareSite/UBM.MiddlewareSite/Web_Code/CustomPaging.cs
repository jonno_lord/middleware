﻿using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace WebsiteFunctions
{
    /// <summary>
    ///   Includes methods required for custom paging for specific gridviews through objectdatasources. Supplies total number of records to return and the dataset as an object to the objectdatasource to utilise
    /// </summary>
    public class CustomPaging
    {
        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        /// <summary>
        ///   returns total number of customers, filtered by any values inputted by the user
        /// </summary>
        /// <param name = "batchNo">batch number filter value</param>
        /// <param name = "system">system filter value</param>
        /// <param name = "sourceURN">source URN filter value</param>
        /// <param name = "name">name filter value</param>
        /// <param name = "surname">surname filter value</param>
        /// <param name = "company">company filter value</param>
        /// <param name = "product">product filter value</param>
        /// <param name = "address">address filter value</param>
        /// <param name = "postcode">postcode filter value</param>
        /// <returns>total number of customers</returns>
        public int getTotalCustomers(string batchNo, string system, string sourceURN, string name, string surname, string company, string product, string address, string postcode)
        {
            int totalCustomers;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_CustomerCount", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@batchNumber", batchNo);
                command.Parameters.AddWithValue("@systemName", system);
                command.Parameters.AddWithValue("@sourceURN", sourceURN);
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@surname", surname);
                command.Parameters.AddWithValue("@company", company);
                command.Parameters.AddWithValue("@product", product);
                command.Parameters.AddWithValue("@address", address);
                command.Parameters.AddWithValue("@postcode", postcode);

                totalCustomers = int.Parse(command.ExecuteScalar().ToString());
            }
            return totalCustomers;
        }

        /// <summary>
        ///   returns total number of orders, filtered by any values inputted by the user
        /// </summary>
        /// <param name = "batchNo">batch number filter value</param>
        /// <param name = "system">system filter value</param>
        /// <param name = "sourceURN">source URN filter value</param>
        /// <param name = "orderNo">order number filter value</param>
        /// <param name = "lineItemNo">line item number filter value</param>
        /// <param name = "transType">transaction type filter value</param>
        /// <param name = "transValue">transaction value filter value</param>
        /// <param name = "product">product filter value</param>
        /// <param name = "date">date filter value</param>
        /// <returns>total number of orders</returns>
        public int getTotalOrders(string batchNo, string system, string sourceURN, string orderNo, string lineItemNo, string transType, string transValue, string product, string date)
        {
            int totalOrders;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_OrderCount", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@batchNumber", batchNo);
                command.Parameters.AddWithValue("@systemName", system);
                command.Parameters.AddWithValue("@sourceURN", sourceURN);
                command.Parameters.AddWithValue("@orderNo", orderNo);
                command.Parameters.AddWithValue("@lineItemNo", lineItemNo);
                command.Parameters.AddWithValue("@transType", transType);
                command.Parameters.AddWithValue("@transValue", transValue);
                command.Parameters.AddWithValue("@product", product);
                command.Parameters.AddWithValue("@date", date);

                totalOrders = int.Parse(command.ExecuteScalar().ToString());
            }
            return totalOrders;
        }

        /// <summary>
        ///   Retieves a subset of customers as a dataset, based on filters
        /// </summary>
        /// <param name = "batchNo">batch number filter value</param>
        /// <param name = "system">system filter value</param>
        /// <param name = "sourceURN">source URN filter value</param>
        /// <param name = "name">name filter value</param>
        /// <param name = "surname">surname filter value</param>
        /// <param name = "company">company filter value</param>
        /// <param name = "product">product filter value</param>
        /// <param name = "address">address filter value</param>
        /// <param name = "postcode">postcode filter value</param>
        /// <param name = "maximumRows">the amount of rows to return</param>
        /// <param name = "startRowIndex">the index to start returning rows from</param>
        /// <returns>Dataset of stored procedure results</returns>
        public DataSet GetCustomers(string batchNo, string system, string sourceURN, string name, string surname, string company, string product, string address, string postcode, int maximumRows, int startRowIndex)
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_CustomersPaged", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sda = new SqlDataAdapter(command);

                command.Parameters.AddWithValue("@batchNumber", batchNo);
                command.Parameters.AddWithValue("@systemName", system);
                command.Parameters.AddWithValue("@sourceURN", sourceURN);
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@surname", surname);
                command.Parameters.AddWithValue("@company", company);
                command.Parameters.AddWithValue("@product", product);
                command.Parameters.AddWithValue("@address", address);
                command.Parameters.AddWithValue("@postcode", postcode);

                command.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                command.Parameters.AddWithValue("@maximumRows", maximumRows);

                DataSet ds = new DataSet();
                sda.Fill(ds);

                return ds;

            }
        }

        /// <summary>
        ///   Retieves a subset of orders as a dataset, based on filters
        /// </summary>
        /// <param name = "batchNo">batch number filter value</param>
        /// <param name = "system">system filter value</param>
        /// <param name = "sourceURN">source URN filter value</param>
        /// <param name = "orderNo">order number filter value</param>
        /// <param name = "lineItemNo">line item number filter value</param>
        /// <param name = "transType">transaction type filter value</param>
        /// <param name = "transValue">transaction value filter value</param>
        /// <param name = "product">product filter value</param>
        /// <param name = "date">date filter value</param>
        /// <param name = "maximumRows">the amount of rows to return</param>
        /// <param name = "startRowIndex">the index to start returning rows from</param>
        /// <returns>Dataset of stored procedure results</returns>
        public DataSet GetOrders(string batchNo, string system, string sourceURN, string orderNo, string lineItemNo, string transType, string transValue, string product, string date, int maximumRows, int startRowIndex)
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_OrdersPaged", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sda = new SqlDataAdapter(command);

                command.Parameters.AddWithValue("@batchNumber", batchNo);
                command.Parameters.AddWithValue("@systemName", system);
                command.Parameters.AddWithValue("@sourceURN", sourceURN);
                command.Parameters.AddWithValue("@orderNo", orderNo);
                command.Parameters.AddWithValue("@lineItemNo", lineItemNo);
                command.Parameters.AddWithValue("@transType", transType);
                command.Parameters.AddWithValue("@transValue", transValue);
                command.Parameters.AddWithValue("@product", product);
                command.Parameters.AddWithValue("@date", date);

                command.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                command.Parameters.AddWithValue("@maximumRows", maximumRows);

                DataSet ds = new DataSet();
                sda.Fill(ds);

                return ds;
            }
        }
    }
}