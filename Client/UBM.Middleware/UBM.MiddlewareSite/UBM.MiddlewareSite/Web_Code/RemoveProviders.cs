﻿namespace WebsiteFunctions
{
    /// <summary>
    ///   Removes the provider keyword from connectionstring supplied
    /// </summary>
    public class RemoveProviders
    {
        public static string RemoveProvider(string connString)
        {
            connString = connString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connString = connString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connString;
        }
    }
}