﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace WebsiteFunctions
{
    public class GetMiddlewareDetails
    {
        private static SqlConnection connection;

        private static readonly string connectionString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;

        public object GetJobCount() 
        {
            //Return the number total number of jobs
            object retVal;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Count(id) From [Jobs]", connection);
                command.CommandType = CommandType.Text;
                retVal = command.ExecuteScalar();
                return retVal;
                
            }

        }


        //Get the total number of processes running
        public object GetTotalProcesses() 
        {
            //Return the number total number of running Processes
            object retVal;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Count(id) From [Processes] Where Status = 'RUNNING'", connection);
                command.CommandType = CommandType.Text;
                retVal = command.ExecuteScalar();
                return retVal;
            }

        }


        //Get the number of failed jobs
        public object GetJobsDisabled() 
        {
            //Return the number total number of Failed Jobs
            object retVal;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Count(id) From [Jobs] Where Status = 'FAILED'", connection);
                command.CommandType = CommandType.Text;
                retVal = command.ExecuteScalar();
                return retVal;
            }
        }

        /// <summary>
        ///   Gets the last Execution Time of the most recent middleware batch
        ///   added by E Hamlet
        /// </summary>
        /// <returns>Last execution time </returns>
        public object GetLastExecutionTime()
        {
            object executionTime;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select MAX(StartDate) FROM Batches", connection);
                command.CommandType = CommandType.Text;
                executionTime = command.ExecuteScalar();
                return executionTime;
            }

        }

        /// <summary>
        ///   Gets a count of all job batches with an end date  of today and status of completed
        ///   added by E Hamlet
        /// </summary>
        /// <returns>object containing number of jobs completed today</returns>
        public object GetCompleteJobs()
        {
            object completedJobs;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select COUNT(id) FROM Batches WHERE Status =  'COMPLETE' AND EndDate >= Convert(datetime, Convert(varchar, GetDate(), 101))", connection);
                command.CommandType = CommandType.Text;
                //command.Parameters.AddWithValue("@datetime", System.DateTime.Now);
                completedJobs = command.ExecuteScalar();
                return completedJobs;
            }

        }

        ///// <summary>
        ///// Gets percentage of successful job runs today
        ///// added by E Hamlet
        ///// </summary>
        ///// <returns></returns>
        //public object GetSuccessJobs()
        //{
        //    object successJobs;

        //    using (connection = new SqlConnection(connectionString))
        //    {
        //        connection.Open();
        //        SqlCommand command = new SqlCommand("Select COUNT(id) FROM Batches WHERE Status =  'Completed' AND EndDate >= Convert(datetime, Convert(varchar, GetDate(), 101))", connection);
        //        command.CommandType = CommandType.Text;
        //        successJobs = command.ExecuteScalar();
        //        return successJobs;
        //    }

        //}

        public string LoadTooltip()
        {
            //28/01/2011 - BMP Stat Tooltip
            //Adds Middleware data to Stat Tooltip when user hovers mouse over
            SqlConnection cn = new SqlConnection(connectionString);

            string serverName;
            string enviroName;
            object jobCount;
            object procCount;
            object failedJobsCount;

            //Set the server name
            serverName = cn.DataSource;

            //Get the environment name
            enviroName = ConfigurationManager.AppSettings["CurrentEnvironment"];

            GetMiddlewareDetails getMiddlewareDetails = new GetMiddlewareDetails();

            //Jobs Defined
            jobCount = getMiddlewareDetails.GetJobCount();

            //Running Processes
            procCount = getMiddlewareDetails.GetTotalProcesses();

            //Jobs Failed
            failedJobsCount = getMiddlewareDetails.GetJobsDisabled();


            string toolTip = "Server: " + serverName + "<br />" + enviroName + "<br />" + jobCount + " Jobs Defined" + "<br />" + procCount + " Processes" + "<br />" + failedJobsCount + " Failed jobs";

            return toolTip;
        }

    }
}