﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareLogging;
using UBM.MiddlewareProcess;
using UBM.MiddlewareSyncRefresher;
using UBM.MiddlewareSystem;
using UBM.NotificationClient;

/// <summary>
///   Summary description for JobThread
/// </summary>
public class JobThread
{
    private readonly HttpServerUtility Server;

    public JobThread(int jobid, HttpServerUtility server)
    {
        JobId = jobid;
        Server = server;
    }

    private int JobId { get; set; }
    private int batchid { get; set; }
    private int batchprocessid { get; set; }
    private process activeprocess { get; set; }

    protected bool isDisabled()
    {
        // TODO: try capture this block
        job job = SystemAccess.getJob(JobId);
        system system = SystemAccess.getSystem(job.Systemid);

        if (job.Disabled || system.Disabled || job.Status == "DISABLED")
            return true;
        else
            return false;
    }

    private void log(string description)
    {
        log(description, ProcessAccess.enmLevel.Standard);
    }

    private void log(string description, ProcessAccess.enmLevel level)
    {
        int processid = 0;
        if (activeprocess != null)
            processid = activeprocess.id;
        //-- log the event
        Logger.BatchLog(JobId, processid, batchid, level, description);
    }


    public void Execute()
    {
        // TODO: try capture this block
        job job = SystemAccess.getJob(JobId);


        //-- if the entire job is disabled. exit this routine.
        if (isDisabled())
        {
            return;
        }

        //-- set the task as processing
        setJobStatus(job, "PROCESSING", true);

        //-- begin executing processes
        try
        {

            List<process> processes = SystemAccess.getProcesses(JobId);

            // anything "COMPLETE" reset to waiting. Ignore DISABLED's
            resetProcessingStatus(processes, "WAITING");


            // get the processes - Complete should now be Waiting
            log("Retrieving all processes for Job " + job.ShortName);
            processes = SystemAccess.getProcesses(JobId);

            // -- execute each WAITING, FAILED and NON-DISABLED process
            process activeprocess = null;
            try
            {
                foreach (process process in processes)
                {
                    activeprocess = process;

                    //-- look to see if we can execute this process
                    if (!process.Disabled && (process.Status == "WAITING" || process.Status == "FAILED" || process.Status == "PROCESSING")
                        && !isDisabled())
                    {
                        log("found process " + process.ShortName + " sequence #" + process.Sequence);
                        setProcessStatus(process, "CREATING", true);

                        log("Creating object " + process.AssemblyName + "." + process.ClassName);
                        //-- create object through reflection


                        string execPath = Server.MapPath("~");
                        Assembly assembly = Assembly.LoadFrom(execPath + "bin\\" + process.AssemblyName.Trim() + ".dll");
                        ProcessTask processTask = (ProcessTask)assembly.CreateInstance(process.AssemblyName.Trim() + "." + process.ClassName.Trim());
                        //processTask = new UBM.MiddlewareESOP.Customers.MiddlewareScrutiny();
                        //processTask = new UBM.MiddlewareJDE.ReferenceData.JDERefresh();

                        //-- check to make sure that the process was ok.
                        if (processTask == null)
                            throw new Exception("The Assembly " + process.AssemblyName + "." + process.ClassName + " could not be created");

                        //-- set the Id details of the process...
                        processTask.Batchid = batchid;
                        processTask.Processid = process.id;
                        processTask.Jobid = JobId;
                        processTask.Server = Server;
                        processTask.RootPath = execPath;

                        processTask.ParseConfiguration();

                        //-- execute the Process - setting important details
                        setProcessStatus(process, "PROCESSING");
                        log("Executing process step " + process.ShortName + "[" + process.AssemblyName + "." + process.ClassName + "]");


                        SyncRefresher.SendProgressReport(job.ShortName, "Started", 0);
                        processTask.Execute();

                        //-- all went well. complete the task.
                        setProcessStatus(process, "COMPLETE");

                        SyncRefresher.SendProgressReport(job.ShortName, "Complete", 100);


                    }
                    else
                    {
                        log("process is being ignored as it is disabled, or the job / system it belongs to is disabled");
                    }
                }

                //-- completed processing these processes.
                setJobStatus(job, "COMPLETE");
            }
            catch (Exception exProcess)
            {

                SyncRefresher.SendProgressReport(job.ShortName, "Failed", 0);

                Logger.ErrorLog(exProcess);

                //-- try and email the error
                if ((job.FailureNotificationSent ?? false) == false)
                {
                    try
                    {

                        string processDetails = "";

                        if (activeprocess != null)
                        {
                            processDetails = job.ShortName + ":" + activeprocess.ShortName + "\r\n";
                        }

                        //-- only send a message if the job did not timeout.
                        if (!didProcessTimeOut())
                            mailMessage(processDetails + exProcess.Message, "Middleware:" + job.ShortName + " Failed " + DateTime.Now, Priority.High);

                        //-- set and store the Notification.
                        job.FailureNotificationSent = true;
                        SystemAccess.setJob(job);

                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorLog(ex);
                    }
                }

                //-- log the error
                Logger.ErrorLog(exProcess);
                log("PROCESS FAILED: " + exProcess.Message, ProcessAccess.enmLevel.Critical);

                // mark the process as failed if the process did not timeout. 
                // TODO: Mark the Job as Failed when multiple Timeouts occur
                string result = "FAILED";
                if (didProcessTimeOut()) result = "WAITING";
                setProcessStatus(activeprocess, result);

                throw exProcess;
            }
        }
        catch (Exception exJob)
        {
            //Logger.ErrorLog(exJob, "Processing Job details");
            log("JOB FAILED: " + exJob.Message, ProcessAccess.enmLevel.Critical);
            string result = "FAILED";
            if (didProcessTimeOut()) result = "WAITING";
            setJobStatus(job, result);
        }

        //-- Job completed.

    }

    private void setProcessStatus(process process, string status)
    {
        setProcessStatus(process, status, false);
    }

    //-- determines if the process timedout out or not. timeouts do not mean failure...
    private bool didProcessTimeOut()
    {
        log("Checking to see if the process performed a timeout");
        return SystemAccess.getBatchTimedOut(batchid);

    }

    private void setProcessStatus(process process, string status, bool creation)
    {
        if (creation)
        {
            log("Creating a new batch process id for process id " + process.id);
            batchprocessid = ProcessAccess.insertBatchProcessStatus(process.id, batchid);
            log("batch process id was " + batchprocessid);
            process.LastExecuted = DateTime.Now;
            log("setting the last execution datetime to " + process.LastExecuted);
        }
        else
        {
            log("setting the process status to " + status);
            ProcessAccess.updateBatchProcessStatus(batchprocessid, status);
        }
        process.Status = status;
        log("setting the process status (LINQ-SQL)");
        SystemAccess.setProcess(process);
    }


    private void setJobStatus(job job, string status)
    {
        setJobStatus(job, status, false);
    }


    private void setJobStatus(job job, string status, bool creation)
    {

        if (job == null)
        {
            log("Job does not exist.. id{" + JobId + "}", ProcessAccess.enmLevel.Critical);
            return;
        }

        if (creation)
        {
            log("Creating a new Batch number for Job " + JobId);
            batchid = ProcessAccess.insertBatchStatus(JobId);
            log("batch number created was " + batchid);
            job.LastExecuted = DateTime.Now;
            log("setting last execution date as " + job.LastExecuted);
        }
        else
        {
            log("setting Job status as " + status);
            ProcessAccess.updateBatchStatus(batchid, status);
        }

        job.Status = status;
        //-- reset failed messages.
        if (job.Status == "COMPLETE" && (job.FailureNotificationSent ?? false))
        {
            mailMessage(job.ShortName + " now completed.", "Middleware:" + job.ShortName + " Completed", Priority.High);
            job.FailureNotificationSent = false;
        }

        log("setting the job status (LINQ-SQL)");
        SystemAccess.setJob(job);
    }

    private void mailMessage(string message, string subject, Priority level)
    {
        try
        {

            string recipient = ConfigurationManager.AppSettings["JobStatusEmailRecipients"];
            if (recipient.Length > 0)
            {

                NotificationServiceClient.Email("", recipient, "", subject, message, level, true);
            }
        }
        catch (Exception ex)
        {
            Logger.ErrorLog(ex);
        }
    }

    private void resetProcessingStatus(List<process> processes, string status)
    {
        // look to reset the status of a jobs processes where they are all complete
        log("reseting process statuses to " + status);
        var allprocesses = from p in processes where p.Status != "COMPLETE" && p.Status != "DISABLED" select p;
        if (allprocesses.Count() == 0)
        {
            foreach (process process in processes)
            {
                log("resetting process id " + process.id + "[" + process.ShortName + "] to WAITING");
                setProcessStatus(process, status);
            }
        }
        else
        {
            log("processes exist that were not complete, and not disabled (either still waiting or failed). Processes will not be rest to waiting");
        }
    }
}
