﻿using System.Configuration;
using System.Web;

namespace WebsiteFunctions
{
    /// <summary>
    ///   Sets the name of the administration screen, to ste style and data to be displayed
    /// </summary>
    public class SetWebsiteProductName
    {
        #region WebsiteProductName enum

        /// <summary>
        ///   website product names available for admin screens
        /// </summary>
        public enum WebsiteProductName
        {
            middleware,
            atom
        }

        #endregion

        private string url;

        /// <summary>
        ///   website product name to be used
        /// </summary>
        public static string GetWebsiteProductName
        {
            get;
            set;
        }

        public void SetProductName()
        {
            url = HttpContext.Current.Request.Url.Host.ToString();
            UBM.Logger.Log.Information("Found the Current URL as " + url);

            //GetWebsiteProductName = ConfigurationManager.AppSettings["WebProductName"];

            /*AFTER TESTING UNCOMMENT AND REMOVE WEB.CONFIG REFERENCE*/

            if (url.ToLower().Contains("atom"))
            {
                GetWebsiteProductName = WebsiteProductName.atom.ToString();
            }
            else
            {
                GetWebsiteProductName = WebsiteProductName.middleware.ToString();
            }
        }

   }
}