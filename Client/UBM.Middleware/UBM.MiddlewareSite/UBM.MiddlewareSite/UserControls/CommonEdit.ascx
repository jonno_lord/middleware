﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls.UserControls_CommonEdit" Codebehind="CommonEdit.ascx.cs" %>
<div class="buttonAlignLeft">
    <!--Connectionstring is supplied without the "ConnectionString" suffix-->
    <asp:LinkButton ID="lnkDelete" CssClass="glassbutton" runat="server" CausesValidation="False" OnClientClick="return confirm('Are you sure you wish to delete?');" onClick="btnDeleteEntity_Click"><asp:Image ID="imgDelete" ImageUrl="~/images/deleteicon.png" runat="server" AlternateText="Delete"/><p>Delete <asp:Literal ID="litDeleteTextEntityName" runat="server"></asp:Literal></p></asp:LinkButton>
</div>
<div class="buttonAlignRight">
    <asp:LinkButton ID="lnkCancel" CssClass="glassbutton" runat="server" OnClick="btnBack_Click" CausesValidation="False"><asp:Image ImageUrl="~/images/backicon.png" runat="server" AlternateText="Cancel"/><p>Cancel <asp:Literal ID="litCancelTextEntityName" runat="server"></asp:Literal></p></asp:LinkButton>
    <asp:LinkButton ID="lnkSubmit" CssClass="glassbutton glassbuttonedit" runat="server" OnClick="btnSaveEntity_Click"><asp:Image ID="imgSubmit" runat="server" /><p><asp:Literal ID="litActionName" runat="server"></asp:Literal><asp:Literal ID="litActionTextEntityName" runat="server"></asp:Literal></p></asp:LinkButton>
</div>