﻿using System;
using System.Configuration;
using System.Web.UI;

namespace UserControls
{
    public partial class UserControls_Navigation : UserControl
    {
        private readonly string atomFilter = ConfigurationManager.AppSettings["atomFilter"];

        /// <summary>
        ///   Sets which system the navigation should be styled for and which links it should contain
        /// </summary>
        public string navigationProduct
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string value = navigationProduct;

            switch (value)
            {
                case "atom":

                    pnlAtom.Visible = true;
                    pnlMiddleware.Visible = false;

                    //set filter ids
                    lnkAtomJobs.NavigateUrl = "~/Pages/Configuration/Jobs/Jobs.aspx?id=" + atomFilter;
                    lnkAtomSchedules.NavigateUrl = "~/Pages/Configuration/Schedules/Schedules.aspx?id=" + atomFilter;
                    lnkAtomBatch.NavigateUrl = "~/Pages/Operation/BatchHistory.aspx?id=" + atomFilter;
                    lnkAtomRubJob.NavigateUrl = "~/Pages/Operation/RunJob.aspx?id=" + atomFilter;

                    break;
                
                case "middleware":

                    pnlMiddleware.Visible = true;
                    pnlAtom.Visible = false;

                    break;
            }
        }
    }
}