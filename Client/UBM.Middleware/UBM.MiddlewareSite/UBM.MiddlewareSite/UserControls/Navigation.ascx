﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls.UserControls_Navigation" Codebehind="Navigation.ascx.cs" %>

<!--This is the main navigation at the top for all pages. Links and icons changed here will be reflected across all pages-->

<!--Navigation for Middleware only-->
<asp:Panel ID="pnlMiddleware" runat="server">
<ul id="topnav" class="topLinks">
    <li>
        <a href="#"><asp:Image ID="imgConfigDropDown" ImageUrl="~/Images/DropDown.png" AlternateText="Configuration Menu" runat="server" />Configuration</a>
        <div class="sub">
            <ul>
                <li class="block"><asp:Hyperlink ID="hypSystems" runat="server" NavigateUrl="~/Pages/Configuration/Systems/systems.aspx"><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Icons/system.png" AlternateText="System" ToolTip="Systems"/><span class="subtext">Systems</span><span class="subtextdescription">SOP, MW and JDE Config</span></asp:Hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypJobs" runat="server" NavigateUrl="~/Pages/Configuration/Jobs/jobs.aspx"><asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Icons/job.png" AlternateText="Jobs" ToolTip="Job" /><span class="subtext">Jobs</span><span class="subtextdescription">feeds SOP from systems to JDE</span></asp:hyperlink></li>
                <li class="block"><asp:Hyperlink ID="hypProcesses" runat="server" NavigateUrl="~/Pages/Configuration/Processes/processes.aspx"><asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Icons/process.png" AlternateText="Process" ToolTip="Processes" /><span class="subtext">Processes</span><span class="subtextdescription">ETL routines to process data</span></asp:Hyperlink></li>
                <li class="block"><asp:Hyperlink ID="hypSchedules" runat="server" NavigateUrl="~/Pages/Configuration/Schedules/schedules.aspx"> <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Icons/schedule.png" AlternateText="Schedule" ToolTip="Schedules" /><span class="subtext">Schedules</span><span class="subtextdescription">timed events for jobs</span></asp:Hyperlink></li> 
            </ul>
            <ul>
                <li class="block"><asp:hyperlink ID="hypSystemNames" runat="server" NavigateUrl="~/Pages/Configuration/Systems/SystemNames.aspx"><asp:Image runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="System Names" ToolTip="System Names" /><span class="subtext">System Names</span><span class="subtextdescription">system aliases</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypProcessTypes" runat="server" NavigateUrl="~/Pages/Configuration/Processes/ProcessTypes.aspx"><asp:Image runat="server" ImageUrl="~/Images/Icons/processtypes.png" AlternateText="Process Types" ToolTip="Process Types"/><span class="subtext">Process Types</span><span class="subtextdescription">extract, translate, load</span></asp:hyperlink></li>
            </ul>
        </div>
    </li> 

    <li>
        <a href="#"><asp:Image ID="imgOpsDropDown" ImageUrl="~/Images/DropDown.png" AlternateText="Operation Menu" runat="server" />Operation</a>
        <div class="sub operation">
            <ul>
                <li class="block"><asp:hyperlink ID="hypDashboard" runat="server" NavigateUrl="~/Pages/Operation/Dashboard.aspx"><asp:Image ID="imgDashboard" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Dashboard" ToolTip="Dashboard" /><span class="subtext">Dashboard</span><span class="subtextdescription">summary, status, metrics,</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypBatchHistory" runat="server" NavigateUrl="~/Pages/Operation/BatchHistory.aspx"><asp:Image ID="imgBatchHistory" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Batch History" ToolTip="Batch History" /><span class="subtext">Batch History</span><span class="subtextdescription">summary of batches</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypRunJob" runat="server" NavigateUrl="~/Pages/Operation/RunJob.aspx"><asp:Image ID="imgRunJob" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Run Jobs" ToolTip="Run Jobs" /><span class="subtext">Run Jobs</span><span class="subtextdescription">manual job runner, job status</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypeRunSingleJob" runat="server" NavigateUrl="~/Pages/Operation/RunSingleJob.aspx"><asp:Image ID="imgRunSingleJob" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Run Single Job" ToolTip="Run Single Job" /><span class="subtext">Run Single Job</span><span class="subtextdescription">manual job runner, job status</span></asp:hyperlink></li>
            </ul>
            <ul>
                <li class="block"><asp:hyperlink ID="hypDuration" runat="server" NavigateUrl="~/Pages/Operation/BatchDuration.aspx"><asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Batch Duration" ToolTip="Batch Duration Chart" /><span class="subtext">Batch Duration</span><span class="subtextdescription">chart of batch durations</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypConfigsettings" runat="server" NavigateUrl="~/Pages/Operation/ConfigurationSettings.aspx"><asp:Image ID="Image10" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Configuration Settings" ToolTip="Configuration Settings" /><span class="subtext">Configuration Settings</span><span class="subtextdescription">configuration for systems</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypErrors" runat="server" NavigateUrl="~/Pages/Operation/ErrorLog.aspx"><asp:Image ID="imgError" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Error Log" ToolTip="Error Log" /><span class="subtext">Error Log</span><span class="subtextdescription">error log, stack trace</span></asp:hyperlink></li>
            </ul>
        </div>
    </li>

    <li>
        <a href="#"><asp:Image ID="imgDefDropDown" ImageUrl="~/Images/DropDown.png" AlternateText="Defaults Menu" runat="server" />Defaults</a>
         
         <div class="sub defaults">
            <ul>
                <li><h1>All Systems</h1></li>
                <li class="block"><asp:hyperlink ID="hypScrutinyRules" runat="server" NavigateUrl="~/Pages/Defaults/CustomerScrutinyRules.aspx"><asp:Image ID="imgScrutinyRules" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Customer Scrutiny Rules" ToolTip="Customer Scrutiny Rules" /><span class="subtext">Scrutiny Rules</span><span class="subtextdescription">customer scrutiny rules</span></asp:hyperlink></li>
                <li><h1>ESOP</h1></li>
                <li class="block"><asp:hyperlink ID="hypTaxOverride" runat="server" NavigateUrl="~/Pages/Defaults/ESOP/TaxOverride.aspx"><asp:Image ID="imgTaxOverride" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Tax Override" ToolTip="Tax Override Codes" /><span class="subtext">Tax Override</span><span class="subtextdescription">ESOP tax override codes</span></asp:hyperlink></li>   
                <li><h1>IPSOP</h1></li>
                <li class="block"><asp:hyperlink ID="hypBusinessUnits" runat="server" NavigateUrl="~/Pages/Defaults/IPSOP/BusinessUnits.aspx"><asp:Image ID="imgBusinessUnit" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Business Units" ToolTip="Business Units" /><span class="subtext">Business Units</span><span class="subtextdescription">IPSOP business units</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypCountryCode" runat="server" NavigateUrl="~/Pages/Defaults/IPSOP/CountryCodeMapping.aspx"><asp:Image ID="imgCountryCode" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Country Code Mapping" ToolTip="Country Code Mapping" /><span class="subtext">Country Codes</span><span class="subtextdescription">IPSOP country code mapping</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypCreditMessages" runat="server" NavigateUrl="~/Pages/Defaults/IPSOP/CreditMessageMapping.aspx"><asp:Image ID="imgCreditMessage" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Credit Message Mapping" ToolTip="Credit Message Mapping" /><span class="subtext">Credit Messages</span><span class="subtextdescription">IPSOP credit message mapping</span></asp:hyperlink></li>
                <li><h1>N200</h1></li>
                <li class="block"><asp:hyperlink ID="hypBadgeAllocation" runat="server" NavigateUrl="~/Pages/Defaults/N200/BadgeAllocations.aspx"><asp:Image ID="imgBadgeAllocations" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Badge Allocations" /><span class="subtext">Badge Allocations</span><span class="subtextdescription">Configure Badge Allocations</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypEvents" runat="server" NavigateUrl="~/Pages/Defaults/N200/Events.aspx"><asp:Image ID="imgEvents" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="List Of Events" /><span class="subtext">Events</span><span class="subtextdescription">List of Events in N200</span></asp:hyperlink></li>
                 <li class="block"><asp:hyperlink ID="hypPersonnelTypes" runat="server" NavigateUrl="~/Pages/Defaults/N200/PersonnelTypes.aspx"><asp:Image ID="imgPersonnelTypes" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="List Of Personnel Types" /><span class="subtext">Personnel Types</span><span class="subtextdescription">List of Personnel Types in N200</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypRegistrationTypes" runat="server" NavigateUrl="~/Pages/Defaults/N200/RegistrationTypes.aspx"><asp:Image ID="imgRegistrationTypes" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="List Of Registration Types" /><span class="subtext">Registration Types</span><span class="subtextdescription">List of Registration Types in N200</span></asp:hyperlink></li>
            </ul>
            <ul>
                <li><h1>ASOP</h1></li>
                <li class="block"><asp:hyperlink ID="hypActivityTypes" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/ActivityTypes.aspx"><asp:Image ID="imgActivityTypes" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Activity Types" ToolTip="Activity Types" /><span class="subtext">Activity Types</span><span class="subtextdescription">ASOP activity type mappings</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypClashingTitleCards" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/ClashingTitleCards.aspx"><asp:Image ID="imgClashing" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Clashing Title Cards" ToolTip="Clashing Title Cards" /><span class="subtext">Clashing Title Cards</span><span class="subtextdescription">ASOP clashing title cards</span></asp:hyperlink></li>       
                <li class="block"><asp:hyperlink ID="hypCustomerTypes" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/CustomerTypes.aspx"><asp:Image ID="imgCustomerTypes" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Customer Types" ToolTip="Customer Types" /><span class="subtext">Customer Types</span><span class="subtextdescription">ASOP customer types</span></asp:hyperlink></li>       
                <li class="block"><asp:hyperlink ID="hypDocumentMaintenance" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/DocumentMaintenance.aspx"><asp:Image ID="imgDocMain" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Document Maintenance" ToolTip="Document Maintenance" /><span class="subtext">Document Maintenance</span><span class="subtextdescription">ASOP document maintenance</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypPaymentTypes" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/PaymentTypes.aspx"><asp:Image ID="imgPaymentTypes" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Payment Types" ToolTip="Payment Types" /><span class="subtext">Payment Types</span><span class="subtextdescription">ASOP payment types</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypPubtoBU" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/PublicationToBusinessUnits.aspx"><asp:Image ID="imgPubToBU" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Publication to Business Units" ToolTip="Publication to Business Units" /><span class="subtext">Business Units</span><span class="subtextdescription">ASOP publication, BU mapping</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypSummaryCards" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/ValidSummaryCards.aspx"><asp:Image ID="imgValidSummaryCards" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Valid Summary Cards" ToolTip="Valid Summary Cards" /><span class="subtext">Valid Summary Cards</span><span class="subtextdescription">ASOP valid summary cards</span></asp:hyperlink></li>
                <li class="block"><asp:hyperlink ID="hypTaxCompany" runat="server" NavigateUrl="~/Pages/Defaults/ASOP/TaxCompanies.aspx"><asp:Image ID="imgTaxCompany" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Tax Companies" ToolTip="Tax Companies" /><span class="subtext">Tax Companies</span><span class="subtextdescription">ASOP tax companies</span></asp:hyperlink></li>
            </ul>
        </div>
    </li>
</ul>
</asp:Panel>

<!--Navigation for Atom only-->
<asp:Panel ID="pnlAtom" runat="server">
<ul id="topnav" class="topLinks">
    <li>
        <a href="#"><asp:Image ID="Image5" ImageUrl="~/Images/DropDown.png" AlternateText="Configuration Menu" runat="server" />Configuration</a>
        <div class="sub atomConfig">
            <ul>
                <li><asp:hyperlink ID="lnkAtomJobs" runat="server" NavigateUrl="~/Pages/Configuration/Jobs/Jobs.aspx"><asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Icons/job.png" AlternateText="Jobs" ToolTip="Job" /><span class="subtext">Jobs</span><span class="subtextdescription">feeds SOP from systems to JDE</span></asp:hyperlink></li>
                <li><asp:Hyperlink ID="lnkAtomProcesses" runat="server" NavigateUrl="~/Pages/Configuration/Processes/processes.aspx"><asp:Image ID="Image8" runat="server" ImageUrl="~/Images/Icons/process.png" AlternateText="Process" ToolTip="Processes" /><span class="subtext">Processes</span><span class="subtextdescription">ETL routines to process data</span></asp:Hyperlink></li>
                <li><asp:Hyperlink ID="lnkAtomSchedules" runat="server" NavigateUrl="~/Pages/Configuration/Schedules/schedules.aspx"> <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/Icons/schedule.png" AlternateText="Schedule" ToolTip="Schedules" /><span class="subtext">Schedules</span><span class="subtextdescription">timed events for jobs</span></asp:Hyperlink></li> 
            </ul>
        </div>
    </li> 

    <li>
        <a href="#"><asp:Image ID="Image12" ImageUrl="~/Images/DropDown.png" AlternateText="Operation Menu" runat="server" />Operation</a>
        <div class="sub atomOperation">
            <ul>
                <li><asp:hyperlink ID="lnkAtomBatch" runat="server" NavigateUrl="~/Pages/Operation/BatchHistory.aspx"><asp:Image ID="Image14" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Batch History" ToolTip="Batch History" /><span class="subtext">Batch History</span><span class="subtextdescription">summary of batches</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="lnkAtomRubJob" runat="server" NavigateUrl="~/Pages/Operation/RunJob.aspx"><asp:Image ID="Image15" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Run Jobs" ToolTip="Run Jobs" /><span class="subtext">Run Jobs</span><span class="subtextdescription">manual job runner, job status</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="lnkAtomErrorLog" runat="server" NavigateUrl="~/Pages/Operation/ErrorLog.aspx"><asp:Image ID="Image16" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Error Log" ToolTip="Error Log" /><span class="subtext">Error Log</span><span class="subtextdescription">error log, stack trace</span></asp:hyperlink></li>
            </ul>
            <ul>
                 <li class="block"><asp:hyperlink ID="hypDash" runat="server" NavigateUrl="~/Pages/Operation/Dashboard.aspx"><asp:Image ID="Image11" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Dashboard" ToolTip="Dashboard" /><span class="subtext">Dashboard</span><span class="subtextdescription">summary, status, metrics,</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="hypConfig" runat="server" NavigateUrl="~/Pages/Defaults/Atom/Configuration.aspx"><asp:Image ID="imgConfig" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Configuration" ToolTip="Configuration" /><span class="subtext">Atom Configuration</span><span class="subtextdescription">file paths for TRA and DAT</span></asp:hyperlink></li>
            </ul>
        </div>
    </li>

    <li>
        <a href="#"><asp:Image ID="Image17" ImageUrl="~/Images/DropDown.png" AlternateText="Defaults Menu" runat="server" />Defaults</a>
         <div class="sub atomDefaults">
            <ul>
                <li><asp:hyperlink ID="hypCustomer" runat="server" NavigateUrl="~/Pages/Defaults/Atom/Customers.aspx"><asp:Image ID="imgCustomers" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Customers" ToolTip="Customers" /><span class="subtext">Customers</span><span class="subtextdescription">listing of system customer data</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="hypOrders" runat="server" NavigateUrl="~/Pages/Defaults/Atom/Orders.aspx"><asp:Image ID="imgOrders" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Orders" ToolTip="Orders" /><span class="subtext">Orders</span><span class="subtextdescription">listing of system order data</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="hypOverride" runat="server" NavigateUrl="~/Pages/Defaults/Atom/AtomOverride.aspx"><asp:Image ID="imgOverride" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Atom Overrides" ToolTip="Atom Overrides" /><span class="subtext">Atom Overrides</span><span class="subtextdescription">trans type and value mappings</span></asp:hyperlink></li>
            </ul>
            <ul>
                <li><asp:hyperlink ID="hypProductMapping" runat="server" NavigateUrl="~/Pages/Defaults/Atom/ProductMappings.aspx"><asp:Image ID="imgProductMapping" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Product Mappings" ToolTip="Product Mappings" /><span class="subtext">Product Mappings</span><span class="subtextdescription">product mappings</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="hypAtomBatchHistory" runat="server" NavigateUrl="~/Pages/Defaults/Atom/BatchHistory.aspx"><asp:Image ID="imgAtomBatchHistory" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Batch History" ToolTip="Batch History" /><span class="subtext">Atom Batch History</span><span class="subtextdescription">ETL status, TRA and DAT files</span></asp:hyperlink></li>
                <li><asp:hyperlink ID="hypSystem" runat="server" NavigateUrl="~/Pages/Defaults/Atom/Systems.aspx"><asp:Image ID="imgSystems" runat="server" ImageUrl="~/Images/Icons/systemnames.png" AlternateText="Systems" ToolTip="Systems" /><span class="subtext">Systems</span><span class="subtextdescription">systems default mappings</span></asp:hyperlink></li>
            </ul>
        </div>
    </li>
</ul>
</asp:Panel>
