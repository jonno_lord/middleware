﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Configuration;
using System.Web.UI;

namespace UserControls
{
    public partial class UserControls_CommonActions : UserControl
    {
        #region Private Fields

        private static SqlConnection connection;

        private bool storedProcExists;

        #endregion

        #region Public Properties

        /// <summary>
        ///   name of entity text to be displayed, should be plural
        /// </summary>
        public string EntityName
        {
            get;
            set;
        }

        /// <summary>
        ///   name of table
        /// </summary>
        public string TableName
        {
            get;
            set;
        }

        /// <summary>
        ///   URL of page of control for redirect
        /// </summary>
        public string EntityURL
        {
            get;
            set;
        }

        /// <summary>
        ///   connection string of entity to enable, disable, where the stored procedures are held
        /// </summary>
        public string ConnectionString
        {
            get;
            set;
        }

        /// <summary>
        ///   set functionality of enable/disable buttons
        /// </summary>
        public string disableEnable
        {
            get;
            set;
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";

            litEmailEntityName.Text = EntityName;
            litDisableEntityName.Text = EntityName;
            litEnableTextEntityName.Text = EntityName;
            litPrintEntityName.Text = EntityName;

            //wire up events if buttons are required else disable them
            if (disableEnable == "true")
            {
                lnkEnable.Enabled = true;
                lnkDisable.Enabled = true;
                lnkEnable.Click += lnkEnable_Click;
                lnkDisable.Click += lnkDisable_Click;
            }
            else
            {
                lnkEnable.Visible = false;
                lnkDisable.Visible = false;
             
            }

        }

        protected void lnkPrint_Click(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: printerView(); ", true);
        }

        protected void lnkEnable_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = WebConfigurationManager.ConnectionStrings[ConnectionString + "ConnectionString"].ConnectionString;
 
                if (storedProcedureExists("usp_UPD_Enable" + EntityName.Replace(" ", "")))
                {

                    using (connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        string spEntity = EntityName.Replace(" ", "");
                        SqlCommand command = new SqlCommand("usp_UPD_Enable" + spEntity, connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.ExecuteReader();

                        //Random number = new Random();
                        // number.Next(0, 1000);

                        //Response.Redirect(EntityURL + "?rand=" + number.Next(0, 1000));
                        Response.Redirect(EntityURL + "?filter=" + ConnectionString);
                    }
                }
                else
                {
                    lblMessage.Text = " Stored Procedure does not exist for this operation";
                    lblMessage.ForeColor = Color.Red;
                }
            }
            catch
            {
                lblMessage.Text = "There is no valid connection string";
                lblMessage.ForeColor = Color.Red;
            }
        }

        protected void lnkDisable_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = WebConfigurationManager.ConnectionStrings[ConnectionString + "ConnectionString"].ConnectionString;

                if (storedProcedureExists("usp_UPD_Disable" + EntityName.Replace(" ", "")))
                {
                    using (connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        string spEntity = EntityName.Replace(" ", "");
                        SqlCommand command = new SqlCommand("usp_UPD_Disable" + spEntity, connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.ExecuteReader();

                        //Random number = new Random();
                        // number.Next(0, 1000);

                        //Response.Redirect(EntityURL + "?rand=" + number.Next(0, 1000));
                        Response.Redirect(EntityURL + "?filter=" + ConnectionString);
                    }
                }
                else
                {
                    lblMessage.Text = " Stored Procedure does not exist for this operation";
                    lblMessage.ForeColor = Color.Red;
                }
            }
            catch
            {
                lblMessage.Text = "There is no valid connection string";
                lblMessage.ForeColor = Color.Red;
            }
        }

        /// <summary>
        ///   Checks whether stored procedure exists in selected system database, if so then there are Customer scrutiny rules to be viewed
        /// </summary>
        /// <param name = "storedProcedure"></param>
        /// <returns>bool that states whether stpred procedure exists</returns>
        protected bool storedProcedureExists(string storedProcedure)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings[ConnectionString + "ConnectionString"].ConnectionString;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='" + storedProcedure + "'", connection);
                SqlDataReader sdrSpExists = command.ExecuteReader();

                if (sdrSpExists.Read())

                    storedProcExists = true;

                return storedProcExists;
            }
        }
        #endregion
    }
}