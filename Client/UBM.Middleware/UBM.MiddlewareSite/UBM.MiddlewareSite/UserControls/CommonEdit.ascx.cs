﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace UserControls
{
    public partial class UserControls_CommonEdit : UserControl
    {
        #region Private Fields and Propteries

        private static SqlConnection connection;

        private bool add = true;

        #endregion

        #region Public Properties

        /// <summary>
        ///   Name of entity text to display, should be singular
        /// </summary>
        public string EntityName
        {
            get;
            set;
        }

        /// <summary>
        ///   URL to redirect once edit has been made
        /// </summary>
        public string EntityURL
        {
            get;
            set;
        }

        /// <summary>
        ///   connectionstring to be used for delete action
        /// </summary>
        public string ConnectionString
        {
            get; 
            set; 
        }

        /// <summary>
        ///   gets id from entity page for cancel redirect, to maintain page filters
        /// </summary>
        public int Cancelid
        {
            get; 
            set;
        }

        /// <summary>
        ///   gets optional id from entity page for cancel redirect to maintain page filters
        /// </summary>
        public int OptionalCancelid
        {
            get;
            set;
        }

        /// <summary>
        ///   gets optional querystring name from entity page
        /// </summary>
        public string OptionalCancelName
        {
            get;
            set;
        }

        /// <summary>
        ///   enables or disables delete button
        /// </summary>
        public bool deleteEnabled
        {
            get; 
            set;
        }

        /// <summary>
        ///   sets stored procedure entity name, supply only table name suffix, e.g. 'jobs' for 'usp-DEL Jobs'
        /// </summary>
        public string deleteSPName
        {
            get;
            set;
        }

        /// <summary>
        ///   sets parameter name for stored procedure, supply only prefix e.g. 'job' for '@jobid'
        /// </summary>
    public string deleteParameter
        {
            get;
            set;
        }

        public event EventHandler ucSaveEntity_Click;

        public event EventHandler ucCancelEntity_Click;

        public event EventHandler ucDeleteEntity_Click;

        #endregion

        #region Private Methods

        // sets submit button design based on whether screen is for adding or editing
        private void defineAction()
        {
            add = Request.QueryString["id"] == null;

            if (add == false)
            {
                //if it is an edit action
                litActionName.Text = "Save ";
                imgSubmit.ImageUrl = "~/images/editicon.png";
                imgSubmit.AlternateText = "Edit";

                //tooltips
                lnkSubmit.ToolTip = "Save " + EntityName + " Edit";
                lnkCancel.ToolTip = "Cancel " + EntityName + " Edit";
                lnkDelete.ToolTip = "Delete this " + EntityName;
            }
            else
            {
                //if it is an add action
                litActionName.Text = "Add ";
                imgSubmit.ImageUrl = "~/images/addicon.png";
                imgSubmit.AlternateText = "Add";

                //tooltips
                lnkSubmit.ToolTip = "Add a new " + EntityName;
                lnkCancel.ToolTip = "Cancel Adding New" + EntityName;

                //hides delete button on add
                lnkDelete.Visible = false;
                imgDelete.Visible = false;
                litDeleteTextEntityName.Visible = false;
            }
        }

        //sets entity name
        private void defineButtonAttributes()
        {
            litCancelTextEntityName.Text = EntityName;
            litActionTextEntityName.Text = EntityName;
            litDeleteTextEntityName.Text = EntityName;

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            defineAction();
            defineButtonAttributes();


            //disables deletion of entity
            if (deleteEnabled == false)
            {
                lnkDelete.Enabled = false;
            }
        }

        //handles maintaining system/job/process filters on cancel
        protected void btnBack_Click(object sender, EventArgs e)
        {
            //gets an id to redirect with filter
            ucCancelEntity_Click.Invoke(this, new EventArgs());

            //two filters
            if (OptionalCancelid != 0)
            {
                Response.Redirect(EntityURL + "?id=" + Cancelid + "&" + OptionalCancelName + "=" + OptionalCancelid);
            }
            else if (add && (Request.QueryString["addid"] != null))
            {
                int addid = int.Parse(Request.QueryString["addid"]);

                Response.Redirect(EntityURL + "?id=" + addid);
            }
            else if (Cancelid != 0)
            {
                //if there is a value to filter by from user input
                if ((add == false) || (add && (Request.QueryString["addid"] == null)))
                {
                    Response.Redirect(EntityURL + "?id=" + Cancelid);
                }
            }
            else
            {
                Response.Redirect(EntityURL);
            }
        }

        //uses details page to save entity
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            ucSaveEntity_Click.Invoke(this, new EventArgs());
        }

        //deletes entity
        protected void btnDeleteEntity_Click(object sender, EventArgs e)
        {
            if (ConnectionString.Contains("MiddlewareASOP"))
            {
                //gets an id to redirect with filter
                ucDeleteEntity_Click.Invoke(this, new EventArgs());
            }

            ConnectionString = ConnectionString.Contains("N200") ? ConnectionString.Replace("Middleware", "") : ConnectionString;
            
            string connectionString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings[ConnectionString + "ConnectionString"].ConnectionString);

            int id = int.Parse(Request.QueryString["id"]);

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_DEL_" + deleteSPName, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@" + deleteParameter + "id", id);

                command.ExecuteNonQuery();

                Random number = new Random();     
                number.Next(0, 1000);

                Response.Redirect(EntityURL + "?rand=" + number.Next(0, 1000));
            }
        }
        #endregion
    }
}