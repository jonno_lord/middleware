﻿using System;
using System.Web.UI;

namespace UserControls
{
    public partial class UserControls_CommonSummary : UserControl
    {
        #region Public Properties

        /// <summary>
        ///   title for page to appear next to add button
        /// </summary>
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        ///   Name of Entity to be displayed as text, should be singular
        /// </summary>
        public string EntityName
        {
            get;
            set;
        }

        /// <summary>
        ///   url of entity page e.g. "jobdetails.aspx"
        /// </summary>
        public string AddEntityURL
        {
            get;
            set;
        }

        /// <summary>
        ///   querystring name for filtering
        /// </summary>
        public string QueryStringFilter
        {
            get; 
            set; 
        }

        /// <summary>
        ///   second querystring for filtering
        /// </summary>
        public string SecondQueryStringFilter
        {
            get;
            set;
        }

        //gets a second id set in the page for auto setting values based on filters on adding
        public int Secondid
        {
            get; 
            set;
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            litTitle.Text = Title;
            litAddEntity.Text = EntityName;

            //if no add is required disable button
            if (AddEntityURL == null)
            {
                lnkAdd.Enabled = false;
                lnkAdd.Attributes.Add("class", "buttonDisabled");
            }
        }

        //sets link and its querystrings for add button
        protected void addEntity_Click(object sender, EventArgs e)
        {
            //if there are querystring filters apply them to to the redirect to add page
            if (Request.QueryString[QueryStringFilter] != null | Secondid != 0)
            {
                int id;

                try
                {              
                    id = int.Parse(Request.QueryString[QueryStringFilter]);
                }
                catch
                {
                    id = 0;
                }

                if (Secondid != 0)
                {
                    Response.Redirect(AddEntityURL + "?add" + QueryStringFilter + "=" + id + "&add" + SecondQueryStringFilter + "=" + Secondid);
                }
                else 
                {
                    Response.Redirect(AddEntityURL + "?add" + QueryStringFilter + "=" + id);
                }
            }
            else 
            {
                Response.Redirect(AddEntityURL);
            }
        }
        #endregion
    }
}