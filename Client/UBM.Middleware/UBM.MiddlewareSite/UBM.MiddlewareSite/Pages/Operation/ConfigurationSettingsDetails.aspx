﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="ConfigurationSettingsDetails" Codebehind="ConfigurationSettingsDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Configuration Settings</h1>
</div>
<div class="validationSummary">
    <div class="margin">
        <p><asp:label ID="lblMessage" runat="server" class="formMargin" /></p>
    </div>
    <asp:ValidationSummary ID="vdsConfigurationSettings" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEntryForm">

        <div class="formRow">
           <asp:Label ID="lblSystem" runat="server" cssClass="formMargin labelInput" Text="System:" Width="100px"/>
           <asp:DropDownList ID="ddlSystem" runat="server" cssClass="formMargin" ToolTip="System for Configuration Setting to Apply to"  Width="250" />
        <asp:RequiredFieldValidator ID="rfvSystem" runat="server"  ControlToValidate="ddlSystem" ErrorMessage="System Required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
           <asp:Label ID="lblKeyCategory" runat="server" cssClass="formMargin labelInput" Text="Key Category:" Width="100px"/>
           <asp:TextBox ID="txtKeyCategory" runat="server" cssClass="formMargin" ToolTip="Key Category for Configuration Setting" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvKeyCategory" runat="server"  ControlToValidate="txtKeyCategory" ErrorMessage="Key Category Name Required">
        *
        </asp:RequiredFieldValidator>
    </div>

    
    <div class="formRow">
           <asp:Label ID="lblKeyName" runat="server" cssClass="formMargin labelInput" Text="Key Name:" Width="100px"/>
           <asp:TextBox ID="txtKeyName" runat="server" cssClass="formMargin" ToolTip="Key Name for Configuration Setting" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvKeyName" runat="server"  ControlToValidate="txtKeyName" ErrorMessage="Key Name Required">
        *
        </asp:RequiredFieldValidator>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEntryForm">

    <div class="formRow">
        <asp:Label ID="lblKeyValue" runat="server" cssClass="formMargin labelInput" Text="Value:" Width="100px"/>
        <asp:TextBox ID="txtKeyValue" runat="server" cssClass="formMargin" ToolTip="Value for Configuration Setting" TextMode="MultiLine" Columns="39" Rows="2"/>
        <asp:RegularExpressionValidator ID="revKeyValue" ControlToValidate="txtKeyValue" ErrorMessage="Description is exceeding 2048 characters" ValidationExpression="^[\s\S]{0,2048}$" runat="server" >
        *
        </asp:RegularExpressionValidator>  
        <asp:RequiredFieldValidator ID="rfvKeyValue" runat="server" ControlToValidate="txtKeyValue" ErrorMessage="Value is Required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
        <asp:Label ID="lblDataType" runat="server" cssClass="formMargin labelInput" Text="Data Type:" Width="100px"/>
        <asp:DropDownList ID="ddlDataType" cssClass="formMargin" runat="server" tooltip="Data Type of Setting" Width="250">
                    <asp:ListItem>Boolean</asp:ListItem>
                    <asp:ListItem>DateTime</asp:ListItem>
                    <asp:ListItem>Double</asp:ListItem>
                    <asp:ListItem>Integer</asp:ListItem>
                    <asp:ListItem>String</asp:ListItem>
             </asp:DropDownList>
    </div>

    <div class="formRow">
           <asp:Label ID="lblEnvironmentName" runat="server" cssClass="formMargin labelInput" Text="Environment Name:" Width="100px"/>
             <asp:DropDownList ID="ddlEnvironmentName" cssClass="formMargin" runat="server" tooltip="Name of Environment setting applies to" Width="250">
                    <asp:ListItem>DEV</asp:ListItem>
                    <asp:ListItem>UAT</asp:ListItem>
                    <asp:ListItem>PROD</asp:ListItem>
             </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvEnvironmentName" runat="server"  ControlToValidate="ddlEnvironmentName" ErrorMessage="Environment Name Required">
        *
        </asp:RequiredFieldValidator>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Setting" EntityURL="ConfigurationSettings.aspx" deleteEnabled="true" deleteSPName="ConfigurationSetting"/>
</asp:Content>

