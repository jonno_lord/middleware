﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

public partial class ConfigurationSettingsDetails : Page
{
    #region private fields and properties

    private static SqlConnection connection;

    private bool storedProcExists;

    private static string systemString
    {
        get
        {
            string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
            string returnString = RemoveProviders.RemoveProvider(unchangedString);
            return returnString;
        }
    }

    /// <summary>
    ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
    ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
    /// </summary>
    private int id
    {
        get
        {
            int returnValue;
            int.TryParse(Request.QueryString["id"], out returnValue);
            return returnValue;
        }
    }

    private int systemid
    {
        get
        {
            int returnValue;
            int.TryParse(Request.QueryString["systemid"], out returnValue);
            return returnValue;
        }
    }

    private string connectionString
    {
        get
        {
            string unchangedString = WebConfigurationManager.ConnectionStrings[ddlSystem.SelectedItem.Text + "ConnectionString"].ConnectionString;
            string returnString = RemoveProviders.RemoveProvider(unchangedString);
            return returnString;
        }
    }

    #endregion

    #region private methods

    /// <summary>
    ///   Gets the configuration settings for a specified id
    /// </summary>
    private void GetConfigurationSetting()
    {
        GetSystemNames();

        if (storedProcedureExists("usp_GET_ConfigurationSetting"))
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_ConfigurationSetting", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", id);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtKeyCategory.Text = sdr["keyCategory"].ToString();
                    txtKeyName.Text = sdr["keyName"].ToString();
                    txtKeyValue.Text = sdr["keyValue"].ToString();
                    ddlDataType.SelectedValue = sdr["keyDataType"].ToString();
                    ddlEnvironmentName.SelectedValue = sdr["EnvironmentName"].ToString();
                }
            }
        }
        else
        {
            lblMessage.Text = "No stored procedure exists for retrieving records";
            lblMessage.ForeColor = Color.Red;
        }
    }

    private void GetSystemNames()
    {

        using (connection = new SqlConnection(systemString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlDataReader sdrSystems = command.ExecuteReader();

            while (sdrSystems.Read())
            {
                ListItem itmSystem = new ListItem();
                itmSystem.Value = sdrSystems["id"].ToString();
                itmSystem.Text = sdrSystems["ShortName"].ToString();

                if (systemid != 0)
                {
                    if (itmSystem.Value == systemid.ToString())
                    {
                        itmSystem.Selected = true;
                    }
                }
                else
                {
                    if (Session["SettingSystem"] != null)
                    {
                        if (itmSystem.Value == Session["SettingSystem"].ToString())
                        {
                            itmSystem.Selected = true;
                        }
                    }
                }

                ddlSystem.Items.Add(itmSystem);
            }

        }

    }

    // should be set in each screen depending on what value you want the cancel redirect to filter on.
    private void setCancelRedirect()
    {
        //allows buttons user control to access systemid value
        //  setting to 0 will represent no filtering
        int selectedValue = 0;
        try
        {
            selectedValue = Convert.ToInt32(ddlSystem.SelectedValue);
            CommonButtons.Cancelid = selectedValue;
        }
        catch
        {
            CommonButtons.Cancelid = selectedValue;
        }

    }

    #endregion

    #region protected methods and events

    protected void Page_Load(object sender, EventArgs e)
    {
        //allow rule to be saved from button user control via method in this cs file
        CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

        CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

        if (!IsPostBack)
        {
            if (id != 0)
            {
                GetConfigurationSetting();
            }
            else
            {
                GetSystemNames();
            }
        }

        //sets connectionstring for deletion
        CommonButtons.ConnectionString = ddlSystem.SelectedItem.Text;
    }

    /// <summary>
    ///   Checks whether stored procedure exists in selected system database, if so then there are Customer scrutiny rules to be viewed
    /// </summary>
    /// <param name = "storedProcedure"></param>
    /// <returns>bool that states whether stpred procedure exists</returns>
    protected bool storedProcedureExists(string storedProcedure)
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("select * FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='" + storedProcedure + "'", connection);
            SqlDataReader sdrSpExists = command.ExecuteReader();

            if (sdrSpExists.Read())
                storedProcExists = true;
            return storedProcExists;
        }
    }

    //called by button control to save scrutiny rule and redirect page
    protected void btnSaveEntity_Click(object sender, EventArgs e)
    {

        if (storedProcedureExists("usp_SAV_ConfigurationSettings"))
        {
            SaveEntity();

            Response.Redirect("ConfigurationSettings.aspx?id=" + ddlSystem.SelectedValue);
        }
        else
        {
            lblMessage.Text = "Cannot save Configuration Settings on this system";
            lblMessage.ForeColor = Color.Red;
        }
    }

    //gives value to filter jobs page by on cancel, on edit action
    protected void btnCancelEntity_Click(object sender, EventArgs e)
    {
        setCancelRedirect();
    }

    #endregion

    #region public methods

    /// <summary>
    ///   Saves configuration setting to selected database via user control Common Buttons
    /// </summary>
    public void SaveEntity()
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_SAV_ConfigurationSettings", connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@keyCategory", txtKeyCategory.Text);
            command.Parameters.AddWithValue("@keyName", txtKeyName.Text);
            command.Parameters.AddWithValue("@keyValue", txtKeyValue.Text);
            command.Parameters.AddWithValue("@keyDataType", ddlDataType.SelectedValue);
            command.Parameters.AddWithValue("@EnvironmentName", ddlEnvironmentName.SelectedValue);

            command.ExecuteNonQuery();

        }
    }

    #endregion
}