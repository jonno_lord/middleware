﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Operation
{
    public partial class BatchHistory : Page
    {
        #region Private Fields and Methods

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadGrid()
        {

            using (connection = new SqlConnection(connectionString))
            {

                //If the string cannot be converted then use the current system date
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_Batches", connection);
                command.CommandType = CommandType.StoredProcedure;

                //to allow system filter e.g for otis
                if (Request.QueryString["id"] != null)
                {
                    ddlSystems.SelectedValue = Request.QueryString["id"];
                    GetJobs();

                }

                //-- if the user selected the first item, then it represents no filtering.
                if (ddlStatusFilter.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@BatchStatus", ddlStatusFilter.SelectedValue);
                }

                if (ddlJobs.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@JobId", ddlJobs.SelectedValue);
                }

                if (ddlSystems.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@SystemId", ddlSystems.SelectedValue);
                }


                command.Parameters.AddWithValue("@Date", txtBatchDate.Text);

                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                sda.Fill(ds);
                dgrBatches.DataSource = ds.Tables[0];

                dgrBatches.DataBind();
            }
        }
        #endregion

        #region Protected Fields and Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlStatusFilter.SelectedValue = "ALL";
                txtBatchDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                GetSystems();
                LoadGrid();
            }
        }

        protected void GetSystems()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                ListItem itmSystem = new ListItem(" ALL");
                ddlSystems.Items.Add(itmSystem);

                while (sdrSystems.Read())
                {
                    itmSystem = new ListItem
                                    {
                                        Value = sdrSystems["id"].ToString(),
                                        Text = sdrSystems["ShortName"].ToString()
                                    };

                    ddlSystems.Items.Add(itmSystem);
                }
            }
        }

        protected void GetJobs()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Jobs", connection);
                command.CommandType = CommandType.StoredProcedure;

                if (ddlSystems.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("systemid", ddlSystems.SelectedValue);
                }
                //ISSUE HERE!! clearing items and passing value to filter jobs when set to all
                ddlJobs.Items.Clear();

                SqlDataReader sdrJobs = command.ExecuteReader();

                ListItem itmJob = new ListItem("ALL");
                ddlJobs.Items.Add(itmJob);

                while (sdrJobs.Read())
                {
                    itmJob = new ListItem();
                    itmJob.Value = sdrJobs["id"].ToString();
                    itmJob.Text = sdrJobs["ShortName"].ToString();

                    ddlJobs.Items.Add(itmJob);
                }
            }
        }

        protected void ddlSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGrid();
            GetJobs();

        }

        protected void dllJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Reload the Gridview according to the job and system selected
            LoadGrid();
        }

        protected void ddlFilterStatusSelect(object sender, EventArgs e)
        {
            LoadGrid();
        }

        protected void txtBatchDate_TextChanged(object sender, EventArgs e)
        {
            LoadGrid();   
        }

        protected void dgrBatches_PageIndexChanged(object sender, EventArgs e)
        {

        }
        protected void dgrBatches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrBatches.PageIndex = e.NewPageIndex;
            LoadGrid();

        }   

        #endregion
    }
}