﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BatchDuration : Page
{
    #region private fields and properties

    private static SqlConnection connection;

    private static string connectionString
    {
        get
        {
            string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
            return returnString;
        }
    }

    #endregion

    #region private methods

    private void LoadChart()
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();

            string storedProcedureName = cbxFailedFilter.Checked ? "usp_SEL_JobsFailedBatches" : "usp_SEL_JobsBatches";

            SqlCommand command = new SqlCommand(storedProcedureName, connection);
            command.CommandType = CommandType.StoredProcedure;

            if (cbxFailedFilter.Checked)
            {
                command.Parameters.AddWithValue("@startDate", txtBatchDate.Text);
                command.Parameters.AddWithValue("@endDate", txtBatchDate.Text);
            }

            SqlDataReader sdr = command.ExecuteReader();

            rptJobs.DataSource = sdr;           
            rptJobs.DataBind();
            
        }
    }

    //needs to be bound separatly otherwise causes ItemDataBoundConflict
    private void BindNames()
    {
               using (connection = new SqlConnection(connectionString))
        {
            connection.Open();

            string storedProcedureName = cbxFailedFilter.Checked ? "usp_SEL_JobsFailedBatches" : "usp_SEL_JobsBatches";

            SqlCommand command = new SqlCommand(storedProcedureName, connection);
            command.CommandType = CommandType.StoredProcedure;

            if (cbxFailedFilter.Checked)
            {
                command.Parameters.AddWithValue("@startDate", txtBatchDate.Text);
                command.Parameters.AddWithValue("@endDate", txtBatchDate.Text);
            }

            SqlDataReader sdr = command.ExecuteReader();

            rptNames.DataSource = sdr;           
            rptNames.DataBind();
            
        }

    }
    private void LoadTime()
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_Sel_Timeintervals", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader sdr = command.ExecuteReader();

            rptTime.DataSource = sdr;

            rptTime.DataBind();
        }
    }


    #endregion

    #region protected methods and events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtBatchDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            LoadTime();
            BindNames();
            LoadChart();
        }
    }

    protected void rptJob_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_BatchDuration", connection);
                command.CommandType = CommandType.StoredProcedure;

                object jobid = DataBinder.Eval(e.Item.DataItem, "id");

                command.Parameters.AddWithValue("@jobid", jobid);
                command.Parameters.AddWithValue("@startDate", txtBatchDate.Text);
                command.Parameters.AddWithValue("@endDate", txtBatchDate.Text);

                SqlDataReader sdr = command.ExecuteReader();

                Repeater rptJobDuration = (Repeater)item.FindControl("rptJobDuration");

                rptJobDuration.DataSource = sdr;

                rptJobDuration.DataBind();

            }
        }
    }

    protected void rptJobDuration_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "failed":

                if (e.CommandArgument.ToString() != "")
                {
                    Response.Redirect("~/Pages/Operation/BatchDetails.aspx?id=" + e.CommandArgument);
                }
                break;

            default:

                //do nothing

                break;
        }
    }

    protected void txtBatchDate_TextChanged(object sender, EventArgs e)
    {
        LoadChart();

    }

    protected void cbxFailedFilter_Checked(object sender, EventArgs e)
    {
        BindNames();
        LoadChart();
    }

    

    #endregion
}