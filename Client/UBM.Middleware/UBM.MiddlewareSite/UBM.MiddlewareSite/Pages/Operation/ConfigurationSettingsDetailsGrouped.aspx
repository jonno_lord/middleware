﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="ConfigurationSettingsDetailsGrouped" Codebehind="ConfigurationSettingsDetailsGrouped.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Configuration Settings</h1>
</div>
<div class="validationSummary">
    <div class="margin">
        <p><asp:label ID="lblMessage" runat="server"/></p>
    </div>
    <asp:ValidationSummary ID="vdsConfigurationSettings" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div id="DetailsGrouped">
    <div class="Labels Environment DevHeader"><h1>DEV</h1></div>
    <div class="Environment rightHeader"><h1>UAT</h1></div>
    <div class="Environment rightHeader"><h1>PROD</h1></div>
    <div class="Environment Labels">
        <div class="addEntryForm">

             <div class="formRow">
                   <asp:Label ID="lblSystem" runat="server" cssClass="formMargin labelInput" Text="System:" Width="90px"/>
                   <asp:DropDownList ID="ddlSystemDev" runat="server" cssClass="formMargin" ToolTip="System for Configuration Setting to Apply to"  Width="250" enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSystemDev_Changed"/>
            </div>

            <div class="formRow">
                   <asp:Label ID="lblKeyCategory" runat="server" cssClass="formMargin labelInput" Text="Key Category:" Width="90px"/>
                   <asp:TextBox ID="txtKeyCategoryDev" runat="server" cssClass="formMargin" ToolTip="Key Category for Configuration Setting DEV" MaxLength="50" Width="250" AutoPostBack="true" OnTextChanged="txtCatDev_TextChanged"/>
                <asp:RequiredFieldValidator ID="rfvKeyCategory" runat="server"  ControlToValidate="txtKeyCategoryDev" ErrorMessage="Key Category Name Required">
                *
                </asp:RequiredFieldValidator>
            </div>

             <div class="formRow">
                   <asp:Label ID="lblKeyName" runat="server" cssClass="formMargin labelInput" Text="Key Name:" Width="90px"/>
                   <asp:TextBox ID="txtKeyNameDev" runat="server" cssClass="formMargin" ToolTip="Key Name for Configuration Setting DEV" MaxLength="50" Width="250" AutoPostBack="true" OnTextChanged="txtNameDev_TextChanged"/>
                <asp:RequiredFieldValidator ID="rfvKeyName" runat="server"  ControlToValidate="txtKeyNameDev" ErrorMessage="Key Name Required for DEV">
                *
                </asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
                <asp:Label ID="lblKeyValue" runat="server" cssClass="formMargin labelInput" Text="Value:" Width="90px"/>
                <asp:TextBox ID="txtKeyValueDev" runat="server" cssClass="formMargin" ToolTip="Value for Configuration Setting DEV" TextMode="MultiLine" Columns="38" Rows="2"/>
                <asp:RegularExpressionValidator ID="revKeyValue" ControlToValidate="txtKeyValueDev" ErrorMessage="Description is exceeding 2048 characters for DEV" ValidationExpression="^[\s\S]{0,2048}$" runat="server" >
                *
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvKeyValue" runat="server" ControlToValidate="txtKeyValueDev" ErrorMessage="Value is Required for DEV">
                *
                </asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
                <asp:Label ID="lblDataType" runat="server" cssClass="formMargin labelInput" Text="Data Type:" Width="90px"/>
                    <asp:DropDownList ID="ddlDataTypeDev" cssClass="formMargin" runat="server" tooltip="Data Type of Setting DEV" Width="250" AutoPostBack="true" OnSelectedIndexChanged="ddlDataTypeDev_Changed">
                        <asp:ListItem>Boolean</asp:ListItem>
                        <asp:ListItem>DateTime</asp:ListItem>
                        <asp:ListItem>Double</asp:ListItem>
                        <asp:ListItem>Integer</asp:ListItem>
                        <asp:ListItem>String</asp:ListItem>
                    </asp:DropDownList>
            </div>

            <div class="formRow">
                   <asp:Label ID="lblEnvironmentName" runat="server" cssClass="formMargin labelInput" Text="Environment Name:" Width="90px"/>
                  <asp:DropDownList ID="ddlEnvironmentNameDev" cssClass="formMargin" runat="server" tooltip="Name of Environment setting applies to" Width="250" >
                    <asp:ListItem>DEV</asp:ListItem>
                    <asp:ListItem>UAT</asp:ListItem>
                    <asp:ListItem>PROD</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvEnvironmentName" runat="server"  ControlToValidate="ddlEnvironmentNameDev" ErrorMessage="Environment Name Required for DEV">
                *
                </asp:RequiredFieldValidator>
            </div>

        </div>
    </div>
    <div class="Environment">
        <div class="addEntryForm">

             <div class="formRow">
                   <asp:DropDownList ID="ddlSystemUat" runat="server" cssClass="formMargin" ToolTip="System for Configuration Setting to Apply to"  Width="250" enabled="false" AutoPostBack="true"  OnSelectedIndexChanged="ddlSystemUat_Changed"/>
            </div>

            <div class="formRow">
                <asp:TextBox ID="txtKeyCategoryUat" runat="server" cssClass="formMargin" ToolTip="Key Category for Configuration Setting UAT" MaxLength="50" Width="250" AutoPostBack="true" OnTextChanged="txtCatUat_TextChanged"/>
                <asp:RequiredFieldValidator ID="rfvKeyCategoryUat" runat="server"  ControlToValidate="txtKeyCategoryUat" ErrorMessage="Key Category Name Required for UAT">
                *
                </asp:RequiredFieldValidator>
            </div>

             <div class="formRow">
                   <asp:TextBox ID="txtKeyNameUat" runat="server" cssClass="formMargin" ToolTip="Key Name for Configuration Setting UAT" MaxLength="50" Width="250" AutoPostBack="true" OnTextChanged="txtNameUat_TextChanged"/>
                <asp:RequiredFieldValidator ID="rfvKeyNameUat" runat="server"  ControlToValidate="txtKeyNameUat" ErrorMessage="Key Name Required for UAT">
                *
                </asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
                <asp:TextBox ID="txtKeyValueUat" runat="server" cssClass="formMargin" ToolTip="Value for Configuration Setting UAT" TextMode="MultiLine" Columns="38" Rows="2"/>
                <asp:RegularExpressionValidator ID="revKeyValueUat" ControlToValidate="txtKeyValueUat" ErrorMessage="Description is exceeding 2048 characters for UAT" ValidationExpression="^[\s\S]{0,2048}$" runat="server" >
                *
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvKeyValueUat" runat="server" ControlToValidate="txtKeyValueUat" ErrorMessage="Value is Required for UAT">
                *
                </asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
                    <asp:DropDownList ID="ddlDataTypeUat" cssClass="formMargin" runat="server" tooltip="Data Type of Setting UAT" Width="250" AutoPostBack="true" OnSelectedIndexChanged="ddlDataTypeUat_Changed">
                        <asp:ListItem>Boolean</asp:ListItem>
                        <asp:ListItem>DateTime</asp:ListItem>
                        <asp:ListItem>Double</asp:ListItem>
                        <asp:ListItem>Integer</asp:ListItem>
                        <asp:ListItem>String</asp:ListItem>
                    </asp:DropDownList>
            </div>

            <div class="formRow">
                <asp:DropDownList ID="ddlEnvironmentNameUat" cssClass="formMargin" runat="server" tooltip="Name of Environment setting applies to" Width="250">
                    <asp:ListItem>DEV</asp:ListItem>
                    <asp:ListItem>UAT</asp:ListItem>
                    <asp:ListItem>PROD</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvEnvironmentUat" runat="server"  ControlToValidate="ddlEnvironmentNameUat" ErrorMessage="Environment Name Required for UAT">
                *
                </asp:RequiredFieldValidator>
            </div>

        </div>
    </div>
    <div class="Environment">
        <div class="addEntryForm">

             <div class="formRow">
                   <asp:DropDownList ID="ddlSystemProd" runat="server" cssClass="formMargin" ToolTip="System for Configuration Setting to Apply to"  Width="250" enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSystemProd_Changed"/>
            </div>

            <div class="formRow">
                <asp:TextBox ID="txtKeyCategoryProd" runat="server" cssClass="formMargin" ToolTip="Key Category for Configuration Setting PRODUCTION" MaxLength="50" Width="250" AutoPostBack="true" OnTextChanged="txtCatProd_TextChanged"/>
                <asp:RequiredFieldValidator ID="rfvKeyCategoryProd" runat="server"  ControlToValidate="txtKeyCategoryProd" ErrorMessage="Key Category Name Required for PRODUCTION">
                *
                </asp:RequiredFieldValidator>
            </div>

             <div class="formRow">
                   <asp:TextBox ID="txtKeyNameProd" runat="server" cssClass="formMargin" ToolTip="Key Name for Configuration Setting PRODUCTION" MaxLength="50" Width="250" AutoPostBack="true" OnTextChanged="txtNameProd_TextChanged"/>
                <asp:RequiredFieldValidator ID="rfvKeyNameProd" runat="server"  ControlToValidate="txtKeyNameProd" ErrorMessage="Key Name Required for PRODUCTION">
                *
                </asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
                <asp:TextBox ID="txtKeyValueProd" runat="server" cssClass="formMargin" ToolTip="Value for Configuration Setting PRODUCTION" TextMode="MultiLine" Columns="38" Rows="2"/>
                <asp:RegularExpressionValidator ID="revKeyValueProd" ControlToValidate="txtKeyValueProd" ErrorMessage="Description is exceeding 2048 characters for PRODUCTION" ValidationExpression="^[\s\S]{0,2048}$" runat="server" >
                *
                </asp:RegularExpressionValidator>  
                <asp:RequiredFieldValidator ID="rfvKeyValueProd" runat="server" ControlToValidate="txtKeyValueProd" ErrorMessage="Value is Required for PRODUCTION">
                *
                </asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
                    <asp:DropDownList ID="ddlDataTypeProd" cssClass="formMargin" runat="server" tooltip="Data Type of Setting PRODUCTION" Width="250" AutoPostBack="true" OnSelectedIndexChanged="ddlDataTypeProd_Changed">
                        <asp:ListItem>Boolean</asp:ListItem>
                        <asp:ListItem>DateTime</asp:ListItem>
                        <asp:ListItem>Double</asp:ListItem>
                        <asp:ListItem>Integer</asp:ListItem>
                        <asp:ListItem>String</asp:ListItem>
                    </asp:DropDownList>
            </div>

            <div class="formRow">

                 <asp:DropDownList ID="ddlEnvironmentNameProd" cssClass="formMargin" runat="server" tooltip="Name of Environment setting applies to" Width="250">
                    <asp:ListItem>DEV</asp:ListItem>
                    <asp:ListItem>UAT</asp:ListItem>
                    <asp:ListItem>PROD</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvEnvironmentNameProd" runat="server"  ControlToValidate="ddlEnvironmentNameProd" ErrorMessage="Environment Name Required for PRODUCTION">
                *
                </asp:RequiredFieldValidator>
            </div>

        </div>
    </div>
</div>
<div id="editButtons">
    <div class="buttonAlignLeft">
        <asp:LinkButton ID="lnkDelete" CssClass="glassbutton" runat="server" CausesValidation="False" OnClientClick="return confirm('Are you sure you wish to delete?');" onClick="btnDeleteEntity_Click"><asp:Image ID="imgDelete" ImageUrl="~/images/deleteicon.png" runat="server" AlternateText="Delete"/><p>Delete Settings</p></asp:LinkButton>
    </div>
    <div class="buttonAlignRight" style="margin-right: 20px;">
        <asp:LinkButton ID="lnkCancel" CssClass="glassbutton" runat="server" OnClick="btnBack_Click" CausesValidation="False"><asp:Image ID="Image1" ImageUrl="~/images/backicon.png" runat="server" AlternateText="Cancel"/><p>Cancel Settings</p></asp:LinkButton>
        <asp:LinkButton ID="lnkSubmit" CssClass="glassbutton glassbuttonedit" runat="server" OnClick="btnSaveEntity_Click"><asp:Image ID="imgSubmit" runat="server" /><p><asp:Literal ID="litActionName" runat="server"></asp:Literal> Settings</p></asp:LinkButton>
    </div>
</div>
</asp:Content>

