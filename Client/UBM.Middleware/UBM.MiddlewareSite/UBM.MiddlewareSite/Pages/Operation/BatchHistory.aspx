﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Operation.BatchHistory" Codebehind="BatchHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Batch History</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
        <asp:label ID="lblDate" runat="server" Text="Date:" class="labelMargin formRowNoClear"/>
        <asp:TextBox id="txtBatchDate" runat="server" ClientIDMode="Static" OnTextChanged="txtBatchDate_TextChanged" AutoPostBack="true" class="formMargin formRowNoClear"/>
        <asp:DropDownList ID="ddlSystems" runat="server" DataTextField="ShortName" 
            DataValueField="ShortName" onselectedindexchanged="ddlSystem_SelectedIndexChanged" 
            AutoPostBack="true" ToolTip="Select a System" class="formMargin formRowNoClear" />
        <asp:DropDownList ID="ddlJobs" runat="server" DataTextField="ShortName" DataValueField="ShortName"
            onselectedindexchanged="dllJob_SelectedIndexChanged"
            AutoPostBack="true" ToolTip="Select a Job" class="formMargin formRowNoClear" />
        <asp:label ID="lblStatus" runat="server" Text="Status:" class="labelMargin formRowNoClear"/>
        <asp:DropDownList ID="ddlStatusFilter" runat="server" ToolTip="Filter by Status" OnSelectedIndexChanged="ddlFilterStatusSelect" 
        AutoPostBack="True" class="formMargin formRowNoClear">
              <asp:ListItem>ALL</asp:ListItem>
            <asp:ListItem>COMPLETE</asp:ListItem>
            <asp:ListItem>CREATING</asp:ListItem>
            <asp:ListItem>DISABLED</asp:ListItem>
            <asp:ListItem>FAILED</asp:ListItem>
            <asp:ListItem>PROCESSING</asp:ListItem>
            <asp:ListItem>WAITING</asp:ListItem>
        </asp:DropDownList>

            <asp:GridView ID="dgrBatches" runat="server" AutoGenerateColumns="False" DataKeyNames="id" EnableModelValidation="True" CellPadding="4" 
                EnableSortingAndPagingCallbacks="True"  AllowPaging="True" onpageindexchanged="dgrBatches_PageIndexChanged" onpageindexchanging="dgrBatches_PageIndexChanging" PageSize="30" cssClass="break">
                <emptydatatemplate>
                    No Batch History data found based on filters applied
                </emptydatatemplate>
                <Columns>        
                    <asp:HyperlinkField DataTextField="Status" HeaderText="Status" 
                        SortExpression="Status" DataNavigateUrlFields="id" ItemStyle-CssClass="GridCentre" DataNavigateUrlFormatString="batchDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
                    <asp:HyperlinkField DataTextField="id" HeaderText="Batch Number" 
                        SortExpression="id" DataNavigateUrlFields="id" ItemStyle-CssClass="GridCentre" DataNavigateUrlFormatString="batchDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
                    <asp:HyperlinkField DataTextField="SystemName" HeaderText="System" 
                        SortExpression="SystemName" DataNavigateUrlFields="id" DataNavigateUrlFormatString="batchDetails.aspx?id={0}"/>
                    <asp:HyperlinkField HeaderText="Job" SortExpression="Job" DataTextField="Job" DataNavigateUrlFields="id" DataNavigateUrlFormatString="batchDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
                    <asp:HyperlinkField HeaderText="Start Date" 
                        SortExpression="StartDate" DataNavigateUrlFields="id"  ItemStyle-CssClass="GridCentre" DataTextField="StartDate" DataNavigateUrlFormatString="batchDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
                    <asp:HyperlinkField HeaderText="End Date" 
                        SortExpression="EndDate" DataTextField="EndDate" DataNavigateUrlFields="id" ItemStyle-CssClass="GridCentre" DataNavigateUrlFormatString="batchDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
                    <asp:HyperlinkField HeaderText="Duration in Seconds" 
                    SortExpression="DurationSeconds" DataTextField="DurationSeconds" DataNavigateUrlFields="id" ItemStyle-CssClass="GridCentre" DataNavigateUrlFormatString="batchDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
                </Columns>
                <PagerStyle CssClass="pageIndex" />
            </asp:GridView>
<script type="text/javascript">  
    $(function () {
        $("#txtBatchDate").datepicker({ dateFormat: 'yy-mm-dd' });
    });
</script>
</asp:Content>

