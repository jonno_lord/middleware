﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Operation.BatchDetails" Codebehind="BatchDetails.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Batch Summary</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div class="Batchheader">
    <asp:Image ID="imgStatus" ImageUrl="~/Images/BatchStatus/complete.png" runat="server" class="Batchicon"/>
    <div class="BatchSummaryHeader">
        <div class="BatchDetails"> 

            <h2><asp:label ID="lblJobTitle" runat="server" Text=""/></h2>
            <div class="entry">
                <h1>Batch Number:</h1>
                <asp:label ID="lblBatchNumber" runat="server" Text=""/>
            </div> 

            <div class="entry"> 
                <h1>Start Date:</h1>
                <asp:label ID="lblStartDate" runat="server" Text=""/>
            </div>

            <div class="entry">
                <h1>Start Time:</h1>
                <asp:label ID="lblStartTime" runat="server" Text=""/>
            </div>

            <div class="entry">
                <h1>End Time:</h1>
                <asp:label ID="lblEndTime" runat="server" Text=""/>
            </div>

            <div class="entry">
                <h1>Duration:</h1>
                <asp:label ID="lblDuration" runat="server" Text=""/>
            </div>
    </div>
</div>
</div>
<div class="filters">
<asp:checkbox ID="cbxLogLevelFilter" runat="server" ToolTip="Filter Log Level" OnCheckedChanged="cbxLogFilter" AutoPostBack="true" Checked="true" Text="Ignore all non-critical log entries: " TextAlign="Left"/>
</div>
<asp:UpdatePanel ID="updBatches" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView ID="dgrBatchDetails" runat="server" AllowSorting="True" AutoGenerateColumns="false" EnableSortingAndPagingCallbacks="True"  AllowPaging="True" onpageindexchanging="dgrBatchDetails_PageIndexChanging" PageSize="30">
        <emptydatatemplate>
        <p>No Batch data found</p>
        </emptydatatemplate>
    <Columns>
           <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" ItemStyle-CssClass="GridCentre"/>
           <asp:BoundField DataField="LogLevel" HeaderText="Class" ItemStyle-CssClass="GridCentre"/>
           <asp:BoundField DataField="ClassName" HeaderText="Level" ItemStyle-CssClass="GridCentre"/>
           <asp:BoundField DataField="Description" HeaderText="Description"/>
    </Columns>
      <PagerStyle CssClass="pageIndex" />
</asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="cbxLogLevelFilter" EventName="CheckedChanged" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>

