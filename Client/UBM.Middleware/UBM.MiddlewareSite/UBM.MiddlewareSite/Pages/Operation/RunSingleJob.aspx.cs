﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Operation
{
    public partial class RunSingleJob:Page
    {
        #region Private Fields and Methods

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        private void GetSystemNames()
        {
            
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                ListItem itmSystem = new ListItem("System");
                ddlSystemFilter.Items.Add(itmSystem);

                while (sdrSystems.Read())
                {
                    itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    ddlSystemFilter.Items.Add(itmSystem);
                }
            }
        }


        private void GetJobsBySystemName()
        {
            //Loads all Jobs based on the System selected in ddlSystemFilter
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Jobs", connection);

                if (ddlSystemFilter.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@systemid", ddlSystemFilter.SelectedValue);
                }
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                ListItem itmJobs = new ListItem("Job");
                ddlSystemFilter.Items.Add(itmJobs);

                while (sdr.Read())
                {
                    itmJobs = new ListItem();
                    itmJobs.Value = sdr["id"].ToString();
                    itmJobs.Text = sdr["ShortName"].ToString();

                    ddlJobsFilter.Items.Add(itmJobs);
                }
            }

        }

        private string GetBatchNo(string jobid)
        {
            //Gets Batch Details for currently running job.
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_BatchNumber", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@jobid", jobid);

                string batchNo = command.ExecuteScalar().ToString();
                return batchNo;
            }

        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
                    
                if (!IsPostBack)
                {
                    GetSystemNames();
                    
                }
        }

        protected void ddlSystemFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlJobsFilter.Visible = true;
            btnRunJob.Visible = true;
            lblJob.Visible = true;
            GetJobsBySystemName();
        }

        protected void ddlJobFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = ddlJobsFilter.SelectedValue;
            btnRunJob.Attributes.Add("onclick", string.Format("RunJob('" + id + "')"));
        }

        protected void btnRunJob_Click(object sender, EventArgs e)
        {

            string batchNo = GetBatchNo(ddlJobsFilter.SelectedValue);
            GetBatchDetails(batchNo);
            LoadGrid(batchNo);
        }

        protected void GetBatchDetails(string batchNo)
        {
            //Gets Batch Details for currently running job.
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Batch", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@batchid", batchNo);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {

                    lblBatchNumber.Text = batchNo;
                    lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", sdr["StartDate"]);
                    lblStartTime.Text = String.Format("{0:HH:mm:ss}", sdr["StartDate"]);
                    lblEndTime.Text = String.Format("{0:HH:mm:ss}", sdr["EndDate"]);
                    lblDuration.Text = sdr["DurationSeconds"].ToString();
                    imgStatus.ImageUrl = "~/images/BatchStatus/" + sdr["status"] + ".png";


                }
            }

        }


        ////load grid if there is a batch number supplied in query string
        private void LoadGrid(string batchid)
        {

            if (batchid != "")
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("usp_SEL_BatchLogs", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@batchid", batchid);

                    SqlDataAdapter sda = new SqlDataAdapter(command);

                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    dgrBatchDetails.DataSource = ds.Tables[0];

                    dgrBatchDetails.DataBind();

                }
            }
        }

        protected void tmrGrid_Tick (object sender, EventArgs e)
        {
            string batchNo = GetBatchNo(ddlJobsFilter.SelectedValue);

            if (batchNo != "")
            {
                LoadGrid(batchNo);
            
            }
        }

        protected void tmrBatchHeader_Tick (object sender, EventArgs e)
        {
            string batchNo = GetBatchNo(ddlJobsFilter.SelectedValue);

            if (batchNo != "")
            {
                GetBatchDetails(batchNo);
            }
        }
}
}