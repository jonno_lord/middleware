﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="ConfigurationSettings" Codebehind="ConfigurationSettings.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
     <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Setting"  Title="MW"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
<div class="filter">
        <div class="rowA">
            <p>System:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList id="ddlFilterSystems" runat="server" tooltip="System for Configuration Settings" onselectedindexchanged="ddlFilterSystemSelect" AutoPostBack="True"/>
        </div>
        <div class="rowC">
         <asp:UpdatePanel ID="updLabel" runat="server" >
            <ContentTemplate>
               <asp:Label ID="lblMessage" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
     <uc:CommonActions ID="CommonActions" runat="server" EntityName="Settings" TableName="ConfigurationSettings" DisableEnable="false"/>
<div class="filter">
        <div class="rowB">
         Full View: <asp:RadioButton ID="rdoConfiguration" runat="server" CssClass="IgnoreCheckbox" GroupName="configuration" AutoPostBack="true" oncheckedchanged="rdoConfiguration_CheckedChanged"/>
         Grouped View:<asp:RadioButton ID="rdoGrouped" runat="server" CssClass="IgnoreCheckbox" GroupName="configuration" AutoPostBack="true"  oncheckedchanged="rdoConfiguration_CheckedChanged"/>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Configuration Settings</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
<asp:Panel ID="pnlConfiguration" runat="server">
 <asp:UpdatePanel ID="updConfiguration" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="uppConfiguration" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updConfiguration">
                <ProgressTemplate>
                     <div class="progressIndicator"> Loading...<asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptConfiguration_ItemCommand">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Key Category</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Data Type</th>
                        <th>Environment</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Configuration Setting " +Eval("keyName")%>'/></td>
                      <td class="GridCentre"><%# Eval("keyCategory") %></td>
                      <td class="GridCentre"><%# Eval("keyName") %></td>
                      <td class="GridCentre"><%# Eval("keyValue") %></td>
                      <td class="GridCentre"><%# Eval("keyDataType") %></td>
                      <td class="GridCentre"><%# Eval("EnvironmentName") %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlConfigurationGrouped" runat="server">
<asp:UpdatePanel ID="updConfigurationGrouped" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="uppConfigurationGrouped" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updConfiguration">
                <ProgressTemplate>
                     <div class="progressIndicator"> Loading...<asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Repeater ID="rptGridGrouped" runat="server" OnItemCommand="rptConfigurationGrouped_ItemCommand">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Key Category</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("keyCategory") + ","+ Eval( ("keyName"))%>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Configuration Setting" +Eval("KeyName")%>'/></td>
                      <td class="GridCentre"><%# Eval("keyCategory") %></td>
                      <td class="GridCentre"><%# Eval("keyName") %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>
</asp:Content>

