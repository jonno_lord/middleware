﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Operation.RunJob" Codebehind="RunJob.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/JavaScript/RunJobs.js") %> "></script> 

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Job Runner</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
    <div class="filters">
        System: <asp:DropDownList ID="ddlSystemFilter" runat="server" CssClass="ComponentInline"
        ToolTip="System Job belongs to" onselectedindexchanged="ddlFilterSystemSelect" AutoPostBack="True"/>
    </div>
        <asp:GridView ID="dgrJobs" runat="server" AutoGenerateColumns="False" 
        ClientIDRowSuffix="id"
        ClientIDMode="Static"
        DataKeyNames="id" EnableModelValidation="True" cssClass="GridStyle" onrowdatabound="dgrJobs_RowDataBound"  AllowPaging="True" onpageindexchanged="dgrRun_PageIndexChanged" onpageindexchanging="dgrRun_PageIndexChanging" PageSize="30" OnRowCommand="dgrJobs_RowCommand">
        <EmptyDataTemplate>
        No Jobs found for specified System
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderText="Status" ItemStyle-Width="35px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                <ItemTemplate>
                    <image id="imgStatus_<%# Eval("id")%>" class="imgStatus" src="../../Images/JobStatus/<%# Eval("Status")%>.png" alt="<%# Eval("Status")%>"/>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:BoundField DataField="SystemName" ItemStyle-Width="105px" HeaderText="System" SortExpression="SystemName"  ControlStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre"/>
                 <asp:BoundField DataField="id" ItemStyle-Width="35px" HeaderText="Job ID" SortExpression="id" HeaderStyle-CssClass="GridCentre"/>
                <asp:BoundField DataField="ShortName" ItemStyle-Width="155px" HeaderText="Job" SortExpression="ShortName" HeaderStyle-CssClass="GridCentre"/>
                <asp:TemplateField HeaderText="Status" ItemStyle-Width="85px" ItemStyle-CssClass="GridCentre">
                <ItemTemplate>
                    <span id="lblJobstatus_<%# Eval("id")%>"><%# Eval("status")%></span>
                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField ItemStyle-Width="95px" HeaderText="Last Executed">
                    <ItemTemplate>
                        <span id="lblLastExecuted_<%# Eval("id")%>"><%# Eval("LastExecuted")%></span>
                    </ItemTemplate>
                </asp:TemplateField>

            <asp:TemplateField HeaderText="Status" ItemStyle-Width="30px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                <ItemTemplate>
                    <asp:Button id="btnDisabled" runat="server" CommandName="btnDisable" CommandArgument='<%# Eval("id") %>' Width="55"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start" ItemStyle-Width="40px" ItemStyle-CssClass="GridCentre RunningJobDetails">
                <ItemTemplate>
                    <a href="javascript:void(0);" id="btnRunJob_<%# Eval("id")%>" onclick="javascript:RunJob('<%# Eval("id")%>');">RUN</a> 
                    <img src="/images/loading.gif" style="display:none;padding-left:14px" id="imgWait_<%# Eval("id")%>" alt="Please wait"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Kill" ItemStyle-Width="40px" ItemStyle-CssClass="GridCentre">
                <ItemTemplate>
                    <asp:LinkButton id="btnKillJob"  runat="server" Text="Kill" ToolTip='<%# "Abort Job " +Eval("ShortName")%>' CausesValidation="False" />
                </ItemTemplate>
            </asp:TemplateField>
               <asp:TemplateField HeaderText="Details" ItemStyle-Width="45px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                   <ItemTemplate>
                        <asp:Linkbutton ID="hypStatus" runat="server" Text="Info" ToolTip="Most Recent Batch Information" CommandName="btnStatus" CommandArgument='<%# Eval("id") %>'/>
                   </ItemTemplate>
               </asp:TemplateField>
        </Columns>
        </asp:GridView>

        <script type="text/javascript" language="javascript">

            $(document).ready(function () {
                setInterval(updateGrid, 1000);
            });
        </script>

</asp:Content>

