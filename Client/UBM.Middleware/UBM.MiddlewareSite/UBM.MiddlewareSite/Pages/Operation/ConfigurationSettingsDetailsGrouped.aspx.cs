﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

public partial class ConfigurationSettingsDetailsGrouped : Page
{
    #region private fields and properties

    private static SqlConnection connection;

    private bool storedProcExists;

    private static string systemString
    {
        get
        {
            string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
            string returnString = RemoveProviders.RemoveProvider(unchangedString);
            return returnString;
        }
    }

    /// <summary>
    ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
    ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
    /// </summary>
    private string category
    {
        get
        {
            string returnValue = Request.QueryString["cat"];
            return returnValue;
        }
    }

    private string name
    {
        get
        {
            string returnValue = Request.QueryString["name"];
            return returnValue;
        }
    }

    private int systemid
    {
        get
        {
            int returnValue;
            int.TryParse(Request.QueryString["systemid"], out returnValue);
            return returnValue;
        }
    }

    private int idDev { get; set; }

    private int idUat { get; set; }

    private int idProd { get; set; }

    private string connectionString
    {
        get
        {
            string unchangedString = WebConfigurationManager.ConnectionStrings[ddlSystemDev.SelectedItem.Text + "ConnectionString"].ConnectionString;
            string returnString = RemoveProviders.RemoveProvider(unchangedString);
            return returnString;
        }
    }
    #endregion

    #region private methods

    /// <summary>
    ///   Gets configuration settings for dev, uat and productiuon based on key categoryName and name matches
    /// </summary>
    private void GetConfigurationSettings()
    {
        GetSystemNames();

        if (storedProcedureExists("usp_GET_ConfigurationSettingGrouped"))
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_ConfigurationSettingGrouped", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@category", category);
                command.Parameters.AddWithValue("@name", name);

                SqlDataReader sdr = command.ExecuteReader();
                while (sdr.Read())
                {
                    if (sdr["EnvironmentName"].ToString() == "DEV")
                    {
                        idDev = int.Parse(sdr["id"].ToString());
                        txtKeyCategoryDev.Text = sdr["keyCategory"].ToString();
                        txtKeyNameDev.Text = sdr["keyName"].ToString();
                        txtKeyValueDev.Text = sdr["keyValue"].ToString();
                        ddlDataTypeDev.SelectedValue = sdr["keyDataType"].ToString();
                        ddlEnvironmentNameDev.SelectedValue = sdr["EnvironmentName"].ToString();
                    }
                    else if (sdr["EnvironmentName"].ToString() == "UAT")
                    {
                        idUat = int.Parse(sdr["id"].ToString());
                        txtKeyCategoryUat.Text = sdr["keyCategory"].ToString();
                        txtKeyNameUat.Text = sdr["keyName"].ToString();
                        txtKeyValueUat.Text = sdr["keyValue"].ToString();
                        ddlDataTypeUat.SelectedValue = sdr["keyDataType"].ToString();
                        ddlEnvironmentNameUat.SelectedValue = sdr["EnvironmentName"].ToString();
                    }
                    else if (sdr["EnvironmentName"].ToString() == "PROD")
                    {
                        idProd = int.Parse(sdr["id"].ToString());
                        txtKeyCategoryProd.Text = sdr["keyCategory"].ToString();
                        txtKeyNameProd.Text = sdr["keyName"].ToString();
                        txtKeyValueProd.Text = sdr["keyValue"].ToString();
                        ddlDataTypeProd.SelectedValue = sdr["keyDataType"].ToString();
                        ddlEnvironmentNameProd.SelectedValue = sdr["EnvironmentName"].ToString();
                    }
                }
            }
        }
        else
        {
            lblMessage.Text = "No stored procedure exists for retrieving records";
            lblMessage.ForeColor = Color.Red;
        }
    }

    private void defineAction()
    {

        //sets environments for each sub form
        ddlEnvironmentNameDev.SelectedIndex = 0;
        ddlEnvironmentNameUat.SelectedIndex = 1;
        ddlEnvironmentNameProd.SelectedIndex = 2;

        if (name != null)
        {
            //if it is an edit action
            litActionName.Text = "Save ";
            imgSubmit.ImageUrl = "~/images/editicon.png";
            imgSubmit.AlternateText = "Edit";

            //tooltips
            lnkSubmit.ToolTip = "Save Setting Edit";
            lnkCancel.ToolTip = "Cancel Setting Edit";
            lnkDelete.ToolTip = "Delete this Setting";

            ddlSystemDev.Enabled = false;
            ddlSystemUat.Enabled = false;
            ddlSystemProd.Enabled = false;

            ddlEnvironmentNameDev.Enabled = false;
            ddlEnvironmentNameUat.Enabled = false;
            ddlEnvironmentNameProd.Enabled = false;
        }
        else
        {
            ddlEnvironmentNameDev.Enabled = false;
            ddlEnvironmentNameUat.Enabled = false;
            ddlEnvironmentNameProd.Enabled = false;

            //if it is an add action
            litActionName.Text = "Add ";
            imgSubmit.ImageUrl = "~/images/addicon.png";
            imgSubmit.AlternateText = "Add";

            //tooltips
            lnkSubmit.ToolTip = "Add a new Setting";
            lnkCancel.ToolTip = "Cancel Adding New Setting";

            //hides delete button on add
            lnkDelete.Visible = false;
            imgDelete.Visible = false;

        }
    }

    private void GetSystemNames()
    {

        using (connection = new SqlConnection(systemString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlDataReader sdrSystems = command.ExecuteReader();

            while (sdrSystems.Read())
            {
                ListItem itmSystem = new ListItem();
                itmSystem.Value = sdrSystems["id"].ToString();
                itmSystem.Text = sdrSystems["ShortName"].ToString();

                if (systemid != 0)
                {
                    if (itmSystem.Value == systemid.ToString())
                    {
                        itmSystem.Selected = true;
                    }
                }
                else
                {
                    if (Session["SettingSystem"] != null)
                    {
                        if (itmSystem.Value == Session["SettingSystem"].ToString())
                        {
                            itmSystem.Selected = true;
                        }
                    }
                }

                ddlSystemDev.Items.Add(itmSystem);
                ddlSystemUat.Items.Add(itmSystem);
                ddlSystemProd.Items.Add(itmSystem);

            }

        }

    }


    private void DeleteEntity()
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_DEL_ConfigurationSettings", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@category", category);
            command.Parameters.AddWithValue("@name", name);

            command.ExecuteNonQuery();
        }
    }

    private void SaveEntity()
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_SAV_ConfigurationSettingsEnvironment", connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@Category", txtKeyCategoryDev.Text);
            command.Parameters.AddWithValue("@Name", txtKeyNameDev.Text);
            command.Parameters.AddWithValue("@EnvironmentNameDev", ddlEnvironmentNameDev.SelectedValue);
            command.Parameters.AddWithValue("@EnvironmentNameUat", ddlEnvironmentNameUat.SelectedValue);
            command.Parameters.AddWithValue("@EnvironmentNameProd", ddlEnvironmentNameProd.SelectedValue);

            command.Parameters.AddWithValue("@idDev", idDev);
            command.Parameters.AddWithValue("@keyValueDev", txtKeyValueDev.Text);
            command.Parameters.AddWithValue("@keyDataTypeDev", ddlDataTypeDev.SelectedValue);

            command.Parameters.AddWithValue("@idUat", idUat);
            command.Parameters.AddWithValue("@keyValueUat", txtKeyValueUat.Text);
            command.Parameters.AddWithValue("@keyDataTypeUat", ddlDataTypeUat.SelectedValue);

            command.Parameters.AddWithValue("@idProd", idProd);
            command.Parameters.AddWithValue("@keyValueProd", txtKeyValueProd.Text);
            command.Parameters.AddWithValue("@keyDataTypeProd", ddlDataTypeProd.SelectedValue);

            command.ExecuteNonQuery();

        }
    }

    #endregion

    #region protected methods and events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            defineAction();

            if (name != null & category != null)
            {
                GetConfigurationSettings();
            }
            else
            {
                GetSystemNames();
                ddlSystemDev.Enabled = true;
                ddlSystemUat.Enabled = true;
                ddlSystemProd.Enabled = true;
            }
        }
    }

    /// <summary>
    ///   Checks whether stored procedure exists in selected system database, if so then there are Customer scrutiny rules to be viewed
    /// </summary>
    /// <param name = "storedProcedure"></param>
    /// <returns>bool that states whether stpred procedure exists</returns>
    protected bool storedProcedureExists(string storedProcedure)
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("select * FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='" + storedProcedure + "'", connection);
            SqlDataReader sdrSpExists = command.ExecuteReader();

            if (sdrSpExists.Read())
                storedProcExists = true;
            return storedProcExists;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session["grouped"] = "true";

        Response.Redirect("ConfigurationSettings.aspx?id=" + ddlSystemDev.SelectedValue);
    }

    protected void btnDeleteEntity_Click(object sender, EventArgs e)
    {
        if (storedProcedureExists("usp_DEL_ConfigurationSettings"))
        {
            DeleteEntity();
            Session["grouped"] = "true";

            Response.Redirect("ConfigurationSettings.aspx?id=" + ddlSystemDev.SelectedValue);
        }
        else
        {
            lblMessage.Text = "Cannot Delete Configuration Settings on this system";
            lblMessage.ForeColor = Color.Red;
        }
    }

    protected void btnSaveEntity_Click(object sender, EventArgs e)
    {
        if (storedProcedureExists("usp_SAV_ConfigurationSettingsEnvironment"))
        {
            SaveEntity();

            Session["grouped"] = "true";
            Response.Redirect("ConfigurationSettings.aspx?id=" + ddlSystemDev.SelectedValue);
        }
        else
        {
            lblMessage.Text = "Cannot save Configuration Settings on this system";
            lblMessage.ForeColor = Color.Red;
        }
    }

    //sets systems for all environments to be the same on adding
    protected void ddlSystemDev_Changed(object sender, EventArgs e)
    {
        string selectedValue = ddlSystemDev.SelectedValue;
        ddlSystemUat.SelectedValue = selectedValue;
        ddlSystemProd.SelectedValue = selectedValue;
    }

    protected void ddlSystemUat_Changed(object sender, EventArgs e)
    {
        string selectedValue = ddlSystemUat.SelectedValue;
        ddlSystemDev.SelectedValue = selectedValue;
        ddlSystemProd.SelectedValue = selectedValue;
    }

    protected void ddlSystemProd_Changed(object sender, EventArgs e)
    {
        string selectedValue = ddlSystemProd.SelectedValue;
        ddlSystemDev.SelectedValue = selectedValue;
        ddlSystemUat.SelectedValue = selectedValue;
    }

    //sets the category and names for all entries
    protected void txtCatDev_TextChanged(object sender, EventArgs e)
    {
        string textCatValue = txtKeyCategoryDev.Text;

        txtKeyCategoryUat.Text = textCatValue;
        txtKeyCategoryProd.Text = textCatValue;
    }

    protected void txtCatUat_TextChanged(object sender, EventArgs e)
    {
        string textCatValue = txtKeyCategoryUat.Text;

        txtKeyCategoryDev.Text = textCatValue;
        txtKeyCategoryProd.Text = textCatValue;
    }

    protected void txtCatProd_TextChanged(object sender, EventArgs e)
    {
        string textCatValue = txtKeyCategoryProd.Text;

        txtKeyCategoryUat.Text = textCatValue;
        txtKeyCategoryDev.Text = textCatValue;
    }

    //sets the category and names for all entries
    protected void txtNameDev_TextChanged(object sender, EventArgs e)
    {
        string textNameValue = txtKeyNameDev.Text;

        txtKeyNameUat.Text = textNameValue;
        txtKeyNameProd.Text = textNameValue;
    }

    protected void txtNameUat_TextChanged(object sender, EventArgs e)
    {
        string textNameValue = txtKeyNameUat.Text;

        txtKeyNameDev.Text = textNameValue;
        txtKeyNameProd.Text = textNameValue;
    }

    protected void txtNameProd_TextChanged(object sender, EventArgs e)
    {
        string textNameValue = txtKeyNameProd.Text;

        txtKeyNameUat.Text = textNameValue;
        txtKeyNameDev.Text = textNameValue;
    }

    //sets datatypes to be the same on adding
    protected void ddlDataTypeDev_Changed(object sender, EventArgs e)
    {
        string selectedValue = ddlDataTypeDev.SelectedValue;
        ddlDataTypeUat.SelectedValue = selectedValue;
        ddlDataTypeProd.SelectedValue = selectedValue;
    }

    protected void ddlDataTypeUat_Changed(object sender, EventArgs e)
    {
        string selectedValue = ddlDataTypeUat.SelectedValue;
        ddlDataTypeDev.SelectedValue = selectedValue;
        ddlDataTypeProd.SelectedValue = selectedValue;
    }

    protected void ddlDataTypeProd_Changed(object sender, EventArgs e)
    {
        string selectedValue = ddlDataTypeProd.SelectedValue;
        ddlDataTypeDev.SelectedValue = selectedValue;
        ddlDataTypeUat.SelectedValue = selectedValue;
    }
    #endregion
}