﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Operation
{
    public partial class MasterPages_Default : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        #endregion

        #region Private Methods

        private void LoadGrid(string entryDate)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;

            using (connection = new SqlConnection(connectionString))
            {
                DateTime filterDate;

                connection.Open();
                //Set the SQL command to the stored procedure for Batches
                SqlCommand command = new SqlCommand("usp_SEL_ErrorLogs", connection);
                command.CommandType = CommandType.StoredProcedure;

                if (entryDate == "")
                {
                    entryDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                }
                else
                {
                    if (DateTime.TryParse(entryDate, out filterDate))
                    {
                        entryDate = filterDate.ToString("yyyy-MM-dd hh:mm:ss");
                    }
                    else
                    {
                        entryDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    }
                }

                command.Parameters.AddWithValue("@Date", DateTime.Parse(entryDate));

                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                sda.Fill(ds);

                dgrError.DataSource = ds.Tables[0];
                dgrError.DataBind();
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                LoadGrid(txtDate.Text);
            }
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            LoadGrid(txtDate.Text);
        }


        protected void dgrError_PageIndexChanged(object sender, EventArgs e)
        {
        }

        protected void dgrError_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrError.PageIndex = e.NewPageIndex;
            LoadGrid(txtDate.Text);
        }

        #endregion
    }
}