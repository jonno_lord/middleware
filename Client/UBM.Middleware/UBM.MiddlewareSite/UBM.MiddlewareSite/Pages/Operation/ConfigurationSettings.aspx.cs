﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

public partial class ConfigurationSettings : Page
{
    #region private fields and properties

    private static SqlConnection connection;

    private bool filter;

    private bool storedProcExists;

    private static string systemString
    {
        get
        {
            string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
            return returnString;
        }
    }

    private string connectionString
    {
        get
        {
            string unchangedString = WebConfigurationManager.ConnectionStrings[ddlFilterSystems.SelectedItem.Text + "ConnectionString"].ConnectionString;
            string returnString = RemoveProviders.RemoveProvider(unchangedString);
            return returnString;
        }
    }

    #endregion

    #region private methods

    private void LoadGrid()
    {
        try
        {
            string connectionStringConfiguration = "";


            if (Session["grouped"] != null)
            {
                if (Session["grouped"].ToString() == "true")
                {
                    rdoGrouped.Checked = true;
                }
                else if (Session["grouped"].ToString() == "false")
                {
                    rdoConfiguration.Checked = true;
                }
            }
            else
            {
                rdoConfiguration.Checked = true;
            }

            if (rdoConfiguration.Checked)
            {
                pnlConfiguration.Visible = true;
                pnlConfigurationGrouped.Visible = false;
                connectionStringConfiguration = "usp_SEL_ConfigurationSettings";

                SummaryActions1.AddEntityURL = "ConfigurationSettingsDetails.aspx";

            }
            else if (rdoGrouped.Checked)
            {
                pnlConfigurationGrouped.Visible = true;
                pnlConfiguration.Visible = false;
                connectionStringConfiguration = "usp_SEL_ConfigurationSettingsGrouped";

                SummaryActions1.AddEntityURL = "ConfigurationSettingsDetailsGrouped.aspx";
            }

            if (storedProcedureExists(connectionStringConfiguration))
            {
                lblMessage.Text = "";

                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(connectionStringConfiguration, connection);
                    command.CommandType = CommandType.StoredProcedure;

                    if (rdoConfiguration.Checked)
                    {
                        rptGrid.DataSource = command.ExecuteReader();
                        rptGrid.DataBind();
                    }
                    else if (rdoGrouped.Checked)
                    {
                        rptGridGrouped.DataSource = command.ExecuteReader();
                        rptGridGrouped.DataBind();
                    }
                }
            }
            else
            {
                lblMessage.Text = "No Configuration Settings data for this system";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch(Exception ex)
        {
            lblMessage.Text = "There is no valid connection string available for the chosen system (" + ex.Message.ToString() +")";
            lblMessage.ForeColor = Color.Red;
        }
    }

    private void GetSystemNames()
    {

        using (connection = new SqlConnection(systemString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlDataReader sdrSystems = command.ExecuteReader();

            while (sdrSystems.Read())
            {
                ListItem itmSystem = new ListItem();
                itmSystem.Value = sdrSystems["id"].ToString();
                itmSystem.Text = sdrSystems["ShortName"].ToString();

                if (Request.QueryString["id"] != null)
                {
                    if (itmSystem.Value == Request.QueryString["id"])
                    {
                        itmSystem.Selected = true;
                    }
                }

                ddlFilterSystems.Items.Add(itmSystem);
            }
        }
    }

    // This function prevent the page being retrieved from browser cache
    private void ExpirePageCache()
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now - new TimeSpan(1, 0, 0));
        Response.Cache.SetLastModified(DateTime.Now);
        Response.Cache.SetAllowResponseInBrowserHistory(false);
    }

    //checks if page is filtered by dropdowns
    private bool isFiltered()
    {
        filter = ddlFilterSystems.SelectedIndex != 0;

        return filter;
    }

    #endregion

    #region protected methods and events

    protected void Page_Init(object sender, EventArgs e)
    {
        AsyncPostBackTrigger statusTrigger = new AsyncPostBackTrigger();

        statusTrigger.ControlID = ddlFilterSystems.UniqueID;

        statusTrigger.EventName = "SelectedIndexChanged";

        updConfiguration.Triggers.Add(statusTrigger);

        AsyncPostBackTrigger statusGroupedTrigger = new AsyncPostBackTrigger();

        statusGroupedTrigger.ControlID = ddlFilterSystems.UniqueID;

        statusGroupedTrigger.EventName = "SelectedIndexChanged";

        updConfigurationGrouped.Triggers.Add(statusGroupedTrigger);

        AsyncPostBackTrigger labelTrigger = new AsyncPostBackTrigger();

        labelTrigger.ControlID = ddlFilterSystems.UniqueID;

        labelTrigger.EventName = "SelectedIndexChanged";

        updLabel.Triggers.Add(labelTrigger);


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ExpirePageCache();

        //sets link to add page based on view type selected
        if (rdoConfiguration.Checked)
        {
            SummaryActions1.AddEntityURL = "ConfigurationSettingsDetails.aspx";

        }
        else if (rdoGrouped.Checked)
        {
            SummaryActions1.AddEntityURL = "ConfigurationSettingsDetailsGrouped.aspx";
        }

        if (!IsPostBack)
        {
            GetSystemNames();

            pnlConfigurationGrouped.Visible = false;

            LoadGrid();
        }

        //if page is filtered keep summary on
        if (isFiltered())
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: summaryDisplayNoAnimation(); ", true);
        }
    }

    /// <summary>
    ///   Checks whether stored procedure exists in selected system database, if so then there are Customer scrutiny rules to be viewed
    /// </summary>
    /// <param name = "storedProcedure"></param>
    /// <returns>bool that states whether stpred procedure exists</returns>
    protected bool storedProcedureExists(string storedProcedure)
    {
        using (connection = new SqlConnection(connectionString))
        {
            connection.Open();
            SqlCommand command = new SqlCommand("select * FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='" + storedProcedure + "'", connection);
            SqlDataReader sdrSpExists = command.ExecuteReader();

            if (sdrSpExists.Read())

                storedProcExists = true;

            return storedProcExists;
        }
    }

    protected void ddlFilterSystemSelect(object sender, EventArgs e)
    {
        //clears out grids of data on postback
        rptGrid.DataSource = null;
        rptGrid.DataBind();

        Session["SettingSystem"] = ddlFilterSystems.SelectedValue;

        LoadGrid();

    }

    //links to details page with id of setting
    protected void rptConfiguration_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "btnedit":
                //links to details page as an edit, including the record's id in the url
                Response.Redirect("ConfigurationSettingsDetails.aspx?id=" + e.CommandArgument + "&systemid=" + ddlFilterSystems.SelectedValue);

                break;
        }
    }

    //links to a grouped layout details page with id of setting
    protected void rptConfigurationGrouped_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "btnedit":

                //splits to obtain multiple command arguments
                string[] commandArgs = e.CommandArgument.ToString().Split(new[] { ',' });
                string category = commandArgs[0]; //gives keyCategory              
                string name = commandArgs[1]; //gives keyName

                //links to grouped layout details page as an edit, 
                Response.Redirect("ConfigurationSettingsDetailsGrouped.aspx?cat=" + category + "&name=" + name + "&systemid=" + ddlFilterSystems.SelectedValue);

                break;
        }
    }

    protected void rdoConfiguration_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoConfiguration.Checked)
        {
            Session["grouped"] = "false";
        }
        else
        {
            Session["grouped"] = "true";
        }

        LoadGrid();
    }

    #endregion
}