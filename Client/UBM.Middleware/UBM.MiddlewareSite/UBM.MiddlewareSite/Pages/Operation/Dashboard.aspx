﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Operation.Dashboard" Codebehind="Dashboard.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/DashboardFunctions.js") %> "></script>    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Dashboard</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
    <div id="accordion">
    <h3><a href="#">Summary</a></h3>
    <div>
        <div class="break MiddlewareDashboardStatus"></div> <div class="formRowNoClear MiddlewareStatus">Middleware status is</div> <div class="MiddlewareStatusText MiddlewareStatus"></div>
        <div class="formRowNoClear DashboardStatusMargin MiddlewareStatus">
             Last Execution Time was <asp:Label ID="lblExecutionTime" runat="server" />
        </div>
        <div class="break"></div>

        <div class="formRowNoClear DashboardStatusMargin">
            <asp:label ID="lblJobsRunning" class="JobCount" runat="server"></asp:label> &nbsp;Jobs Currently Running</div>
        <div class="formRowNoClear DashboardStatusMargin">
            <asp:Label ID="lblJobCount" runat="server" /> Jobs Defined
        </div> 
        <div class="formRowNoClear DashboardStatusMargin">
            <asp:Label ID="lblCompleteJobs" runat="server" /> Jobs Completed Today
        </div>
        <div class="formRowNoClear DashboardStatusMargin">
            <asp:Label ID="lblFailedJobs" runat="server" /> Failed Jobs
        </div>

        <div class="break"></div>
        <div class="formRowNoClear DashboardStatusMargin">
            <a href="../../Webservices/JobsService.asmx" title="Webservice Information">Information on Webservices</a>
        </div>

    </div>
        <h3><a href="#">Current Activity</a></h3>
        <div>
        <div class="displayBoxHeader">
            <h1>Running Jobs</h1>
                <div id="jobsRunningOutput" class="displayBox displayRun displayMargin">
                    <ul id="jobRun">
                    </ul>
                </div>
        </div>
        <div class="displayBoxHeader">
            <h1>Failed Jobs</h1>
                <div id="jobsFailedOutput" class="displayBox displayMargin rightFloat">
                    <ul id="jobFail">
                    </ul>
                </div>
        </div>
            <div class="displayBoxHeader">
                <h1>Recent Batches</h1>
                 <div id="jobsLastBatchesOutput" class="break displayBox displayRun displayMargin">
                    <ul id="batchLast10">
                    </ul>
                </div>
        </div>
            <div class="displayBoxHeader">
                <h1>Recently Failed Batches</h1>
                   <div id="batchesLast10Output" class="displayBox displayBoxEnd rightFloat">
                      <ul id="batches">
                      </ul>
                   </div>
            </div>
        </div>
        <h3><a href="#">Graphs</a></h3>
        <div>

            <asp:UpdatePanel ID="udpCpuGraph" UpdateMode="Conditional" runat="server">
                <ContentTemplate> 
                <div class="graphArea">
                    <div class="graphInput">
                        <h1>CPU Utilisation</h1>
                       <asp:textbox ID="txtCPUStartDate" runat="server" ClientIDMode="Static" width="100px" ToolTip="Enter Start Date" class="formRowNoClear dashboardMargin"/>
                        <asp:label ID="lblBetween1" runat="server" Text="Between" class="formRowNoClear dashboardMargin" /><asp:textbox ID="txtCPUEndDate" runat="server" ClientIDMode="Static" width="100px" tooltip="Enter End Date" class="formRowNoClear dashboardMargin"/>
                        <asp:ImageButton ID="btnSearchCpu" runat="server" ImageUrl="../../images/search_icon.png" tooltip="Search Dates" OnClick="btnSearchCpu_Click" autopostback="true" class="formRowNoClear dashboardMargin"/>   
                     </div>
                     <div class="graph break">  
                        <asp:Chart ID="chtCPUUtilisation" runat="server" Width="435" Height="300">
                            <Series>
                                <asp:Series ChartType="Line" Name="chtCPUSeries" ChartArea="chtArea">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="chtArea" BorderColor="DarkGray">
                                    <AxisY LineColor="DarkGray">
                                    </AxisY>
                                    <AxisX LineColor="DarkGray">
                                    </AxisX>
                                </asp:ChartArea>
                            </ChartAreas>
                             <Titles>
                                <asp:Title Docking="left" Name="chtCPUValue" Text="Utilisation in %" 
                                     Alignment="MiddleCenter" ForeColor="DarkGray">
                                </asp:Title>
                            </Titles>
                          </asp:Chart>
                    </div>
                </div>
                <div class="graphArea">
                    <div class="graphInput">
                        <h1>Drive Space on Server</h1>
                        <asp:textbox ID="txtDriveStartDate" runat="server" ClientIDMode="Static" width="100px" ToolTip="Enter Start Date" class="formRowNoClear dashboardMargin"/>
                        <asp:label ID="lblBetween2" runat="server" Text="Between" class="formRowNoClear dashboardMargin" /><asp:textbox ID="txtDriveEndDate" runat="server" ClientIDMode="Static" width="100px" tooltip="Enter End Date" class="formRowNoClear dashboardMargin"/>
                        <asp:ImageButton ID="btnSearchDrive" runat="server" class="formRowNoClear dashboardMargin" ImageUrl="../../images/search_icon.png" tooltip="Search Dates" OnClick="btnSearchDrive_Click" autopostback="true"/>   
                     </div>
                     <div class="graph break">  
                                <asp:Chart ID="chtHardDriveSpace" runat="server" Width="435" Height="300" 
                                    onload="chtHardDriveSpace_Load">
                             <Series>
                                 <asp:Series ChartType="Line" Name="chtDriveSeries" ChartArea="chtArea">
                                 </asp:Series>
                             </Series>
                             <ChartAreas>
                                 <asp:ChartArea Name="chtArea" BorderColor="DarkGray">
                                     <AxisY LineColor="DarkGray">
                                     </AxisY>
                                     <AxisX LineColor="DarkGray">
                                     </AxisX>
                                 </asp:ChartArea>
                             </ChartAreas>
                             <Titles>
                                <asp:Title Docking="left" Name="chtDriveValue" Text="Space Free (GB)" ForeColor="DarkGray">
                                </asp:Title>
                            </Titles>
                         </asp:Chart>
                    </div>
                </div>
              </ContentTemplate>
            </asp:UpdatePanel> 
        </div>
    </div>

<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {

            $("#accordion").accordion({ autoHeight: false });
            $("#txtCPUStartDate").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#txtCPUEndDate").datepicker({ dateFormat: 'yy-mm-dd' });

            $("#txtDriveStartDate").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#txtDriveEndDate").datepicker({ dateFormat: 'yy-mm-dd' });
        });

    </script>

</asp:Content>

