﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace Pages.Operation
{
    public partial class RunJob : Page
    {
        #region Private Fields and Methods

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_JobsStatus", connection);
                command.CommandType = CommandType.StoredProcedure;

                //to allow system filter e.g for otis
                if (Request.QueryString["id"] != null)
                {
                    ddlSystemFilter.SelectedValue = Request.QueryString["id"];
                }

                if (ddlSystemFilter.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@systemid", ddlSystemFilter.SelectedValue);
                }


                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                sda.Fill(ds);
                dgrJobs.DataSource = ds.Tables[0];

                dgrJobs.DataBind();
            }

        }


        private void GetSystemNames()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_ActiveSystems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                ListItem itmSystem = new ListItem("ALL");
                ddlSystemFilter.Items.Add(itmSystem);

                while (sdrSystems.Read())
                {
                    itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    ddlSystemFilter.Items.Add(itmSystem);
                }
            }
        }


        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
                if (!IsPostBack)
                {
                    GetSystemNames();
                    LoadGrid();
                }
        }

        protected void dgrJobs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //disables run button while job is running
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Text == "COMPLETE")
                {
                    e.Row.Cells[6].Enabled = true;
                    e.Row.Cells[7].Enabled = false;
                }
                else if (e.Row.Cells[3].Text == "WAITING")
                {
                    e.Row.Cells[6].Enabled = true;
                    e.Row.Cells[7].Enabled = false;
                }
                else if (e.Row.Cells[3].Text == "FAILED")
                {
                    e.Row.Cells[6].Enabled = true;
                    e.Row.Cells[7].Enabled = false;
                }
                else
                {
                    //-- set the run to disabled and the kill to enabled..
                    e.Row.Cells[6].Enabled = false;
                    e.Row.Cells[7].Enabled = true;
                }

                // apply Javascript function to link button and pass id of job
                //LinkButton button = (LinkButton)e.Row.FindControl("btnRunJob");
                //string id = DataBinder.Eval(e.Row.DataItem, "id").ToString();
                //button.Attributes.Add("onclick", string.Format("RunJob('" + id + "')"));


                // apply Javascript function to link button and pass id of job
                LinkButton killButton = (LinkButton)e.Row.FindControl("btnKillJob");
                string idKill = DataBinder.Eval(e.Row.DataItem, "id").ToString();
                killButton.Attributes.Add("onclick", string.Format("KillJob('" + idKill + "')"));

                //checks whether Job is disabled and sets row to disabled
                Button disabledButton = (Button)e.Row.FindControl("btnDisabled");

                disabledButton.Attributes.Add("id", DataBinder.Eval(e.Row.DataItem, "id").ToString());
                DataBinder.Eval(e.Row.DataItem, "Disabled").ToString();
                disabledButton.Text = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "Disabled")) ? "Off" : "On";
                disabledButton.CssClass = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "Disabled")) ? "disableButton" : "enableButton";
                disabledButton.ToolTip = disabledButton.Text == "On" ? "Disable Job" : "";

                //if (disabledButton.Text == "Off")
                //{
                //    e.Row.Cells[0].Enabled = false;
                //    e.Row.Cells[1].Enabled = false;
                //    e.Row.Cells[2].Enabled = false;
                //    e.Row.Cells[3].Enabled = false;
                //    e.Row.Cells[4].Enabled = false;
                //    e.Row.Cells[6].Enabled = false;
                //    e.Row.Cells[7].Enabled = false;
                //    e.Row.Cells[8].Enabled = false;
                //    disabledButton.Text = "Off";
                //    disabledButton.CssClass = "disableButton";
                //    disabledButton.ToolTip = "";
                //}
                //else 
                if (e.Row.Cells[3].Text == "DISABLED")
                {
                    e.Row.Cells[0].Enabled = false;
                    e.Row.Cells[1].Enabled = false;
                    e.Row.Cells[2].Enabled = false;
                    e.Row.Cells[3].Enabled = false;
                    e.Row.Cells[4].Enabled = false;
                    e.Row.Cells[6].Enabled = false;
                    e.Row.Cells[7].Enabled = false;
                    e.Row.Cells[8].Enabled = false;
                    e.Row.Cells[0].CssClass = "disableText";
                    e.Row.Cells[1].CssClass = "disableText";
                    e.Row.Cells[2].CssClass = "disableText";
                    disabledButton.Text = "Off";
                    disabledButton.CssClass = "disableButton";
                    disabledButton.ToolTip = "";
                }
                else
                {
                    e.Row.Cells[0].Enabled = true;
                    e.Row.Cells[1].Enabled = true;
                    e.Row.Cells[2].Enabled = true;
                    e.Row.Cells[3].Enabled = true;
                    e.Row.Cells[4].Enabled = true;
                    e.Row.Cells[6].Enabled = true;
                    e.Row.Cells[7].Enabled = true;
                    e.Row.Cells[8].Enabled = true;
                }
            }
        }

  
        //on click of row, get id and supply to stored procedure to find most recent batch of job and
        //carry out redirect to batchHistoryDetails.aspx using batch number as parameter
        protected void dgrJobs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnstatus":

                     using (connection = new SqlConnection(connectionString))
                     {
                         connection.Open();
                         SqlCommand command = new SqlCommand("usp_GET_BatchNumber", connection);
                         command.CommandType = CommandType.StoredProcedure;

                         command.Parameters.AddWithValue("@jobid", e.CommandArgument);

                         string batchNo = command.ExecuteScalar().ToString();
                         Response.Redirect("BatchDetails.aspx?id=" + batchNo);
                        
                     }

                    break;

                case "btndisable":

                    using (connection = new SqlConnection(connectionString))
                    {
                        GridViewRow row = (GridViewRow)((Button)e.CommandSource).Parent.Parent;

                        //changes text to bool value
                        Button button = row.FindControl("btnDisabled") as Button;

                        if (button != null)
                        {
                            bool btnDisabled;

                            if (button.Text == "On")
                            {
                                button.Text = "Off";
                                btnDisabled = true;
                            }
                            else
                            {
                                button.Text = "On";
                                btnDisabled = false;
                            }

                            connection.Open();

                            SqlCommand command = new SqlCommand("usp_UPD_Job", connection);
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@jobid", e.CommandArgument);
                            command.Parameters.AddWithValue("@jobDisabled", btnDisabled);

                            command.ExecuteReader();

                            button.CssClass = button.Text == "Off" ? "disableButton" : "enableButton";
                            button.ToolTip = button.Text == "On" ? "Disable Job" : "";

                        }
                    }

                    break;
            }

        }

        protected void tmrRunJob_Tick(object sender, EventArgs e)
        {
            //LoadGrid();

        }

        protected void ddlFilterSystemSelect(object sender, EventArgs e)
        {
            LoadGrid();
        }

        protected void dgrRun_PageIndexChanged(object sender, EventArgs e)
        {

        }
        protected void dgrRun_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrJobs.PageIndex = e.NewPageIndex;
            LoadGrid();

        }

        #endregion
}
}