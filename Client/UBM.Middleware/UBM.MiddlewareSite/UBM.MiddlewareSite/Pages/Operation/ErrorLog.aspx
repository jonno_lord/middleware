﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Operation.MasterPages_Default" Codebehind="ErrorLog.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Error Log</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div class="innerFilter">
    <asp:label runat="server" Text="Date:" Width="50" class="innerFilter"/> <asp:TextBox id="txtDate" runat="server" ClientIDMode="Static" OnTextChanged="txtDate_TextChanged" AutoPostBack="true" CssClass="innerFilter"/>
</div>
<asp:GridView ID="dgrError" runat="server" AutoGenerateColumns="false"  AllowPaging="True" onpageindexchanged="dgrError_PageIndexChanged" onpageindexchanging="dgrError_PageIndexChanging" PageSize="30">
            <emptydatatemplate>
                No Error Logs found for current date
                </emptydatatemplate>
            <EmptyDataRowStyle/>
            <Columns>
                <asp:BoundField DataField="EntryDate" HeaderText="Date" HeaderStyle-Width="50px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre"/>
                <asp:BoundField DataField="Message" HeaderText="Message" HeaderStyle-Width="150px" HeaderStyle-CssClass="GridCentre"/>
                <asp:BoundField DataField="StackTrace" HeaderText="Stack Trace" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre"/>
            </Columns>
            <PagerStyle CssClass="pageIndex" />
</asp:GridView> 
 
<script type="text/javascript">
    $(function () {
        $("#txtDate").datepicker({ dateFormat: 'yy-mm-dd' });

    });
</script>
</asp:Content>

