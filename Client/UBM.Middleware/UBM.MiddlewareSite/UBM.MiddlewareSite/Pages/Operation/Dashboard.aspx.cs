﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Operation
{
    public partial class Dashboard : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadCpuChart()
        {

            DateTime startDate = DateTime.Parse(txtCPUStartDate.Text);
            DateTime endDate = DateTime.Parse(txtCPUEndDate.Text);

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                //if selected dates are between 1 day and a week
                if ((endDate - startDate).Days < 7)
                {

                    SqlCommand command = new SqlCommand("usp_SEL_AverageHourMetrics", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    //get hours for selected date
                    command.Parameters.AddWithValue("@StartDate", txtCPUStartDate.Text);
                    command.Parameters.AddWithValue("@EndDate", txtCPUEndDate.Text);
                    command.Parameters.AddWithValue("@MetricType", 2);
                    SqlDataReader sdr = command.ExecuteReader();
                    chtCPUUtilisation.DataSource = sdr;

                    chtCPUUtilisation.Series["chtCPUSeries"].XValueMember = "StartTime";
                    chtCPUUtilisation.Series["chtCPUSeries"].YValueMembers = "MetricValue";

                    chtCPUUtilisation.DataBind();
                }
                else
                {
                    //if selected dates are over a week
                    SqlCommand command = new SqlCommand("usp_SEL_AverageDayMetrics", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    //get selected dates
                    command.Parameters.AddWithValue("@StartDate", txtCPUStartDate.Text);
                    command.Parameters.AddWithValue("@EndDate", txtCPUEndDate.Text);
                    command.Parameters.AddWithValue("@MetricType", 2);
                    SqlDataReader sdr = command.ExecuteReader();
                    chtCPUUtilisation.DataSource = sdr;

                    chtCPUUtilisation.Series["chtCPUSeries"].XValueMember = "Dated";
                    chtCPUUtilisation.Series["chtCPUSeries"].YValueMembers = "MetricValue";

                    chtCPUUtilisation.DataBind();

                }
            }
        }

        private void LoadDriveChart()
        {

            DateTime startDate = DateTime.Parse(txtDriveStartDate.Text);
            DateTime endDate = DateTime.Parse(txtDriveEndDate.Text);

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                //if selected dates are between 1 day and a week
                if ((endDate - startDate).Days < 7)
                {
                    SqlCommand command = new SqlCommand("usp_SEL_AverageHourMetrics", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    //get hours for selected date
                    command.Parameters.AddWithValue("@StartDate", txtDriveStartDate.Text);
                    command.Parameters.AddWithValue("@EndDate", txtDriveEndDate.Text);
                    command.Parameters.AddWithValue("@MetricType", 1);
                    SqlDataReader sdr = command.ExecuteReader();
                    chtHardDriveSpace.DataSource = sdr;

                    chtHardDriveSpace.Series["chtDriveSeries"].XValueMember = "StartTime";
                    chtHardDriveSpace.Series["chtDriveSeries"].YValueMembers = "MetricValue";

                    chtHardDriveSpace.DataBind();
                }
                else
                {
                    //if selected dates are over a week

                    SqlCommand command = new SqlCommand("usp_SEL_AverageDayMetrics", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    //get selected dates
                    command.Parameters.AddWithValue("@StartDate", txtDriveStartDate.Text);
                    command.Parameters.AddWithValue("@EndDate", txtDriveEndDate.Text);
                    command.Parameters.AddWithValue("@MetricType", 1);
                    SqlDataReader sdr = command.ExecuteReader();
                    chtHardDriveSpace.DataSource = sdr;

                    chtHardDriveSpace.Series["chtDriveSeries"].XValueMember = "Dated";
                    chtHardDriveSpace.Series["chtDriveSeries"].YValueMembers = "MetricValue";

                    chtHardDriveSpace.DataBind();

                }
            }
        }

        private void getStatus()
        {
            GetMiddlewareDetails getMiddlewareDetails = new GetMiddlewareDetails();

            lblJobCount.Text = getMiddlewareDetails.GetJobCount().ToString();
            lblFailedJobs.Text = getMiddlewareDetails.GetJobsDisabled().ToString();
            lblExecutionTime.Text = getMiddlewareDetails.GetLastExecutionTime().ToString();
            lblCompleteJobs.Text = getMiddlewareDetails.GetCompleteJobs().ToString();

        }


        #endregion

        #region Protected Fields and Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtCPUStartDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                txtCPUEndDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                LoadCpuChart();

                txtDriveStartDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                txtDriveEndDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                LoadDriveChart();

                getStatus();
            }
            else
            {
                LoadCpuChart();
                LoadDriveChart();
            }
        }

        protected void btnSearchCpu_Click(object sender, EventArgs e)
        {
            LoadCpuChart();
        }

        protected void btnSearchDrive_Click(object sender, EventArgs e)
        {
            LoadCpuChart();
        }

        #endregion

        public SqlConnection conn { get; set; }

        protected void chtJobsDuration_Load(object sender, EventArgs e)
        {

        }
        protected void chtHardDriveSpace_Load(object sender, EventArgs e)
        {

        }
    }
}