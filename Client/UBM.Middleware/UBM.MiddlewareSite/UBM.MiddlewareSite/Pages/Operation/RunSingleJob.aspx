﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Operation.RunSingleJob" Codebehind="RunSingleJob.aspx.cs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/JavaScript/RunJobs.js") %> "></script> 
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">

<asp:UpdatePanel ID="updBatchDetails" runat="server" UpdateMode ="Conditional">
        <ContentTemplate>
            <div class="Batchheader">
                 <asp:Timer ID="tmrBatchHeader" runat="server" Interval="800" OnTick="tmrBatchHeader_Tick" />
                    <asp:Image ID="imgStatus" ImageUrl="~/Images/BatchStatus/complete.png" runat="server" class="Batchicon"/>
    
                <div class="BatchSummaryHeader BatchMargin">
                    <div class="BatchDetails"> 

                        <h2><asp:label ID="lblJobTitle" runat="server" Text=""/></h2>
                        <div class="entry">
                            <h1>Batch Number:</h1>
                            <asp:label ID="lblBatchNumber" runat="server" Text=""/>
                        </div> 

                        <div class="entry"> 
                            <h1>Start Date:</h1>
                            <asp:label ID="lblStartDate" runat="server" Text=""/>
                        </div>

                        <div class="entry">
                            <h1>Start Time:</h1>
                            <asp:label ID="lblStartTime" runat="server" Text=""/>
                        </div>

                        <div class="entry">
                            <h1>End Time:</h1>
                            <asp:label ID="lblEndTime" runat="server" Text=""/>
                        </div>

                        <div class="entry">
                            <h1>Duration:</h1>
                            <asp:label ID="lblDuration" runat="server" Text=""/>
                        </div>
                    </div>
                </div>
            </div>
      </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRunJob" EventName="click" />
        </Triggers>
</asp:UpdatePanel>

    <div id="runJobFilters" class="break">

        <div class="formRowNoClear">
               <asp:Label ID="lblSystem" runat="server" Text="System:" CssClass="formMargin labelInput" Width="50px"/>
               <asp:DropDownList ID="ddlSystemFilter" runat="server" CssClass="formMargin" ToolTip="System of Job" Width="250" AutoPostBack="True" onselectedindexchanged="ddlSystemFilter_SelectedIndexChanged"/>
        </div>

        <div class="formRowNoClear">
               <asp:Label ID="lblJob" runat="server" Text="Job:" Visible="False" CssClass="formMargin labelInput" Width="50px"/>
               <asp:DropDownList ID="ddlJobsFilter" runat="server" Visible="False" CssClass="formMargin" ToolTip="Job to Run" Width="250" onselectedindexchanged="ddlJobFilter_SelectedIndexChanged" AutoPostBack="true"/>
        </div>

        <div class="formRowNoClear">
                <asp:LinkButton ID="btnRunJob" runat="server" 
                 CssClass ="glassbutton"  onclick="btnRunJob_Click" AlternateText="Run Job" Visible="False"><asp:Image ID="Image1" ImageUrl="~/images/Editicon.png" runat="server" AlternateText="Cancel"/><p>Run Job</p></asp:LinkButton>
        </div>

    </div>
 

    <asp:UpdatePanel ID="updBatches" runat="server" UpdateMode ="Conditional">
    <ContentTemplate>
        <asp:Timer ID="tmrGrid" runat="server" Interval="1000" OnTick="tmrGrid_Tick" />
        <asp:GridView ID="dgrBatchDetails" CssClass= "dgrSinglJobBatchDetails break" runat="server" AllowSorting="True" AutoGenerateColumns="false" EnableSortingAndPagingCallbacks="True"  AllowPaging="True" PageSize="30">
        <emptydatatemplate>
        <p>No Batch data found</p>
        </emptydatatemplate>
        <Columns>
           <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" ItemStyle-CssClass="GridCentre"/>
           <asp:BoundField DataField="LogLevel" HeaderText="Class" ItemStyle-CssClass="GridCentre"/>
           <asp:BoundField DataField="ClassName" HeaderText="Level" ItemStyle-CssClass="GridCentre"/>
           <asp:BoundField DataField="Description" HeaderText="Description"/>
        </Columns>
      <PagerStyle CssClass="pageIndex" />
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnRunJob" EventName="click" />
    </Triggers>

</asp:UpdatePanel>

</asp:Content>
