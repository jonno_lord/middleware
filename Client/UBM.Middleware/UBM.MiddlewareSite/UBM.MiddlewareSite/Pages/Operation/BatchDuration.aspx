﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="BatchDuration" Codebehind="BatchDuration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
      <link href="/Styles/BatchDuration.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Batch Duration Chart</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
    <div id="Filters" class="FloatLeft">
        <asp:updatepanel id="updJobFilter" runat="server">
            <ContentTemplate>
               <div class="FloatLeft formMargin">Date:</div> <asp:TextBox id="txtBatchDate" runat="server" ClientIDMode="Static" OnTextChanged="txtBatchDate_TextChanged" AutoPostBack="true" class="formMargin formRowNoClear"/>
               <div class="FloatLeft  formMargin">Filter by Failed Batches: </div>
               <asp:CheckBox ID="cbxFailedFilter" CssClass="CheckboxMargin FloatLeft" runat="server" OnCheckedChanged="cbxFailedFilter_Checked" AutoPostBack="true"/>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="txtBatchDate" EventName="textChanged" />
                <asp:AsyncPostBackTrigger ControlID="cbxFailedFilter" EventName="checkedChanged" />
            </Triggers>
       </asp:updatepanel>
   </div>


<asp:UpdatePanel ID="updJobs" runat="server">
    <ContentTemplate>
        <asp:UpdateProgress ID="uppJobs" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updJobs">
            <ProgressTemplate>
                    <div class="progressIndicator"> Loading...<asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" /></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div id="BatchDurationChart">
            <div class="break JobName FloatLeft">
                <div class="JobName FloatLeft break">&nbsp;</div>
                <asp:Repeater ID="rptNames" runat="server">
                            <ItemTemplate>
                            <div class="break JobName FloatLeft JobMargin"><%# Eval("ShortName") %></div>
                            </ItemTemplate>
                    </asp:Repeater>
                 </div>
         
                <div id="Chart" class="FloatLeft">
                    <div id="ChartArea">
                         <asp:Repeater ID="rptTime" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lblTime" runat="server" style="float:left; Width:60px; font-weight: bold; color:#7e7e7e;" Text='<%# Eval("HeaderTime") %>'/>
                            </ItemTemplate>
                         </asp:Repeater>
                         <asp:Repeater ID="rptJobs" runat="server" onItemDataBound="rptJob_ItemDataBound">
                             <ItemTemplate>
                                <div id="JobDurationArea" class="FloatLeft">
                                        <asp:Repeater ID="rptJobDuration" runat="server" onItemCommand="rptJobDuration_ItemCommand">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="hypBatch" runat="server" CommandName='<%# Eval("Status") %>' CommandArgument='<%# Eval("Batchid") %>' ><div class="<%# Eval("Status") %> MainDuration FloatLeft" title="<%# Eval("JobName") %>" style="width:<%# Int32.Parse(Eval("Duration").ToString()) %>px;">&nbsp;</div></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>

<script type="text/javascript">
        $(function () {
            $("#txtBatchDate").datepicker({ dateFormat: 'yy-mm-dd' });
        });
</script>

</asp:Content>

