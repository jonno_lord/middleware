﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Operation
{
    public partial class BatchDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int batchid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        private enum LogLevel
        {
            LowPriority = 300
        }

        #endregion

        #region Private Methods

        private void LoadSummary()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Batch", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@batchid", batchid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    lblBatchNumber.Text = sdr["id"].ToString();
                    lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", sdr["StartDate"]);
                    lblStartTime.Text = String.Format("{0:HH:mm:ss}", sdr["StartDate"]);
                    lblEndTime.Text = String.Format("{0:HH:mm:ss}", sdr["EndDate"]);
                    lblJobTitle.Text = sdr["JobName"].ToString();
                    lblDuration.Text = sdr["DurationSeconds"].ToString();
                    imgStatus.ImageUrl = "~/images/BatchStatus/" + sdr["status"] + ".png";

                }
            }
        }

        //load grid if there is a batch number supplied in query string
        private void LoadGrid()
        {

            if (batchid != 0)
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("usp_SEL_BatchLogs", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@batchid", batchid);

                    if (cbxLogLevelFilter.Checked)
                    {
                        command.Parameters.AddWithValue("@batchLogLevel", LogLevel.LowPriority);
                    }

                    SqlDataAdapter sda = new SqlDataAdapter(command);

                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    dgrBatchDetails.DataSource = ds.Tables[0];

                    dgrBatchDetails.DataBind();

                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSummary();
                LoadGrid();
            }

            //hides filter if there is no data
        }

        protected void cbxLogFilter(object sender, EventArgs e)
        {
            LoadGrid();
        }


        protected void dgrBatchDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrBatchDetails.PageIndex = e.NewPageIndex;
            LoadGrid();

        }
        #endregion
    }
}