﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireUp="true" Inherits="Pages.Configuration.Jobs.Jobs" Codebehind="Jobs.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="SummaryActions" ContentPlaceHolderID="SummaryActions" runat="server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Job"  Title="MW System" AddEntityURL="JobDetails.aspx" QueryStringFilter="id"/>
</asp:Content>

<asp:Content id="content3" ContentPlaceHolderID="filters" runat="server">
 <div class="filter">
        <div class="rowA">
            <p>System:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList id="ddlFilterSystems" runat="server" tooltip="Systems Job belongs to" onselectedindexchanged="ddlFilterSystemSelect" AutoPostBack="True"/>
        </div>
    </div>
    <div class="filter">
        <div class="rowA">
            <p>Status:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList ID="ddlFilterStatus" runat="server" tooltip="Status of the Job" OnSelectedIndexChanged="ddlFilterStatusSelect" AutoPostBack="True">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>COMPLETE</asp:ListItem>
                <asp:ListItem>CREATING</asp:ListItem>
                <asp:ListItem>DISABLED</asp:ListItem>
                <asp:ListItem>FAILED</asp:ListItem>
                <asp:ListItem>PROCESSING</asp:ListItem>
                <asp:ListItem>WAITING</asp:ListItem>
            </asp:DropDownList>
        </div>
 </div>
</asp:Content>

<asp:Content ID="PageActions" ContentPlaceHolderID="PageActions" runat="server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Jobs" TableName="Jobs" EntityURL="Jobs.aspx" ConnectionString="MiddlewareSystem" DisableEnable="true"/>
</asp:Content>

<asp:Content ID="Title" ContentPlaceHolderID="PageTitle" runat="server">
<h1>Middleware Jobs</h1>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="dataTable" Runat="Server">
    <asp:UpdatePanel ID="updJobs" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="uppJobs" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updJobs">
                <ProgressTemplate>
                     <div class="progressIndicator"> Loading...<asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptJobs_ItemCommand" onItemDataBound="rptJobs_ItemDataBound">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>System</th>
                        <th>Job</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Last Executed</th>
                        <th>Turn Off</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Job " +Eval("ShortName")%>'/></td>
                      <td><%# Eval("Systemname") %></td>
                      <td><asp:hyperlink id="hypJob" runat="server" Text='<%# Eval("ShortName") %>' ToolTip='<%# Eval("ShortName") + " Processes" %>' NavigateUrl='<%# "~/Pages/Configuration/Processes/processes.aspx?id=" +Eval("id") %>'/></td>
                      <td><%# Eval("Description") %></td>
                      <td class="GridCentre"> <img id="imgStatus" class="imgStatus" src="../../../images/JobStatus/<%# Eval("Status")%>.png" alt="Job Status is <%# Eval("Status")%>"  title="Job Status is <%# Eval("Status")%>"  /></td>
                      <td class="GridCentre"><%# Eval("LastExecuted") %></td>
                      <td class="GridCentre" style="width:20px;">           
                            <asp:Button id="btnDisabled" runat="server" CommandName="btnDisable" CommandArgument='<%# Eval("id") %>' Width="55"/>
                      </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>   
    </asp:UpdatePanel>

<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>
</asp:Content>

