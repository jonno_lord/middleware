﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Configuration.Jobs.JobDetails" Codebehind="JobDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Middleware Jobs</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsJobs" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblSystemidEnter" runat="server" cssClass="formMargin labelInput" Text="System:" Width="100px"/>
           <asp:DropDownList ID="ddlSystemid" runat="server" cssClass="formMargin" DataTextField="ShortName" DataValueField="id" ToolTip="System Job applies to" Width="250">
           </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvSystemid" runat="server"  ControlToValidate="ddlSystemid" ErrorMessage="System id required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
           <asp:Label ID="lblShortNameEnter" runat="server" cssClass="formMargin labelInput" Text="Name:" Width="100px"/>
           <asp:TextBox ID="txtShortName" runat="server" cssClass="formMargin" ToolTip="Name of the Job" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvShortName" runat="server" ControlToValidate="txtShortName" ErrorMessage="Please Enter a Name for the Job" >
        *
        </asp:RequiredFieldValidator>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblDescriptionEnter" runat="server" cssClass="formMargin labelInput" Text="Description:" Width="100px"/>
           <asp:TextBox ID="txtDescription" runat="server" cssClass="formMargin" ToolTip="Detail on what the job does" TextMode="MultiLine" Columns="39" Rows="2"/>
        <asp:RegularExpressionValidator ID="txtLengthValidator" ControlToValidate="txtDescription" ErrorMessage="Description is exceeding 1024 characters" ValidationExpression="^[\s\S]{0,1024}$" runat="server" >
        *
        </asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" ErrorMessage="Please Enter a Description the Job" >
        *
        </asp:RequiredFieldValidator>
     </div>
    
    <div class="formRow"> 
           <asp:Label ID="lblDisabledEnter" runat="server" cssClass="formMargin labelInput" Text="Disabled:" Width="100px"/>
           <asp:CheckBox ID="cbxDisable" runat="server" cssClass="formMargin checkboxLayout" ToolTip="Disable Job" Checked="false" Height="12px"/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

      <div class="formRow">
            <asp:Label ID="lblSequenceEnter" runat="server" cssClass="formMargin labelInput" Text="Sequence:" Width="100px"/>
            <asp:TextBox ID="txtSequence" runat="server" cssClass="formMargin" ToolTip="Sequence Number" MaxLength="6" Width="250"/>      
        <asp:CompareValidator ID="cvrSequence" runat="server" ControlToValidate="txtSequence" Display="Dynamic" ErrorMessage="Numerical value required for Sequence number" 
        Operator="DataTypeCheck" SetFocusOnError="True" Type="Integer">
        *
        </asp:CompareValidator>
    </div>

     <div class="formRow">
           <asp:Label ID="lblStatusEnter" runat="server" cssClass="formMargin labelInput" Text="Status:" Width="100px"/>
           <asp:DropDownList ID="ddlStatus" runat="server" cssClass="formMargin" ToolTip="Status of the Job (Note: Middleware will update this)" Width="250">
                <asp:ListItem>COMPLETE</asp:ListItem>
                <asp:ListItem>CREATING</asp:ListItem>
                <asp:ListItem>DISABLED</asp:ListItem>
                <asp:ListItem>FAILED</asp:ListItem>
                <asp:ListItem>PROCESSING</asp:ListItem>
                <asp:ListItem>WAITING</asp:ListItem>
           </asp:DropDownList>
      </div>
    
    <div class="formRow">
            <asp:Label ID="lblLastExecutedEnter" runat="server" cssClass="formMargin labelInput" Text="Last Executed:" Width="100px"/>
            <asp:TextBox ID="txtLastExecuted" runat="server" cssClass="formMargin" ToolTip="Date of last execution of the Job" MaxLength="20" Width="250" />
    </div>
</div>   
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Job" EntityURL="Jobs.aspx" ConnectionString="MiddlewareSystem" deleteEnabled="true" deleteSPName="Job" deleteParameter="Job"/>
</asp:Content>

