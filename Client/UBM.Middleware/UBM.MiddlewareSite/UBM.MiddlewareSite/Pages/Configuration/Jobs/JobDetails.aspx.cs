﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Jobs
{
    public partial class JobDetails : Page
    {
        #region Private fields and properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int jobid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?addid=1212)
        /// </summary>
        private int addid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["addid"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with job data
        private void GetJob()
        {
            GetSystemNames();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Job", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@jobid", jobid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlSystemid.SelectedValue = sdr["Systemid"].ToString();
                    txtSequence.Text = sdr["Sequence"].ToString();
                    txtShortName.Text = sdr["ShortName"].ToString();
                    txtDescription.Text = sdr["Description"].ToString();
                    ddlStatus.SelectedValue = sdr["Status"].ToString();
                    txtLastExecuted.Text = String.Format("{0:yyyy-MM-dd HH:MM:ss}", sdr["LastExecuted"]);
                    cbxDisable.Checked = (bool)sdr["Disabled"];

                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                selectedValue = Convert.ToInt32(ddlSystemid.SelectedValue);
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        //populates systems filter
        private void GetSystemNames()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader sdrSystems = command.ExecuteReader();

                while (sdrSystems.Read())
                {
                    ListItem itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    if (addid > 0)
                    {
                        if (itmSystem.Value == addid.ToString())
                        {
                            itmSystem.Selected = true;
                        }
                    }

                    ddlSystemid.Items.Add(itmSystem);
                }

            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (jobid > 0)
                {
                    GetJob();
                    txtLastExecuted.Enabled = false;
                }
                else
                {
                    GetSystemNames();
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("jobs.aspx?id=" + ddlSystemid.SelectedValue);
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves jobs to the database either as an insert or update and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_Job", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@jobid", jobid);
                command.Parameters.AddWithValue("@jobSystemid", ddlSystemid.SelectedValue);
                command.Parameters.AddWithValue("@jobSequence", txtSequence.Text);
                command.Parameters.AddWithValue("@jobShortName", txtShortName.Text);
                command.Parameters.AddWithValue("@jobDescription", txtDescription.Text);
                command.Parameters.AddWithValue("@jobStatus", ddlStatus.SelectedValue);
                command.Parameters.AddWithValue("@jobLastExecuted", txtLastExecuted.Text);
                command.Parameters.AddWithValue("@jobDisabled", cbxDisable.Checked);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}