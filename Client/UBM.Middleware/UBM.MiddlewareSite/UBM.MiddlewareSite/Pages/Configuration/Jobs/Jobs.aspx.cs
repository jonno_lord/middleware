﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Jobs
{
    public partial class Jobs : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private bool filter;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //checks if page is filtered by dropdowns
        private bool IsFiltered()
        {
            if (ddlFilterSystems.SelectedIndex == 0 && ddlFilterStatus.SelectedIndex == 0)
            {
                filter = false;
            }
            else
            {
                filter = true;
            }

            return filter;
        }

        //loads grid data from database
        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                RetrieveFilterValues();

                SqlCommand command = new SqlCommand("usp_SEL_Jobs", connection);
                command.CommandType = CommandType.StoredProcedure;

                //-- if the user selected the first item, then it represents no filtering

                if (ddlFilterStatus.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@status", ddlFilterStatus.SelectedValue);
                }

                if (Request.QueryString["id"] != null)
                {
                    command.Parameters.AddWithValue("@systemid", Request.QueryString["id"]);
                }
                else if (ddlFilterSystems.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@systemid", ddlFilterSystems.SelectedValue);
                }

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();           

            }
        }

        //populates system dropdown filter
        private void GetSystemNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                ListItem itmSystem = new ListItem("ALL");
                ddlFilterSystems.Items.Add(itmSystem);

                while (sdrSystems.Read())
                {
                    itmSystem = new ListItem
                                    {
                                        Value = sdrSystems["id"].ToString(),
                                        Text = sdrSystems["ShortName"].ToString()
                                    };

                    if (Request.QueryString["id"] != null)
                    {
                        if (itmSystem.Value == Request.QueryString["id"])
                        {
                            itmSystem.Selected = true;
                        }
                    }

                    ddlFilterSystems.Items.Add(itmSystem);
                }
            }
        }

        //retrieves status filter values from session
        private void RetrieveFilterValues()
        {
            //maintains status filter if system filter is used and causes a redirect
            if (Session["statusValue"] != null & Session["statusIndex"] != null)
            {
                ddlFilterStatus.SelectedValue = Session["statusValue"].ToString();

                ddlFilterStatus.SelectedIndex = int.Parse(Session["statusIndex"].ToString());
            }
        }

        // This function prevent the page being retrieved from broswer cache
        private void ExpirePageCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now - new TimeSpan(1, 0, 0));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger statusTrigger = new AsyncPostBackTrigger();

            statusTrigger.ControlID = ddlFilterStatus.UniqueID;

            statusTrigger.EventName = "SelectedIndexChanged";

            updJobs.Triggers.Add(statusTrigger);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ExpirePageCache();

            if (!IsPostBack)
            {
                LoadGrid();
                GetSystemNames();

                //if page is filtered keep summary on
                if (IsFiltered())
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: summaryDisplayNoAnimation(); ", true);
                }
            }

        }
        
        //links to details page with id of job
        protected void rptJobs_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("JobDetails.aspx?id=" + e.CommandArgument);

                    break;

                case "btndisable":
                    
                    using (connection = new SqlConnection(connectionString))
                    {
                        //changes text to bool value
                        Button button = e.Item.FindControl("btnDisabled") as Button;

                        if (button != null) 
                        {
                            bool btnDisabled;

                            if (button.Text == "On")
                            {
                                button.Text = "Off";
                                btnDisabled = true;
                            }
                            else
                            {
                                button.Text = "On";
                                btnDisabled = false;
                            }

                            connection.Open();

                            SqlCommand command = new SqlCommand("usp_UPD_Job", connection);
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@jobid", e.CommandArgument);
                            command.Parameters.AddWithValue("@jobDisabled", btnDisabled);

                            command.ExecuteReader();

                            button.ToolTip = button.Text == "On" ? "Disable Job " : "Enable Job";
                            button.CssClass = button.Text == "Off" ? "disableButton" : "enableButton";

                        }
                    }

                    break;
            }
        }

        //applies value of id to each button in the repeater and binds database value to controls
        protected void rptJobs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DbDataRecord row = e.Item.DataItem as DbDataRecord;
                Button button = e.Item.FindControl("btnDisabled") as Button;

                if (button != null)
                {
                    if (row != null) button.Attributes.Add("id", row["id"].ToString());

                    button.Text = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "JobDisabled")) ? "Off" : "On";
                    button.CssClass = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "JobDisabled")) ? "disableButton" : "enableButton";
                    button.ToolTip = button.Text == "On" ? "Disable Job " : "Enable Job";
                }
            }
        } 

        //filters grid contents by status
        protected void ddlFilterStatusSelect(object sender, EventArgs e)
        {
            //stores value to be recalled on response.redirects
            Session["statusValue"] = ddlFilterStatus.SelectedValue;
            Session["statusIndex"] = ddlFilterStatus.SelectedIndex;  

            LoadGrid();

        }

        //filters grid by systems
        protected void ddlFilterSystemSelect(object sender, EventArgs e)
        {

            if (ddlFilterSystems.SelectedIndex == 0)
            {
                Response.Redirect("Jobs.aspx");
            }
            else
            {
                Response.Redirect("Jobs.aspx?id=" + ddlFilterSystems.SelectedValue);
            }
        }

        #endregion
    }
}