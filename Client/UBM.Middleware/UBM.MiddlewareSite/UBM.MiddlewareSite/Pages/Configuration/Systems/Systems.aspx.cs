﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Systems
{
    public partial class Systems : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //binds database dat to grid/repeater
        private void LoadGrid()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();
            }
        }

        // This function prevent the page being retrieved from broswer cache
        private void ExpirePageCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now - new TimeSpan(1, 0, 0));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ExpirePageCache();

            if (!IsPostBack)
            {
                LoadGrid();
            }
        }

        //links to details page with id of system
        protected void rptSystems_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("SystemDetails.aspx?id=" + e.CommandArgument);

                    break;

                case "btndisable":

                    using (connection = new SqlConnection(connectionString))
                    {
                        //changes text to bool value
                        Button button = e.Item.FindControl("btnDisabled") as Button;

                        if (button != null)
                        {
                            bool btnDisabled;

                            if (button.Text == "On")
                            {
                                button.Text = "Off";
                                btnDisabled = true;
                            }
                            else
                            {
                                button.Text = "On";
                                btnDisabled = false;
                            }

                            connection.Open();

                            SqlCommand command = new SqlCommand("usp_UPD_Systems", connection);
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@systemid", e.CommandArgument);
                            command.Parameters.AddWithValue("@systemDisabled", btnDisabled);

                            command.ExecuteReader();

                            button.CssClass = button.Text == "Off" ? "disableButton" : "enableButton";
                            button.ToolTip = button.Text == "On" ? "Disable System " : "Enable System";

                        }
                    }

                    break;
            }
        }

        //applies value of id to each checkbox in the repeater and binds database value to controls
        protected void rptSystems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DbDataRecord row = e.Item.DataItem as DbDataRecord;
                Button button = e.Item.FindControl("btnDisabled") as Button;

                if (button != null)
                {
                    if (row != null) button.Attributes.Add("id", row["id"].ToString());

                    button.Text = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled")) ? "Off" : "On";
                    button.CssClass = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled")) ? "disableButton" : "enableButton";
                    button.ToolTip = button.Text == "On" ? "Disable System" : "Enable System";
                }
            }
        }

        #endregion
    }
}