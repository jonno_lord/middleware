﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace Pages.Configuration.Systems
{
    public partial class SystemDetails : Page
    {
        #region Private fields and properties
   
        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int systemid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //gets specified system data and populates form
        private void GetSystem()
        {
            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_System", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@systemid", systemid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtShortName.Text = sdr["ShortName"].ToString();
                    txtAcronym.Text = sdr["Acronym"].ToString();
                    txtVersion.Text = sdr["Version"].ToString();
                    txtDescription.Text = sdr["Description"].ToString();
                    cbxDisable.Checked = (bool)sdr["Disabled"];
                    txtConnName.Text = sdr["ConnectionName"].ToString();
                }
            }
        }

        //should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void SetCancelRedirect()
        {
            //setting to 0 will represent no filtering
            //allows buttons user control to access id value
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via code in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (systemid != 0)
                {
                    GetSystem();
                }
            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("systems.aspx");
        }

        //gives value to filter jobs page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            SetCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves systems to the database either as an insert or update and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_System", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@systemid", systemid);
                command.Parameters.AddWithValue("@systemShortName", txtShortName.Text);
                command.Parameters.AddWithValue("@systemAcronym", txtAcronym.Text);
                command.Parameters.AddWithValue("@systemVersion", txtVersion.Text);
                command.Parameters.AddWithValue("@systemDescription", txtDescription.Text);
                command.Parameters.AddWithValue("@systemDisabled", cbxDisable.Checked);
                command.Parameters.AddWithValue("@systemConnName", txtConnName.Text);

                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}