﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Configuration.Systems.SystemDetails" Codebehind="SystemDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Middleware Systems</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsSystems" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
     <div class="formRow">
            <asp:Label ID="lblShortNameEnter" cssClass="formMargin labelInput" runat="server" Text="Name:" Width="100px"/>
            <asp:TextBox ID="txtShortName" CssClass="formMargin" runat="server" ToolTip="Name of the System" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvShortName" runat="server" ControlToValidate="txtShortName" ErrorMessage="Please Enter a Name for the System" >
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
            <asp:Label ID="lblVersionEnter" cssClass="formMargin labelInput" runat="server" Text="Version:" Width="100px"/>
            <asp:TextBox ID="txtVersion" CssClass="formMargin" runat="server" 
            ToolTip="Version of the System" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvVersion" runat="server" ControlToValidate="txtVersion" ErrorMessage="Please Enter a version number the System" >
        *
        </asp:RequiredFieldValidator>
    </div>
    
    <div class="formRow">                         
            <asp:Label ID="lblDescriptionEnter" cssClass="formMargin labelInput" runat="server" Text="Description:" Width="100px"/>
            <asp:TextBox ID="txtDescription" CssClass="formMargin" runat="server" ToolTip="Detail on what the system does" TextMode="MultiLine" Columns="40" Rows="3"/>
          <asp:RegularExpressionValidator ID="txtLengthValidator" ControlToValidate="txtDescription" ErrorMessage="Description is exceeding 1024 characters" ValidationExpression="^[\s\S]{0,1024}$" runat="server" >
          *
          </asp:RegularExpressionValidator>
          <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" ErrorMessage="Please Enter a description for the System" >
           *
          </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
            <asp:Label ID="lblDisabledEnter" cssClass="formMargin labelInput" runat="server" Text="Disabled:" Width="100px"/>
            <asp:CheckBox ID="cbxDisable" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Disable System" Checked="false" Height="12px" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">

 <div class="formRow">
            <asp:Label ID="lblAcronymEnter" cssClass="formMargin labelInput" runat="server" Text="Acronym:" Width="100px"/>         
            <asp:TextBox ID="txtAcronym" CssClass="formMargin" runat="server" ToolTip="Acronym of System Name" MaxLength="10" Width="300"/>
        <asp:RequiredFieldValidator ID="rfvAcronymEnter" runat="server" ControlToValidate="txtAcronym" ErrorMessage="Please Enter an Acronym the System" >
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
                <asp:Label ID="lblConnNameEnter" runat="server" cssClass="formMargin labelInput" Text="Connection Name:" Width="100px"></asp:Label>
                <asp:TextBox ID="txtConnName" CssClass="formMargin" runat="server" MaxLength="50" ToolTip="Connection string for system database" width="300"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvConnName" runat="server" ControlToValidate="txtConnName" ErrorMessage="Please Enter a Connection Name for the System" >
            *
            </asp:RequiredFieldValidator>
        </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="System" EntityURL="Systems.aspx" ConnectionString="MiddlewareSystem" deleteEnabled="true" deleteSPName="System" deleteParameter="system"/>
</asp:Content>

