﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Systems
{
    public partial class SystemNames : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //binds database dat to grid/repeater
        private void LoadGrid()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_SystemNames", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }

        //links to details page with id of system
        protected void rptSystemNames_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("SystemNameDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }

        #endregion
    }
}