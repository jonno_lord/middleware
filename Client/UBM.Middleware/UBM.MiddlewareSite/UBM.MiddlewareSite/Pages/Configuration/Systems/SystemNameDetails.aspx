﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Configuration.Systems.SystemNameDetails" Codebehind="SystemNameDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Middleware System Names</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsSystemName" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
 </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEntryForm">

       <div class="formRow">
            <asp:Label ID="lblSystemidEnter" CssClass="formMargin labelInput" runat="server" Text="Middleware System:" Width="100px"/>
            <asp:TextBox ID="txtSystemName" CssClass="formMargin" runat="server" MaxLength="6" Width="250" ToolTip="System matching the System Name" />          
        <asp:RequiredFieldValidator ID="rfvSystemName" runat="server" ControlToValidate="txtSystemName" ErrorMessage="Please Enter Name for the Middleware System" >
        *
        </asp:RequiredFieldValidator>
    </div>
    <div class="formRow">
            <asp:Label ID="lblSystemNameEnter" CssClass="formMargin labelInput" runat="server" Text="Middleware System Name:" Width="100px"/>
            <asp:DropDownList ID="ddlSystemid" CssClass="formMargin" runat="server" DataTextField="ShortName" DataValueField="id"  width="250" ToolTip="System Name">
            </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvSystemid" runat="server"  ControlToValidate="ddlSystemid" ErrorMessage="System Name required">
        *
        </asp:RequiredFieldValidator>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="System" EntityURL="SystemNames.aspx" ConnectionString="MiddlewareSystem" deleteEnabled="true" deleteSPName="SystemName" deleteParameter="SystemName"/>
</asp:Content>

