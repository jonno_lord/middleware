﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace Pages.Configuration.Systems
{
    public partial class SystemNameDetails : Page
    {
        #region Private fields and properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int systemNameid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }
        #endregion

        #region Private Methods

        //gets specified system data and populates form
        private void GetSystemName()
        {
            GetSystems();

            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_SystemName", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@systemNameid", systemNameid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlSystemid.SelectedValue = sdr["Systemid"].ToString();
                    txtSystemName.Text = sdr["SystemName"].ToString();
                }
            }
        }

        //populates system dropdown list
        private void GetSystems()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;
                ddlSystemid.DataSource = command.ExecuteReader();
                ddlSystemid.DataBind();
            }
        }

        //should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void SetCancelRedirect()
        {
            //setting to 0 will represent no filtering
            //allows buttons user control to access id value
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via code in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (systemNameid != 0)
                {
                    GetSystemName();
                }
                else
                {
                    GetSystems();
                }

            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("SystemNames.aspx");
        }

        //gives value to filter jobs page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            SetCancelRedirect();
        }


        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves systems to the database either as an insert or update and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_SystemName", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@systemNameid", systemNameid);
                command.Parameters.AddWithValue("@Systemid", ddlSystemid.SelectedValue);
                command.Parameters.AddWithValue("@systemName", txtSystemName.Text);

                command.ExecuteNonQuery();
            }
        }
            
        #endregion
    }
}