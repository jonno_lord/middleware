﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Configuration.Systems.Systems" Codebehind="Systems.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
     <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="System"  Title="MW System" AddEntityURL="SystemDetails.aspx" QueryStringFilter="id"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Systems" TableName="System" EntityURL="Systems.aspx" ConnectionString="MiddlewareSystem" disableEnable="true"/>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Middleware Systems</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
  <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptSystems_ItemCommand" onItemDataBound="rptSystems_ItemDataBound">
        <HeaderTemplate>
            <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>Edit</th>
                <th>System</th>
                <th>Acronym</th>
                <th>Version</th>
                <th>Description</th>
                <th>Connection Name</th>
                <th>Turn Off</th>
            </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
              <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit System " +Eval("ShortName")%>'/></td>
              <td><asp:hyperlink id="hypJob" runat="server" Text='<%# Eval("ShortName") %>' ToolTip='<%# Eval("ShortName") + " Jobs" %>' NavigateUrl='<%# "~/Pages/Configuration/Jobs/jobs.aspx?id=" +Eval("id") %>'/></td>
              <td class="GridCentre"><%# Eval("Acronym") %></td>
              <td class="GridCentre"><%# Eval("Version")%></td>
              <td><%# Eval("Description") %></td>
              <td class="GridCentre"><%# Eval("ConnectionName")%></td>
              <td class="GridCentre" style="width:20px;">
                 <asp:Button id="btnDisabled" runat="server" CommandName="btnDisable" CommandArgument='<%# Eval("id") %>' Width="55"/>
              </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
        </table>
        </FooterTemplate>
    </asp:Repeater>         
</asp:Content>

