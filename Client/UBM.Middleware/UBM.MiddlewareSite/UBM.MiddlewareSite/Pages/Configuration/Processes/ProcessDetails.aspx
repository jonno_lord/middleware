﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Configuration.Processes.ProcessDetails" Codebehind="ProcessDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Middleware Processes</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsProcesses" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
  <div class="formRow">
                <asp:Label ID="lblSystemidEnter" cssClass="formMargin labelInput" runat="server" Text="System:" Width="100px"/>
                <asp:DropDownList ID="ddlSystemid" CssClass="formMargin" runat="server" DataTextField="ShortName" DataValueField="id" ToolTip="Select the System for the Job you wish to configure a Process for" AutoPostBack="true" OnSelectedIndexChanged="GetJobNames" Width="250"/>
            <asp:RequiredFieldValidator ID="rfvSystemid" runat="server"  ControlToValidate="ddlSystemid" ErrorMessage="System id required">
            *
            </asp:RequiredFieldValidator>
        </div>

<asp:UpdatePanel ID="udpJobs" runat="server" >
<ContentTemplate>
        <div class="formRow">
            <asp:Label ID="lblJobidEnter" cssClass="formMargin labelInput" runat="server" Text="Job:" Width="100px"/>
            <asp:DropDownList ID="ddlJobid" CssClass="formMargin" runat="server" DataTextField="ShortName" DataValueField="id" ToolTip="Select the Job the Process applies to." Width="270">
            </asp:DropDownList>
        </div>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="ddlSystemid" EventName="SelectedIndexChanged" />
</Triggers> 
</asp:UpdatePanel>

<asp:RequiredFieldValidator ID="rfvJobid" runat="server"  ControlToValidate="ddlJobid" ErrorMessage="There is no Job ID selected">
    *
</asp:RequiredFieldValidator>

    <div class="formRow">
            <asp:Label ID="lblAssemblyNameEnter" cssClass="formMargin labelInput" runat="server" Text="Assembly Name:" Width="100px"/>
            <asp:TextBox ID="txtAssemblyName" CssClass="formMargin" runat="server" ToolTip="Name of Assembly" MaxLength="512" Width="250"/>
        <asp:RequiredFieldValidator ID="rfAssemblyName" runat="server" ControlToValidate="txtAssemblyName" ErrorMessage="Please Enter an Assembly Name for the Process" >
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
            <asp:Label ID="lblClassNameEnter" cssClass="formMargin labelInput" runat="server" Text="Class Name:" Width="100px"/>
            <asp:TextBox ID="txtClassName" CssClass="formMargin" runat="server" ToolTip="Name of the Class" MaxLegth="512" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvClassName" runat="server" ControlToValidate="txtClassName" ErrorMessage="Please Enter a Class Name for the Process" >
        *
        </asp:RequiredFieldValidator>
    </div>
    
    <div class="formRow">               
            <asp:Label ID="lblShortNameEnter" cssClass="formMargin labelInput" runat="server" Text="Name:" Width="100px"/>
            <asp:TextBox ID="txtShortName" CssClass="formMargin" runat="server" ToolTip="Name of the Process" MaxLength="50" Width="270"/>
        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtShortName" ErrorMessage="Please Enter a Name for the Process" >
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
            <asp:Label ID="lblProcessTypeidEnter" cssClass="formMargin labelInput" runat="server" Text="Process Type:" Width="100px"/>
            <asp:DropDownList ID="ddlProcessTypeid" CssClass="formMargin" runat="server" DataTextField="ProcessType" DataValueField="id" ToolTip="Type of Process" Width="250">
            </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvProcessTypeid" runat="server"  ControlToValidate="ddlProcessTypeid" ErrorMessage="Process Type id required">
        *
        </asp:RequiredFieldValidator>
    </div>      

    <div class="formRow">
            <asp:Label ID="lblDescriptionEnter" cssClass="formMargin labelInput" runat="server" Text="Description:" Width="100px"/>
            <asp:TextBox ID="txtDescription" CssClass="formMargin" runat="server" ToolTip="Detail on what the Process does" TextMode="MultiLine" Columns="39" Rows="2" />
        <asp:RegularExpressionValidator ID="txtLengthValidator" ControlToValidate="txtDescription" ErrorMessage="Description is exceeding 1024 characters" ValidationExpression="^[\s\S]{0,1024}$" runat="server" >
        *
        </asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" ErrorMessage="Please Enter a Description for the Process" >
        *
        </asp:RequiredFieldValidator>
    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">

    <div class="formRow">
            <asp:Label ID="lblSequenceEnter" cssClass="formMargin labelInput" runat="server" Text="Sequence:" Width="100px" />
            <asp:TextBox ID="txtSequence" CssClass="formMargin" runat="server" ToolTip="Sequence Number" MaxLength="6" Width="250"/>
    </div>

    <div class="formRow">
            <asp:Label ID="lblStatusEnter" cssClass="formMargin labelInput" runat="server" Text="Status:" Width="100px"/>
            <asp:DropDownList ID="ddlStatus" CssClass="formMargin" runat="server" ToolTip="Status of the Process (Note: Middleware will update this)" Width="250">
                <asp:ListItem>COMPLETE</asp:ListItem>
                <asp:ListItem>CREATING</asp:ListItem>
                <asp:ListItem>DISABLED</asp:ListItem>
                <asp:ListItem>FAILED</asp:ListItem>
                <asp:ListItem>PROCESSING</asp:ListItem>
                <asp:ListItem>WAITING</asp:ListItem>
            </asp:DropDownList>
    </div>

    <div class="formRow">      
           <asp:Label ID="lblLastExecutedEnter" cssClass="formMargin labelInput" runat="server" Text="Last Executed:" Width="100px"/>
           <asp:TextBox ID="txtLastExecuted" CssClass="formMargin" runat="server" ToolTip="Date of last execution of the Process" MaxLength="20" Width="250"/>
    </div>       
    
    <div class="formRow">   
            <asp:Label ID="lblDisabledEnter" cssClass="formMargin labelInput" runat="server" Text="Disabled:" Width="100px"/>
            <asp:CheckBox ID="cbxDisable" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Disable Process" Checked="false" Height="12px"/>
  </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
      <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Process" EntityURL="Processes.aspx" deleteEnabled="true" ConnectionString="MiddlewareSystem" deleteSPName="Process" deleteParameter="process"/>
</asp:Content>

