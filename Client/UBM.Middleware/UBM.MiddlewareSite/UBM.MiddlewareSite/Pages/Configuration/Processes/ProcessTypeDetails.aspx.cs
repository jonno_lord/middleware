﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace Pages.Configuration.Processes
{
    public partial class ProcessTypeDetails : Page
    {
        #region Private fields and properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int processTypeid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetProcessType()
        {

            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_ProcessType", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@processTypeid", processTypeid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtProcessType.Text = sdr["ProcessType"].ToString();
                }
            }
        }

        //should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void SetCancelRedirect()
        {
            //setting to 0 will represent no filtering
            //allows buttons user control to access id value
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow process type to be saved from button user control via code in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (processTypeid != 0)
                {
                    GetProcessType();
                }
            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("ProcessTypes.aspx");
        }

        //gives value to filter jobs page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            SetCancelRedirect();
        }

        #endregion

        #region Public Methods

        public void SaveEntity()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_ProcessType", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@processTypeid", processTypeid);
                command.Parameters.AddWithValue("@processType", txtProcessType.Text);

                command.ExecuteNonQuery();

            }

        }

        #endregion
    }
}