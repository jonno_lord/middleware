﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Processes
{
    public partial class Processes : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private bool filter;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        private int jobid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //checks if page is filtered by dropdowns
        private bool isFiltered()
        {
            if (ddlFilterJobs.SelectedIndex == 0 & ddlFilterStatus.SelectedIndex == 0)
            {
                filter = false;
            }
            else
            {
                filter = true;
            }

            return filter;
        }

        //loads data from database to grid/repeater
        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                retrieveFilterValues();

                SqlCommand command = new SqlCommand("usp_SEL_Processes", connection);
                command.CommandType = CommandType.StoredProcedure;

                //-- if the user selected the first item, then it represents no filtering

                if (ddlFilterStatus.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@status", ddlFilterStatus.SelectedValue);
                }

                //filter by job from jobs page
                //followed by filter by chosen job on process page
                if (Request.QueryString["id"] != null)
                {
                    command.Parameters.AddWithValue("@jobid", Request.QueryString["id"]);
                }
                else if (ddlFilterJobs.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@jobid", ddlFilterJobs.SelectedValue);
                }

                SqlDataReader sdr = command.ExecuteReader();
                
                rptGrid.DataSource = sdr;
                rptGrid.DataBind();

            }
        }

        //populates job dropdown list filter
        private void GetJobNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_JobsByName", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrJobs = command.ExecuteReader();

                ListItem itmJob = new ListItem("ALL");
                ddlFilterJobs.Items.Add(itmJob);

                while (sdrJobs.Read())
                {
                    itmJob = new ListItem();
                    itmJob.Value = sdrJobs["id"].ToString();
                    itmJob.Text = sdrJobs["ShortName"].ToString();

                    if (Request.QueryString["id"] != null)
                    {
                        if (itmJob.Value == Request.QueryString["id"])
                        {
                            itmJob.Selected = true;
                        }
                    }

                    ddlFilterJobs.Items.Add(itmJob);
                }
            }
        }

        //gets system of job to be used as querystring for filtering add action
        private int GetSystemIdFromJobId()
        {
            int returnJobId = 0;

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_GET_Job", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@jobid", jobid);

                SqlDataReader sdrJobs = command.ExecuteReader();
                if (sdrJobs.Read())
                {
                    returnJobId = int.Parse(sdrJobs["SystemId"].ToString());
                }
            }

            return returnJobId;
        }

        //retrieves status filter from session
        private void retrieveFilterValues()
        {
            //maintains status filter if system filter is used and causes a redirect
            if (Session["statusValue"] != null & Session["statusIndex"] != null)
            {
                ddlFilterStatus.SelectedValue = Session["statusValue"].ToString();

                ddlFilterStatus.SelectedIndex = int.Parse(Session["statusIndex"].ToString());
            }
        }

        // This function prevent the page being retrieved from broswer cache
        private void ExpirePageCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now - new TimeSpan(1, 0, 0));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger statusTrigger = new AsyncPostBackTrigger();

            statusTrigger.ControlID = ddlFilterStatus.UniqueID;

            statusTrigger.EventName = "SelectedIndexChanged";

            updProcesses.Triggers.Add(statusTrigger);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ExpirePageCache();

            if (!IsPostBack)
            {
                LoadGrid();
                GetJobNames();

                //if page is filtered keep summary on
                if (isFiltered())
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: summaryDisplayNoAnimation(); ", true);
                }

            }

            if (jobid != 0)
            {
                //gets systemid of selected job
                int systemid = Convert.ToInt32(GetSystemIdFromJobId());
                SummaryActions1.Secondid = systemid;
            }
        }

        //links to details page with id of process
        protected void rptProcesses_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("ProcessDetails.aspx?id=" + e.CommandArgument);

                    break;

                case "btndisable":

                    using (connection = new SqlConnection(connectionString))
                    {
                        //changes text to bool value
                        Button button = e.Item.FindControl("btnDisabled") as Button;

                        if (button != null)
                        {
                            bool btnDisabled;

                            if (button.Text == "On")
                            {
                                button.Text = "Off";
                                btnDisabled = true;
                            }
                            else
                            {
                                button.Text = "On";
                                btnDisabled = false;
                            }

                            connection.Open();

                            SqlCommand command = new SqlCommand("usp_UPD_Process", connection);
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@processid", e.CommandArgument);
                            command.Parameters.AddWithValue("@processDisabled", btnDisabled);

                            command.ExecuteReader();

                            button.CssClass = button.Text == "Off" ? "disableButton" : "enableButton";
                            button.ToolTip = button.Text == "On" ? "Disable Process " : "Enable Process";

                        }
                    }

                    break;
            }
        }

        //applies value of id to each checkbox in the repeater and binds database value to controls
        protected void rptProcesses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DbDataRecord row = e.Item.DataItem as DbDataRecord;
                Button button = e.Item.FindControl("btnDisabled") as Button;

                if (button != null)
                {
                    if (row != null) button.Attributes.Add("id", row["id"].ToString());

                    button.Text = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled")) ? "Off" : "On";
                    button.CssClass = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled")) ? "disableButton" : "enableButton";
                    button.ToolTip = button.Text == "On" ? "Disable Process " : "Enable Process";
                }
            }
        }

        //filters grid contents by status
        protected void ddlFilterStatusSelect(object sender, EventArgs e)
        {
            //stores value to be recalled on response.redirect
            Session["statusValue"] = ddlFilterStatus.SelectedValue;
            Session["statusIndex"] = ddlFilterStatus.SelectedIndex;

            LoadGrid();

        }

        //filters grid content by job
        protected void ddlFilterJobsSelect(object sender, EventArgs e)
        {
            if (ddlFilterJobs.SelectedIndex == 0)
            {
                Response.Redirect("Processes.aspx");
            }
            else
            {
                Response.Redirect("Processes.aspx?id=" + ddlFilterJobs.SelectedValue);
            }
        }
        #endregion
    }
}