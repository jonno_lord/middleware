﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Processes
{
    public partial class ProcessDetails : Page
    {
        #region Private fields and properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int processid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //gets specified process data and populates form
        private void GetProcess()
        {
            GetJobNames();
            GetProcessTypeNames();
            GetSystemNames();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Process", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@processid", processid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlJobid.SelectedValue = sdr["Jobid"].ToString();
                    txtSequence.Text = sdr["Sequence"].ToString();
                    txtAssemblyName.Text = sdr["AssemblyName"].ToString();
                    txtClassName.Text = sdr["ClassName"].ToString();
                    txtShortName.Text = sdr["ShortName"].ToString();
                    ddlProcessTypeid.SelectedValue = sdr["ProcessTypeid"].ToString();
                    txtDescription.Text = sdr["Description"].ToString();
                    ddlStatus.SelectedValue = sdr["Status"].ToString();
                    txtLastExecuted.Text = String.Format("{0:yyyy-MM-dd HH:MM:ss}", sdr["LastExecuted"]);
                    cbxDisable.Checked = (bool)sdr["Disabled"];
                    ddlSystemid.SelectedValue = sdr["Systemid"].ToString();

                }
            }
        }

        //  should be set in each screen depending on what value you want the cancel redirect to filter on.     
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //   setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                selectedValue = Convert.ToInt32(ddlJobid.SelectedValue);
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        //populates process type dropdown list
        private void GetProcessTypeNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_ProcessTypes", connection);
                command.CommandType = CommandType.StoredProcedure;
                ddlProcessTypeid.DataSource = command.ExecuteReader();
                ddlProcessTypeid.DataBind();
            }

        }

        //populates system dropdown list
        private void GetSystemNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                while (sdrSystems.Read())
                {
                    ListItem itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    if (Request.QueryString["addSystemid"] != null)
                    {
                        if (itmSystem.Value == Request.QueryString["addSystemid"])
                        {
                            itmSystem.Selected = true;
                        }
                    }

                    ddlSystemid.Items.Add(itmSystem);
                }
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow process to be saved from button user control via code in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (processid != 0)
                {
                    GetProcess();
                    txtLastExecuted.Enabled = false;
                }
                else if (Request.QueryString["addid"] != null)
                {
                    GetSystemNames();
                    GetJobNames();
                    GetProcessTypeNames();
                }
                else
                {
                    GetSystemNames();
                    GetJobNames();
                    GetProcessTypeNames();
                }
            }
        }

        //calls get job names when triggered
        protected void GetJobNames(object sender, EventArgs e)
        {
            GetJobNames();
        }

        //populates the job dropdown list filter
        protected void GetJobNames()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Jobs", connection);
                command.Parameters.AddWithValue("@systemid", ddlSystemid.SelectedValue);

                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrJobs = command.ExecuteReader();

                ddlJobid.Items.Clear();

                while (sdrJobs.Read())
                {
                    ListItem itmJob = new ListItem();
                    itmJob.Value = sdrJobs["id"].ToString();
                    itmJob.Text = sdrJobs["ShortName"].ToString();

                    if (Request.QueryString["addid"] != null)
                    {
                        if (itmJob.Value == Request.QueryString["addid"])
                        {
                            itmJob.Selected = true;
                        }
                    }
                    ddlJobid.Items.Add(itmJob);
                }
            }
        }

        //called by button control to save process and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of process being created or edited
            Response.Redirect("processes.aspx?id=" + ddlJobid.SelectedValue);
        }

        //gives value to filter process page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves process to the database either as an insert or update and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_Process", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@processid", processid);
                command.Parameters.AddWithValue("@processJobid", ddlJobid.SelectedValue);
                command.Parameters.AddWithValue("@processSequence", txtSequence.Text);
                command.Parameters.AddWithValue("@processAssemblyName", txtAssemblyName.Text);
                command.Parameters.AddWithValue("@processClassName", txtClassName.Text);
                command.Parameters.AddWithValue("@processShortName", txtShortName.Text);
                command.Parameters.AddWithValue("@processTypeid", ddlProcessTypeid.SelectedValue);
                command.Parameters.AddWithValue("@processDescription", txtDescription.Text);
                command.Parameters.AddWithValue("@processStatus", ddlStatus.SelectedValue);
                command.Parameters.AddWithValue("@processLastExecuted", txtLastExecuted.Text);
                command.Parameters.AddWithValue("@processDisabled", cbxDisable.Checked);

                command.ExecuteNonQuery();

            }
        }
        #endregion
    }
}