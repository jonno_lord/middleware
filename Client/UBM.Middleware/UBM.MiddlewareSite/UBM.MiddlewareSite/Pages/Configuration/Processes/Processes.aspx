﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Configuration.Processes.Processes" Codebehind="Processes.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Process"  Title="MW System" AddEntityURL="ProcessDetails.aspx" QueryStringFilter="id" SecondQueryStringFilter="systemid"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
 <div class="filter">
     <div class="rowA">
            <p>Status:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList ID="ddlFilterStatus" runat="server" tooltip="Status of the Process" OnSelectedIndexChanged="ddlFilterStatusSelect" AutoPostBack="True">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>COMPLETE</asp:ListItem>
                <asp:ListItem>CREATING</asp:ListItem>
                <asp:ListItem>DISABLED</asp:ListItem>
                <asp:ListItem>FAILED</asp:ListItem>
                <asp:ListItem>PROCESSING</asp:ListItem>
                <asp:ListItem>WAITING</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="rowA">
            <p>Job:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList ID="ddlFilterJobs" runat="server" ToolTip="Job Process belongs to" onselectedindexchanged="ddlFilterJobsSelect" AutoPostBack="True"/>
        </div>
 </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Processes" TableName="Process" EntityURL="Processes.aspx" ConnectionString="MiddlewareSystem" disableEnable="true" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Middleware Processes</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
 <asp:UpdatePanel ID="updProcesses" runat="server" >
        <ContentTemplate>
            <asp:UpdateProgress ID="uppProcesses" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updProcesses">
                <ProgressTemplate>
                     <div class="progressIndicator"> Loading...<asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptProcesses_ItemCommand" onItemDataBound="rptProcesses_ItemDataBound">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Job</th>
                        <th>Process</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Last Executed</th>
                        <th>Turn Off</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Process " +Eval("ShortName")%>'/></td>
                      <td><%# Eval("JobName") %></td>
                      <td><%# Eval("ShortName") %></td>
                      <td class="GridCentre"><%# Eval("ProcessType") %></td>
                      <td><%# Eval("Description")%></td>
                      <td class="GridCentre"> <img id="imgStatus" class="imgStatus" src="../../../images/JobStatus/<%# Eval("Status")%>.png" alt="Process Status is <%# Eval("Status")%>"  title="Process Status is <%# Eval("Status")%>"  /></td>
                      <td><%# Eval("LastExecuted")%></td>
                      <td class="GridCentre" style="width:20px;">             
                           <asp:Button id="btnDisabled" runat="server" CommandName="btnDisable" CommandArgument='<%# Eval("id") %>' Width="55"/>
                      </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>

<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>
    
</asp:Content>

