﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Configuration.Processes.ProcessTypes" Codebehind="ProcessTypes.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Process"  Title="MW System" AddEntityURL="ProcessTypeDetails.aspx" QueryStringFilter="id"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Process" TableName="ProcessTypes" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Middleware Process Types</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
<asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptProcessTypes_ItemCommand">
        <HeaderTemplate>
            <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>Edit</th>
                <th>Process Type</th>
            </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
              <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit " +Eval("ProcessType")%>'/></td>
              <td class="GridCentre"><%# Eval("ProcessType") %></td>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
        </table>
        </FooterTemplate>
    </asp:Repeater>         
</asp:Content>

