﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Configuration.Schedules.Schedules" Codebehind="Schedules.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Schedule"  Title="MW System" AddEntityURL="ScheduleDetails.aspx" QueryStringFilter="id" SecondQueryStringFilter="jobid"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
<div class="filter">
     <div class="rowA">
            <p>System:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList ID="ddlFilterSystems" runat="server" tooltip="System of the Schedule" OnSelectedIndexChanged="ddlFilterSystemSelect" AutoPostBack="True"/>
        </div>
        <div class="rowA">
            <p>Job:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList ID="ddlFilterJobs" runat="server" ToolTip="Job of the Schedule" onselectedindexchanged="ddlFilterJobSelect" AutoPostBack="True"/>
        </div>
 </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Schedules" TableName="Schedule" EntityURL="Schedules.aspx" ConnectionString="MiddlewareSystem" disableEnable="true" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Middleware Schedules</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
<asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptSchedule_ItemCommand" onItemDataBound="rptSchedule_ItemDataBound">
        <HeaderTemplate>
            <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>Edit</th>
                <th>Job</th>
                <th>Days of the Week</th>
                <th>Start Date</th>
                <th>Start Time</th>
                <th>End Date</th>
                <th>End Time</th>
                <th>Freq (s)</th>
                <th>Turn Off</th>
            </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
              <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") + "&jobid=" + Eval("jobid") + "&systemid=" + Eval("Systemid")%>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Schedule " +Eval("ShortName")%>'/></td>
              <td><%# Eval("ShortName") %></td>
              <td><%# Eval("Days") %></td>
              <td class="GridCentre"><%# DataBinder.Eval(Container.DataItem, "StartDate", "{0:yyyy-MM-dd}") %></td>
              <td class="GridCentre"><%# Eval("StartTime") %></td>
              <td class="GridCentre"><%# DataBinder.Eval(Container.DataItem, "EndDate", "{0:yyyy-MM-dd}") %></td>
              <td class="GridCentre"><%# Eval("EndTime") %></td>
              <td class="GridCentre"><%# Eval("Frequency") %></td>
              <td class="GridCentre" style="width:20px;">             
                   <asp:Button id="btnDisabled" runat="server" CommandName="btnDisable" CommandArgument='<%# Eval("id") %>' Width="55"/>
              </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
        </table>
        </FooterTemplate>
    </asp:Repeater>         
</asp:Content>

