﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Pages.Configuration.Schedules
{
    public partial class Schedules : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private bool filter;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //checks if page is filtered by dropdowns
        private bool isFiltered()
        {
            if (ddlFilterJobs.SelectedIndex == 0 & ddlFilterSystems.SelectedIndex == 0)
            {
                filter = false;
            }
            else
            {
                filter = true;
            }

            return filter;
        }

        //loads data from database to grid/repeater
        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_Schedulesv2", connection);
                command.CommandType = CommandType.StoredProcedure;

                if (Request.QueryString["id"] != null)
                {
                    command.Parameters.AddWithValue("@systemid", Request.QueryString["id"]);

                    //filter of jobs from edit page
                    if (Session["jobid"] != null)
                    {
                        command.Parameters.AddWithValue("@jobid", Session["jobid"]);
                    }
                    else if (ddlFilterJobs.SelectedIndex != 0)
                    {
                        command.Parameters.AddWithValue("@jobid", ddlFilterJobs.SelectedValue);
                    }
                }

                //if not edit or add but drop down changed
                if (Request.QueryString["id"] == null && ddlFilterJobs.SelectedIndex != 0)
                {
                    command.Parameters.AddWithValue("@jobid", ddlFilterJobs.SelectedValue);
                }

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();

                GetXML();
            }
        }

        ///// <summary>
        ///// On grid load, GetXML will run and take the schedule information and place it within an xml file, so on Insert, Update, Delete
        ///// </summary>
        private void GetXML()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Schedules", connection);
                command.CommandType = CommandType.StoredProcedure;


                SqlDataReader sdr = command.ExecuteReader();
                XmlDocument xmlDoc = new XmlDocument();

                XmlElement schedules = xmlDoc.CreateElement("schedules");
                xmlDoc.AppendChild(schedules);

                while (sdr.Read())
                {
                    {
                        XmlElement schedule = xmlDoc.CreateElement("schedule");
                        schedules.AppendChild(schedule);

                        XmlElement id = xmlDoc.CreateElement("scheduleid");
                        id.InnerText = sdr["id"].ToString();
                        schedule.AppendChild(id);
                        XmlElement jobid = xmlDoc.CreateElement("jobid");
                        jobid.InnerText = sdr["jobid"].ToString();
                        schedule.AppendChild(jobid);
                        XmlElement description = xmlDoc.CreateElement("description");
                        description.InnerText = sdr["Description"].ToString();
                        schedule.AppendChild(description);
                        XmlElement startDate = xmlDoc.CreateElement("startdate");
                        startDate.InnerText = sdr["StartDate"].ToString();
                        schedule.AppendChild(startDate);
                        XmlElement startTime = xmlDoc.CreateElement("starttime");
                        startTime.InnerText = sdr["StartTime"].ToString();
                        schedule.AppendChild(startTime);
                        XmlElement endDate = xmlDoc.CreateElement("enddate");
                        endDate.InnerText = sdr["EndDate"].ToString();
                        schedule.AppendChild(endDate);
                        XmlElement endTime = xmlDoc.CreateElement("endtime");
                        endTime.InnerText = sdr["EndTime"].ToString();
                        schedule.AppendChild(endTime);
                        XmlElement monday = xmlDoc.CreateElement("monday");
                        monday.InnerText = sdr["Monday"].ToString();
                        schedule.AppendChild(monday);
                        XmlElement tuesday = xmlDoc.CreateElement("tuesday");
                        tuesday.InnerText = sdr["Tuesday"].ToString();
                        schedule.AppendChild(tuesday);
                        XmlElement wednesday = xmlDoc.CreateElement("wednesday");
                        wednesday.InnerText = sdr["Wednesday"].ToString();
                        schedule.AppendChild(wednesday);
                        XmlElement thursday = xmlDoc.CreateElement("thursday");
                        thursday.InnerText = sdr["Thursday"].ToString();
                        schedule.AppendChild(thursday);
                        XmlElement friday = xmlDoc.CreateElement("friday");
                        friday.InnerText = sdr["Friday"].ToString();
                        schedule.AppendChild(friday);
                        XmlElement saturday = xmlDoc.CreateElement("saturday");
                        saturday.InnerText = sdr["Saturday"].ToString();
                        schedule.AppendChild(saturday);
                        XmlElement sunday = xmlDoc.CreateElement("sunday");
                        sunday.InnerText = sdr["Sunday"].ToString();
                        schedule.AppendChild(sunday);
                        XmlElement frequency = xmlDoc.CreateElement("frequency");
                        frequency.InnerText = sdr["frequency"].ToString();
                        schedule.AppendChild(frequency);
                        XmlElement disabled = xmlDoc.CreateElement("disabled");
                        disabled.InnerText = sdr["Disabled"].ToString();
                        schedule.AppendChild(disabled);
                    }


                        xmlDoc.Save(Server.MapPath("~") + "/Schedule/Schedule.xml");

                }

            }
        }

        //populates job dropdown list filter
        private void GetJobNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_JobsByName", connection);

                if (ddlFilterSystems.SelectedValue != "ALL")
                {
                    command.Parameters.AddWithValue("status", "");
                    command.Parameters.AddWithValue("systemid", ddlFilterSystems.SelectedValue);
                }

                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrJobs = command.ExecuteReader();

                ListItem itmJob = new ListItem("ALL");
                ddlFilterJobs.Items.Add(itmJob);

                while (sdrJobs.Read())
                {
                    itmJob = new ListItem();
                    itmJob.Value = sdrJobs["id"].ToString();
                    itmJob.Text = sdrJobs["ShortName"].ToString();

                    if (Request.QueryString["jobid"] != null)
                    {
                        if (itmJob.Value == Request.QueryString["jobid"])
                        {
                            itmJob.Selected = true;
                        }
                    }

                    ddlFilterJobs.Items.Add(itmJob);
                }
            }
        }

        //populates system dropdown list filter
        private void GetSystemNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                ListItem itmSystem = new ListItem("ALL");
                ddlFilterSystems.Items.Add(itmSystem);

                while (sdrSystems.Read())
                {
                    itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    if (Request.QueryString["id"] != null)
                    {
                        if (itmSystem.Value == Request.QueryString["id"])
                        {
                            itmSystem.Selected = true;
                        }                        
                    }

                    ddlFilterSystems.Items.Add(itmSystem);
                }
            }
        }

        // This function prevent the page being retrieved from broswer cache
        private void ExpirePageCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now - new TimeSpan(1, 0, 0));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ExpirePageCache();

            if (!IsPostBack)
            {
                GetSystemNames();
                GetJobNames();
                LoadGrid();

                //if page is filtered keep summary on
                if (isFiltered())
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: summaryDisplayNoAnimation(); ", true);
                }
            }
            //gives add button jobid for redirect
            if (Session["jobid"] != null)
            {
                int jobid = Convert.ToInt32(Session["jobid"].ToString());
                SummaryActions1.Secondid = jobid;
            }
        }
        //links to details page with id of schedule
        protected void rptSchedule_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("ScheduleDetails.aspx?id=" + e.CommandArgument);

                    break;

                case "btndisable":

                    using (connection = new SqlConnection(connectionString))
                    {
                        //changes text to bool value
                        Button button = e.Item.FindControl("btnDisabled") as Button;

                        if (button != null)
                        {
                            bool btnDisabled;

                            if (button.Text == "On")
                            {
                                button.Text = "Off";
                                btnDisabled = true;
                            }
                            else
                            {
                                button.Text = "On";
                                btnDisabled = false;
                            }

                            connection.Open();

                            SqlCommand command = new SqlCommand("usp_UPD_Schedule", connection);
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@scheduleid", e.CommandArgument);
                            command.Parameters.AddWithValue("@scheduleDisabled", btnDisabled);

                            command.ExecuteReader();

                            button.CssClass = button.Text == "Off" ? "disableButton" : "enableButton";
                            button.ToolTip = button.Text == "On" ? "Disable Schedule " : "Enable Schedule";

                        }
                    }

                    break;
            }
        }

        //applies value of id to each checkbox in the repeater and binds database value to controls
        protected void rptSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DbDataRecord row = e.Item.DataItem as DbDataRecord;

                Button button = e.Item.FindControl("btnDisabled") as Button;

                if (button != null)
                {
                    if (row != null) button.Attributes.Add("id", row["id"].ToString());

                    button.Text = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled")) ? "Off" : "On";
                    button.CssClass = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled")) ? "disableButton" : "enableButton";
                    button.ToolTip = button.Text == "On" ? "Disable Schedule " : "Enable Schedule";
                }
            }
        }

        //filters by system
        protected void ddlFilterSystemSelect(object sender, EventArgs e)
        {
            //clear jobid session variable
            if (Session["jobid"] != null)
            {
                Session.Remove("jobid");
            }

            if (ddlFilterSystems.SelectedIndex == 0)
            {
                Response.Redirect("Schedules.aspx");
            }
            else
            {
                Response.Redirect("Schedules.aspx?id=" + ddlFilterSystems.SelectedValue);
            }

        }

        //filters by job
        protected void ddlFilterJobSelect(object sender, EventArgs e)
        {
            //clear jobid session variable
            if (Session["jobid"] != null)
            {
                Session.Remove("jobid");
            }

            //stores jobid for add page filtering
            if (ddlFilterJobs.SelectedValue == "ALL")
            {
                Session["jobid"] = 0;
            }
            else
            {
                Session["jobid"] = ddlFilterJobs.SelectedValue;
            }

            LoadGrid();
        }

        #endregion
    }
}