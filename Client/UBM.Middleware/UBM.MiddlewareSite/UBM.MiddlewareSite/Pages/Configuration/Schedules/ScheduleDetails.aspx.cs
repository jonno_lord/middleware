﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Configuration.Schedules
{
    public partial class ScheduleDetails : Page
    {
        #region Private fields and properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int scheduleid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        ///   Gets the schedule data for a specified scheduleid as selected from the schedule page and display
        ///   in the form controls. Also populates the drop down lists using methods GetJobNames and GetSystemNames.
        /// </summary>
        private void GetSchedule()
        {
            GetSystemNames();
            GetJobNames();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Schedule", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@scheduleid", scheduleid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlJobid.SelectedValue = sdr["Jobid"].ToString();
                    txtDescription.Text = sdr["Description"].ToString();
                    txtStartDate.Text = String.Format("{0:yyyy-MM-dd}", sdr["StartDate"]);
                    txtStartTime.Text = sdr["StartTime"].ToString();
                    txtEndDate.Text = String.Format("{0:yyyy-MM-dd}", sdr["EndDate"]);
                    txtEndTime.Text = sdr["EndTime"].ToString();
                    cbxMonday.Checked = (bool)sdr["Monday"];
                    cbxTuesday.Checked = (bool)sdr["Tuesday"];
                    cbxWednesday.Checked = (bool)sdr["Wednesday"];
                    cbxThursday.Checked = (bool)sdr["Thursday"];
                    cbxFriday.Checked = (bool)sdr["Friday"];
                    cbxSaturday.Checked = (bool)sdr["Saturday"];
                    cbxSunday.Checked = (bool)sdr["Sunday"];
                    txtFrequency.Text = sdr["Frequency"].ToString();
                    cbxDisable.Checked = (bool)sdr["Disabled"];

                    ddlSystemid.SelectedValue = sdr["systemid"].ToString();
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //   setting to 0 will represent no filtering
            //allows buttons user control to access systemid value and job id value
            int selectedSystem = 0;
            int selectedJob = 0;

            try
            {
                selectedSystem = Convert.ToInt32(ddlSystemid.SelectedValue);
                CommonButtons.Cancelid = selectedSystem;

                selectedJob = Convert.ToInt32(ddlJobid.SelectedValue);
                CommonButtons.OptionalCancelid = selectedJob;
            }
            catch
            {
                CommonButtons.Cancelid = selectedSystem;
                CommonButtons.OptionalCancelid = selectedJob;
            }
        }

        //populates system dropdown filter
        private void GetSystemNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                while (sdrSystems.Read())
                {
                    ListItem itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    if (Request.QueryString["addid"] != null)
                    {
                        if (itmSystem.Value == Request.QueryString["addid"])
                        {
                            itmSystem.Selected = true;
                        }
                    }

                    ddlSystemid.Items.Add(itmSystem);
                }
            }
        }

        //Load Dropdownlist with different time criteria
        private void LoadDdlTime()
        {
            ddlTime.Items.Add("Seconds");
            ddlTime.Items.Add("Minutes");
            ddlTime.Items.Add("Hours");
        }

        //this method converts the Frequency time into seconds if entered as minutes or hours
        private int CalculateTimeinSecs()
        {
            int freqTime = Int32.Parse(txtFrequency.Text);

            switch (ddlTime.SelectedValue)
            {
                case "Seconds":
                    break;
                case "Minutes":
                    freqTime = freqTime * 60;
                    break;
                case "Hours":
                    freqTime = freqTime * 3600;
                    break;
                default:
                    break;
            }
            return freqTime;
        }

        //dynamically adds compare validator for dates, as they are in different content areas
        private void addCompareDateValidator()
        {
            //if (Master != null)
            //{
            //    TextBox textStart = (TextBox)Master.FindControl("leftForm").FindControl("txtStartDate");
            //    TextBox textEnd = (TextBox)Master.FindControl("rightForm").FindControl("txtEndDate");  
            //    CompareValidator cvlDate = new CompareValidator();
            //    cvlDate.ControlToCompare =  textStart.UniqueID;
            //    cvlDate.ControlToValidate = textEnd.UniqueID;
            //    cvlDate.Operator = ValidationCompareOperator.GreaterThanEqual;
            //    cvlDate.Type = ValidationDataType.Date;
            //    cvlDate.ErrorMessage = "Start Date must be before End Date";
            //    cvlDate.Text = "*";
            //}
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {
            //dynamically adds validator
            //addCompareDateValidator();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow schedule to be saved from button user control via code in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (scheduleid != 0)
                {
                    GetSchedule();
                    GetJobNames();
                    LoadDdlTime();
                }
                else
                {
                    GetSystemNames();
                    GetJobNames();
                    LoadDdlTime();
                }
            }
        }

        //calls get jobs name method when invoked 
        protected void GetJobNames(object sender, EventArgs e)
        {
            GetJobNames();
        }

        //populates job dropdownlist filter
        protected void GetJobNames()
        {
            ddlJobid.Items.Clear();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Jobs", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@systemid", ddlSystemid.SelectedValue);

                SqlDataReader sdrJobs = command.ExecuteReader();


                while (sdrJobs.Read())
                {
                    ListItem itmJob = new ListItem();
                    itmJob.Value = sdrJobs["id"].ToString();
                    itmJob.Text = sdrJobs["ShortName"].ToString();

                    //if its add use add qstring, or edit use edit qstring
                    string jobid = "";

                    if (Request.QueryString["jobid"] != null)
                    {
                        jobid = Request.QueryString["jobid"];
                    }
                    else if (Request.QueryString["addjobid"] != null)
                    {
                        jobid = Request.QueryString["addjobid"];
                    }

                    if (itmJob.Value == jobid)
                    {
                        itmJob.Selected = true;
                    }

                    ddlJobid.Items.Add(itmJob);
                }
            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited

            Session["jobid"] = ddlJobid.SelectedValue;

            Response.Redirect("schedules.aspx?id=" + ddlSystemid.SelectedValue + "&jobid=" + ddlJobid.SelectedValue);
        }

        //gives value to filter process page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_Schedule", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@scheduleid", scheduleid);
                command.Parameters.AddWithValue("@schedulejobid", ddlJobid.SelectedValue);
                command.Parameters.AddWithValue("@scheduleDescription", txtDescription.Text);
                command.Parameters.AddWithValue("@scheduleStartDate", txtStartDate.Text);
                command.Parameters.AddWithValue("@scheduleStartTime", txtStartTime.Text);
                command.Parameters.AddWithValue("@scheduleEndDate", (txtEndDate.Text.Length == 0 ? null : txtEndDate.Text));
                command.Parameters.AddWithValue("@scheduleEndTime", txtEndTime.Text);
                command.Parameters.AddWithValue("@scheduleMonday", cbxMonday.Checked);
                command.Parameters.AddWithValue("@scheduleTuesday", cbxTuesday.Checked);
                command.Parameters.AddWithValue("@scheduleWednesday", cbxWednesday.Checked);
                command.Parameters.AddWithValue("@scheduleThursday", cbxThursday.Checked);
                command.Parameters.AddWithValue("@scheduleFriday", cbxFriday.Checked);
                command.Parameters.AddWithValue("@scheduleSaturday", cbxSaturday.Checked);
                command.Parameters.AddWithValue("@scheduleSunday", cbxSunday.Checked);
                command.Parameters.AddWithValue("@scheduleFrequency", CalculateTimeinSecs());
                command.Parameters.AddWithValue("@scheduleDisabled", cbxDisable.Checked);

                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}