﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Configuration.Schedules.ScheduleDetails" Codebehind="ScheduleDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Middleware Schedule</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsSchedules" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">

<asp:UpdatePanel ID="udpJobs" runat="server" >
<ContentTemplate>
    <div class="formRow">
                <asp:Label ID="lblSystemidEnter" CssClass="formMargin labelInput" runat="server" Text="System:" Width="100px"/>
                <asp:DropDownList ID="ddlSystemid" CssClass="formMargin" runat="server" DataTextField="ShortName" DataValueField="id" ToolTip="Select the System for a Job that you wish to configure a schedule for" OnSelectedIndexChanged="GetJobNames" AutoPostBack="true" Width="250"/>
            <asp:RequiredFieldValidator ID="rfvSystemid" runat="server" ControlToValidate="ddlSystemid" ErrorMessage="System required">
            *
            </asp:RequiredFieldValidator>
        </div>

        <div class="formRow">
                <asp:Label ID="lblJobidEnter" CssClass="formMargin labelInput" runat="server" Text="Job:" Width="100px"/>
                 <asp:DropDownList ID="ddlJobid" CssClass="formMargin" runat="server" 
                 DataTextField="ShortName" DataValueField="id" ToolTip="Job to be Scheduled" 
                 Width="250">
                 </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvJobid" runat="server"  ControlToValidate="ddlJobid" ErrorMessage="There is no Job ID selected">
            *
            </asp:RequiredFieldValidator>
        </div>
 </ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="ddlSystemid" EventName="SelectedIndexChanged" />
</Triggers> 
</asp:UpdatePanel>

    <div class="formRow">
            <asp:Label ID="lblDescriptionEnter" CssClass="formMargin labelInput" runat="server" Text="Description:" Width="100px"/>
            <asp:TextBox ID="txtDescription" CssClass="formMargin" runat="server" ToolTip="Detail on what the job does" TextMode="MultiLine" Columns="39" Rows="2" />
        <asp:RegularExpressionValidator ID="txtLengthValidator" ControlToValidate="txtDescription" ErrorMessage="Description is exceeding 1024 characters" ValidationExpression="^[\s\S]{0,1024}$" runat="server" >
        *
        </asp:RegularExpressionValidator>
    </div>

    <div class="formRow">    
            <asp:label ID="lblStartDateEnter" CssClass="formMargin labelInput" runat="server" Text="Start Date:" Width="100px"/>
            <asp:TextBox ID="txtStartDate" CssClass="formMargin" runat="server" ToolTip="Date to Start Job" MaxLength="20" Width="250" ClientIDMode="Static"/>
    </div>

    <div class="formRow">
           
            <asp:Label ID="lblStartTimeEnter" CssClass="formMargin labelInput" runat="server" Text="Start Time:" Width="100px"/>
            <asp:TextBox ID="txtStartTime" CssClass="formMargin" runat="server" ToolTip="Time Job starts" MaxLength="50" Width="250"/>

            <cc1:MaskedEditExtender ID="MaskedEditTime" runat="server" TargetControlID="txtStartTime" 
                MaskType="Time" Mask="99:99:99" MessageValidatorTip="true" AcceptNegative="None" 
                InputDirection="RightToLeft" ErrorTooltipEnabled="true" >
            </cc1:MaskedEditExtender>   
            
            <cc1:MaskedEditValidator ID="mevStartTime" runat="server" 
                 ControlToValidate="txtStartTime" ControlExtender="MaskedEditTime" MaximumValueMessage="Please enter a valid start time" MinimumValueMessage="Please enter a valid start time"
                  EmptyValueMessage="Please enter start time" MinimumValue="00:00:" MaximumValue="23:59:59" IsValidEmpty="false" Display="None">
            </cc1:MaskedEditValidator>
    </div>

    <div class="formRow">
            <asp:label ID="lblEndDateEnter" CssClass="formMargin labelInput" runat="server" Text="End Date:" Width="100px"/>
            <asp:TextBox ID="txtEndDate" CssClass="formMargin" runat="server" ToolTip="Date for end of Job" MaxLength="20" Width="250" ClientIDMode="Static"/>
   </div>

     <div class="formRow">
            <asp:Label ID="lblEndTimeEnter" CssClass="formMargin labelInput" runat="server" Text="End Time:" Width="100px"/>
            <asp:TextBox ID="txtEndTime" CssClass="formMargin" runat="server" ToolTip="Time Job ends" MaxLength="50" Width="250"/>
            <cc1:MaskedEditExtender ID="EndTime" runat="server" TargetControlID="txtEndTime" 
                MaskType="Time" Mask="99:99:99" MessageValidatorTip="true" AcceptNegative="None" 
                InputDirection="RightToLeft" ErrorTooltipEnabled="true">
            </cc1:MaskedEditExtender>         
    </div>

    <div class="formRow">                                                                                                                             
            <asp:Label ID="lblDisabledEnter" CssClass="formMargin labelInput" runat="server" Text="Disabled:" Width="100px"/>
            <asp:checkbox ID="cbxDisable" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Disable this schedule" Checked="false" Height="12px"/>
    </div>



    <div class="formRowNoClear clear"> 
            <asp:Label ID="lblFrequency" CssClass="formMargin labelInput" runat="server" Text="Frequency:" Width="100px"/>
            <asp:TextBox ID="txtFrequency" CssClass="formMargin" runat="server" ToolTip="How often the Job will run" MaxLength="6" Width="200"/>
        <asp:RequiredFieldValidator ID="rfvFrequency" runat="server" ControlToValidate="txtFrequency" ErrorMessage="Please add a value in seconds for how often you want the Job to run">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRowNoClear">
            <asp:DropDownList ID="ddlTime" CssClass="formMargin" runat="server">
            </asp:DropDownList>
    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">

     <div class="formRow">      
            <asp:Label ID="lblMondayEnter" CssClass="formMargin labelInput" runat="server" Text="Monday:" Width="100px" />
            <asp:checkbox ID="cbxMonday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Mondays" Checked="false" Height="12px"/>
    </div>

    <div class="formRow"> 
            <asp:Label ID="lblTuesdayEnter" CssClass="formMargin labelInput" runat="server" Text="Tuesday:" Width="100px" />
            <asp:checkbox ID="cbxTuesday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Tuesdays" Checked="false" Height="12px" />
    </div>

    <div class="formRow"> 
            <asp:Label ID="lblWednesdayEnter" CssClass="formMargin labelInput" runat="server" Text="Wednesday:" Width="100px" />
            <asp:checkbox ID="cbxWednesday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Wednesdays" Checked="false" Height="12px"/>
    </div>

    <div class="formRow"> 
            <asp:Label ID="lblThursdayEnter" CssClass="formMargin labelInput" runat="server" Text="Thursday:" Width="100px" />
            <asp:checkbox ID="cbxThursday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Thursdays" Checked="false" Height="12px"/>
    </div>

    <div class="formRow">
            <asp:Label ID="lblFridayEnter" CssClass="formMargin labelInput" runat="server" Text="Friday:" Width="100px" />
            <asp:checkbox ID="cbxFriday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Fridays" Checked="false" Height="12px"/>
    </div>

    <div class="formRow"> 
            <asp:Label ID="lblSaturdayEnter" CssClass="formMargin labelInput" runat="server" Text="Saturday:" Width="100px" />
            <asp:checkbox ID="cbxSaturday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Saturday" Checked="false" Height="12px"/>
    </div>

    <div class="formRow"> 
            <asp:Label ID="lblSundayEnter" CssClass="formMargin labelInput" runat="server" Text="Sunday:" Width="100px" />
            <asp:checkbox ID="cbxSunday" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Enable Job on Sundays" Checked="false" Height="12px"/>
    </div>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Schedule" EntityURL="Schedules.aspx" ConnectionString="MiddlewareSystem" OptionalCancelName="jobid" deleteEnabled="true" deleteSPName="Schedule" deleteParameter="Schedule"/>

    <script type="text/javascript">
        $(function () {
            $("#txtStartDate").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#txtEndDate").datepicker({ dateFormat: 'yy-mm-dd' });
        });
    </script>
</asp:Content>

  