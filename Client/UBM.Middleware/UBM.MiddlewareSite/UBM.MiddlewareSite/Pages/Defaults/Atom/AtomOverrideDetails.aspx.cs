﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class AtomOverrideDetails : Page
    {
        #region Private Field and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int overrideid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with atom override data
        private void GetOverride()
        {
            GetSystemNames();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_OtisOverride", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", overrideid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtType.Text = sdr["OtisType"].ToString();
                    txtValue.Text = sdr["OtisValue"].ToString();

                    ddlSystemid.SelectedValue = sdr["System_id"] == DBNull.Value ? "ALL" : sdr["System_id"].ToString();
                
                    txtMapped.Text = sdr["MappedValue"].ToString();
                }
            }
        }

        //populates systems 
        private void GetSystemNames()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader sdrSystems = command.ExecuteReader();
                
                ListItem itmSystem = new ListItem("ALL");
                ddlSystemid.Items.Add(itmSystem);

                while (sdrSystems.Read())
                {
                    itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["System_Name"].ToString();

                    ddlSystemid.Items.Add(itmSystem);
                }

            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (overrideid > 0)
                {
                    GetOverride();
                }
                else
                {
                    GetSystemNames();
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("AtomOverride.aspx");
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves atom override to OtisOverride table, either an insert or update is invoked by the stored
        ///   procedure based on whether record exists
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_OtisOverride", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@id", overrideid);
                command.Parameters.AddWithValue("@otisType", txtType.Text);
                command.Parameters.AddWithValue("@otisValue", txtValue.Text);
                command.Parameters.AddWithValue("@mappedValue", txtMapped.Text);

                //if all selected, insert null, otherwise insert value
                if (ddlSystemid.SelectedValue != "ALL")
                {
                    command.Parameters.AddWithValue("@systemid", ddlSystemid.SelectedValue);
                    
                }
                else
                {
                    command.Parameters.AddWithValue("@systemid", DBNull.Value);
                }

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}