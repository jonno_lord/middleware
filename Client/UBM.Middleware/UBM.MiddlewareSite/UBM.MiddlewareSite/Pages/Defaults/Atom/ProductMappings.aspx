﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.ProductMappings" Codebehind="ProductMappings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Product Mappings</h1>
<div class="break"></div>
<div class="toggleFilter"><asp:image ID="imgFilter" runat="server" imageUrl="~/Images/filter_icon.png" cssClass="filterButton" ToolTip="Filter Product Mappings"/></div>
<div class="IgnoreLabel innerFilter">
    <div class="IgnoreText">
    <div class="formRowNoClear">
    Show mapped products<asp:RadioButton ID="rdoMapped" runat="server" CssClass="IgnoreCheckbox" Checked="true" GroupName="productMappings" AutoPostBack="true" oncheckedchanged="rdoIgnore_CheckedChanged"/>
    Show unmapped products<asp:RadioButton ID="rdoUnmapped" runat="server" CssClass="IgnoreCheckbox" GroupName="productMappings" AutoPostBack="true"  oncheckedchanged="rdoIgnore_CheckedChanged"/>
    </div>
    <asp:UpdatePanel ID="updCheckBoxFilters" runat="server" class="formRowNoClear">
        <ContentTemplate>
            Show ignored mappings <asp:CheckBox CssClass="IgnoreCheckbox" ID="cbxIgnore" AutoPostBack="true" runat="server" oncheckedchanged="cbxIgnore_CheckedChanged" />
            Show selected mappings <asp:CheckBox CssClass="IgnoreCheckbox" ID="cbxSelect" AutoPostBack="true" runat="server" oncheckedchanged="cbxSelect_CheckedChanged" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="cbxIgnore" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="cbxSelect" EventName="CheckedChanged" />
        </Triggers>
    </asp:UpdatePanel>
  </div>  
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div id="outerFilter" class="outerFilter queryFormRow">
<asp:UpdatePanel ID="updPublication" runat="server" >

     <ContentTemplate>
        <div class="break"></div>
        <asp:Label ID="lblFilter" runat="server" Text="Filters:" Width="40" CssClass="innerFilter"/>
        <asp:TextBox ID="txtSystem" runat="server" Width="85" CssClass="innerFilter"/>
        <asp:TextBox ID="txtProductCode" runat="server" Width="85" CssClass="innerFilter"/>
        <asp:TextBox ID="txtProduct" runat="server" Width="390" CssClass="innerFilter"/>
        <asp:TextBox ID="txtBlueSheepCode" runat="server" Width="130" CssClass="innerFilter"/>
        <asp:imagebutton ID="btnSearch" runat="server" imageUrl="~/Images/search_icon.png" tooltip="Filter Mappings" cssClass="filterSubmit" onclick="btnSearch_Click"/>
        <asp:imagebutton ID="btnClear" runat="server" imageUrl="~/Images/clear_icon.png" tooltip="Clear Filters" cssClass="filterSubmit" onclick="btnClear_Click" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
    </Triggers>
</asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updProductMappings" runat="server" >
    <ContentTemplate>

     <asp:UpdateProgress ID="uppProductMappings" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updProductMappings">
        <ProgressTemplate>
                <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:GridView ID="dgrProductMappings" runat="server" cssClass="formRowNoClear" AutoGenerateColumns="False" DataKeyNames="id, SystemName, ProductCode" CellPadding="4" AllowPaging="True" OnRowCommand="dgrProductMappings_RowCommand" OnRowDataBound="dgrProductMappings_RowDataBound" onpageindexchanging="dgrProductMappings_PageIndexChanging" PageSize="10" AllowSorting="false">
        <emptydatatemplate>
            No Product Mapping data found.
        </emptydatatemplate>
        <Columns>        

            <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="10px" ItemStyle-Width="30px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" ImageUrl="~/images/AddIconsmall.png" ToolTip="Edit Mapping"  runat="server" CommandArgument='<%# Eval("id") %>' CommandName="edit"/>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:HyperlinkField DataTextField="SystemName" HeaderText="System"  ItemStyle-Width="80px"
                DataNavigateUrlFields="id" ItemStyle-CssClass="GridCentre" DataNavigateUrlFormatString="ProductMappingsDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
            <asp:HyperlinkField DataTextField="ProductCode" HeaderText="Code" ItemStyle-Width="80px"
                DataNavigateUrlFields="id" ItemStyle-CssClass="GridCentre" DataNavigateUrlFormatString="ProductMappingsDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
            <asp:HyperlinkField HeaderText="Product" ItemStyle-Width="380px"
                DataTextField="Product" DataNavigateUrlFields="id" DataNavigateUrlFormatString="ProductMappingsDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
            <asp:HyperlinkField HeaderText="BlueSheep Code" ItemStyle-Width="120px"
                 DataNavigateUrlFields="id"  ItemStyle-CssClass="GridCentre" DataTextField="BlueSheepCode" DataNavigateUrlFormatString="ProductMappingsDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
            <asp:HyperlinkField HeaderText="BlueSheep Prospect Code" ItemStyle-Width="165px"
                 DataNavigateUrlFields="id"  ItemStyle-CssClass="GridCentre" DataTextField="bs_prospect_cd" DataNavigateUrlFormatString="ProductMappingsDetails.aspx?id={0}" HeaderStyle-CssClass="GridCentre"/>
            <asp:TemplateField HeaderText="Selected" ItemStyle-CssClass="GridCentre">
                <ItemTemplate>
                    <asp:CheckBox ID="cbxSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbxSelectedProduct_CheckedChanged"/>
                </ItemTemplate>
            </asp:TemplateField>
            </Columns>
        <PagerStyle CssClass="pageIndex" />
    </asp:GridView>
    </ContentTemplate>
     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>

