﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" CodeBehind="Systems.aspx.cs" Inherits="UBM.MiddlewareSite.Pages.Defaults.Atom.Systems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <h1>Systems</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" runat="server">
<asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptSystems_ItemCommand">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Description</th>
                        <th>System Name</th>
                        <th>Product Code</th>
                        <th>Prospect Code</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit System " +Eval("System_Name")%>'/></td>
                      <td><%# Eval("Description") %></td>
                      <td><%# Eval("System_Name") %></td>
                      <td><%# Eval("bs_prod_cd")%></td>
                      <td><%# Eval("bs_prospect_cd")%></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
</asp:Content>
