﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" CodeBehind="SystemDetails.aspx.cs" Inherits="UBM.MiddlewareSite.Pages.Defaults.Atom.SystemDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <h1>System - <asp:Label ID="lblSystemName" runat="server"/></h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" runat="server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblProductCode" runat="server" cssClass="formMargin labelInput boldLabel" Text="Product Code:" Width="120px"/>
           <asp:TextBox ID="txtProductCode" runat="server" cssClass="formMargin" ToolTip="Blue Sheep Product Code" Width="250"/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" runat="server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblProspectCode" runat="server" cssClass="formMargin labelInput boldLabel" Text="Prospect Code:" Width="120px"/>
           <asp:TextBox ID="txtProspectCode" runat="server" cssClass="formMargin" ToolTip="Blue Sheep Prospect Code" Width="250"/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" runat="server">
<div class="buttonAlignRight">
      <asp:LinkButton ID="lnkSubmit" CssClass="glassbutton glassbuttonedit" runat="server" OnClick="btnSaveEntity_Click"><asp:Image ID="imgSubmit" runat="server" ImageUrl="../../../Images/EditIcon.png"/><p><asp:Literal ID="litActionName" runat="server" Text="Save System"></asp:Literal></p></asp:LinkButton>
</div>
</asp:Content>
