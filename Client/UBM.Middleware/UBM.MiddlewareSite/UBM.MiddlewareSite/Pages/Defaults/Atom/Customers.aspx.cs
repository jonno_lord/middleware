﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Defaults.Atom
{
    public partial class Customers : Page
    {
        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSearch_click(object sender, EventArgs e)
        {
        }

        //clear filters and reload grid
        protected void btnFilter_click(object sender, EventArgs e)
        {
            txtBatchNo.Text = "";
            txtSystem.Text = "";
            txtSourceURN.Text = "";
            txtName.Text = "";
            txtSurname.Text = "";
            txtCompany.Text = "";
            txtAddress.Text = "";
            txtPostcode.Text = "";
            txtProduct.Text = "";

        }

        //links to details page with id of job
        protected void dgrCustomers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    //links to details page, including the record's id in the url
                    Response.Redirect("CustomerDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }
        #endregion
    }
}