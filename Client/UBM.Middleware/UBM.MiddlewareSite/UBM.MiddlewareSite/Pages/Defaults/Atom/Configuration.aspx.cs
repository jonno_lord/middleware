﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class Configuration : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }
        #endregion

        #region Private Methods

        private void GetConfiguration()
        {

            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Configuration", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtLastRunDate.Text = sdr["LastRunDate"].ToString();
                    txtTime.Text = sdr["LastRunTime"].ToString();
                    txtWaitingFilePath.Text = sdr["WaitingFilePath"].ToString();
                    txtProcessedFilePath.Text = sdr["ProcessedFilePath"].ToString();
                    txtServerName.Text = sdr["FtpServer"].ToString();
                    txtUserName.Text = sdr["FtpUser"].ToString();
                    txtPassword.Text = sdr["FtpPassword"].ToString();
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetConfiguration();
            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_UPD_Configuration", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@lastRunDate", txtLastRunDate.Text + " " + txtTime.Text);
                command.Parameters.AddWithValue("@waitingFilePath", txtWaitingFilePath.Text);
                command.Parameters.AddWithValue("@processedFilePath", txtProcessedFilePath.Text);
                command.Parameters.AddWithValue("@serverName", txtServerName.Text);
                command.Parameters.AddWithValue("@userName", txtUserName.Text);
                command.Parameters.AddWithValue("@password", txtPassword.Text);
                command.ExecuteNonQuery();

            }
            Response.Redirect("/");
        }

        
        #endregion
    }
}