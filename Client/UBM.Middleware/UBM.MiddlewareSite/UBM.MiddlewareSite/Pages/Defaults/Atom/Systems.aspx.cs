﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace UBM.MiddlewareSite.Pages.Defaults.Atom
{
    public partial class Systems : Page
    {
        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        private void LoadSystems()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSystems();
            }
        }

        //links to details page with id of system
        protected void rptSystems_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("SystemDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }
    }
}