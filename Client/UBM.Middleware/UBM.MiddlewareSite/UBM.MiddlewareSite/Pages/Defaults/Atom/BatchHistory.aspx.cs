﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class BatchHistory : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_Batches", connection);
                command.CommandType = CommandType.StoredProcedure;

                // convert the amount in the batch number textbox into an integer
                int batchNumber;
                int.TryParse(txtBatchNumber.Text, out batchNumber);

                command.Parameters.AddWithValue("@batchNumber", batchNumber);

                // convert date and time into a single datetime field (note uses system culture and local)
                DateTime filterDate;
                if (DateTime.TryParse(txtDate.Text + " " + txtTime.Text, out filterDate))
                    command.Parameters.AddWithValue("@date", filterDate);
                else
                    command.Parameters.AddWithValue("@date", null);

                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet ds = new DataSet();

                sda.Fill(ds);
                dgrBatches.DataSource = ds.Tables[0].DefaultView;

                dgrBatches.DataBind();
            }
        }
        protected void dgrBatches_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            const string ImageTrueURL = "/Images/AlertGreen.png";
            const string ImageFalseURL = "/Images/AlertWhite.png";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("usp_SEL_Batches", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    string id = DataBinder.Eval(e.Row.DataItem, "id").ToString();
                    command.Parameters.AddWithValue("batchNumber", id);
                    SqlDataReader sdr = command.ExecuteReader();

                    Image imgExtract = (Image)e.Row.FindControl("imgExtract");
                    Image imgTranslate = (Image)e.Row.FindControl("imgTranslate");
                    Image imgLoad = (Image)e.Row.FindControl("imgLoad");

                    if (sdr.Read())
                    {
                        imgExtract.ImageUrl = sdr["ExtractionDate"] != DBNull.Value ? ImageTrueURL : ImageFalseURL;
                        imgExtract.ToolTip = sdr["ExtractionDate"] != DBNull.Value ? "<br/><center>Extracted on " + sdr["ExtractionDate"] + "</center>" : "<br/><center>Not Extracted</center>";

                        imgTranslate.ImageUrl = sdr["TranslationDate"] != DBNull.Value ? ImageTrueURL : ImageFalseURL;
                        imgTranslate.ToolTip = sdr["TranslationDate"] != DBNull.Value ? "<br/><center>Translated on " + sdr["TranslationDate"] + "</center>" : "<br/><center>Not Translated</center>";

                        imgLoad.ImageUrl = sdr["SentDate"] != DBNull.Value ? ImageTrueURL : ImageFalseURL;
                        imgLoad.ToolTip = sdr["SentDate"] != DBNull.Value ? "<br/><center>Sent to BlueSheep on " + sdr["SentDate"] + "</center>" : "<br/><center>Not Sent to BlueSheep</center>";

                    }
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }

        protected void cbxSelected_Checked(object sender, EventArgs e)
        {
            using (connection = new SqlConnection(connectionString))
            {
                CheckBox cbxSelect = (CheckBox)sender;
                GridViewRow row = (GridViewRow)cbxSelect.NamingContainer;

                if (dgrBatches != null)
                {
                    int id = Convert.ToInt32(dgrBatches.DataKeys[row.RowIndex].Value);

                    connection.Open();
                    SqlCommand command = new SqlCommand("usp_UPD_Batch", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@batchNumber", id);
                    command.Parameters.AddWithValue("@selected", cbxSelect.Checked);

                    command.ExecuteReader();
                }
            }

        }

        //apply filters
        protected void btnSearch_click(object sender, EventArgs e)
        {
            LoadGrid();
        }

        //clear filters and reload grid
        protected void btnFilter_click(object sender, EventArgs e)
        {
            txtBatchNumber.Text = "";
            txtDate.Text = "";
            txtTime.Text = "";

            LoadGrid();
        }

        protected void dgrBatches_PageIndexChanged(object sender, EventArgs e)
        {
        }

        protected void dgrBatches_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrBatches.PageIndex = e.NewPageIndex;
            LoadGrid();

        }

        #endregion
    }
}