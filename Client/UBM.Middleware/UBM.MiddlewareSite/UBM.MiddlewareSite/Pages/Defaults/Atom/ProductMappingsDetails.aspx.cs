﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class ProductMappingDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int id
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with job data
        private void GetMappings()
        {
            GetSystemNames();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_ProductMapping", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", id);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlSystems.SelectedValue = sdr["SystemName"].ToString();
                    txtProduct.Text = sdr["Product"].ToString();
                    txtProductCode.Text = sdr["ProductCode"].ToString();
                    txtBlueSheepCode.Text = "" + sdr["BlueSheepCode"];
                    txtBlueSheepProspect.Text = "" + sdr["bs_prospect_cd"];

                    if (sdr["ignore"] != DBNull.Value)
                    {
                        cbxIgnore.Checked = Convert.ToBoolean(sdr["ignore"]);
                    }
                }
            }
        }

        private void GetSystemNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();
                while (sdr.Read())
                {
                    ListItem itmSystemName = new ListItem();
                    itmSystemName.Value = sdr["System_Name"].ToString();
                    itmSystemName.Text = sdr["Description"].ToString();

                    ddlSystems.Items.Add(itmSystemName);
                }
            }

        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                GetMappings();

                if (Session["mappings"] != null)
                {
                    string mapped = Session["mappings"].ToString();

                    //disable delete if unmapped
                    CommonButtons.deleteEnabled = mapped != "unmapped";
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("ProductMappings.aspx");
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Updates customer types  and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SET_ProductMapping", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@systemName", ddlSystems.SelectedValue);
                command.Parameters.AddWithValue("@ProductCode", txtProductCode.Text);
                command.Parameters.AddWithValue("@BlueSheepCode", txtBlueSheepCode.Text);
                command.Parameters.AddWithValue("@BlueSheepProspect", txtBlueSheepProspect.Text);
                command.Parameters.AddWithValue("@ignore", cbxIgnore.Checked);

                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}