﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class ProductMappings : Page
    {
        #region Private Fields and Methods

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //links to details page with id of job
        protected void dgrProductMappings_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("ProductMappingsDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }

        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                Session["mappings"] = "mapped";

                string storedProcedureName = StoredProcedureToUse();

                SqlCommand command = new SqlCommand(storedProcedureName, connection);
                command.CommandType = CommandType.StoredProcedure;

                if (cbxIgnore.Checked)
                {
                    cbxSelect.Checked = false;
                    cbxSelect.Enabled = false;
                    command.Parameters.AddWithValue("@ignore", true);
                }

                command.Parameters.AddWithValue("@SystemName", txtSystem.Text);
                command.Parameters.AddWithValue("@ProductCode", txtProductCode.Text);
                command.Parameters.AddWithValue("@Product", txtProduct.Text);
                command.Parameters.AddWithValue("@BlueSheepCode", txtBlueSheepCode.Text);

                SqlDataAdapter sda = new SqlDataAdapter(command);
                
                DataSet ds = new DataSet();
                sda.Fill(ds);

                dgrProductMappings.DataSource = ds.Tables[0].DefaultView;
                dgrProductMappings.DataBind();

            }
        }

        private string StoredProcedureToUse()
        {
            string storedProcedureName = "";

            //shows mapped or unmapped products
            if (rdoMapped.Checked)
            {
                cbxIgnore.Enabled = true;
                cbxSelect.Enabled = true;

                if (cbxSelect.Checked)
                {
                    storedProcedureName = "usp_SEL_SelectedProductMappings";
                }
                else
                {
                    storedProcedureName = "usp_SEL_ProductMappings";
                }
            }
            else if (rdoUnmapped.Checked)
            {
                storedProcedureName = "usp_SEL_UnknownProductMappings";

                if (cbxIgnore.Checked)
                {
                    cbxIgnore.Checked = false;
                }

                if (cbxSelect.Checked)
                {
                    cbxSelect.Checked = false;
                }

                Session["mappings"] = "unmapped";
                cbxIgnore.Enabled = false;
                cbxSelect.Enabled = false;
            }

            return storedProcedureName;
        }

        #endregion

        #region Protected Fields and Methods

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger productTrigger = new AsyncPostBackTrigger();

            productTrigger.ControlID = cbxIgnore.UniqueID;

            productTrigger.EventName = "CheckedChanged";

            updProductMappings.Triggers.Add(productTrigger);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string unmapped = Request.QueryString["unmapped"];
                if (!string.IsNullOrEmpty(unmapped))
                    cbxIgnore.Checked = true;

                LoadGrid();
            }
        }

        #region selected checkbox functionality

        protected void dgrProductMappings_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (rdoUnmapped.Checked)
            {
                dgrProductMappings.Columns[6].Visible = false;

            }
            else
            {
                dgrProductMappings.Columns[6].Visible = true;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView row = (DataRowView)e.Row.DataItem;

                    CheckBox cbox = e.Row.FindControl("cbxSelected") as CheckBox;

                    //sets id as an attribute of checkbox
                    if (cbox != null)
                    {
                        if (row != null)
                        {
                            cbox.Attributes.Add("blueSheepCode", row["BlueSheepCode"].ToString());
                            cbox.Attributes.Add("blueSheepProspect", row["bs_prospect_cd"].ToString());
                        }

                        if (DataBinder.Eval(e.Row.DataItem, "selected") != DBNull.Value)
                        {
                            cbox.Checked = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "selected"));
                        }

                        cbox.ToolTip = cbox.Checked == false ? "Select Product" : "Unselect Product";
                    }
                }
            }
            
        }

        protected void cbxSelectedProduct_CheckedChanged(object sender, EventArgs e)
        {
            using (connection = new SqlConnection(connectionString))
            {

                //gets id of row checked
                CheckBox checkBox = sender as CheckBox;
                if (checkBox != null)
                {
                    string productid = checkBox.Attributes["blueSheepCode"];
                    string prospectCode = checkBox.Attributes["blueSheepProspect"];

                    // gets value of checkbox checked property
                    CheckBox cbox = checkBox.FindControl("cbxSelected") as CheckBox;

                    if (cbox != null)
                    {
                        bool cbxSelected = cbox.Checked;

                        connection.Open();
                        SqlCommand command = new SqlCommand("usp_UPD_ProductMapping", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@blueSheepCode", productid);
                        command.Parameters.AddWithValue("@blueSheepProspect", prospectCode);
                        command.Parameters.AddWithValue("@selected", cbxSelected);

                        command.ExecuteNonQuery();

                        LoadGrid();
                    }

                    if (cbox != null) cbox.ToolTip = cbox.Checked == false ? "Select Product" : "Unselect Product";
                }
            }
        }

        #endregion

        protected void dgrProductMappings_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            LoadGrid();
            dgrProductMappings.PageIndex = e.NewPageIndex;
            dgrProductMappings.DataBind();
        }

        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            LoadGrid();
        }
        protected void btnClear_Click(object sender, ImageClickEventArgs e)
        {
            txtBlueSheepCode.Text = "";
            txtProduct.Text = "";
            txtProductCode.Text = "";
            txtSystem.Text = "";
            LoadGrid();
        }

        #region filter events

        protected void rdoIgnore_CheckedChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }

        protected void cbxIgnore_CheckedChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }

        protected void cbxSelect_CheckedChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }

        #endregion

        #endregion
    }
}