﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.BatchHistory" Codebehind="BatchHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Batch History</h1>
<div class="toggleFilter"><asp:image ID="imgFilter" runat="server" imageUrl="~/Images/filter_icon.png" alt="filter" cssClass="filterButton" ToolTip="Filter Batch History"/></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div id="outerFilter" class="outerFilter queryFormRow">
    <asp:UpdatePanel ID="updBatchFilter" runat="server" >
         <ContentTemplate>
            <asp:TextBox ID="txtBatchNumber" runat="server" Width="80" CssClass="innerFilter" ToolTip="Batch Number"/>
            <asp:TextBox ID="txtDate" runat="server"  ClientIDMode="Static" Width="155" CssClass="innerFilter" ToolTip="Date"/>
            <asp:TextBox ID="txtTime" runat="server" Width="120" CssClass="innerFilter" ToolTip="Time 00:00:00"/>
            <asp:imagebutton ID="btnSearch" runat="server" imageUrl="~/Images/search_icon.png" alt="Go" tooltip="Filter Mappings" cssClass="filterSubmit" onClick="btnSearch_click"/>
            <asp:imagebutton ID="btnClear" runat="server" imageUrl="~/Images/clear_icon.png" alt="Clear" tooltip="Clear Filters" cssClass="filterSubmit" onClick="btnFilter_click"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updBatches" runat="server">

    <ContentTemplate>
            <asp:UpdateProgress ID="uppBatches" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updBatches">
                <ProgressTemplate>
                     <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
                </ProgressTemplate>
            </asp:UpdateProgress>

    <asp:GridView ID="dgrBatches" runat="server" cssClass="break" AutoGenerateColumns="False" DataKeyNames="id" EnableModelValidation="True" CellPadding="4" onrowdatabound="dgrBatches_RowDataBound"
               AllowPaging="True" onpageindexchanged="dgrBatches_PageIndexChanged" onpageindexchanging="dgrBatches_PageIndexChanging" PageSize="30">
                <emptydatatemplate>
                    No Batch History data found based on filters applied
                </emptydatatemplate>
                <Columns>        
                   <asp:BoundField DataField="id" HeaderText="Batch Number" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="RunDate" HeaderText="Filter Date" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="150px"/>
                   <asp:TemplateField HeaderText="E" ItemStyle-Width="20px" HeaderStyle-CssClass="GridCentre">
                       <ItemTemplate>
                           <asp:Image ID="imgExtract" runat="server" width="20px"  CssClass="StatusTooltip tooltipCentre" />
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="T" ItemStyle-Width="20px" HeaderStyle-CssClass="GridCentre">
                       <ItemTemplate>
                           <asp:Image ID="imgTranslate" runat="server" width="20px" CssClass="StatusTooltip tooltipCentre"/>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="L" ItemStyle-Width="20px" HeaderStyle-CssClass="GridCentre">
                       <ItemTemplate>
                           <asp:Image ID="imgLoad" runat="server" width="20px" CssClass="StatusTooltip tooltipCentre"/>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Customers" HeaderStyle-CssClass="GridCentre" ItemStyle-Width="240px">
                        <ItemTemplate>
                            <span class="DownloadAtomFile"><%# Eval("DATRows")%> Customers &nbsp;&nbsp; <a href="<%# Eval("DATFilePath")%>" title="<%# "Download " + Eval("DATFileName")%>">Download</a></span>
                        </ItemTemplate>
                   </asp:TemplateField>

                   <asp:TemplateField HeaderText="Orders" HeaderStyle-CssClass="GridCentre" ItemStyle-Width="240px">
                        <ItemTemplate>
                            <span class="DownloadAtomFile"><%# Eval("TRARows")%> Orders &nbsp;&nbsp; <a href="<%# Eval("TRAFilePath")%>" title="<%# "Download " + Eval("TRAFileName")%>">Download</a></span>
                        </ItemTemplate>
                   </asp:TemplateField>
                      <asp:TemplateField HeaderText="Resend" ItemStyle-Width="20px" ItemStyle-CssClass="GridCentre">
                       <ItemTemplate>
                           <asp:CheckBox id="cbxSelected" runat="server" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"Selected"))%>' ToolTip='<%# "Set to extract again for batch number: " +Eval("id")%>' OnCheckedChanged="cbxSelected_Checked" AutoPostBack="true"/>
                       </ItemTemplate>
               </asp:TemplateField>

                </Columns>
                <PagerStyle CssClass="pageIndex" />
            </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
    </Triggers>
</asp:UpdatePanel>

<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>

<script type="text/javascript">
    $(function () {
        $("#txtDate").datepicker({ dateFormat: 'yy-mm-dd' });
    });
</script>
</asp:Content>

