﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.Orders" Codebehind="Orders.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Orders</h1>
<div class="toggleFilter"><asp:image ID="imgFilter" runat="server" imageUrl="~/Images/filter_icon.png" alt="filter" cssClass="filterButton" ToolTip="Filter Orders"/></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div id="outerFilter" class="outerFilter queryFormRow">
    <asp:UpdatePanel ID="updOrdersFilter" runat="server" >
         <ContentTemplate>
            <asp:Label ID="lblFilter" runat="server" Text="Filters:" Width="50" CssClass="innerFilter"/>
            <asp:TextBox ID="txtBatchNo" runat="server" Width="60" CssClass="innerFilter" ToolTip="Batch Number"/>
            <asp:TextBox ID="txtSystem" runat="server" Width="60" CssClass="innerFilter" ToolTip="System Name"/>
            <asp:TextBox ID="txtSourceURN" runat="server"  ClientIDMode="Static" Width="84" CssClass="innerFilter" ToolTip="Source URN"/>
            <asp:TextBox ID="txtOrderNo" runat="server" Width="65" CssClass="innerFilter" ToolTip="Order Number"/>
            <asp:TextBox ID="txtLineItemNo" runat="server" Width="85" CssClass="innerFilter" ToolTip="Line Item Number"/>
            <asp:TextBox ID="txtTransType" runat="server" Width="105" CssClass="innerFilter" ToolTip="Transaction Type"/>
            <asp:TextBox ID="txtTransValue" runat="server" Width="100" CssClass="innerFilter" ToolTip="Transaction Value"/>
            <asp:TextBox ID="txtProduct" runat="server" Width="82" CssClass="innerFilter" ToolTip="Product"/>
            <asp:TextBox ID="txtDate" runat="server" Width="100" ClientIDMode="Static" CssClass="innerFilter" ToolTip="Execution Date"/>
            <asp:imagebutton ID="btnSearch" runat="server" imageUrl="~/Images/search_icon.png" alt="Go" tooltip="Filter Mappings" cssClass="filterSubmit" onClick="btnSearch_click"/>
            <asp:imagebutton ID="btnClear" runat="server" imageUrl="~/Images/clear_icon.png" alt="Clear" tooltip="Clear Filters" cssClass="filterSubmit" onClick="btnFilter_click"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updOrders" runat="server">

    <ContentTemplate>
    
     <asp:UpdateProgress ID="uppOrders" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updOrders">
        <ProgressTemplate>
                <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:GridView ID="dgrOrders" runat="server" cssClass="break" AutoGenerateColumns="False" DataKeyNames="id" EnableModelValidation="True" CellPadding="4" OnRowCommand="dgrOrders_RowCommand"
               AllowPaging="True" ViewStateMode="Enabled" DataSourceID="odsorders">
                <emptydatatemplate>
                    No Order data
                </emptydatatemplate>
                <Columns>
                  <asp:TemplateField HeaderText="Details" HeaderStyle-Width="8px" ItemStyle-Width="30px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDetails" ImageUrl="~/images/AddIconsmall.png" ToolTip="Viewn full Order details"  runat="server" CommandArgument='<%# Eval("id") %>' CommandName="edit"/>
                        </ItemTemplate>
                   </asp:TemplateField>
                   <asp:BoundField DataField="Batch_Number" HeaderText="Batch Number" ItemStyle-CssClass="GridCentre" ItemStyle-Width="54" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="System_Name" HeaderText="System" ItemStyle-CssClass="GridCentre" ItemStyle-Width="54" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="Source_URN" HeaderText="Source URN" ItemStyle-CssClass="GridCentre" ItemStyle-Width="76" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="Order_No" HeaderText="Order" ItemStyle-CssClass="GridCentre" ItemStyle-Width="65" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="Line_Item_No" HeaderText="Line Item Number" ItemStyle-CssClass="GridCentre" ItemStyle-Width="75" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="Trans_Type" HeaderText="Transaction Type" ItemStyle-CssClass="GridCentre" ItemStyle-Width="96" HeaderStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="TransValue" HeaderText="Transaction Value" ItemStyle-Width="85" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="Product_cd" HeaderText="Product" ItemStyle-Width="80" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre"/>
                   <asp:BoundField DataField="Extraction_Date" HeaderText="Extraction Date" ItemStyle-Width="140" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre"/>
                </Columns>
                <PagerStyle CssClass="pageIndex" />
            </asp:GridView>
             <asp:ObjectDataSource ID="odsOrders" runat="server" EnablePaging="true" TypeName="WebsiteFunctions.CustomPaging" SelectMethod="getOrders" SelectCountMethod="getTotalOrders" StartRowIndexParameterName="startRowIndex" MaximumRowsParameterName="maximumRows">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBatchNo" Name="batchNo" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtSystem" Name="system" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtSourceURN" Name="sourceURN" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtOrderNo" Name="orderNo" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtLineItemNo" Name="lineItemNo" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtTransType" Name="transType" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtTransValue" Name="transValue" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtProduct" Name="product" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtDate" Name="date" PropertyName="Text" Type="string" />
                </SelectParameters>
            </asp:ObjectDataSource>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
    </Triggers>
</asp:UpdatePanel>

<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>

<script type="text/javascript">

    $(function () {
        $("#txtDate").datepicker({ dateFormat: 'yy-mm-dd' });
    });
</script>
</asp:Content>

