﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace UBM.MiddlewareSite.Pages.Defaults.Atom
{
    public partial class SystemDetails : System.Web.UI.Page
    {
        #region private fields and properties
        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        private int systemid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region private methods

        private void LoadSystem()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_System", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@systemid", systemid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    lblSystemName.Text = sdr["System_Name"].ToString();
                    txtProductCode.Text = sdr["bs_prod_cd"].ToString();
                    txtProspectCode.Text = sdr["bs_prospect_cd"].ToString();
                }
            }
        }

        #endregion

        #region protected methods and events

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadSystem();
            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_UPD_System", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@systemid", systemid);
                command.Parameters.AddWithValue("@productCode", txtProductCode.Text);
                command.Parameters.AddWithValue("@prospectCode", txtProspectCode.Text);
                command.ExecuteNonQuery();

            }
            Response.Redirect("Systems.aspx");
        }

        #endregion
    }
}