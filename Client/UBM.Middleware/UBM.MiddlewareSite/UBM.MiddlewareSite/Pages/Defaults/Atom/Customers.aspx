﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.Customers" Codebehind="Customers.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Customers</h1>
<div class="toggleFilter"><asp:image ID="imgFilter" runat="server" imageUrl="~/Images/filter_icon.png" alt="filter" cssClass="filterButton" ToolTip="Filter Customers"/></div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="mainContents" Runat="Server">
<div id="outerFilter" class="outerFilter queryFormRow">
    <asp:UpdatePanel ID="updCustomersFilter" runat="server" >
         <ContentTemplate>
            <asp:Label ID="lblFilter" runat="server" Text="Filters:" Width="50" CssClass="innerFilter"/>
            <asp:TextBox ID="txtBatchNo" runat="server" Width="60" CssClass="innerFilter" ToolTip="Batch Number"/>
            <asp:TextBox ID="txtSystem" runat="server" Width="60" CssClass="innerFilter" ToolTip="System Name"/>
            <asp:TextBox ID="txtSourceURN" runat="server" Width="62" CssClass="innerFilter" ToolTip="Source URN"/>
            <asp:TextBox ID="txtName" runat="server" Width="75" CssClass="innerFilter" ToolTip="Customer Name"/>
            <asp:TextBox ID="txtSurname" runat="server" Width="80" CssClass="innerFilter" ToolTip="Customer Surname"/>
            <asp:TextBox ID="txtCompany" runat="server" Width="110" CssClass="innerFilter" ToolTip="Customer Company"/>
            <asp:TextBox ID="txtProduct" runat="server" Width="78" CssClass="innerFilter" ToolTip="Product"/> 
            <asp:TextBox ID="txtAddress" runat="server" Width="110" CssClass="innerFilter" ToolTip="First Line of Address"/>
            <asp:TextBox ID="txtPostcode" runat="server" Width="68" CssClass="innerFilter" ToolTip="Postcode"/>
            <asp:imagebutton ID="btnSearch" runat="server" imageUrl="~/Images/search_icon.png" alt="Go" tooltip="Filter Mappings" cssClass="filterSubmit" onClick="btnSearch_click"/>
            <asp:imagebutton ID="btnClear" runat="server" imageUrl="~/Images/clear_icon.png" alt="Clear" tooltip="Clear Filters" cssClass="filterSubmit" onClick="btnFilter_click"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updCustomers" runat="server">

    <ContentTemplate>

     <asp:UpdateProgress ID="uppCustomers" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updCustomers">
        <ProgressTemplate>
                <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/>Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:GridView ID="dgrCustomers" runat="server" cssClass="break" AutoGenerateColumns="False" DataKeyNames="id" EnableModelValidation="True" CellPadding="4" OnRowCommand="dgrCustomers_RowCommand" AllowPaging="True" ViewStateMode="Enabled" DataSourceID="odsCustomers">
                <emptydatatemplate>
                    No Customer data
                </emptydatatemplate>
                <Columns>
                   
                   <asp:TemplateField HeaderText="Details" HeaderStyle-Width="8px" ItemStyle-Width="30px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDetails" ImageUrl="~/images/AddIconsmall.png" ToolTip="View full Customer details"  runat="server" CommandArgument='<%# Eval("id") %>' CommandName="edit"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:BoundField DataField="Batch_Number" HeaderText="Batch Number" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="56"/>
                   <asp:BoundField DataField="System_Name" HeaderText="System" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="56" />
                   <asp:BoundField DataField="Source_URN" HeaderText="Source URN" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="56" />
                   <asp:BoundField DataField="ForeName" HeaderText="Name" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="71" />
                   <asp:BoundField DataField="Surname" HeaderText="Surname" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="71" />
                   <asp:BoundField DataField="Company" HeaderText="Company" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="101" />
                   <asp:BoundField DataField="Product_cd" HeaderText="Product" HeaderStyle-CssClass="GridCentre" ItemStyle-CssClass="GridCentre" ItemStyle-Width="71"/>
                   <asp:BoundField DataField="Address1" HeaderText="Address" HeaderStyle-CssClass="GridCentre" ItemStyle-Width="101" />
                   <asp:BoundField DataField="Postcode" HeaderText="Postcode" ItemStyle-CssClass="GridCentre" ItemStyle-Width="56" />
                   <asp:BoundField DataField="Extraction_date" HeaderText="Last Extracted" HeaderStyle-CssClass="GridCentre"/>
                </Columns>
                <PagerStyle CssClass="pageIndex" />
            </asp:GridView>
            <asp:ObjectDataSource ID="odsCustomers" runat="server" EnablePaging="true" TypeName="WebsiteFunctions.CustomPaging" SelectMethod="getCustomers" SelectCountMethod="getTotalCustomers" StartRowIndexParameterName="startRowIndex" MaximumRowsParameterName="maximumRows">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBatchNo" Name="batchNo" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtSystem" Name="system" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtSourceURN" Name="sourceURN" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtName" Name="name" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtSurname" Name="surname" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtCompany" Name="company" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtProduct" Name="product" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtAddress" Name="address" PropertyName="Text" Type="string" />
                    <asp:ControlParameter ControlID="txtPostcode" Name="postcode" PropertyName="Text" Type="string" />
                </SelectParameters>
            </asp:ObjectDataSource>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>

