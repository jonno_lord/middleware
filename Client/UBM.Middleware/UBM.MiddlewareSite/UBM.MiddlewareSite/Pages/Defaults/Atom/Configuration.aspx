﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.Configuration" ValidateRequest="false" Codebehind="Configuration.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Configuration</h1>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsConfiguration" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">

        <div class="formRow">
            <asp:Label ID="lblLastRunDate" runat="server" cssClass="formMargin labelInput" Text="Last Run Date:" Width="100px"/>
            <asp:TextBox ID="txtLastRunDate" runat="server" CssClass="formMargin" ClientIDMode="Static" ToolTip="Last Run Date" Width="200px" />
            <asp:TextBox ID="txtTime" runat="server" CssClass="formMargin" ToolTip="Last Run Time" Width="60px" />
        <asp:RequiredFieldValidator ID="rfvLastRunDate" runat="server" ControlToValidate="txtLastRunDate" ErrorMessage="Last Run Date Required">
        *
        </asp:RequiredFieldValidator>

            <cc1:MaskedEditExtender ID="MaskedEditTime" runat="server" TargetControlID="txtTime" 
            MaskType="Time" Mask="99:99:99" MessageValidatorTip="true" AcceptNegative="None" 
            InputDirection="RightToLeft" ErrorTooltipEnabled="true" >
            </cc1:MaskedEditExtender>   
            
            <cc1:MaskedEditValidator ID="mevStartTime" runat="server" 
            ControlToValidate="txtTime" ControlExtender="MaskedEditTime" MaximumValueMessage="Please enter a valid time" MinimumValueMessage="Please enter a valid time"
            EmptyValueMessage="Please enter a time" MinimumValue="00:00:00" MaximumValue="23:59:59" IsValidEmpty="false" Display="None">
            </cc1:MaskedEditValidator>
        </div>

        <div class="formRow">
            <asp:Label ID="lblWaitingFilePath" runat="server" cssClass="formMargin labelInput" Text="Waiting File Path:" Width="100px"/>
            <asp:TextBox ID="txtWaitingFilePath" runat="server" cssClass="formMargin" ToolTip="Waiting File Path" Width="285"/>        
            <asp:RequiredFieldValidator ID="rfvWaitingFilePath" runat="server" ControlToValidate="txtWaitingFilePath" ErrorMessage="Waiting File Path Required">
            *
            </asp:RequiredFieldValidator>
        </div>
        
            <div class="formRow">
               <asp:Label ID="lblProcessedFilePath" runat="server" cssClass="formMargin labelInput" Text="Processed File Path:" Width="100px"/>
               <asp:TextBox ID="txtProcessedFilePath" runat="server" cssClass="formMargin" ToolTip="Processed File Path" Width="250"/>
            <asp:RequiredFieldValidator ID="rfvProcessedFilePath" runat="server" ControlToValidate="txtProcessedFilePath" ErrorMessage="Processed File Path Required">
            *
            </asp:RequiredFieldValidator>
        </div> 

    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="rightForm" Runat="Server">
     <div class="addEditForm">

            <div class="formRow">
               <asp:Label ID="lblServerName" runat="server" cssClass="formMargin labelInput" Text="FTP Server Name:" Width="100px"/>
               <asp:TextBox ID="txtServerName" runat="server" cssClass="formMargin" ToolTip="Server Name for FTP" Width="250"/>
            <asp:RequiredFieldValidator ID="rfvServerName" runat="server" ControlToValidate="txtServerName" ErrorMessage="FTP Server Name Required">
            *
            </asp:RequiredFieldValidator>
        </div> 

      <div class="formRow">
            <asp:Label ID="lblUserName" runat="server" cssClass="formMargin labelInput" Text="FTP User Name:" Width="100px"/>
            <asp:TextBox ID="txtUserName" runat="server" cssClass="formMargin" ToolTip="User Name for FTP" MaxLength="50" Width="250" Text=""/>        
         <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="FTP Server Name Required">
          *
         </asp:RequiredFieldValidator>
      </div>

        <div class="formRow">
            <asp:Label ID="lblPassword" runat="server" cssClass="formMargin labelInput" Text="FTP Password:" Width="100px"/>
            <asp:TextBox ID="txtPassword" runat="server" cssClass="formMargin" ToolTip="Password for FTP" MaxLength="50" Width="250" Text=""/>        
         <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="FTP Password Required">
          *
         </asp:RequiredFieldValidator>
      </div>
        
    </div>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
    <div class="buttonAlignRight">
      <asp:LinkButton ID="lnkSubmit" CssClass="glassbutton glassbuttonedit" runat="server" OnClick="btnSaveEntity_Click"><asp:Image ID="imgSubmit" runat="server" ImageUrl="../../../Images/EditIcon.png"/><p><asp:Literal ID="litActionName" runat="server" Text="Save Configuration"></asp:Literal></p></asp:LinkButton>
</div>
    <script type="text/javascript">
        $(function () {
            $("#txtLastRunDate").datepicker({ dateFormat: 'yy-mm-dd' });
        });
    </script>
</asp:Content>

