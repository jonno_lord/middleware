﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class CustomerDetails : Page
    {
        #region Private fields and methods

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int customerid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with job data
        private void GetCustomer()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Customers", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", customerid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtBatchNumber.Text = sdr["Batch_Number"].ToString();
                    txtSystemName.Text = sdr["System_Name"].ToString();
                    txtExtractionDate.Text = sdr["Extraction_date"].ToString();
                    txtSourceURN.Text = sdr["Source_URN"].ToString();
                    txtBSURN.Text = sdr["BS_URN"].ToString();
                    txtCampaignCd.Text = sdr["Campaign_cd"].ToString();
                    txtSourceCd.Text = sdr["Source_cd"].ToString();
                    txtTitle.Text = sdr["Title"].ToString();
                    txtInitials.Text = sdr["Initials"].ToString();
                    txtForname.Text = sdr["Forename"].ToString();
                    txtSurname.Text = sdr["Surname"].ToString();
                    txtJobTitle.Text = sdr["Job_Title"].ToString();
                    txtDept.Text = sdr["Dept"].ToString();
                    txtCompany.Text = sdr["Company"].ToString();
                    txtAddress1.Text = sdr["Address1"].ToString();
                    txtAddress2.Text = sdr["Address2"].ToString();
                    txtAddress3.Text = sdr["Address3"].ToString();
                    txtTown.Text = sdr["Town"].ToString();
                    txtCounty.Text = sdr["County"].ToString();
                    txtPostcode.Text = sdr["Postcode"].ToString();
                    txtCountry.Text = sdr["country_name"].ToString();
                    txtTel.Text = sdr["Tel"].ToString();
                    txtFax.Text = sdr["Fax"].ToString();
                    txtWebsite.Text = sdr["Website"].ToString();
                    txtMobile.Text = sdr["Mobile"].ToString();
                    txtEmail.Text = sdr["Email"].ToString();
                    txtParentCompany.Text = sdr["Parent_Company"].ToString();
                    txtLanguage.Text = sdr["Language"].ToString();
                    txtStatus.Text = sdr["Status"].ToString();
                    txtDateAttended.Text = sdr["Date_Attended"].ToString();
                    txtLastActDate.Text = sdr["Last_act_dt"].ToString();
                    txtRegCd.Text = sdr["Reg_cd"].ToString();
                    txtUsage.Text = sdr["Usage"].ToString();
                    txtExpiryDate.Text = sdr["Expiry_dt"].ToString();
                    txtListName.Text = sdr["Listname"].ToString();
                    txtProductCd.Text = sdr["Description"].ToString();
                    txtSupplier.Text = sdr["Supplier"].ToString();
                    txtTimestamp.Text = sdr["Timestamp"].ToString();
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (customerid == 0)
                {
                    Response.Redirect("Customers.aspx");
                }
                else
                {
                    GetCustomer();
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Customers.aspx");
        }

        #endregion
    }
}