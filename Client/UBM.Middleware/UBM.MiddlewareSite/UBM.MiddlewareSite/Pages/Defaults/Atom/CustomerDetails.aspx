﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.CustomerDetails" Codebehind="CustomerDetails.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Customers</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblBatchNumber" runat="server" cssClass="formMargin labelInput boldLabel" Text="Batch Number:" Width="100px"/>
           <asp:Label ID="txtBatchNumber" runat="server" cssClass="formMargin" ToolTip="Batch Number of Customer" Width="250"/>
    </div>

    <div class="formRow">
           <asp:Label ID="lblSystemName" runat="server" cssClass="formMargin labelInput boldLabel" Text="Name:" Width="100px"/>
           <asp:Label ID="txtSystemName" runat="server" cssClass="formMargin" ToolTip="Name of System" Width="250"/>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblExtractionDate" runat="server" cssClass="formMargin labelInput boldLabel" Text="Extraction Date:" Width="100px"/>
           <asp:Label ID="txtExtractionDate" runat="server" cssClass="formMargin" ToolTip="Extraction Date of Customer" Width="250"/>
     </div>
    
    <div class="formRow"> 
           <asp:Label ID="lblSourceURN" runat="server" cssClass="formMargin labelInput boldLabel" Text="Source URN:" Width="100px"/>
            <asp:Label ID="txtSourceURN" runat="server" cssClass="formMargin" ToolTip="Source Unique Reference Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblBSURN" runat="server" cssClass="formMargin labelInput boldLabel" Text="Blue Sheep URN:" Width="100px"/>
        <asp:Label ID="txtBSURN" runat="server" cssClass="formMargin" ToolTip="Blue Sheep Unique Reference Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblCampaignCd" runat="server" cssClass="formMargin labelInput boldLabel" Text="Campaign Code:" Width="100px"/>
        <asp:Label ID="txtCampaignCd" runat="server" cssClass="formMargin" ToolTip="Campaign Code" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblSourceCd" runat="server" cssClass="formMargin labelInput boldLabel" Text="Source Code:" Width="100px"/>
        <asp:Label ID="txtSourceCd" runat="server" cssClass="formMargin" ToolTip="Source Code" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTitle" runat="server" cssClass="formMargin labelInput boldLabel" Text="Title:" Width="100px"/>
        <asp:Label ID="txtTitle" runat="server" cssClass="formMargin" ToolTip="Customer's Title" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblInitials" runat="server" cssClass="formMargin labelInput boldLabel" Text="Initials:" Width="100px"/>
        <asp:Label ID="txtInitials" runat="server" cssClass="formMargin" ToolTip="Customer's Initials" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblForeName" runat="server" cssClass="formMargin labelInput boldLabel" Text="Forename:" Width="100px"/>
        <asp:Label ID="txtForname" runat="server" cssClass="formMargin" ToolTip="Customer's Forename" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblSurname" runat="server" cssClass="formMargin labelInput boldLabel" Text="Surname:" Width="100px"/>
        <asp:Label ID="txtSurname" runat="server" cssClass="formMargin" ToolTip="Customer's Surname" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblJobTitle" runat="server" cssClass="formMargin labelInput boldLabel" Text="Job Title:" Width="100px"/>
        <asp:Label ID="txtJobTitle" runat="server" cssClass="formMargin" ToolTip="Customer's Job Title" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblDept" runat="server" cssClass="formMargin labelInput boldLabel" Text="Department:" Width="100px"/>
        <asp:Label ID="txtDept" runat="server" cssClass="formMargin" ToolTip="Customer's Department" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblCompany" runat="server" cssClass="formMargin labelInput boldLabel" Text="Company:" Width="100px"/>
        <asp:Label ID="txtCompany" runat="server" cssClass="formMargin" ToolTip="Customer's Company" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblAddress1" runat="server" cssClass="formMargin labelInput boldLabel" Text="Address 1:" Width="100px"/>
        <asp:Label ID="txtAddress1" runat="server" cssClass="formMargin" ToolTip="Customer's Address Line 1" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblAddress2" runat="server" cssClass="formMargin labelInput boldLabel" Text="Address 2:" Width="100px"/>
        <asp:Label ID="txtAddress2" runat="server" cssClass="formMargin" ToolTip="Customer's Address Line 2" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblAddress3" runat="server" cssClass="formMargin labelInput boldLabel" Text="Address 3:" Width="100px"/>
        <asp:Label ID="txtAddress3" runat="server" cssClass="formMargin" ToolTip="Customer's Address Line 3" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTown" runat="server" cssClass="formMargin labelInput boldLabel" Text="Town:" Width="100px"/>
        <asp:Label ID="txtTown" runat="server" cssClass="formMargin" ToolTip="Customer's Town" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblCounty" runat="server" cssClass="formMargin labelInput boldLabel" Text="County:" Width="100px"/>
        <asp:Label ID="txtCounty" runat="server" cssClass="formMargin" ToolTip="Customer's County" Width="250"/>         
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">
    <div class="formRow"> 
        <asp:Label ID="lblPostcode" runat="server" cssClass="formMargin labelInput boldLabel" Text="Postcode:" Width="100px"/>
        <asp:Label ID="txtPostcode" runat="server" cssClass="formMargin" ToolTip="Customer's Postcode" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblCountry" runat="server" cssClass="formMargin labelInput boldLabel" Text="Country:" Width="100px"/>
        <asp:Label ID="txtCountry" runat="server" cssClass="formMargin" ToolTip="Customer's Country" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTel" runat="server" cssClass="formMargin labelInput boldLabel" Text="Telephone:" Width="100px"/>
        <asp:Label ID="txtTel" runat="server" cssClass="formMargin" ToolTip="Customer's Telephone Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblFax" runat="server" cssClass="formMargin labelInput boldLabel" Text="Fax:" Width="100px"/>
        <asp:Label ID="txtFax" runat="server" cssClass="formMargin" ToolTip="Customer's Fax" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblWebsite" runat="server" cssClass="formMargin labelInput boldLabel" Text="Website:" Width="100px"/>
        <asp:Label ID="txtWebsite" runat="server" cssClass="formMargin" ToolTip="Customer's Website" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblMobile" runat="server" cssClass="formMargin labelInput boldLabel" Text="Mobile:" Width="100px"/>
        <asp:Label ID="txtMobile" runat="server" cssClass="formMargin" ToolTip="Customer's Mobile Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblEmail" runat="server" cssClass="formMargin labelInput boldLabel" Text="Email:" Width="100px"/>
        <asp:Label ID="txtEmail" runat="server" cssClass="formMargin" ToolTip="Customer's Email" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblParentCompany" runat="server" cssClass="formMargin labelInput boldLabel" Text="Parent Company:" Width="100px"/>
        <asp:Label ID="txtParentCompany" runat="server" cssClass="formMargin" ToolTip="Parent Company" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblLanguage" runat="server" cssClass="formMargin labelInput boldLabel" Text="Language:" Width="100px"/>
        <asp:Label ID="txtLanguage" runat="server" cssClass="formMargin" ToolTip="Customer's Language" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblStatus" runat="server" cssClass="formMargin labelInput boldLabel" Text="Status:" Width="100px"/>
        <asp:Label ID="txtStatus" runat="server" cssClass="formMargin" ToolTip="Status" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblDateAttended" runat="server" cssClass="formMargin labelInput boldLabel" Text="Date Attended:" Width="100px"/>
        <asp:Label ID="txtDateAttended" runat="server" cssClass="formMargin" ToolTip="Date Attended" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblLastActDate" runat="server" cssClass="formMargin labelInput boldLabel" Text="Last Act Date:" Width="100px"/>
        <asp:Label ID="txtLastActDate" runat="server" cssClass="formMargin" ToolTip="Last Act Date" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblRegCd" runat="server" cssClass="formMargin labelInput boldLabel" Text="Reg cd:" Width="100px"/>
        <asp:Label ID="txtRegCd" runat="server" cssClass="formMargin" ToolTip="Reg cd" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblUsage" runat="server" cssClass="formMargin labelInput boldLabel" Text="Usage:" Width="100px"/>
        <asp:Label ID="txtUsage" runat="server" cssClass="formMargin" ToolTip="Usage" Width="250"/>         
    </div>
    
    <div class="formRow"> 
        <asp:Label ID="lblExpiryDate" runat="server" cssClass="formMargin labelInput boldLabel" Text="Expiry Date:" Width="100px"/>
        <asp:Label ID="txtExpiryDate" runat="server" cssClass="formMargin" ToolTip="Expiry Date" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblListName" runat="server" cssClass="formMargin labelInput boldLabel" Text="Listname:" Width="100px"/>
        <asp:Label ID="txtListName" runat="server" cssClass="formMargin" ToolTip="Last Act Date" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblProductCd" runat="server" cssClass="formMargin labelInput boldLabel" Text="Product:" Width="100px"/>
        <asp:Label ID="txtProductCd" runat="server" cssClass="formMargin" ToolTip='<%# "Product Code" + Eval("Product_cd") %>' Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblSupplier" runat="server" cssClass="formMargin labelInput boldLabel" Text="Supplier:" Width="100px"/>
        <asp:Label ID="txtSupplier" runat="server" cssClass="formMargin" ToolTip="Supplier" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTimestamp" runat="server" cssClass="formMargin labelInput boldLabel" Text="Timestamp:" Width="100px"/>
        <asp:Label ID="txtTimestamp" runat="server" cssClass="formMargin" ToolTip="Timestamp" Width="250"/>         
    </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
<div class="buttonAlignRight">
      <asp:LinkButton ID="lnkCancel" CssClass="glassbutton glassbuttonedit" runat="server" OnClick="btnCancel_Click"><asp:Image ID="imgCancel" runat="server" ImageUrl="../../../Images/backicon.png"/><p><asp:Literal ID="litActionName" runat="server" Text="Back to Customers"></asp:Literal></p></asp:LinkButton>
</div>
</asp:Content>

