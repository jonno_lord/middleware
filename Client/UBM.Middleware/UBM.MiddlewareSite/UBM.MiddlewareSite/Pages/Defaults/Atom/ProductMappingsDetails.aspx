﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.ProductMappingDetails" Codebehind="ProductMappingsDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Product Mapping</h1>
</div>
<div class="validationSummary">
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblSystemname" runat="server" cssClass="formMargin labelInput" Text="System Name:" Width="100px"/>
           <asp:DropDownList ID="ddlSystems" runat="server" cssClass="formMargin" DataTextField="SystemName" DataValueField="SystemName" ToolTip="System Names" Width="250" Enabled="false"/>
    </div>   

     <div class="formRow"> 
           <asp:Label ID="lblProductCode" runat="server" cssClass="formMargin labelInput" Text="Product Code:" Width="100px"/>
           <asp:TextBox ID="txtProductCode" cssClass="formMargin" runat="Server" ToolTip="Product Code" Width="250" Enabled="False"/>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblProduct" runat="server" cssClass="formMargin labelInput" Text="Product:" Width="100px"/>
           <asp:TextBox ID="txtProduct" cssClass="formMargin" runat="Server" ToolTip="Product" Width="250" Enabled="False"/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblBlueSheepCode" runat="server" cssClass="formMargin labelInput" Text="Blue Sheep Code:" Width="100px"/>
           <asp:TextBox ID="txtBlueSheepCode" runat="server" cssClass="formMargin" 
               ToolTip="Tax Company" Width="250"/>
    </div>   

        <div class="formRow">
           <asp:Label ID="lblBlueSheepProspect" runat="server" cssClass="formMargin labelInput" Text="Blue Sheep Prospect Code:" Width="100px"/>
           <asp:TextBox ID="txtBlueSheepProspect" runat="server" cssClass="formMargin" 
               ToolTip="BlueSheep Prospect Code" Width="250"/>
    </div> 

    <div class="formRow">
           <asp:Label ID="lblIgnore" runat="server" cssClass="formMargin labelInput" Text="Ignore:" Width="100px"/>
           <asp:Checkbox ID="cbxIgnore" runat="server" cssClass="formMargin checkboxLayout" ToolTip="Ignore Product" Checked="false"/>
    </div>   

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Mapping" EntityURL="ProductMappings.aspx" ConnectionString="OTISControl" deleteEnabled="true" deleteSPName="ProductMapping" deleteParameter=""/>
</asp:Content>

