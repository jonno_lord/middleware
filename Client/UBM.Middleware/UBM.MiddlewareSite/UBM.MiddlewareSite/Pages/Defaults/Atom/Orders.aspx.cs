﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Defaults.Atom
{
    public partial class Orders : Page
    {
        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSearch_click(object sender, EventArgs e)
        {
        }

        //clear filters and reload grid
        protected void btnFilter_click(object sender, EventArgs e)
        {
            txtBatchNo.Text = "";
            txtSystem.Text = "";
            txtSourceURN.Text = "";
            txtOrderNo.Text = "";
            txtLineItemNo.Text = "";
            txtTransType.Text = "";
            txtTransValue.Text = "";
            txtProduct.Text = "";
            txtDate.Text = "";

        }

        //links to details page with id of job
        protected void dgrOrders_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    //links to details page, including the record's id in the url
                    Response.Redirect("OrderDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }
        #endregion
    }
}