﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.OrderDetails" Codebehind="OrderDetails.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Orders</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblBatchNumber" runat="server" cssClass="formMargin labelInput boldLabel" Text="Batch Number:" Width="100px"/>
           <asp:Label ID="txtBatchNumber" runat="server" cssClass="formMargin" ToolTip="Batch Number of Order" Width="250"/>
    </div>

    <div class="formRow">
           <asp:Label ID="lblSystemName" runat="server" cssClass="formMargin labelInput boldLabel" Text="Name:" Width="100px"/>
           <asp:Label ID="txtSystemName" runat="server" cssClass="formMargin" ToolTip="Name of System" Width="250"/>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblExtractionDate" runat="server" cssClass="formMargin labelInput boldLabel" Text="Extraction Date:" Width="100px"/>
           <asp:Label ID="txtExtractionDate" runat="server" cssClass="formMargin" ToolTip="Extraction Date of Order" Width="250"/>
     </div>
    
    <div class="formRow"> 
           <asp:Label ID="lblSourceURN" runat="server" cssClass="formMargin labelInput boldLabel" Text="Source URN:" Width="100px"/>
            <asp:Label ID="txtSourceURN" runat="server" cssClass="formMargin" ToolTip="Source Unique Reference Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblBSURN" runat="server" cssClass="formMargin labelInput boldLabel" Text="Blue Sheep URN:" Width="100px"/>
        <asp:Label ID="txtBSURN" runat="server" cssClass="formMargin" ToolTip="Blue Sheep Unique Reference Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblOrderNo" runat="server" cssClass="formMargin labelInput boldLabel" Text="Order Number:" Width="100px"/>
        <asp:Label ID="txtOrderNo" runat="server" cssClass="formMargin" ToolTip="Order Number" Width="250"/>         
    </div>

</div>


</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

        <div class="formRow"> 
        <asp:Label ID="lblLineItemNo" runat="server" cssClass="formMargin labelInput boldLabel" Text="Line Item Number:" Width="100px"/>
        <asp:Label ID="txtLineItemNo" runat="server" cssClass="formMargin" ToolTip="Line Item Number" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTransType" runat="server" cssClass="formMargin labelInput boldLabel" Text="Transaction Type:" Width="100px"/>
        <asp:Label ID="txtTransType" runat="server" cssClass="formMargin" ToolTip="Type of Transaction" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTransValue" runat="server" cssClass="formMargin labelInput boldLabel" Text="Transaction Value:" Width="100px"/>
        <asp:Label ID="txtTransValue" runat="server" cssClass="formMargin" ToolTip="Value of Transaction" Width="250"/>         
    </div>
    
    <div class="formRow"> 
        <asp:Label ID="lblProductCd" runat="server" cssClass="formMargin labelInput boldLabel" Text="Product:" Width="100px"/>
        <asp:Label ID="txtProductCd" runat="server" cssClass="formMargin" ToolTip='<%# "Product Code" + Eval("Product_cd") %>' Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblSupplier" runat="server" cssClass="formMargin labelInput boldLabel" Text="Supplier:" Width="100px"/>
        <asp:Label ID="txtSupplier" runat="server" cssClass="formMargin" ToolTip="Supplier" Width="250"/>         
    </div>

    <div class="formRow"> 
        <asp:Label ID="lblTimestamp" runat="server" cssClass="formMargin labelInput boldLabel" Text="Timestamp:" Width="100px"/>
        <asp:Label ID="txtTimestamp" runat="server" cssClass="formMargin" ToolTip="Timestamp" Width="250"/>         
    </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
 <div class="buttonAlignRight">
      <asp:LinkButton ID="lnkCancel" CssClass="glassbutton glassbuttonedit" runat="server" OnClick="btnCancel_Click"><asp:Image ID="imgCancel" runat="server" ImageUrl="../../../Images/backicon.png"/><p><asp:Literal ID="litActionName" runat="server" Text="Back to Orders"></asp:Literal></p></asp:LinkButton>
</div>
</asp:Content>

