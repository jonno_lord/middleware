﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.Atom.AtomOverrideDetails" Codebehind="AtomOverrideDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Atom Overrides</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblType" runat="server" cssClass="formMargin labelInput" Text="Type:" Width="100px"/>
           <asp:textbox ID="txtType" runat="server" cssClass="formMargin" ToolTip="Atom Transaction Type" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvType" runat="server"  ControlToValidate="txtType" ErrorMessage="Atom Type required">
        *
        </asp:RequiredFieldValidator>
    </div>

        <div class="formRow">
           <asp:Label ID="lblValue" runat="server" cssClass="formMargin labelInput" Text="Value:" Width="100px"/>
           <asp:textbox ID="txtValue" runat="server" cssClass="formMargin" ToolTip="Atom Transaction Value" Width="250" />
        <asp:RequiredFieldValidator ID="rfvValue" runat="server"  ControlToValidate="txtValue" ErrorMessage="Atom Value required">
        *
        </asp:RequiredFieldValidator>
    </div>
    
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

        <div class="formRow">
           <asp:Label ID="lblSystem" runat="server" cssClass="formMargin labelInput" Text="System:" Width="100px"/>
           <asp:dropdownlist ID="ddlSystemid" runat="server" cssClass="formMargin" DataTextField="System_Name" DataValueField="id" ToolTip="System mapping applies to" Width="250"/>
        </div>

         <div class="formRow">
           <asp:Label ID="lblMapped" runat="server" cssClass="formMargin labelInput" Text="Mapped Value:" Width="100px"/>
           <asp:textbox ID="txtMapped" runat="server" cssClass="formMargin" ToolTip="Mapped Value" Width="250" />
        </div>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Override" EntityURL="AtomOverride.aspx" ConnectionString="OtisControl" deleteEnabled="true" deleteSPName="OtisOverride" deleteParameter=""/>
</asp:Content>

