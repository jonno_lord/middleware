﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.Atom
{
    public partial class OrderDetails : Page
    {
        #region Private fields and methods

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string returnString = RemoveProviders.RemoveProvider(WebConfigurationManager.ConnectionStrings["OTISControlConnectionString"].ConnectionString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int orderid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with job data
        private void GetOrder()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Orders", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", orderid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtBatchNumber.Text = sdr["Batch_Number"].ToString();
                    txtSystemName.Text = sdr["System_Name"].ToString();
                    txtExtractionDate.Text = sdr["Extraction_date"].ToString();
                    txtSourceURN.Text = sdr["Source_URN"].ToString();
                    txtBSURN.Text = sdr["BS_URN"].ToString();
                    txtOrderNo.Text = sdr["Order_No"].ToString();
                    txtLineItemNo.Text = sdr["Line_Item_No"].ToString();
                    txtTransType.Text = sdr["Trans_Type"].ToString();
                    txtTransValue.Text = sdr["TransValue"].ToString();
                    txtProductCd.Text = sdr["Description"].ToString();
                    txtSupplier.Text = sdr["Supplier"].ToString();
                    txtTimestamp.Text = sdr["Timestamp"].ToString();
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (orderid == 0)
                {
                    Response.Redirect("Orders.aspx");
                }
                else
                {
                    GetOrder();
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Orders.aspx");
        }

        #endregion
    }
}