﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults
{
    public partial class CustomerScrutinyRules : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private bool filter;

        private bool storedProcExists;

        private static string systemString
        {
            get
            {
                string returnString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                return returnString;
            }
        }

        private string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings[ddlFilterSystems.SelectedItem.Text + "ConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //checks if page is filtered by dropdowns
        private bool isFiltered()
        {
            filter = ddlFilterSystems.SelectedIndex != 0;

            return filter;
        }

        //fix to ensure rule can be saved to correct system on add
        private void SetSystem()
        {
            Session["scrutinySystem"] = ddlFilterSystems.SelectedItem.Text;
        }

        private void LoadGrid()
        {
            try
            {
              
                if (storedProcedureExists("usp_SEL_CustomerScrutinyRules"))
                {
                    lblMessage.Text = "";

                    using (connection = new SqlConnection(connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("usp_SEL_CustomerScrutinyRules", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        rptGrid.DataSource = command.ExecuteReader();
                        rptGrid.DataBind();
                    }

                    SetSystem();
                }
                else
                {
                    lblMessage.Text = "No Scrutiny Rule data for this system";
                    lblMessage.ForeColor = Color.Red;

                }

            }
            catch
            {
                lblMessage.Text = "There is no valid connection string available for the chosen system";
                lblMessage.ForeColor = Color.Red;
            }
        }

        private void GetSystemNames()
        {

            using (connection = new SqlConnection(systemString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                while (sdrSystems.Read())
                {
                    ListItem itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    if (Request.QueryString["id"] != null)
                    {
                        if (itmSystem.Value == Request.QueryString["id"])
                        {
                            itmSystem.Selected = true;
                        }
                    }

                    //filter on enable disable
                    if (itmSystem.Text == Request.QueryString["filter"])
                    {
                        itmSystem.Selected = true;
                    }

                    ddlFilterSystems.Items.Add(itmSystem);
                }
            }
        }

        // This function prevent the page being retrieved from broswer cache
        private void ExpirePageCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now - new TimeSpan(1, 0, 0));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
        }

        private void buttonChecker()
        {
            try
            {
              
                if (storedProcedureExists("usp_SEL_CustomerScrutinyRules"))
                {

                    using (connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                    }

                    CommonActions.disableEnable = "true";
                }
                else
                {
                    CommonActions.disableEnable = "false";
                }
            }
            catch
            {
                CommonActions.disableEnable = "false";
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger statusTrigger = new AsyncPostBackTrigger();

            statusTrigger.ControlID = ddlFilterSystems.UniqueID;

            statusTrigger.EventName = "SelectedIndexChanged";

            updScrutinyRules.Triggers.Add(statusTrigger);

            AsyncPostBackTrigger labelTrigger = new AsyncPostBackTrigger();

            labelTrigger.ControlID = ddlFilterSystems.UniqueID;

            labelTrigger.EventName = "SelectedIndexChanged";

            updLabel.Triggers.Add(labelTrigger);

            AsyncPostBackTrigger buttonTrigger = new AsyncPostBackTrigger();

            buttonTrigger.ControlID = ddlFilterSystems.UniqueID;

            buttonTrigger.EventName = "SelectedIndexChanged";

            updSummary.Triggers.Add(buttonTrigger);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ExpirePageCache();

            if (!IsPostBack)
            {
                GetSystemNames();

                LoadGrid();

                //if page is filtered keep summary on
                if (isFiltered())
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: summaryDisplayNoAnimation(); ", true);
                }
            }

            //sets connectionstring for enable/disable
            CommonActions.ConnectionString = ddlFilterSystems.SelectedItem.Text;

            //checks whether buttons should be disabled or not
            buttonChecker();
        }

        /// <summary>
        ///   Checks whether stored procedure exists in selected system database, if so then there are Customer scrutiny rules to be viewed
        /// </summary>
        /// <param name = "storedProcedure"></param>
        /// <returns>bool that states whether stpred procedure exists</returns>
        protected bool storedProcedureExists(string storedProcedure)
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='" + storedProcedure + "'", connection);
                SqlDataReader sdrSpExists = command.ExecuteReader();

                if (sdrSpExists.Read())
                    
                    storedProcExists = true;

                return storedProcExists;
            }
        }

        protected void ddlFilterSystemSelect(object sender, EventArgs e)
        {
            //clears out grid of data on postback
            rptGrid.DataSource = null;
            rptGrid.DataBind();

            LoadGrid();
            SetSystem();

        }

        //links to details page with id of job
        protected void rptScrutinyRules_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("CustomerScrutinyRuleDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }

        //applies value of id to each checkbox in the repeater and binds database value to controls
        protected void rptScrutinyRules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DbDataRecord row = e.Item.DataItem as DbDataRecord;

                CheckBox cbox = e.Item.FindControl("cbxDisabled") as CheckBox;

                //sets id as an attribute of checkbox
                if (cbox != null)
                {
                    if (row != null) cbox.Attributes.Add("id", row["id"].ToString());

                    cbox.Checked = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Disabled"));

                    cbox.ToolTip = cbox.Checked == false ? "Disable Scrutiny Rule " : "Enable Scrutiny Rule";
                }
            }
        }

        //disables and enables scrutiny rules
        protected void cbxDisableScrutinyRule_Checked(object sender, EventArgs e)
        {

            using (connection = new SqlConnection(connectionString))
            {

                //gets id of row checked
                CheckBox checkBox = sender as CheckBox;
                if (checkBox != null)
                {
                    int id = Convert.ToInt32(checkBox.Attributes["id"]);

                    // gets value of checkbox checked property
                    CheckBox cbox = checkBox.FindControl("cbxDisabled") as CheckBox;
                    if (cbox != null)
                    {
                        bool cbxDisabled = cbox.Checked;

                        connection.Open();

                        //note: a new stored procedure will be needed for each system database
                        SqlCommand command = new SqlCommand("usp_UPD_ScrutinyRule", connection);
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@scrutinyRuleid", id);
                        command.Parameters.AddWithValue("@scrutinyRuleDisabled", cbxDisabled);

                        command.ExecuteReader();
                    }

                    if (cbox != null) cbox.ToolTip = cbox.Checked == false ? "Disable Scrutiny Rule" : "Enable Scrutiny Rule";
                }
            }
        }

        #endregion
    }
}