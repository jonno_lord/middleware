﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Defaults.CustomerScrutinyRules" Codebehind="CustomerScrutinyRules.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
     <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Scrutiny Rule" Title="MW" AddEntityURL="CustomerScrutinyRuleDetails.aspx" QueryStringFilter="id"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
<div class="filter">
        <div class="rowA">
            <p>System:</p>
        </div>
        <div class="rowB">
            <asp:DropDownList id="ddlFilterSystems" runat="server" tooltip="Systems for Customer Scrutiny Rule" onselectedindexchanged="ddlFilterSystemSelect" AutoPostBack="True"/>
        </div>
        <div class="rowC">
         <asp:UpdatePanel ID="updLabel" runat="server" >
            <ContentTemplate>
               <asp:Label ID="lblMessage" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
<asp:UpdatePanel ID="updSummary" runat="server" >
    <ContentTemplate>
        <uc:CommonActions ID="CommonActions" runat="server" EntityName="Scrutiny Rules" TableName="CustomerScrutinyRules" EntityURL="CustomerScrutinyRules.aspx" ConnectionString="" disableEnable="true"/>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>Customer Scrutiny Rules</h1>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="dataTable" Runat="Server">
    <asp:UpdatePanel ID="updScrutinyRules" runat="server" >
        <ContentTemplate>

     <asp:UpdateProgress ID="uppScrutinyRules" runat="server" DisplayAfter="5" AssociatedUpdatePanelID="updScrutinyRules" >
        <ProgressTemplate>
                <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="FixedWidthTable">
            <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptScrutinyRules_ItemCommand" onItemDataBound="rptScrutinyRules_ItemDataBound">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0" >
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Description</th>
                        <th>Returns Violation</th>
                        <th>Returns Duplicates</th>
                        <th>System</th>
                        <th>Query</th>
                        <th>Turn Off</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre">
                      <a href="CustomerScrutinyRuleDetails.aspx?id=<%# Eval("id") + "&systemname=" + ddlFilterSystems.SelectedItem.Text + "&systemid=" + ddlFilterSystems.SelectedValue %>"><asp:Image ImageUrl="~/images/AddIcon.png" runat="server"/></a>
                      <td><%# Eval("Description") %></td>
                      <td class="GridCentre"><%# Eval("ReturnsViolation") %></td>
                      <td class="GridCentre"><%# Eval("ReturnsDuplicates") %></td>
                      <td><%# Eval("ShortName") %></td>
                      <td><%# Eval("Query") %></td>
                      <td class="GridCentre" style="width:20px;">           
                           <asp:CheckBox id="cbxDisabled" runat="server" onCheckedChanged="cbxDisableScrutinyRule_Checked" AutoPostBack="true"/>
                      </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

