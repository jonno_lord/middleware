﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.UBMA.DefaultCreditControllerDetail" Codebehind="DefaultCreditControllerDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>UBMA Default Credit Controllers</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsCreditControllers" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblDefaultCreditController" runat="server" cssClass="formMargin labelInput" Text="Default Credit Controller:" Width="100px"/>
           <asp:TextBox ID="txtDefaultCreditController" runat="server" cssClass="formMargin" ToolTip="Name of the DefaultCreditController" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvDefaultCreditController" runat="server" ControlToValidate="txtDefaultCreditController" ErrorMessage="Please Enter a Name for the DefaultCreditController" >
        *
        </asp:RequiredFieldValidator>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblDefaultCreditManager" runat="server" cssClass="formMargin labelInput" Text="Default Credit Manager:" Width="100px"/>
           <asp:TextBox ID="txtDefaultCreditManager" runat="server" cssClass="formMargin" ToolTip="Name of the Default Credit Manager" MaxLength="50"/>
        <asp:RequiredFieldValidator ID="rfvDefaultCreditManager" runat="server" ControlToValidate="txtDefaultCreditManager" ErrorMessage="Please Enter a DefaultCreditManager" >
        *
        </asp:RequiredFieldValidator>
     </div>

 </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblCompanyNumber" runat="server" cssClass="formMargin labelInput" Text="Company Number:" Width="100px"/>
           <asp:TextBox ID="txtCompanyNumber" runat="server" cssClass="formMargin" ToolTip="Company Number for credit controller" MaxLength="50" Width="250" Enabled="false"/>      
    </div>

    <div class="formRow">
            <asp:Label ID="lblCreditControllerInitials" runat="server" cssClass="formMargin labelInput" Text="Credit Controller Initials:" Width="100px"/>
            <asp:TextBox ID="txtCreditControllerInitials" runat="server" cssClass="formMargin" ToolTip="Initals of Default Credit Controller" MaxLength="3" Width="250"/>      
       <asp:RequiredFieldValidator ID="rfvCreditControllerInitials" runat="server"  ControlToValidate="txtCreditControllerInitials" ErrorMessage="Credit Controller Initials required">
        *
        </asp:RequiredFieldValidator>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Controller" EntityURL="DefaultCreditControllers.aspx" ConnectionString="MiddlewareUBMA" deleteEnabled="true" deleteSPName="DefaultCreditController" deleteParameter="defaultCreditController"/>
</asp:Content>

