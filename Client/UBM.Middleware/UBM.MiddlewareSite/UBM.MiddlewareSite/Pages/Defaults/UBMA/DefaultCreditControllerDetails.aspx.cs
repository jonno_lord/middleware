﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.UBMA
{
    public partial class DefaultCreditControllerDetail : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareUBMAConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private int dccid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetDefaultCreditController()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_DefaultCreditController", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@DefaultCreditControllerid", dccid);


                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtCompanyNumber.Text = sdr["CompanyNumber"].ToString();
                    txtCreditControllerInitials.Text = sdr["CreditControllerInitials"].ToString();
                    txtDefaultCreditController.Text = sdr["DefaultCreditController"].ToString();
                    txtDefaultCreditManager.Text = sdr["DefaultCreditManager"].ToString();

                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow Credit Message to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (dccid != 0)
                {
                    GetDefaultCreditController();
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("DefaultCreditControlers.aspx");
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_DefaultCreditController", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@defaultCreditControllerid", dccid);
                command.Parameters.AddWithValue("@defaultCreditControllerInitials", txtCreditControllerInitials.Text);
                command.Parameters.AddWithValue("@defaultCreditController", txtDefaultCreditController.Text);
                command.Parameters.AddWithValue("@defaultCreditManager", txtDefaultCreditManager.Text);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}