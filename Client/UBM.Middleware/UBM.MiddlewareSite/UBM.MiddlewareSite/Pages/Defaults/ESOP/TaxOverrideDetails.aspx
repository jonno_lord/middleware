﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ESOP.TaxOverrideDetails" Codebehind="TaxOverrideDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">

<div class="buttonAlignLeft">
    <h1>ESOP Tax Override Codes</h1>
</div>
<div class="validationSummary">
    <div class="margin">
        <p><asp:label ID="lblMessage" runat="server" class="formMargin" /></p>
    </div>
    <asp:ValidationSummary ID="vdsTaxOverrides" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">
        <div class="formRow">
            <asp:Label ID="lblCountryCode" runat="server" cssClass="formMargin labelInput" Text="Country Code:" Width="100px"/>
            <asp:TextBox ID="txtCountryCode" runat="server" CssClass="formMargin" ToolTip="Country Code for Business Unit" MaxLength="3" Width="100px" />
        <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtCountryCode" ErrorMessage="Country Code Required">
        *
        </asp:RequiredFieldValidator>
        </div>
    
        <div class="formRow">
               <asp:Label ID="lblBusinessUnit" runat="server" cssClass="formMargin labelInput" Text="Business Unit:" Width="100px"/>
               <asp:DropDownList ID="ddlBusinessUnit" runat="server" cssClass="formMargin" DataTextField="Business_Unit" DataValueField="Business_Unit" ToolTip="Business Unit and Name" Width="300">
               </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvBusinessUnit" runat="server"  ControlToValidate="ddlBusinessUnit" ErrorMessage="Business Unit number required">
            *
            </asp:RequiredFieldValidator>
        </div>

        <div class="formRow">
            <asp:Label ID="lblTaxCode" runat="server" cssClass="formMargin labelInput" Text="Tax Code:" Width="100px"/>
            <asp:TextBox ID="txtTaxCode" runat="server" cssClass="formMargin" ToolTip="Tax Code for Business Unit" MaxLength="10" Width="250"/>        
            <asp:RequiredFieldValidator ID="rfvTaxCode" runat="server" ControlToValidate="txtTaxCode" ErrorMessage="Tax Code Required">
            *
            </asp:RequiredFieldValidator>
        </div>

        <div class="formRow">
               <asp:Label ID="lblTaxOverrideCode" runat="server" cssClass="formMargin labelInput" Text="Tax Override Code:" Width="100px"/>
               <asp:TextBox ID="txtTaxOverrideCode" runat="server" cssClass="formMargin" ToolTip="Tax Override Code for Business Unit" MaxLength="20" Width="250"/>
            <asp:RequiredFieldValidator ID="rfvTaxOverrideCode" runat="server" ControlToValidate="txtTaxOverrideCode" ErrorMessage="Tax Override Code Required">
            *
            </asp:RequiredFieldValidator>
        </div>   
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
    <div class="addEditForm">
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Tax Override" EntityURL="TaxOverride.aspx" deleteEnabled="true" deleteSPName="TaxOverride" deleteParameter="tax"/>
</asp:Content>

