﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Defaults.ESOP.TaxOverride" Codebehind="TaxOverride.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Override Code"  Title="MW ESOP" AddEntityURL="TaxOverrideDetails.aspx" QueryStringFilter="id"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Tax Override" TableName="TaxOverride" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>ESOP Tax Override Codes</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
 <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptTaxOverride_ItemCommand">
    <HeaderTemplate>
        <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
        <thead>
        <tr>
            <th>Edit</th>
            <th>Country Code</th>
            <th>Business Unit</th>
            <th>Business Unit Name</th>
            <th>Tax Code</th>
            <th>Tax Override Code</th>
            <th>Entry Date</th>
        </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("Tax_Override_Id") + "&CountryCode=" + Eval("Country_Code") + "&BusinessUnit=" + Eval("Business_Unit") + "&TaxCode=" + Eval("Tax_Code") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Override Code " +Eval("Tax_Override_Code")%>'/></td>
            <td class="GridCentre"><%# Eval("Country_Code") %></td>
            <td class="GridCentre"><%# Eval("Business_Unit") %></td>
            <td><%# Eval("Description") %></td>
            <td class="GridCentre"><%# Eval("Tax_Code") %></td>
            <td class="GridCentre"><%# Eval("Tax_Override_Code") %></td>
            <td class="GridCentre"><%# Eval("Entry_Date") %></td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </tbody>
        </table>
    </FooterTemplate>
    </asp:Repeater>
</asp:Content>

