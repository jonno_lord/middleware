﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ESOP
{
    public partial class TaxOverrideDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private bool duplicate;

        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareESOPConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int taxid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetTaxOverrideDetails()
        {

            GetBusinessUnitNames();

            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_TaxOverride", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TaxId", taxid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtCountryCode.Text = sdr["Country_Code"].ToString();
                    ddlBusinessUnit.SelectedValue = sdr["BusinessUnit"].ToString();
                    ddlBusinessUnit.Enabled = false;
                    txtTaxCode.Text = sdr["Tax_Code"].ToString();
                    txtTaxOverrideCode.Text = sdr["Tax_Override_Code"].ToString();
                }
            }
        }

        private void GetBusinessUnitNames()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_JDEBusinessUnits", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrBusinessUnit = command.ExecuteReader();
                // gets Business Units and its description via a view in the stored proc
                while (sdrBusinessUnit.Read())
                {
                    ListItem itmBusinessUnit = new ListItem();
                    itmBusinessUnit.Value = sdrBusinessUnit["BusinessUnit"].ToString();
                    itmBusinessUnit.Text = sdrBusinessUnit["BusinessUnit"] + " - " + sdrBusinessUnit["Description"];

                    ddlBusinessUnit.Items.Add(itmBusinessUnit);
                }

            }

        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow rule to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (taxid != 0)
                {
                    GetTaxOverrideDetails();
                }

                GetBusinessUnitNames();
            }
        }

        protected bool CheckForDuplicate()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_TaxOverrideDuplicate", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@CountryCode", txtCountryCode.Text);
                command.Parameters.AddWithValue("@BusinessUnit", ddlBusinessUnit.SelectedValue);
                command.Parameters.AddWithValue("@TaxCode", txtTaxCode.Text);
                command.Parameters.AddWithValue("@taxid", taxid);

                SqlDataReader sdrDuplicate = command.ExecuteReader();

                if (sdrDuplicate.Read())
                    duplicate = true;

                return duplicate;
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            if (CheckForDuplicate())
            {
                lblMessage.Text = "This is a duplicate entry, please correct.";
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                SaveEntity();

                Response.Redirect("TaxOverride.aspx");
            }
          
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        //Saves Tax Override codes to ESOP database via user control Common Buttons
        private void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_TaxOverride", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@TaxId", taxid);
                command.Parameters.AddWithValue("@CountryCode", txtCountryCode.Text);
                command.Parameters.AddWithValue("@BusinessUnit", ddlBusinessUnit.SelectedValue);
                command.Parameters.AddWithValue("@TaxCode", txtTaxCode.Text);
                command.Parameters.AddWithValue("@TaxOverrideCode", txtTaxOverrideCode.Text);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}