﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class ClashingTitleCards : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //loads grid data from database
        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_GET_ClashingTitleCards", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@longName", txtLongName.Text);
                command.Parameters.AddWithValue("@shortName", txtShortName.Text);

                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                sda.Fill(ds);

                dgrClashingTitleCards.DataSource = ds.Tables[0];
                dgrClashingTitleCards.DataBind();

            }
        }

        #endregion

        #region Protected Methods and Events
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }

        //apply filters
        protected void btnSearch_click(object sender, EventArgs e)
        {
            LoadGrid();
        }

        //clear filters and reload grid
        protected void btnFilter_click(object sender, EventArgs e)
        {
            txtLongName.Text = "";
            txtShortName.Text = "";

            LoadGrid();
        }

        protected void dgrClashingTitleCards_PageIndexChanged(object sender, EventArgs e)
        {
        }

        protected void dgrClashingTitleCards_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrClashingTitleCards.PageIndex = e.NewPageIndex;
            LoadGrid();

        }

        #endregion
    }
}