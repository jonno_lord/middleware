﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.PublicationToBusinessUnitDetails" Codebehind="PublicationToBusinessUnitDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <div class="buttonAlignLeft">
     <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsPublication" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblTitleCard" runat="server" cssClass="formMargin labelInput" Text="Title Card:" Width="100px"/>
           <asp:DropDownList ID="ddlTitleCard" runat="server" cssClass="formMargin" 
               DataTextField="Title_Short_Name" DataValueField="Title_Short_Name" 
               ToolTip="Publication Name and Description" Width="250"/>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblBusinessUnit" runat="server" cssClass="formMargin labelInput" Text="Business Unit:" Width="100px"/>
           <asp:DropDownList ID="ddlBusinessUnit" runat="server" cssClass="formMargin" DataTextField="Business_Unit" DataValueField="Business_Unit" ToolTip="Business Unit and Description" Width="250"/>
     </div>
    
    <div class="formRow"> 
           <asp:Label ID="lblCollectionManager" runat="server" cssClass="formMargin labelInput" Text="Collection Client:" Width="100px"/>
           <asp:DropDownList ID="ddlCollectionClient"  runat="server" cssClass="formMargin" DataTextField="LogonName" DataValueField="LogonName" ToolTip="Collection Client" Width="250" />
        <%--<asp:RequiredFieldValidator ID="rfvCollectionManager" runat="server" ControlToValidate="ddlCollectionClient" ErrorMessage="Collection Client is a required" >
        *
        </asp:RequiredFieldValidator>--%>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblCollectionAgent" runat="server" cssClass="formMargin labelInput" Text="Collection Agent:" Width="100px"/>
           <asp:DropDownList ID="ddlCollectionAgent" runat="server" cssClass="formMargin" DataTextField="LogonName" DataValueField="LogonName" ToolTip="Collection Agent" Width="250" />
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblMonthlyWeekly" runat="server" cssClass="formMargin labelInput" Text="Monthly/Weekly:" Width="100px"/>
           <asp:DropDownList ID="ddlMonthlyWeekly"  runat="server" cssClass="formMargin" ToolTip="Frequency of publication" Width="250">
               <asp:ListItem>M</asp:ListItem>
               <asp:ListItem>W</asp:ListItem>
           </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvMonthlyWeekly" runat="server" ControlToValidate="ddlMonthlyWeekly" ErrorMessage="Collection Manager is a required" >
        *
        </asp:RequiredFieldValidator>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
    <div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblTaxCompany" runat="server" cssClass="formMargin labelInput" Text="Tax Company:" Width="100px"/>
           <asp:DropDownList ID="ddlTaxCompany" runat="server" cssClass="formMargin"  DataTextField="LogonName" DataValueField="LogonName" ToolTip="Tax Company" Width="250"/>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblSalesAdminName" runat="server" cssClass="formMargin labelInput" Text="Sales Admin Name:" Width="100px"/>
           <asp:TextBox ID="txtSalesAdminName" runat="server" cssClass="formMargin" ToolTip="Name of Sales Admin" Width="250"/>
     </div>
    
    <div class="formRow"> 
           <asp:Label ID="lblSalesAdminPhone" runat="server" cssClass="formMargin labelInput" Text="Sales Admin Phone:" Width="100px"/>
           <asp:TextBox ID="txtSalesAdminPhone"  cssClass="formMargin" runat="server" ToolTip="Telephone Number of Sales Admin" Width="250"/>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblAdminFax" runat="server" cssClass="formMargin labelInput" Text="Sales Admin Fax:" Width="100px"/>
           <asp:TextBox ID="txtAdminFax"  cssClass="formMargin" runat="server" ToolTip="Fax of Sales Admin" Width="250"/>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblAdminEmail" runat="server" cssClass="formMargin labelInput" Text="Sales Admin Email:" Width="100px"/>
           <asp:TextBox ID="txtAdminEmail"  cssClass="formMargin" runat="server" ToolTip="Email of Sales Admin" Width="250"/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Mapping" EntityURL="PublicationToBusinessUnits.aspx" ConnectionString="MiddlewareASOPUK" deleteEnabled="true" deleteSPName="PublicationToBusinessUnit" deleteParameter="publicationToBusinessUnit"/>
</asp:Content>

