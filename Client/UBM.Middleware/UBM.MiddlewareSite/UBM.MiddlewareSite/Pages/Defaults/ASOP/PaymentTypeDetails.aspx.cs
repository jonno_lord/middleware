﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class PaymentTypesDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private int paymentid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetPaymentDetails()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_GET_PaymentType", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Payment_Type_ID", paymentid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtName.Text = sdr["Name"].ToString();
                    txtPaymentType.Text = sdr["Payment_Type"].ToString();
                    txtPaymentsetting.Text = sdr["Payment_setting"].ToString();
                    txtDocumentType.Text = sdr["Document_Type"].ToString();
                    txtCreditDocumentType.Text = sdr["Credit_Document_Type"].ToString();
                    txtInvoiceType.Text = sdr["Invoice_Type"].ToString();
                }
            }

        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering

            int selectedValue = int.Parse(Request.QueryString["system"]);
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }
        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {
            asopType = Request.QueryString["system"];

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Publication To Business Units";
                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Publication To Business Units";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Publication To Business Units";

                    break;
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow rule to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (paymentid == 0)
                {
                    Response.Redirect("PaymentTypes.aspx");
                }

                determineASOPVersion();
                GetPaymentDetails();
                
            }
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            Response.Redirect("PaymentTypes.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        //Saves Tax Override codes to ESOP database via user control Common Buttons
        public void SaveEntity()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_PaymentType", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@Payment_Type_ID", paymentid);
                command.Parameters.AddWithValue("@Name", txtName.Text);
                command.Parameters.AddWithValue("@Payment_Type", txtPaymentType.Text);
                command.Parameters.AddWithValue("@Payment_setting", txtPaymentsetting.Text);
                command.Parameters.AddWithValue("@Document_Type", txtDocumentType.Text);
                command.Parameters.AddWithValue("@Credit_Document_Type", txtCreditDocumentType.Text);
                command.Parameters.AddWithValue("@Invoice_Type", txtInvoiceType.Text);

                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}