﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{


    public partial class ActivityTypeDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;
        private string connectionName;


        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private int Id
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetActivityDetails()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_GET_ActivityType", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Id", Id);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();

                if (sdr.Read())
                {
                    ddlJDEActivity.SelectedValue = sdr["ActivityID"].ToString();
                    ddlASOPReason.SelectedValue = sdr["ReasonID"].ToString();
                     
                }
            }

        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            if (Request.QueryString["system"] != null)
            {
                selectedValue = int.Parse(Request.QueryString["system"]);
            }
            else
            {
                if (!String.IsNullOrEmpty(Session["ddlSystem"].ToString()))
                {
                    selectedValue = int.Parse(Session["ddlSystem"].ToString());
                }

            }
            CommonButtons.Cancelid = selectedValue;
       }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {

            if (Request.QueryString["system"] != null)
            {
                asopType = Request.QueryString["system"];
            }
            else
            {
                try
                {
                    if (!String.IsNullOrEmpty(Session["ddlSystem"].ToString()))
                    {
                        asopType = Session["ddlSystem"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Response.Redirect("ActivityTypes.aspx");
                }
            }

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Activity Types";
                    connectionName = "MiddlewareASOPDaltons";
                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Publication To Activity Types";
                    connectionName = "MiddlewareASOPHolland";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Publication To Activity Types";
                    connectionName = "MiddlewareASOPUK";

                    break;
            }
        }

        private void LoadMappedDropdowns()
        {
            //This sub will load both ddls using the preffered connection string.

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_ActivityTypes", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();
                while (sdr.Read())
                {
                    ListItem itmJDE = new ListItem();
                    itmJDE.Value = sdr["ActivityID"].ToString();
                    itmJDE.Text = sdr["ActivityID"] + " - " + sdr["Description"];

                    ddlJDEActivity.Items.Add(itmJDE);

                    ListItem itmReason = new ListItem();
                    itmReason.Value = sdr["ReasonID"].ToString();
                    itmReason.Text = sdr["ReasonID"] + " - " + sdr["Long_name"];

                    ddlASOPReason.Items.Add(itmReason);
                }


            }
        }

        private void LoadUnmappedDropDowns()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_JDEReasonCode", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader sdrActivity = command.ExecuteReader();

                while (sdrActivity.Read())
                {
                    ListItem itmJDE = new ListItem();
                    itmJDE.Value = sdrActivity["ActivityID"].ToString();
                    itmJDE.Text = sdrActivity["JDEReasonCode"].ToString();

                    ddlJDEActivity.Items.Add(itmJDE);
                }
            }

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_ASOPReasonCode", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader sdrReason = command.ExecuteReader();

                while (sdrReason.Read())
                {
                    ListItem itmReason = new ListItem();
                    itmReason.Value = sdrReason["Reason_ID"].ToString();
                    itmReason.Text = sdrReason["ASOPReasonCode"].ToString();

                    ddlASOPReason.Items.Add(itmReason);
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow rule to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            CommonButtons.ucDeleteEntity_Click += btnDeleteEntity_Click;

            if (!IsPostBack)
            {
                determineASOPVersion();
                LoadUnmappedDropDowns();

                if (Id > 0)
                {
                    LoadMappedDropdowns();
                    GetActivityDetails();
                }
                
            }

        }

        //called by button control to delete and redirect page
        protected void btnDeleteEntity_Click(object sender, EventArgs e)
        {
            determineASOPVersion();
            CommonButtons.ConnectionString = connectionName;
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            Response.Redirect("ActivityTypes.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, if edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        //Saves activity type mapping to ASOP database via user control Common Buttons
        public void SaveEntity()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_ActivityType", connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.AddWithValue("@Id", Id);
                command.Parameters.AddWithValue("@ActivityID", ddlJDEActivity.SelectedValue);
                command.Parameters.AddWithValue("@ReasonID", ddlASOPReason.SelectedValue);

                command.ExecuteNonQuery();

            }
        }

        #endregion
    }
}