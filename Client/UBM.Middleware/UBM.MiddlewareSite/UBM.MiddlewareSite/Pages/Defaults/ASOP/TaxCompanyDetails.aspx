﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" CodeBehind="TaxCompanyDetails.aspx.cs" Inherits="Pages.Defaults.ASOP.TaxCompanyDetails" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <div class="buttonAlignLeft">
      <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
    </div>
    <div class="validationSummary">
        <asp:ValidationSummary ID="vdsTaxCompany" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" runat="server">
        <div class="addEditForm">

            <div class="formRow">
              <asp:Label ID="lblTaxCompanyCode" runat="server" cssClass="formMargin labelInput" Text="Tax Company Code:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtTaxCompanyCode" runat="server" cssClass="formMargin" ToolTip="Tax Company Code" Width="250" MaxLength="4"/>
              <asp:RequiredFieldValidator ID="rfvTaxCompanyCode" runat="server" ControlToValidate="txtTaxCompanyCode" ErrorMessage="please enter a tax company code">*</asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
              <asp:Label ID="lblName" runat="server" cssClass="formMargin labelInput" Text="Name:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtName" runat="server" cssClass="formMargin" ToolTip="Name of Company" Width="250" MaxLength="50"/>
              <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="please enter a company name">*</asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
              <asp:Label ID="lblVatRegNo" runat="server" cssClass="formMargin labelInput" Text="VAT Registration Number:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtVatRegNo" runat="server" cssClass="formMargin" ToolTip="Company VAT Registration Number" Width="250" MaxLength="50"/>
              <asp:RequiredFieldValidator ID="rfvVatRegNo" runat="server" ControlToValidate="txtVatRegNo" ErrorMessage="please enter a VAT Registration Number">*</asp:RequiredFieldValidator>
            </div>

    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" runat="server">

    <div class="addEditForm">

            <div class="formRow">
              <asp:Label ID="lblAddress1" runat="server" cssClass="formMargin labelInput" Text="Address Line 1:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtAddress1" runat="server" cssClass="formMargin" ToolTip="First line of address" Width="250" MaxLength="50"/>
              <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1" ErrorMessage="please enter the first line of address">*</asp:RequiredFieldValidator>
            </div>

              <div class="formRow">
              <asp:Label ID="lblAddress2" runat="server" cssClass="formMargin labelInput" Text="Address Line 2:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtAddress2" runat="server" cssClass="formMargin" ToolTip="Second line of address" Width="250" MaxLength="50"/>
              <asp:RequiredFieldValidator ID="rfvAddress2" runat="server" ControlToValidate="txtAddress2" ErrorMessage="please enter the second line of address">*</asp:RequiredFieldValidator>
            </div>

             <div class="formRow">
              <asp:Label ID="lblAddress3" runat="server" cssClass="formMargin labelInput" Text="Address Line 3:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtAddress3" runat="server" cssClass="formMargin" ToolTip="Third line of address" Width="250" MaxLength="50"/>
            </div>

            <div class="formRow">
              <asp:Label ID="lblTown" runat="server" cssClass="formMargin labelInput" Text="Town:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtTown" runat="server" cssClass="formMargin" ToolTip="Town" Width="250" MaxLength="40"/>
              <asp:RequiredFieldValidator ID="rfvTown" runat="server" ControlToValidate="txtTown" ErrorMessage="please enter a town">*</asp:RequiredFieldValidator>
            </div>

             <div class="formRow">
              <asp:Label ID="lblPostcode" runat="server" cssClass="formMargin labelInput" Text="Postcode:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtPostcode" runat="server" cssClass="formMargin" ToolTip="Postcode" Width="250" MaxLength="10"/>
              <asp:RequiredFieldValidator ID="rfvPostcode" runat="server" ControlToValidate="txtPostcode" ErrorMessage="please enter a postcode">*</asp:RequiredFieldValidator>
            </div>

             <div class="formRow">
              <asp:Label ID="lblCounty" runat="server" cssClass="formMargin labelInput" Text="County:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtCounty" runat="server" cssClass="formMargin" ToolTip="County" Width="250" MaxLength="20"/>
            </div>

             <div class="formRow">
              <asp:Label ID="lblCountry" runat="server" cssClass="formMargin labelInput" Text="Country:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtCountry" runat="server" cssClass="formMargin" ToolTip="Country" Width="250" MaxLength="40"/>
              <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="txtCountry" ErrorMessage="please enter the country">*</asp:RequiredFieldValidator>
            </div>

     </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" runat="server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Tax Company" EntityURL="TaxCompanies.aspx" ConnectionString="MiddlewareASOPUK" deleteEnabled="true" deleteSPName="TaxCompany" deleteParameter="taxCompany"/>
</asp:Content>
