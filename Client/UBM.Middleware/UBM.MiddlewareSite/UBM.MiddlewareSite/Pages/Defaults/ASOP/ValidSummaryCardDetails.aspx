﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.ValidSummaryCardDetails" Codebehind="ValidSummaryCardDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
   <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsSummaryCards" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">
    
    <div class="formRow"> 
           <asp:Label ID="lblJDEObjectCode" runat="server" cssClass="formMargin labelInput" Text="Group Code:" Width="100px"/>
           <asp:TextBox ID="txtJDEObjectCode" runat="server" cssClass="formMargin" ToolTip="JDE Object Code" Width="250" MaxLength="50"/>
        <asp:RequiredFieldValidator ID="rfvJDEObjectCode" runat="server" ControlToValidate="txtJDEObjectCode" ErrorMessage="JDE Object Code Required " >
        *
        </asp:RequiredFieldValidator>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblJDESubsidiaryCode" runat="server" cssClass="formMargin labelInput" Text="Group Code:" Width="100px"/>
           <asp:TextBox ID="txtJDESubsidiaryCode" runat="server" cssClass="formMargin" ToolTip="JDE Subsidiary Code" Width="250" MaxLength="50"/>
        <asp:RequiredFieldValidator ID="rfvSubsidiaryCode" runat="server" ControlToValidate="txtJDESubsidiaryCode" ErrorMessage="JDE Subsidiary Code Required " >
        *
        </asp:RequiredFieldValidator>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblValidEnter" runat="server" cssClass="formMargin labelInput" Text="Valid:" Width="100px"/>
           <asp:CheckBox ID="cbxValid" runat="server" cssClass="formMargin checkboxLayout" ToolTip="Is Card Valid?" Height="12px"/>
    </div>
</div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblSummaryCard" runat="server" cssClass="formMargin labelInput" Text="Summary Card:" Width="100px"/>
           <asp:TextBox ID="txtSummaryCard" runat="server" cssClass="formMargin" ToolTip="Summary Card Name" Width="250"/>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblShortCode" runat="server" cssClass="formMargin labelInput" Text="Short Code:" Width="100px"/>
           <asp:TextBox ID="txtShortCode" runat="server" cssClass="formMargin" ToolTip="Short Code of Summary Type" Width="250"/>
     </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
       <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Summary Card" EntityURL="ValidSummaryCards.aspx" ConnectionString="MiddlewareASOPUK"/>
</asp:Content>

