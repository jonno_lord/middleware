﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class CustomerTypeDetails : Page
    {
        #region Private Fields and Properties


        private static SqlConnection connection;
        private string asopType;
        private string connectionString;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int typeid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with customer type data
        private void GetCustomerType()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_CustomerType", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", typeid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtShortName.Text = sdr["Short_name"].ToString();
                    txtLongName.Text = sdr["Long_name"].ToString();
                    ddlGroupCode.SelectedValue = sdr["Group_code"].ToString();
                }
            }
        }

        private void GetGroupCodeDescription()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_CustomerTypeGroups", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                ListItem itmType = new ListItem
                                       {
                                           Value = null,
                                           Text = "Null"
                                       };
                ddlGroupCode.Items.Add(itmType);

                while (sdr.Read())
                {
                    itmType = new ListItem();
                    itmType.Value = sdr["Group_code"].ToString();
                    itmType.Text = sdr["Group_code"] + " - " + sdr["Group_Description"];
                    ddlGroupCode.Items.Add(itmType);
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = int.Parse(Request.QueryString["system"]);
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {
            asopType = Request.QueryString["system"];

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Customer Types";

                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Customer Types";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Customer Types";

                    break;
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                
                //prevent add page from being accessed
                if (typeid == 0)
                {
                    Response.Redirect("CustomerTypes.aspx");
                }

                determineASOPVersion();
                GetGroupCodeDescription();
                GetCustomerType();
                
                //non-editable fields
                txtShortName.Enabled = false;
                txtLongName.Enabled = false;

                //MUST be set to disable deletion button
                CommonButtons.deleteEnabled = false;
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("CustomerTypes.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Updates customer types  and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_UPD_CustomerType", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@id", typeid);
                command.Parameters.AddWithValue("@groupCode", ddlGroupCode.SelectedValue);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}