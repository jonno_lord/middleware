﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class TaxCompany : Page
    {

        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region private methods

        private void LoadGrid()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_TaxCompanies", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();
            }
        }

        private void SetASOPType()
        {
            if (Request.QueryString["id"] != null)
            {
                ddlChangeASOP.SelectedValue = Request.QueryString["id"];
            }
        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void DetermineASOPVersion()
        {

            asopType = ddlChangeASOP.SelectedItem.ToString();

            switch (asopType)
            {
                case "ASOP Daltons":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Tax Companies";

                    break;
                case "ASOP Holland":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Tax Companies";

                    break;
                case "ASOP UK":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Tax Companies";

                    break;
            }
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger asopTrigger = new AsyncPostBackTrigger();

            asopTrigger.ControlID = ddlChangeASOP.UniqueID;

            asopTrigger.EventName = "SelectedIndexChanged";

            updTaxCompany.Triggers.Add(asopTrigger);

            AsyncPostBackTrigger asopTitleTrigger = new AsyncPostBackTrigger();

            asopTitleTrigger.ControlID = ddlChangeASOP.UniqueID;

            asopTitleTrigger.EventName = "SelectedIndexChanged";

            updHeader.Triggers.Add(asopTitleTrigger);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //set to UK as default
                if (Request.QueryString["id"] == null)
                {
                    ddlChangeASOP.SelectedValue = "3";
                }

                SetASOPType();
                DetermineASOPVersion();

                LoadGrid();

                Session["ddlTaxSystem"] = ddlChangeASOP.SelectedValue;
            }
        }

        //links to details page with id of payment type
        protected void rptTaxCompany_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("TaxCompanyDetails.aspx?Id=" + e.CommandArgument + "&system=" + ddlChangeASOP.SelectedValue);
                    break;
            }
        }

        //reloads grid with chosen data
        protected void ddlASOPSystemSelect(object sender, EventArgs e)
        {

            string system = ddlChangeASOP.SelectedValue;
            Session["ddlTaxSystem"] = system;

            Response.Redirect("TaxCompanies.aspx?id=" + ddlChangeASOP.SelectedValue);
      
        }
    }
}