﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.PaymentTypesDetails" Codebehind="PaymentTypeDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsPaymentTypes" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">

        <div class="formRow">
            <asp:Label ID="lblName" runat="server" cssClass="formMargin labelInput" Text="Name:" Width="100px"/>
            <asp:TextBox ID="txtName" runat="server" CssClass="formMargin" ToolTip="Name" MaxLength="27" Width="250px" />
        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name Required">
        *
        </asp:RequiredFieldValidator>
        </div>

        <div class="formRow">
            <asp:Label ID="lblPaymentType" runat="server" cssClass="formMargin labelInput" Text="Payment Type:" Width="100px"/>
            <asp:TextBox ID="txtPaymentType" runat="server" cssClass="formMargin" ToolTip="Payment Type" MaxLength="4" Width="250"/>        
        </div>

        <div class="formRow">
               <asp:Label ID="lblPaymentsetting" runat="server" cssClass="formMargin labelInput" Text="Payment Setting:" Width="100px"/>
               <asp:TextBox ID="txtPaymentsetting" runat="server" cssClass="formMargin" ToolTip="Payment setting" MaxLength="3" Width="250"/>
            <asp:RequiredFieldValidator ID="rfvPaymentsetting" runat="server" ControlToValidate="txtPaymentsetting" ErrorMessage="Payment setting Required">
            *
            </asp:RequiredFieldValidator>
        </div> 

        <div class="formRow">
            <asp:Label ID="lblDocumentType" runat="server" cssClass="formMargin labelInput" Text="Document Type:" Width="100px"/>
            <asp:TextBox ID="txtDocumentType" runat="server" cssClass="formMargin" ToolTip="Document Type" MaxLength="50" Width="250"/>        
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="rightForm" Runat="Server">
     <div class="addEditForm">

        <div class="formRow">
            <asp:Label ID="lblCreditDocumentType" runat="server" cssClass="formMargin labelInput" Text="Credit Document Type:" Width="100px"/>
            <asp:TextBox ID="txtCreditDocumentType" runat="server" cssClass="formMargin" ToolTip="Credit Document Type" MaxLength="50" Width="250"/>        
        </div>

        <div class="formRow">
            <asp:Label ID="lblInvoiceType" runat="server" cssClass="formMargin labelInput" Text="Invoice Type:" Width="100px"/>
            <asp:TextBox ID="txtInvoiceType" runat="server" cssClass="formMargin" ToolTip="Invoice Type" MaxLength="50" Width="250"/>        
        </div>

        
    </div>
</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Type" EntityURL="PaymentTypes.aspx" />
</asp:Content>


