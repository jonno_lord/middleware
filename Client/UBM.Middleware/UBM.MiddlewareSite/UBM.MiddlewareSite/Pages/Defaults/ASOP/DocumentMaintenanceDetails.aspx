﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.DocumentMaintenanceDetails" Codebehind="DocumentMaintenanceDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
     <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblDocument" runat="server" cssClass="formMargin labelInput" Text="Document:" Width="100px"/>
           <asp:TextBox ID="txtDocument" runat="server" cssClass="formMargin" ToolTip="Document Name and Group"  Width="250"/>
    </div>   
    
    <div class="formRow"> 
           <asp:Label ID="lblPaymentSetting" runat="server" cssClass="formMargin labelInput" Text="Payment Setting:" Width="100px"/>
           <asp:textbox ID="txtPaymentSetting"  cssClass="formMargin" runat="server" ToolTip="Group Code" Width="250" MaxLength="50"/>
    </div>

     <div class="formRow"> 
           <asp:Label ID="lblDocumentType" runat="server" cssClass="formMargin labelInput" Text="Document Type:" Width="100px"/>
           <asp:textbox ID="txtDocumentType"  cssClass="formMargin" runat="server" ToolTip="Document Type" Width="250" MaxLength="50"/>
    </div>

        <div class="formRow"> 
           <asp:Label ID="lblCreditDocument" runat="server" cssClass="formMargin labelInput" Text="Credit Document:" Width="100px"/>
           <asp:textbox ID="txtCreditDocument"  cssClass="formMargin" runat="server" ToolTip="Credit Document Type" Width="250" MaxLength="50"/>
    </div>

        <div class="formRow"> 
           <asp:Label ID="lblInvoice" runat="server" cssClass="formMargin labelInput" Text="Invoice Type:" Width="100px"/>
           <asp:dropdownlist ID="ddlInvoice"  cssClass="formMargin" runat="server" ToolTip="Invoice Type of Document" DataTextField="" DataValueField=""/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblReceipt" runat="server" cssClass="formMargin labelInput" Text="Receipt:" Width="100px"/>
           <asp:CheckBox ID="cbxReceipt" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Is Receiptable? " Checked="false" Height="12px" />
    </div>   

     <div class="formRow">
           <asp:Label ID="lblJersey" runat="server" cssClass="formMargin labelInput" Text="Jersey:" Width="100px"/>
           <asp:CheckBox ID="cbxJersey" CssClass="formMargin checkboxLayout" runat="server" ToolTip="Jersey" Checked="false" Height="12px" />
     </div>

     <div class="formRow"> 
           <asp:Label ID="lblJDEAccountNo" runat="server" cssClass="formMargin labelInput" Text="JDE Bucket Account Number:" Width="100px"/>
           <asp:TextBox ID="txtJDEAccountNo"  cssClass="formMargin" runat="server" ToolTip="JDE Account Number" MaxLegth="20" Width="250"/>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
<uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Document" EntityURL="DocumentMaintenance.aspx" ConnectionString="MiddlewareASOPUK"/>    
</asp:Content>

