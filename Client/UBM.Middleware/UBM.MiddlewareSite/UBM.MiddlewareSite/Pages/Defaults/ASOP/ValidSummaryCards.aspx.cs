﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class ValidSummaryCards : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //loads grid data from database
        private void LoadGrid()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_ValidSummaryCards", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();

            }
        }

        private void SetASOPType()
        {
            if (Request.QueryString["id"] != null)
            {
                ddlChangeASOP.SelectedValue = Request.QueryString["id"];
            }
        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {

            asopType = ddlChangeASOP.SelectedItem.ToString();

            switch (asopType)
            {
                case "ASOP Daltons":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Valid Summary Cards";

                    break;
                case "ASOP Holland":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Valid Summary Cards";

                    break;
                case "ASOP UK":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Valid Summary Cards";

                    break;
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger asopTrigger = new AsyncPostBackTrigger();

            asopTrigger.ControlID = ddlChangeASOP.UniqueID;

            asopTrigger.EventName = "SelectedIndexChanged";

            updSummaryCardsASOP.Triggers.Add(asopTrigger);

            AsyncPostBackTrigger asopTitleTrigger = new AsyncPostBackTrigger();

            asopTitleTrigger.ControlID = ddlChangeASOP.UniqueID;

            asopTitleTrigger.EventName = "SelectedIndexChanged";

            updHeader.Triggers.Add(asopTitleTrigger);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //set to UK as default
                if (Request.QueryString["id"] == null)
                {
                    ddlChangeASOP.SelectedValue = "3";
                }

                SetASOPType();
                determineASOPVersion();

                LoadGrid();
            }
        }

        //links to details page with id of valid summary card
        protected void rptSummaryCard_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("ValidSummaryCardDetails.aspx?id=" + e.CommandArgument + "&system=" + ddlChangeASOP.SelectedValue);

                    break;
            }
        }

        //disables checkbox clicking
        protected void rptSummaryCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DbDataRecord row = e.Item.DataItem as DbDataRecord;

                CheckBox cbox = e.Item.FindControl("cbxDisabled") as CheckBox;

                //sets id as an attribute of checkbox
                if (cbox != null)
                {
                    cbox.Enabled = false;
                }
            }
        }

        //reloads grid with chosen data
        protected void ddlASOPSystemSelect(object sender, EventArgs e)
        {
            determineASOPVersion();
            LoadGrid();
        }

        #endregion
    }
}