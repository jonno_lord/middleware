﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.ClashingTitleCards" Codebehind="ClashingTitleCards.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>ASOP Clashing Title Cards</h1>
<div class="toggleFilter"><asp:image ID="imgFilter" runat="server" imageUrl="~/Images/filter_icon.png" alt="filter" cssClass="filterButton" ToolTip="Filter Clashing Title Cards"/></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<div id="outerFilter" class="outerFilter queryFormRow">
    <asp:UpdatePanel ID="updPublication" runat="server" >
         <ContentTemplate>
            <asp:TextBox ID="txtLongName" runat="server" Width="180" CssClass="innerFilter"/>
            <asp:TextBox ID="txtShortName" runat="server" Width="130" CssClass="innerFilter"/>
            <asp:imagebutton ID="btnSearch" runat="server" imageUrl="~/Images/search_icon.png" alt="Go" tooltip="Filter Mappings" cssClass="filterSubmit" onClick="btnSearch_click"/>
            <asp:imagebutton ID="btnClear" runat="server" imageUrl="~/Images/clear_icon.png" alt="Clear" tooltip="Clear Filters" cssClass="filterSubmit" onClick="btnFilter_click"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updJobs" runat="server" >
     <ContentTemplate>
        <asp:GridView ID="dgrClashingTitleCards" runat="server" cssClass="break" AutoGenerateColumns="false"  AllowPaging="True" onpageindexchanged="dgrClashingTitleCards_PageIndexChanged" onpageindexchanging="dgrClashingTitleCards_PageIndexChanging" PageSize="30">
            <emptydatatemplate>
                No Clashing Title Cards Found
                </emptydatatemplate>
            <EmptyDataRowStyle/>
            <Columns>
                <asp:BoundField DataField="Long_Name" HeaderText="Name" HeaderStyle-Width="150px" HeaderStyle-CssClass="GridCentre"/>
                <asp:BoundField DataField="Short_Name" HeaderText="Short Name"  HeaderStyle-Width="100px" HeaderStyle-CssClass="GridCentre"/>
            </Columns>
            <PagerStyle CssClass="pageIndex" />
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>

