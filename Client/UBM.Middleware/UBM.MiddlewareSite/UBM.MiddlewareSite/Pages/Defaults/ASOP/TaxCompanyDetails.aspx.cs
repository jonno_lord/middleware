﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class TaxCompanyDetails : System.Web.UI.Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;
        private string connectionName;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int id
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with job data
        private void GetTaxCompany()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_TaxCompany", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", id);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtTaxCompanyCode.Text = sdr["Tax_Company_Code"].ToString();
                    txtName.Text = sdr["Entity_name"].ToString();
                    txtVatRegNo.Text = sdr["Entity_VAT_Reg_no"].ToString();
                    txtAddress1.Text = sdr["Address_1"].ToString();
                    txtAddress2.Text = sdr["Address_2"].ToString();
                    txtAddress3.Text = sdr["Address_3"].ToString();
                    txtTown.Text = sdr["Town"].ToString();
                    txtPostcode.Text = sdr["Postcode"].ToString();
                    txtCounty.Text = sdr["County"].ToString();
                    txtCountry.Text = sdr["Country"].ToString();
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering

            int selectedValue = 0;
             if (Request.QueryString["system"] != null)
                {
                 selectedValue = int.Parse(Request.QueryString["system"]);
                }
            else
            {
                if (!String.IsNullOrEmpty(Session["ddlTaxSystem"].ToString()))
                {
                    selectedValue = int.Parse(Session["ddlTaxSystem"].ToString());
                }
               
            }
                CommonButtons.Cancelid = selectedValue;

        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void DetermineASOPVersion()
        {
            if (Request.QueryString["system"] != null)
            {
                asopType = Request.QueryString["system"];
            }
            else
            {
                try
                {
                    if (!String.IsNullOrEmpty(Session["ddlTaxSystem"].ToString()))
                    {
                        asopType = Session["ddlTaxSystem"].ToString();
                    }
                }
                catch(Exception exception)
                {
                    Response.Redirect("TaxCompanies.aspx");
                }

            }

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Publication To Business Units";
                    connectionName = "MiddlewareASOPDaltons";
                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Publication To Business Units";
                    connectionName = "MiddlewareASOPHolland";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Publication To Business Units";
                    connectionName = "MiddlewareASOPUK";

                    break;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            CommonButtons.ucDeleteEntity_Click += btnDeleteEntity_Click;

            if (!IsPostBack)
            {
                if (id > 0)
                {
                    DetermineASOPVersion();
                    GetTaxCompany();
                }
                else
                {
                    DetermineASOPVersion();
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnDeleteEntity_Click(object sender, EventArgs e)
        {
            DetermineASOPVersion();
            CommonButtons.ConnectionString = connectionName;

            //filters results by system of job being created or edited
            Response.Redirect("TaxCompanies.aspx?id=" + asopType);
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("TaxCompanies.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Updates customer types  and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            DetermineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_TaxCompany", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@id", id);
                command.Parameters.AddWithValue("@taxCompanyCode", txtTaxCompanyCode.Text);
                command.Parameters.AddWithValue("@name", txtName.Text);
                command.Parameters.AddWithValue("@vatRegNo", txtVatRegNo.Text);
                command.Parameters.AddWithValue("@address1", txtAddress1.Text);
                command.Parameters.AddWithValue("@address2", txtAddress2.Text);
                command.Parameters.AddWithValue("@address3", txtAddress3.Text);
                command.Parameters.AddWithValue("@town", txtTown.Text);
                command.Parameters.AddWithValue("@postcode", txtPostcode.Text);
                command.Parameters.AddWithValue("@county", txtCounty.Text);
                command.Parameters.AddWithValue("@country", txtCountry.Text);

                command.ExecuteNonQuery();
            }
        }
        #endregion

    }
}