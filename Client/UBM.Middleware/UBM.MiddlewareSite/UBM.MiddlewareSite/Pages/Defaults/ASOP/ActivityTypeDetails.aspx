﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.ActivityTypeDetails" Codebehind="ActivityTypeDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <div class="buttonAlignLeft">
      <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
    </div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsPaymentTypes" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">

        <div class="formRow">
          <asp:Label ID="lblJDEActivity" runat="server" cssClass="formMargin labelInput" Text="JDE Activity:"></asp:Label>
            <asp:DropDownList ID="ddlJDEActivity" runat="server" AutoPostBack="True" cssClass="formMargin"/>
        </div>

        <div class="formRow">
        <asp:RequiredFieldValidator ID="rfvJDEDesc" runat="server" ControlToValidate="ddlJDEActivity" ErrorMessage="JDE Activity Required">
        *
        </asp:RequiredFieldValidator>
        </div>


    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="rightForm" Runat="Server">
     <div class="addEditForm">

        <div class="formRow">
               <asp:Label ID="lblASOPReason" runat="server" cssClass="formMargin labelInput"  Text="ASOP Reason:"></asp:Label>
               <asp:DropDownList ID="ddlASOPReason" runat="server" AutoPostBack="True" cssClass="formMargin">
               </asp:DropDownList>
        </div> 

        <div class="formRow">
            <asp:RequiredFieldValidator ID="rfvASOPDesc" runat="server" ControlToValidate="ddlASOPReason" ErrorMessage="ASOP Reason Required">
            *
            </asp:RequiredFieldValidator>
        </div> 
     </div>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Type" EntityURL="ActivityTypes.aspx" ConnectionString="MiddlewareASOPUK" deleteEnabled="true" deleteSPName="ActivityType" deleteParameter=""/>
</asp:Content>


