﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class DocumentMaintenanceDetails : Page
    {
        #region Private Fields and Methods

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private string groupCode
        {
            get
            {
                string returnValue = Request.QueryString["groupCode"];
                return returnValue;
            }
        }

        private string paymentType
        {
            get
            {
                string returnValue = Request.QueryString["paymentType"];
                return returnValue;
            }
        }

        private string paymentSetting
        {
            get
            {
                string returnValue = Request.QueryString["paymentSetting"];
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with customer type payment data
        private void GetDocument()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_CustomerTypePayment", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@GroupCode", groupCode);
                command.Parameters.AddWithValue("@PaymentType", paymentType);
                command.Parameters.AddWithValue("@PaymentSetting", paymentSetting);


                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtDocument.Text = sdr["Name"] + " - " + sdr["Group_Description"] + " (" + sdr["Group_Code"] + ")";
                    txtPaymentSetting.Text = sdr["Payment_setting"].ToString();
                    txtDocumentType.Text = sdr["Document_Type"].ToString();
                    txtCreditDocument.Text = sdr["Credit_Document_Type"].ToString();
                    ddlInvoice.SelectedValue = sdr["Invoice_Type"].ToString();

                    //handle null values
                    cbxReceipt.Checked = sdr["Receipt"] != DBNull.Value && Convert.ToBoolean(sdr["Receipt"]);
                    cbxJersey.Checked = sdr["Jersey"] != DBNull.Value && Convert.ToBoolean(sdr["Jersey"]);

                    txtJDEAccountNo.Text = sdr["Jde_Account_No"].ToString();
                }
            }
        }

        private void GetBusinessUnitPublicationNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_InvoiceTypes", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();
                while (sdr.Read())
                {
                    ListItem itmInvoice = new ListItem();
                    itmInvoice.Value = sdr["Invoice_Type"].ToString();
                    itmInvoice.Text = sdr["Description"].ToString();

                    ddlInvoice.Items.Add(itmInvoice);
                }
            }

        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = int.Parse(Request.QueryString["system"]);
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {
            asopType = Request.QueryString["system"];

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Valid Summary Cards";

                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Valid Summary Cards";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Valid Summary Cards";

                    break;
            }
        }
        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                determineASOPVersion();
                GetBusinessUnitPublicationNames();
                GetDocument();

                //prevents edits taking place
                txtDocument.Enabled = false;
                txtPaymentSetting.Enabled = false;

                //MUST be set to disable deletion button
                CommonButtons.deleteEnabled = false;
            }
        }

        //called by button control to save summary card and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("DocumentMaintenance.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Updates valid summary cards and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_CustomerTypePayment", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@GroupCode", groupCode);
                command.Parameters.AddWithValue("@PaymentType", paymentType);
                command.Parameters.AddWithValue("@PaymentSetting", paymentSetting);
                command.Parameters.AddWithValue("@Receipt", cbxReceipt.Checked);
                command.Parameters.AddWithValue("@Jersey", cbxJersey.Checked);
                command.Parameters.AddWithValue("@JdeAccountNo", txtJDEAccountNo.Text);
                command.Parameters.AddWithValue("@DocumentType", txtDocumentType.Text);
                command.Parameters.AddWithValue("@CreditDocumentType", txtCreditDocument.Text);
                command.Parameters.AddWithValue("@InvoiceType", ddlInvoice.SelectedValue);

                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}