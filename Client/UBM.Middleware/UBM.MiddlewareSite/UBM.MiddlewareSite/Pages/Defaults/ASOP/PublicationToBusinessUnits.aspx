﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.PublicationToBusinessUnits" Codebehind="PublicationToBusinessUnits.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Mapping"  Title="MW ASOP" AddEntityURL="PublicationToBusinessUnitDetails.aspx" QueryStringFilter="id"/>  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
<div class="filter">
            <div class="rowA">
                <p>ASOP:</p>
            </div>
            <div class="rowB">
                <asp:DropDownList id="ddlChangeASOP" runat="server" tooltip="Version of ASOP" OnSelectedIndexChanged="ddlASOPSystemSelect" AutoPostBack="True">
                    <asp:ListItem Text="ASOP Daltons" Value="1"/>
                    <asp:ListItem Text="ASOP Holland" Value="2"/>
                    <asp:ListItem Text="ASOP UK" Value="3"/>
                </asp:DropDownList>
            </div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Mappings" TableName="PublicationToBusinessUnit" DisableEnable="false"/>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
<asp:UpdatePanel ID="updHeader" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
    </ContentTemplate>
</asp:UpdatePanel>
<div class="toggleFilter"><asp:image ID="imgFilter" runat="server" imageUrl="~/Images/filter_icon.png" alt="filter" cssClass="filterButton" ToolTip="Filter Publication To Business Units"/></div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
<div id="outerFilter" class="outerFilter queryFormRow">
    <asp:UpdatePanel ID="updPublication" runat="server" >
         <ContentTemplate>
            <asp:Label ID="lblFilter" runat="server" Text="Filters:" Width="50" CssClass="innerFilter"/>
            <asp:TextBox ID="txtTitleCardFilter" runat="server" Width="130" CssClass="innerFilter" ToolTip="Title Card"/>
            <asp:TextBox ID="txtDescriptionFilter" runat="server" Width="295" CssClass="innerFilter" ToolTip="Title Card Description"/>
            <asp:TextBox ID="txtBusinessUnit" runat="server" Width="130" CssClass="innerFilter" ToolTip="Business Unit"/>
            <asp:TextBox ID="txtBUDescriptionFilter" runat="server" Width="240" CssClass="innerFilter" ToolTip="Business Unit Description"/>
            <asp:imagebutton ID="btnSearch" runat="server" imageUrl="~/Images/search_icon.png" alt="Go" tooltip="Filter Mappings" cssClass="filterSubmit" onClick="btnSearch_click"/>
            <asp:imagebutton ID="btnClear" runat="server" imageUrl="~/Images/clear_icon.png" alt="Clear" tooltip="Clear Filters" cssClass="filterSubmit" onClick="btnFilter_click"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<div id="pageGrid">
    <asp:UpdatePanel ID="updJobs" runat="server" updateMode="Conditional">
         <ContentTemplate>
            <asp:UpdateProgress ID="uppRunJobs" runat="server" DisplayAfter="5">
                <ProgressTemplate>
                     <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:GridView ID="dgrPublicationToBusinessUnits" runat="server" cssClass="formRowNoClear" OnRowCommand="dgrPublication_RowCommand" AutoGenerateColumns="false"  AllowPaging="True" onpageindexchanged="dgrPublication_PageIndexChanged" onpageindexchanging="dgrPublication_PageIndexChanging" PageSize="30">
                <emptydatatemplate>
                    No Publication to Business Unit mappings found
                    </emptydatatemplate>
                <EmptyDataRowStyle/>
                <Columns>
                    <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="10px" ItemStyle-Width="10px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" ImageUrl="~/images/AddIconsmall.png" ToolTip="Edit Mapping"  runat="server" CommandArgument='<%# Eval("id") %>' CommandName="edit" ItemStyle-CssClass="GridCentre"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Title_Short_Name" HeaderText="Title Card" HeaderStyle-Width="140px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre"/>
                    <asp:BoundField DataField="Title_Card" HeaderText="Title Card Description"  HeaderStyle-Width="290px" HeaderStyle-CssClass="GridCentre"/>
                    <asp:BoundField DataField="Business_Unit" HeaderText="Business Unit"  HeaderStyle-Width="120px" ItemStyle-CssClass="GridCentre" HeaderStyle-CssClass="GridCentre"/>
                    <asp:BoundField DataField="Business_Unit_Description" HeaderText="Description"  HeaderStyle-Width="280px" HeaderStyle-CssClass="GridCentre"/>
                </Columns>
                <PagerStyle CssClass="pageIndex" />
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
</asp:Content>

