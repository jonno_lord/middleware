﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.CustomerTypeDetails" Codebehind="CustomerTypeDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <div class="buttonAlignLeft">
   <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsCustomerTypes" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblShortName" runat="server" cssClass="formMargin labelInput" Text="Short Name:" Width="100px"/>
           <asp:TextBox ID="txtShortName" runat="server" cssClass="formMargin" ToolTip="Short Name of Customer Type" MaxLength="4" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvShortName" runat="server" ControlToValidate="txtShortName" ErrorMessage="Short Name required for Customer Type" >
        *
        </asp:RequiredFieldValidator>
    </div>   

     <div class="formRow">
           <asp:Label ID="lblLongname" runat="server" cssClass="formMargin labelInput" Text="Long Name:" Width="100px"/>
           <asp:TextBox ID="txtLongName" runat="server" cssClass="formMargin" ToolTip="Full name of Customer Type" MaxLength="27"  Width="250"/>
        <asp:RequiredFieldValidator ID="rfvLongName" runat="server" ControlToValidate="txtLongName" ErrorMessage="Long Name required for Customer Type" >
        *
        </asp:RequiredFieldValidator>
     </div>
    
    <div class="formRow"> 
           <asp:Label ID="lblGroupCode" runat="server" cssClass="formMargin labelInput" Text="Group Code:" Width="100px"/>
           <asp:dropdownlist ID="ddlGroupCode"  cssClass="formMargin" runat="server" DataTextField="Group_Code" DataValueField="Group_Code" ToolTip="Group Code" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvGroupCode" runat="server" ControlToValidate="ddlGroupCode" ErrorMessage="Group Type is required for Customer Type" >
        *
        </asp:RequiredFieldValidator>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
       <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Type" EntityURL="CustomerTypes.aspx" ConnectionString="MiddlewareASOPUK"/>
</asp:Content>

