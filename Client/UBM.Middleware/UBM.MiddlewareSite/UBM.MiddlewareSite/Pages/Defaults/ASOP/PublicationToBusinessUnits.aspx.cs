﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class PublicationToBusinessUnits : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        //loads grid data from database
        private void LoadGrid()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_SEL_PublicationToBusinessUnits", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@publication", txtTitleCardFilter.Text);
                command.Parameters.AddWithValue("@titleCard", txtDescriptionFilter.Text);
                command.Parameters.AddWithValue("@businessUnit", txtBusinessUnit.Text);
                command.Parameters.AddWithValue("@businessUnitDescription", txtBUDescriptionFilter.Text);

                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataSet ds = new DataSet();
                sda.Fill(ds);

                dgrPublicationToBusinessUnits.DataSource = ds.Tables[0];
                dgrPublicationToBusinessUnits.DataBind();

            }
        }

        private void SetASOPType()
        {
            if (Request.QueryString["id"] != null)
            {
                ddlChangeASOP.SelectedValue = Request.QueryString["id"];
            }
        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {

            asopType = ddlChangeASOP.SelectedItem.ToString();

            switch (asopType)
            {
                case "ASOP Daltons":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Publication To Business Units";
                    break;
                case "ASOP Holland":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Publication To Business Units";
                    break;
                case "ASOP UK":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Valid Publication To Business Units";
                    break;
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Init(object sender, EventArgs e)
        {
            AsyncPostBackTrigger asopTrigger = new AsyncPostBackTrigger();

            asopTrigger.ControlID = ddlChangeASOP.UniqueID;

            asopTrigger.EventName = "SelectedIndexChanged";

            updJobs.Triggers.Add(asopTrigger);

            AsyncPostBackTrigger asopTitleTrigger = new AsyncPostBackTrigger();

            asopTitleTrigger.ControlID = ddlChangeASOP.UniqueID;

            asopTitleTrigger.EventName = "SelectedIndexChanged";

            updHeader.Triggers.Add(asopTitleTrigger);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //set to UK as default
                ddlChangeASOP.SelectedValue = Request.QueryString["id"] ?? "3";

                SetASOPType();

                LoadGrid();

                Session["ddlSystem"] = ddlChangeASOP.SelectedValue;
            }
        }

        //links to details page with id of job
        protected void dgrPublication_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "edit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("PublicationToBusinessUnitDetails.aspx?id=" + e.CommandArgument + "&system=" + ddlChangeASOP.SelectedValue);

                    break;
            }
        }

        //apply filters
        protected void btnSearch_click(object sender, EventArgs e)
        {
            LoadGrid();
        }

        //clear filters and reload grid
        protected void btnFilter_click(object sender, EventArgs e)
        {
            txtTitleCardFilter.Text = "";
            txtDescriptionFilter.Text = "";
            txtBusinessUnit.Text = "";
            txtBUDescriptionFilter.Text = "";

            LoadGrid();
        }

        protected void dgrPublication_PageIndexChanged(object sender, EventArgs e)
        {
        }

        protected void dgrPublication_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrPublicationToBusinessUnits.PageIndex = e.NewPageIndex;
            LoadGrid();

        }

        //reloads grid with chosen data
        protected void ddlASOPSystemSelect(object sender, EventArgs e)
        {
            string system = ddlChangeASOP.SelectedValue;
            Session["ddlSystem"] = system;

            Response.Redirect("PublicationToBusinessUnits.aspx?id=" + ddlChangeASOP.SelectedValue);
        }


        #endregion
    }
}