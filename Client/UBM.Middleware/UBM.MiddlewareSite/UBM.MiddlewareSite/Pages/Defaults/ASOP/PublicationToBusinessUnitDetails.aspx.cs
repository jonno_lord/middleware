﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class PublicationToBusinessUnitDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;
        private string connectionName;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int id
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //populates form on edit action with job data
        private void GetPublicationToBusinessUnit()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_PublicationToBusinessUnit", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", id);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlTitleCard.Items.Add(sdr["Title_Short_Name"] + " - " + sdr["Title_Card"]);
                    ddlTitleCard.Enabled = false;
                    ddlBusinessUnit.SelectedValue = sdr["Business_Unit"].ToString();
                    ddlCollectionClient.SelectedValue = sdr["Collection_Manager_Client"].ToString();
                    ddlCollectionAgent.SelectedValue = sdr["Collection_Manager_Agent"].ToString();
                    ddlMonthlyWeekly.SelectedValue = sdr["Monthly_Weekly"].ToString();
                    ddlTaxCompany.SelectedValue = sdr["Tax_Company_Code"].ToString();
                    txtSalesAdminName.Text = sdr["Sales_admin_Name"].ToString();
                    txtSalesAdminPhone.Text = sdr["Sales_admin_tel"].ToString();
                    txtAdminFax.Text = sdr["Sales_admin_fax"].ToString();
                    txtAdminEmail.Text = sdr["Sales_admin_email"].ToString();

                }
            }
        }

        private void GetBusinessUnits()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_BusinessUnit", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();
                while (sdr.Read())
                {
                    ListItem itmBusinessUnit = new ListItem();
                    itmBusinessUnit.Value = sdr["BUSINESSUNIT"].ToString();
                    itmBusinessUnit.Text = sdr["BUSINESSUNITDESCRIPTION"].ToString();

                    ddlBusinessUnit.Items.Add(itmBusinessUnit);

                }
            }
        }

        private void GetTitleCards()
        {
            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_TitleCard", connection);
                command.CommandType = CommandType.StoredProcedure;

                  SqlDataReader sdr = command.ExecuteReader();
                  while (sdr.Read())
                  {
                      ListItem itmPublication = new ListItem();
                      itmPublication.Value = sdr["SHORTNAME"].ToString();
                      itmPublication.Text = sdr["TITLECARDDESCRIPTION"].ToString();

                      ddlTitleCard.Items.Add(itmPublication);
                  }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
             if (Request.QueryString["system"] != null)
                {
                 selectedValue = int.Parse(Request.QueryString["system"]);
                }
            else
            {
                if (!String.IsNullOrEmpty(Session["ddlSystem"].ToString()))
                {
                    selectedValue = int.Parse(Session["ddlSystem"].ToString());
                }
               
            }
                CommonButtons.Cancelid = selectedValue;

        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {
            if (Request.QueryString["system"] != null)
            {
                asopType = Request.QueryString["system"];
            }
            else
            {
                try
                {
                    if (!String.IsNullOrEmpty(Session["ddlSystem"].ToString()))
                    {
                        asopType = Session["ddlSystem"].ToString();
                    }
                }
                catch(Exception exception)
                {
                    Response.Redirect("PublicationToBusinessUnits.aspx");
                }

            }

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Publication To Business Units";
                    connectionName = "MiddlewareASOPDaltons";
                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Publication To Business Units";
                    connectionName = "MiddlewareASOPHolland";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Publication To Business Units";
                    connectionName = "MiddlewareASOPUK";

                    break;
            }
        }

        private void GetCollectionNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_CollectionNames", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();
                while (sdr.Read())
                {
                    ListItem itm= new ListItem();

                    string value = sdr["LogonName"].ToString();
                    itm.Value = value.Trim();
                    itm.Text = value.Trim();

                    ListItem itmAgent = new ListItem();
                    itmAgent.Value = value.Trim();
                    itmAgent.Text = value.Trim();


                    ddlCollectionClient.Items.Add(itm);
                    ddlCollectionAgent.Items.Add(itmAgent);
                }
            }
        }

        private void SetTaxCompanies()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_TaxCompanies", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    ListItem itm = new ListItem();

                    itm.Value = sdr["Tax_Company_Code"].ToString();
                    itm.Text = sdr["Tax_Company_Code"].ToString();

                    ddlTaxCompany.Items.Add(itm);
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            CommonButtons.ucDeleteEntity_Click += btnDeleteEntity_Click;

            if (!IsPostBack)
            {
                determineASOPVersion();
                GetCollectionNames();
                SetTaxCompanies();
                GetBusinessUnits();

                if (id > 0)
                    GetPublicationToBusinessUnit();
                else
                    GetTitleCards();


            }
        }

        //called by button control to save job and redirect page
        protected void btnDeleteEntity_Click(object sender, EventArgs e)
        {
            determineASOPVersion();
            CommonButtons.ConnectionString = connectionName;
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("PublicationToBusinessUnits.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Updates customer types  and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_PublicationToBusinessUnit", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@id", id);
                command.Parameters.AddWithValue("@businessUnit", ddlBusinessUnit.SelectedValue);
                command.Parameters.AddWithValue("@publication", ddlTitleCard.SelectedValue);
                command.Parameters.AddWithValue("@collectionManager", ddlCollectionClient.SelectedValue);
                command.Parameters.AddWithValue("@collectionAgent", ddlCollectionAgent.SelectedValue);
                command.Parameters.AddWithValue("@monthlyWeekly", ddlMonthlyWeekly.SelectedValue);
                command.Parameters.AddWithValue("@taxCompany", ddlTaxCompany.SelectedValue);
                command.Parameters.AddWithValue("@salesAdminName", txtSalesAdminName.Text);
                command.Parameters.AddWithValue("@salesAdminPhone", txtSalesAdminPhone.Text);
                command.Parameters.AddWithValue("@adminFax", txtAdminFax.Text);
                command.Parameters.AddWithValue("@adminEmail", txtAdminEmail.Text);

                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}