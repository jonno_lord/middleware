﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Defaults.ASOP.ValidSummaryCards" Codebehind="ValidSummaryCards.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
     <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Summary Card"  Title="MW ASOP"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
 <div class="filter">
            <div class="rowA">
                <p>ASOP:</p>
            </div>
            <div class="rowB">
                <asp:DropDownList id="ddlChangeASOP" runat="server" tooltip="Version of ASOP" OnSelectedIndexChanged="ddlASOPSystemSelect" AutoPostBack="True">
                    <asp:ListItem Text="ASOP Daltons" Value="1"/>
                    <asp:ListItem Text="ASOP Holland" Value="2"/>
                    <asp:ListItem Text="ASOP UK" Value="3"/>
                </asp:DropDownList>
            </div>
      </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Summary Cards" TableName="CustomerTypes" DisableEnable="false" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
<asp:UpdatePanel ID="updHeader" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
<asp:UpdatePanel ID="updSummaryCardsASOP" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:UpdateProgress ID="uppSummaryCards" runat="server" DisplayAfter="5">
            <ProgressTemplate>
                    <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptSummaryCard_ItemCommand" OnItemDataBound="rptSummaryCards_ItemDataBound">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Summary Card</th>
                        <th>Short Code</th>
                        <th>JDE Object Code</th>
                        <th>JDE Subsidiary Code</th>
                        <th>Valid</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("Valid_Summary_Card_Id") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip="Edit Valid Summary Card "/></td>
                        <td class="GridCentre"><%# Eval("Summary_Card") %></td>
                        <td class="GridCentre"><%# Eval("Short_Code") %></td>
                        <td class="GridCentre"><%# Eval("JDE_Object_Code") %></td>
                        <td class="GridCentre"><%# Eval("JDE_Subsidiary_Code") %></td>
                        <td class="GridCentre" style="width:20px;">           
                              <asp:CheckBox id="cbxDisabled" runat="server" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"Valid"))%>'/>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>
</asp:Content>

