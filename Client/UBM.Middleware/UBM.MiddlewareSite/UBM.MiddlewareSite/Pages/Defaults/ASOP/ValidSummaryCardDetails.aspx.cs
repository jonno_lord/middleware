﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.ASOP
{
    public partial class ValidSummaryCardDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;
        private string asopType;
        private string connectionString;
        private string connectionName;

        private static string connectionStringUK
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPUKConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringDaltons
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPDaltonsConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private static string connectionStringHolland
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareASOPHollandConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int cardid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetSummaryCards()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_ValidSummaryCard", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", cardid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtSummaryCard.Text = sdr["Summary_Card"].ToString();
                    txtShortCode.Text = sdr["Short_Code"].ToString();
                    txtJDEObjectCode.Text = sdr["JDE_Object_Code"].ToString();
                    txtJDESubsidiaryCode.Text = sdr["JDE_Subsidiary_Code"].ToString();
                    cbxValid.Checked = Convert.ToBoolean(sdr["Valid"]);
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = int.Parse(Request.QueryString["system"]);
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        //sets whether page is for ASOP UK, ASOP Daltons or ASOP Holland
        private void determineASOPVersion()
        {
            asopType = Request.QueryString["system"];

            switch (asopType)
            {
                case "1":
                    connectionString = connectionStringDaltons;
                    lblAsopHeader.Text = "Middleware ASOP Daltons Publication To Business Units";
                    connectionName = "MiddlewareASOPDaltons";
                    break;
                case "2":
                    connectionString = connectionStringHolland;
                    lblAsopHeader.Text = "Middleware ASOP Holland Publication To Business Units";
                    connectionName = "MiddlewareASOPHolland";

                    break;
                case "3":
                    connectionString = connectionStringUK;
                    lblAsopHeader.Text = "Middleware ASOP UK Publication To Business Units";
                    connectionName = "MiddlewareASOPUK";

                    break;
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            CommonButtons.ucDeleteEntity_Click += btnDeleteEntity_Click;

            if (!IsPostBack)
            {
                //prevent add page from being accessed
                if (cardid == 0)
                {
                    Response.Redirect("ValidSummaryCards.aspx");
                }

                determineASOPVersion();
                GetSummaryCards();

                //prevents edits taking place
                txtSummaryCard.Enabled = false;
                txtShortCode.Enabled = false;
                cbxValid.Enabled = false;

                //MUST be set to disable deletion button
                CommonButtons.deleteEnabled = false;

            }
        }

        //called by button control to save job and redirect page
        protected void btnDeleteEntity_Click(object sender, EventArgs e)
        {
            determineASOPVersion();
            CommonButtons.ConnectionString = connectionName;

            //filters results by system of job being created or edited
            Response.Redirect("ValidSummaryCards.aspx?id=" + asopType);
        }

        //called by button control to save summary card and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("ValidSummaryCards.aspx?id=" + asopType);
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Updates valid summary cards and is invoked by the common buttons user control 
        ///   on the btnsubmit control
        /// </summary>
        public void SaveEntity()
        {
            determineASOPVersion();

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_UPD_ValidSummaryCard", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@id", cardid);
                command.Parameters.AddWithValue("@objectCode", txtJDEObjectCode.Text);
                command.Parameters.AddWithValue("@subsidiaryCode", txtJDESubsidiaryCode.Text);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}