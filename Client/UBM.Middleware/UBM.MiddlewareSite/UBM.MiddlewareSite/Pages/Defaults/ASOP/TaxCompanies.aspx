﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeBehind="TaxCompanies.aspx.cs" Inherits="Pages.Defaults.ASOP.TaxCompany" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" runat="server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Tax Company"  Title="MW ASOP" AddEntityURL="TaxCompanyDetails.aspx" QueryStringFilter="Tax_Company_ID"/>  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" runat="server">
<div class="filter">
            <div class="rowA">
                <p>ASOP:</p>
            </div>
            <div class="rowB">
                <asp:DropDownList id="ddlChangeASOP" runat="server" tooltip="Version of ASOP" OnSelectedIndexChanged="ddlASOPSystemSelect" AutoPostBack="True">
                    <asp:ListItem Text="ASOP Daltons" Value="1"/>
                    <asp:ListItem Text="ASOP Holland" Value="2"/>
                    <asp:ListItem Text="ASOP UK" Value="3"/>
                </asp:DropDownList>
            </div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" runat="server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Tax Companies" TableName="TaxCompany" DisableEnable="false"/>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" runat="server">
<asp:UpdatePanel ID="updHeader" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:label ID="lblAsopHeader" runat="server" CssClass="asopHeaderLabel" />
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" runat="server">
<asp:UpdatePanel ID="updTaxCompany" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:UpdateProgress ID="uppTaxCompany" runat="server" DisplayAfter="5">
            <ProgressTemplate>
                    <div class="progressIndicator"><asp:image id="animatedLogo" runat="server" ImageUrl="~/Images/loading.gif" cssclass="buttonALignLeft"/> Loading...</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptTaxCompany_ItemCommand">
        <HeaderTemplate>
            <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>Edit</th>
                <th>Tax Company Code</th>
                <th>Name</th>
                <th>VAT Registration Number</th>
                <th>Address Line 1</th>
                <th>Address Line 2</th>
                <th>Town</th>
                <th>Postcode</th>
                <th>County</th>
                <th>Country</th>
            </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit"  runat="server" ImageUrl="~/images/AddIcon.png" CommandArgument='<%# Eval("Tax_Company_ID") %>'/></td>
                <td class="GridCentre"><%# Eval("Tax_Company_Code") %></td>
                <td><%# Eval("Entity_Name")%></td>
                <td><%# Eval("Entity_VAT_Reg_no")%></td>
                <td><%# Eval("Address_1")%></td>
                <td><%# Eval("Address_2")%></td>
                <td><%# Eval("Town")%></td>
                <td><%# Eval("Postcode")%></td>
                <td><%# Eval("County")%></td>
                <td><%# Eval("Country")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript" src=" <%=VirtualPathUtility.ToAbsolute("~/Javascript/RebindJQuery.js") %> "></script>
</asp:Content>
