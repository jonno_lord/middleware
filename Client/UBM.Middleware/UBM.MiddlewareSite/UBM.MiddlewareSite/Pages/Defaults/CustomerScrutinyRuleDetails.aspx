﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.CustomerScrutinyRuleDetails" Codebehind="CustomerScrutinyRuleDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>Customer Scrutiny Rules</h1>
</div>
<div class="validationSummary">
    <div class="margin">
        <p><asp:label ID="lblMessage" runat="server" class="formMargin" /></p>
    </div>
    <asp:ValidationSummary ID="vdsScruntiyRules" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEntryForm">

    <div class="formRow">
            <asp:Label ID="lblDescriptionEnter" CssClass="formMargin labelInput" runat="server" Text="Description:" Width="130px"/>
            <asp:TextBox ID="txtDescription" CssClass="formMargin" runat="server" ToolTip="Description of Scrutiny Rule" TextMode="MultiLine" Columns="39" Rows="4" />
        <asp:RegularExpressionValidator ID="revDescription" ControlToValidate="txtDescription" ErrorMessage="Description is exceeding 1024 characters" ValidationExpression="^[\s\S]{0,1024}$" runat="server" >
        *
        </asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="rfvDescription" runat="server"  ControlToValidate="txtDescription" ErrorMessage="Description is required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
            <asp:Label ID="lblReturnsViolationEnter" CssClass="formMargin labelInput" runat="server" Text="Returns Violation:" Width="130px"/>
            <asp:CheckBox ID="cbxReturnsViolation" CssClass="formMargin  checkboxLayout" runat="server" ToolTip="Enable Return Violation" Checked="false" Height="12px"/>
    </div>

    <div class="formRow">
            <asp:Label ID="lblReturnsDuplicatesEnter" CssClass="formMargin labelInput" runat="server" Text="Returns Duplicates:" Width="130px"/>
            <asp:CheckBox ID="cbxReturnsDuplicates" CssClass="formMargin  checkboxLayout" runat="server" ToolTip="Enable Returns Duplicates" Checked="false" Height="12px"/>
    </div>

    <div class="formRow">
            <asp:Label ID="lblQuerySystemidEnter" CssClass="formMargin labelInput" runat="server" Text="System:" Width="130px"/>
            <asp:DropDownList ID="ddlQuerySystemid" CssClass="formMargin" runat="server" DataTextField="ShortName" DataValueField="id" ToolTip="Select the System the Scrutiny Rule is for" width="250"  />
        <asp:RequiredFieldValidator ID="rfvQuerySystemid" runat="server" ControlToValidate="ddlQuerySystemid" ErrorMessage="System required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
            <asp:Label ID="lblIsQueryBased" CssClass="formMargin labelInput" runat="server" Text="Is Query Based?" Width="130px" />
            <asp:CheckBox ID="cbxIsQueryBased" CssClass="FormMargin checkboxLayout" runat="server" ToolTip="Is Rule Query Based?" Checked="true" Height="12px" autopostback="true" OnCheckedChanged="cbxIsQueryBased_Checked" />
    </div>

     <div class="formRow">
            <asp:Label ID="lblDisabled" CssClass="formMargin labelInput" runat="server" Text="Disabled:" Width="130px"/>
            <asp:CheckBox ID="cbxDisabled" CssClass="formMargin  checkboxLayout" runat="server" ToolTip="Enable or Disable Scrutiny Rule" Checked="false" Height="12px"/>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEntryForm">

    <asp:Panel ID="pnlQueryPanel" runat="server">
        <div class="formRow">
            <asp:Label ID="lblQuery" CssClass="formMargin labelInput" runat="server" Text="Query:" Width="130px" /> 
            <asp:TextBox ID="txtQuery" CssClass="formMargin" runat="server" ToolTip="Query of Scrutiny Rule" TextMode="MultiLine" Columns="75" Rows="15"/>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlClassPanel" runat="server">
        <div class="formRow">
            <asp:Label ID="lblAssemblyName" CssClass="formMargin labelInput" runat="server" Text="Assembly Name:" Width="130px" /> 
            <asp:TextBox ID="txtAssemblyName" CssClass="formMargin" runat="server" ToolTip="Assembly Name for Rule" MaxLength="250" Width="250"/>
        </div>

         <div class="formRow">
            <asp:Label ID="lblClass" CssClass="formMargin labelInput" runat="server" Text="Class Name:" Width="130px" /> 
            <asp:TextBox ID="txtClassName" CssClass="formMargin" runat="server" ToolTip="Class Name for Rule" MaxLength="250" Width="250"/>
        </div>
    </asp:Panel>

</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Rule" EntityURL="CustomerScrutinyRules.aspx" deleteEnabled="true" deleteSPName="CustomerScrutinyRules" deleteParameter="customerScrutinyRule"/>
</asp:Content>

