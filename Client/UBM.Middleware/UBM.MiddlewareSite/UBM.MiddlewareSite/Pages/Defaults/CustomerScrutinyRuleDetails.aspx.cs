﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults
{
    public partial class CustomerScrutinyRuleDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private bool storedProcExists;

        private static string systemString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int customerScrutinyid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        private int systemid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["systemid"], out returnValue);
                return returnValue;
            }
        }

        private string systemName;

        private string SystemName
        {
            get
            {
                string returnValue = Request.QueryString["systemName"];
                return returnValue;
            }
        }

        private string ConnectionString
        {
            get
            {
                if (!IsPostBack)
                {
                    systemName = SystemName;
                }
                string unchangedString = WebConfigurationManager.ConnectionStrings[systemName + "ConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        ///   Gets the schedule data for a specified scheduleid as selected from the schedule page and display
        ///   in the form controls. Also populates the drop down lists using methods GetJobNames and GetSystemNames.
        /// </summary>
        private void GetScrutinyRule()
        {
            GetSystemNames();

            if (storedProcedureExists("usp_GET_CustomerScrutinyRule"))
            {
                using (connection = new SqlConnection(ConnectionString))
                {

                    connection.Open();
                    SqlCommand command = new SqlCommand("usp_GET_CustomerScrutinyRule", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@customerScrutinyRuleid", customerScrutinyid);

                    SqlDataReader sdr = command.ExecuteReader();
                    if (sdr.Read())
                    {
                        txtDescription.Text = sdr["Description"].ToString();
                        cbxReturnsViolation.Checked = (bool)sdr["ReturnsViolation"];
                        cbxReturnsDuplicates.Checked = (bool)sdr["ReturnsDuplicates"];
                        ddlQuerySystemid.SelectedValue = sdr["QuerySystemid"].ToString();
                        cbxIsQueryBased.Checked = (bool)sdr["IsQueryBased"];
                        txtQuery.Text = sdr["Query"].ToString();
                        txtAssemblyName.Text = sdr["AssemblyName"].ToString();
                        txtClassName.Text = sdr["ClassName"].ToString();
                        cbxDisabled.Checked = (bool)sdr["Disabled"];

                    }
                }
            }
            else
            {
                lblMessage.Text = "No stored procedure exists for retrieving records";
                lblMessage.ForeColor = Color.Red;
            }
        }

        private void GetSystemNames()
        {

            using (connection = new SqlConnection(systemString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_Systems", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrSystems = command.ExecuteReader();

                while (sdrSystems.Read())
                {
                    ListItem itmSystem = new ListItem();
                    itmSystem.Value = sdrSystems["id"].ToString();
                    itmSystem.Text = sdrSystems["ShortName"].ToString();

                    ddlQuerySystemid.Items.Add(itmSystem);
                }

            }

        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                //todo
                selectedValue = Convert.ToInt32(systemid);
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow rule to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            //sets connectionstring for deletion

            CommonButtons.ConnectionString = SystemName;

            if (!IsPostBack)
            {
                if (customerScrutinyid != 0)
                {
                    GetScrutinyRule();
                    //set connectionstring for save
                }
                else
                {
                    GetSystemNames();
                    if (Session["scrutinySystem"] != null)
                    {
                        foreach (ListItem item in ddlQuerySystemid.Items)
                        {
                            string system = Session["scrutinySystem"].ToString();
                            if (item.Text == system)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }

                //checks IsQueryBased checkbox on Load for display of correct input boxes
                cbxIsQueryBased_Checked(sender, e);
            }

        }

        //shows either query box or classname/assemblyname boxes based on checkbox checked
        protected void cbxIsQueryBased_Checked(object sender, EventArgs e)
        {
            if (cbxIsQueryBased.Checked)
            {
                pnlQueryPanel.Visible = true;
                pnlClassPanel.Visible = false;

            }
            else
            {
                pnlQueryPanel.Visible = false;
                pnlClassPanel.Visible = true;
            }

        }


        /// <summary>
        ///   Checks whether stored procedure exists in selected system database, if so then there are Customer scrutiny rules to be viewed
        /// </summary>
        /// <param name = "storedProcedure"></param>
        /// <returns>bool that states whether stpred procedure exists</returns>
        protected bool storedProcedureExists(string storedProcedure)
        {
            using (connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='" + storedProcedure + "'", connection);
                SqlDataReader sdrSpExists = command.ExecuteReader();

                if (sdrSpExists.Read())
                    storedProcExists = true;
                return storedProcExists;
            }
        }

        //called by button control to save scrutiny rule and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            systemName = ddlQuerySystemid.SelectedItem.Text;
            if (storedProcedureExists("usp_SAV_CustomerScrutinyRule"))
            {
                SaveEntity();

                Response.Redirect("CustomerScrutinyRules.aspx?id=" + systemid);
            }
            else
            {
                lblMessage.Text = "Cannot save customer scrutiny details on this system";
                lblMessage.ForeColor = Color.Red;
            }
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves customer scrutiny rules to selected datbase via user control Common Buttons
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_CustomerScrutinyRule", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@customerScrutinyRuleid", customerScrutinyid);
                command.Parameters.AddWithValue("@customerScrutinyRuleDescription", txtDescription.Text);
                command.Parameters.AddWithValue("@customerScrutinyRuleReturnsViolation", cbxReturnsViolation.Checked);
                command.Parameters.AddWithValue("@customerScrutinyRuleReturnsDuplicates", cbxReturnsDuplicates.Checked);
                command.Parameters.AddWithValue("@customerScrutinyRuleQuerySystemid", ddlQuerySystemid.SelectedValue);
                command.Parameters.AddWithValue("@customerScrutinyRuleIsQueryBased", cbxIsQueryBased.Checked);
                command.Parameters.AddWithValue("@customerScrutinyRuleQuery", txtQuery.Text);
                command.Parameters.AddWithValue("@customerScrutinyRuleAssemblyName", txtAssemblyName.Text);
                command.Parameters.AddWithValue("@customerScrutinyRuleClassName", txtClassName.Text);
                command.Parameters.AddWithValue("@customerScrutinyRuleDisabled", cbxDisabled.Checked);

                command.ExecuteNonQuery();

            }
        }
        #endregion
    }
}