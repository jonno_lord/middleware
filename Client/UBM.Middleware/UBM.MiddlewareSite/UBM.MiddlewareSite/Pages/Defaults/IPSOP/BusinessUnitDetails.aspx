﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.IPSOP.BusinessUnitDetails" Codebehind="BusinessUnitDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>IPSOP Business Units</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsBusinessUnits" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">

    <div class="formRow">
           <asp:Label ID="lblBusinessUnit" runat="server" cssClass="formMargin labelInput" Text="Business Unit:" Width="100px"/>
           <asp:DropDownList ID="ddlBusinessUnit" runat="server" cssClass="formMargin" DataTextField="Business_Unit" DataValueField="Business_Unit" ToolTip="Business Unit and Name" Width="300">
           </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvBusinessUnit" runat="server"  ControlToValidate="ddlBusinessUnit" ErrorMessage="Business Unit number required">
        *
        </asp:RequiredFieldValidator>
    </div>

     <div class="formRow">
            <asp:Label ID="lblAdminContactName" runat="server" cssClass="formMargin labelInput" Text="Admin Contact Name:" Width="100px"/>
            <asp:TextBox ID="txtAdminContactName" runat="server" cssClass="formMargin" ToolTip="Contact Name of admin" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvAdminContactName" runat="server" ControlToValidate="txtAdminContactName" ErrorMessage="Admin Contact Name Required">
        *
        </asp:RequiredFieldValidator>
    </div> 
    
     <div class="formRow">
            <asp:Label ID="lblAdminContactNumber" runat="server" cssClass="formMargin labelInput" Text="Admin Contact Number:" Width="100px"/>
            <asp:TextBox ID="txtAdminContactNumber" runat="server" cssClass="formMargin" ToolTip="Contact extention number of admin" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvAdminContactNumber" runat="server" ControlToValidate="txtAdminContactNumber" ErrorMessage="Admin Contact Number Required">
        *
        </asp:RequiredFieldValidator>
    </div>     

</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
<div class="addEditForm">

 <div class="formRow">
        <asp:Label ID="lblCollectionManagerClient" runat="server" cssClass="formMargin labelInput" Text="Collection Manager Client:" Width="100px"/>
        <asp:TextBox ID="txtCollectionManagerClient" runat="server" cssClass="formMargin" ToolTip="Collection Manager Client" MaxLength="50" Width="250"/>        
        <asp:RequiredFieldValidator ID="rfvCollectionManagerClient" runat="server" ControlToValidate="txtCollectionManagerClient" ErrorMessage="Collection Manager Client Required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
           <asp:Label ID="lblCollectionManagerAgent" runat="server" cssClass="formMargin labelInput" Text="Collection Manager Agent:" Width="100px"/>
           <asp:TextBox ID="txtManagerCollectionAgent" runat="server" cssClass="formMargin" ToolTip="Collection Manager Agent" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvCollectionManagerAgent" runat="server" ControlToValidate="txtManagerCollectionAgent" ErrorMessage="Collection Manager Agent Required">
        *
        </asp:RequiredFieldValidator>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
     <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Business Unit" EntityURL="BusinessUnits.aspx" ConnectionString="MiddlewareIPSOP" deleteEnabled="true" deleteSPName="BusinessUnitMapping" deleteParameter="BUMapping"/>
</asp:Content>

