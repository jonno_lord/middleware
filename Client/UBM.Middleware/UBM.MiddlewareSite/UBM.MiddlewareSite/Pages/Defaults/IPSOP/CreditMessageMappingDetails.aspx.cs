﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.IPSOP
{
    public partial class CreditMessageMappingDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareIPSOPConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int CMMappingid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetCreditMessageMappingDetails()
        {
            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_CreditMessageMapping", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@CMMappingid", CMMappingid);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtCreditMessageCode.Text = sdr["Credit_Message_Code"].ToString();
                    txtCreditMessageDescription.Text = sdr["Credit_Message_Description"].ToString();
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow Credit Message to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (CMMappingid != 0)
                {
                    GetCreditMessageMappingDetails();
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("CreditMessageMapping.aspx");
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   saves credit message mapping to IPSOp database via the user control CommonButtons
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_CreditMessageMapping", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@CMMappingid", CMMappingid);
                command.Parameters.AddWithValue("@CreditMessageCode", txtCreditMessageCode.Text);
                command.Parameters.AddWithValue("@CreditMessageDescription", txtCreditMessageDescription.Text);

                command.ExecuteNonQuery();

            }

        }

        #endregion
    }
}