﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.IPSOP
{
    public partial class BusinessUnitDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareIPSOPConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        /// <summary>
        ///   this property returns 0 if no id has been specifed, else the QueryString integer value 
        ///   is returned (on the id field part of the uri - e.g. /page.aspx?id=1212)
        /// </summary>
        private int BusinessUnitMappingid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        private int BusinessUnit
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["BusinessUnit"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        //retrieves data to populate form from specified id
        private void GetBusinessUnitDetails()
        {
            GetBusinessUnitNames();

            using (connection = new SqlConnection(connectionString))
            {

                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_BusinessUnitMapping", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@BUMappingid", BusinessUnitMappingid);
                command.Parameters.AddWithValue("@BusinessUnit", BusinessUnit);

                //-- executes the command - and reads in the first row into the DataReader object
                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    ddlBusinessUnit.SelectedValue = sdr["Business_Unit"].ToString();
                    txtCollectionManagerClient.Text = sdr["Collection_Manager_Client"].ToString();
                    txtManagerCollectionAgent.Text = sdr["Collection_Manager_Agent"].ToString();
                    txtAdminContactName.Text = sdr["Admin_Contact_Name"].ToString();
                    txtAdminContactNumber.Text = sdr["Admin_Contact_Number"].ToString();
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        // gets Business Units and its description/name to populate dropdown
        private void GetBusinessUnitNames()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_BusinessUnits", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdrBusinessUnit = command.ExecuteReader();
                while (sdrBusinessUnit.Read())
                {
                    ListItem itmBusinessUnit = new ListItem();
                    itmBusinessUnit.Value = sdrBusinessUnit["Business_Unit"].ToString();
                    itmBusinessUnit.Text = sdrBusinessUnit["Business_Unit"] + " - " + sdrBusinessUnit["Description"];

                    ddlBusinessUnit.Items.Add(itmBusinessUnit);
                }
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow Business Unit to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (BusinessUnitMappingid != 0)
                {
                    GetBusinessUnitDetails();
                }

                GetBusinessUnitNames();
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("BusinessUnits.aspx");
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves business unit mappings to IPSOP database via user control Common Buttons
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_BusinessUnitMapping", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@BUMappingid", BusinessUnitMappingid);
                command.Parameters.AddWithValue("@BusinessUnit", ddlBusinessUnit.SelectedValue);
                command.Parameters.AddWithValue("@CollectionClient", txtCollectionManagerClient.Text);
                command.Parameters.AddWithValue("@CollectionAgent", txtManagerCollectionAgent.Text);
                command.Parameters.AddWithValue("@AdminContactName", txtAdminContactName.Text);
                command.Parameters.AddWithValue("@AdminContactNumber", txtAdminContactNumber.Text);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}