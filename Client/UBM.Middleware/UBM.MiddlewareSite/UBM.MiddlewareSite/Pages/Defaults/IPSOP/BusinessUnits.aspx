﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Defaults.IPSOP.BusinessUnits" Codebehind="BusinessUnits.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
    <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="Business Unit"  Title="MW IPSOP" AddEntityURL="BusinessUnitDetails.aspx"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
    <uc:CommonActions ID="CommonActions" runat="server" EntityName="Business Units" TableName="BusinessUnits" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
    <h1>IPSOP Business Units</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">
 <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptBusinessUnit_ItemCommand">
                <HeaderTemplate>
                    <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                    <tr>
                        <th>Edit</th>
                        <th>Business Unit</th>
                        <th>Business Unit Name</th>
                        <th>Collection Manager Client</th>
                        <th>Collection Manager Agent</th>
                        <th>Admin Contact Name</th>
                        <th>Admin Contact Number</th>
                    </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                      <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit" CommandArgument='<%# Eval("id") + "&BusinessUnit=" + Eval("Business_Unit") %>' runat="server" ImageUrl="~/images/AddIcon.png" ToolTip='<%# "Edit Business Unit " +Eval("Business_Unit")%>'/></td>
                      <td class="GridCentre"><%# Eval("Business_Unit") %></td>
                      <td class="GridCentre"><%# Eval("Description") %></td>
                      <td class="GridCentre"><%# Eval("Collection_Manager_Client") %></td>
                      <td class="GridCentre"> <%# Eval("Collection_Manager_Agent")%></td>
                      <td class="GridCentre"><%# Eval("Admin_Contact_Name") %></td>
                      <td class="GridCentre"><%# Eval("Admin_Contact_Number") %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
</asp:Content>

