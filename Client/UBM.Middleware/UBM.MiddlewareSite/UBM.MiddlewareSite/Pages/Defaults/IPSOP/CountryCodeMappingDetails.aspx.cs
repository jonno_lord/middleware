﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using WebsiteFunctions;

namespace Pages.Defaults.IPSOP
{
    public partial class CountryCodeMappingDetails : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareIPSOPConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private int CCMappingid
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetCountryCodeMappingDetails()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_CountryCodeMapping", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@CCMappingid", CCMappingid);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtCountryName.Text = sdr["Country_Name"].ToString();
                    txtCountryCode.Text = sdr["Country_Code"].ToString();
                }
            }
        }

        // should be set in each screen depending on what value you want the cancel redirect to filter on.
        private void setCancelRedirect()
        {
            //allows buttons user control to access systemid value
            //  setting to 0 will represent no filtering
            int selectedValue = 0;
            try
            {
                CommonButtons.Cancelid = selectedValue;
            }
            catch
            {
                CommonButtons.Cancelid = selectedValue;
            }

        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //allow job to be saved from button user control via method in this cs file
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;

            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;

            if (!IsPostBack)
            {
                if (CCMappingid != 0)
                {
                    GetCountryCodeMappingDetails();
                }
            }
        }

        //called by button control to save job and redirect page
        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveEntity();

            //filters results by system of job being created or edited
            Response.Redirect("CountryCodeMapping.aspx");
        }

        //gives value to filter jobs page by on cancel, on edit action
        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            setCancelRedirect();
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Saves country code mappings to IPSOP database via user control Common Buttons
        /// </summary>
        public void SaveEntity()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_CountryCodeMapping", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@CCMappingid", CCMappingid);
                command.Parameters.AddWithValue("@CountryName", txtCountryName.Text);
                command.Parameters.AddWithValue("@CountryCode", txtCountryCode.Text);

                command.ExecuteNonQuery();

            }

        }
        #endregion
    }
}