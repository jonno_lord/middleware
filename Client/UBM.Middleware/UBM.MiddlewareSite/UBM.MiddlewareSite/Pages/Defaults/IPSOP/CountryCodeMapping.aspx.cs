﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.IPSOP
{
    public partial class CountryCodeMapping : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["MiddlewareIPSOPConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadGrid()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();


                SqlCommand command = new SqlCommand("usp_SEL_CountryCodeMappings", connection);
                command.CommandType = CommandType.StoredProcedure;

                rptGrid.DataSource = command.ExecuteReader();
                rptGrid.DataBind();
            }
        }

        #endregion

        #region Protected Methods and Events


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }

        //links to details page with id of job
        protected void rptCountryCodeMapping_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("CountryCodeMappingDetails.aspx?id=" + e.CommandArgument);

                    break;
            }
        }

        #endregion
    }
}