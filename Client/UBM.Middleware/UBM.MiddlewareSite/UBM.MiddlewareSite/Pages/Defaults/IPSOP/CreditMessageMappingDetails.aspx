﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.IPSOP.CreditMessageMappingDetails" Codebehind="CreditMessageMappingDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<div class="buttonAlignLeft">
    <h1>IPSOP Credit Message Mapping</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsBusinessUnits" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
<div class="addEditForm">
    
    <div class="formRow">
           <asp:Label ID="lblCreditMessageCode" runat="server" cssClass="formMargin labelInput" Text="Credit Message Code:" Width="100px"/>
           <asp:TextBox ID="txtCreditMessageCode" runat="server" cssClass="formMargin" ToolTip="Code of Credit Message" MaxLength="10" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvCreditMessageCode" runat="server"  ControlToValidate="txtCreditMessageCode" ErrorMessage="Business Unit number required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
        <asp:Label ID="lblCreditMessageDescription" runat="server" cssClass="formMargin labelInput" Text="Collection Manager Client:" Width="100px"/>
        <asp:TextBox ID="txtCreditMessageDescription" runat="server" cssClass="formMargin" ToolTip="Collection Manager Client" TextMode="MultiLine" Columns="40" Rows="2"/>
        <asp:RegularExpressionValidator ID="txtLengthValidator" ControlToValidate="txtCreditMessageDescription" ErrorMessage="Description is exceeding 255 characters" ValidationExpression="^[\s\S]{0,255}$" runat="server" >
        *
        </asp:RegularExpressionValidator>  
        <asp:RequiredFieldValidator ID="rfvCreditMessageDescription" runat="server" ControlToValidate="txtCreditMessageDescription" ErrorMessage="Credit Message Mapping Description Required">
        *
        </asp:RequiredFieldValidator>

    </div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
         <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Mapping" EntityURL="CreditMessageMapping.aspx" ConnectionString="MiddlewareIPSOP" deleteEnabled="true" deleteSPName="CreditMessageMapping" deleteParameter="CMMapping"/>
</asp:Content>

