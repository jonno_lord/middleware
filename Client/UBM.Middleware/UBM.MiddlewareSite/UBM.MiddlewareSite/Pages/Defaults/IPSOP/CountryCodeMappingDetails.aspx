﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.IPSOP.CountryCodeMappingDetails" Codebehind="CountryCodeMappingDetails.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    <div class="buttonAlignLeft">
    <h1>IPSOP Country Code Mapping</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsCountryCodeMapping" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" Runat="Server">
    <div class="addEditForm">

         <div class="formRow">
           <asp:Label ID="lblCountryName" runat="server" cssClass="formMargin labelInput" Text="Country Name:" Width="100px"/>
           <asp:TextBox ID="txtCountryName" runat="server" cssClass="formMargin" ToolTip="Name of the Country" MaxLength="50" Width="250"/>
        <asp:RequiredFieldValidator ID="rfvCountryName" runat="server"  ControlToValidate="txtCountryName" ErrorMessage="Country name required">
        *
        </asp:RequiredFieldValidator>
    </div>

    <div class="formRow">
        <asp:Label ID="lblCountryCode" runat="server" cssClass="formMargin labelInput" Text="Country Code:" Width="100px"/>
        <asp:TextBox ID="txtCountryCode" runat="server" cssClass="formMargin" ToolTip="Valid County Code" MaxLength="5" Width="250"/> 
        <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtCountryCode" ErrorMessage="Country Code Required">
        *
        </asp:RequiredFieldValidator>
    </div>

    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="rightForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" Runat="Server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Mapping" EntityURL="CountryCodeMapping.aspx" ConnectionString="MiddlewareIPSOP" deleteEnabled="true" deleteSPName="CountryCodeMapping" deleteParameter="CCMapping"/>
</asp:Content>

