﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" Inherits="Pages.Defaults.N200.BadgeAllocations" Codebehind="BadgeAllocations.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonActions" Src="~/UserControls/CommonActions.ascx" %>
<%@ Register TagPrefix="uc" TagName="SummaryActions" Src="~/UserControls/CommonSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SummaryActions" Runat="Server">
     <uc:SummaryActions ID="SummaryActions1" runat="server" EntityName="mapping"  Title="N200" AddEntityURL="BadgeAllocationDetail.aspx" QueryStringFilter="id"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="filters" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageActions" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>N200 Badge Allocations</h1>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="dataTable" Runat="Server">

    <asp:Repeater ID="rptGrid" runat="server" OnItemCommand="rptBadgeAllocation_ItemCommand">
        <HeaderTemplate>
            <table id="dgrGrid" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>Edit</th>
                <th>Event Code</th>
                <th>Stand Size From</th>
                <th>Stand Size To</th>
                <th>Main</th>
                <th>Shared</th>
            </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="GridCentre"><asp:ImageButton id="btnEdit" CommandName="btnEdit"  runat="server" ImageUrl="~/images/AddIcon.png" CommandArgument='<%# Eval("Id") %>'/></td>
                <td class="GridCentre"><%# Eval("EventCode") %></td>
                <td><%# Eval("StandSizeFrom")%></td>
                <td><%# Eval("StandSizeTo")%></td>
                <td><%# Eval("Main")%></td>
                <td><%# Eval("Sharer")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
        </asp:Repeater>

</asp:Content>

