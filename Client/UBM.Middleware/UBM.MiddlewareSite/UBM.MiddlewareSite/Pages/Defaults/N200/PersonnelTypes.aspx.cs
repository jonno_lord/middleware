﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.N200
{
    public partial class PersonnelTypes : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["N200ConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadGrid()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();


                SqlCommand command = new SqlCommand("usp_SEL_PersonnelTypes", connection);
                command.CommandType = CommandType.StoredProcedure;

                rptGrid.DataSource = command.ExecuteReader();
                rptGrid.DataBind();
            }
        }

        #endregion

        #region Protected Methods and Events


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }
        
        #endregion
    }
}