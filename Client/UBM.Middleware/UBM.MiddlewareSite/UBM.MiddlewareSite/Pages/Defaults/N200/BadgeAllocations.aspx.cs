﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebsiteFunctions;

namespace Pages.Defaults.N200
{
    public partial class BadgeAllocations : Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;


        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["N200ConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        #endregion

        #region Private Methods

        private void LoadData()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("usp_GET_BadgeAllocations", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataReader sdr = command.ExecuteReader();

                rptGrid.DataSource = sdr;
                rptGrid.DataBind();
            }
        }

        //links to details page with id of payment type
        protected void rptBadgeAllocation_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "btnedit":
                    //links to details page as an edit, including the record's id in the url
                    Response.Redirect("BadgeAllocationDetail.aspx?Id=" + e.CommandArgument);
                    break;
            }
        }

        #endregion

        #region Protected Methods and Events


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                LoadData();
            }
        }
        
        #endregion
    }
}