﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Edit.master" AutoEventWireup="true" Inherits="Pages.Defaults.N200.BadgeAllocationDetail" Codebehind="BadgeAllocationDetail.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="leftForm" runat="server">
        <div class="addEditForm">

            <div class="formRow">
              <asp:Label ID="lblEventCode" runat="server" cssClass="formMargin labelInput" Text="Event Code:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtEventCode" runat="server" cssClass="formMargin" ToolTip="Event Code" Width="250" MaxLength="10"/>
              <asp:RequiredFieldValidator ID="rfvEventCode" runat="server" ControlToValidate="txtEventCode" ErrorMessage="Please enter an event code">*</asp:RequiredFieldValidator>
            </div>

            <div class="formRow">
              <asp:Label ID="lblStandSizeFrom" runat="server" cssClass="formMargin labelInput" Text="Stand Size From:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtStandSizeFrom" runat="server" cssClass="formMargin" ToolTip="Stand Size From" Width="250" MaxLength="8"/>
              <asp:RequiredFieldValidator ID="rfvStandSizeFrom" runat="server" ControlToValidate="txtStandSizeFrom" ErrorMessage="Please enter a stand size from">*</asp:RequiredFieldValidator>
              <br />
              <asp:RegularExpressionValidator ID="revStandSizeFrom" ControlToValidate="txtStandSizeFrom" runat="server" ErrorMessage="Integer value only" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            </div>

            <div class="formRow">
              <asp:Label ID="lblStandSizeTo" runat="server" cssClass="formMargin labelInput" Text="Stand Size To:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtStandSizeTo" runat="server" cssClass="formMargin" ToolTip="Stand Size To" Width="250" MaxLength="8"/>
              <asp:RequiredFieldValidator ID="rfvStandSizeTo" runat="server" ControlToValidate="txtStandSizeFrom" ErrorMessage="Please enter a stand size from">*</asp:RequiredFieldValidator>
              <br />
              <asp:RegularExpressionValidator ID="revStandSizeTo" ControlToValidate="txtStandSizeTo" runat="server" ErrorMessage="Integer value only" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            </div>

            <div class="formRow">
              <asp:Label ID="lblMain" runat="server" cssClass="formMargin labelInput" Text="Main:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtMain" runat="server" cssClass="formMargin" ToolTip="Main" Width="250" MaxLength="3"/>
              <asp:RequiredFieldValidator ID="rfvMain" runat="server" ControlToValidate="txtMain" ErrorMessage="Please enter a Main allocation">*</asp:RequiredFieldValidator>
              <br />
              <asp:RegularExpressionValidator ID="revMain" ControlToValidate="txtMain" runat="server" ErrorMessage="Integer value only" ValidationExpression="\d+"></asp:RegularExpressionValidator>
              
            </div>

            <div class="formRow">
              <asp:Label ID="lblSharer" runat="server" cssClass="formMargin labelInput" Text="Sharer:" Width="100px"></asp:Label>
              <asp:TextBox ID="txtSharer" runat="server" cssClass="formMargin" ToolTip="Sharer" Width="250" MaxLength="3"/>
               <asp:RequiredFieldValidator ID="rfvSharer" runat="server" ControlToValidate="txtStandSizeFrom" ErrorMessage="Please enter a Sharer allocation">*</asp:RequiredFieldValidator>
              <br />
              <asp:RegularExpressionValidator ID="revSharer" ControlToValidate="txtSharer" runat="server" ErrorMessage="Integer value only" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            </div>

    </div>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="submitButtons" runat="server">
    <uc:CommonButtons ID="CommonButtons" runat="server" EntityName="Badge Allocation" EntityURL="BadgeAllocations.aspx" ConnectionString="MiddlewareN200" deleteEnabled="true" deleteSPName="BadgeAllocation"/>
</asp:Content>

