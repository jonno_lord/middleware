﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using WebsiteFunctions;

namespace Pages.Defaults.N200
{
    public partial class BadgeAllocationDetail : System.Web.UI.Page
    {
        #region Private Fields and Properties

        private static SqlConnection connection;

        private static string connectionString
        {
            get
            {
                string unchangedString = WebConfigurationManager.ConnectionStrings["N200ConnectionString"].ConnectionString;
                string returnString = RemoveProviders.RemoveProvider(unchangedString);
                return returnString;
            }
        }

        private int id
        {
            get
            {
                int returnValue;
                int.TryParse(Request.QueryString["id"], out returnValue);
                return returnValue;
            }
        }

        #endregion

        #region Private Methods

        private void GetBadgeAllocation()
        {

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_GET_BadgeAllocation", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", id);

                SqlDataReader sdr = command.ExecuteReader();
                if (sdr.Read())
                {
                    txtEventCode.Text = sdr["EventCode"].ToString();
                    txtStandSizeFrom.Text = sdr["StandSizeFrom"].ToString();
                    txtStandSizeTo.Text = sdr["StandSizeTo"].ToString();
                    txtMain.Text = sdr["Main"].ToString();
                    txtSharer.Text = sdr["Sharer"].ToString();
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        protected void Page_Load(object sender, EventArgs e)
        {
            CommonButtons.ucSaveEntity_Click += btnSaveEntity_Click;
            CommonButtons.ucCancelEntity_Click += btnCancelEntity_Click;
            CommonButtons.ucDeleteEntity_Click += btnDeleteEntity_Click;

            if (!IsPostBack)
            {
                if (id > 0)
                {
                    GetBadgeAllocation();
                }
            }
        }

        protected void btnDeleteEntity_Click(object sender, EventArgs e)
        {
            DeleteBadgeAllocation();
            Response.Redirect("BadgeAllocations.aspx");
        }

        protected void btnSaveEntity_Click(object sender, EventArgs e)
        {
            SaveBadgeAllocation();
            Response.Redirect("BadgeAllocations.aspx");
        }

        protected void btnCancelEntity_Click(object sender, EventArgs e)
        {
            Response.Redirect("BadgeAllocations.aspx");
        }

        #endregion

        #region Public Methods

        public void SaveBadgeAllocation()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SAV_BadgeAllocation", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@Id", id);
                command.Parameters.AddWithValue("@EventCode", txtEventCode.Text);
                command.Parameters.AddWithValue("@StandSizeFrom", txtStandSizeFrom.Text);
                command.Parameters.AddWithValue("@StandSizeTo", txtStandSizeTo.Text);
                command.Parameters.AddWithValue("@Main", txtMain.Text);
                command.Parameters.AddWithValue("@Sharer", txtSharer.Text);

                command.ExecuteNonQuery();
            }
        }

        public void DeleteBadgeAllocation()
        {
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_DEL_BadgeAllocation", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@id", id);
                
                command.ExecuteNonQuery();
            }
        }
        #endregion

    }
}