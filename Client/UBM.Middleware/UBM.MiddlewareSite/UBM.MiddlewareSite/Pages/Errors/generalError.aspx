﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages_Errors_generalError" Codebehind="generalError.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
<h1>Sorry there is a problem with your request</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContents" Runat="Server">
<p>Please try again or contact your administrator</p>

<div id="stackTrace">
    <asp:Label ID="lblStacktrace" runat="server"></asp:Label>
</div>
</asp:Content>

