﻿using System;
using System.Web;
using System.Web.UI;
using UBM.Logger;

public partial class Pages_Errors_generalError : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpContext ctx = HttpContext.Current;
        Exception exception = ctx.Server.GetLastError();
        Log.Error("MW Site error", exception);
        lblStacktrace.Text = exception.StackTrace;
    }

}