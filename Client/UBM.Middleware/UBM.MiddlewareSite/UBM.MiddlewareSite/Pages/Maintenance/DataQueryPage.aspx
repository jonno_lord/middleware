﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Operations.master" AutoEventWireup="true" Inherits="Pages.Maintenance.DataQueryPage" Codebehind="DataQueryPage.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="CommonButtons" Src="~/UserControls/CommonEdit.ascx" %>



<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link href="../../Styles/StyleSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">

 <div class="buttonAlignLeft">
    <h1>Middleware Data Query Page</h1>
</div>
<div class="validationSummary">
    <asp:ValidationSummary ID="vdsPaymentTypes" runat="server" DisplayMode="BulletList" HeaderText="<div class='validationHeader'>Please make the following changes:</div>" />
</div>

<div style="height: 536px">


    <asp:DropDownList ID="ddlConnectonStrings" runat="server" Height="20px" 
        Width="494px"></asp:DropDownList>


    <br />
    <br />

    <asp:TextBox ID="txtQuery" runat="server" Height="220px" 
      Width="809px" style="margin-top: 0px"></asp:TextBox>

    
    <br />
    <asp:Button ID="cmdGo" CssClass="cmdGo" runat="server" Text="Go" Width="80px" 
        onclick="cmdGo_Click" style="height: 26px" />
    <br />
    <br />
    <asp:GridView ID="dgrQueryResults" runat="server" Height="16px" Width="315px" 
        EnableSortingAndPagingCallbacks="True"  AllowPaging="True"
        
        OnPageIndexChanging="dgrQueryResults_PageIndexChanging" 
        OnPageIndexChanged = "dgrQueryResults_PageIndexChanged"  PageSize="10"> 
    
        <PagerStyle CssClass="pageIndex" />
    </asp:GridView>

    <br />

</div>




</asp:Content>
