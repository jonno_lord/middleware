﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pages.Maintenance
{
    public partial class DataQueryPage : Page
    {
        private static SqlConnection connection;

        protected void Page_Load(object sender, EventArgs e)
        {
            //First load the controls.
           if (!IsPostBack)
           { 
               GetConnectionStrings(); 
           }

           if (txtQuery.Text != "")
           { 
               SetConnectionString(); 
           }

        }

        private void GetConnectionStrings()
        {
            //Loop through the list of connection strings in the the Web.config file and
            //load them into ddlConnectionStrings so that they are available for the user to
            //select.


            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;

            if (settings != null)
            {
                //Loop through each connection string in the connection string collection
                foreach (ConnectionStringSettings cs in settings)
                {
                    //Load the name of each connection string into ddlConnectionStrings
                    ddlConnectonStrings.Items.Add(cs.Name);
                }
            }

        }

        protected void cmdGo_Click(object sender, EventArgs e)
        {
            //When the user clicks "Go", we need to take the query, from txtQuery
            //con catinate it with the chosen connection string and run the query.

            SetConnectionString();

        }

        private void SetConnectionString()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings[ddlConnectonStrings.SelectedValue].ConnectionString;
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");

            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(txtQuery.Text, connection);
  

                    //dgrQueryResults.DataSource = command.ExecuteReader();
                    SqlDataAdapter sda = new SqlDataAdapter(command);

                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    dgrQueryResults.DataSource = ds.Tables[0];
                    dgrQueryResults.DataBind();
                }
            }
            catch (Exception ex)
            {
                txtQuery.Text = ex.Message;
                txtQuery.ForeColor = Color.Red;

            }

        }

        protected void dgrQueryResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrQueryResults.PageIndex = e.NewPageIndex;
            SetConnectionString();
        }

        protected void dgrQueryResults_PageIndexChanged(object sender, EventArgs e)
        {
            SetConnectionString();
        }
}

}
