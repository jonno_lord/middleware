﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Text.RegularExpressions;

namespace UBM.MiddlewareReceipts.Receipts
{
    public class PrintCopy : ProcessTask
    {

        private string serverName;
        private string databaseName;
        private string userName;
        private string password;

        private void setProperties(string ConnectionString)
        {
            databaseName = Regex.Match(ConnectionString, "(Initial Catalog=|Database=){1}([\\w-]+)[;]?").Groups[2].Value;
            serverName = Regex.Match(ConnectionString, "(Data Source=){1}([\\w-]+)[;]?").Groups[2].Value;
            userName = Regex.Match(ConnectionString, "(User ID=){1}([\\w-]+)[;]?").Groups[2].Value;
            password = Regex.Match(ConnectionString, "(Password=){1}([\\w-]+)[;]?").Groups[2].Value;
        }

        public override ProcessEnums.enmStatus Execute()
        {
            log("Retrieving Middleware Common Connection");
            string commonConnection = Connections.GetConnectionString("MiddlewareCommon");

            //-- retrieve batches to process
            log("Getting Receipt Numbers");
            DataSet receiptCopies = InvoiceAccess.GetReceiptNumbers(commonConnection);
            bool errors = false;

            foreach (DataRow receiptNumberRow in receiptCopies.Tables[0].Rows)
            {

                try
                {
                    //-- get settings for this RECEIPT
                    int id = int.Parse(receiptNumberRow["id"].ToString());

                    int receiptNumber = int.Parse(receiptNumberRow["ReceiptNumber"].ToString());
                    string middlewareSystemName = "Middleware" + receiptNumberRow["SystemName"].ToString();
                    string systemName = receiptNumberRow["SystemName"].ToString();

                    string connectionString = Connections.GetConnectionString(middlewareSystemName);
                    string outputPath = ConfigurationManager.AppSettings["ReceiptsPath"].ToString().Trim();

                    log("Found receipt Number" + receiptNumberRow["receiptNumber"].ToString() + " on System " + receiptNumberRow["SystemName"].ToString());

                    //-- set up the Crystal connectivity information
                    setProperties(connectionString);
                    ConnectionInfo conInfo = new ConnectionInfo();
                    conInfo.ServerName = this.serverName;
                    conInfo.DatabaseName = this.databaseName;
                    conInfo.UserID = this.userName;
                    conInfo.Password = this.password;

                    log("Connection was:" + connectionString);

                    //-- this is nolonger required.?.
                    string rptFileLocation = this.Server.MapPath("~/ReceiptTemplates/");

                    log("Creating Receipt at:" + outputPath);

                    if(PdfGenerator.CreateReceiptCopy(receiptNumber, systemName, outputPath, conInfo, rptFileLocation))
                    {
                        log("Setting receiptCopy id " +id.ToString() +" processed.");
                        InvoiceAccess.SetReceiptCopy(commonConnection, id);
                    }
                    else
                    {
                        throw new Exception("Failed to create copy batch - review log");
                    }

                }
                catch (Exception ex)
                {
                    //-- log the fault and continue to the next one.
                    log(ex.Message, ProcessAccess.enmLevel.Critical);

                    throw;
                }
            }

            if(errors)
            {
                throw new Exception("Problems prevented all copy receipts from being produced. Please refer to logs.");
            }


            return ProcessEnums.enmStatus.Successful;
        }


    }

}
