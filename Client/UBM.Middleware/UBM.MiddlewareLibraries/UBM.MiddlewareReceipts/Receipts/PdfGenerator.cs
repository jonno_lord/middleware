﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using UBM.MiddlewareLogging;

namespace UBM.MiddlewareReceipts.Receipts
{
    public class PdfGenerator
    {
        public static bool CreateBatchReceipts(int batchNumber, string systemName, string outputPath, ConnectionInfo connectionInfo, string rptFileLocation)
        {
            return CreateReport(0, batchNumber, systemName, outputPath, connectionInfo, rptFileLocation);
        }

        public static bool CreateReceiptCopy(int receiptNumber, string systemName, string outputPath, ConnectionInfo connectionInfo, string rptFileLocation)
        {
            return CreateReport(receiptNumber, 0, systemName, outputPath, connectionInfo, rptFileLocation);
        }


        private static ReceiptASOP SetCrystalDB(ReceiptASOP report, string systemName, ConnectionInfo connectionInfo)
        {

            connectionInfo.DatabaseName = "Middleware" + systemName;

            foreach (Table t in report.Database.Tables)
            {
                TableLogOnInfo tl = t.LogOnInfo;
                tl.ConnectionInfo = connectionInfo;
                t.ApplyLogOnInfo(tl);
            }

            report.Refresh();

            return report;
        }

        public static bool CreateReport(int receiptNumber, int batchNumber, string systemName, string outputPath, ConnectionInfo connectionInfo, string rptFileLocation)
        {
          
            //@JP
            bool copy = true;
            if (batchNumber > 0)
            {
                copy = false;
            }

            ReceiptASOP receiptAsop = new ReceiptASOP();
            //receiptAsop.Load(rptFileLocation + @"\ReceiptASOP.rpt");

            ReceiptASOP crystal = SetCrystalDB(receiptAsop, systemName, connectionInfo);

            //crystal.Load(rptFileLocation + @"\ReceiptASOP.rpt");
            crystal.SetParameterValue("ReceiptNumber", receiptNumber);
            crystal.SetParameterValue("BatchNumber", batchNumber);
            crystal.SetParameterValue("Copy", copy);

            string number = receiptNumber != 0 ? receiptNumber.ToString() : batchNumber.ToString();

            bool valid = false;
            if(copy)
            {
                valid = CreatePdfFromReport(crystal, outputPath + @"\" + systemName + "-" + number + "-Copy.pdf");
            }
            else
            {
                valid = CreatePdfFromReport(crystal, outputPath + @"\" + number + "-Receipts.pdf");
            }

            return valid;


        }

        private static bool CreatePdfFromReport(ReportDocument crystal, string path)
        {
          
                DiskFileDestinationOptions destinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions formatTypeOptions = new PdfRtfWordFormatOptions();
                destinationOptions.DiskFileName = path;
                ExportOptions exportOptions = crystal.ExportOptions;
                {
                    exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    exportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    exportOptions.DestinationOptions = destinationOptions;
                    exportOptions.FormatOptions = formatTypeOptions;
                }
                crystal.Export();
                return true;
         
        }
    }
}
