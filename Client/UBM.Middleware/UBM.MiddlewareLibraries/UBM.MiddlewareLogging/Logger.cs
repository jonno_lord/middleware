﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareDataAccess;
using UBM.Logger;
using System.Configuration;

namespace UBM.MiddlewareLogging
{
    public static class Logger
    {

        //private static Log log = new Log();

        public static void BatchLog(int jobid, int processid, int batchid, ProcessAccess.enmLevel level, string description)
        {
            // to be superceeded by the UBM.Logger application - see below
            LogAction(jobid, processid, batchid, description, level);
        }

        public static void ErrorLog(Exception ex, string description = "", int batchid = 0, int processid = 0, int jobid = 0)
        {
            // to be superceeded by the UBM.Logger application - see below
            LogAction(jobid, processid, batchid, description, ProcessAccess.enmLevel.Critical, ex);
        }

        // call to the correct way to log this information.
        public static void LogAction(int jobid=0, int processid=0, int batchid=0, string description="", ProcessAccess.enmLevel level = ProcessAccess.enmLevel.Important, Exception ex = null)
        {

            string innerException = "";
            if (ex != null && ex.InnerException != null) 
                innerException = ex.InnerException.Message.ToString();

            int loglevel = 0;

            if (ConfigurationManager.AppSettings.Count > 0)
            {
                try
                {
                    int.TryParse(ConfigurationManager.AppSettings["LoggingLevel"].ToString(), out loglevel);
                }
                catch (Exception exLogging)
                {
                    //-- an exception in the logger, attempting to log an exception.
                    //-- :(
                    //TODO: Deal with exceptions logged as exceptions...
                    System.Diagnostics.Debug.WriteLine(exLogging.Message);
                }
            }
            switch (level)
            {
                case ProcessAccess.enmLevel.Critical:
                    if(ex == null)
                        ProcessAccess.insertErrorLog(description, innerException, "", "");
                    else
                        ProcessAccess.insertErrorLog(ex.Message, innerException, ex.Source, ex.StackTrace);
                    Log.Error(description, ex);
                    ProcessAccess.insertBatchLog(jobid, processid, batchid, level, description);
                    break;

                case ProcessAccess.enmLevel.Serious:
                    if (ex == null)
                        ProcessAccess.insertErrorLog(description, innerException, "", "");
                    else
                        ProcessAccess.insertErrorLog(ex.Message, innerException, ex.Source, ex.StackTrace);
                    Log.Error(description, ex);
                    ProcessAccess.insertBatchLog(jobid, processid, batchid, level, description);
                    break;

                case ProcessAccess.enmLevel.Important:
                    if (loglevel >= 1)
                        ProcessAccess.insertBatchLog(jobid, processid, batchid, level, description);
                    Log.Warning(description);
                    break;

                case  ProcessAccess.enmLevel.Standard:
                    if(loglevel >= 2)
                        ProcessAccess.insertBatchLog(jobid, processid, batchid, level, description);
                    Log.Information(description);
                    break;

            }

        }


    }
}
