﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Collections;
using System.Configuration;

namespace UBM.MiddlewareESOP.Helpers
{
    public static class CommonFeedTasks
    {
        public static string UnprocessedDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["UnprocessedDirectory"].ToString().Trim();
            }
        }

        public static string ProcessedDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["ProcessedDirectory"].ToString().Trim();
            }
        }

        public static string ESOPWebFeedSFTPServer
        {
            get
            {
                return ConfigurationManager.AppSettings["ESOPWebFeedSFTPServer"].ToString().Trim();
            }
        }

        public static string ESOPWebFeedFTPServer
        {
            get
            {
                return ConfigurationManager.AppSettings["ESOPWebFeedFTPServer"].ToString().Trim();
            }
        }


        public static string ESOPWebFeedUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["ESOPWebFeedSFTPUser"].ToString().Trim();
            }
        }

        public static string ESOPWebFeedPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["ESOPWebFeedSFTPPassword"].ToString().Trim();
            }
        }

        public static string ESOPWebFeedFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["ESOPWebFeedFilePath"].ToString().Trim();
            }
        }

        public static string AdestraWebFeedFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["AdestraWebFeedFilePath"].ToString().Trim();
            }
        }

        public static string ESOPWebFeedSFTPDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["ESOPWebFeedSFTPDirectory"].ToString().Trim();
            }
        }

        public static string AdestraWebFeedSFTPDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["AdestraWebFeedSFTPDirectory"].ToString().Trim();
            }
        }



        public static int InsertFileDetailsHistory(int fileDetailsId, string Description, bool isError, ProcessTask task)
        {
            SqlParameter[] parms = new SqlParameter[3];
            parms[0] = new SqlParameter("fileDetailId", fileDetailsId);
            parms[1] = new SqlParameter("Description", Description);
            parms[2] = new SqlParameter("isError", isError);

            return (task.FireStoredProcedure("usp_INS_FileDetailHistory", parms));

        }


        public static int InsertFailedFileDetails(int categoryId, ProcessTask task, string message = "No files found")
        {
            SqlParameter[] parms = new SqlParameter[7];
            parms[0] = new SqlParameter("FileName", message);
            parms[1] = new SqlParameter("FilePath", "");
            parms[2] = new SqlParameter("CategoryId", categoryId);
            parms[3] = new SqlParameter("FileSize", "0");
            parms[4] = new SqlParameter("FileDate", DateTime.Now);
            parms[5] = new SqlParameter("BatchNumber", task.Batchid);
            parms[6] = new SqlParameter("Status", "FAILED");

            return (task.FireScalarStoredProcedure("usp_INS_FileDetails", parms));
            
        }

        public static int InsertFileDetailsEntry(FileInfo file, int categoryId, ProcessTask task)
        {
            SqlParameter[] parms = new SqlParameter[6];
            parms[0] = new SqlParameter("FileName", file.Name);
            parms[1] = new SqlParameter("FilePath", file.Directory.ToString());
            parms[2] = new SqlParameter("CategoryId", categoryId);
            parms[3] = new SqlParameter("FileSize", file.Length);
            parms[4] = new SqlParameter("FileDate", file.CreationTime);
            parms[5] = new SqlParameter("BatchNumber", task.Batchid);

            return (task.FireScalarStoredProcedure("usp_INS_FileDetails", parms));

        }

        public static void UpdateFileSize(int fileDetailsId, long fileSize, ProcessTask task)
        {
            SqlParameter[] parms = new SqlParameter[2];
            parms[0] = new SqlParameter("FileDetailId", fileDetailsId);
            parms[1] = new SqlParameter("FileSize", fileSize);
            task.FireStoredProcedure("usp_UPD_FileDetailsSize", parms);
        }

        public static void UpdateFileDetailsEntry(int fileDetailsId, string status, DateTime? ftpDate, DateTime? movedFileDate, ProcessTask task)
        {

            SqlParameter[] parms = new SqlParameter[2];

            if ((ftpDate != null && movedFileDate == null) || (ftpDate == null && movedFileDate != null))
            {
                parms = new SqlParameter[3];

                if(ftpDate != null)
                    parms[2] = new SqlParameter("FTPDate", ftpDate);
            
                if(movedFileDate != null)
                    parms[2] = new SqlParameter("MovedFileDate", movedFileDate);
            }

            if ((ftpDate != null && movedFileDate != null))
            {
                parms = new SqlParameter[4];
                parms[2] = new SqlParameter("FTPDate", ftpDate);
                parms[3] = new SqlParameter("MovedFileDate", movedFileDate);
            }

            parms[0] = new SqlParameter("FileDetailId", fileDetailsId);
            parms[1] = new SqlParameter("Status", status);

            task.FireStoredProcedure("usp_UPD_FileDetails", parms);

        }



        public static void RenameLocalFile(FileInfo file, string DestinationFolder, string newFileName,
        int fileDetailsId, ProcessTask task)
        {

            try
            {
                string renamedFile = newFileName;
                task.log("renaming " + file.Name + " to " + renamedFile);
                file.MoveTo(DestinationFolder + renamedFile);

                UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", null, DateTime.Now, task);

            }
            catch (Exception ex)
            {
                task.log("Failed to Move file " + file.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to Move file :" + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);

            }
        }


        public static void MoveAndRenameFile(FileInfo file, string DestinationFolder, string newExtension, 
                int fileDetailsId, ProcessTask task)
        {

            try
            {
                string renamedFile = DestinationFolder + file.Name.Substring(0, file.Name.LastIndexOf(".")) + newExtension;

                task.log("moving " + file.Name + " to " + renamedFile);

                if(File.Exists(renamedFile))
                    File.Delete(renamedFile);

                file.MoveTo(renamedFile);

                UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", null, DateTime.Now, task);

            }
            catch (Exception ex)
            {
                task.log("Failed to Move file " + file.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to Move file :" + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);

            }
        }

        /// <summary>
        /// Deletes a file on the FTP server
        /// </summary>
        /// <param name="ftpSettings">the ftp settings on the server</param>
        /// <param name="FTPFileName">the filename</param>
        /// <param name="fileDetailsId">the id of the file used for logging</param>
        /// <param name="task">the task object</param>
        /// <returns>void</returns>
        public static void DeleteFile(FTPSettings ftpSettings, string FTPFileName, int fileDetailsId, ProcessTask task)
        {
            string uri = "ftp://" + ftpSettings.ServerName + ":" + ftpSettings.PortNumber + "/" + ftpSettings.SubFolderName + FTPFileName;
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
            reqFTP.Credentials = new NetworkCredential(ftpSettings.UserName, ftpSettings.Password);

            task.log("Deleting Remote file " + uri);

            reqFTP.KeepAlive = false;
            reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;
            reqFTP.UseBinary = true;
            try
            {
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                //-- file could not be sent
                task.log("Failed to Delete file " + FTPFileName + ", it may not exist." + ex.Message.ToString(), ProcessAccess.enmLevel.Important);
            }

        }


        /// <summary>
        /// Renames a file that has been downloaded.
        /// </summary>
        /// <param name="ftpSettings">Settings data</param>
        /// <param name="FTPFileName">Filename to rename</param>
        /// <param name="FTPNewName">the new name of the file</param>
        /// <param name="fileDetailsId">the id of the file this belongs to</param>
        /// <param name="task">the task that spawned this process</param>
        /// <returns>Failure (false) success (true)</returns>
        /// 
        public static bool RenameFile(FTPSettings ftpSettings, string FTPFileName, string FTPNewName, int fileDetailsId, ProcessTask task)
        {
            bool renamedFile = false;
            string uri = "ftp://" + ftpSettings.ServerName + ":" + ftpSettings.PortNumber + "/" + ftpSettings.SubFolderName + FTPFileName;
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
            reqFTP.Credentials = new NetworkCredential(ftpSettings.UserName, ftpSettings.Password);

            //-- delete the new file name if it exists
            DeleteFile(ftpSettings, FTPNewName, fileDetailsId, task);

            task.log("renaming " + uri + " to " + FTPNewName);
            InsertFileDetailsHistory(fileDetailsId, "renaming " + uri +" to " + FTPNewName, false, task);

            reqFTP.KeepAlive = false;
            reqFTP.Method = WebRequestMethods.Ftp.Rename;
            reqFTP.RenameTo = FTPNewName;
            reqFTP.UseBinary = true;
            try
            {

                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", DateTime.Now, null, task);
                renamedFile = true;

            }
            catch (Exception ex)
            {
                //-- file could not be sent
                task.log("Failed to rename file " + FTPFileName +" to " + FTPNewName + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to rename file :" + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
            }

            return renamedFile;
        }


        /// <summary>
        /// returns a listing of files on a remote directory on an FTP server, as specified in teh ftpSettings object
        /// </summary>
        /// <param name="ftpSettings">the details of the FTP server to connect to</param>
        /// <param name="task">a process task that implements the process interface (normally "this")</param>
        /// <returns></returns>
        public static List<string > DirectoryListing(FTPSettings ftpSettings, ProcessTask task)
        {

            List<string> files = new List<string>();

            string uri = "ftp://" + ftpSettings.ServerName + ":" + ftpSettings.PortNumber + "/" + ftpSettings.SubFolderName; 
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));

            task.log("Reading from" + uri);

            reqFTP.Credentials = new NetworkCredential(ftpSettings.UserName, ftpSettings.Password);
            task.log("FTP Credentials are User:" + ftpSettings.UserName + " Password:" + ftpSettings.Password);

            reqFTP.KeepAlive = false;
            reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
            reqFTP.UsePassive = true;
            reqFTP.UseBinary = true;


            try
            {

                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                while (!reader.EndOfStream)
                {
                    files.Add(reader.ReadLine().ToLower());
                }


                //Clean-up
                reader.Close();
                responseStream.Close(); //redundant
                response.Close();

            }
            catch (Exception ex)
            {
                task.log(ex.Message.ToString());
                //-- file could not be sent
                task.log("could not read listing from " + uri);
            }

            return files;

        }


        /// <summary>
        /// Sends a file via FTP to the recipient. if this fails, it will return the exception as an out
        /// parameter and return false
        /// </summary>
        /// <param name="ftpSettings">The FTP connection details</param>
        /// <param name="fileName">The name of the file to send</param>
        /// <param name="error">the error the routine encountered</param>
        /// <returns>failure (false) success (true)</returns>
        public static bool UploadFile(FTPSettings ftpSettings, FileInfo file,  int fileDetailsId, ProcessTask task)
        {

            bool fileUploaded = false;

            string uri = "ftp://" + ftpSettings.ServerName + ":" + ftpSettings.PortNumber + "/" + ftpSettings.SubFolderName + file.Name; 
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));

            InsertFileDetailsHistory(fileDetailsId, "Sending to " + uri, false, task);
            task.log("Sending to " + uri);

            reqFTP.Credentials = new NetworkCredential(ftpSettings.UserName, ftpSettings.Password);
            InsertFileDetailsHistory(fileDetailsId, "FTP Credentials are User:" + ftpSettings.UserName + " Password:" + ftpSettings.Password, false, task);
            task.log("FTP Credentials are User:" + ftpSettings.UserName + " Password:" + ftpSettings.Password);

            reqFTP.KeepAlive = false;
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
            reqFTP.UseBinary = true;
            reqFTP.ContentLength = file.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file
            // to be uploaded
            FileStream fs = file.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();
                contentLen = fs.Read(buff, 0, buffLength);
                task.log("Reading file stream for " + file.Name);

                int totalLength = 0;
                while (contentLen != 0)
                {
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                    totalLength += contentLen;
                }

                task.log("Read / Wrote " + totalLength.ToString() + " bytes. Closing file handles");
                

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();

                fileUploaded = true;
                InsertFileDetailsHistory(fileDetailsId, "File succesfully Sent (" + totalLength.ToString() + " bytes)", false, task);

                //-- mark the file as sent
                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "SENT", DateTime.Now, null, task);

            }
            catch (Exception ex)
            {
                //-- file could not be sent
                task.log("could not send file to " + uri);
                task.log("Failed to send file " + file.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                if (fileDetailsId != 0)
                {
                    InsertFileDetailsHistory(fileDetailsId, "Failed to Send file :" + ex.Message.ToString(), true, task);

                    //-- Mark it as failed
                    UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);


                }
            }

            return fileUploaded;
        }

        public static string RemoveProvider(string connectionString)
        {
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connectionString;
        }

    }
}
