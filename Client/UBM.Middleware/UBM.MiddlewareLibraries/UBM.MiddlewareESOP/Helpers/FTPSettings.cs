﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareESOP.Helpers
{
    public class FTPSettings
    {

        public string ServerName { get; set; }
        public string UserName {get;set;}
        public string Password { get; set; }
        public string PortNumber { get; set; }
        public string SubFolderName { get; set; }

        public FTPSettings(string serverName, string userName, string password, string portNumber, string subFolderName)
        {

            this.ServerName = serverName;
            this.UserName = userName;
            this.Password = password;
            this.PortNumber = portNumber;
            this.SubFolderName = subFolderName;

            Validate();   //-- ensure that FTP details are set
        }

        private void Validate()
        {
            //-- check to make sure that the properties have been set up.
            if (this.ServerName == null || this.UserName == null || this.Password == null || this.PortNumber == null)
                throw new Exception("Server, Password, UserName or Port was not specified");
        }

        public FTPSettings(Dictionary<string, Dictionary<string,string>> configuration, string subFolderName)
        {
            this.ServerName = configuration["SERVER"]["FTP-ServerName"];
            this.UserName = configuration["SERVER"]["FTP-UserName"];
            this.Password = configuration["SERVER"]["FTP-Password"];
            this.PortNumber = configuration["SERVER"]["FTP-PortNumber"];
            this.SubFolderName = subFolderName;

            Validate(); //-- ensure that FTP details are set

        }


    }
}
