﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareESOP.Customers
{
    public class MiddlewareToMiddlewareOut : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_CustomersToEsopFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
