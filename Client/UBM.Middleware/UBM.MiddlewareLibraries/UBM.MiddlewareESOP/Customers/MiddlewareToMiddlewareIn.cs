﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareESOP.Customers
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.FireDTSPackage("ESOP Rejection Notes Middleware To Source.dtsx", 
                  "SELECT TOP 10 * FROM Customers WHERE Stage=1 AND RejectedInScrutiny IS NOT NULL AND ISNULL(RejectionNoteSent,0) = 0",
                Connections.GetConnectionString("MiddlewareESOP"), false);

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_CustomersToJDEFormat",
                    "SELECT TOP 10 * FROM Customers WHERE Stage = 1 AND Accepted = 1",
                    Connections.GetConnectionString("MiddlewareESOP"));

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
