﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareESOP
{
    internal static class Common
    {
        internal static string MiddlewareESOPConnection(int systemid)
        {
            return MiddlewareDataAccess.Connections.GetConnectionString(systemid);
        }
    }
}
