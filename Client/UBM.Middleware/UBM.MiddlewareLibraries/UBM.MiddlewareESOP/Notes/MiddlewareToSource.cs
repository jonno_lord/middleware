﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareESOP.Notes
{
    public class MiddlewareToSource:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ESOP Credit Controller Notes Middleware to Source.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
