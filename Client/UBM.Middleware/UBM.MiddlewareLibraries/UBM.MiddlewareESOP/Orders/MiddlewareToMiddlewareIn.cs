﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareESOP.Orders
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_OrdersToJDEFormat",
                "SELECT TOP 1 * FROM Orders WHERE Stage = 1",
                 Connections.GetConnectionString("MiddlewareESOP"));

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
