﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareESOP.Orders
{
    public class JDEToMiddleware:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.MiddlewareSourceConnection = "MiddlewareESOP";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Orders JDE to Middleware.dtsx", true);
            return ProcessEnums.enmStatus.Successful;
        }
    }

}
