﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareESOP.Orders
{
    public class MiddlewareToJDE:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.MiddlewareSourceConnection = "MiddlewareESOP";

            //-- if this fails it will throw an exception up.);
            this.FireDTSPackage("COMMON Orders Middleware To JDE.dtsx",
                "SELECT TOP 1 * FROM Orders WHERE Stage = 2",
                Connections.GetConnectionString("MiddlewareESOP"),true
                );

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
