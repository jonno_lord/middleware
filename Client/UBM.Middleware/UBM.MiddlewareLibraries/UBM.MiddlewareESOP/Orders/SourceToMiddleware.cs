﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareESOP.Orders
{
    public class SourceToMiddleware:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ESOP Orders Source to Middleware.dtsx",
                 "SELECT * FROM Contract_Order_Line_Detail WHERE ISNULL(Passed_from_Esop_to_MW ,0) = 0",
                Connections.GetConnectionString("ESOP"));

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
