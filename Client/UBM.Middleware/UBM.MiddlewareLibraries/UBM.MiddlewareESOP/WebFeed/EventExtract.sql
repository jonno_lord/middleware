﻿SELECT CAST(Banner_Name.Banner_Name_Id AS bigint) AS [Banner Name Id], Banner_Name.Banner_Name AS [Exhibitor Name], 
	Banner_Name.Web_Username AS [Exhibitor Username], Banner_Name.Web_Password AS [Exhibitor Password], 
	Contract.Exhibiting_Name, Company.Company_Name, Stand.Stand_Number, Hall.Hall_Name, Event.Event_Code, 
	Stand.Ground_Dimension_X, Stand.Ground_Dimension_Y, Stand.Ground_Floor_Area, Stand.Net_Area, 
	Product_Type.Product_Type_Name, Event_Product.Event_Product_Name, Contract.Status, Contact.Title, Contact.First_Name, 
	Contact.Last_Name, Contact.Address_1, Contact.Address_2, Contact.Address_3, Contact.City, Contact.County, 
	Contact.Post_Code, Country.Country_Name, Contact.Phone, Contact.Fax, Contact.Email_text, Contact_1.Title AS Web_Title, 
	Contact_1.First_Name AS Web_First_Name, Contact_1.Last_Name AS Web_Last_Name, Contact_1.Address_1 AS Web_Address_1, 
	Contact_1.Address_2 AS Web_Address_2, Contact_1.Address_3 AS Web_Address_3, Contact_1.City AS Web_City, Contact_1.County AS Web_County, 
	Contact_1.Post_Code AS Web_Post_Code, Country_1.Country_Name AS Web_Country, Contact_1.Phone AS Web_Phone, Contact_1.Fax AS Web_Fax, 
	Contact_1.Email_text AS Web_Email, Company_1.WWW AS Company_WWW, Company_1.Email AS Company_Email, 
	Company_1.Web_Username AS Company_Web_Username, Company_1.Web_Password AS Company_Web_Password, 
	Banner_Name.Banner_Sharer AS [Exhibitor Type], CAST(Stand.Stand_Id AS BIGINT) AS [Stand Id], 
	CAST(Banner_Name.Exhibitor_Id AS bigint) AS [Exhibitor Id], Company_1.Phone AS Company_Phone, 
	Event.Use_Company_Phone_Webfeed AS Use_Company_Phone, cast(contact.contact_id as bigint) as [contact_id], 
	Company.Linkedin AS [Company Linkedin], Company.Facebook AS [Company Facebook], Company.Twitter AS [Company Twitter], 
	Contract.Access_Code, 
	Contact_2.Title AS OPS_Title, 
	Contact_2.First_Name AS OPS_First_Name, Contact_2.Last_Name AS OPS_Last_Name, Contact_2.Address_1 AS 
	OPS_Address_1, 
	Contact_2.Address_2 AS OPS_Address_2, Contact_2.Address_3 AS OPS_Address_3, Contact_2.City AS OPS_City, 
	Contact_2.County AS OPS_County, 
	Contact_2.Post_Code AS OPS_Post_Code, Country_1.Country_Name AS OPS_Country, Contact_2.Phone AS OPS_Phone, 
	Contact_2.Fax AS OPS_Fax, 
	Contact_2.Email_text AS OPS_Email, 
	cast(contact_2.contact_id as bigint)as[contact_2_id] 

FROM Country Country_1 
	INNER JOIN Contact Contact_1 ON Country_1.Country_Id = Contact_1.Country_Id 
	INNER JOIN Event 
	INNER JOIN 
	Banner_Name INNER JOIN 
	Stand_Sharers ON Banner_Name.Stand_Sharers_Id = Stand_Sharers.Stand_Sharers_Id INNER JOIN 
	Stand ON Stand_Sharers.Stand_Id = Stand.Stand_Id INNER JOIN 
	Contract ON Stand.Contract_Id = Contract.Contract_Id INNER JOIN 
	Company ON Stand.Company_Id = Company.Company_Id INNER JOIN 
	Hall ON Stand.Hall_Id = Hall.Hall_Id INNER JOIN 
	Event_Product ON Stand.Event_Product_Id = Event_Product.Event_Product_Id INNER JOIN 
	Product_Type ON Stand.Product_Type_Id = Product_Type.Product_Type_Id ON Event.Event_Id = Contract.Event_Id ON 
	Contact_1.Contact_Id = Stand_Sharers.Web_Contact INNER JOIN 
	Country INNER JOIN 
	Contact ON Country.Country_Id = Contact.Country_Id ON Stand_Sharers.Admin_Contact = Contact.Contact_Id INNER JOIN 
	Company Company_1 ON Contact.Company_Id = Company_1.Company_Id 
	INNER JOIN Contact Contact_2 ON Contact_2.Contact_id = Stand_Sharers.OPS_Contact 
	WHERE (Event.Event_Code = '[**EVENT_CODE**]') AND (Banner_Name.Banner_Report_Flag = '1') ORDER BY Event.Event_Code, Stand.Stand_Number
