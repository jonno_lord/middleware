﻿SELECT CAST(h.Hall_Id AS BIGINT) AS "Hall Id", h.Hall_Link AS "Hall Link", h.Hall_Name AS "Hall Name", h.Hall_Prefix AS "Hall Prefix",
h.Vertex1_X AS "Hall Vertex X1", h.Vertex2_X AS "Hall Vertex X2", h.Vertex1_Y AS "Hall Vertex Y1", h.Vertex2_Y AS "Hall Vertex Y2"
FROM Hall h, Event e
WHERE e.Event_Code = '[**EVENT_CODE**]'
AND e.Event_Id = h.Event_Id
