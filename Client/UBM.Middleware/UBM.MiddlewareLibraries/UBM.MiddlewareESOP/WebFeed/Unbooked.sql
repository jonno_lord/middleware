﻿SELECT     Stand.Stand_Number, Hall.Hall_Name, Stand.Ground_Dimension_X, Stand.Ground_Dimension_Y, Stand.Ground_Floor_Area, 
                      Stand.Net_Area, Event.Event_Code
FROM         Stand INNER JOIN
                      Hall ON Stand.Hall_Id = Hall.Hall_Id INNER JOIN
                      Event ON Hall.Event_Id = Event.Event_Id
WHERE     Event.Event_Code = '[**EVENT_CODE**]'
ORDER BY Event.Event_Code, Stand.Stand_Number