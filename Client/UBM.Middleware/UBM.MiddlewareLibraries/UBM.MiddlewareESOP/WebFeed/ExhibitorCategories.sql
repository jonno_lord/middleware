﻿SELECT DISTINCT cast(Banner_Name.Exhibitor_Id as bigint) AS [Exhibitor Id], cast(Isnull(Sector.Sector_Id,0) as bigint) as [Category Id] 
FROM  Country Country_1 
INNER JOIN Contact Contact_1 ON Country_1.Country_Id = Contact_1.Country_Id 
INNER JOIN Event 
	INNER JOIN Banner_Name 
		INNER JOIN Stand_Sharers ON Banner_Name.Stand_Sharers_Id = Stand_Sharers.Stand_Sharers_Id 
			INNER JOIN Stand ON Stand_Sharers.Stand_Id = Stand.Stand_Id 
			INNER JOIN Contract ON Stand.Contract_Id = Contract.Contract_Id
			INNER JOIN Event EventSubQuery ON Contract.Event_Id = EventSubQuery.Event_id 
			INNER JOIN Sector_Interest  
			INNER JOIN Company ON Company.Company_ID = Sector_Interest.Company_ID  
							ON Stand.Company_Id = Company.Company_Id 
					INNER JOIN Sector on Sector_Interest.sector_ID = Sector.Sector_ID 
					INNER JOIN Show on sector.show_id = show.show_id and show.show_id = EventSubQuery.Show_Id 
					       
				INNER JOIN  Hall  ON Stand.Hall_Id = Hall.Hall_Id 
INNER JOIN Event_Product ON Stand.Event_Product_Id = Event_Product.Event_Product_Id 
INNER JOIN Product_Type ON Stand.Product_Type_Id = Product_Type.Product_Type_Id 
       ON Event.Event_Id = Contract.Event_Id 
              ON  Contact_1.Contact_Id = Stand_Sharers.Web_Contact 
INNER JOIN Country 
INNER JOIN Contact ON Country.Country_Id = Contact.Country_Id 
       ON Stand_Sharers.Admin_Contact = Contact.Contact_Id 
INNER JOIN Company Company_1 ON Contact.Company_Id = Company_1.Company_Id 
WHERE (Event.Event_Code = '[**EVENT_CODE**]' ) 
AND    (Banner_Name.Banner_Report_Flag = '1') 