﻿SELECT CAST(sg2.Stand_Geometry2_Id AS BIGINT) AS "Stand Geometry2 Id", e.Event_Code AS "Event Code", h.Hall_Prefix AS "Hall Prefix",
CAST(h.Hall_Id AS BIGINT) AS "Hall Id", s.Stand_Number AS "Stand Number", CAST(s.Stand_Id AS BIGINT) AS "Stand Id", sg2.Sequence2, sg2.X1, sg2.Y1, sg2.X2, sg2.Y2,
sg2.X3, sg2.Y3, sg2.X4, sg2.Y4, sg2.Stand_Link AS "Stand Link", sg2.N1, sg2.N2, sg2.N3, sg2.N4
FROM Stand_Geometry2 sg2, Stand s, Hall h, Event e
WHERE e.Event_Code = '[**EVENT_CODE**]'
AND e.Event_Id = h.Event_Id
AND h.Hall_Id = s.Hall_Id
AND s.Stand_Id = sg2.Stand_Id
