﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using UBM.MiddlewareESOP.Helpers;
using System.Reflection;
using System.Net;
using UBM.Utilities;
using Rebex.Net;
using System.Configuration;

namespace UBM.MiddlewareESOP.WebFeed
{
    public class MiddlewareToSource : ProcessTask
    {

        #region Fields

        private List<String> EventCodes;
        private string EventCode { get; set; }

        private string ESOPConnectionString
        {
            get
            {
                return CommonFeedTasks.RemoveProvider(Connections.GetConnectionString("ESOP"));
            }
        }

        private string ESOPWebFeedSFTPServer
        {
            get 
            {
                return CommonFeedTasks.ESOPWebFeedSFTPServer;
            }
        }

        private string ESOPWebFeedFTPServer
        {
            get
            {
                return CommonFeedTasks.ESOPWebFeedFTPServer;
            }
        }

        private string ESOPWebFeedUserName
        {
            get
            {
                return CommonFeedTasks.ESOPWebFeedUserName;
            }
        }

        private string ESOPWebFeePassword
        {
            get
            {
                return CommonFeedTasks.ESOPWebFeedPassword;
            }
        }

        private string ESOPWebFeedFilePath
        {
            get
            {
                return CommonFeedTasks.ESOPWebFeedFilePath;
            }
        }

        private string AdestraWebFeedFilePath
        {
            get
            {
                return CommonFeedTasks.AdestraWebFeedFilePath;
            }
        }

        private string ESOPWebFeedSFTPDirectory
        {
            get
            {
                return CommonFeedTasks.ESOPWebFeedSFTPDirectory;
            }
        }

        private string AdestraWebFeedSFTPDirectory
        {
            get
            {
                return CommonFeedTasks.AdestraWebFeedSFTPDirectory;
            }
        }

        #endregion

        #region Private Methods

        private void AssignEventCodes()
        {
            EventCodes = new List<string>();

            string sqlText = "SELECT Event_Code,cast(show_id as int) as show_id, Divisional_Report_Type  FROM Event WHERE Search_Type = '0' and Event.WebFeeder = '1' ";

            using (SqlConnection connection = new SqlConnection(ESOPConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlText;

                    SqlDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        EventCodes.Add(dr["Event_Code"].ToString());
                    }
                }
            }

            log(EventCodes.Count + " events to process");
        }

        /// <summary>
        /// The data set to write out to a text file (delimited with commas)
        /// </summary>
        /// <param name="dataSet">Data set to write</param>
        /// <param name="fileName">the file name (excluding slashes)</param>
        private void WriteDataSet(DataSet dataSet, string fileName, bool includeEventCode, bool includeHeaders)
        {
            int rows = dataSet.Tables[0].Rows.Count;

            StreamWriter dataStream;
            string filePath;

            if (includeEventCode)
            {
                filePath = CommonFeedTasks.UnprocessedDirectory + @"\" + fileName + "_" + EventCode + ".csv";
            }
            else
            {
                filePath = CommonFeedTasks.UnprocessedDirectory + @"\" + fileName + ".csv";
            }

            ArchiveFile(filePath);

            dataStream = new StreamWriter(filePath, false, new UTF8Encoding(false));

            using (dataStream)
            {
                //-- write headers
                if (includeHeaders)
                {
                    for (int i = 0; i < dataSet.Tables[0].Columns.Count; i++)
                    {
                        if (i != dataSet.Tables[0].Columns.Count - 1)
                        {
                            dataStream.Write(dataSet.Tables[0].Columns[i] + ",");
                        }
                        else
                        {
                            dataStream.Write(dataSet.Tables[0].Columns[i]);
                        }
                    }

                    dataStream.WriteLine();
                }

                //-- write rows
                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    string result = "";
                    int rowCount = row.ItemArray.Count();

                    for (int i = 0; i < rowCount; i++)
                    {
                        string typeName = row.ItemArray[i].GetType().Name;

                        if (typeName == "String")
                            result = "\"" + row.ItemArray[i].ToString() + "\"";
                        else if (typeName == "Byte[]")
                            result = BitConverter.ToString((byte[])row.ItemArray[i]).Replace("-", "");
                        else
                            result = row.ItemArray[i].ToString();
                        
                        if (rowCount - 1 == i)
                            dataStream.Write(result);
                        else
                            dataStream.Write(result + ",");
                    }

                    dataStream.WriteLine();
                }

                dataStream.Close();
            }

            if (ConfigurationManager.AppSettings["SendToFTP"] == "1")
                SendFileToFTP(filePath);
        }

        private string PrimeSQL(string sql)
        {
            sql = sql.Replace("[**EVENT_CODE**]", this.EventCode);
            return sql;
        }


        private DataSet ExecuteEmbededResource(string resourceFileName)
        {
            DataSet ds = new DataSet();

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "UBM.MiddlewareESOP.WebFeed." + resourceFileName;

            string result = "";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }

            string sqlText = PrimeSQL(result);

            using (SqlConnection connection = new SqlConnection(ESOPConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlText;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        sda.Fill(ds);
                    }
                }
            }
            return ds;
        }

        private void ArchiveFile(string filePath)
        {
            string oldFilePath = filePath.Replace(".csv", "_OLD.csv");

            if (File.Exists(oldFilePath))
            {
                File.Delete(oldFilePath);
            }

            if (File.Exists(filePath))
            {
                File.Move(filePath, oldFilePath);
            }
        }

        private void SendFileToFTP(string filePath)
        {
            string fileName = Path.GetFileName(filePath);
            string oldFileName = fileName.Replace(".csv", "_OLD.csv");

            FtpHelper ftp = new FtpHelper(ESOPWebFeedFTPServer, ESOPWebFeedUserName, ESOPWebFeePassword, ESOPWebFeedFilePath);
            ftp.UploadFile(fileName);

            //put old.csv in old directory on FTP
            //string server = ESOPWebFeedFTPServer.Replace("testing", "old");

            ftp = new FtpHelper(ESOPWebFeedFTPServer, ESOPWebFeedUserName, ESOPWebFeePassword, ESOPWebFeedFilePath);
            ftp.UploadFile(oldFileName);
        }

        private void SendFilesToSFTP(string filePath, string sftpFilePath)
        {
            using (Sftp sftp = new Sftp())
            {

                SshParameters parms = new SshParameters();
                parms.HostKeyAlgorithms = SshHostKeyAlgorithm.Any;
                parms.KeyExchangeAlgorithms = SshKeyExchangeAlgorithm.DiffieHellmanGroup14SHA1;

                sftp.Connect(ESOPWebFeedSFTPServer, 22, parms);
                sftp.Login(ESOPWebFeedUserName, ESOPWebFeePassword);
                sftp.PutFiles(filePath + @"\*", sftpFilePath, SftpBatchTransferOptions.Default, SftpActionOnExistingFiles.OverwriteAll);
            }
        }

        private void LogConnections()
        {
            string ftp = ConfigurationManager.AppSettings["SendToFTP"].ToString();
            string sftp = ConfigurationManager.AppSettings["SendToSFTP"].ToString();
            
            log("ESOP: " + ESOPConnectionString);
            log("FTP Server: " + ESOPWebFeedFTPServer);
            log("Send to FTP: " + ftp);
            log("SFTP Directory: " + ESOPWebFeedSFTPDirectory);
            log("Adestra SFTP Directory: " + AdestraWebFeedSFTPDirectory);
            log("SFTP Server: " + ESOPWebFeedSFTPServer);
            log("Send to SFTP: " + sftp);
            log("Username: " + ESOPWebFeedUserName);
            log("Password: " + ESOPWebFeePassword);
            log("Filepath: " + ESOPWebFeedFilePath);
            log("Adestra Filepath: " + AdestraWebFeedFilePath);
        }

        #endregion

        #region Public Methods

        public override ProcessEnums.enmStatus Execute()
        {
            DataSet ds = new DataSet();

            LogConnections();
            AssignEventCodes();    

            foreach (string code in EventCodes)
            {
                this.EventCode = code;

                ds = ExecuteEmbededResource("EventInformation.sql");
                WriteDataSet(ds, @"CTS_Webfeed2\booked_stands", true, false);

                ds = ExecuteEmbededResource("EventInformationAdestra.sql");
                WriteDataSet(ds, @"CTS_Webfeed2\Adestra\booked_stands", true, true);

                ds = ExecuteEmbededResource("Unbooked.sql");
                WriteDataSet(ds, @"CTS_Webfeed2\unbooked_stands", true, false);

                ds = ExecuteEmbededResource("HallInformation.sql");
                WriteDataSet(ds, @"CTS_Webfeed2\hall", true, false);

                ds = ExecuteEmbededResource("Categories.sql");
                WriteDataSet(ds, @"CTS_Webfeed2\categories", true, false);

                ds = ExecuteEmbededResource("ExhibitorCategories.sql");
                WriteDataSet(ds, @"CTS_Webfeed2\exhibitor_categories", true, false);
            }

            ds = ExecuteEmbededResource("BookedStandsIH.sql");
            WriteDataSet(ds, @"CTS_Webfeed2\booked_stands_IH", false, false);

            ds = ExecuteEmbededResource("CategoriesIH.sql");
            WriteDataSet(ds, @"CTS_Webfeed2\categories_IH", false, false);

            if (ConfigurationManager.AppSettings["SendtoSFTP"] == "1")
            {
                SendFilesToSFTP(ESOPWebFeedFilePath, ESOPWebFeedSFTPDirectory);
                SendFilesToSFTP(AdestraWebFeedFilePath, AdestraWebFeedSFTPDirectory);
            }

            return ProcessEnums.enmStatus.Successful;

        }

        #endregion

    }
}
