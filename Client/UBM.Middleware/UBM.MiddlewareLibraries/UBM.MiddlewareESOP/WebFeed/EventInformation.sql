﻿SELECT DISTINCT bn.Banner_Name_Id as [Banner Name Id], bn.Banner_Name AS [Exhibitor Name], 
               bn.Web_Username AS [Exhibitor Username], bn.Web_Password AS [Exhibitor Password], 
               ct.Exhibiting_Name, cm.Company_Name, s.Stand_Number, h.Hall_Name, e.Event_Code, 
               s.Ground_Dimension_X, s.Ground_Dimension_Y, s.Ground_Floor_Area, s.Net_Area, 
               pt.Product_Type_Name, ep.Event_Product_Name, ct.Status, cnAdmin.Title, cnAdmin.First_Name, 
               cnAdmin.Last_Name, cnAdmin.Address_1, cnAdmin.Address_2, cnAdmin.Address_3, cnAdmin.City, cnAdmin.County, 
               cnAdmin.Post_Code, cyAdmin.Country_Name, cyAdmin.Country_Code, cnAdmin.Phone, cnAdmin.Fax, cnAdmin.Email_text, cnWeb.Title AS Web_Title, 
               cnWeb.First_Name AS Web_First_Name, cnWeb.Last_Name AS Web_Last_Name, cnWeb.Address_1 AS Web_Address_1, 
               cnWeb.Address_2 AS Web_Address_2, cnWeb.Address_3 AS Web_Address_3, cnWeb.City AS Web_City, cnWeb.County AS Web_County, 
               cnWeb.Post_Code AS Web_Post_Code, cyWeb.Country_Name AS Web_Country, cyWeb.Country_Code as Web_Country_Code, cnWeb.Phone AS Web_Phone, cnWeb.Fax AS Web_Fax, 
               cnWeb.Email_text AS Web_Email, cmWeb.WWW AS Company_WWW, cmWeb.Email AS Company_Email, 
               cmWeb.Web_Username AS Company_Web_Username, cmWeb.Web_Password AS Company_Web_Password, 
               bn.Banner_Sharer AS [Exhibitor Type], CAST(s.Stand_Id AS BIGINT) AS [Stand Id], 
               bn.Exhibitor_Id AS [Exhibitor Id], cm.Phone AS Company_Phone, 
               e.Use_Company_Phone_Webfeed AS Use_Company_Phone, cast(cnAdmin.contact_id as bigint) as [contact_id], cast(cnWeb.contact_id as bigint) as [contactId],
               cm.Linkedin AS [Company Linkedin], cm.Facebook AS [Company Facebook], cm.Twitter AS [Company Twitter],
			   ct.Access_Code, cnOPS.Title AS OPS_Title, cnOPS.First_Name AS OPS_First_Name, cnOPS.Last_Name AS OPS_Last_Name, 
			   cnOPS.Address_1 AS OPS_Address_1, cnOPS.Address_2 AS OPS_Address_2, cnOPS.Address_3 AS OPS_Address_3, cnOPS.City AS OPS_City,
               cnOPS.County AS OPS_County, cnOPS.Post_Code AS OPS_Post_Code, cyOPS.Country_Name AS OPS_Country, cyOPS.Country_Code as OPS_Country_Code, 
			   cnOPS.Phone AS OPS_Phone, cnOPS.Fax AS OPS_Fax, cnOPS.Email_text AS OPS_Email, cast(cnOPS.contact_id as bigint)as[contact_2_id], 
			   ct.Retention_Status, ct.co_status, ez.Event_Zone_Name, eg.group_Name
FROM Banner_Name bn
INNER JOIN Stand_Sharers ss ON bn.Stand_Sharers_Id = ss.Stand_Sharers_Id 
INNER JOIN Stand s ON ss.Stand_Id = s.Stand_Id 
INNER JOIN Contract ct ON s.Contract_Id = ct.Contract_Id 
INNER JOIN Company cm ON s.Company_Id = cm.Company_Id 
INNER JOIN Hall h ON s.Hall_Id = h.Hall_Id 
INNER JOIN Event_Product ep ON s.Event_Product_Id = ep.Event_Product_Id 
INNER JOIN Event e ON ep.Event_Id = e.Event_Id
LEFT JOIN Event_Zone ez on s.Event_Zone_Id = ez.Event_Zone_Id
LEFT JOIN Event_Group eg on s.Event_Group_Id = eg.Event_Group_Id
INNER JOIN Product_Type pt ON s.Product_Type_Id = pt.Product_Type_Id 
INNER JOIN Contact cnWeb ON ss.Web_Contact = cnWeb.Contact_Id
INNER JOIN Country cyWeb ON cnWeb.Country_Id = cyWeb.Country_Id
INNER JOIN Company cmWeb ON cnWeb.Company_Id = cmWeb.Company_Id
INNER JOIN Contact cnAdmin ON ss.Admin_Contact = cnAdmin.Contact_Id 
INNER JOIN Country cyAdmin ON cnAdmin.Country_Id = cyAdmin.Country_Id
INNER JOIN Company cmAdmin ON cnAdmin.Company_Id = cmAdmin.Company_Id
INNER JOIN Contact cnOPS ON ss.OPS_Contact = cnOPS.Contact_Id 
INNER JOIN Country cyOPS ON cnOPS.Country_Id = cyOPS.Country_Id
INNER JOIN Company cmOPS ON cnOPS.Company_Id = cmOPS.Company_Id               
WHERE (e.Event_Code = '[**EVENT_CODE**]') AND (bn.Banner_Report_Flag = '1') 
ORDER BY e.Event_Code, s.Stand_Number
               