﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareSystem;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareMetrics
{

    //-- this routine calculates metrics and FTP's the details
    //-- for use with a WebService on ubmidevelopment.com
    public class FTPMetrics : UBM.MiddlewareProcess.ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            string status = ProcessAccess.GetMiddlewareStatus();

            string dated = DateTime.Now.ToString("ddddd dd mmm yyyy");
            string timed = DateTime.Now.ToString("hh:mm:ss");

            //var alljobs = from s in db.jobs where s.id == jobid select s;
            var jobs = SystemAccess.getJobsWithStatus("COMPLETE");
            string completedJobs = jobs.Count().ToString();

            jobs = SystemAccess.getJobsWithStatus("FAILED");
            string failedJobs = jobs.Count().ToString();

            string lastExecuted = "";

            jobs = SystemAccess.getJobsLastExecuted(this.Jobid);
            if (jobs.Count > 0) lastExecuted = jobs.SingleOrDefault().LastExecuted.ToString();



            return ProcessEnums.enmStatus.Successful;
        }


    }

}
