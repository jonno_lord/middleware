﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Management;
using System.Configuration;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Threading;
using System.Data;

namespace UBM.MiddlewareMetrics
{

    public enum MetricType
    {
        DiskSpace = 1,
        CPU = 2,
        ESOPCustFF = 3
    }


    public class Record : UBM.MiddlewareProcess.ProcessTask
    {
        PerformanceCounter cpuCounter;

      //  private static SqlConnection connection;

        public override ProcessEnums.enmStatus Execute()
        {
            //-- inserts metric value for both disk space and cpu utilisation
            InsertMetric(CalculateFreeDiskSpace(), MetricType.DiskSpace);

            float cpuUtilisation = CalculateCPUUtilisation();

            InsertMetric(cpuUtilisation, MetricType.CPU);

            return ProcessEnums.enmStatus.Successful;
        }

        //-- calculates and returns local cpu utilistaion in % using a performance counter
        private float CalculateCPUUtilisation()
        {
            cpuCounter = new PerformanceCounter();

            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            cpuCounter.NextValue();
            Thread.Sleep(100);

            return float.Parse(cpuCounter.NextValue().ToString());
        }

        //-- obtains value of free disk space based on disc specified in middleware.config DiskName
        private float CalculateFreeDiskSpace()
        {
            string diskName = ConfigurationManager.AppSettings["DiskName"];
            
            ManagementObject disk = new 
            ManagementObject("win32_logicaldisk.deviceid=\"" + diskName + ":\""); 
            disk.Get();
            float spaceInBytes = float.Parse(disk["FreeSpace"].ToString());
            float spaceInGB = spaceInBytes / 1073741824;
            return spaceInGB;
        }

        ////private void GetJobDuration()
        ////{
        ////    string connectionString = ConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ConnectionString;

        ////    SqlCommand command = new SqlCommand("usp_GET_BatchDuration", connection);
        ////    command.CommandType = CommandType.StoredProcedure;

        ////    ////command.Parameters.AddWithValue("@batchNumber", this.Batchid);
        ////    ////command.Parameters.AddWithValue("@jobid", this.Jobid);

        ////}

        private void InsertMetric(float value, MetricType metricType)
        {
            SqlParameter[] parms = new SqlParameter[2];
            parms[0] = new SqlParameter("Value", value);
            parms[1] = new SqlParameter("Enumid", metricType);
            this.FireStoredProcedure("usp_INS_Metrics", parms);
        }


    }
}
