﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareIPSOP
{
    internal static class Common
    {
        internal static string MiddlewareIPSOPConnection(int systemid)
        {
            return MiddlewareDataAccess.Connections.GetConnectionString(systemid);
        }
    }
}
