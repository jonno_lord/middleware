﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareIPSOP.Orders
{
    public class SourceToMiddleware:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("IPSOP Orders Source to Middleware.dtsx",
                 "SELECT * FROM Subscription_Service_Invoice WHERE ISNULL(Collected_by_MW ,0) = 0",
                Connections.GetConnectionString("IPSOP"));

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
