﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareIPSOP.Orders
{
    public class JDEToMiddleware:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareSourceConnection = "MiddlewareIPSOP";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("Common Orders JDE to Middleware.dtsx", true);
            return ProcessEnums.enmStatus.Successful;
        }
    }

}
