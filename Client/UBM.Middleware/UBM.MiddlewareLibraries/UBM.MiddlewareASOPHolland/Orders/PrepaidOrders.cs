﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPHolland.Orders
{
    public class PrepaidOrders : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareASOPConnection = "MiddlewareASOPHolland";
            this.ASOPConnection = "ASOPHolland";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Prepaid Orders Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
