﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPHolland.Customers
{
    public class MiddlewareToSource : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- set the version of ASOP we are talking to.
            this.MiddlewareASOPConnection = "MiddlewareASOPHolland";
            this.ASOPConnection = "ASOPHolland";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Customers Middleware to Source.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
