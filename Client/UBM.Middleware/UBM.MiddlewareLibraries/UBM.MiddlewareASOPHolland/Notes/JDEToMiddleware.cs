﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPHolland.Notes
{
    public class JDEToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareSourceConnection = "MiddlewareASOPHolland";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Credit Controller Notes JDE to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
