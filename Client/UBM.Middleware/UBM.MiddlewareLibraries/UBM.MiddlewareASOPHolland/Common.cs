﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareASOPHolland
{
    internal static class Common
    {
        internal static string MiddlewareASOPConnection(int systemid)
        {
            return MiddlewareDataAccess.Connections.GetConnectionString(systemid);
        }
    }
}
