﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPHolland.ReferenceData
{
    public class DataRefresh : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.ASOPConnection = "ASOPHolland";
            this.MiddlewareASOPConnection = "MiddlewareASOPHolland";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Reference Data.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
