﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareCARS.Customers
{
    public class SourceToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            //this.CARSConnectionString = ConfigurationManager.AppSettings["CARSConnectionString"];
                //ConfigurationSettings["SERVER"]["CARSConnection"];

            // If this fails it will throw an exception up.
            this.FireDTSPackage("CARS Customers Source to Middleware.dtsx");    

            return ProcessEnums.enmStatus.Successful;
        }
    }
}

