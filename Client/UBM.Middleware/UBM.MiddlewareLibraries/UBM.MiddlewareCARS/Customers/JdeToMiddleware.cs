﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareCARS.Customers
{
    public class JdeToMiddleware : ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {
            // Source connection needs to be defined for generic (common) packages.
            this.MiddlewareSourceConnection = "MiddlewareCARS";

            // If this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Customers JDE to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
