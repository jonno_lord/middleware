﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using System.Configuration;

namespace UBM.MiddlewareCARS.Customers
{
    public class MiddlewareToSource : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            //this.CARSConnectionString = ConfigurationManager.AppSettings["CARSConnectionString"];
            //ConfigurationSettings["SERVER"]["CARSConnection"];

            // If this fails it will throw an exception up.
            this.FireDTSPackage("CARS Customers Middleware to Source.dtsx");
            return ProcessEnums.enmStatus.Successful;
        }
    }
}
