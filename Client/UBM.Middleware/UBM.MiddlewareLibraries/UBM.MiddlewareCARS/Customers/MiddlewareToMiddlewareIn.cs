﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareCARS.Customers
{
    public class MiddlewareToMiddlewareIn  : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            // If this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_CustomersToJDEFormat",
                                     "SELECT TOP 10 * FROM Customers WHERE Stage = 1",
                                     Connections.GetConnectionString("MiddlewareCARS"));

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
