﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareCARS.Customers
{
    public class MiddlewareToMiddlewareOut : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            // If this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_CustomersToCARSFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
