﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareCARS
{
    internal static class Common
    {
        internal static string MiddlewareCARSConnection(int systemid)
        {
            return MiddlewareDataAccess.Connections.GetConnectionString(systemid);
        }
    }
}
