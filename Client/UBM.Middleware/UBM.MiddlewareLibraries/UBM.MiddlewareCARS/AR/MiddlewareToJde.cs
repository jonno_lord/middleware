﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareCARS.AR
{
    public class MiddlewareToJde : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            // Set the source of this package.
            this.MiddlewareSourceConnection = "MiddlewareCARS";

            // If this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Accounts Receivable Middleware to JDE.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }
}