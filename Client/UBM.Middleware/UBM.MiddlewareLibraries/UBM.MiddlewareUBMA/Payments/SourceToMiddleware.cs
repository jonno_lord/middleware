﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareUBMA.Payments
{
    public class SourceToMiddleware : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("UBMA Payments Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;

        }
    }
}
