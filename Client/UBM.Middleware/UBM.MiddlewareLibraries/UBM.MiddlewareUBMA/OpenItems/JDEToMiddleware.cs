﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using System.Data.SqlClient;
using UBM.MiddlewareDataAccess;
using System.Configuration;
using System.IO;


namespace UBM.MiddlewareUBMA.OpenItems
{
    public class JDEToMiddleware : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            //-- set the source of where the customers are going
            this.MiddlewareSourceConnection = "MiddlewareUBMA";

            this.FireDTSPackage("COMMON Open Items JDE to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }


        #region Private Properties

        private string MSIFilesDirectory
        {
            get
            {
                string path = ConfigurationManager.AppSettings["MSIFilesDirectory"].ToString().Trim();
                if (!path.EndsWith(@"\")) path += @"\";
                if (path.Length == 0)
                    throw new Exception("MSIFileDirectory configuration setting is empty.");
                return path;
            }
        }

        private string UBMAConnectionString
        {
            get 
            {
                return this.RemoveProvider(Connections.GetConnectionString(this.SystemId));
            }
        }

        #endregion

        #region Private Members

        //** process the file MarkAsProcessed
        private void ProcessMSIFile(string fileName)
        {

            //MSIFileConnection
            log("Processing file " + fileName);

            //-- for each file, supply it to the packagae and then export it out to the F03b11 in Middleware
            this.FileConnection = this.MSIFilesDirectory + fileName;
            this.FireDTSPackage("COMMON Open Items JDE to Middleware.dtsx", true);

            MarkAsProcessed(fileName);
        }

        private void MarkAsProcessed(string fileName)
        {
            //-- open sp and retrieve whether this file has been processed or not
            using (SqlConnection cn = new SqlConnection(UBMAConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("usp_INS_FileProcessed", cn))
                {
                    cmd.CommandType = global::System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FileName", fileName);
                    cmd.Parameters.AddWithValue("@filePath", this.MSIFilesDirectory);
                    cmd.ExecuteNonQuery();
                }
            }

        }

        private bool FileProcessed(string fileName)
        {
            
            bool fileHasBeenProcessed = false;

            //-- open sp and retrieve whether this file has been processed or not
            using(SqlConnection cn = new SqlConnection(UBMAConnectionString)){
                cn.Open();
                using(SqlCommand cmd = new SqlCommand("usp_SEL_FileProcessed", cn))
                {
                    cmd.CommandType = global::System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FileName", fileName);
                    SqlDataReader dr = cmd.ExecuteReader();

                    if(dr.Read()) fileHasBeenProcessed = true;

                }
            }

            return fileHasBeenProcessed;

        }
        #endregion

        public ProcessEnums.enmStatus OldExecute()
        {

            //-- set the source of where the customers are going
            this.MiddlewareSourceConnection = "MiddlewareUBMA";

            log("checking directory " + this.MSIFilesDirectory);
            if (!Directory.Exists(this.MSIFilesDirectory))
            {
                throw new Exception("Directory specified [" + this.MSIFilesDirectory + "] is not accessible.");
            }

            //** process each file in the directory
            string[] fileEntries = Directory.GetFiles(this.MSIFilesDirectory);

            foreach (string fileName in fileEntries)
            {
                string MsifileName = fileName.ToUpper().Substring(fileName.LastIndexOf(@"\") +1);


                //** check to make sure this is a detail MSI file (this file starts with MSID)
                //** - if so also check to make sure it has a TXT suffix
                if (MsifileName.StartsWith("MSID") && MsifileName.EndsWith(".CSV"))
                {
                    if (!FileProcessed(MsifileName))
                    {
                        ProcessMSIFile(MsifileName);
                    }
                    else
                    {
                        log("File already processed " + fileName);
                    }
                }
                else
                {
                    log("Ignoring " + fileName);
                }
            }
            return ProcessEnums.enmStatus.Successful;
        }

    }


}
