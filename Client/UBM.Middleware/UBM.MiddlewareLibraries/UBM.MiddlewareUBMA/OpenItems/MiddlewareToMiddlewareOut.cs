﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareUBMA.OpenItems
{
    class MiddlewareToMiddlewareOut : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_OpenItemsToUBMAFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}