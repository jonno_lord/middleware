﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareUBMA.Revenue
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.FireStoredProcedure("usp_TRA_RevenueToJDEFormat",
                   "SELECT TOP 10 * FROM Revenues WHERE Stage = 1",
                   Connections.GetConnectionString("MiddlewareUBMA"));

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
