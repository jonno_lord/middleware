﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareUBMA.Revenue
{
    public class SourceToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.FireDTSPackage("UBMA Revenue Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
