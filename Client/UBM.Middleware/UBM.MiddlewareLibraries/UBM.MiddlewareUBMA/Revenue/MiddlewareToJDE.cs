﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareUBMA.Revenue
{
    public class MiddlewareToJDE : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareSourceConnection = "MiddlewareUBMA";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("Common Revenue Recognition Middleware to JDE.dtsx", true);

            return ProcessEnums.enmStatus.Successful;

        }
    }
}
