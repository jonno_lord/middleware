﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareUBMA.Customers
{
    public class MiddlewareToJDE : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- the source connection name
            this.MiddlewareSourceConnection = "MiddlewareUBMA";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Customers Middleware to JDE.dtsx",
                "SELECT TOP 1 * FROM Customers WHERE Stage = 2", 
                Connections.GetConnectionString("MiddlewareUBMA"), true);
              
            return ProcessEnums.enmStatus.Successful;
        }
    }

}
