﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareUBMA.Customers
{
    public class MiddlewareToSource : ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("UBMA Customers Middleware to Source.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}