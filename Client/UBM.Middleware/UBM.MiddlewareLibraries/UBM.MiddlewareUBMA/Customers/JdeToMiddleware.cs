﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareUBMA.Customers
{
    public class JdeToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- set the source of where the customers are going
            this.MiddlewareSourceConnection = "MiddlewareUBMA";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Customers JDE to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
