﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareUBMA.AR
{
    public class SourceToMiddleware : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("UBMA Accounts Receivable Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;

        }
    }
}
