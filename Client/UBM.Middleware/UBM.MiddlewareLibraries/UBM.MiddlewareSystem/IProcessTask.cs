﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareSystem
{
    public interface IProcessTask
    {
        int processid { get; set; }
        int jobid { get; set; }
        int batchid { get; set; }

        void Execute(int processid, int jobid, int batchid);
    }
}
