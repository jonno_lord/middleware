﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewarePaymentPortal.AR
{

    public class MiddlewareToMiddlewareIn : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            this.FireStoredProcedure("usp_TRA_AccountsReceivableToJDEFormat",
                  "SELECT TOP 10 * FROM Portal_Credit_Card_Orders WHERE Stage = 1",
                  Connections.GetConnectionString("MiddlewarePaymentPortal"));

            return ProcessEnums.enmStatus.Successful;

        }
    }

}
