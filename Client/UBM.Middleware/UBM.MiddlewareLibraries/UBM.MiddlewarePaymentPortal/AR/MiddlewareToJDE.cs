﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewarePaymentPortal.AR
{
    public class MiddlewareToJDE : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareSourceConnection = "MiddlewarePaymentPortal";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Accounts Receivable Middleware to JDE.dtsx", true);

            return ProcessEnums.enmStatus.Successful;

        }
    }
}
