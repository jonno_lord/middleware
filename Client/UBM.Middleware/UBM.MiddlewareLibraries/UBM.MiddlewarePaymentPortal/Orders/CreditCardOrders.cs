﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewarePaymentPortal.Orders
{
    public class CreditCardOrders:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("Payment Portal Credit Card Orders Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
