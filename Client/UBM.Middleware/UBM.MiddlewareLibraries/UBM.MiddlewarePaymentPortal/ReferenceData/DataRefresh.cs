﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewarePaymentPortal.ReferenceData
{
    public class DataRefresh : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("Payment Portal Reference Data Refresh.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
