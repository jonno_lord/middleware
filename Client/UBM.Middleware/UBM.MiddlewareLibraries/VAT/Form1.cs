﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UBM.MiddlewareScrutiny;

namespace VAT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UBM.MiddlewareScrutiny.VerifyVATNumber vat = new VerifyVATNumber();
            vat.CheckVatNumber("1234567-8", "GB");
        }
    }
}
