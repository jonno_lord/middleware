﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace UBM.MiddlewareScrutiny
{
    /// <summary>
    /// This interface is used for calculating custom scrutiny rules (i.e. non-sql-query based
    /// rules. This could be for example a rule to validate a VAT number via a web-service.
    /// It is not necessarily required to implement a class in this library (MiddlewareScrutiny)
    /// to execute a custom rule, as the ScrutinyRules structure allows the AssemblyName of
    /// the customScrutiny rule to be specified, allowing implementation in another namespace /
    /// library is so desired. The only overhead is that the results from this query must be
    /// returned in a DataSet - which is not perfect, but allows a structured loose data set
    /// from any type of query.
    /// 
    /// Note: if your custom scrutiny rule is a violation, the dataset should return a row
    /// if it breaks the rule, otherwise it should return no rows.
    ///     
    ///         The first column (row[0][0]) should contain a text description of the Violation.
    /// 
    /// if your custom scrutiny rule is a duplicate, it should return the customer duplicates
    /// that the supplied customer clashes with in a DataSet format. The field should be in the format
    /// of
    ///         CustomerName
    ///         AddressLine1
    ///         AddressLine2
    ///         AddressLine3
    ///         AddressLine4
    ///         Postcode
    ///         Country
    ///         Url
    ///         Email
    ///         Telephone
    /// </summary>
    interface ICustomScrutiny
    {
        DataSet GetScrutinyResults(System.Data.DataSet customer, CustomerScrutinyRule rule, int SystemId);
    }
}
