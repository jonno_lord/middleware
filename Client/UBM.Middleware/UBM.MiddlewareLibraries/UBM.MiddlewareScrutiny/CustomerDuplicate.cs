﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareSystem;

namespace UBM.MiddlewareScrutiny
{
    public class CustomerDuplicate : IScrutinyRule 
    {
        public string CustomerReferenceNumber { get; set; }
        public int SystemNameId { get; set; }

        
        public bool Expired{ get; set; }

        public string CustomerName{ get;set;}
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string JDEAccountNumber { get; set; }
        public int CustomerScrutinyRuleId { get; set; }

        public bool isJDE { get;set; }



        public CustomerDuplicate(DataRow row, int customerscrutinyruleid, int systemnameid)
        {
            //-- begin to process the row of returned data.
            this.CustomerScrutinyRuleId = customerscrutinyruleid;
            this.SystemNameId = systemnameid;
            this.CustomerReferenceNumber = rowValue(row, "CustomerRef", 0);
            this.CustomerName = rowValue(row, "CustomerName", 1);
            this.AddressLine1 = rowValue(row, "AddressLine1", 2);
            this.AddressLine2 = rowValue(row, "AddressLine2", 3);
            this.AddressLine3 = rowValue(row, "AddressLine3", 4);
            this.AddressLine4 = rowValue(row, "AddressLine4", 5);
            this.Postcode = rowValue(row, "Postcode", 6);
            this.Country = rowValue(row, "Country", 7);
            this.Url = rowValue(row, "Url", 8);
            this.Email = rowValue(row, "Email", 9);
            this.Telephone = rowValue(row, "Telephone", 10);
            this.JDEAccountNumber = rowValue(row, "JdeAccountNumber", 11);

            //-- bodge. 1:30 am - tired.
            UBM.MiddlewareSystem.system systemName = SystemAccess.getSystem(SystemNameId);
            this.isJDE = systemName.ShortName.ToUpper().Contains("JDE");

            //-- 
            if (this.isJDE)
            {

                this.JDEAccountNumber = this.CustomerReferenceNumber;

                try
                {
                    //-- bodge to find out if an account is inactive (expired)
                    if (row["SearchType"].ToString().Substring(0, 1) == "Z")
                        this.Expired = true;

                }
                catch (Exception ex)
                {
                    MiddlewareLogging.Logger.ErrorLog(ex);
                }

            }

        }

        private string rowValue(DataRow row,string columnname, int columnno)
        {
            if(row.Table.Columns.Contains(columnname))
            {
                return row[columnname].ToString();
            }

            if(columnno < row.ItemArray.GetUpperBound(0))
                return row[columnno].ToString();

            return String.Empty;
        }
    }
}
