﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using UBM.MiddlewareSystem;
using System.Reflection;

namespace UBM.MiddlewareScrutiny
{
    internal class RulesCalculator
    {

        private int SystemId { get; set; }
        private int CustomerId { get; set; }
        private int BatchId { get; set; }
        private int ProcessId { get; set; }
        private int JobId { get; set; }
        internal List<Exception> Errors { get; set; }

        public string DLLDirectory { get; set; }

        private List<CustomerDuplicate> customerduplicates = null;
        private List<CustomerViolation> customerviolations = null;

        private DataSet Customer { get; set; } //-- loosely coupled Customer definition to deal with all sorts of data.

        private int TotalRulesBroken
        {
            get{ 
                int duplicates = ((customerduplicates == null) ? 0 : customerduplicates.Count);
                int violations = ((customerviolations == null) ? 0 : customerviolations.Count);
                return duplicates + violations;
            }
        }

        private string RemoveProvider(string connectionString)
        {
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connectionString;
        }

        internal void CommitScrutiny()
        {
            SqlConnection cn = null;
            SqlTransaction trn = null;
            try
            {
                //--BeginTransaction - at the moment, I'm not sure how to do Transactional
                //-- type processing outside of our SQLHelper class, as the connections
                //-- are opened and closed per command. Transactional requires all per same
                //-- connection...
                string con = this.RemoveProvider(ConnectionStringFromSystemId(this.SystemId));
                using(cn = new SqlConnection(con))
                {
                    cn.Open();
                    trn = cn.BeginTransaction();

                    SqlCommand cmd = cn.CreateCommand();
                    cmd.Transaction = trn;

                    ClearDuplicates(this.CustomerId, cmd);

                    ClearViolations(this.CustomerId, cmd);

                    //-- commit duplicates to the CustomerDuplicates table
                    InsertDuplicates(this.customerduplicates, this.CustomerId, cmd);

                    //-- commit violations to the customer violations table
                    InsertViolations(this.customerviolations, this.CustomerId, cmd);

                    //--CommitTransaction - set the inScrutiny flags, last updated etc.
                    SetScrutinyDetails(this.CustomerId, cmd);
                    trn.Commit();

                }
            }
            catch (Exception ex)
            {
                log("failed to commit scrutiny details. :" + ex.Message.ToString());
                MiddlewareLogging.Logger.ErrorLog(ex);
                Errors.Add(ex);

                if (trn != null) trn.Rollback();

                if (cn != null && cn.State != ConnectionState.Closed) cn.Close();
            }
        }

        private void SetScrutinyDetails(int customerid, SqlCommand cmd)
        {
            cmd.CommandText = "UPDATE Customers SET NumberOfScrutinyRulesBroken=" + (this.TotalRulesBroken.ToString()) + ", MiddlewareScrutiny = GETDATE() WHERE id = " + customerid.ToString();
            cmd.ExecuteNonQuery();
        }

        private void InsertViolations(List<CustomerViolation> violations, int customerid, SqlCommand cmd)
        {
            foreach (CustomerViolation violation in violations)
            {
                cmd.CommandText = "INSERT INTO CustomerViolations(CustomerId, CustomerScrutinyRuleId, Violation)";
                cmd.CommandText += "VALUES (";
                cmd.CommandText += this.CustomerId.ToString() + ",";
                cmd.CommandText += violation.CustomerScrutinyRuleId.ToString() + ",";
                cmd.CommandText += "'" + dq(violation.ViolationMessage) + "')";

                cmd.ExecuteNonQuery();
            }
        }

        private void InsertDuplicates(List<CustomerDuplicate> duplicates, int customerid, SqlCommand cmd)
        {
            foreach (CustomerDuplicate duplicate in duplicates)
            {
                cmd.CommandText = "INSERT INTO CustomerDuplicates (CustomerId, CustomerScrutinyRuleId, SystemNameId, CustomerReferenceNumber, CustomerName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Postcode,Country,Telephone,Email,URL, JDEAccountExpired, JDEAccountNumber)";
                cmd.CommandText += "VALUES (";
                cmd.CommandText += this.CustomerId.ToString() + ",";
                cmd.CommandText += duplicate.CustomerScrutinyRuleId.ToString() + ",";
                cmd.CommandText += dq(duplicate.SystemNameId.ToString()) + ",";
                cmd.CommandText += "'" + dq(duplicate.CustomerReferenceNumber) + "',";
                cmd.CommandText += "'" + dq(duplicate.CustomerName) + "',";
                cmd.CommandText += "'" + dq(duplicate.AddressLine1) + "',";
                cmd.CommandText += "'" + dq(duplicate.AddressLine2) + "',";
                cmd.CommandText += "'" + dq(duplicate.AddressLine3) + "',";
                cmd.CommandText += "'" + dq(duplicate.AddressLine4) + "',";
                cmd.CommandText += "'" + dq(duplicate.Postcode) + "',";
                cmd.CommandText += "'" + dq(duplicate.Country) + "',";
                cmd.CommandText += "'" + dq(duplicate.Telephone) + "',";
                cmd.CommandText += "'" + dq(duplicate.Email) + "',";
                cmd.CommandText += "'" + dq(duplicate.Url) + "',";
                cmd.CommandText += "" + (duplicate.Expired == true ?  '1' : '0') + ",";
                cmd.CommandText += "'" + (duplicate.JDEAccountNumber) +"')";

                cmd.ExecuteNonQuery();

            }
        }

        private string dq(string input)
        {
            return input.Replace("'", "''");
        }

        private void ClearDuplicates(int customerid, SqlCommand cmd)        
        {
            cmd.CommandText = "DELETE FROM CustomerDuplicates WHERE CustomerId = " + customerid.ToString();
            cmd.ExecuteNonQuery();
        }

        private void ClearViolations(int customerid, SqlCommand cmd)
        {
            cmd.CommandText = "DELETE FROM CustomerViolations WHERE CustomerId = " + customerid.ToString();
            cmd.ExecuteNonQuery();
        }

        internal RulesCalculator(int systemid, int customerid, string dllDirectory, int batchid, int processid, int jobid)
        {
            this.SystemId = systemid;
            this.CustomerId = customerid;
            this.DLLDirectory = dllDirectory;

            this.JobId = jobid;
            this.BatchId = batchid;
            this.ProcessId = processid;
            this.Errors = new List<Exception>();

        }

        private string ConnectionStringFromSystemId(int systemid)
        {
            string connection = "";
            try
            {
                connection = MiddlewareDataAccess.Connections.GetConnectionString(systemid);
                connection = this.RemoveProvider(connection);
            }
            catch(Exception ex)
            {
                Errors.Add(ex);
                log("Could not find connection string details for system id " + systemid.ToString() + ". Please check .config and Systems tables");
                MiddlewareLogging.Logger.ErrorLog(ex);
            }
            return connection;
        }

        private void log(string message)
        {
            //-- log if we have batch details enabled
            if(this.BatchId > 0) MiddlewareLogging.Logger.BatchLog(this.JobId, this.ProcessId, this.BatchId, 
                    MiddlewareDataAccess.ProcessAccess.enmLevel.Standard, message);
        }

        internal void LoadCustomer()
        {
            //TODO: Set connection string based on System Id
            log("loading customer " + CustomerId);
            string connectionstring = MiddlewareDataAccess.Connections.GetConnectionString(this.SystemId);
            connectionstring = this.RemoveProvider(connectionstring);
            Customer = MiddlewareDataAccess.CustomerAccess.GetCustomer(CustomerId, connectionstring);
        }

        internal int CalculateDuplicates(List<CustomerScrutinyRule> duplicates)
        {

            customerduplicates = new List<CustomerDuplicate>(); 

            if(duplicates != null) log("found " + duplicates.Count +" duplicate rules");
            foreach (CustomerScrutinyRule rule in duplicates)
            {

                DataSet results = null;
                log("processing " + rule.Description);

                if (rule.IsQueryBased)
                {
                    string ExpandedSQL = ExpandSQL(rule.Query);

                    log("expansion was " + ExpandedSQL);
                    //-- now we have the expanded SQL (containing all the refernces to execute).
                    //-- Fire this SQL on the connection provided
                    log("executing SQL on " + this.ConnectionStringFromSystemId(rule.QuerySystemid));

                    results = UBM.MiddlewareDataAccess.CustomerAccess.GetScrutinyResults(ExpandedSQL, this.ConnectionStringFromSystemId(rule.QuerySystemid));
                }
                else
                {
                    string execPath = this.DLLDirectory;
                    Assembly assembly = Assembly.LoadFrom(execPath + "bin\\" + rule.AssemblyName.ToString() + ".dll");
                    ICustomScrutiny processScrutiny = (ICustomScrutiny)assembly.CreateInstance(rule.AssemblyName.ToString() + "." + rule.ClassName.ToString());
                    results = processScrutiny.GetScrutinyResults(this.Customer, rule, this.SystemId);
                }

                //-- now process each row
                foreach (DataRow row in results.Tables[0].Rows)
                {
                    log("adding duplicate " + row[0].ToString());
                    customerduplicates.Add(new CustomerDuplicate(row, rule.id, rule.QuerySystemid));
                }

            }


            //-- duplicates are now all calculated
            return customerduplicates == null ? 0 : customerduplicates.Count;
        }

        internal int CalculateViolations(List<CustomerScrutinyRule> violations)
        {
            customerviolations = new List<CustomerViolation>();

            if (violations != null) log("found " + violations.Count + " violation rules");
            foreach (CustomerScrutinyRule rule in violations)
            {
                log("processing " + rule.Description);

                DataSet results = null;

                if (rule.IsQueryBased)
                {
                    string ExpandedSQL = ExpandSQL(rule.Query);
                    log("expansion was " + ExpandedSQL);
                    //-- now we have the expanded SQL (containing all the refernces to execute).
                    //-- Fire this SQL on the connection provided
                    log("executing SQL on " + this.ConnectionStringFromSystemId(rule.QuerySystemid));
                    results = UBM.MiddlewareDataAccess.CustomerAccess.GetScrutinyResults(ExpandedSQL, this.ConnectionStringFromSystemId(rule.QuerySystemid));
                }
                else
                {
                    string execPath = this.DLLDirectory;
                    Assembly assembly = Assembly.LoadFrom(execPath + "\\" + rule.AssemblyName.ToString() + ".dll");
                    ICustomScrutiny processScrutiny = (ICustomScrutiny)assembly.CreateInstance(rule.AssemblyName.ToString() + "." + rule.ClassName.ToString());
                    results = processScrutiny.GetScrutinyResults(this.Customer, rule, this.SystemId);
                }

                //-- if there are violations
                if (results.Tables.Count > 0)
                {
                    foreach (DataRow row in results.Tables[0].Rows)
                    {
                        log("adding violation " + row[0].ToString());
                        customerviolations.Add(new CustomerViolation(row[0].ToString(), rule.id));
                    }
                }//-- 

            }

            //-- duplicates are now all calculated
            return customerviolations == null ? 0 : customerviolations.Count;
        }

        private string ExpandSQL(string statement)
        {
            //-- all SQL is case insensitive.
            statement = statement.ToUpper();
            log("processing " + statement);
            foreach (DataColumn column in Customer.Tables[0].Columns)
            {
                string value=Customer.Tables[0].Rows[0][column].ToString();
                value = value.Replace("'", "''"); // -- are we still building inline SQL here? 2am :(
                statement = statement.Replace("*" + column.ColumnName.ToUpper() + "*", value);
            }
            return statement;
        }
    }

}
