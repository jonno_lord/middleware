﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareScrutiny
{
    public class CustomerViolation : IScrutinyRule 
    {
        public int CustomerScrutinyRuleId { get; set; }
        public string ViolationMessage { get; set; }

        public CustomerViolation(string violationmessage, int customerscrutinyruleid)
        {
            this.CustomerScrutinyRuleId = customerscrutinyruleid;
            this.ViolationMessage = violationmessage;
        }
    }
}
