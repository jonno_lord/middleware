﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net;
using UBM.MiddlewareScrutiny.Europa;

namespace UBM.MiddlewareScrutiny
{
    public class VerifyVATNumber : ICustomScrutiny 
    {

        public DataSet GetScrutinyResults(DataSet customer, CustomerScrutinyRule rule, int SystemId)
        {
            //-- no batch, job or processid supplied.
            return GetScrutinyResults(customer, rule, SystemId, 0, 0, 0);
        }


        /// <summary>
        /// Calculates the Scrutiny results for a customer with regards to their VAT number.
        /// currently only coded for ESOP.
        /// </summary>
        /// <param name="customer">The customer to validate</param>
        /// <param name="rule">The rule that defines what should be checked</param>
        /// <param name="SystemId">The id of the system in the system table</param>
        /// <returns>0 rows (scrutiny passed) - >0 rows (scrutiny failed)</returns>
        public DataSet GetScrutinyResults(DataSet customer, CustomerScrutinyRule rule, int SystemId, int jobid, int processid, int batchid)
        {

            DataSet results = new DataSet();

            //-- determine which field we need, based from the systemId
            UBM.MiddlewareSystem.system system = MiddlewareDataAccess.SystemAccess.getSystem(SystemId);

            string vatNumber = "";

            //-- not pretty, but there is no System Enumerator.
            switch (system.ShortName.ToLower())
            {
                case "middlewareesop":
                    //-- use the UK one where specified, else use the EC version.
                    vatNumber = customer.Tables[0].Rows[0]["EC_Vat_Registration_No"].ToString();
                    break;

                case "middlewareasopdaltons":
                    vatNumber = customer.Tables[0].Rows[0]["Vat_Reg_No"].ToString();
                    break;

                default:
                    throw new NotImplementedException();
            }

            //-- if there is no vat Number we cannot check its vailidity (first 2 characters are country code
            //-- next numbers are VAT number
            if (vatNumber.Length < 3) return results;

            string numberPart = vatNumber.Substring(2).Trim();
            string countryPart = vatNumber.Substring(0, 2).Trim();

            //-- perform the check on the Vat Number
            string message = CheckVatNumber(numberPart, countryPart);
            if (message.Length > 0)
            {
                results.Tables.Add();
                results.Tables[0].Columns.Add("Message");
                results.Tables[0].Rows.Add();

                results.DataSetName = "ViolationResult";
                results.Tables[0].Rows[0]["Message"] = message;
            }

            //-- if the results has rows, we broke violations.
            return results;
        }

        /// <summary>
        /// Checks the validity of a VAT number through the europa web service.
        /// </summary>
        /// <param name="vatNumber">the VAT number to check</param>
        /// <returns>a blank string if it is valid, or a description if not.</returns>
        public string CheckVatNumber(string vatNumber, string countryCode) 
        {

            string result = "";     //-- return result

            try
            {
                Europa.VATCheck vc = new VATCheck();
                result = vc.VatCheck(vatNumber, countryCode);

            }
            catch (Exception ex)
            {
                //-- store the fault.
                string fault = ex.Message;
                if (ex.Message.Contains("MS_UNAVAILABLE")) fault = "VAT validation service for this country is offline";

                result = "Could not validate VAT Number:" + fault;
                MiddlewareLogging.Logger.ErrorLog(ex);
            }

            return result;

        }

    }
}
