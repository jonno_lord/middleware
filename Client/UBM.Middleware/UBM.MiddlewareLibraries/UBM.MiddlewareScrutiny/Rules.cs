﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareLogging;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareScrutiny
{
    public static class Rules
    {

        public const int SCRUTINY_FAILED = -1;


        public static int Calculate(int systemId, int customerId)
        {

            List<Exception> errs = null;
            String exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            string dir = System.IO.Path.GetDirectoryName(exePath);

            return Calculate(systemId, customerId, out errs, dir);

        }

        public static int Calculate(int systemId, int customerId, string dllPath)
        {
            List<Exception> errs = null;
            return Calculate(systemId, customerId, out errs, dllPath);
        }


        internal static string RemoveProvider(string connectionString)
        {
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connectionString;
        }

        /// <summary>
        /// Calculates the scrutiny for a given system, based on its Customer entry
        /// details as sent by a customer Extraction. This uses the CustomerScrutinyRules table
        /// defined in its system database (e.g. MiddlewareESOP, MiddlewareASOP etc) - and will
        /// create entries in CustomerDuplicates and Customer Violations as entries are breached.
        /// It will remove entries from these tables that no longer break scrutiny also. (TBC: DG + JP).
        /// When complete, it will also update the Customers "InScrutiny" flag.
        /// </summary>
        /// <param name="systemId">The ID that relates to the system</param>
        /// <param name="customerId">The ID of the customer in that system</param>
        /// <param name="BatchId">The id of the batch - only required for Job Processing</param>
        /// <param name="JobId">The id of the job - only required for Job Processing</param>
        /// <param name="ProcessId">The id of the process - only required for Job Processing</param>
        /// <returns></returns>
        public static int Calculate(int systemId, int customerId, out List<Exception> errors, string dllDirectory,
                                int batchid=0, int jobid=0, int processid=0)
        {

            int rulesBrokenCount = 0;

            //-- instantiation only sets values.
            RulesCalculator calculator = new RulesCalculator(systemId, customerId, dllDirectory, batchid, processid, jobid);

            errors = null;

            //TODO: Set Context of LINQ dependent on System.


            try
            {

                //-- attempt to load the customer. If this fails, capture on the exception
                calculator.LoadCustomer();

                //-- get duplication rules
                string con = Rules.RemoveProvider(Connections.GetConnectionString(systemId));
                UBM.MiddlewareScrutiny.ScrutinyRulesDataContext db = new UBM.MiddlewareScrutiny.ScrutinyRulesDataContext(con);

                var duplicateRules = from d in db.CustomerScrutinyRules where (d.Disabled == false && d.ReturnsDuplicates == true) select d;
                List<CustomerScrutinyRule> duplicates = duplicateRules.ToList<MiddlewareScrutiny.CustomerScrutinyRule>();
                rulesBrokenCount += calculator.CalculateDuplicates(duplicates);

                //-- get violation rules
                var violationRules = from v in db.CustomerScrutinyRules where (v.Disabled == false && v.ReturnsViolation == true) select v;
                List<CustomerScrutinyRule> violations = violationRules.ToList<MiddlewareScrutiny.CustomerScrutinyRule>();
                rulesBrokenCount += calculator.CalculateViolations(violations);

                //-- write the scrutiny to the Customer Tables
                calculator.CommitScrutiny();

            }
            catch (Exception ex)
            {
                //-- if this was part of the batch (system processing) process, log it with exception details.
                if (batchid != 0)
                {
                    Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Critical, "Failed to Calculate scrutiny for Customer Id " + 
                                    customerId + " System " + systemId + ":" + ex.Message.ToString());
                }
                errors = calculator.Errors;
                Logger.ErrorLog(ex);
            }

            //-- return the total number of duplicates and violations made. If there are errors
            //-- do NOT reurn 0, as this could be a sign that everything was ok. return an arbitary
            //-- value- e.g -1 or 999 at least if the calling function does not respect the return errors
            //-- collection, it can use the FAILED_SCRUTINY_VALUE. 
            if (errors != null)
                return SCRUTINY_FAILED;
            else
                return rulesBrokenCount;
        }
    }
}
