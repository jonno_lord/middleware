﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareJDE.ReferenceData
{
    public class JDERefresh : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("JDE Reference Data Refresh.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }


    }
}
