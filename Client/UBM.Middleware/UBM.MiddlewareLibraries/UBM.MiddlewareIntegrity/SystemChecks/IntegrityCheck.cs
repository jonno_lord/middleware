﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.ServiceProcess;
using System.Data;
using System.Reflection;
using System.Net;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareIntegrity.SystemChecks
{
    public class IntegrityCheck : ProcessTask
    {

        private static SqlConnection connection;

        public string BaseSiteUrl
        {
            get
            {
                string baseUrl = "";
                HttpContext context = HttpContext.Current;
                if (context.Request.ApplicationPath != null)
                {
                    baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
                }
                return baseUrl;
            }
        }

        public override ProcessEnums.enmStatus Execute()
        {
            bool[] checks = new bool[5];

            //checks whether classes can be instantiated
            checks[0] = ClassChecker(Jobid, Processid, Batchid);

            //checks connection strings can connect
            checks[1] = ConnectionStringCheck(Jobid, Processid, Batchid);

            //checks middlewareJDE has data
            checks[2] = JDEDataCheck(Jobid, Processid, Batchid);

            //checks scheduler is running
            checks[3] = SchedulerCheck(Jobid, Processid, Batchid);

            //checks web services respond
            checks[4] = WebServiceCheck(Jobid, Processid, Batchid);

            for (int i = 0; i < 5; i++)
            {
                if (checks[i] == false)
                    throw new Exception("Integrity checks failed. Please review batch for details");
            }

            return ProcessEnums.enmStatus.Successful;
        }

        /// <summary>
        /// Checks whether MiddlewareJDE tables have data by selecting all 
        /// and checking if data can be found
        /// </summary>
        /// <returns>boolean true if all return data</returns>
        public bool JDEDataCheck(int jobid, int processid, int batchid)
        {
            bool hasData = false;
            int counter = 0;

            log("JDE Data Check");

            string connectionString = Connections.GetConnectionString("MiddlewareJDE");
            connectionString = RemoveProvider(connectionString);

            string[] storedProcedures = new string[] { "F0005", "F0006", "F0010", "F0013", "F0014", "F0101", "F0111", "F0115", "F0116", "F03b30", "F09190", "F4008" };

            foreach (string storedProcedure in storedProcedures)
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("SELECT TOP 10 * FROM " + storedProcedure, connection);
                    command.CommandType = CommandType.Text;
                    SqlDataReader sdrCheckData = command.ExecuteReader();

                    if (sdrCheckData.Read())
                    {
                        counter++;
                    }
                    else
                    {
                        MiddlewareLogging.Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Serious, "No data returned from" + storedProcedure + " in MiddlewareJDE");
                    }
                }
            }

            if (counter == storedProcedures.Length)
            {
                hasData = true;
            }

            if (!hasData)
            {
                throw new Exception("Data missing from JDE tables, review Log");
            }

            return hasData;
        }

        ///// <summary>
        ///// Gets all processes that are not disabled and checks that they can be instantiated
        ///// </summary>
        ///// <returns></returns>
        public bool ClassChecker(int jobid, int processid, int batchid)
        {
            string connectionString = Connections.GetConnectionString("MiddlewareSystem");
            string diskName = ConfigurationManager.AppSettings["DiskName"];

            int failCount = 0; //-- store the numbers of failed instiations

            log("class Checker");

            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("usp_SEL_ProcessDisabled", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader sdrClass = command.ExecuteReader();

                while (sdrClass.Read())
                {
                    // attempt to instiate the objects
                    string assemblyName = sdrClass["assemblyName"].ToString();
                    string className = sdrClass["ClassName"].ToString();

                    try
                    {
                        string execPath = Server.MapPath("~");
                        Assembly assembly = Assembly.LoadFrom(execPath + "bin\\" + assemblyName.Trim() + ".dll");
                        ProcessTask processTask = (ProcessTask)assembly.CreateInstance(assemblyName.Trim() + "." + className.Trim());

                    }
                    catch (Exception ex)
                    {
                        //** log the fault
                        MiddlewareLogging.Logger.BatchLog(jobid, processid, batchid,
                            ProcessAccess.enmLevel.Serious,
                            "Failed to instantiate DLL " + assemblyName + ".dll - class " + className + " \r\n Exception:" + ex.Message);

                        failCount++;
                    }

                }
            }

            if (failCount > 0)
                return false;

            return true;
        }

        /// <summary>
        /// Checks for response from webServices used by MiddlwareSite
        /// </summary>
        /// <returns>boolean of true if responses are recieved</returns>
        public bool WebServiceCheck(int jobid, int processid, int batchid)
        {
            bool responding = false;
            int counter = 0;

            string Url = ConfigurationManager.AppSettings["RootWebsiteUrl"];

            if (string.IsNullOrEmpty(Url))
                throw new Exception("RootWebsiteURL is missing from middleware.config on the website");

            if (!Url.EndsWith("/")) Url += "/";

            log("Web Service Check");

            string[] webServices = new string[] { Url + "Webservices/JobsService.asmx",  Url + "Webservices/SchedulesService.asmx", Url + "Webservices/VATCheck.asmx" };

            foreach (string webService in webServices)
            {

                try
                {
                    WebRequest request = WebRequest.Create(webService);     

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    if (response != null)
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            counter++;
                        }
                        else
                        {
                            MiddlewareLogging.Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Serious, "Web Service" + webService + " is not responding");
                        }
                }
                catch (Exception ex)
                {
                    MiddlewareLogging.Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Serious, "Web Service" + webService + " is not responding " + ex.Message );
                }


            }

            if (counter == webServices.Length)
            {
                responding = true;
            }
          
            return responding;
        }

        /// <summary>
        /// Checks the status of Middleware Scheduler Windows Service to see if it is running
        /// </summary>
        /// <returns>boolean true if status is running</returns>
        public bool SchedulerCheck(int jobid, int processid, int batchid)
        {
            bool running = false;

            log("Scheduler Check");

            ServiceController service = new ServiceController("Middleware Scheduler");

            if (service.Status.Equals(ServiceControllerStatus.Running))
            {
                running = true;
            }
            else
            {
                MiddlewareLogging.Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Serious, "Middleware Scheduler is not running.");
            }

            return running;
        }

        /// <summary>
        /// Loops through all connection strings in web.config and tests its connection
        /// </summary>
        /// <returns>boolean of whether connections could be made</returns>
        public bool ConnectionStringCheck(int jobid, int processid, int batchid)
        {
            bool connect = false;
            int counter = 0;
            log("Connection String Check");
            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;

            if (settings != null)
            {
                //Loop through each connection string in the connection string collection
                foreach (ConnectionStringSettings connectionString in settings)
                {
                    //removes connection string text from name
                    string connName = connectionString.Name.Replace("ConnectionString", "").Trim();

                    string conString = Connections.GetConnectionString(connName);
                    conString = RemoveProvider(conString);

                    using (connection = new SqlConnection(conString))
                    {
                        try
                        {
                            connection.Open();

                            if (connection.State == ConnectionState.Open)
                            {
                                counter++;
                            }
                        }
                        catch
                        {
                            MiddlewareLogging.Logger.BatchLog(jobid, processid, batchid, ProcessAccess.enmLevel.Serious, "Cannot connect to " + conString + " connection string");
                        }

                    }
                }
            }

            if (settings != null)
                if (counter == settings.Count)
                {
                    connect = true;
                }

            return connect;
        }

    }
}
