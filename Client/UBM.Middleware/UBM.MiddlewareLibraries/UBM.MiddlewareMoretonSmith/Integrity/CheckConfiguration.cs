﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.Utilities;
using System.Data.SqlClient;
using System.Collections;
using UBM.MiddlewareMoretonSmith.Feeds;
using UBM.MiddlewareMoretonSmith.Common;



namespace UBM.MiddlewareMoretonSmith.Integrity
{
    public class CheckConfiguration : ProcessTask
    {

        private List<string> folders = new List<string>();
        private List<string> servers = new List<string>();

        private void primeListings()
        {

            folders.Add("CreditStatusDestination");
            folders.Add("CreditStatusArchive");
            servers.Add("FTP-CreditStatusFolder");

            servers.Add("FTP-Create!FormFolder");
            folders.Add("Create!FormSource");
            folders.Add("Create!FormArchive");

            servers.Add("FTP-DiaryNotesFolder");
            folders.Add("DiaryNotesDestination");

            servers.Add("FTP-AsiaDiaryNotesFolder");
            folders.Add("AsiaDiaryNotesDestination");

            servers.Add("FTP-EventsForceFolder");
            folders.Add("EventsForceSource");
            folders.Add("EventsForceArchive");

            servers.Add("FTP-InvoiceFolder");
            folders.Add("InvoiceSource");
            folders.Add("InvoiceArchive");

            servers.Add("FTP-PaymentsFolder");
            folders.Add("PaymentsDestination");
            folders.Add("PaymentsArchive");

        }

        public override ProcessEnums.enmStatus Execute()
        {
            // I love software like this.
            log("Priming all configuration types", ProcessAccess.enmLevel.Standard);
            primeListings();

            //-- now lets test folders
            foreach(string folderName in folders)
            {
                log("Processing SETTING " + folderName);
                //-- now try and read each folder
                if (!ConfigurationSettings["FOLDER"].ContainsKey(folderName))
                {
                    log("[FOLDER]:" + folderName + " entry does not exist", ProcessAccess.enmLevel.Critical);
                }
                else
                {
                    //-- folder is good
                    String path = ConfigurationSettings["FOLDER"][folderName];
                    if(path.Length ==0)
                    {
                        log("Foldername is empty (" + folderName +")");    
                    }
                    else
                    {
                        log("Path for " + folderName + " = " + path);
                        try
                        {

                            string userName = global::System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                            if(userName != null) log("I am:" + userName);
                            //-- setting is valid, see if we can read and write to the folder
                            TextWriter tw = new StreamWriter(path + "deleteme.txt");
                            // write a line of text to the file
                            tw.WriteLine(DateTime.Now);
                            // close the stream
                            tw.Close();
                        }
                        catch (Exception ex)
                        {
                            log(ex.Message.ToString());
                            log(folderName + ": Couldn't write to this folder [" + path + "deleteme.txt]", ProcessAccess.enmLevel.Critical);
                        }

                    } // -- end of key is > 0

                } // -- end of key exists test

            }//-- 


            //-- reading FTP information
            log("Reading FTP details");
            string ftpServerName = ConfigurationSettings["SERVER"]["FTP-ServerName"];
            string ftpUserName = ConfigurationSettings["SERVER"]["FTP-UserName"];
            string ftpPassword = ConfigurationSettings["SERVER"]["FTP-Password"];
            string ftpPortNumber = ConfigurationSettings["SERVER"]["FTP-PortNumber"];
            log("Server:" + ftpServerName);
            log("UserName:" + ftpUserName);
            log("Password:" + ftpPassword);
            log("PortNumber:" + ftpPortNumber);


            //-- upload a test to each folder
            foreach (string ftpFolder in servers)
            {

                log("Processing ftp setting " + ftpFolder);
                if (!ConfigurationSettings["SERVER"].ContainsKey(ftpFolder))
                {
                    log("configuration setting" + ftpFolder + " does not exist.", ProcessAccess.enmLevel.Critical);
                }
                else
                {
                    string folder = ConfigurationSettings["SERVER"][ftpFolder];

                    log("Setting found - value was [" + folder + "]");
                    FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, folder);
                    log("uploading DeleteIcon.png");
                    FileInfo file = new FileInfo(this.Server.MapPath("~/Images/DeleteIcon.png"));
                    if (!CommonFeedTasks.UploadFile(ftpSettings, file, 0, this))
                    {
                        log(ftpFolder + "[" + folder + "] failed to upload.", ProcessAccess.enmLevel.Critical);
                    }
                }
            }

            return ProcessEnums.enmStatus.Successful;

        }
    }

}
