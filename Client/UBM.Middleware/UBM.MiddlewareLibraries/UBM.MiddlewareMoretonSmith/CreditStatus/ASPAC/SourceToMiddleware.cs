﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Feeds;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.CreditStatus.ASPAC
{
    public class SourceToMiddleware:ProcessTask 
    {
        private const int CATEGORY_IDENTIFIER = 1;          //-- Credit Status Import
        private string CREDITSTATUSFILE = "APAC_customerstatusexport.csv";

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-AsiaCreditStatusFolder"];
            string DestinationFolder = ConfigurationSettings["FOLDER"]["AsiaCreditStatusDestination"];
            string ArchiveFolder = ConfigurationSettings["FOLDER"]["AsiaCreditStatusArchive"];

            FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

            //-- Retrieve the Diary Notes file, and rename it.
            log("Starting process - Retrieving " + CREDITSTATUSFILE + " file to " + DestinationFolder);

            // remove the file if it exists
            log("Removing existing local file");
            if (File.Exists(DestinationFolder + CREDITSTATUSFILE)) File.Delete(DestinationFolder + CREDITSTATUSFILE);

            //-- create the file and create a reference to it.
            File.Create(DestinationFolder + CREDITSTATUSFILE).Close();
            DirectoryInfo diSource = new DirectoryInfo(DestinationFolder);
            FileInfo[] Files = diSource.GetFiles(CREDITSTATUSFILE);

            //-- check to see if the file exists.
            //if (Files.Length < 1) throw new Exception("Couldn't read the file " + DestinationFolder + CREDITSTATUSFILE);

            //-- if there are no files, this represents a failure in the job.
            if (Files.Count() == 0)
            {
                CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                reportHelper.PackageFailed();
            }

            int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(Files[0], CATEGORY_IDENTIFIER, this);

            //-- Download the file, and then rename the version on the remove server as .BAK
            if (CommonFeedTasks.DownLoadFile(ftpSettings, CREDITSTATUSFILE, Files[0], fileDetailsId, this, CATEGORY_IDENTIFIER))
            {
                FileInfo fileInfo = new FileInfo(DestinationFolder + CREDITSTATUSFILE);
                CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                CommonFeedTasks.RenameFile(ftpSettings, CREDITSTATUSFILE, CREDITSTATUSFILE.Replace(".csv", ".bak"), fileDetailsId, this);
            }

            return ProcessEnums.enmStatus.Successful;

        }
    }

}
