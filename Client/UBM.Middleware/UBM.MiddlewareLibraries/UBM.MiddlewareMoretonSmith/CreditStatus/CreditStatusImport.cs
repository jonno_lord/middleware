﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Feeds;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.CreditStatus
{
    public class CreditStatusImport:ProcessTask 
    {
        private const int CATEGORY_IDENTIFIER = 1;

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            List<FileInfo> Files = new List<FileInfo>();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-CreditStatusFolder"];
                string DestinationFolder = ConfigurationSettings["FOLDER"]["CreditStatusDestination"];
                string FileName = ConfigurationSettings["FILE"]["CreditStatusFileName"];

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                log("Starting process - Examining files in " + ftpSettings.SubFolderName);
                List<string> files = CommonFeedTasks.DirectoryListing(ftpSettings, this, FileName);
                DirectoryInfo directorySource = new DirectoryInfo(DestinationFolder);

                log("Found (" + files.Count().ToString() + ") to process");

                foreach (string file in files)
                {
                    File.Create(DestinationFolder + file).Close();
                    Files.Add(directorySource.GetFiles(file).FirstOrDefault());
                }

                reportHelper.SetReportTitleAndFileCount("Credit Status Import", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Starting process - Retrieving " + file + " to " + DestinationFolder);

                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        if (CommonFeedTasks.DownLoadFile(ftpSettings, file.Name, file, fileDetailsId, this, CATEGORY_IDENTIFIER))
                        {
                            FileInfo fileInfo = new FileInfo(DestinationFolder + file.Name);
                            CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                            reportHelper.RecordSuccessfulUpload(fileInfo);

                            //CommonFeedTasks.RenameFile(ftpSettings, file.Name, file.Name.Replace(".txt", ".bak"), fileDetailsId, this);
                            CommonFeedTasks.DeleteFile(ftpSettings, file.Name, fileDetailsId, this);
                        }
                        else
                        {
                            try
                            {
                                if (File.Exists(DestinationFolder + file.Name))
                                    File.Delete(DestinationFolder + file.Name);
                            }
                            catch (Exception ex)
                            {
                                log("Failed to remove empty file " + ex.Message.ToString());
                            }

                            reportHelper.RecordFailedUpload(file);
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch
            {
                reportHelper.PackageFailed();
                throw;
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
