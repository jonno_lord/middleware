﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Feeds;

namespace UBM.MiddlewareMoretonSmith.CreditStatus
{
    public class MiddlewareToJDE:ProcessTask 
    {
        private const int CATEGORY_IDENTIFIER = 1;          //-- Credit Status Import
        private const string CREDITSTATUSFILE = "customerstatusexport.csv";

        public override ProcessEnums.enmStatus Execute()
        {

            log("Starting Process");

            string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-CreditStatusFolder"];
            string DestinationFolder = ConfigurationSettings["FOLDER"]["CreditStatusDestination"];
            string ArchiveFolder = ConfigurationSettings["FOLDER"]["CreditStatusArchive"];

            if (File.Exists(DestinationFolder + CREDITSTATUSFILE))
            {
                this.FileConnection = DestinationFolder + CREDITSTATUSFILE;
                if (File.Exists(this.FileConnection))
                {
                    this.FireDTSPackage("MoretonSmith Credit Status Middleware to JDE.dtsx");
                }

                string destinationFileName = ArchiveFolder +
                                              CREDITSTATUSFILE.Replace(".csv",
                                                                       "" +
                                                                       DateTime.Today.ToShortDateString().Replace("/", "")) +
                                              "-" +
                                              DateTime.Now.ToShortTimeString().Replace(":", "") + ".csv";

                log("Archiving file " + destinationFileName);

                File.Move(DestinationFolder + CREDITSTATUSFILE, destinationFileName);

            }


            return ProcessEnums.enmStatus.Successful;

        }
    }

}
