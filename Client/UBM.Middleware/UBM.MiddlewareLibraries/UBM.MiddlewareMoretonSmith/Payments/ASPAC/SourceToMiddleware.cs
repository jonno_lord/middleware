﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Feeds;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.Payments.ASPAC
{
    public class SourceToMiddleware : ProcessTask
    {
        private const int CATEGORY_IDENTIFIER = 12;      //-- Payments Import
        private const string PAYMENTSFILE = "APAC_msipayments.csv";

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-AsiaPaymentsFolder"];
            string DestinationFolder = ConfigurationSettings["FOLDER"]["AsiaPaymentsDestination"];
            string ArchiveFolder = ConfigurationSettings["FOLDER"]["AsiaPaymentsArchive"];

            FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

            //-- Retrieve the Diary Notes file, and rename it.
            log("Starting process - Retrieving " + PAYMENTSFILE +" file to " + DestinationFolder);
            
            // remove the file if it exists
            log("Removing existing local file");
            if (File.Exists(DestinationFolder + PAYMENTSFILE)) File.Delete(DestinationFolder + PAYMENTSFILE);

            //-- create the file and create a reference to it.
            File.Create(DestinationFolder + PAYMENTSFILE).Close();
            DirectoryInfo diSource = new DirectoryInfo(DestinationFolder);
            FileInfo[] Files = diSource.GetFiles(PAYMENTSFILE);

            //-- check to see if the file exists.
            //if (Files.Length < 1) throw new Exception("Couldn't read the file " + DestinationFolder + PAYMENTSFILE);

            if (Files.Count() == 0)
            {
                CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                reportHelper.PackageFailed();
            }

            int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(Files[0], CATEGORY_IDENTIFIER, this);

            //-- Download the file, and then rename the version on the remove server as .BAK
            if (CommonFeedTasks.DownLoadFile(ftpSettings, PAYMENTSFILE, Files[0], fileDetailsId, this, CATEGORY_IDENTIFIER))
            {

                FileInfo fileInfo = new FileInfo(DestinationFolder + PAYMENTSFILE);
                CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                CommonFeedTasks.RenameFile(ftpSettings, PAYMENTSFILE, PAYMENTSFILE.Replace(".csv", ".bak"), fileDetailsId, this);
                
                //-- the file succesfully downloaded, now import into JDE F03b13z1
                this.FileConnection = DestinationFolder + PAYMENTSFILE;
                FireDTSPackage("MoretonSmith Payments Source to Middleware.dtsx");

                //-- move the imported file..
                File.Move(DestinationFolder + PAYMENTSFILE, ArchiveFolder + DateTime.Now.ToShortDateString().Replace("/", "") + DateTime.Now.ToShortTimeString().Replace(":", "") + PAYMENTSFILE);

            }

            return ProcessEnums.enmStatus.Successful;

        }
    }
    
}
