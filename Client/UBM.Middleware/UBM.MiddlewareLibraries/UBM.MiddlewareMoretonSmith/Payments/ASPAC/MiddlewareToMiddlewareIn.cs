﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareMoretonSmith.Payments.ASPAC
{

    public class MiddlewareToMiddlewareIn : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            this.FireStoredProcedure("usp_TRA_PaymentsToJDEFormat",
                  "SELECT TOP 10 * FROM Payments WHERE Stage = 1",
                  Connections.GetConnectionString("MiddlewareMoretonSmith"));

            return ProcessEnums.enmStatus.Successful;

        }
    }

}
