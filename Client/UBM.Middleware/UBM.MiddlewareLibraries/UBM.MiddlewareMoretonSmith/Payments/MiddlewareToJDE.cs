﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;


namespace UBM.MiddlewareMoretonSmith.Payments
{
    public class MiddlewareToJDE : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareSourceConnection = "MiddlewareMoretonSmith";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Payments Middleware to JDE.dtsx", true);

            return ProcessEnums.enmStatus.Successful;

        }
    }
}
