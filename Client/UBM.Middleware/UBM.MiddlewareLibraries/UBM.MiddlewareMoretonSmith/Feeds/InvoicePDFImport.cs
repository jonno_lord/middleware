﻿using System.Collections.Generic;
using System.IO;
using UBM.MiddlewareMoretonSmith.Common;
using UBM.MiddlewareProcess;
using System.Linq;
using System;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class InvoicePDFImport : ProcessTask
    {
        private const int CATEGORY_IDENTIFIER = 11;

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            List<FileInfo> Files = new List<FileInfo>();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-InvoicePDFFolder"];
                string USDestinationFolder = ConfigurationSettings["FOLDER"]["USInvoicePDFDestination"];
                string EMEADestinationFolder = ConfigurationSettings["FOLDER"]["EMEAInvoicePDFDestination"];
                string FileName = ConfigurationSettings["FILE"]["InvoicePDFFileName"];

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                log("Starting process - Examining files in " + ftpSettings.SubFolderName);
                List<string> files = CommonFeedTasks.DirectoryListing(ftpSettings, this, FileName);
                DirectoryInfo directorySourceUS = new DirectoryInfo(USDestinationFolder);
                DirectoryInfo directorySourceEMEA = new DirectoryInfo(EMEADestinationFolder);

                log("Found (" + files.Count().ToString() + ") files to process");

                foreach (string file in files)
                {
                    if (file.Substring(0, 3) == "101")
                    {
                        File.Create(USDestinationFolder + file).Close();
                        Files.Add(directorySourceUS.GetFiles(file).FirstOrDefault());
                    }
                    else
                    {
                        File.Create(EMEADestinationFolder + file).Close();
                        Files.Add(directorySourceEMEA.GetFiles(file).FirstOrDefault());
                    }
                }

                reportHelper.SetReportTitleAndFileCount("Invoice PDF Import", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Starting process - Retrieving " + file + " to " + file.ToString().Substring(0,3) == "101" ? USDestinationFolder : EMEADestinationFolder);

                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        if (CommonFeedTasks.DownLoadFile(ftpSettings, file.Name, file, fileDetailsId, this, CATEGORY_IDENTIFIER))
                        {
                            FileInfo fileInfo = new FileInfo(file.ToString().Substring(0, 3) == "101" ? USDestinationFolder + file.Name : EMEADestinationFolder + file.Name);
                            CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                            reportHelper.RecordSuccessfulUpload(fileInfo);

                            //CommonFeedTasks.RenameFile(ftpSettings, file.Name, file.Name.Replace(".txt", ".bak"), fileDetailsId, this);
                            CommonFeedTasks.DeleteFile(ftpSettings, file.Name, fileDetailsId, this);
                        }
                        else
                        {
                            try
                            {
                                if (File.Exists(file.ToString().Substring(0, 3) == "101" ? USDestinationFolder + FileName : EMEADestinationFolder + FileName))
                                    File.Delete(file.ToString().Substring(0, 3) == "101" ? USDestinationFolder + FileName : EMEADestinationFolder + FileName);
                            }
                            catch (Exception ex)
                            {
                                log("Failed to remove empty file " + ex.Message.ToString());
                            }

                            reportHelper.RecordFailedUpload(file);
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch (Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }


            return ProcessEnums.enmStatus.Successful;

        }
    }
}
