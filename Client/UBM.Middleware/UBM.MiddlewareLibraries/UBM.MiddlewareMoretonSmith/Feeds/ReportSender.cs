﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareProcess;
using UBM.NotificationClient;
using UBM.Utilities.HtmlReportGenerator;

namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class ReportSender : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            HtmlReport report = new HtmlReport();

            report.AddHeader("MoretonSmith Feeds - Daily Report");


            DataSet todaysSummaries = ReportHelper.GetTodaysSummaryReports(GenericCommands.RemoveProvider(Connections.GetConnectionString(this.SystemId)));

            foreach (DataRow dataRow in  todaysSummaries.Tables[0].Rows)
            {
                report.AddHtml(dataRow[0].ToString());
            }

            string emailTo = ConfigurationSettings["EMAIL"]["ReportRecipients"];

            NotificationServiceClient.Email("", emailTo, "", "Moreton Smith Feeds", report.ToString(), Priority.Normal, true);


            return ProcessEnums.enmStatus.Successful;
        }
    }
}
