﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.Utilities;
using System.Data.SqlClient;
using UBM.Utilities.HtmlReportGenerator;
using UBM.MiddlewareMoretonSmith.Common;


namespace UBM.MiddlewareMoretonSmith.Feeds.ASPAC
{
    public class CreateFormExport:ProcessTask 
    {

        private const int CATEGORY_IDENTIFIER = 8;  //-- Create Form Export
                                                    //-- Get these magic constants from 
                                                    //-- select * from Categories

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-ASPACCreate!FormFolder"];
                string SourceFolder = ConfigurationSettings["FOLDER"]["ASPACCreate!FormSource"];
                string ArchiveFolder = ConfigurationSettings["FOLDER"]["ASPACCreate!FormArchive"];

                log("Starting process - Examining files in " + SourceFolder);

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);
                DirectoryInfo diSource = new DirectoryInfo(SourceFolder);
                FileInfo[] Files = diSource.GetFiles("*.PDF");

                log("Found (" + Files.Count().ToString() + ") files to process");

                reportHelper.SetReportTitleAndFileCount("ASPAC Create! Form Export", Files.Count());

                //-- if there are no files, this represents a failure in the job.
                if (Files.Count() == 0)
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                    reportHelper.PackageFailed();
                }

                foreach (FileInfo file in Files)
                {

                    log("Recording file " + file.FullName.ToString());
                    int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                    log("Uploading file " + file.FullName.ToString());
                    if (CommonFeedTasks.UploadFile(ftpSettings, file, fileDetailsId, this))
                    {
                        reportHelper.RecordSuccessfulUpload(file);   
                        CommonFeedTasks.MoveAndRenameFile(file, ArchiveFolder, ".pdf", fileDetailsId, this);                     
                    }
                    else
                    {
                        reportHelper.RecordFailedUpload(file);
                    }
                }
                
            }
            catch
            {
                reportHelper.PackageFailed();
                throw;
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                                          reportHelper.GetReportParametersForStoredProc());
                
            }

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
