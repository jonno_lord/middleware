﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.Feeds.ASPAC
{
    public class DiaryNotesImport : ProcessTask
    {
        private const int CATEGORY_IDENTIFIER = 11;          //-- Diary Notes Import
        private string NOTESFILE = "APAC_msinotes.txt";

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-AsiaDiaryNotesFolder"];
                string DestinationFolder = ConfigurationSettings["FOLDER"]["AsiaDiaryNotesDestination"];

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                //-- Retrieve the Diary Notes file, and rename it.
                log("Starting process - Retrieving msinotes.txt files to " + DestinationFolder);

                // remove the file if it exists
                log("Removing existing local file");
                if (File.Exists(DestinationFolder + NOTESFILE))
                {
                    //-- this represents a failure where we are about to download the file, but the existing version exists.
                    File.Move(DestinationFolder + NOTESFILE, DestinationFolder + NOTESFILE.Replace(".txt", "-" + DateTime.Today.ToShortDateString().Replace("/", "")) + "-" +
                        DateTime.Now.ToShortTimeString().Replace(":", "") + "-waiting.txt");
                }

                //-- create the file and create a reference to it.
                File.Create(DestinationFolder + NOTESFILE).Close();
                DirectoryInfo diSource = new DirectoryInfo(DestinationFolder);
                FileInfo[] Files = diSource.GetFiles(NOTESFILE);

                reportHelper.SetReportTitleAndFileCount("Diary Notes Import", Files.Count());

                //-- check to see if the file exists.);)
                //if (Files.Length < 1) throw new Exception("Couldn't read the file " + DestinationFolder + NOTESFILE);

                int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(Files[0], CATEGORY_IDENTIFIER, this);

                //-- Download the file, and then rename the version on the remove server as .BAK
                if (CommonFeedTasks.DownLoadFile(ftpSettings, NOTESFILE, Files[0], fileDetailsId, this, CATEGORY_IDENTIFIER))
                {
                    FileInfo fileInfo = new FileInfo(DestinationFolder + NOTESFILE);
                    CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                    reportHelper.RecordSuccessfulUpload(Files[0]);
                    CommonFeedTasks.RenameFile(ftpSettings, NOTESFILE, NOTESFILE.Replace(".txt", ".bak"), fileDetailsId, this);
                }
                else
                {
                    try
                    {
                        if (File.Exists(DestinationFolder + NOTESFILE))
                            File.Delete(DestinationFolder + NOTESFILE);
                    }
                    catch (Exception ex)
                    {
                        log("Failed to remove empty file " + ex.Message.ToString());
                    }

                    reportHelper.RecordFailedUpload(Files[0]);
                }
            }
            catch
            {
                reportHelper.PackageFailed();
                throw;
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }
        

            return ProcessEnums.enmStatus.Successful;

        }
    }
    
}
