﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.Utilities;
using System.Data.SqlClient;
using UBM.Utilities.HtmlReportGenerator;
using UBM.MiddlewareMoretonSmith.Common;


namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class CreateFormExport:ProcessTask 
    {

        private const int CATEGORY_IDENTIFIER = 3;

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            List<FileInfo> Files = new List<FileInfo>();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-Create!FormFolder"];
                string SourceFolder = ConfigurationSettings["FOLDER"]["Create!FormSource"];
                string ArchiveFolder = ConfigurationSettings["FOLDER"]["Create!FormArchive"];

                log("Starting process - Examining files in " + SourceFolder);

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                DirectoryInfo diSource = new DirectoryInfo(SourceFolder);
                string[] fileExtensions = new string[] { "*.pdf", "*.txt" };

                Files = CommonFeedTasks.GetFilesByExtenstion(diSource, fileExtensions);

                log("Found (" + Files.Count().ToString() + ") files to process");

                reportHelper.SetReportTitleAndFileCount("Oracle PDF Export", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Recording file " + file.FullName.ToString());
                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        log("Uploading file " + file.FullName.ToString());

                        if (CommonFeedTasks.UploadFile(ftpSettings, file, fileDetailsId, this))
                        {
                            reportHelper.RecordSuccessfulUpload(file);

                            string fileExtension = Path.GetExtension(file.DirectoryName + @"\" + file.Name);

                            CommonFeedTasks.MoveAndRenameFile(file, ArchiveFolder, fileExtension, fileDetailsId, this);
                        }
                        else
                        {
                            reportHelper.RecordFailedUpload(file);
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch (Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
