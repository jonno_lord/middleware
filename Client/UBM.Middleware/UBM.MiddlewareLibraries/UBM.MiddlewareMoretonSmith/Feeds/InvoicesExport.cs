﻿using System;
using System.Linq;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Common;


namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class InvoicesExport:ProcessTask 
    {
        private const int CATEGORY_IDENTIFIER = 4; 

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-InvoiceFolder"];
                string SourceFolder = ConfigurationSettings["FOLDER"]["InvoiceSource"];
                string ArchiveFolder = ConfigurationSettings["FOLDER"]["InvoiceArchive"];
                string FileName = ConfigurationSettings["FILE"]["InvoiceFileName"];

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                log("Starting process - Examining files in " + SourceFolder);
                DirectoryInfo diSource = new DirectoryInfo(SourceFolder);
                FileInfo[] Files = CommonFeedTasks.DeleteOldestFiles(SourceFolder, FileName);

                log("Found (" + Files.Count().ToString() + ") files to process");

                reportHelper.SetReportTitleAndFileCount("Invoices Export", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Starting process - Sending " + file + " to " + ftpSettings.ServerName);
                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        log("Uploading " + file + " to " + ftpSettings.ServerName);
                        if (CommonFeedTasks.UploadFile(ftpSettings, file, fileDetailsId, this))
                        {
                            reportHelper.RecordSuccessfulUpload(file);
                            CommonFeedTasks.MoveAndRenameFile(file, ArchiveFolder, ".txt", fileDetailsId, this);
                        }
                        else
                        {
                            log("Failed to upload " + file + " to " + ftpSettings.ServerName);
                            reportHelper.RecordFailedUpload(file);
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch(Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }
         

            return ProcessEnums.enmStatus.Successful;

        }
    }

}
