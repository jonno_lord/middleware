﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using UBM.Utilities.HtmlReportGenerator;

namespace UBM.MiddlewareMoretonSmith.Feeds
{
    internal class ReportHelper
    {
        #region Fields

        private string reportTitle;
        private readonly List<FileInfo> successFiles;
        private readonly List<FileInfo> failedFiles;
        private bool packageFailed;

        private readonly HtmlReport report;

        public const string INSERT_SUMMARY_REPORT_SP = "usp_INS_SummaryReports";
        public const string GET_TODAYS_REPORTS_SP = "usp_GET_TodaysSummaryReports";

        #endregion

        #region Contructor

        public ReportHelper()
        {
            failedFiles = new List<FileInfo>();
            successFiles = new List<FileInfo>();
            packageFailed = false;
            reportTitle = "";
            
            report = new HtmlReport();
        }

        #endregion

        #region Private Methods

        private static string ValidateFile(FileInfo file)
        {
            try
            {
                string warnings = "";

                if (file.Length == 0)
                    warnings += " - 0 byte file";

                if (file.CreationTime.AddDays(3) < DateTime.Now)
                    warnings += " - file created more than 3 days ago";

                if (warnings.Length > 0)
                    return file.FullName + warnings;

            }
            catch (Exception ex)
            {
                //-- can't report the exception... as there is no reference to the process task
            }

            return "";
        }

        private void SummariseReport()
        {
            if(successFiles.Count > 0)
            {
                report.AddParagraph(successFiles.Count + " files successfully uploaded");
            }
            else
            {
                report.AddParagraph("0 files were uploaded", TextStyleName.Failure);
            }

            if(failedFiles.Count > 0) 
                report.AddParagraph(failedFiles.Count + " failed to upload", TextStyleName.Failure);

            foreach (FileInfo successFile in successFiles)
            {
                string warnings = ValidateFile(successFile);
                if(!string.IsNullOrEmpty(warnings))
                    report.AddParagraph(warnings,TextStyleName.Caution);
            }

            if(packageFailed)
            {
                report.AddParagraph(reportTitle + " Package failed to complete.", TextStyleName.Failure);    
            }
            report.AddParagraph("<hr/>", TextStyleName.Normal);

        }

        #endregion

        #region Public Methods

        public void SetReportTitleAndFileCount(string title, int numberOfFiles)
        {
            this.reportTitle = title;
            report.AddParagraph(title + " - " + DateTime.Now);
            report.AddParagraph("Files to process: " + numberOfFiles + " files");
        }

        public void RecordSuccessfulUpload(FileInfo file)
        {
            successFiles.Add(file);
        }

        public void RecordFailedUpload(FileInfo file)
        {
            failedFiles.Add(file);
        }

        public void PackageFailed()
        {
            packageFailed = true;
        }

        public SqlParameter[] GetReportParametersForStoredProc()
        {
            SummariseReport();
            
            return new[]
                       {
                           new SqlParameter("@JobName", reportTitle),
                           new SqlParameter("@Summary", report.ToString() )
                       };
        }

        public static DataSet GetTodaysSummaryReports(string connection)
        {
            using(SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();

                SqlCommand command = new SqlCommand(GET_TODAYS_REPORTS_SP, sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 60;
                command.Prepare();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;

                DataSet results = new DataSet();
                adapter.Fill(results);

                return results;
            }
        }
    

        #endregion

    }
}
