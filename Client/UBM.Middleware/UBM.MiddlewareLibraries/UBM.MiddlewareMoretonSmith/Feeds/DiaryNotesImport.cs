﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class DiaryNotesImport : ProcessTask
    {
        private const int CATEGORY_IDENTIFIER = 2;    

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            List<FileInfo> Files = new List<FileInfo>();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-DiaryNotesFolder"];
                string DestinationFolder = ConfigurationSettings["FOLDER"]["DiaryNotesDestination"];
                string FileName = ConfigurationSettings["FILE"]["DiaryNotesFileName"];

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                log("Starting process - Examining files in " + ftpSettings.SubFolderName);
                List<string> files = CommonFeedTasks.DirectoryListing(ftpSettings, this, FileName);
                DirectoryInfo directorySource = new DirectoryInfo(DestinationFolder);

                log("Found (" + files.Count().ToString() + ") files to process");

                foreach (string file in files)
                {
                    File.Create(DestinationFolder + file).Close();
                    Files.Add(directorySource.GetFiles(file).FirstOrDefault());
                }

                reportHelper.SetReportTitleAndFileCount("Diary Notes Import", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Starting process - Retrieving " + file + " to " + DestinationFolder);

                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        if (CommonFeedTasks.DownLoadFile(ftpSettings, file.Name, file, fileDetailsId, this, CATEGORY_IDENTIFIER))
                        {
                            FileInfo fileInfo = new FileInfo(DestinationFolder + file.Name);
                            CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                            reportHelper.RecordSuccessfulUpload(fileInfo);

                            //CommonFeedTasks.RenameFile(ftpSettings, file.Name, file.Name.Replace(".txt", ".bak"), fileDetailsId, this);
                            CommonFeedTasks.DeleteFile(ftpSettings, file.Name, fileDetailsId, this);
                        }
                        else
                        {
                            try
                            {
                                if (File.Exists(DestinationFolder + FileName))
                                    File.Delete(DestinationFolder + FileName);
                            }
                            catch (Exception ex)
                            {
                                log("Failed to remove empty file " + ex.Message.ToString());
                            }

                            reportHelper.RecordFailedUpload(file);
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch(Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }
        

            return ProcessEnums.enmStatus.Successful;

        }
    }
    
}
