﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.Utilities;
using System.Data.SqlClient;
using UBM.MiddlewareMoretonSmith.Common;


namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class GenericExport : ProcessTask 
    {
        private const int CATEGORY_IDENTIFIER = 10; 

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-GenericExportFolder"];
                string SourceFolder = ConfigurationSettings["FOLDER"]["GenericExportSource"];
                string ArchiveFolder = ConfigurationSettings["FOLDER"]["GenericExportArchive"];

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                log("Starting process - Examining files in " + SourceFolder);
                DirectoryInfo diSource = new DirectoryInfo(SourceFolder);
                FileInfo[] Files = diSource.GetFiles();

                log("Found (" + Files.Count().ToString() + ") files to process");

                reportHelper.SetReportTitleAndFileCount("Daily Allocations Export", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Starting process - Sending " + file + " to " + ftpSettings.ServerName);
                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        log("Uploading " + file + " to " + ftpSettings.ServerName);
                        if (CommonFeedTasks.UploadFile(ftpSettings, file, fileDetailsId, this))
                        {
                            reportHelper.RecordSuccessfulUpload(file);
                            CommonFeedTasks.MoveAndRenameFile(file, ArchiveFolder, ".bak", fileDetailsId, this);
                        }
                        else
                        {
                            log("Failed to upload " + file + " to " + ftpSettings.ServerName);
                            reportHelper.RecordFailedUpload(file);
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch(Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }
         

            return ProcessEnums.enmStatus.Successful;

        }
    }

}
