﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.Utilities;
using System.Data.SqlClient;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class CARSExport : ProcessTask
    {
        private const int CATEGORY_IDENTIFIER = 8;

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();
            List<FileInfo> Files = new List<FileInfo>();

            try
            {
                string middlewareConnectionString = MiddlewareDataAccess.Connections.GetConnectionString("MiddlewareCARS");
                middlewareConnectionString = GenericCommands.RemoveProvider(middlewareConnectionString);

                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-CARSPDFFolder"];
                string SourceFolder = ConfigurationSettings["FOLDER"]["CARSSource"];
                string ArchiveFolder = ConfigurationSettings["FOLDER"]["CARSArchive"];

                log("Starting process - Examining files in " + SourceFolder);

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                DirectoryInfo diSource = new DirectoryInfo(SourceFolder);
                Files = diSource.GetFiles("*.pdf").ToList();

                log("Found (" + Files.Count().ToString() + ") files to process");

                reportHelper.SetReportTitleAndFileCount("CARS PDF Export", Files.Count());

                if (Files.Count() != 0)
                {
                    foreach (FileInfo file in Files)
                    {
                        log("Recording file " + file.FullName.ToString());
                        int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(file, CATEGORY_IDENTIFIER, this);

                        string documentNumberFilename = MiddlewareDataAccess.InvoiceAccess.GetCARSDocumentNumber(middlewareConnectionString, file.Name);

                        if (documentNumberFilename == "")
                        {
                            log("Could not generate a file name for this document number " + file, ProcessAccess.enmLevel.Important);
                        }
                        else
                        {
                            if (File.Exists(SourceFolder + documentNumberFilename))
                            {
                                File.Delete(SourceFolder + documentNumberFilename);
                            }

                            log("Renaming file " + file.Name + " to " + documentNumberFilename);
                            CommonFeedTasks.RenameLocalFile(file, SourceFolder, documentNumberFilename, fileDetailsId, this);

                            FileInfo jdeFile = new FileInfo(file.FullName);

                            log("Uploading file " + jdeFile.FullName.ToString());

                            if (CommonFeedTasks.UploadFile(ftpSettings, jdeFile, fileDetailsId, this))
                            {
                                reportHelper.RecordSuccessfulUpload(file);
                                CommonFeedTasks.MoveAndRenameFile(jdeFile, ArchiveFolder, ".pdf", fileDetailsId, this);
                            }
                            else
                            {
                                reportHelper.RecordFailedUpload(file);
                            }
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch(Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
