﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using UBM.Utilities;
using System.Data.SqlClient;
using UBM.MiddlewareMoretonSmith.Common;

namespace UBM.MiddlewareMoretonSmith.Feeds
{
    public class EventsForceImport : ProcessTask
    {
        private const int CATEGORY_IDENTIFIER = 6;

        public override ProcessEnums.enmStatus Execute()
        {
            ReportHelper reportHelper = new ReportHelper();

            try
            {
                string FTPSubFolder = ConfigurationSettings["SERVER"]["FTP-EventsForceFolder"];
                string SourceFolder = ConfigurationSettings["FOLDER"]["EventsForceSource"];

                log("Starting process - Examining files in " + FTPSubFolder);

                FTPSettings ftpSettings = new FTPSettings(ConfigurationSettings, FTPSubFolder);

                ftpSettings.ServerName = ConfigurationSettings["SERVER"]["FTP-EventsForceServer"];
                ftpSettings.UserName = ConfigurationSettings["SERVER"]["FTP-EventsForceUserName"];
                ftpSettings.Password = ConfigurationSettings["SERVER"]["FTP-EventsForcePassword"];
                ftpSettings.SubFolderName = ConfigurationSettings["SERVER"]["FTP-EventsForceFolder"];
                ftpSettings.PortNumber = "21";

                log("Remote Path: " + ftpSettings.ServerName + ftpSettings.SubFolderName);
                log("Username: " + ftpSettings.UserName);
                log("Password: " + ftpSettings.Password);
                log("Local Path: " + null);

                List<string> files = CommonFeedTasks.FTPDirectoryListing(ftpSettings, this);

                log("Found (" + files.Count().ToString() + ") files to process");
                reportHelper.SetReportTitleAndFileCount("Events Force Export", files.Count());

                if (files.Count() != 0)
                {
                    foreach (string file in files)
                    {
                        if (file.EndsWith(".pdf"))
                        {
                            if (File.Exists(SourceFolder + file))
                            {
                                File.Delete(SourceFolder + file);
                            }

                            File.Create(SourceFolder + file).Close();
                            FileInfo fileInfo = new FileInfo(SourceFolder + file);

                            int fileDetailsId = CommonFeedTasks.InsertFileDetailsEntry(fileInfo, CATEGORY_IDENTIFIER, this);

                            if (CommonFeedTasks.DownloadFTPFile(ftpSettings, file, SourceFolder, fileDetailsId, this, CATEGORY_IDENTIFIER))
                            {
                                fileInfo = new FileInfo(SourceFolder + file);
                                CommonFeedTasks.UpdateFileSize(fileDetailsId, fileInfo.Length, this);

                                CommonFeedTasks.RenameFTPFile(ftpSettings, file, file.Replace(".pdf", ".old"), fileDetailsId, this);
                            }
                        }
                    }
                }
                else
                {
                    CommonFeedTasks.InsertFailedFileDetails(CATEGORY_IDENTIFIER, this);
                }
            }
            catch (Exception ex)
            {
                log(ex.Message, ProcessAccess.enmLevel.Critical);
                reportHelper.PackageFailed();
            }
            finally
            {
                FireScalarStoredProcedure(ReportHelper.INSERT_SUMMARY_REPORT_SP,
                          reportHelper.GetReportParametersForStoredProc());
            }

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
