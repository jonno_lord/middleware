﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Collections;
using Rebex.Net;
using UBM.Utilities;

namespace UBM.MiddlewareMoretonSmith.Common
{
    public static class CommonFeedTasks
    {
        #region PublicMethods

        public static int InsertFileDetailsHistory(int fileDetailsId, string Description, bool isError, ProcessTask task)
        {
            SqlParameter[] parms = new SqlParameter[3];
            parms[0] = new SqlParameter("fileDetailId", fileDetailsId);
            parms[1] = new SqlParameter("Description", Description);
            parms[2] = new SqlParameter("isError", isError);

            return (task.FireStoredProcedure("usp_INS_FileDetailHistory", parms));

        }


        public static int InsertFailedFileDetails(int categoryId, ProcessTask task, string message = "No files found")
        {
            SqlParameter[] parms = new SqlParameter[7];
            parms[0] = new SqlParameter("FileName", message);
            parms[1] = new SqlParameter("FilePath", "");
            parms[2] = new SqlParameter("CategoryId", categoryId);
            parms[3] = new SqlParameter("FileSize", "0");
            parms[4] = new SqlParameter("FileDate", DateTime.Now);
            parms[5] = new SqlParameter("BatchNumber", task.Batchid);
            parms[6] = new SqlParameter("Status", "COMPLETE");

            return (task.FireScalarStoredProcedure("usp_INS_FileDetails", parms));
            
        }

        public static int InsertFileDetailsEntry(FileInfo file, int categoryId, ProcessTask task)
        {
            SqlParameter[] parms = new SqlParameter[6];
            parms[0] = new SqlParameter("FileName", file.Name);
            parms[1] = new SqlParameter("FilePath", file.Directory.ToString());
            parms[2] = new SqlParameter("CategoryId", categoryId);
            parms[3] = new SqlParameter("FileSize", file.Length);
            parms[4] = new SqlParameter("FileDate", file.CreationTime);
            parms[5] = new SqlParameter("BatchNumber", task.Batchid);

            return (task.FireScalarStoredProcedure("usp_INS_FileDetails", parms));

        }

        public static void UpdateFileSize(int fileDetailsId, long fileSize, ProcessTask task)
        {
            SqlParameter[] parms = new SqlParameter[2];
            parms[0] = new SqlParameter("FileDetailId", fileDetailsId);
            parms[1] = new SqlParameter("FileSize", fileSize);
            task.FireStoredProcedure("usp_UPD_FileDetailsSize", parms);
        }

        public static void UpdateFileDetailsEntry(int fileDetailsId, string status, DateTime? ftpDate, DateTime? movedFileDate, ProcessTask task)
        {

            SqlParameter[] parms = new SqlParameter[2];

            if ((ftpDate != null && movedFileDate == null) || (ftpDate == null && movedFileDate != null))
            {
                parms = new SqlParameter[3];

                if(ftpDate != null)
                    parms[2] = new SqlParameter("FTPDate", ftpDate);
            
                if(movedFileDate != null)
                    parms[2] = new SqlParameter("MovedFileDate", movedFileDate);
            }

            if ((ftpDate != null && movedFileDate != null))
            {
                parms = new SqlParameter[4];
                parms[2] = new SqlParameter("FTPDate", ftpDate);
                parms[3] = new SqlParameter("MovedFileDate", movedFileDate);
            }

            parms[0] = new SqlParameter("FileDetailId", fileDetailsId);
            parms[1] = new SqlParameter("Status", status);

            task.FireStoredProcedure("usp_UPD_FileDetails", parms);

        }



        public static void RenameLocalFile(FileInfo file, string DestinationFolder, string newFileName,
        int fileDetailsId, ProcessTask task)
        {

            try
            {
                string renamedFile = newFileName;

                if (File.Exists(DestinationFolder + renamedFile))
                {
                    File.Delete(DestinationFolder + renamedFile);
                }

                file.MoveTo(DestinationFolder + newFileName);

                UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", null, DateTime.Now, task);

            }
            catch (Exception ex)
            {
                task.log("Failed to Move file " + file.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to Move file :" + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);

            }
        }


        public static void MoveAndRenameFile(FileInfo file, string DestinationFolder, string newExtension, 
                int fileDetailsId, ProcessTask task)
        {

            try
            {
                string renamedFile = DestinationFolder + file.Name.Substring(0, file.Name.LastIndexOf(".")) + newExtension;

                task.log("moving " + file.Name + " to " + renamedFile);

                if(File.Exists(renamedFile))
                    File.Delete(renamedFile);

                file.MoveTo(renamedFile);

                UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", null, DateTime.Now, task);

            }
            catch (Exception ex)
            {
                task.log("Failed to Move file " + file.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to Move file :" + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);

            }
        }

        public static List<FileInfo> GetFilesByExtenstion(DirectoryInfo diSource, params string[] fileExtensions)
        {
            IEnumerable<FileInfo> files = new List<FileInfo>();

            foreach (string extension in fileExtensions)
            {
                files = files.Concat(diSource.GetFiles(extension));
            }

            return files.ToList();
        }

        public static FileInfo[] DeleteOldestFiles(string sourceFolder, string fileName)
        {
            var allFiles = new DirectoryInfo(sourceFolder).GetFiles(fileName).ToList();

            if (allFiles.Count() > 2)
            {
                var headerFile = new DirectoryInfo(sourceFolder).GetFiles().OrderByDescending(f => f.LastWriteTime).Where(f => f.FullName.Contains("Header")).FirstOrDefault();
                var detailFile = new DirectoryInfo(sourceFolder).GetFiles().OrderByDescending(f => f.LastWriteTime).Where(f => f.FullName.Contains("Lines")).FirstOrDefault();

                foreach (FileInfo file in allFiles)
                {
                    if (file.Name.Contains("Header") && file.Name != headerFile.Name)
                    {
                        File.Delete(file.FullName);
                    }

                    if (file.Name.Contains("Lines") && file.Name != detailFile.Name)
                    {
                        File.Delete(file.FullName);
                    }
                }
            }

            return new DirectoryInfo(sourceFolder).GetFiles(fileName);
        }

        #endregion

        #region FTPMethods

        public static void DeleteFTPFile(FTPSettings ftpSettings, string FTPFileName, int fileDetailsId, ProcessTask task)
        {
            FtpHelper ftpHelper = new FtpHelper("ftp://" + ftpSettings.ServerName + ftpSettings.SubFolderName + "/" + FTPFileName, ftpSettings.UserName, ftpSettings.Password, null);
            task.log("Deleting Remote file " + ftpHelper.FtpServerDirectory);
           
            try
            {
                ftpHelper.DeleteFile();
            }
            catch (Exception ex)
            {
                task.log("Failed to Delete file " + FTPFileName + ", it may not exist." + ex.Message.ToString(), ProcessAccess.enmLevel.Important);
            }
        }

        public static bool RenameFTPFile(FTPSettings ftpSettings, string FTPFileName, string FTPNewName, int fileDetailsId, ProcessTask task)
        {
            bool renamedFile = false;

            task.log("renaming " + FTPFileName + " to " + FTPNewName);
            InsertFileDetailsHistory(fileDetailsId, "renaming " + FTPFileName + " to " + FTPNewName, false, task);

            FtpHelper ftpHelper = new FtpHelper("ftp://" + ftpSettings.ServerName + ftpSettings.SubFolderName + "/" + FTPFileName, ftpSettings.UserName, ftpSettings.Password, null);

            try
            {
                ftpHelper.RenameRemoteFile(FTPFileName, FTPNewName);
                renamedFile = true;
            }
            catch (Exception ex)
            {
                task.log("Failed to rename file " + FTPFileName + " to " + FTPNewName + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to rename file :" + ex.Message.ToString(), true, task);
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
            }

            return renamedFile;
        }

        public static bool DownloadFTPFile(FTPSettings ftpSettings, string FTPFileName, string sourcePath, int fileDetailsId, ProcessTask task, int categoryIdentifier)
        {
            bool fileDownloaded = false;
            long fileSize = 0;

            task.log("Reading file stream for " + FTPFileName);
            InsertFileDetailsHistory(fileDetailsId, "Retrieving from " + ftpSettings.ServerName, false, task);

            FtpHelper ftpHelper = new FtpHelper("ftp://" + ftpSettings.ServerName + ftpSettings.SubFolderName + FTPFileName, ftpSettings.UserName, ftpSettings.Password, sourcePath);
            
            try
            {
                ftpHelper.DownloadFile(FTPFileName);

                fileSize = ftpHelper.GetFileSize(FTPFileName);
                
                fileDownloaded = true;
                InsertFileDetailsHistory(fileDetailsId, "File succesfully Downloaded (" + fileSize +  "kb)", false, task);
                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "RECEIVED", DateTime.Now, null, task);
            }
            catch (Exception ex)
            {
                //-- file could not be sent
                task.log("Failed to download file " + FTPFileName + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to download file :" + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
            }
           
            return fileDownloaded;
        }

        public static List<string> FTPDirectoryListing(FTPSettings ftpSettings, ProcessTask task)
        {
            List<string> files = new List<string>();

            task.log("Reading from" + ftpSettings.ServerName + ftpSettings.SubFolderName);
            task.log("FTP Credentials are User:" + ftpSettings.UserName + " Password:" + ftpSettings.Password);

            FtpHelper ftpHelper = new FtpHelper("ftp://" + ftpSettings.ServerName + ftpSettings.SubFolderName, ftpSettings.UserName, ftpSettings.Password, null);

            try
            {
                files = ftpHelper.GetDirectoryListing(".pdf");
            }
            catch (Exception ex)
            {
                //Some logging here...
            }

            return files;
        }

        #endregion

        #region SFTPMethods

        public static bool RenameFile(FTPSettings ftpSettings, string FTPFileName, string FTPNewName, int fileDetailsId, ProcessTask task)
        {
            bool renamedFile = false;

            //DeleteFTPFile(ftpSettings, FTPNewName, fileDetailsId, task);

            task.log("renaming " + FTPFileName + " to " + FTPNewName);
            InsertFileDetailsHistory(fileDetailsId, "renaming " + FTPFileName + " to " + FTPNewName, false, task);

            SftpHelper sftpHelper = new SftpHelper(ftpSettings.ServerName, ftpSettings.UserName, ftpSettings.Password, null);
            
            try
            {
                sftpHelper.RenameRemoteFile(ftpSettings.SubFolderName + "/" + FTPFileName, ftpSettings.SubFolderName + "/" + FTPNewName);
                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", DateTime.Now, null, task);
                renamedFile = true;
            }   
            catch (Exception ex)
            {
                task.log("Failed to rename file " + FTPFileName + " to " + FTPNewName + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to rename file :" + ex.Message.ToString(), true, task);
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
            }

            return renamedFile;
        }

        public static bool DeleteFile(FTPSettings ftpSettings, string FTPFileName, int fileDetailsId, ProcessTask task)
        {
            bool deletedFile = false;

            task.log("deleting " + FTPFileName + " from " + ftpSettings.SubFolderName);
            InsertFileDetailsHistory(fileDetailsId, "deleting " + FTPFileName + " from " + ftpSettings.SubFolderName, false, task);

            SftpHelper sftpHelper = new SftpHelper(ftpSettings.ServerName, ftpSettings.UserName, ftpSettings.Password, null);

            try
            {
                sftpHelper.DeleteFile(ftpSettings.SubFolderName + "/" + FTPFileName);
                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "COMPLETE", DateTime.Now, null, task);
                deletedFile = true;
            }
            catch (Exception ex)
            {
                task.log("Failed to delete file " + FTPFileName + " from " + ftpSettings.SubFolderName + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to delete file :" + ex.Message.ToString(), true, task);
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
            }

            return deletedFile;
        }

        public static bool DownLoadFile(FTPSettings ftpSettings, string FTPFileName, FileInfo sourceFile, int fileDetailsId, ProcessTask task, int categoryIdentifier)
        {
            bool fileDownloaded = false;
            long fileSize = 0;

            SftpHelper sftpHelper = new SftpHelper(ftpSettings.ServerName, ftpSettings.UserName, ftpSettings.Password, ftpSettings.SubFolderName);
            InsertFileDetailsHistory(fileDetailsId, "Retrieving from " + ftpSettings.ServerName, false, task);
    
            task.log("Reading file stream for " + FTPFileName);

            try
            {
                sftpHelper.DownloadFile(sourceFile.DirectoryName, FTPFileName);
                
                fileSize = sftpHelper.GetFileSize(FTPFileName);
                task.log("Downloaded " + fileSize + " bytes"); 

                InsertFileDetailsHistory(fileDetailsId, "File succesfully Downloaded (" + fileSize + "kb)", false, task); 

                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "RECEIVED", DateTime.Now, null, task);
                fileDownloaded = true;
            }    
            catch (Exception ex)
            {
                //-- file could not be sent
                task.log("Failed to download file " + sourceFile.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                InsertFileDetailsHistory(fileDetailsId, "Failed to download file : " + ex.Message.ToString(), true, task);

                //-- Mark it as failed
                UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
            }

            return fileDownloaded;
        }

        public static List<string> DirectoryListing(FTPSettings ftpSettings, ProcessTask task, string fileName)
        {
            List<string> files = new List<string>();

            task.log("Reading from " + ftpSettings.ServerName + ftpSettings.SubFolderName);
            task.log("FTP Credentials are User: " + ftpSettings.UserName + " | Password: " + ftpSettings.Password);

            SftpHelper sftpHelper = new SftpHelper(ftpSettings.ServerName, ftpSettings.UserName, ftpSettings.Password, ftpSettings.SubFolderName);
            
            try
            {
                files = sftpHelper.GetDirectoryListing(fileName).ToList();
            }
            catch (Exception ex)
            {
                task.log(ex.Message.ToString());
                task.log("Could not read listing from " + ftpSettings.ServerName + ftpSettings.SubFolderName);
            }
            
            return files;
        }

        public static bool UploadFile(FTPSettings ftpSettings, FileInfo file, int fileDetailsId, ProcessTask task)
        {
            bool fileUploaded = false;

            SftpHelper sftpHelper = new SftpHelper(ftpSettings.ServerName, ftpSettings.UserName, ftpSettings.Password, ftpSettings.SubFolderName);
            
            InsertFileDetailsHistory(fileDetailsId, "Sending to " + ftpSettings.ServerName, false, task);
            task.log("Sending to " + ftpSettings.ServerName);

            InsertFileDetailsHistory(fileDetailsId, "SFTP Credentials are User: " + ftpSettings.UserName + " Password: " + ftpSettings.Password, false, task);
            task.log("FTP Credentials are User: " + ftpSettings.UserName + " | Password: " + ftpSettings.Password);

            task.log("Reading file stream for " + file.Name);
            
            try
            {
                sftpHelper.UploadFile(file.DirectoryName, file.Name);
                fileUploaded = true;
                InsertFileDetailsHistory(fileDetailsId, "File succesfully Sent (" + file.Length.ToString() + " bytes)", false, task);
                //-- mark the file as sent
                CommonFeedTasks.UpdateFileDetailsEntry(fileDetailsId, "SENT", DateTime.Now, null, task);
            }
            catch (Exception ex)
            {
                //-- file could not be sent
                task.log("could not send file to " + ftpSettings.ServerName + ftpSettings.SubFolderName);
                task.log("Failed to send file " + file.Name + " - " + ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                if (fileDetailsId != 0)
                {
                    InsertFileDetailsHistory(fileDetailsId, "Failed to Send file : " + ex.Message.ToString(), true, task);

                    //-- Mark it as failed
                    UpdateFileDetailsEntry(fileDetailsId, "FAILED", null, null, task);
                }
            }
            return fileUploaded;
        }

        public static bool UpdloadFiles(FTPSettings ftpSettings, ProcessTask task, string filePath)
        {
            SftpHelper sftpHelper = new SftpHelper(ftpSettings.ServerName, ftpSettings.UserName, ftpSettings.Password, ftpSettings.SubFolderName);

            try
            {
                sftpHelper.UploadFiles(filePath);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        #endregion

    }
}
