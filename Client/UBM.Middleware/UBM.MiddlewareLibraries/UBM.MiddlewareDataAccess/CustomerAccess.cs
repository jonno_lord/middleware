﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using UBM.SQLDataAccessLayer;

namespace UBM.MiddlewareDataAccess
{
    public static class CustomerAccess
    {
        /// <summary>
        /// Returns a customer from a system. Is loosly coupled to database logic, in order
        /// that the same query can be used over multiple environments.
        /// </summary>
        /// <param name="id">the id of the customer</param>
        /// <param name="connectionstring">the connectionstring relating to the system</param>
        /// <returns></returns>
        public static DataSet GetCustomer(int id, string connectionstring)
        {
            //-- this could be an SP.... (would need to exist per system).
            string sqlCommand = "SELECT * FROM Customers WHERE id = " + id.ToString();
            return (DataSet)SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

        /// <summary>
        /// Retrieves a listing of customers that are in the scrutiny queue
        /// </summary>
        /// <param name="connectionstring"></param>
        /// <returns></returns>
        public static DataSet GetScrutinyCustomers(string connectionstring)
        {
            string sqlCommand = "SELECT id FROM Customers WHERE Stage = 1 AND ISNULL(Accepted,0) = 0 AND ISNULL(Rejected,0) = 0";
            return (DataSet)SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

        /// <summary>
        /// Returns a dataset of results fired from a scrutiny rule. This is dynamically expanded
        /// at run time, and is designed to run accross any environment, which is why it is supplied
        /// with a connection string, and is not a SP.
        /// </summary>
        /// <param name="sqlstatement">the sql to execute</param>
        /// <param name="connectionstring">the connectionstring to execute on</param>
        /// <returns>a dataset of results - if no results are returned, no scrutiny was broken.</returns>
        public static DataSet GetScrutinyResults(string sqlstatement, string connectionstring)
        {
            return (DataSet)SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlstatement);
        }

        /// <summary>
        /// Retrives a list of records which needs contacts created in N200
        /// </summary>
        /// <param name="connectionstring">The connection string to execute on</param>
        /// <returns>A dataset of contacts to be created</returns>
        public static DataSet GetPartnerInsertsForN200(string connectionstring)
        {
            //-- this could be an SP.... (would need to exist per system).
            string sqlCommand = "usp_GET_PartnerInserts";
            return SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

        /// <summary>
        /// Retrives a list of records which needs contacts created in N200
        /// </summary>
        /// <param name="connectionstring">The connection string to execute on</param>
        /// <returns>A dataset of contacts to be created</returns>
        public static DataSet GetContactInsertsForN200(string connectionstring)
        {
            //-- this could be an SP.... (would need to exist per system).
            string sqlCommand = "usp_GET_ContactInserts";
            return SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

        /// <summary>
        /// Retrives a list of records which needs contacts created in N200
        /// </summary>
        /// <param name="connectionstring">The connection string to execute on</param>
        /// <returns>A dataset of contacts to be created</returns>
        public static DataSet GetUpdatesForN200(string connectionstring)
        {
            //-- this could be an SP.... (would need to exist per system).
            string sqlCommand = "usp_GET_Updates";
            return SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

    }
}
