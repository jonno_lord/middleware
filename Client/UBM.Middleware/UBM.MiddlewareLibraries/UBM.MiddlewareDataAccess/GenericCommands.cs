﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using UBM.SQLDataAccessLayer;

namespace UBM.MiddlewareDataAccess
{
    public static class GenericCommands
    {

        public static string RemoveProvider(string connectionString)
        {
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connectionString;
        }

        public static bool ExecuteScalarSqlStatement(string connectionString, string commandText)
        {
            connectionString = GenericCommands.RemoveProvider(connectionString);
            SqlDataReader sdr = SqlHelper.ExecuteReader(connectionString, CommandType.Text, commandText);
            if (sdr.Read())
                return true;
            else
                return false;
        }

        public static int ExecuteScalarStoredProcedure(SqlParameter[] parms, string connectionString, string storedProcName)
        {
            connectionString = GenericCommands.RemoveProvider(connectionString);
            return (int)SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, storedProcName, parms);
        }

        public static int ExecuteScalarStoredProcedureId(SqlParameter[] parms, string connectionString, string storedProcName)
        {
            connectionString = GenericCommands.RemoveProvider(connectionString);
            SqlDataReader sdr = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, storedProcName, parms);
            if (sdr.Read())
                return int.Parse(sdr[0].ToString());
            else
                return 0;
        }


    }
}
