﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using UBM.MiddlewareSystem;
using UBM.SQLDataAccessLayer;
using System.Data;

namespace UBM.MiddlewareDataAccess
{
    public static class SystemAccess
    {

        public static int GetSystemnameId(int systemid)
        {
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("systemid", systemid);

            return int.Parse(SqlHelper.ExecuteScalar(Connections.MiddlewareConnectionString,
               CommandType.StoredProcedure, "usp_SEL_SystemNameFromSystemId", parms).ToString());
        }

        public static bool getBatchTimedOut(int batchId)
        {
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("BatchNumber", batchId);

            SqlDataReader dr = (SqlDataReader)SqlHelper.ExecuteReader(Connections.MiddlewareConnectionString, 
                                    CommandType.StoredProcedure, 
                                    "usp_SEL_BatchTimeOuts", parms);            
            if (dr.Read())
                return true;
            else
                return false;

        }

        #region Data retrieval for system, job and process entities 


        public static List<MiddlewareSystem.StatusType> getStatusTypes()
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allstatuses = from s in db.StatusTypes select s;
            return allstatuses.ToList<MiddlewareSystem.StatusType>();
        }

        public static List<MiddlewareSystem.Schedule> getSchedules(int jobid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allschedules = from s in db.Schedules where s.Jobid == jobid select s;
            return allschedules.ToList<MiddlewareSystem.Schedule>();
        }

        public static List<MiddlewareSystem.system> getSystems()
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allsystems = from s in db.systems select s;
            return allsystems.ToList<MiddlewareSystem.system>();
        }

        public static UBM.MiddlewareSystem.job getJob(MiddlewareSystem.job job)
        {
            return getJob(job.id);
        }

        public static MiddlewareSystem.process getProcess(int processid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allprocesses = from s in db.processes where s.id == processid select s;
            return (MiddlewareSystem.process)allprocesses.SingleOrDefault();
        }

        public static MiddlewareSystem.Schedule getSchedule(MiddlewareSystem.Schedule schedule)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            return getSchedule(schedule.id);
        }

        public static MiddlewareSystem.Schedule getSchedule(int scheduleid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allschedules = from s in db.Schedules where s.id == scheduleid select s;
            return (MiddlewareSystem.Schedule)allschedules.SingleOrDefault();
        }

        public static MiddlewareSystem.job getJob(int jobid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var alljobs = from s in db.jobs where s.id == jobid select s;
            return (MiddlewareSystem.job)alljobs.SingleOrDefault();
        }

        public static MiddlewareSystem.system getSystem(MiddlewareSystem.system system)
        {
            return getSystem(system.id);
        }
        public static MiddlewareSystem.system getSystem(int systemid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allsystems = from s in db.systems where s.id == systemid select s;
            return (MiddlewareSystem.system)allsystems.SingleOrDefault();
        }

        public static List<MiddlewareSystem.Schedule> getSchedules()
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allschedules = from s in db.Schedules select s;
            return allschedules.ToList<MiddlewareSystem.Schedule>();

        }

        public static List<MiddlewareSystem.job> getJobs(MiddlewareSystem.system system)
        {
            return getJobs(system.id);
        }

        public static List<MiddlewareSystem.job> getJobs()
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();
            var alljobs = from j in db.jobs select j;
            return alljobs.ToList<MiddlewareSystem.job>();
        }

        public static List<MiddlewareSystem.job> getJobs(int systemid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var alljobs = from j in db.jobs where j.Systemid == systemid select j;
            return alljobs.ToList<MiddlewareSystem.job>();
        }

        public static List<MiddlewareSystem.job> getJobsWithStatus(string status)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var alljobs = from j in db.jobs where j.Status==status select j;
            return alljobs.ToList<MiddlewareSystem.job>();
        }


        public static List<MiddlewareSystem.job> getJobsLastExecuted(int excludeId)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var alljobs = from j in db.jobs where j.id != excludeId orderby j.LastExecuted descending select j;
            return alljobs.ToList<MiddlewareSystem.job>();
        }




        public static List<MiddlewareSystem.job> getFailedJobs(DateTime fromDate)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var alljobs = from j in db.jobs where j.Status == "FAILED" && j.LastExecuted > fromDate select j;
            return alljobs.ToList<MiddlewareSystem.job>();
        }



        
        public static List<MiddlewareSystem.process> getProcesses(MiddlewareSystem.job job)
        {
            return getProcesses(job.id);
        }
        public static List<MiddlewareSystem.process> getProcesses(int jobid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            var allprocesses = from p in db.processes where p.Jobid == jobid orderby p.Sequence select p;
            return allprocesses.ToList<MiddlewareSystem.process>();
        }


        #endregion

        #region "Set Commands"

        public static void setSchedule(MiddlewareSystem.Schedule schedule)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            if (schedule.id == 0)
            {
                db.Schedules.InsertOnSubmit(schedule);
            }
            else
            {
                db.Schedules.Attach(schedule);
            }

            db.SubmitChanges();
        }

        public static void setJob(MiddlewareSystem.job job)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            if (job.id == 0)
            {
                db.jobs.InsertOnSubmit(job);
            }
            else
            {
                MiddlewareSystem.job prevJob = db.jobs.Single(sys => sys.id == job.id);

                //-- what is the point of using ORM?
                prevJob.Description = job.Description;
                prevJob.Disabled = job.Disabled;
                prevJob.LastExecuted = job.LastExecuted;
                prevJob.Sequence = job.Sequence;
                prevJob.ShortName = job.ShortName;
                prevJob.Status = job.Status;
                prevJob.FailureNotificationSent = job.FailureNotificationSent;
                prevJob.Systemid = job.Systemid;
                //-- end of what is the point...
            }
            db.SubmitChanges();
        }

        public static void setProcess(MiddlewareSystem.process process)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            if (process.id == 0)
            {
                db.processes.InsertOnSubmit(process);
            }
            else
            {

                MiddlewareSystem.process prevProcess = db.processes.Single(sys => sys.id == process.id);

                //-- what is the point of using ORM?
                prevProcess.AssemblyName = process.AssemblyName;
                prevProcess.ClassName = process.ClassName;
                prevProcess.Description = process.Description;
                prevProcess.Disabled = process.Disabled;
                prevProcess.Jobid = process.Jobid;
                prevProcess.LastExecuted = process.LastExecuted;
                prevProcess.id = process.id;
                prevProcess.ProcessTypeid = process.ProcessTypeid;
                prevProcess.Sequence = process.Sequence;
                prevProcess.ShortName = process.ShortName;
                prevProcess.Status = process.Status;
                //-- end of what is the point...
            }
            db.SubmitChanges();
        }

        public static void setSystem(MiddlewareSystem.system system)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            if (system.id == 0)
            {
                db.systems.InsertOnSubmit(system);
                db.SubmitChanges();
            }
            else
            {
                MiddlewareSystem.system prevSystem = db.systems.Single(sys => sys.id == system.id);

                //-- what is the point of using ORM?
                prevSystem.ShortName = system.ShortName;
                prevSystem.Version = system.Version;
                prevSystem.Description = system.Description;
                prevSystem.Disabled = system.Disabled;
                prevSystem.Acronym = system.Acronym;
                //-- end of what is the point...
                db.SubmitChanges();
            }
            
        }

        public static void disableAllSystems()
        {
            List<MiddlewareSystem.system> systems = getSystems();
            foreach (MiddlewareSystem.system system in systems)
            {
                system.Disabled = true;
                setSystem(system);
            }
        }

        public static void enableAllSystems()
        {
            List<MiddlewareSystem.system> systems = getSystems();
            foreach (MiddlewareSystem.system system in systems)
            {
                system.Disabled = false;
                setSystem(system);
            }
        }

        public static void delSchedule(int scheduleid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            MiddlewareSystem.Schedule removeSchedule = db.Schedules.Single(sys => sys.id == scheduleid);
            db.Schedules.DeleteOnSubmit(removeSchedule);
            db.SubmitChanges();
        }

        public static void delSystem(int systemid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            MiddlewareSystem.system removeSystem = db.systems.Single(sys => sys.id == systemid);
            db.systems.DeleteOnSubmit(removeSystem);
            db.SubmitChanges();
        }

        public static void delProcess(int processid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            MiddlewareSystem.process removeProcess = db.processes.Single(sys => sys.id == processid);
            db.processes.DeleteOnSubmit(removeProcess);
            db.SubmitChanges();
        }

        public static void disableAllSchedules(int jobid)
        {

            List<MiddlewareSystem.Schedule> schedules = getSchedules(jobid);
            foreach (MiddlewareSystem.Schedule schedule in schedules)
            {
                schedule.Disabled = true;
                setSchedule(schedule);
            }
        }


        public static void enableAllSchedules(int jobid)
        {
            List<MiddlewareSystem.Schedule> schedules = getSchedules(jobid);
            foreach (MiddlewareSystem.Schedule schedule in schedules)
            {
                schedule.Disabled = false;
                setSchedule(schedule);
            }
        }


        public static void disableAllProcesses(int jobid)
        {
            List<MiddlewareSystem.process> processes = getProcesses(jobid);
            foreach (MiddlewareSystem.process process in processes)
            {
                process.Disabled = true;
                setProcess(process);
            }
        }

        public static void enableAllProcesses(int jobid)
        {
            List<MiddlewareSystem.process> processes = getProcesses(jobid);
            foreach (MiddlewareSystem.process process in processes)
            {
                process.Disabled = false;
                setProcess(process);
            }
        }


        public static void disableAllJobs(int systemid)
        {
            List<MiddlewareSystem.job> jobs = getJobs(systemid);
            foreach (MiddlewareSystem.job job in jobs)
            {
                job.Disabled = true;
                setJob(job);
            }
        }

        public static void enableAllJobs(int systemid)
        {
            List<MiddlewareSystem.job> jobs = getJobs(systemid);
            foreach (MiddlewareSystem.job job in jobs)
            {
                job.Disabled = false;
                setJob(job);
            }
        }

        public static void delJob(int jobid)
        {
            UBM.MiddlewareSystem.MiddlewareSystemDataContext db = new MiddlewareSystem.MiddlewareSystemDataContext();

            MiddlewareSystem.job removeJob = db.jobs.Single(sys => sys.id == jobid);
            db.jobs.DeleteOnSubmit(removeJob);
            db.SubmitChanges();
        }




        #endregion
    }

}
