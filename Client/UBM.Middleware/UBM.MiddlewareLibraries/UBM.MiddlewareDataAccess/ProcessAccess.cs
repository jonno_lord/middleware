﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

using UBM.SQLDataAccessLayer;

namespace UBM.MiddlewareDataAccess
{
    public static class ProcessAccess
    {

        public enum enmLevel
        {
            Critical = 10,
            Serious = 20,
            Important = 100,
            Standard = 300
        }



        public enum enmMiddlewareStatus
        {
            RED = 1,
            ORANGE = 2,
            YELLOW = 3,
            GREEN = 4,
            WHITE = 5
        }



        public static string GetMiddlewareStatus()
        {

            string middlewarestatus = (string)SqlHelper.ExecuteScalar(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_SEL_MiddlewareStatus");

            return middlewarestatus;

        }

        /// <summary>
        /// Sets a job status to failed, along with any process that were PROCESSING
        /// or CREATING. This is mainly used for unhandled failures, and thread aborts.
        /// </summary>
        /// <param name="jobId"></param>
        public static void SetJobToFailed(int jobId)
        {
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("jobid", SqlDbType.Int);
            parms[0].Value = jobId;

            SqlHelper.ExecuteScalar(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_UPD_JobFailed", parms);

        }

        /// <summary>
        /// Creates a new batch number based on a Job - this is used for the execution of all processes for a run
        /// to keep track of the actions of the job run. 
        /// </summary>
        /// <param name="jobid">The id of the job to assign a new Batch number</param>
        /// <returns>a new batch number</returns>
        public static int insertBatchStatus(int jobid)
        {
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("jobid", SqlDbType.Int);
            parms[0].Value = jobid;

            return int.Parse(SqlHelper.ExecuteScalar(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_ins_Batch", parms).ToString());
        }

        public static void insertErrorLog(string message, string innerException, string source, string stackTrace)
        {

            SqlParameter[] parms = new SqlParameter[4];

            parms[0] = new SqlParameter("message", message);
            parms[1] = new SqlParameter("innerException", innerException);
            parms[2] = new SqlParameter("source", source);
            parms[3] = new SqlParameter("stackTrace", stackTrace);

            SqlHelper.ExecuteNonQuery(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_ins_ErrorLog", parms);
        }


        public static void updateBatchStatus(int batchid, string status)
        {
            SqlParameter[] parms = new SqlParameter[2];

            parms[0] = new SqlParameter("batchid", batchid);
            parms[1] = new SqlParameter("status", status);

            SqlHelper.ExecuteNonQuery(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_upd_Batch", parms);
        }



        /// <summary>
        /// Writes job and process log information to the system.
        /// </summary>
        /// <param name="jobid">The job id to log</param>
        /// <param name="processid">The process being logged</param>
        /// <param name="level">the severity of the logging entry</param>
        /// <param name="description">Details of the log entry</param>
        public static void insertBatchLog(int jobid, int processid, int batchid, enmLevel level, string description)
        {
            SqlParameter[] parms = new SqlParameter[5];

            parms[0] = new SqlParameter("@jobid", SqlDbType.Int);
            parms[0].Value = jobid;

            parms[1] = new SqlParameter("@processid", SqlDbType.Int);
            parms[1].Value = processid;

            parms[2] = new SqlParameter("@batchid", SqlDbType.Int);
            parms[2].Value = batchid;

            parms[3] = new SqlParameter("@loglevel", SqlDbType.Int);
            parms[3].Value = (int)level;

            parms[4] = new SqlParameter("@description", SqlDbType.VarChar, 1024);
            parms[4].Value = description;

            SqlHelper.ExecuteNonQuery(Connections.MiddlewareConnectionString,
                 CommandType.StoredProcedure, "usp_ins_BatchLog", parms);
        }

        public static int insertBatchProcessStatus(int processid, int batchid)
        {
            SqlParameter[] parms = new SqlParameter[2];

            parms[0] = new SqlParameter("processid", processid);
            parms[1] = new SqlParameter("batchid", batchid);

            return int.Parse(SqlHelper.ExecuteScalar(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_ins_BatchProcess", parms).ToString());
        }

        public static void updateBatchProcessStatus(int batchprocessid, string status)
        {
            SqlParameter[] parms = new SqlParameter[2];

            parms[0] = new SqlParameter("batchprocessid", batchprocessid);
            parms[1] = new SqlParameter("status", status);

            SqlHelper.ExecuteNonQuery(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_upd_BatchProcess", parms);
        }


    }
}
