﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using UBM.SQLDataAccessLayer;

namespace UBM.MiddlewareDataAccess
{
    public static class Connections
    {

        /// <summary>
        /// Returns a SqlConnection to the MiddlewareConnectionString. This should be the only 
        /// hardcoded reference to a connection, and is required for the system to know what
        /// to connect to, in order to establish it's environment. As a consequence 
        /// </summary>
        public static SqlConnection MiddlewareConnectionString
        {
            get { return new SqlConnection(Connections.GetMiddlewareConnectionString()); }
        }

        public static SqlConnection MiddlewareCommonConnection
        {
            get { return new SqlConnection(Connections.GetMiddlewareCommonString()); }
        }

        /// <summary>
        /// Returns the connection string for a system Id, defined in the Systems Table.
        /// Each Connection must have its "connection name" - e.g. "ESOPConnectionString" in
        /// the systems table under its relevant record - and then that connection name should
        /// be defined in the web.config such as 
        /// 	"...add name="MiddlewareSystemConnectionString" connectionString="Data Source=STEVE-PC;Initial Catalog=MiddlewareSystem;Persist Security Info=True;User ID=middlewareadmin;Password=ginger" providerName="System.Data.SqlClient"
        /// These two entries need to match up, otherwise the system does not know which connection is which.
        /// </summary>
        /// <param name="systemid">the id in the system table that relates to the connection</param>
        /// <returns>the connection string defined in .config</returns>
        public static string GetConnectionString(int systemid)
        {
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("systemid", SqlDbType.Int);
            parms[0].Value = systemid;

            string connectioname = (string)SqlHelper.ExecuteScalar(Connections.MiddlewareConnectionString,
                CommandType.StoredProcedure, "usp_SEL_SystemConnectionName", parms);

            //-- make sure we could find this system
            if (connectioname == null || connectioname.Length == 0)
                throw new Exception("Could not find connection name for system id" + systemid.ToString());

            //-- make sure that the connection string is in the config file.
            if (ConfigurationManager.ConnectionStrings[connectioname] == null)
                throw new Exception("Connection " + connectioname + " does not exist in .config");

            return ConfigurationManager.ConnectionStrings[connectioname].ToString();

        }

        /// <summary>
        /// Returns the connection string -> note do not supply the word "ConnectionString" to the System,
        /// this method automatically adds it.
        /// </summary>
        /// <param name="systemName">The system (e.g. ASOP, ESOP)</param>
        /// <returns>the connection string</returns>
        public static string GetConnectionString(string systemName)
        {
            return ConfigurationManager.ConnectionStrings[systemName + "ConnectionString"].ToString();
        }


        /// <summary>
        /// The middleware connection string is the only direct reference to the Connectionstring
        /// Settings configuration in the web.config. All other connections should go via "GetConnectionString"
        /// This reference is required, as it is the only way Middleware can connect to its repository.
        /// </summary>
        /// <returns></returns>
        public static string GetMiddlewareConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MiddlewareSystemConnectionString"].ToString();
        }

        public static string GetMiddlewareCommonString()
        {
            return ConfigurationManager.ConnectionStrings["MiddlewareCommonConnectionString"].ToString();
        }


    }
}
