﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using UBM.SQLDataAccessLayer;
using System.Data.SqlClient;

namespace UBM.MiddlewareDataAccess
{
    public static class InvoiceAccess
    {
        /// <summary>
        /// Returns a list of the batch numbers to produce receipts for
        /// </summary>
        /// <returns>Dataset of the BatchNumbers</returns>
        public static DataSet GetReceiptBatches(string connectionstring)
        {
            //-- this could be an SP.... (would need to exist per system).
            string sqlCommand = "usp_SEL_ReceiptBatches";
            return (DataSet)SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

        public static DataSet GetReceiptNumbers(string connectionstring)
        {
            string sqlCommand = "usp_SEL_ReceiptCopies";
            return (DataSet)SqlHelper.ExecuteDataset(connectionstring, CommandType.Text, sqlCommand);
        }

        public static void SetReceiptCopy(string connectionstring, int id)
        {
            string sqlCommand = "execute usp_SET_ReceiptCopy " + id.ToString() + "";
            SqlHelper.ExecuteNonQuery(connectionstring, CommandType.Text, sqlCommand);
        }

        public static string GetEventsforceDocumentNumber(string connectionstring, string invoiceNumber)
        {
            //-- look up the Documnet Numner from JDE
            invoiceNumber = invoiceNumber.Replace(".pdf", "");

            int number = 0;
            if (!int.TryParse(invoiceNumber, out number)) return "";

            string SQL = "SELECT DISTINCT '82_' + CONVERT(VARCHAR,JDE_Account_No) + '~' + c.ShipTo + '_' + Document_Number + '_' + Document_Number + '_' + " +
                         "CONVERT(VARCHAR(10), Invoice_Date, 112) as FileName from Invoices i " +
                         "INNER JOIN Customers c ON i.JDE_Account_No = c.JDE_Account_Number " +
                         "WHERE Document_Number = " + invoiceNumber;

            SqlDataReader dr = SqlHelper.ExecuteReader(connectionstring, CommandType.Text, SQL);
            if(dr.Read())
            {
                return dr["FileName"].ToString() +".pdf";
            }

            return "";
        }

        public static string GetCARSDocumentNumber(string connectionstring, string invoiceNumber)
        {
            //-- look up the Documnet Numner from JDE
            invoiceNumber = invoiceNumber.Replace(".pdf", "");

            int number = 0;
            if (!int.TryParse(invoiceNumber, out number)) return "";

            string SQL = "SELECT DISTINCT '82_' + CONVERT(VARCHAR,JDE_Account_No) + '~' + c.ShipTo + '_' + Document_Number + '_' + Document_Number + '_' + " + 
                         "CONVERT(VARCHAR(10), Invoice_Date, 112) as FileName from Invoices i " +
                         "INNER JOIN Customers c ON i.JDE_Account_No = c.JdeAccountNo " +
                         "WHERE Document_Number = " + invoiceNumber;

            SqlDataReader dr = SqlHelper.ExecuteReader(connectionstring, CommandType.Text, SQL);
            if (dr.Read())
            {
                return dr["FileName"].ToString() + ".pdf";
            }

            return "";
        }

    }
}
