﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace UBM.MiddlewareProcess
{

    /// <summary>
    /// See the Process Task for a defintion of each of these interface descriptions
    /// </summary>
    internal interface IProcessTask
    {

        int Processid 
        { 
            get; 
            set; 
        }
        
        int Jobid 
        { 
            get; 
            set; 
        }
        
        int Batchid 
        { 
            get; 
            set; 
        }
        
        int SystemId
        { 
            get; 
        }

        string RootPath 
        { 
            get; 
            set; 
        }

        HttpServerUtility Server 
        { 
            get; 
            set; 
        }
        
        MiddlewareSystem.job Job 
        { 
            get; 
        }
        
        MiddlewareSystem.process Process
        { 
            get; 
        }
        
        MiddlewareSystem.system System 
        { 
            get; 
        }

        ProcessEnums.enmStatus Execute();
    }
}
