﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareLogging;
using UBM.MiddlewareSSIS;
using System.Configuration;



namespace UBM.MiddlewareProcess
{
    /// <summary>
    /// This is the base class that every middleware process class MUST inherit from.
    /// </summary>
    public abstract class ProcessTask : IProcessTask
    {

        public Dictionary<string, Dictionary<string, string>> ConfigurationSettings = new Dictionary<string,Dictionary<string,string>>();

        #region IProcessTask Members

        /// <summary>
        /// The root path of where other related DLL's can be loaded
        /// </summary>
        public string RootPath
        {
            get;
            set;
        }

        /// <summary>
        /// The id relating to the process this Process belongs to
        /// </summary>
        public int Processid
        {
            get;
            set;
        }

        /// <summary>
        /// The id relating to the job the process belongs to
        /// </summary>
        public int Jobid
        {
            get;
            set;
        }

        /// <summary>
        /// The batch id that this process has been assigned to
        /// </summary>
        public int Batchid
        {
            get;
            set;
        }

        /// <summary>
        /// The system that this process belongs to (readonly)
        /// </summary>
        public int SystemId
        {
            get
            {
                return this.Job.Systemid;
            }
        }

        /// <summary>
        /// The passed server utility from the Website
        /// </summary>
        public HttpServerUtility Server
        {
            get;
            set;
        }

        /// <summary>
        /// The job this process belongs to
        /// </summary>
        public MiddlewareSystem.job Job
        {
            get
            {
                return SystemAccess.getJob(this.Jobid);
            }
        }

        /// <summary>
        /// The process this job belongs to
        /// </summary>
        public MiddlewareSystem.process Process
        {
            get
            {
                return SystemAccess.getProcess(this.Processid);
            }
        }

        /// <summary>
        /// The system this process belongs to
        /// </summary>
        public MiddlewareSystem.system System
        {
            get
            {
                return SystemAccess.getSystem(this.Job.Systemid);
            }
        }

        /// <summary>
        /// Logger wrapper method, used to log messages, and errors to the Middleware logging tables
        /// </summary>
        /// <param name="msg">The message to log</param>
        /// <param name="level">The level of the log entry (Standard, Error etc)</param>
        public void log(string msg, ProcessAccess.enmLevel level = ProcessAccess.enmLevel.Standard)
        {
            UBM.MiddlewareLogging.Logger.BatchLog(this.Jobid,
                                                this.Processid, this.Batchid,
                                                level, msg);
        }

        #endregion

        /// <summary>
        /// Reads the configuration settings (if applicable for this system) and validates that each datatype
        /// is valid. This routine will throw an exception if the data type configured is not correct, or
        /// if there is a fault reading the ConfigurationSettings table. This Method should not throw an 
        /// exception IF the configurationSettings table does not exist as at the moment (30/03/11) not all
        /// middlewareSystems will contain one.
        /// </summary>
        public void ParseConfiguration()
        {
            //-- this is the only way that we can ascertain whether the routine to get Configuration Settings is in the current Database
            string sqlVerifyUSP = "SELECT name FROM sys.objects WHERE type_desc ='SQL_STORED_PROCEDURE' AND name ='usp_GET_ConfigurationSettings'";
            try
            {
                string connectionString = Connections.GetConnectionString(this.SystemId);
                connectionString = RemoveProvider(connectionString);

                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //-- find out if the SP is ok
                        cmd.Connection = cn;
                        cmd.CommandText = sqlVerifyUSP;
                        cmd.CommandType = global::System.Data.CommandType.Text;

                        SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.Read())
                        {
                            //-- this system has a Stored Procedure called usp_GET_ConfigurationSetting.
                            //-- read the configuration
                            ReadConfiguration();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // fail the process if we cannot work this out or read the config. 
                throw new Exception("Could not determine / read if there is a ConfigurationSetting for this process : " + ex.Message);
            }

        }

        /// <summary>
        /// Reads in all configuration settings into a publicly accessible 
        /// </summary>
        private void ReadConfiguration()
        {

            string connectionString = Connections.GetConnectionString(this.SystemId);
            connectionString = RemoveProvider(connectionString);

            using (SqlConnection cn = new SqlConnection(connectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    //-- configure the SP to return all keys and values for this environment..
                    cmd.Connection = cn;
                    cmd.CommandType = global::System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.usp_GET_ConfigurationSettings";
                    string environmentName = ConfigurationManager.AppSettings["CurrentEnvironment"];
                    
                    cmd.Parameters.AddWithValue("EnvironmentName", environmentName);

                    //-- populate the configuration structure
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {

                        string category = sdr["keyCategory"].ToString();
                        string keyName = sdr["KeyName"].ToString();
                        string keyValue = sdr["KeyValue"].ToString();

                        if (!ConfigurationSettings.ContainsKey(category))
                            ConfigurationSettings.Add(category, new Dictionary<string, string>());

                        if (!ConfigurationSettings[category].ContainsKey(keyName))
                            ConfigurationSettings[category].Add(keyName, keyValue);
                        else
                        {
                            //-- if the same key is specified multiple times, retain the last key.
                            ConfigurationSettings[category][keyName] = keyValue;
                        }

                        //TODO: Perform data validation check accross datatypes
                    }

                }//-- end of connection open
            }
        }


        /// <summary>
        /// Constructor - no requirement as the object is instantiated typically through
        /// the webserver.
        /// </summary>
        public ProcessTask()
        {
            //string ServerName = ConfigurationSettings["FT"]["Value"];
        }

        /// <summary>
        /// Performs the functions of process.
        /// </summary>
        /// <returns>Succesful, or will throw an exception.</returns>
        public virtual ProcessEnums.enmStatus Execute()
        {
            //-- define the processes, batches and log details for the execution
            //-- of this class..
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stores the name of the ASOP system to connect to - used for ASOP SSIS packages
        /// that know the ASOP system via the ASOPConnection connection string
        /// </summary>
        public string ASOPConnection
        {
            get;
            set;
        }

        /// <summary>
        /// Holds the name of the payware table used for retrieving data from the payware system. this typically
        /// changes from EFTHIST to EFTTEST depending on the environment - and should be retrieved from the settings
        /// in middleware.config on the application.
        /// </summary>
        public string PaywareTable
        {
            get;
            set;
        }

        /// <summary>
        /// Holds the Connection string for the CARS database, this is required because it is an Oracle OLEDB connection
        /// and can't be set as a standard dts connection property. 
        /// </summary>
        public string CARSConnectionString
        {
            get;
            set;
        }

        /// <summary>
        /// This is a generic property that stores the name of the version of ASOP that
        /// is being used, in order to set up the SSIS packages so they connect to the
        /// correct repository
        /// </summary>
        public string MiddlewareASOPConnection
        {
            get;
            set;
        }

        /// <summary>
        /// This is a generic property used by COMMON SSiS packages, that have different
        /// Source systems - but a common destination. An example of this maybe the Customer
        /// Load to JDE - the SourceConnection changes depending on which process is calling it
        /// (for ESOP it would be MidddlewareESOP, for UBMA it would be MiddlewareUBMA etc...)
        /// </summary>
        public string MiddlewareSourceConnection
        {
            get;
            set;
        }

        /// <summary>
        /// This property is used for processing MSI files and contains the directory path and
        /// name of the file to be processed.
        /// </summary>
        public string FileConnection
        {
            get;
            set;
        }

        /// <summary>
        /// Executes the supplied stored procedure on the SystemId the process belongs to
        /// </summary>
        /// <param name="storedProcedureName">The name of the sp - including owner</param>
        /// <returns>number of rows affected</returns>
        public int FireStoredProcedure(string storedProcedureName)
        {

            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("BatchNumber", this.Batchid);
            return FireStoredProcedure(storedProcedureName, parms);

        }


        /// <summary>
        /// This Method will run the execute Scalar Stored Procedure method defined in the SQLDataAccess library
        /// </summary>
        /// <param name="storedProcedureName">name of the procedure of the sp</param>
        /// <param name="parms">array of SqlParameter used for the SP</param>
        /// <returns>an integer value relating to rows affected</returns>
        public int FireStoredProcedure(string storedProcedureName, SqlParameter[] parms)
        {

            this.log("executing stored procedure " + storedProcedureName);
            string connectionString = Connections.GetConnectionString(this.SystemId);

            //-- this should be performed with RegEx. Connection strings can hold OLE connection details that don't work with SQL data providers.
            //-- remove them.
            connectionString = RemoveProvider(connectionString);

            int results = GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, storedProcedureName);
            this.log("affected " + results.ToString() + " entries");

            return results;

        }


        public string RemoveProvider(string connectionString)
        {
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connectionString;
        }

        /// <summary>
        /// Fire a stored procedure, but only if the sqlPrecondition can be met. this is used so that
        /// systems do not unnecessarily attempt to calculate data
        /// </summary>
        /// <param name="storedProcedureName">The name of the stored procedure to execute</param>
        /// <param name="sqlPrecondition">the SQL condition that should be met in order to execute the SP</param>
        /// <param name="systemConnectionString">the SQLConnection that the precondition fires on</param>
        /// <returns>The number of rows affected, or 0 if no rows were affected</returns>     
        public int FireStoredProcedure(string storedProcedureName, string sqlPrecondition, string systemConnectionString)
        {
            // remove OLEdb details from connection string
            systemConnectionString = RemoveProvider(systemConnectionString);

            if (GenericCommands.ExecuteScalarSqlStatement(systemConnectionString, sqlPrecondition))
                return FireStoredProcedure(storedProcedureName);
            else
                return 0;

        }


        public int FireScalarStoredProcedure(string storedProcedureName, SqlParameter[] parms)
        {

            this.log("executing stored procedure " + storedProcedureName);
            string connectionString = Connections.GetConnectionString(this.SystemId);

            //-- this should be performed with RegEx. Connection strings can hold OLE connection details that don't work with SQL data providers.
            //-- remove them.
            connectionString = RemoveProvider(connectionString);

            int results = GenericCommands.ExecuteScalarStoredProcedureId(parms, connectionString, storedProcedureName);
            return results;

        }




        /// <summary>
        /// Fires an SSIS package, but only when the sqlPrecondition returns at least one row.
        /// </summary>
        /// <param name="packageName">The name of the package to fire</param>
        /// <param name="sqlPrecondition">The precondition that must be me in order for the package to run</param>
        /// <param name="systemConnectionString">The id of the system that the SQL should fire on.</param>
        /// <param name="common">If this package is in the SSIS/COMMON directory</param>
        public void FireDTSPackage(string packageName, string sqlPrecondition, string systemConnectionString, bool common = false)
        {
            systemConnectionString = RemoveProvider(systemConnectionString);

            if (GenericCommands.ExecuteScalarSqlStatement(systemConnectionString, sqlPrecondition))
                FireDTSPackage(packageName, common);

        }

        /// <summary>
        /// Overloaded version of the FireSSIS package, but without a precondtion. This method will
        /// execute an SSIS package using the UBM.MiddlewareSSIS library.
        /// </summary>
        /// <param name="packageName">The name of the package to execute</param>
        /// <param name="common">if this is a common package (i.e. sits in the SSIS/Common folder</param>
        public void FireDTSPackage(string packageName, bool common = false)
        {
            try
            {
                this.log("loading package " + packageName);

                UBM.MiddlewareSSIS.DTSPackage dts = new UBM.MiddlewareSSIS.DTSPackage(this.Processid, this.Batchid, this.Jobid, common);
                dts.MiddlewareSourceConnection = this.MiddlewareSourceConnection;   //-- specify a bespoke source connection
                dts.FileConnection = this.FileConnection;
                dts.ASOPConnection = this.ASOPConnection;
                dts.PaywareTable = this.PaywareTable;
                dts.CARSConnectionString = this.CARSConnectionString;
                dts.MiddlewareASOPConnection = this.MiddlewareASOPConnection;
                
                if (dts.ExecutePackage(packageName) == DTSPackage.DTSPackageExecution.Failed)
                    throw new Exception("Failed to execute Package " + packageName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
