﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareProcess
{
    /// <summary>
    /// Definition of Enumerators used for Middleware processes
    /// </summary>
    public static class ProcessEnums
    {
        /// <summary>
        /// The status levels that a process can return after execution
        /// </summary>       
        public enum enmStatus
        {
            /// <summary>
            /// The process worked
            /// </summary>
            Successful,

            /// <summary>
            /// The process did not work
            /// </summary>
            Failed
        }

    }
}
