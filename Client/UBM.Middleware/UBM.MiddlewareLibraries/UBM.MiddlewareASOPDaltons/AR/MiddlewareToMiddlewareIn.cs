﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using UBM.MiddlewareASOPDaltons.Receipts;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareASOPDaltons.AR
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {

        private string serverName;
        private string databaseName;
        private string userName;
        private string password;

        private void setProperties(string ConnectionString)
        {
            databaseName = Regex.Match(ConnectionString, "(Initial Catalog=|Database=){1}([\\w-]+)[;]?").Groups[2].Value;
            serverName = Regex.Match(ConnectionString, "(Data Source=){1}([\\w-]+)[;]?").Groups[2].Value;
            userName = Regex.Match(ConnectionString, "(User ID=){1}([\\w-]+)[;]?").Groups[2].Value;
            password = Regex.Match(ConnectionString, "(Password=){1}([\\w-]+)[;]?").Groups[2].Value;
        }

        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_AccountsReceivableToJDEFormat");
            string connectionString = MiddlewareDataAccess.Connections.GetConnectionString(this.SystemId);

            //-- retrieve batches to process
            DataSet batches = InvoiceAccess.GetReceiptBatches(connectionString);

            setProperties(connectionString);

            foreach (DataRow batch in batches.Tables[0].Rows)
            {
                string batchNumber = batch["BATCH_NUMBER"].ToString();
                string outputPath = ConfigurationManager.AppSettings["ReceiptsPath"].ToString().Trim();

                ConnectionInfo conInfo = new ConnectionInfo();
                conInfo.ServerName = this.serverName;
                conInfo.DatabaseName = this.databaseName;
                conInfo.UserID = this.userName;
                conInfo.Password = this.password;

                string rptFileLocation = this.Server.MapPath("~/ReceiptTemplates/");

                PdfGenerator.CreateBatchReceipts(int.Parse(batchNumber), "ASOPDaltons", outputPath, conInfo, rptFileLocation);
            }


            return ProcessEnums.enmStatus.Successful;
        }


    }

}
