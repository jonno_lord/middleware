﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPDaltons.AR
{
    public class MiddlewareToJde : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            // Set the source of this package.
            this.MiddlewareSourceConnection = "MiddlewareASOPDaltons";

            // If this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Accounts Receivable Middleware to JDE.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
