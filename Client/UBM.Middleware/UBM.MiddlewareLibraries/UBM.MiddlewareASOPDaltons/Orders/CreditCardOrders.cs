﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using System.Configuration;

namespace UBM.MiddlewareASOPDaltons.Orders
{
    public class CreditCardOrders:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareASOPConnection = "MiddlewareASOPDaltons";
            this.ASOPConnection = "ASOPDaltons";
            this.PaywareTable = ConfigurationManager.AppSettings["PaywareTable"].ToString().Trim();

            
            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Credit Card Orders Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
