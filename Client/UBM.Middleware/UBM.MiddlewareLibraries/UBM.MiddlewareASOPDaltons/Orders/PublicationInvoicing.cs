﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPDaltons.Orders
{
    public class PublicationInvoicing:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareASOPConnection = "MiddlewareASOPDaltons";
            this.ASOPConnection = "ASOPDaltons";
            
            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Publication Invoicing Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
