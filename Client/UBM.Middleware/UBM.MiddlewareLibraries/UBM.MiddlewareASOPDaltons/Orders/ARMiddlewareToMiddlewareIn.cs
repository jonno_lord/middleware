﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPDaltons.Orders
{
    public class ArMiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("[usp_TRA_AccountsReceivableToJDEFormat]");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
