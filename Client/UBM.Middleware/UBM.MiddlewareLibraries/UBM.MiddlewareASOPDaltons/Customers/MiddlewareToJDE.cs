﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareASOPDaltons.Customers
{
    public class MiddlewareToJDE : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- set the source of this package 
            this.MiddlewareSourceConnection = "MiddlewareASOPDaltons";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Customers Middleware to JDE.dtsx",
                "SELECT TOP 10 * FROM Customers WHERE Stage = 2",
                Connections.GetConnectionString("MiddlewareASOPDaltons"), true);

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
