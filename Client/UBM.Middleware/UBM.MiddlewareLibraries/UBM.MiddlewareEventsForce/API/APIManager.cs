﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;

namespace UBM.MiddlewareEventsForce.API
{
    public static class APIManager
    {
        public static string baseURI
        {
            get
            {
                return "https://www.eventsforce.net/" + ConfigurationManager.AppSettings["ClientAccount"] + "/api/v2/";
            }
        }

        public static string invoiceNumberAfter
        {
            get
            {
                return "invoices.json?invoiceNumberAfter=";
            }
        }

        public static WebClient CreateWebClient()
        {
            var client = new WebClient();
            string apiKey = ConfigurationManager.AppSettings["EventsforceAPIKey"];
            client.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(":" + apiKey));
            client.Headers["Content-Type"] = "application/json";
             
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            return client;
        }
    }
}
