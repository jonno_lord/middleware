﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Invoice
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            FireDTSPackage("EventsForce F03B11Z1 F0911Z1 Prime (MW - MW)");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
