﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Invoice
{
    public class SourceToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            CommonFF common = new CommonFF(this);

            return common.InvoicesToMiddleware();           
        }
    }
}
