﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce
{
    internal class AbstractCommon
    {
        #region Fields

        protected SqlConnection Connection;
        protected ProcessTask ProcessTask;

        #endregion

        #region Protected Methods

        protected void FireStoredProcedure(string storedProc)
        {
            SqlCommand command = new SqlCommand(storedProc, Connection);
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 600;
            command.Prepare();

            command.ExecuteNonQuery();
        }

        protected void FireStoredProcedure(string storedProc, SqlParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProc, Connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);
            command.CommandTimeout = 600;
            command.Prepare();

            command.ExecuteNonQuery();
        }

        protected List<int> GetMissingInvoices(string storedProc, int latestInvoice)
        {
            //Get Invoices we've received
            List<int> receivedInvoices = new List<int>();

            SqlCommand command = new SqlCommand(storedProc, Connection);
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 6000;
            command.Parameters.AddWithValue("startFrom", ConfigurationManager.AppSettings["startFrom"]);

            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                receivedInvoices.Add(int.Parse(item["Document_Number"].ToString()));
            }

            //Work out where we have gaps
            int start = int.Parse(ConfigurationManager.AppSettings["startFrom"]) + 1;

            List<int> missingInvoices = new List<int>();
            for (int i = start; i <= latestInvoice; i++)
            {
                if (!receivedInvoices.Contains(i))
                {
                    missingInvoices.Add(i);
                }
            }

            return missingInvoices;
        }

        #endregion

        public AbstractCommon(ProcessTask processTask)
        {
            this.ProcessTask = processTask;
        }

        internal void SetupConnection(string connectionString)
        {
            Connection = new SqlConnection(connectionString);
            Connection.Open();
        }

        internal void CloseConnection()
        {
            Connection.Dispose();
        }

    }
}
