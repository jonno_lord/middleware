﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.AR
{
    public class MiddlewareToJde : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            // Set the source of this package.
            this.MiddlewareSourceConnection = "MiddlewareEventsForce";

            // If this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Accounts Receivable Middleware to JDE.dtsx", true);

            this.FireStoredProcedure("usp_UPD_InvoiceStatus");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
