﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.AR
{
    public class SourceToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            CommonFF common = new CommonFF(this);
            return common.InvoicesToMiddleware();
        }
    }
}
