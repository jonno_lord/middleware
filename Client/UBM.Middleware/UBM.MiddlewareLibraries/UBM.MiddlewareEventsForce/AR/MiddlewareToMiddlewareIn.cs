﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.AR
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            FireDTSPackage("EventsForce Credit Cards Accounts Receivable.dtsx");

            FireStoredProcedure("usp_TRA_AccountsReceivableToJdeFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
