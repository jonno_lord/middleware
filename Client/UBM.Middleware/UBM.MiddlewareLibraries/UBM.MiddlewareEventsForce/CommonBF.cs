﻿using System;
using System.Data;
using System.Data.SqlClient;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareProcess;
using UBM.MiddlewareEventsForce.JsonModels;
using System.Configuration;
using Newtonsoft.Json;
using UBM.MiddlewareEventsForce.API;

namespace UBM.MiddlewareEventsForce
{
    internal class CommonBF : AbstractCommon
    {
        #region Constructor

        public CommonBF(ProcessTask processTask) : base(processTask)
        {
        }

        #endregion

        #region Internal Methods

        internal ProcessEnums.enmStatus PaymentsToEventsForce()
        {
            try
            {
                SetupConnection(ProcessTask.RemoveProvider(Connections.GetConnectionString("MiddlewareEventsForce")));

                FireStoredProcedure("usp_CRE_OutgoingPayments");
                FireStoredProcedure("usp_INS_PaymentsOut");

                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ##EF_Outgoing_Payments", Connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                    {
                        try
                        {
                            Payment p = new Payment();
                            p.amount = dataRow["Amount"].ToString();
                            p.comment = dataRow["Comment"].ToString();
                            p.currencyCode = dataRow["Currency"].ToString();
                            p.transactionReference = dataRow["EFPYID"].ToString();
                            p.paymentMethod = dataRow["paymentMethod"].ToString();

                            string invoiceNumber = dataRow["RelatedInvoiceNumber"].ToString();
                            string uri = APIManager.baseURI + "invoices/" + invoiceNumber + "/payments.json";
                            var content = JsonConvert.SerializeObject(p);

                            using (var client = APIManager.CreateWebClient())
                            {
                                string response = client.UploadString(uri, "POST", content);

                                JsonModels.Payments payments = JsonConvert.DeserializeObject<JsonModels.Payments>(response);

                                if (payments.responseCode == 201)
                                {
                                    ProcessTask.log("Succesfully uploaded payment for invoice " + invoiceNumber);
                                }
                                else
                                {
                                    ProcessTask.log("Failed to upload payment for invoice " + invoiceNumber + ". " + payments.userErrorMessage, ProcessAccess.enmLevel.Standard);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessTask.log("Problem sending payment to Eventsforce " + ex.Message, ProcessAccess.enmLevel.Important);
                        }
                    }
                }

                FireStoredProcedure("usp_UPD_F5503B14");

            }
            catch (Exception exception)
            {
                ProcessTask.log("Payments to Eventsforce failed: " + exception, ProcessAccess.enmLevel.Serious);
                throw;
            }
            finally
            {
                CloseConnection();
            }

            return ProcessEnums.enmStatus.Successful;
        }


        internal ProcessEnums.enmStatus CustomersToEventsforce()
        {
            try
            {

                SetupConnection(ProcessTask.RemoveProvider(Connections.GetConnectionString("MiddlewareEventsForce")));

                FireStoredProcedure("usp_CRE_OutgoingCustomersTable");
                FireStoredProcedure("usp_INS_CustomersOut");

                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM ##EF_Outgoing_Customers", Connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                    {
                        try
                        {
                            Person p = new Person();
                            p.externalCustomerAccount = dataRow["ExternalCustomerAccount"].ToString();

                            string personId = dataRow["CodId"].ToString();
                           
                            string uri = APIManager.baseURI + "people/" + personId + ".json";
                            var content = JsonConvert.SerializeObject(p);

                            using (var client = APIManager.CreateWebClient())
                            {
                                string response = client.UploadString(uri, "PATCH", content);

                                People people = JsonConvert.DeserializeObject<People>(response);

                                if (people.responseCode == 200)
                                {
                                    ProcessTask.log("Succesfully updated External Customer Account number to " + p.externalCustomerAccount + " on personId " + personId);
                                }
                                else
                                {
                                    ProcessTask.log("Failed to update External Customer Account for personId " + personId + ". " + people.userErrorMessage, ProcessAccess.enmLevel.Standard);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessTask.log("Problem updating customer in Eventsforce" + ex.Message, ProcessAccess.enmLevel.Important);
                        }
                    }
                }

                FireStoredProcedure("usp_UPD_CustomersOut");

            }
            catch (Exception exception)
            {
                ProcessTask.log("Customers to Eventsforce failed: " + exception, ProcessAccess.enmLevel.Serious);
                throw;
            }
            finally
            {
                CloseConnection();
            }

            return ProcessEnums.enmStatus.Successful;
        }

        #endregion
    }
}
