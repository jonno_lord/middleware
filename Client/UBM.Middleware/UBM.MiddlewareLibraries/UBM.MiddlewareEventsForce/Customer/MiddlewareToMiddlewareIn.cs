﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Customer
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            FireStoredProcedure("usp_TRA_CustomersToJdeFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
