﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using UBM.MiddlewareProcess;
using UBM.MiddlewareEventsForce.API;
using System.Data.SqlClient;
using UBM.MiddlewareDataAccess;
using System.Data;
using System.Collections.Generic;
using UBM.MiddlewareEventsForce;

namespace UBM.MiddlewareEventsForce.Customer
{
    public class SourceToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            CommonFF commonFF = new CommonFF(this);
        
            return commonFF.CustomersToMiddleware();
        }
    }
}
