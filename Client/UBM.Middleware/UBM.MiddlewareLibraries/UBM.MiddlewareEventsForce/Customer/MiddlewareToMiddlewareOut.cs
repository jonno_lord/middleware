﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Customer
{
    public class MiddlewareToMiddlewareOut : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.FireStoredProcedure("usp_TRA_CustomersToEventsForceFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
