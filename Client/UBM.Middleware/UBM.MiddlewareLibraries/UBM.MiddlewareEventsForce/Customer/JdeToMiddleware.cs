﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Customer
{
    public class JdeToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.MiddlewareSourceConnection = "MiddlewareEventsForce";
            this.FireDTSPackage("COMMON Customers JDE to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
