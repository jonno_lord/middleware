﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Customer
{
    public class MiddlewareToSource : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            CommonBF commonBF = new CommonBF(this);

            return commonBF.CustomersToEventsforce();
        }
    }
}
