﻿using UBM.MiddlewareDataAccess;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Customer
{
    public class MiddlewareToJde : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.MiddlewareSourceConnection = "MiddlewareEventsForce";
            this.FireDTSPackage("COMMON Customers Middleware to JDE.dtsx", "SELECT TOP 10 * FROM Customers WHERE Stage = 2", Connections.GetConnectionString("MiddlewareEventsForce"), true);

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
