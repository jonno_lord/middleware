﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Payments
{
    public class JdeToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- source connection needs to be defined for a generic (common) package
            this.MiddlewareSourceConnection = "MiddlewareEventsForce";

            this.FireDTSPackage("Common Payments JDE to Middleware.dtsx", true); 

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
