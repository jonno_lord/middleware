﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Payments
{
    public class MiddlewareToMiddlewareOut : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            // this.FireStoredProcedure("usp_TRA_PaymentsToEventsForceFormat");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
