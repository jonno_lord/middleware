﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareEventsForce.Payments
{
    public class MiddlewareToSource : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            CommonBF common = new CommonBF(this);
            return common.PaymentsToEventsForce();
        }
    }
}
 