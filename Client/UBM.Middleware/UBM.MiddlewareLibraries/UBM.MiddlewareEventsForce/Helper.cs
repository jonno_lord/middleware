﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareProcess;
using UBM.MiddlewareEventsForce.JsonModels;


namespace UBM.MiddlewareEventsForce
{
    public static class Helper
    {

        #region Public Static Methods

        public static void InsertCustomer(People person, Invoices invoice, Events ev, string connectionString, ProcessTask processTask)
        {
            try
            {
                SqlParameter[] parms = SetCustomerSqlParameters(person, invoice, ev);
                GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_INS_Customer");

                processTask.log("Successfully inserted customer " + person.data.personID);

            }
            catch (Exception ex)
            {
                processTask.log("Unable to insert customer " + person.data.personID + ". " + ex.Message);
            }
        }

        public static void InsertInvoice(People person, Invoices invoice, Events ev, Payment p, string connectionString, ProcessTask processTask)
        {
            try
            {
                int itemNumber = 0;

                foreach (InvoiceLineItem lineItem in invoice.data.invoiceLineItems)
                {
                    if (lineItem.type == "item") itemNumber++;

                    SqlParameter[] parms = SetInvoiceSqlParameters(person, invoice, ev, p, lineItem, itemNumber);
                    GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_INS_Invoice");
                }

                processTask.log("Successfully inserted invoice " + invoice.data.invoiceNumber);
            }
            catch (Exception ex)
            {
                processTask.log("Unable to insert invoice " + invoice.data.invoiceNumber + ". " + ex.Message);
            }
        }

        public static SqlParameter[] SetInvoiceSqlParameters(People person, Invoices invoice, Events ev, Payment p, InvoiceLineItem lineItem, int itemNumber)
        {
            SqlParameter[] parms = new SqlParameter[34];

            parms[0] = new SqlParameter("invoiceNumber", invoice.data.invoiceNumber);
            parms[1] = new SqlParameter("originalInvoiceNumber", invoice.data.previousRelatedInvoiceNumber );
            parms[2] = new SqlParameter("codId", person.data.personID);
            parms[3] = new SqlParameter("bookingReference", invoice.data.registrationReference);
            parms[4] = new SqlParameter("eventName", ev.data.eventName);
            parms[5] = new SqlParameter("eventStartDate", ev.data.eventStartDateTime);
            parms[6] = new SqlParameter("eventEndDate", ev.data.eventEndDateTime);
            parms[7] = new SqlParameter("currencyCode", invoice.data.currencyCode);
            parms[8] = new SqlParameter("grossPrice", invoice.data.grandTotal);
            parms[9] = new SqlParameter("vatAmount", invoice.data.vatApplied);
            parms[10] = new SqlParameter("paymentMethod", invoice.data.selectedPaymentMethod);
            parms[11] = new SqlParameter("firstName", invoice.data.invoiceAddress.firstName);
            parms[12] = new SqlParameter("lastName", invoice.data.invoiceAddress.lastName);
            parms[13] = new SqlParameter("addressLine1", invoice.data.invoiceAddress.addressLine1);
            parms[14] = new SqlParameter("addressLine2", invoice.data.invoiceAddress.addressLine2);
            parms[15] = new SqlParameter("addressLine3", invoice.data.invoiceAddress.addressLine3);
            parms[16] = new SqlParameter("city", invoice.data.invoiceAddress.town);
            parms[17] = new SqlParameter("postCode", invoice.data.invoiceAddress.postCode);
            parms[18] = new SqlParameter("country", invoice.data.invoiceAddress.country);
            parms[19] = new SqlParameter("companyName", invoice.data.invoiceAddress.company);
            parms[20] = new SqlParameter("email", invoice.data.invoiceAddress.email);
            parms[21] = new SqlParameter("vatRegistrationNumber", invoice.data.vatRegistrationNumber);
            parms[22] = new SqlParameter("businessUnit", invoice.data.businessUnit);
            parms[23] = new SqlParameter("venue", ev.data.venueName);
            parms[24] = new SqlParameter("numberOfAttendees", invoice.data.numberOfAttendees ?? DBNull.Value);
            parms[25] = new SqlParameter("type", lineItem.type);
            parms[26] = new SqlParameter("description", lineItem.description);
            parms[27] = new SqlParameter("quantity", lineItem.quantity ?? 0);
            parms[28] = new SqlParameter("unitPrice", lineItem.unitPrice);
            parms[29] = new SqlParameter("amount", lineItem.amount);
            parms[30] = new SqlParameter("purchaseCreated", invoice.data.creationTimestamp);
            parms[31] = new SqlParameter("itemNumber", itemNumber);
            parms[32] = new SqlParameter("accountNumber", person.data.externalCustomerAccount);
            parms[33] = new SqlParameter("transactionReference", p == null ? "" : p.transactionReference);
            
            return parms;
        }

        public static SqlParameter[] SetCustomerSqlParameters(People person, Invoices invoice, Events ev)
        {
            SqlParameter[] parms = new SqlParameter[22];

            parms[0] = new SqlParameter("codId", person.data.personID);
            parms[1] = new SqlParameter("firstName", person.data.firstName);
            parms[2] = new SqlParameter("lastName", person.data.lastName);
            parms[3] = new SqlParameter("salutation", person.data.title);
            parms[4] = new SqlParameter("addressLine1", person.data.addressLine1);
            parms[5] = new SqlParameter("addressLine2", person.data.addressLine2);
            parms[6] = new SqlParameter("addressLine3", person.data.addressLine3);
            parms[7] = new SqlParameter("jobTitle", person.data.jobTitle);
            parms[8] = new SqlParameter("postcode", person.data.postCode);
            parms[9] = new SqlParameter("country", person.data.country);
            parms[10] = new SqlParameter("company", person.data.company);
            parms[11] = new SqlParameter("email", person.data.email);
            parms[12] = new SqlParameter("telephone", person.data.phoneNumber);
            parms[13] = new SqlParameter("town", person.data.town);
            parms[14] = new SqlParameter("eventName", ev.data.eventName);
            parms[15] = new SqlParameter("eventStartDate", ev.data.eventStartDateTime);
            parms[16] = new SqlParameter("paymentMethod", invoice.data.selectedPaymentMethod);
            parms[17] = new SqlParameter("businessUnit", invoice.data.businessUnit);
            parms[18] = new SqlParameter("purchaseStatus", invoice.data.paymentStatus);
            parms[19] = new SqlParameter("grossPrice", Convert.ToDecimal(invoice.data.grandTotal));
            parms[20] = new SqlParameter("currencyCode", invoice.data.currencyCode);
            parms[21] = new SqlParameter("county", person.data.county);

            return parms;
        }

        public static int GetLatestInvoice(string connectionString)
        {
            int i;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = new SqlCommand("dbo.usp_GET_LatestInvoice"))
                {
                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var returnParameter = sqlCommand.Parameters.Add("@retValue", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    sqlCommand.ExecuteNonQuery();

                    i = (int)returnParameter.Value;
                }

                sqlConnection.Close();
            }

            return i;
        }

        #endregion
    }
}
