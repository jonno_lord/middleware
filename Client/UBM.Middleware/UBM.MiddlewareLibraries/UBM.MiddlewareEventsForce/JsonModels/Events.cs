﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareEventsForce
{
    public class Events
    {
        public int responseCode { get; set; }
        public object systemErrorCode { get; set; }
        public string systemErrorMessage { get; set; }
        public string userErrorMessage { get; set; }
        public int itemCount { get; set; }
        public Event data { get; set; }
    }

    public class Event
    {
        public string detailsURL { get; set; }
        public int eventID { get; set; }
        public string eventName { get; set; }
        public string eventStatus { get; set; }
        public string eventStartDateTime { get; set; }
        public string eventEndDateTime { get; set; }
        public string venueName { get; set; }
        public string timeZone { get; set; }
    }
}
