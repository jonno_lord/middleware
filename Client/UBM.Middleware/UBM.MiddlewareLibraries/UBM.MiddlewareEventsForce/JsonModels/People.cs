﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareEventsForce.JsonModels
{
    public class CustomData
    {
        public string key { get; set; }
        public string value { get; set; }
        public string code { get; set; }
    }

    public class Person
    {
        public string detailsURL { get; set; }
        public int personID { get; set; }
        public string email { get; set; }
        public string company { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string title { get; set; }
        public string jobTitle { get; set; }
        public string phoneNumber { get; set; }
        public string workPhoneNumber { get; set; }
        public string pictureURL { get; set; }
        public string pictureThumbnailURL { get; set; }
        public string lastModified { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string town { get; set; }
        public string postCode { get; set; }
        public string county { get; set; }
        public string country { get; set; }
        public string username { get; set; }
        public string gender { get; set; }
        public object supersededByPersonID { get; set; }
        public string supersededByPersonURL { get; set; }
        public string externalCustomerAccount { get; set; }
        public List<CustomData> customData { get; set; }
    }

    public class People
    {
        public int responseCode { get; set; }
        public object systemErrorCode { get; set; }
        public string systemErrorMessage { get; set; }
        public string userErrorMessage { get; set; }
        public Person data { get; set; }
    }
}
