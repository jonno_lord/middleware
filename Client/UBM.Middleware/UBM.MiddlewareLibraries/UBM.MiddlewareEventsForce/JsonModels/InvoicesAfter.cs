﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareEventsForce
{
    public class InvoiceAfter
    {
        public int invoiceNumber { get; set; }
        public string detailsURL { get; set; }
    }

    public class InvoicesAfter
    {
        public int responseCode { get; set; }
        public object systemErrorCode { get; set; }
        public string systemErrorMessage { get; set; }
        public string userErrorMessage { get; set; }
        public int itemCount { get; set; }
        public List<InvoiceAfter> data { get; set; }
    }
}
