﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareEventsForce.JsonModels
{

    public class Payment
    {
        public string detailsURL { get; set; }
        public int paymentID { get; set; }
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public string paymentMethod { get; set; }
        public string comment { get; set; }
        public string transactionReference { get; set; }
    }

    public class Payments
    {
        public int responseCode { get; set; }
        public object systemErrorCode { get; set; }
        public string systemErrorMessage { get; set; }
        public string userErrorMessage { get; set; }
        public int itemCount { get; set; }
        public List<Payment> data { get; set; }
    }

}
