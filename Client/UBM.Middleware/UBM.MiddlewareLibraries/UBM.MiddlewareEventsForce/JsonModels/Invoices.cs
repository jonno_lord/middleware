﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBM.MiddlewareEventsForce
{

    public class Invoices
    {
        public int responseCode { get; set; }
        public object systemErrorCode { get; set; }
        public string systemErrorMessage { get; set; }
        public string userErrorMessage { get; set; }
        public Invoice data { get; set; }
    }
    public class InvoiceLineItem
    {
        public string description { get; set; }
        public string type { get; set; }
        public int? quantity { get; set; }
        public string unitPrice { get; set; }
        public string amount { get; set; }
    }

    public class InvoiceAddress
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string company { get; set; }
        public string email { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string postCode { get; set; }
        public string town { get; set; }
        public string country { get; set; }
    }

    public class Invoice
    {
        public int invoiceNumber { get; set; }
        public string detailsURL { get; set; }
        public string externalInvoiceReference { get; set; }
        public string type { get; set; }
        public string creationTimestamp { get; set; }
        public int previousRelatedInvoiceNumber { get; set; }
        public object nextRelatedInvoiceNumber { get; set; }
        public string registrationReference { get; set; }
        public string paymentStatus { get; set; }
        public string eventName { get; set; }
        public int eventID { get; set; }
        public string eventDetailsURL { get; set; }
        public string currencyCode { get; set; }
        public string totalBeforeVAT { get; set; }
        public string vatApplied { get; set; }
        public string grandTotal { get; set; }
        public string selectedPaymentMethod { get; set; }
        public object dueDays { get; set; }
        public string invoiceRecipientDetailsURL { get; set; }
        public string creditController { get; set; }
        public string vatRegistrationNumber { get; set; }
        public string businessUnit { get; set; }
        public string customerPONumber { get; set; }
        public object numberOfAttendees { get; set; }
        public bool markedAsPrinted { get; set; }
        public List<InvoiceLineItem> invoiceLineItems { get; set; }
        public InvoiceAddress invoiceAddress { get; set; }
        public string invoiceDocumentURL { get; set; }
    }
}
