﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareEventsForce.API;
using UBM.MiddlewareEventsForce.JsonModels;
using UBM.MiddlewareProcess;
using System.Linq;
using System.Configuration;

namespace UBM.MiddlewareEventsForce
{
    internal class CommonFF : AbstractCommon
    {
        #region Constructor

        public CommonFF(ProcessTask processTask):base(processTask)
        {
        }

        #endregion

        #region Internal Methods

        internal ProcessEnums.enmStatus CustomersToMiddleware()
        {
            try
            {
                var client = APIManager.CreateWebClient();
                var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

                string connectionString = ProcessTask.RemoveProvider(Connections.GetConnectionString("MiddlewareEventsForce"));
                SetupConnection(ProcessTask.RemoveProvider(Connections.GetConnectionString("MiddlewareEventsForce")));

                //Get most recent invoice from MW
                int latestMiddlewareInvoice = Helper.GetLatestInvoice(connectionString);

                //Get invoices from EF after most recent one received
                string inovicesAfter = client.DownloadString(APIManager.baseURI + APIManager.invoiceNumberAfter + latestMiddlewareInvoice.ToString());
                InvoicesAfter invoices = JsonConvert.DeserializeObject<InvoicesAfter>(inovicesAfter, settings);

                if (invoices.data.Count > 0)
                {
                    int latestInvoice = invoices.data.Max(x => x.invoiceNumber);

                    //Get invoices missing between first and last
                    List<int> missingInvoices = GetMissingInvoices("usp_GET_MissingInvoices", latestInvoice);

                    foreach (int missingInvoice in missingInvoices)
                    {
                        try
                        {
                            string invoiceData = client.DownloadString(APIManager.baseURI + "invoices/" + missingInvoice + ".json");
                            Invoices invoice = JsonConvert.DeserializeObject<Invoices>(invoiceData, settings);
                            
                            if (invoice.data.selectedPaymentMethod != "Credit Card")
                            {
                                string peopleData = client.DownloadString(invoice.data.invoiceRecipientDetailsURL);
                                People person = JsonConvert.DeserializeObject<People>(peopleData, settings);

                                if (string.IsNullOrEmpty(person.data.externalCustomerAccount))
                                {
                                    string eventData = client.DownloadString(invoice.data.eventDetailsURL);
                                    Events ev = JsonConvert.DeserializeObject<Events>(eventData, settings);
                                    
                                    Helper.InsertCustomer(person, invoice, ev, connectionString, ProcessTask);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessTask.log("Problem inserting customer into Middleware " + ex.Message, ProcessAccess.enmLevel.Important);
                        }
                    }
                }
                else
                {
                    ProcessTask.log("No customers to process!", ProcessAccess.enmLevel.Standard);

                }
            }
            catch (Exception ex)
            {
                ProcessTask.log(ex.Message, ProcessAccess.enmLevel.Important);
            }
            finally
            {
                CloseConnection();
            }

            return ProcessEnums.enmStatus.Successful;
        }

        internal ProcessEnums.enmStatus InvoicesToMiddleware()
        {
            try
            {
                var client = APIManager.CreateWebClient();
                var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

                string connectionString = ProcessTask.RemoveProvider(Connections.GetConnectionString("MiddlewareEventsForce"));
                SetupConnection(ProcessTask.RemoveProvider(Connections.GetConnectionString("MiddlewareEventsForce")));

                //Get most recent invoice from MW
                int latestMiddlewareInvoice = Helper.GetLatestInvoice(connectionString);
                ProcessTask.log("Latest Middleware invoice number: " + latestMiddlewareInvoice);

                latestMiddlewareInvoice--;

                //Get invoices from EF after most recent one received
                string inovicesAfter = client.DownloadString(APIManager.baseURI + APIManager.invoiceNumberAfter + latestMiddlewareInvoice.ToString());
                InvoicesAfter invoices = JsonConvert.DeserializeObject<InvoicesAfter>(inovicesAfter, settings);

                if (invoices.data.Count > 0)
                {
                    int latestInvoice = invoices.data.Max(x => x.invoiceNumber);

                    //Get invoices missing between first and last
                    List<int> missingInvoices = GetMissingInvoices("usp_GET_MissingInvoices", latestInvoice);
                    ProcessTask.log(missingInvoices.Count + " missing invoices");

                    FireStoredProcedure("usp_CRE_IncomingInvoicesTable");

                    foreach (int missingInvoice in missingInvoices)
                    {
                        try
                        {
                            string invoiceData = client.DownloadString(APIManager.baseURI + "invoices/" + missingInvoice + ".json");

                            Invoices invoice = JsonConvert.DeserializeObject<Invoices>(invoiceData, settings);

                            if (invoice.data.selectedPaymentMethod == "Credit Card")
                            {
                                if (invoice.data.markedAsPrinted)
                                {
                                    string peopleData = client.DownloadString(invoice.data.invoiceRecipientDetailsURL);
                                    People person = JsonConvert.DeserializeObject<People>(peopleData, settings);

                                    int paymentInvoice = invoice.data.type == "credit note" ? invoice.data.previousRelatedInvoiceNumber : missingInvoice;

                                    string paymentData = client.DownloadString(APIManager.baseURI + "invoices/" + paymentInvoice + "/payments.json");
                                    JsonModels.Payments p = JsonConvert.DeserializeObject<JsonModels.Payments>(paymentData, settings);
                                    int tRef = p.data.Count > 0 ? p.data.Max(q => int.Parse(q.transactionReference)) : 0;

                                    if (tRef > 0)
                                    {
                                        Payment payment = p.data.Where(x => int.Parse(x.transactionReference) == tRef).FirstOrDefault();

                                        string eventData = client.DownloadString(invoice.data.eventDetailsURL);
                                        Events ev = JsonConvert.DeserializeObject<Events>(eventData, settings);

                                        Helper.InsertInvoice(person, invoice, ev, payment, connectionString, ProcessTask);
                                        client.DownloadFile(invoice.data.invoiceDocumentURL, ConfigurationManager.AppSettings["EventsforceInvoicePDFFilePath"] + missingInvoice + ".pdf");
                                    }
                                    else
                                    {
                                        ProcessTask.log("Credit card " + invoice.data.invoiceNumber + " has no payment");
                                    }
                                }
                                else
                                {
                                    ProcessTask.log("Credit card " + invoice.data.invoiceNumber + " has not been marked as printed");
                                }
                            }
                            else if (invoice.data.selectedPaymentMethod == "Invoice" || invoice.data.selectedPaymentMethod == "Cheque" || invoice.data.selectedPaymentMethod == "Bank Transfer")
                            {
                                if (invoice.data.markedAsPrinted)
                                {
                                    string peopleData = client.DownloadString(invoice.data.invoiceRecipientDetailsURL);
                                    People person = JsonConvert.DeserializeObject<People>(peopleData, settings);

                                    if (!string.IsNullOrEmpty(person.data.externalCustomerAccount))
                                    {
                                        string eventData = client.DownloadString(invoice.data.eventDetailsURL);
                                        Events ev = JsonConvert.DeserializeObject<Events>(eventData, settings);

                                        Helper.InsertInvoice(person, invoice, ev, null, connectionString, ProcessTask);
                                        client.DownloadFile(invoice.data.invoiceDocumentURL, ConfigurationManager.AppSettings["EventsforceInvoicePDFFilePath"] + missingInvoice + ".pdf");
                                    }
                                }
                                else
                                {
                                    ProcessTask.log("Invoice " + invoice.data.invoiceNumber + " has not been marked as printed");
                                }
                            }
                            else
                            {
                                ProcessTask.log(invoice.data.invoiceNumber + " has no payment method");
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessTask.log("Problem inserting invoice into Middleware " + ex.Message);
                        }
                    }

                    FireStoredProcedure("usp_UPD_EF_Incoming_Invoices");
                    FireStoredProcedure("usp_INS_InvoicesIn");
                    FireStoredProcedure("usp_INS_CreditCardOrdersIn");
                    FireStoredProcedure("usp_UPD_InvoicesIn");
                    FireStoredProcedure("usp_UPD_CreditCardsIn");
                }
                else
                {
                    ProcessTask.log("No invoices to process!", ProcessAccess.enmLevel.Standard);
                }
            }
            catch (Exception ex)
            {
                ProcessTask.log(ex.Message);
            }
            finally
            {
                CloseConnection();
            }

            return ProcessEnums.enmStatus.Successful;
        }

        #endregion

    }
}
