﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Data;
using Rebex.Net;
using UBM.Logger;

namespace UBM.MiddlewareOTIS.OTIS
{

    public class FTPWaitingFiles : ProcessTask 
    {
        //TODO: Read this from the directory settings.
        private string PROCESSED_DIRECTORY = @"";
        private string WAITING_DIRECTORY = @"";
        private string FTP_SERVER = @"";
        private string FTP_USER = @"";
        private string FTP_PASSWORD = @"";

        /// <summary>
        /// Sets up the Directory and FTP details for the transfer process.
        /// </summary>
        private void SetConfiguration()
        {
            string connection = MiddlewareDataAccess.Connections.GetConnectionString(this.SystemId);
            connection = this.RemoveProvider(connection);

            using (SqlConnection cn = new SqlConnection(connection))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 1 * FROM Configuration";
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        FTP_SERVER = dr["FtpServer"].ToString();
                        FTP_USER = dr["FtpUser"].ToString();
                        FTP_PASSWORD = dr["FtpPassword"].ToString();
                        WAITING_DIRECTORY = dr["WaitingFilePath"].ToString();
                        PROCESSED_DIRECTORY = dr["ProcessedFilePath"].ToString();
                    }
                }
            }

            if (!WAITING_DIRECTORY.EndsWith(@"\")) WAITING_DIRECTORY += @"\";
            if (!PROCESSED_DIRECTORY.EndsWith(@"\")) PROCESSED_DIRECTORY += @"\";

        }

        public override ProcessEnums.enmStatus Execute()
        {

            SetConfiguration();

            bool failures = false;  //-- sets this value if the package fails to FTP a file.

            //-- if this fails it will throw an exception up.

            DirectoryInfo di = new DirectoryInfo(WAITING_DIRECTORY);
            log("Waiting Directory: " + WAITING_DIRECTORY);
            FileInfo[] rgFiles = di.GetFiles("*.*");
            log(rgFiles.Count() + " to process");
            foreach (FileInfo fi in rgFiles)
            {
                log("Processing file " + fi);
                // ensure this is a valid name);
                if (fi.Name.IndexOf(".") > 1)
                {
                    string fileNameSP = fi.Name.Substring(0, fi.Name.IndexOf("."));
                    log("New file name: " + fileNameSP);

                    // set the parameters of the SP
                    SqlParameter[] parms = new SqlParameter[1];
                    parms[0] = new SqlParameter("FileName", fileNameSP);

                    //--
                    log("About to SFTP file " + fi);
                    if (SFTPTransferFile(fi))
                    {
                        //-- store the valid vat number
                        GenericCommands.ExecuteScalarStoredProcedure(parms,
                            Connections.GetConnectionString("OTISControl"), "usp_UPD_SentFile");

                        if (File.Exists(PROCESSED_DIRECTORY + fi.Name))
                        {
                            File.Delete(PROCESSED_DIRECTORY + fi.Name);
                        }
                        
                        File.Move(fi.FullName, PROCESSED_DIRECTORY + fi.Name);

                    }
                    else
                    {
                        //-- Does not move files anymore. There's no point, as the process
                        //-- can just pick them up easily from the waiting directory
                        //File.Move(fi.FullName, FAILED_DIRECTORY + fi.Name);
                        failures = true;
                    }
                }
            }

            if (failures)
                throw new Exception("Failed to transfer / move at least one file during transport. Check batch log for errors");

            return ProcessEnums.enmStatus.Successful;
        }

        private bool SFTPTransferFile(FileInfo fileInf)
        {
            //-- .NET does not support natibely SFTP - so there is a third party product rebex.com SFTP which will
            //-- do the task for us. (Installed in the GAC).
            bool success = true;
            try
            {
                Sftp client = new Sftp();
                client.Connect(FTP_SERVER);
                log("Server: " + FTP_SERVER); 
                client.Login(FTP_USER, FTP_PASSWORD);
                log("User: " + FTP_USER);
                log("Password: " + FTP_PASSWORD);

                // upload the 'test.zip' file to the current directory at the server 
                log("Source path: " + fileInf.FullName);
                log("Remote path: " + "/ubmi_ftp/" + fileInf.Name);
                client.PutFile(fileInf.FullName, "/ubmi_ftp/" + fileInf.Name);
                log("Successfully moved file");
                client.Disconnect();
                log("Disconnecting");

            }
            catch (Exception ex)
            {
                log(ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                success = false;
            }
            return success;

        }

        /// <summary>
        /// This routine looks in the processed folder and marks any file that is in there as sent 
        /// </summary>
        private bool FTPTransferFile(FileInfo fileInf)
        {
            bool success = false;

            //    Code ripped off from...
            //    http://www.codeguru.com/csharp/csharp/cs_internet/desktopapplications/article.php/c13163

            string uri = "ftp://" + FTP_SERVER + "/ubmi_ftp/" + fileInf.Name;
            FtpWebRequest reqFTP;

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create
                     (new Uri("ftp://" + FTP_SERVER + "/ubmi_ftp/" + fileInf.Name));

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(FTP_USER, FTP_PASSWORD);

            // By default KeepAlive is true, where the control connection
            // is not closed after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fileInf.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file
            // to be uploaded
            FileStream fs = fileInf.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload
                    // Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();

                success = true;

            }
            catch (Exception ex)
            {
                //-- FTP failued.
                log(ex.Message.ToString(), ProcessAccess.enmLevel.Critical);
                success = false;
            }
            return success;
        }
    }

}
