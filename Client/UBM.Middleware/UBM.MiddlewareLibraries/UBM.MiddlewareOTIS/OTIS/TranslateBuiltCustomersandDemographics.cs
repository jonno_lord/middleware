﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareOTIS.OTIS
{
    public class TranslateBuiltCustomersandDemographics : ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("OTIS Translate Built Customers and Demographics.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
