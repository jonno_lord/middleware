﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareOTIS.OTIS
{
    public class AssignBatchNumber : ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("[usp_SET_BatchNumber]");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
