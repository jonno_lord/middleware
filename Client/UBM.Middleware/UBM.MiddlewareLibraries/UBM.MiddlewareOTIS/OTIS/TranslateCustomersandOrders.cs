﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareOTIS.OTIS
{
    public class TranslateCustomersandOrders : ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("OTIS Translate Customers and Orders.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
