﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareOTIS.OTIS
{

    public class TranslateCustomersviaProduct : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up
            //this.FireDTSPackage("OTIS Translate Customers and Orders via Product.dtsx");
            this.FireDTSPackage("OTIS Extract ESOP Customers and Orders via Product.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
