﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareOTIS.ReferenceData
{
    public class DataRefresh : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.ASOPConnection = "ASOPUK";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("OTIS Reference Data Refresh.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
