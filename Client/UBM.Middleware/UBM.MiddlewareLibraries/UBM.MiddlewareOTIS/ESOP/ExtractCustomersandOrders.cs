﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareOTIS.ESOP
{
    public class ExtractCustomersandOrders : ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("OTIS Extract ESOP Customers and Orders.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
