﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Dts.Runtime;
using UBM.MiddlewareLogging;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareSystem;
using System.Configuration;
using System.Collections;

namespace UBM.MiddlewareSSIS
{

    public class DTSPackage : UBM.SSISExecutor.DTSPackage
    {

        #region Private properties
        private int Processid { get; set; }
        private int Jobid { get; set; }
        private int Batchid { get; set; }
        #endregion


        // Simple enumerators used to display the outcome of the package execution
        public enum DTSPackageExecution
        {
            Success,
            Failed
        }

        public string PaywareTable
        {
            get;
            set;
        }

        public string CARSConnectionString
        {
            get;
            set;
        }

        public string ASOPConnection
        {
            get;
            set;
        }

        public string MiddlewareASOPConnection
        {
            get;
            set;
        }

        public string MiddlewareSourceConnection
        {
            get; set;
        }

        public string FileConnection
        {
            get;
            set;
        }


        private bool isCommon
        {
            get;
            set;
        }

        // Wrapper over the logging function, used for logging activity when DTS pacakage is
        // being executed
        private void log(string msg, ProcessAccess.enmLevel level = ProcessAccess.enmLevel.Standard)
        {
            UBM.MiddlewareLogging.Logger.BatchLog(this.Jobid,
                                                this.Processid, this.Batchid,
                                                level, msg);
        }

        /// <summary>
        /// Creates an instance of the DTSPackage object, with the parameters required
        /// to store its progress to BatchLogs. Inherits from the SSISDTSPackage class defined
        /// in the UBM.SSISExecutor class, and calls its base method which instantiates a local
        /// version of the DTSPackage object defined in the SQLDtsRuntime .net library
        /// </summary>
        /// <param name="processid">the ID of the proces being execited</param>
        /// <param name="batchid">the batch number of the job</param>
        /// <param name="jobid">the id of the job executed</param>
        public DTSPackage(int processid, int batchid, int jobid, bool isCommon):base()
        {
            this.Processid = processid;
            this.Batchid = batchid;
            this.Jobid = jobid;
            this.isCommon = isCommon;
        }

        /// <summary>
        /// Executes a DTSPackage. It will log activity through raised events in a listener
        /// object inside the UBM.MiddlewareSSIS library. logging is performed against the
        /// job, process and batch number supplied at instantiation. 
        /// </summary>
        /// <param name="packageName">the Absolute file name of the package to execute</param>
        /// <returns>an enumerator of Success or Failure</returns>
        public DTSPackageExecution ExecutePackage(string packageName)
        {
            //TODO: Set Connection strings...

            if (!(packageName.Contains(@"\\") || packageName.Contains(@":")))
            {

                // build up the system Name
                try
                {
                    log("building package name details");
                    //-- build the name up from the Package Root and the System's shortName
                    string root = ConfigurationManager.AppSettings["PackageRootPath"];
                    job DTSjob = SystemAccess.getJob(this.Jobid);
                    system DTSsystem = UBM.MiddlewareDataAccess.SystemAccess.getSystem(DTSjob.Systemid);

                    //-- this is a feature - where all instances of ASOP actually use a central MiddlewareASOP
                    //-- directory, rather than a MiddlewareASOPDaltons, MiddlewareASOPHolland etc..
                    string shortName = DTSsystem.ShortName.Replace(" ", "");
                    if (shortName.ToUpper().Contains("ASOP")) shortName = "MiddlewareASOP";

                    //-- common packages do not sit off of the system name, but sit in a Common directory.
                    if(this.isCommon)
                        packageName = root + "Common" + @"\" + packageName;
                    else
                        packageName = root + shortName + @"\" + packageName;
                    log("Package location is :" + packageName);
                }
                catch (Exception ex)
                {
                    log("Failed to set the package name.", ProcessAccess.enmLevel.Critical);
                    MiddlewareLogging.Logger.ErrorLog(ex);
                    throw ex;
                }
            }

            //-- try and load the package, if this fails, capture the error details
            try
            {
                this.LoadPackage(packageName);
            }
            catch (Exception ex)
            {
                MiddlewareLogging.Logger.BatchLog(this.Jobid, this.Processid, this.Batchid, ProcessAccess.enmLevel.Critical, "File could not be loaded.." + packageName);
                MiddlewareLogging.Logger.ErrorLog(ex);
            }


            System.Collections.Hashtable ht = new Hashtable();
            for (int i = 0; i < ConfigurationManager.ConnectionStrings.Count; i++)
            {
                string connectionString = ConfigurationManager.ConnectionStrings[i].ConnectionString;
                string connectionName = ConfigurationManager.ConnectionStrings[i].Name.ToString();
                connectionName = connectionName.Replace("ConnectionString", "");
                ht.Add(connectionName, connectionString);
            }

            if (this.PaywareTable != null)
            {
                this.pkg.Variables["User::PaywareTable"].Value = this.PaywareTable;
            }

            if (this.CARSConnectionString != null)
            {
                this.pkg.Variables["User::CARSConnectionString"].Value = this.CARSConnectionString;
            }

            //-- if we have specified a Middleware source (for generic packages, includes its reference here...
            if (MiddlewareSourceConnection != null)
            {
                ht.Add("MiddlewareSource", ConfigurationManager.ConnectionStrings[this.MiddlewareSourceConnection +"ConnectionString"].ToString());
            }

            if (MiddlewareASOPConnection != null)
            {
                ht.Add("MiddlewareASOP", ConfigurationManager.ConnectionStrings[this.MiddlewareASOPConnection + "ConnectionString"].ToString());
            }

            if (ASOPConnection != null)
            {
                ht.Add("ASOP", ConfigurationManager.ConnectionStrings[this.ASOPConnection + "ConnectionString"].ToString());
            }

            if (FileConnection != null)
            {
                //ht.Add("FileConnection", this.FileConnection);
                this.pkg.Variables["User::MSIFile"].Value = FileConnection;
            }

            this.SetPackageConnections(ht, jobid:this.Jobid, processid:this.Processid,batchid:this.Batchid);


            //-- execute the package, using a listener object to record its activity and progress
            //TODO: put in WCF notification here.
            if (this.PackageStatus == DTSPackage.DTSPackageStatus.Loaded)
            {

                UBM.MiddlewareSSIS.SSISEventListener Listener = new UBM.MiddlewareSSIS.SSISEventListener();
                Listener.Batchid = this.Batchid;
                Listener.Jobid = this.Jobid;
                Listener.Processid = this.Processid;
                this.Execute(Listener);

                // return if the execution worked ok.
                if (this.pkg.Errors.Count > 0)
                    return DTSPackageExecution.Failed;
                else
                    return DTSPackageExecution.Success;

            }
            else
            {
                log("Failed to load package", ProcessAccess.enmLevel.Important);
                return DTSPackageExecution.Failed;
            }
        }
    }
}
