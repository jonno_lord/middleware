﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Dts.Runtime;
using UBM.MiddlewareLogging;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareSSIS
{
    public class SSISEventListener : UBM.SSISExecutor.SSISEventListener 
    {

        /// <summary>
        /// Public members are defined here, as when the listener is instantiated, these
        /// details are then set. 
        /// TODO: move the assignment of these public properties into the class 
        /// instantiation.
        /// </summary>
        #region public Members
        public int Processid { get; set; }
        public int Jobid { get; set; }
        public int Batchid { get; set; }
        #endregion

        /// <summary>
        /// Rasied when an error occurs from the DTS package - 
        /// Will log the details of the failure to Middleware.
        /// </summary>
        /// <param name="source">Where the error was raised from</param>
        /// <param name="errorCode">The error code</param>
        /// <param name="subComponent">Sub component details</param>
        /// <param name="description">The description of the error</param>
        /// <param name="helpFile">details of the help file that can be used for this fault</param>
        /// <param name="helpContext">The context of the error</param>
        /// <param name="idofInterfaceWithError">The id of the interface</param>
        /// <returns></returns>
        public override bool OnError(Microsoft.SqlServer.Dts.Runtime.DtsObject source, int errorCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            UBM.MiddlewareLogging.Logger.BatchLog(this.Jobid,
                                                  this.Processid, 
                                                  this.Batchid,
                                                  ProcessAccess.enmLevel.Critical, 
                                                  source +"," + subComponent +"," + description);
            return base.OnError(source, errorCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError);
        }


        /// <summary>
        /// Raised when an information event occurs in the DTS package.
        /// Will log this information into the middleware system
        /// </summary>
        /// <param name="source">the source of the information</param>
        /// <param name="informationCode">information code</param>
        /// <param name="subComponent">the sub component</param>
        /// <param name="description">the description of the information event</param>
        /// <param name="helpFile">the help file associated</param>
        /// <param name="helpContext">help file context</param>
        /// <param name="idofInterfaceWithError">The interface id of the information</param>
        /// <param name="fireAgain">if this task should fire again.</param>
        public override void OnInformation(DtsObject source, int informationCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError, ref bool fireAgain)
        {

            UBM.MiddlewareLogging.Logger.BatchLog(this.Jobid,
                                        this.Processid,
                                        this.Batchid,
                                        ProcessAccess.enmLevel.Standard, 
                                        source + "," + subComponent + "," + description);
            base.OnInformation(source, informationCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError, ref fireAgain);
        }


        /// <summary>
        /// Raised when a warning occurs in the DTS package.
        /// will log this information into middleware
        /// </summary>
        /// <param name="source">the source of the warning</param>
        /// <param name="warningCode">the warning code</param>
        /// <param name="subComponent">the subcomponent this warning realtes to</param>
        /// <param name="description">the description of the warning</param>
        /// <param name="helpFile">help file</param>
        /// <param name="helpContext">help context</param>
        /// <param name="idofInterfaceWithError">id of the warning</param>
        public override void OnWarning(DtsObject source, int warningCode, string subComponent, string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            UBM.MiddlewareLogging.Logger.BatchLog(this.Jobid,
                            this.Processid,
                            this.Batchid,
                            ProcessAccess.enmLevel.Serious,
                            source + "," + subComponent + "," + description);
            base.OnWarning(source, warningCode, subComponent, description, helpFile, helpContext, idofInterfaceWithError);
        }


    }
}
