﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareASOPUK.Orders
{
    public class ImmediateOrders:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareASOPConnection = "MiddlewareASOPUK";
            this.ASOPConnection = "ASOPUK";
            
            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Immediate Orders Source to Middleware.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
