﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPUK.AR
{
    public class SourceToMiddleware : ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {

            this.MiddlewareASOPConnection = "MiddlewareASOPUK";
            this.ASOPConnection = "ASOPUK";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Accounts Receivable Source to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;

        }
    }
}
