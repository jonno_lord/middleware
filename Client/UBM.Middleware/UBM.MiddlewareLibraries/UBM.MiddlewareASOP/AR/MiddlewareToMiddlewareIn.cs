﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareASOPUK.Receipts;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Text.RegularExpressions;

namespace UBM.MiddlewareASOPUK.AR
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {

        private string serverName;
        private string databaseName;
        private string userName;
        private string password;

        private void setProperties(string ConnectionString)
        {
            databaseName = Regex.Match(ConnectionString, "(Initial Catalog=|Database=){1}([\\w-]+)[;]?").Groups[2].Value;
            serverName = Regex.Match(ConnectionString, "(Data Source=){1}([\\w-]+)[;]?").Groups[2].Value;
            userName = Regex.Match(ConnectionString, "(User ID=){1}([\\w-]+)[;]?").Groups[2].Value;
            password = Regex.Match(ConnectionString, "(Password=){1}([\\w-]+)[;]?").Groups[2].Value;
        }

        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_AccountsReceivableToJDEFormat");
            log("Retrieving Connectionstring for system id " + this.SystemId.ToString());
            string connectionString = MiddlewareDataAccess.Connections.GetConnectionString(this.SystemId);

            //-- retrieve batches to process
            log("Retrieving batches to process");
            DataSet batches = InvoiceAccess.GetReceiptBatches(connectionString);
            log("Found " + batches.Tables[0].Rows.Count.ToString() + " batches");

            setProperties(connectionString);

            foreach (DataRow batch in batches.Tables[0].Rows)
            {
                
                string batchNumber = batch["BATCH_NUMBER"].ToString();
                log("Processing batch " + batchNumber);
                string outputPath = ConfigurationManager.AppSettings["ReceiptsPath"].ToString().Trim();
                log("Determining output path " + batchNumber);

                ConnectionInfo conInfo = new ConnectionInfo();
                conInfo.ServerName = this.serverName;
                conInfo.DatabaseName = this.databaseName;
                conInfo.UserID = this.userName;
                conInfo.Password = this.password;

                string rptFileLocation = this.Server.MapPath("~/ReceiptTemplates/");

                PdfGenerator.CreateBatchReceipts(int.Parse(batchNumber), "ASOPUK", outputPath, conInfo, rptFileLocation);
            }

            return ProcessEnums.enmStatus.Successful;
        }

    }

}
