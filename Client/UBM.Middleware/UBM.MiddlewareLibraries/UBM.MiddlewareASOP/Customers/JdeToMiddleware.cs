﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPUK.Customers
{
    public class JdeToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- source connection needs to be defined for a generic (common) package
            this.MiddlewareSourceConnection = "MiddlewareASOPUK";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("COMMON Customers JDE to Middleware.dtsx", true);

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
