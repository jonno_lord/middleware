﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Data;
using UBM.MiddlewareScrutiny;
using System.Web;
using UBM.MiddlewareLogging;
using UBM.MiddlewareSyncRefresher;


namespace UBM.MiddlewareASOPUK.Customers
{
    public class MiddlewareScrutiny:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {

            // calculate the scrutiny for customers in ASOP
            DataSet customers = CustomerAccess.GetScrutinyCustomers(Common.MiddlewareASOPConnection(this.SystemId));
            
            foreach (DataRow customer in customers.Tables[0].Rows)
            {
                List<Exception> errs = null;

                DateTime calcStart = DateTime.Now;
                string companyName = customer["id"].ToString();

                try
                {
                    if (Rules.Calculate(this.SystemId, int.Parse(customer["id"].ToString()),
                                        out errs, this.RootPath + @"\bin", this.Batchid,
                                        this.Jobid, this.Processid) == Rules.SCRUTINY_FAILED)
                        throw new Exception("Errors occurred calculating Scrutiny");

                    TimeSpan duration = DateTime.Now.Subtract(calcStart);
                    BatchLog("Processed " + companyName + " in " + duration.Milliseconds.ToString() + " ms ( " + duration.Seconds.ToString() +"s)");

                }
                catch (Exception ex)
                {
                    Logger.ErrorLog(ex, companyName);
                }

            }


            // Refresh Synchronisation service call.
            try
            {
                // call the update, if data exists (in dedupe)
                if (customers.Tables[0].Rows.Count > 0)
                {
                    SyncRefresher.RefreshClients();
                }
            }
            catch (Exception ex)
            {
                // record failure
                Logger.ErrorLog(ex);
                BatchLog("Failed to call synchronisation service : " + ex.Message.ToString());
            }

            return ProcessEnums.enmStatus.Successful;
        }

        private void BatchLog(string description)
        {
            Logger.BatchLog(this.Jobid, this.Processid, this.Batchid, ProcessAccess.enmLevel.Standard, description);
        }

    }

}
