﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareASOPUK.Customers
{
    public class MiddlewareToMiddlewareIn : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {

            //-- if this fails it will throw an exception up.
            this.FireStoredProcedure("usp_TRA_CustomersToJDEFormat",
                    "SELECT TOP 10 * FROM Customers WHERE Stage = 1 AND Accepted = 1",
                    Connections.GetConnectionString("MiddlewareASOPUK"));

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
