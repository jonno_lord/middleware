﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPUK.Notes
{
    public class MiddlewareToSource:ProcessTask 
    {
        public override ProcessEnums.enmStatus Execute()
        {
            //-- set the version of ASOP we are talking to.
            this.MiddlewareASOPConnection = "MiddlewareASOPUK";
            this.ASOPConnection = "ASOPUK";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Credit Controller Notes Middleware to Source.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }

}
