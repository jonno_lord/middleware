﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareProcess;

namespace UBM.MiddlewareASOPUK.ReferenceData
{
    public class DataRefresh : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            this.ASOPConnection = "ASOPUK";
            this.MiddlewareASOPConnection = "MiddlewareAsopUK";

            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ASOP Reference Data.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}
