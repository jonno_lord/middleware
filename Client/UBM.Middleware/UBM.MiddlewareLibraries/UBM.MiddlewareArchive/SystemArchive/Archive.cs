﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Management;
using System.Threading;
using System.Configuration;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;

namespace UBM.MiddlewareArchive.SystemArchive
{
    public class Archive : UBM.MiddlewareProcess.ProcessTask
    {

        public override ProcessEnums.enmStatus Execute()
        {
            DeleteLogs();

            return ProcessEnums.enmStatus.Successful;
        }

        private void DeleteLogs()
        {
            //removes batch logs that are citical after a month and non critical after 2 weeks
            this.FireStoredProcedure("usp_UPD_BatchLogs");
            //removes error logs that are a month old
            this.FireStoredProcedure("usp_UPD_ErrorLogs");

        }
    }
}
