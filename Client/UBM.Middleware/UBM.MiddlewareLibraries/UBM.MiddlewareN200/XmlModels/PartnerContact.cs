﻿using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    public class PartnerContact
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }
}
