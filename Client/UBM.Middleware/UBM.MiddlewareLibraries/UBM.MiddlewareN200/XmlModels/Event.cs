﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("event")]
    public class Event
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlAttribute("name")]
        public string EventName { get; set; }

        [XmlArray("partner-settings")]
        public List<PartnerSetting> PartnerSettings { get; set; }
    }
}
