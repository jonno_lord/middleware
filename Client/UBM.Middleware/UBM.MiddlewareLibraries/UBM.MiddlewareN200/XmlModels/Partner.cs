﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("partner")]
    public class Partner
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlElement("stand-number")]
        public string StandNumber { get; set; }

        [XmlElement("stand-size")]
        public string StandSize { get; set; }

        [XmlElement("barcodescanner-count")]
        public string BarcodeScanner { get; set; }

        [XmlElement("scanapp-count")]
        public string MobileApp { get; set; }

        [XmlElement("event")]
        public Event Event { get; set; }

        [XmlElement("registration-type")]
        public RegistrationType RegistrationType { get; set; }

        [XmlElement("contact")]
        public PartnerContact PartnerContact { get; set; }

        //[XmlElement("type")]
        //public string Type { get; set; }

        [XmlArray("personnel-types")]
        public List<Personnel> PersonnelTypes { get; set; }
        
    }
}
