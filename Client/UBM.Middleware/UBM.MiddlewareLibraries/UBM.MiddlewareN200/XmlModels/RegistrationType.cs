﻿using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("registration-type")]
    public class RegistrationType
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("type")]
        public string Type { get; set; }
    }
}

