﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [CollectionDataContract]
    [XmlType("address")]
    public class Address
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [DataMember(Name = "address")]
        [XmlElement("address")]
        public string Name { get; set; }

        [DataMember(Name = "house-number")]
        [XmlElement("house-number")]
        public int HouseNumber { get; set; }

        [DataMember(Name = "house-number-suffix")]
        [XmlElement("house-number-suffix")]
        public string HouseNumberSuffix { get; set; }

        [DataMember(Name = "city")]
        [XmlElement("city")]
        public string City { get; set; }

        [DataMember(Name = "country")]
        [XmlElement("country")]
        public string Country { get; set; }

        [DataMember(Name = "postal-code")]
        [XmlElement("postal-code")]
        public string PostCode { get; set; }

        [DataMember(Name = "state")]
        [XmlElement("state")]
        public string State { get; set; }
    }
}
