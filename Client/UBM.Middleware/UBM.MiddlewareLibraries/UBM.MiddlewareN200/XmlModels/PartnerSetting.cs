﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace UBM.MiddlewareN200.XmlModels
{
    [CollectionDataContract]
    [XmlType("partner-setting")]
    public class PartnerSetting
    {
        [XmlElement("registration-type")]
        public RegistrationType RegistrationType { get; set; }

        [XmlElement("personnel")]
        public List<Personnel> Personnel { get; set; }
    }
}