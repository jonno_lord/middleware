﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [CollectionDataContract]
    [XmlType("personnel")]
    public class Personnel
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlElement("free-badges")]
        public string FreeBadges { get; set; }
    }
}

