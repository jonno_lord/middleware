﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("error")]
    public class Error
    {
        [XmlAttribute("description")]
        public string Description { get; set; }

        [XmlAttribute("additional")]
        public string Additional { get; set; }

        [XmlElement("errorCode")]
        public string ErrorCode { get; set; }
    }
}
