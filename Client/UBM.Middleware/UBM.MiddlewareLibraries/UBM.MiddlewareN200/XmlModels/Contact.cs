﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Collections;
using UBM.MiddlewareN200.XmlModels;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("contact")]
    public class Contact
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlElement("type")]
        public string Type { get; set; }

        [XmlElement("reference")]
        public string Reference { get; set; }

        [XmlElement("gender")]
        public string Gender { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("username")]
        public string Username { get; set; }

        [XmlElement("password")]
        public string Password { get; set; }

        [XmlElement("first-name")]
        public string FirstName { get; set; }

        [XmlElement("last-name")]
        public string LastName { get; set; }

        [XmlElement("date-of-birth")]
        public string DateOfBirth { get; set; }

        [XmlElement("company-name")]
        public string ExhibitingName { get; set; }

        [XmlElement("phone1")]
        public string Telephone { get; set; }

        [XmlElement("email")]
        public string Email { get; set; }

        [XmlElement("company-website")]
        public string Website { get; set; }

        [XmlElement("job-function")]
        public string JobTitle { get; set; }

        [XmlArray("addresses")]
        public List<Address> Addresses { get; set; }

        public Contact()
        { 
        }

        public Contact(Models.Contact contact, List<Address> addresses)
        {
            Type = "partner";
            ExhibitingName = contact.ExhibitingName;
            Title = contact.Title;
            FirstName = contact.FirstName;
            LastName = contact.LastName;
            JobTitle = contact.JobTitle;
            Email = contact.Email;
            Username = contact.Username;
            Password = contact.Password;
            Website = contact.Website;
            Addresses = addresses;
            Reference = contact.ExhibitorId;
            DateOfBirth = contact.DateOfBirth;
        }
    }
}
