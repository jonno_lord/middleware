﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("errors")]
    public class Errors
    {
        [XmlElement("error")]
        public Error Error { get; set; }
    }
}
