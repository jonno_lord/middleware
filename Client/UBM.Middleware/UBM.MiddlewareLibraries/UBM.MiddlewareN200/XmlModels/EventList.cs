﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("events")]
    public class EventList
    {
        [XmlAttribute("more")]
        public string More { get; set; }

        [XmlElement("event")]
        public List<Event> Items { get; set; }
    }
}