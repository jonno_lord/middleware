﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace UBM.MiddlewareN200.XmlModels
{
    [XmlRoot("registration-types")]
    public class RegistrationTypeList
    {
        [XmlAttribute("more")]
        public string More { get; set; }

        [XmlElement("registration-type")]
        public List<RegistrationType> Items { get; set; }
    }
}