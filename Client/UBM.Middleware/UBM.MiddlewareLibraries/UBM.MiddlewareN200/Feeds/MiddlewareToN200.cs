﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareN200.Common;
using UBM.MiddlewareN200.Models;
using UBM.MiddlewareN200.XmlModels;
using UBM.MiddlewareProcess;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace UBM.MiddlewareN200.Feeds
{
    public class MiddlewareToN200 : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            
                log("Retrieving connection string for system id " + SystemId);

                // CONTACTS
                // Insert
                string connectionString = Connections.GetConnectionString(SystemId);
                DataSet contacts = CustomerAccess.GetContactInsertsForN200(connectionString);

                log("Found " + contacts.Tables[0].Rows.Count + " Contacts to INSERT");

                foreach (DataRow contact in contacts.Tables[0].Rows)
                {

                    using (WebClient client = CommonTasks.CreateWebClient())
                    {

                    Models.Contact contactModel = new Models.Contact
                                                      {
                                                          ExhibitingName = contact["ExhibitingName"].ToString(),
                                                          Title = contact["Title"].ToString(),
                                                          FirstName = contact["FirstName"].ToString(),
                                                          LastName = contact["LastName"].ToString(),
                                                          JobTitle = contact["JobTitle"].ToString(),
                                                          Username = contact["Username"].ToString(),
                                                          Password = contact["Password"].ToString(),
                                                          Website = contact["Website"].ToString(),
                                                          ExhibitorId = contact["ExhibitorId"].ToString(),
                                                          Email = contact["Email"].ToString(),
                                                          Type = contact["Type"].ToString(),
                                                          DateOfBirth = string.Empty
                                                      };

                    XmlModels.Address addressModel = new XmlModels.Address
                                                        {
                                                            Type = "postal",
                                                            Name = contact["Address"].ToString(),
                                                            City = contact["City"].ToString(),
                                                            Country = contact["Country"].ToString(),
                                                            PostCode = contact["PostCode"].ToString(),
                                                            State = contact["County"].ToString()
                                                        };
                                                            
                    XmlModels.Contact c = CommonTasks.CreateContact(contactModel,addressModel);

                    try
                    {
                        string uri = CommonTasks.GetURI("contacts");
                        var stream = new MemoryStream();
                        var serializer = new XmlSerializer(c.GetType());
                        var ns = CommonTasks.CreateXMLNamespace();
                        serializer.Serialize(stream, c, ns);
                        
                        WriteStreamToLog(stream);

                        byte[] data = client.UploadData(uri, "POST", stream.ToArray());

                        stream = new MemoryStream(data);

                        var reader = new StreamReader(stream);
                        XmlModels.Contact responseContact = (XmlModels.Contact) serializer.Deserialize(reader);

                        connectionString = Connections.GetConnectionString("N200");

                        SqlParameter[] parms = new SqlParameter[3];
                        parms[0] = new SqlParameter("ContactCode", responseContact.Code);
                        parms[1] = new SqlParameter("ExhibitorId", contactModel.ExhibitorId);
                        parms[2] = new SqlParameter("Type", contactModel.Type);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_UPD_ContactCode");
                    }
                    catch (WebException we)
                    {
                        Errors e = new Errors();
                        var serializer = new XmlSerializer(e.GetType());
                        var resp = new StreamReader(we.Response.GetResponseStream());
                        Errors responseError = (XmlModels.Errors)serializer.Deserialize(resp);

                        var parms = new SqlParameter[3];
                        parms[0] = new SqlParameter("ExhibitorId", contactModel.ExhibitorId);
                        parms[1] = new SqlParameter("Type", contactModel.Type);
                        parms[2] = new SqlParameter("Error", responseError.Error.Additional == null ? responseError.Error.Description : responseError.Error.Description + ": " + responseError.Error.Additional);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_UPD_LogError");
                    }
                    catch (Exception ex)
                    {
                        log(ex.Message);
                    }
                    }
                }

                // PARTNERS
                // Insert
                connectionString = Connections.GetConnectionString(SystemId);
                DataSet partners = CustomerAccess.GetPartnerInsertsForN200(connectionString);

                log("Found " + partners.Tables[0].Rows.Count + " Partners to INSERT");

                foreach (DataRow partner in partners.Tables[0].Rows)
                {
                    using (WebClient client = CommonTasks.CreateWebClient())
                    {

                    Models.Partner partnerModel = new Models.Partner
                                                      {
                                                          N200EventCode = partner["N200EventCode"].ToString(),
                                                          RegistrationType = partner["RegistrationType"].ToString(),
                                                          ContactCode = partner["ContactCode"].ToString(),
                                                          StandNumber = partner["StandNumber"].ToString(),
                                                          StandSize = partner["StandArea"].ToString(),
                                                          PersonnelType = partner["PersonnelType"].ToString(),
                                                          NumberOfBadges = partner["NumberOfBadges"].ToString(),
                                                          ESOPEventCode = partner["ESOPEventCode"].ToString(),
                                                          ExhibitorId = partner["ExhibitorId"].ToString(),
                                                          Type = partner["Type"].ToString(),
                                                          BarcodeScanner = partner["BarcodeScanner"].ToString(),
                                                          MobileApp = partner["MobileApp"].ToString()
                                                      };

                    XmlModels.Partner p = CommonTasks.CreatePartner(partnerModel);

                    try
                    {
                        string uri = CommonTasks.GetURI("partners");
                        var stream = new MemoryStream();
                        var serializer = new XmlSerializer(p.GetType());
                        var ns = CommonTasks.CreateXMLNamespace();
                        serializer.Serialize(stream, p, ns);

                        WriteStreamToLog(stream);

                        byte[] data = client.UploadData(uri, "POST", stream.ToArray());

                        stream = new MemoryStream(data);

                        var reader = new StreamReader(stream);
                        XmlModels.Partner responsePartner = (XmlModels.Partner) serializer.Deserialize(reader);

                        connectionString = Connections.GetConnectionString("N200");

                        var parms = new SqlParameter[4];
                        parms[0] = new SqlParameter("PartnerCode", responsePartner.Code);
                        parms[1] = new SqlParameter("ESOPEventCode", partnerModel.ESOPEventCode);
                        parms[2] = new SqlParameter("ExhibitorId", partnerModel.ExhibitorId);
                        parms[3] = new SqlParameter("Type", partnerModel.Type);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_UPD_PartnerCode");
                    }
                    catch (WebException we)
                    {
                        Errors e = new Errors();
                        var serializer = new XmlSerializer(e.GetType());
                        var resp = new StreamReader(we.Response.GetResponseStream());
                        Errors responseError = (XmlModels.Errors)serializer.Deserialize(resp);

                        var parms = new SqlParameter[4];
                        parms[0] = new SqlParameter("ExhibitorId", partnerModel.ExhibitorId);
                        parms[1] = new SqlParameter("N200EventCode", partnerModel.N200EventCode);
                        parms[2] = new SqlParameter("Type", partnerModel.Type);
                        parms[3] = new SqlParameter("Error", responseError.Error.Additional == null ? responseError.Error.Description : responseError.Error.Description + ": " + responseError.Error.Additional);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_UPD_LogError");
                    }
                    catch (Exception ex)
                    {
                        log(ex.Message);
                    }
                }
                }

                // PARTNER AND CONTACT
                // Updates
                connectionString = Connections.GetConnectionString(SystemId);
                DataSet updates = CustomerAccess.GetUpdatesForN200(connectionString);

                log("Found " + updates.Tables[0].Rows.Count + " Contacts to UPDATE");

                foreach (DataRow update in updates.Tables[0].Rows)
                {
                    using (WebClient client = CommonTasks.CreateWebClient())
                    {

                    Models.Partner partnerModel = new Models.Partner
                                                      {
                                                          N200EventCode = update["N200EventCode"].ToString(),
                                                          RegistrationType = update["RegistrationType"].ToString(),
                                                          ContactCode = update["ContactCode"].ToString(),
                                                          StandNumber = update["StandNumber"].ToString(),
                                                          StandSize = update["StandArea"].ToString(),
                                                          PersonnelType = update["PersonnelType"].ToString(),
                                                          NumberOfBadges = update["NumberOfBadges"].ToString(),
                                                          ExhibitorId = update["ExhibitorId"].ToString(),
                                                          PartnerCode = update["PartnerCode"].ToString(),
                                                          BarcodeScanner = update["BarcodeScanner"].ToString(),
                                                          MobileApp = update["MobileApp"].ToString()
                                                      };

                    Models.Contact contactModel = new Models.Contact
                    {
                        ExhibitingName = update["ExhibitingName"].ToString(),
                        Title = update["Title"].ToString(),
                        FirstName = update["FirstName"].ToString(),
                        LastName = update["LastName"].ToString(),
                        JobTitle = update["JobTitle"].ToString(),
                        Username = update["Username"].ToString(),
                        Password = update["Password"].ToString(),
                        Website = update["Website"].ToString(),
                        ExhibitorId = update["ExhibitorId"].ToString(),
                        Email = update["Email"].ToString(),
                        Type = update["Type"].ToString(),
                        DateOfBirth = string.Empty
                    };

                    XmlModels.Address addressModel = new XmlModels.Address
                    {
                        Type = "postal",
                        Name = update["Address"].ToString(),
                        City = update["City"].ToString(),
                        Country = update["Country"].ToString(),
                        PostCode = update["PostCode"].ToString(),
                        State = update["County"].ToString()
                    };

                    XmlModels.Contact c = CommonTasks.CreateContact(contactModel, addressModel);

                    XmlModels.Partner p = CommonTasks.CreatePartner(partnerModel);

                    try
                    {
                        string uri = CommonTasks.GetURI("contacts");
                        var stream = new MemoryStream();
                        var serializer = new XmlSerializer(c.GetType());
                        var ns = CommonTasks.CreateXMLNamespace();
                        serializer.Serialize(stream, c, ns);

                        WriteStreamToLog(stream);

                        byte[] data = client.UploadData(uri + '/' + partnerModel.ContactCode, "PUT", stream.ToArray());

                        uri = CommonTasks.GetURI("partners");
                        stream = new MemoryStream();
                        serializer = new XmlSerializer(p.GetType());
                        ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        serializer.Serialize(stream, p, ns);

                        WriteStreamToLog(stream);

                        client.UploadData(uri + '/' + partnerModel.PartnerCode, "PUT", stream.ToArray());

                        var parms = new SqlParameter[2];
                        parms[0] = new SqlParameter("ContactCode", partnerModel.ContactCode);
                        parms[1] = new SqlParameter("PartnerCode", partnerModel.PartnerCode);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_UPD_ResetUpdates");
                    }
                    catch (WebException we)
                    {
                        Errors e = new Errors();
                        var serializer = new XmlSerializer(e.GetType());
                        var resp = new StreamReader(we.Response.GetResponseStream());
                        Errors responseError = (XmlModels.Errors)serializer.Deserialize(resp);

                        var parms = new SqlParameter[4];
                        parms[0] = new SqlParameter("ExhibitorId", contactModel.ExhibitorId);
                        parms[1] = new SqlParameter("N200EventCode", partnerModel.N200EventCode);
                        parms[2] = new SqlParameter("Type", contactModel.Type);
                        parms[3] = new SqlParameter("Error", responseError.Error.Additional == null ? responseError.Error.Description : responseError.Error.Description + ": " + responseError.Error.Additional);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_UPD_LogError");
                    }
                    catch (Exception ex)
                    {
                        log(ex.Message);
                    }
                }   
            }

            return ProcessEnums.enmStatus.Successful;
        }

        public void WriteStreamToLog(MemoryStream stream)
        {
            stream.Position = 0;
            var sr = new StreamReader(stream);
            var myStr = sr.ReadToEnd();
            log(myStr);
            stream.Position = 0;
        }
    }
}
