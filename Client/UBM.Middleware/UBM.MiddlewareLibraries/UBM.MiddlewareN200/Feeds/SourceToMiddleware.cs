﻿using UBM.MiddlewareProcess;

namespace UBM.MiddlewareN200.Feeds
{
    public class SourceToMiddleware : ProcessTask
    {
        public override ProcessEnums.enmStatus Execute()
        {
            //-- if this fails it will throw an exception up.
            this.FireDTSPackage("ESOP to N200 Snapshot.dtsx");

            return ProcessEnums.enmStatus.Successful;
        }
    }
}