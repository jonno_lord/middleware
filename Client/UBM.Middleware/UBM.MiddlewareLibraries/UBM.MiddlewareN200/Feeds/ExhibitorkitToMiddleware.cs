﻿using System;
using UBM.MiddlewareProcess;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareN200.ExhibitorkitWebService;
using System.Collections.Generic;
using System.Linq;
using UBM.MiddlewareN200.Common;

namespace UBM.MiddlewareN200.Feeds
{
    public class ExhibitorkitToMiddleware : ProcessTask
    {
        private string SFTPServer { get; set; }
        private string UserName { get; set; }
        private string Password { get; set; }
        private string SFTPDirectoryUnprocessed { get; set; }
        private string SFTPDirectoryProcessed { get; set; }
        private string LocalPath { get; set; }


        public override ProcessEnums.enmStatus Execute()
        {
            string esopConnection = RemoveProvider(ConfigurationManager.ConnectionStrings["ESOPConnectionString"].ToString());
            string n200Connetion = RemoveProvider(ConfigurationManager.ConnectionStrings["N200ConnectionString"].ToString());

            try
            {
                UBMConnectDailySummary webService = CommonTasks.CreateWebService();
                GenericCommands.ExecuteScalarSqlStatement(n200Connetion, "TRUNCATE TABLE Exhibitorkit");

                List<string> eventCodes = CommonTasks.GetExhibitorKitEventCodes(esopConnection);
                
                foreach (string eventCode in eventCodes)
                {
                    Show show = webService.GetShowdetailsByShowCode(eventCode);

                    if (show != null)
                    {
                        InsertExhibitorsIntoDB(show, n200Connetion);
                    }
                    else
                    {
                        log("Unable to retrive data for " + eventCode);
                    }
                }
            }
            catch (Exception ex)
            {
                log("Error: " + ex.Message);
            }
            
            return ProcessEnums.enmStatus.Successful;
        }

        private void InsertExhibitorsIntoDB(Show show, string n200Connection)
        {
            for (int i = 0; i < show.Exhibitors.Count(); i++)
            {
                using (SqlConnection connection = new SqlConnection(n200Connection))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("INSERT INTO Exhibitorkit (ExhibitorId, ShowId, Devices, Apps) VALUES (@ExhibitorId, @ShowId, @Devices, @Apps)");
                        command.CommandType = CommandType.Text;
                        command.Connection = connection;

                        command.Parameters.AddWithValue("@ExhibitorId",  show.Exhibitors[i].UBM_Exhibitor_ID);
                        command.Parameters.AddWithValue(@"ShowId", show.EK_Show_ID);
                        command.Parameters.AddWithValue(@"Devices", show.Exhibitors[i].No_of_Devices);
                        command.Parameters.AddWithValue(@"Apps", show.Exhibitors[i].No_of_Apps);

                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        log("Unable to insert ExhibitorId " + show.Exhibitors[i].UBM_Exhibitor_ID + " into the database");
                        log("Error message: " + ex.Message, ProcessAccess.enmLevel.Important);
                    }
                }
            }
        }
    }
}