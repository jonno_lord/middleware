﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UBM.MiddlewareN200.Common;
using UBM.MiddlewareN200.XmlModels;
using UBM.MiddlewareProcess;
using UBM.MiddlewareDataAccess;
using System.Reflection;
using System.Collections.Generic;
using System.Net;

namespace UBM.MiddlewareN200.Feeds
{
    public class N200ToMiddleware : ProcessTask
    {
        private EventList events;
        private RegistrationTypeList registrationTypes;
        private Event ev;
        private RegistrationType registrationType;

        public override ProcessEnums.enmStatus Execute()
        {
            string uri = CommonTasks.GetURI("events");

            log("Retrieving connection string for system id " + SystemId);

            string connectionString = Connections.GetConnectionString(SystemId);
            string sql = "TRUNCATE TABLE dbo.Events";
            GenericCommands.ExecuteScalarSqlStatement(connectionString, sql);

            sql = "TRUNCATE TABLE dbo.RegistrationTypes";
            GenericCommands.ExecuteScalarSqlStatement(connectionString, sql);

            sql = "TRUNCATE TABLE dbo.PersonnelTypes";
            GenericCommands.ExecuteScalarSqlStatement(connectionString, sql);

            using (var client = CommonTasks.CreateWebClient())
            {
                try
                {
                    events = GetData<EventList>(ref uri);
                    GetPagedResults(events, ref uri);
                    log(events.Items.Count() + " events returned from N200");

                    for (int i = 0; i < events.Items.Count(); i++)
                    {
                        string esopEventName = events.Items[i].EventName;
                        string n200EventCode = events.Items[i].Code;

                        SqlParameter[] parms = new SqlParameter[2];
                        parms[0] = new SqlParameter("ESOPEventName", esopEventName);
                        parms[1] = new SqlParameter("N200EventCode", n200EventCode);

                        GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_INS_Event");

                        uri = CommonTasks.SetEventURI(n200EventCode);
                        ev = GetData<Event>(ref uri);

                        foreach (PartnerSetting ps in ev.PartnerSettings)
                        {
                            foreach (Personnel p in ps.Personnel)
                            {
                                if (p != null)
                                {
                                    parms = new SqlParameter[3];
                                    parms[0] = new SqlParameter("N200EventCode", ev.Code);
                                    parms[1] = new SqlParameter("RegistrationTypeCode", ps.RegistrationType.Code);
                                    parms[2] = new SqlParameter("PersonnelTypeCode", p.Code);

                                    GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_INS_PersonnelType");
                                }
                            }
                        }
                    }

                    uri = CommonTasks.GetURI("registrationtypes") + "?showname=1";
                    registrationTypes = GetData<RegistrationTypeList>(ref uri);
                    GetPagedResults(registrationTypes, ref uri);
                    log(registrationTypes.Items.Count() + " registration types returned from N200");

                    for (int i = 0; i < registrationTypes.Items.Count(); i++)
                    {
                        uri = CommonTasks.GetURI("registrationtypes") + "/" + registrationTypes.Items[i].Code;
                        registrationType = GetData<RegistrationType>(ref uri);

                        if (registrationType.Type == "partner")
                        {
                            SqlParameter[] parms = new SqlParameter[2];
                            parms[0] = new SqlParameter("Description", registrationType.Name);
                            parms[1] = new SqlParameter("Code", registrationType.Code);

                            GenericCommands.ExecuteScalarStoredProcedure(parms, connectionString, "usp_INS_RegistrationType");
                        }
                    }

                }
                catch (Exception ex)
                {
                    log(ex.Message);
                    return ProcessEnums.enmStatus.Failed;
                }
            }
            return ProcessEnums.enmStatus.Successful;
        }

        private static RegistrationTypeList GetPagedResults(RegistrationTypeList registrationTypes, ref string uri)
        {
            try
            {
                if (registrationTypes.More == "/registration-types?revision=1")
                    return registrationTypes;

                uri = CommonTasks.GetURI("registrationtypes") + registrationTypes.More.Replace("/registration-types?", "?showname=1&");

                var list = GetData<RegistrationTypeList>(ref uri);

                if (list.Items.Count == 0)
                {
                    return registrationTypes;
                }
                else
                {
                    AddPagedResultsToList(registrationTypes, list);
                    return GetPagedResults(registrationTypes, ref uri);
                }
            }
            catch (Exception ex)
            {
                return registrationTypes;
            }
        }

        private static EventList GetPagedResults(EventList events, ref string uri)
        {
            try
            {
                if (events.More == "/events?revision=1")
                    return events;

                uri = CommonTasks.GetURI("events") + "&" + events.More.Replace("/events?","");

                var list = GetData<EventList>(ref uri);

                if (list.Items.Count == 0)
                {
                    return events;
                }
                else
                {
                    AddPagedResultsToList(events, list);
                    return GetPagedResults(events, ref uri);
                }
            }    
            catch (Exception ex)
            {
                return events;
            }
        }

        private static T GetData<T>(ref string uri)
        {
            using (var client = CommonTasks.CreateWebClient())
            {
                byte[] data = client.DownloadData(uri);
                var serializer = new XmlSerializer(typeof(T), "");
                var stream = new MemoryStream(data);

                var deserializedStream = (T)serializer.Deserialize(stream);

                return deserializedStream;
            }
        }

        private static EventList AddPagedResultsToList(EventList list, EventList more)
        {
            foreach (Event ev in more.Items)
            {
                list.Items.Add(ev);
            }

            list.More = more.More;

            return list;
        }

        private static RegistrationTypeList AddPagedResultsToList(RegistrationTypeList list, RegistrationTypeList more)
        {
            foreach (RegistrationType rt in more.Items)
            {
                list.Items.Add(rt);
            }

            list.More = more.More;

            return list;
        }
    }
}
