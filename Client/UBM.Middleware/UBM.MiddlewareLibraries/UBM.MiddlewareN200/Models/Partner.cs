﻿namespace UBM.MiddlewareN200.Models
{

    public class Partner
    {
        public string PartnerCode { get; set; }
        public string N200EventCode { get; set; }
        public string ESOPEventCode { get; set; }
        public string ExhibitorId { get; set; }
        public string ContractNumber { get; set; }
        public string RegistrationType { get; set; }
        public string ContactCode { get; set; }
        public string StandNumber { get; set; }
        public string StandSize { get; set; }
        public string BarcodeScanner { get; set; }
        public string MobileApp { get; set; }
        public string PersonnelType { get; set; }
        public string NumberOfBadges { get; set; }
        public string Type { get; set; }
    }
}
