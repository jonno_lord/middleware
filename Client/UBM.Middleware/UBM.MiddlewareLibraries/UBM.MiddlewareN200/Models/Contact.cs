﻿namespace UBM.MiddlewareN200.Models
{
    public class Contact
    {
        public string ExhibitingName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public string Website { get; set; }
        public string ExhibitorId { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public string DateOfBirth { get; set; }
    }
}
