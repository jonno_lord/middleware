﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using UBM.MiddlewareDataAccess;
using UBM.MiddlewareN200.XmlModels;
using System.Xml;
using System.Configuration;
using UBM.MiddlewareN200.ExhibitorkitWebService;

namespace UBM.MiddlewareN200.Common
{
    public static class CommonTasks
    {
        private static string PROXY_SERVER_NAME = @"";
        private static int PROXY_PORT = 0;
        private static string PROXY_USERNAME = @""; 
        private static string PROXY_PASSWORD = @""; 
        private static string PROXY_DOMAIN = @"";
        private static string EVENTS_URI = @"";
        private static string CONTACTS_URI = @"";
        private static string PARTNERS_URI = @"";
        private static string REGISTRATION_TYPES_URI = @"";


        public static string RemoveProvider(string connectionString)
        {
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1;", "");   //-- Remove references to SQL Providers
            connectionString = connectionString.Replace("Provider=SQLNCLI10.1", "");   //-- Remove references to SQL Providers
            return connectionString;
        }

        public static void SetProxy()
        {
            string connection = Connections.GetConnectionString("N200");
            connection = RemoveProvider(connection);

            using (SqlConnection cn = new SqlConnection(connection))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 1 * FROM Configuration";
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        PROXY_SERVER_NAME = dr["ProxyServerName"].ToString();
                        PROXY_PORT = int.Parse(dr["ProxyPort"].ToString());
                        PROXY_USERNAME = dr["ProxyUsername"].ToString();
                        PROXY_PASSWORD = dr["ProxyPassword"].ToString();
                        PROXY_DOMAIN = dr["ProxyDomain"].ToString();
                    }
                }
            }
        }

        public static void SetURI()
        {
            string connection = Connections.GetConnectionString("N200");
            connection = RemoveProvider(connection);

            using (SqlConnection cn = new SqlConnection(connection))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 1 * FROM Configuration";
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        EVENTS_URI = dr["EventsURI"].ToString();
                        CONTACTS_URI = dr["ContactsURI"].ToString();
                        PARTNERS_URI = dr["PartnersURI"].ToString();
                        REGISTRATION_TYPES_URI = dr["RegistrationTypesURI"].ToString();
                    }
                }
            }
        }

        public static WebClient CreateWebClient()
        {
            SetProxy();

            var proxy = new WebProxy(PROXY_SERVER_NAME, PROXY_PORT);
            var client = new WebClient();
            client.Headers["Content-type"] = "application/xml";
            client.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", "474b2f20-82f8-4850-b8e0-3a56f9724517" , "")));
            proxy.Credentials = new NetworkCredential(PROXY_USERNAME, PROXY_PASSWORD, PROXY_DOMAIN);
            client.Proxy = proxy;

            return client;
        }

        public static string GetURI(string entity)
        {
            SetURI();

            switch (entity)
            {
                case "events":
                    return EVENTS_URI;
                case "partners":
                    return PARTNERS_URI;
                case "contacts":
                    return CONTACTS_URI;
                case "registrationtypes":
                    return REGISTRATION_TYPES_URI;
                default:
                    return "";
            }
        }

        public static Contact CreateContact(Models.Contact c, XmlModels.Address a)
        {
            var la = new List<Address>();
            la.Add(a);

            return new Contact(c, la);
        }

        public static Partner CreatePartner(Models.Partner pm)
        {
            var lp = new List<Personnel>();
            var pl = new Personnel
            {
                Code = pm.PersonnelType,
                FreeBadges = pm.NumberOfBadges
            };

            lp.Add(pl);

            Event e = new Event
            {
                Code = pm.N200EventCode
            };

            RegistrationType rt = new RegistrationType
            {
                Code = pm.RegistrationType
            };

            PartnerContact pc = new PartnerContact
            {
                Code = pm.ContactCode
            };

            return new Partner
            {
                StandNumber = pm.StandNumber,
                StandSize = pm.StandSize,
                Event = e,
                RegistrationType = rt,
                PartnerContact = pc,
                PersonnelTypes = lp,
                BarcodeScanner = pm.BarcodeScanner,
                MobileApp = pm.MobileApp
            };
        }

        public static XmlSerializerNamespaces CreateXMLNamespace()
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("","");

            return ns;
        }

        public static string SetEventURI(string n200EventCode)
        {
            string uri = CommonTasks.GetURI("events");
            return uri.Replace("?showname=1", "/" + n200EventCode);
        }

        public static UBMConnectDailySummary CreateWebService()
        {
            UBMConnectDailySummary webService = new UBMConnectDailySummary();

            MySoapHeader soapHeader = new MySoapHeader();
            soapHeader.VerificationID = ConfigurationManager.AppSettings["VerificactionId"].ToString();
            webService.MySoapHeaderValue = soapHeader;

            return webService;
        }


        public static List<string> GetExhibitorKitEventCodes(string esopConnection)
        {
            List<string> eventCodes = new List<string>();

            using (SqlConnection sqlConnection = new SqlConnection(esopConnection))
            {
                string sql = "SELECT DISTINCT Event_Code FROM Event WHERE Exhibitorkit_Export IS NOT NULL";
                SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
                sqlConnection.Open();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        eventCodes.Add(sqlDataReader["Event_Code"].ToString());
                    }
                }
            }

            return eventCodes;
        }
    }
} 
