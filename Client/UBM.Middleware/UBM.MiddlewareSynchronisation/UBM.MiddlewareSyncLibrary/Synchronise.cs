﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using UBM.MiddlewareSyncLibrary.DataContracts;
using UBM.MiddlewareSyncLibrary.Interfaces;

namespace UBM.MiddlewareSyncLibrary
{
    /// <summary>
    /// This class provides functionality allowing Middleware Client application instances to keep in sync with each other regarding customer states.
    /// It keeps a record of who is View or actioning a customer and notifies users accordingly
    /// </summary>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class Synchronise : ISynchronise
    {
        #region Fields

        private const double LOCK_EXPIRATION_HOURS = 12.0;
        private const int AUTO_UPDATE = 300000;

        private static readonly List<Lock> LockedList = new List<Lock>();
        private static readonly List<Viewing> ViewingList = new List<Viewing>();
        private static readonly List<ServerViewing> ServerViewingList = new List<ServerViewing>();
        private static readonly Dictionary<ISynchroniseCallback, string> CallbackList = new Dictionary<ISynchroniseCallback, string>();

        private static readonly Stopwatch Timer = new Stopwatch();
        private static readonly object Locker = new object();

        private static Thread autoUpdate;
        private static bool keepUpdating;

        #endregion

        #region Private Methods

        private static void KillViewings(ISynchroniseCallback callback)
        {
            List<ServerViewing> svsToKill = new List<ServerViewing>();
            // kill viewings
            foreach (ServerViewing sv in ServerViewingList)
            {
                if (sv.Client == callback)
                {
                    ViewingList.Remove(sv.UserView);
                    svsToKill.Add(sv);
                }
            }

            foreach (ServerViewing sv in svsToKill)
            {
                ServerViewingList.Remove(sv);
            }
        }


        private void CheckForExpiredLocks()
        {
            List<Lock> expiredLocks = new List<Lock>();

            foreach (Lock locked in LockedList)
            {
                TimeSpan span = DateTime.Now.Subtract(locked.LockTime);
                if (span.TotalHours >= LOCK_EXPIRATION_HOURS)
                {
                    expiredLocks.Add(locked);
                    UpdateClients("Lock " + locked.ID + " - " + locked.SystemName + " has expired. Time: " + span, true);
                }
            }

            foreach (Lock locked in expiredLocks)
            {
                Unlock(locked);
            }
        }


        private void AutoUpdate()
        {

            for (; ; )
            {
                if (!keepUpdating) break;

                if (Timer.IsRunning)
                {
                    if (Timer.ElapsedMilliseconds > AUTO_UPDATE)
                    {
                        CheckForExpiredLocks();
                        UpdateClients("AutoUpdate");
                    }
                }
                Thread.Sleep(10000);
            }
        }

        private string LockOwner(Lock @lock)
        {
            Lock owned = LockedList.FirstOrDefault(c => c.EqualsIgnoringUserName(@lock));
            return owned != null ? owned.UserName : "";
        }

        private List<Lock> ValidateLocks(List<Lock> locks)
        {
            List<Lock> validLocks = new List<Lock>();

            foreach (Lock @lock in locks)
            {
                if (@lock.IsValid())
                {
                    if (@lock.AlreadyExists(LockedList))
                    {
                        UpdateClients(@lock.ObjectType + " Already Locked By " + LockOwner(@lock) + ": " + @lock.ID + " " + @lock.SystemName, true);
                    }
                    else
                    {
                        validLocks.Add(@lock);
                    }
                }

            }
            return validLocks;
        }
    

        private bool ProcessLocks(List<Lock> locks)
        {
            try
            {

                List<Lock> validLocks = ValidateLocks(locks);

                if (validLocks.Count == 0)
                    return false;

                foreach (Lock validLock in validLocks)
                {
                    lock (Locker)
                    {
                        LockedList.Add(validLock);
                    }
                    if(validLocks.Count == 1) UpdateClients(validLock.ObjectType + " Locked By " + validLock.UserName + ": " + validLock.ID + " " + validLock.SystemName);  
                }

                if (validLocks.Count > 1) UpdateClients(validLocks.Count + " " + validLocks[0].ObjectType + "s Locked By " + validLocks[0].UserName);

                return true;
       
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return false;
            }
        }

        private bool ProcessUnlocks(List<Lock> unlocks)
        {
            try
            {

                foreach (Lock unlock in unlocks)
                {
                    if(unlock.IsValid())
                    {
                        if(LockedList.FirstOrDefault(c=> c.Equals(unlock)) != null)
                        {
                            lock(Locker)
                            {                              
                                LockedList.RemoveAll(c => c.Equals(unlock));
                            }
                            if (unlocks.Count == 1) UpdateClients(unlock.ObjectType + " Unlocked By " + unlock.UserName + ": " + unlock.ID + " " + unlock.SystemName);
                        }
                        else
                        {
                            UpdateClients(unlock.ObjectType + " Already Unlocked" + ": " + unlock.ID + " " + unlock.SystemName, true);
                        }
                    }
                }

                if (unlocks.Count > 1) UpdateClients(unlocks.Count + " " + unlocks[0].ObjectType + "s Unlocked By " + unlocks[0].UserName);

                return true;

            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return false;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Connects to sync server
        /// </summary>
        /// <param name="userName">Windows username</param>
        public void Connect(string userName)
        {
            try
            {
                ISynchroniseCallback guest = OperationContext.Current.GetCallbackChannel<ISynchroniseCallback>();
                if(!CallbackList.ContainsKey(guest)) CallbackList.Add(guest, userName);

                if (!keepUpdating)
                {
                    keepUpdating = true;
                    autoUpdate = new Thread(AutoUpdate);
                    autoUpdate.Start();
                }

                UpdateClients(userName + " has connected");
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
            }
        }

        /// <summary>
        /// Disconnects from sync server
        /// </summary>
        /// <param name="userName">Windows username</param>
        public void Disconnect(string userName)
        {
            try
            {
                ISynchroniseCallback guest = OperationContext.Current.GetCallbackChannel<ISynchroniseCallback>();

                KillViewings(guest);
                CallbackList.Remove(guest);
                UpdateClients(userName + " disconnected");
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
            }
        }

        /// <summary>
        /// Notifies clients that a lock or View has changed
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="messageOnly">Flag to signify not to download an update</param>
        public void UpdateClients(string message = "", bool messageOnly = false)
        {
            List<ISynchroniseCallback> list = new List<ISynchroniseCallback>(CallbackList.Keys);

            list.ForEach(delegate(ISynchroniseCallback callback)
            {
                try
                {
                    string username = CallbackList[callback];

                    try
                    {
                        if (!messageOnly)  callback.SynchroniseNotification();
                        if(message!="")    callback.DebugNotification(message);
                    }
                    catch
                    {
                        CallbackList.Remove(callback); 
                        KillViewings(callback);
                        UpdateClients(username + " has been killed"); // recursive - continues to update clients after dead client has been removed
                    }
                }
                catch
                {
                    // ignore
                }
            });

            if (Timer.IsRunning)
            {
                Timer.Restart();
            }
            else
            {
                Timer.Start();
            }

        }

        /// <summary>
        /// Locks a customer
        /// </summary>
        /// <param name="lock">Lock information object</param>
        /// <returns>Returns True if successful</returns>
        public bool Lock(Lock @lock)
        {
            return ProcessLocks(new List<Lock> {@lock});
        }

        public bool LockRange(List<Lock> locks)
        {
            return ProcessLocks(locks);
        }

        /// <summary>
        /// Unlocks a customer
        /// </summary>
        /// <param name="lock">Lock information object</param>
        /// <returns>Returns True if successful</returns>
        public bool Unlock(Lock @lock)
        {
            return ProcessUnlocks(new List<Lock> {@lock});
        }

        public bool UnlockRange(List<Lock> locks)
        {
            return ProcessUnlocks(locks);
        }

        /// <summary>
        /// Unlocks all for user of the specified type
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="syncObjectType">Type of the sync object.</param>
        /// <returns></returns>
        public bool UnlockAllForUser(string userName, string syncObjectType)
        {
            try
            {
                int unlockCount = 0;
                for (int i = LockedList.Count - 1; i >= 0; i--)
                {
                    if (LockedList[i].UserName == userName && LockedList[i].ObjectType == syncObjectType)
                    {
                        LockedList.Remove(LockedList[i]);
                        unlockCount++;
                    }
                }
                if (unlockCount > 0)
                {
                    UpdateClients("All " + syncObjectType + " Locks Removed For " +
                                  userName);
                    return true;
                }
                return false;
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return false;
            }
           
        }

        /// <summary>
        /// Adds a viewer to a customer
        /// </summary>
        /// <param name="viewing">View information object</param>
        /// <returns>Returns True if successful</returns>
        public bool View(Viewing viewing)
        {
            try
            {
                if (viewing.IsValid()) 
                {                    

                    foreach (Viewing v in ViewingList)
                    {
                        if (v.Equals(viewing))
                        {
                            UpdateClients(v.ObjectType + " Already Viewed By " + v.UserName + ": " + v.ID + " " + v.SystemName, true);
                            return false; // this customer is already being viewed by this user
                        }
                    }

                    ISynchroniseCallback guest = OperationContext.Current.GetCallbackChannel<ISynchroniseCallback>();
                    ServerViewingList.Add(new ServerViewing
                                            {
                                                UserView = viewing,
                                                Client = guest
                                            });

                    ViewingList.Add(viewing);

                    UpdateClients(viewing.ObjectType + " Viewed By " + viewing.UserName + ": " + viewing.ID + " " + viewing.SystemName);
                    return true;
                }
                return false; // invalid view
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return false;
            }
        }

        /// <summary>
        /// Removes a viewer from a customer
        /// </summary>
        /// <param name="viewing">View information object</param>
        /// <returns>Returns True if successful</returns>
        public bool Unview(Viewing viewing)
        {
            try
            {
                if (viewing.IsValid()) 
                {
                    foreach (Viewing v in ViewingList)
                    {
                        if (v.Equals(viewing))
                        {

                            ViewingList.Remove(v);

                            ISynchroniseCallback guest = OperationContext.Current.GetCallbackChannel<ISynchroniseCallback>();
                            foreach (ServerViewing sv in ServerViewingList)
                            {
                                if (sv.UserView == v && sv.Client == guest)
                                {
                                    ServerViewingList.Remove(sv);
                                    break;
                                }
                            }

                            UpdateClients(viewing.ObjectType + " Unviewed By " + viewing.UserName + ": " + viewing.ID + " " + viewing.SystemName);
                            return true;
                        }
                    }
                    UpdateClients(viewing.ObjectType + " is not Viewed By " + viewing.UserName + ": " + viewing.ID + " " + viewing.SystemName, true);
                    return false;
                }
                return false; // invalid view
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return false;
            }
        }

        /// <summary>
        /// Returns a Sync object containing all the current locks and viewings
        /// </summary>
        /// <returns>Returns Sync object</returns>
        public SyncData GetSyncData(string syncObjectType)
        {
            try
            {
                if(string.IsNullOrEmpty(syncObjectType))
                {
                    return new SyncData
                    {
                        LockedList = LockedList,
                        ViewingList = ViewingList
                    };
                }

                // find the right type
                return new SyncData
                {
                    LockedList = LockedList.FindAll(c => c.ObjectType == syncObjectType),
                    ViewingList = ViewingList.FindAll(c => c.ObjectType == syncObjectType)
                };
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return null;
            }

        }

        /// <summary>
        /// Returns an object containing debug information
        /// </summary>
        /// <returns>Returns Debug object</returns>
        public DebugData GetDebugData()
        {
            try
            {
                DebugData debugData = new DebugData
                {
                    ServerViewingList = ServerViewingList,
                    CallbackList = CallbackList,
                    UserList = CallbackList.Values.ToList()
                };
                return debugData;
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
                return null;
            }
        }

        /// <summary>
        /// Notifies clients to refresh from database
        /// </summary>
        public void RefreshClients()
        {
            try
            {
                List<ISynchroniseCallback> list = new List<ISynchroniseCallback>(CallbackList.Keys);
                list.ForEach(callback => callback.RefreshFromDatabaseNotification());
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
            }
          
        }

        /// <summary>
        /// Sends the progress report.
        /// </summary>
        /// <param name="jobName">Name of the job.</param>
        /// <param name="stepDetail">The step detail.</param>
        /// <param name="percentageComplete">The percentage complete.</param>
        public void SendProgressReport(string jobName, string stepDetail, double percentageComplete)
        {
            try
            {
                List<ISynchroniseCallback> list = new List<ISynchroniseCallback>(CallbackList.Keys);
                list.ForEach(callback => callback.ProgressReportNotification(jobName,stepDetail,percentageComplete));
            }
            catch (Exception exception)
            {
                UpdateClients(exception.ToString(), true);
            }
        }

        #endregion



    }
}
