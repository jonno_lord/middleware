﻿using System.Runtime.Serialization;

namespace UBM.MiddlewareSyncLibrary.Interfaces
{

    interface ISyncable
    {
        ///<summary>
        /// Customer ID
        ///</summary>
        [DataMember]
        int ID { get; set; }

        ///<summary>
        /// System the Customer exists on
        ///</summary>
        [DataMember]
        string SystemName { get; set; }

        ///<summary>
        /// Name of the User who owns the lock
        ///</summary>
        [DataMember]
        string UserName { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>The type of the object.</value>
        [DataMember]
        string ObjectType { get; set; }


    }
}
