﻿using System.ServiceModel;

namespace UBM.MiddlewareSyncLibrary.Interfaces
{
    ///<summary>
    /// User callback class
    ///</summary>
    public interface ISynchroniseCallback
    {
        ///<summary>
        /// Callback to notify client that a lock or viewing has changed and a new Sync object must be downloaded
        ///</summary>
        [OperationContract(IsOneWay = true)]
        void SynchroniseNotification();

        ///<summary>
        /// Notification that the client database has changed and a refresh is required
        ///</summary>
        [OperationContract(IsOneWay = true)]
        void RefreshFromDatabaseNotification();

        /// <summary>
        /// Notifies client of the progress of a running job
        /// </summary>
        /// <param name="jobName">Name of the job.</param>
        /// <param name="stepDetail">The step detail.</param>
        /// <param name="percentageComplete">The percentage complete.</param>
        [OperationContract(IsOneWay = true)]
        void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete);

        ///<summary>
        /// Notification for debug information
        ///</summary>
        ///<param name="message">Debug message</param>
        [OperationContract(IsOneWay = true)]
        void DebugNotification(string message);
    }
}
