﻿using System.Collections.Generic;
using System.ServiceModel;
using UBM.MiddlewareSyncLibrary.DataContracts;

namespace UBM.MiddlewareSyncLibrary.Interfaces
{

    ///<summary>
    /// Interface for Sync Service
    ///</summary>
    [ServiceContract(
    CallbackContract = typeof(ISynchroniseCallback),
    SessionMode = SessionMode.Required)]
    public interface ISynchronise
    {
        ///<summary>
        /// Connects a user to the service
        ///</summary>
        ///<param name="userName">Windows username</param>
        [OperationContract(IsOneWay = true)]
        void Connect(string userName);

        ///<summary>
        /// Disconnects user from service
        ///</summary>
        ///<param name="userName">Windows username</param>
        [OperationContract(IsOneWay = true)]
        void Disconnect(string userName);

        ///<summary>
        /// Locks a customer
        ///</summary>
        ///<param name="lock">Lock information object</param>
        ///<returns>True if successful</returns>
        [OperationContract]
        bool Lock(Lock @lock);


        ///<summary>
        /// Locks a customer
        ///</summary>
        ///<param name="locks">Lock information object</param>
        ///<returns>True if successful</returns>
        [OperationContract]
        bool LockRange(List<Lock> locks);
        
        ///<summary>
        /// Unlocks a customer
        ///</summary>
        ///<param name="lock">Lock information object</param>
        ///<returns>True if successful</returns>
        [OperationContract]
        bool Unlock(Lock @lock);


        ///<summary>
        /// Unlocks a customer
        ///</summary>
        ///<param name="locks">Lock information object</param>
        ///<returns>True if successful</returns>
        [OperationContract]
        bool UnlockRange(List<Lock> locks);

        /// <summary>
        /// Unlocks all for user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="syncObjectType">Type of the sync object.</param>
        /// <returns></returns>
        [OperationContract]
        bool UnlockAllForUser(string userName, string syncObjectType);

        ///<summary>
        /// Views a customer
        ///</summary>
        ///<param name="viewing">View information object</param>
        ///<returns>True if successful</returns>
        [OperationContract]
        bool View(Viewing viewing);

        ///<summary>
        /// Unviews a customer
        ///</summary>
        ///<param name="viewing">View information object</param>
        ///<returns>True if successful</returns>
        [OperationContract]
        bool Unview(Viewing viewing);

        ///<summary>
        /// Downloads Sync object containing lock and View lists
        ///</summary>
        ///<returns>Sync object</returns>
        [OperationContract]
        SyncData GetSyncData(string objectType);

        ///<summary>
        /// Downloads Debug object containing debug and diagnostic information
        ///</summary>
        ///<returns>Debug object</returns>
        [OperationContract]
        DebugData GetDebugData();

        ///<summary>
        /// Tells the Sync Service to send a refresh notification to the other clients
        ///</summary>
        [OperationContract(IsOneWay = true)]
        void RefreshClients();

        /// <summary>
        /// Sends the progress report.
        /// </summary>
        /// <param name="jobName">Name of the job.</param>
        /// <param name="stepDetail">The step detail.</param>
        /// <param name="percentageComplete">The percentage complete.</param>
        [OperationContract(IsOneWay = true)]
        void SendProgressReport(string jobName, string stepDetail, double percentageComplete);
    }
}
