﻿using System.Runtime.Serialization;
using UBM.MiddlewareSyncLibrary.Interfaces;

namespace UBM.MiddlewareSyncLibrary.DataContracts
{
    ///<summary>
    /// The ServerViewing class stores a View object and the Callback address of the client associated with the View
    ///</summary>
    [DataContract]
    public class ServerViewing
    {
        ///<summary>
        /// Holds View object
        ///</summary>
        [DataMember]
        public Viewing UserView { get; set; }

        ///<summary>
        /// Client callback
        ///</summary>
        [DataMember]
        public ISynchroniseCallback Client { get; set; }
    }
}