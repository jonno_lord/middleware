﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using UBM.MiddlewareSyncLibrary.Interfaces;

namespace UBM.MiddlewareSyncLibrary.DataContracts
{
    [DataContract]
    public abstract class Syncable : ISyncable
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string SystemName { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string ObjectType { get; set; }


        public bool IsValid()
        {
            if (!string.IsNullOrEmpty(SystemName) && !string.IsNullOrEmpty(UserName))
            {
                return true;
            }
            return false;
        }

      

        public override bool Equals(object obj)
        {
            Syncable syncable = obj as Syncable;

            if (syncable != null)
            {
                return syncable.ID == ID && syncable.SystemName == SystemName && syncable.ObjectType == ObjectType && syncable.UserName == UserName;
            }

            return false;
        }

        public bool EqualsIgnoringUserName(Syncable syncable)
        {           
            if (syncable != null)
            {
                return syncable.ID == ID && syncable.SystemName == SystemName && syncable.ObjectType == ObjectType;
            }

            return false;
        }

    }
}
