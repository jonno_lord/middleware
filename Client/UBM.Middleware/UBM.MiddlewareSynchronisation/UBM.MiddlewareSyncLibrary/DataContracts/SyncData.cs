﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UBM.MiddlewareSyncLibrary.DataContracts
{
    ///<summary>
    /// The Sync class is the main information object for the client
    /// It holds a list of customer locks and customer viewings
    ///</summary>
    [DataContract]
    public class SyncData
    {
        ///<summary>
        /// Customer locks
        ///</summary>
        [DataMember]
        public List<Lock> LockedList { get; set; }

        ///<summary>
        /// Customer viewings
        ///</summary>
        [DataMember]
        public List<Viewing> ViewingList { get; set; }
    }
}