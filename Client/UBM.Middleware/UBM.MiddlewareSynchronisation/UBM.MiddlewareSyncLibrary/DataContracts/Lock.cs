﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UBM.MiddlewareSyncLibrary.Interfaces;

namespace UBM.MiddlewareSyncLibrary.DataContracts
{
    ///<summary>
    /// Lock class holds information about an individual lock on a customer
    ///</summary>
    [DataContract]
    public class Lock : Syncable
    {

        ///<summary>
        /// Time when customer was locked
        ///</summary>
        [DataMember]
        public DateTime LockTime { get; set; }

        public bool AlreadyExists(List<Lock> syncable)
        {
            foreach (Lock sync in syncable)
            {

                if (sync.EqualsIgnoringUserName((this)))
                {

                    return true; // this customer is already Lock
                }
            }
            return false;
        }
    }
}