﻿using System.Runtime.Serialization;
using UBM.MiddlewareSyncLibrary.Interfaces;

namespace UBM.MiddlewareSyncLibrary.DataContracts
{
    ///<summary>
    /// View holds information about a customer viewing
    ///</summary>
    [DataContract]
    public class Viewing : Syncable
    {
      
    }
}