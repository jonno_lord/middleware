﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using UBM.MiddlewareSyncLibrary.Interfaces;

namespace UBM.MiddlewareSyncLibrary.DataContracts
{
    ///<summary>
    /// The Debug class contains debugging and diagnostic data about the Sync Service
    ///</summary>
    [DataContract]
    public class DebugData
    {
        ///<summary>
        /// Server side list of customer viewings
        ///</summary>
        [DataMember]
        public List<ServerViewing> ServerViewingList { get; set; }

        ///<summary>
        /// Dictionary of callbacks against user names
        ///</summary>
        [DataMember]
        public Dictionary<ISynchroniseCallback, string> CallbackList { get; set; }

        ///<summary>
        /// List of user names currently connected
        ///</summary>
        [DataMember]
        public List<string> UserList { get; set; }

    }
}