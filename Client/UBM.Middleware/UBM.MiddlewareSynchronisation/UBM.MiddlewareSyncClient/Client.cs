﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBM.MiddlewareSyncClient.SyncClient;
using System.ServiceModel;

namespace UBM.MiddlewareSyncClient
{
        [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
     CallbackBehavior(UseSynchronizationContext = false)]
    public class Client : ISynchroniseCallback
    {
        private SynchroniseClient synchroniseClient;

        public Client()
        {
            synchroniseClient = new SynchroniseClient(new InstanceContext(this));

        }

        public void Connect()
        {
            synchroniseClient.Connect(Environment.UserName);
        }

        public void Disconnect()
        {
            synchroniseClient.Disconnect(Environment.UserName);
        }

        public bool LockCustomer(Locked locked)
        {
            return synchroniseClient.LockCustomer(locked);
        }

        public bool UnlockCustomer(Locked locked)
        {
            return synchroniseClient.UnlockCustomer(locked);
        }

        public bool ViewCustomer(Viewing viewing)
        {
            return synchroniseClient.ViewCustomer(viewing);
        }

        public bool UnviewCustomer(Viewing viewing)
        {
            return synchroniseClient.UnviewCustomer(viewing);
        }

        public Sync GetSyncInfo()
        {
            return synchroniseClient.GetSyncInfo();
        }
            
        public void UpdateNotification()
        {

        }

        public void RefreshNotification()
        {
        }

        public void DebugNotification(string message)
        {
        }
    }
}
