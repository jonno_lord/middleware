﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using UBM.MiddlewareSyncLibrary;

namespace UBM.MiddlewareSyncService
{
    public partial class ClientSyncService : ServiceBase
    {
        ServiceHost sHost;

        public ClientSyncService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            sHost = new ServiceHost(typeof(MiddlewareSyncLibrary.Synchronise));
            sHost.Open();
        }

        protected override void OnStop()
        {
            sHost.Close();
        }
    }
}
