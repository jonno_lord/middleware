﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace UBM.MiddlewareSyncService
{
    /// <summary>
    /// Wrapper class for the Sync Library. Allows it to run as a windows service
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new ClientSyncService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
