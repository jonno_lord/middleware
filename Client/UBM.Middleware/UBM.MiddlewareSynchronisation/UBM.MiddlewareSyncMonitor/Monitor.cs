﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using UBM.MiddlewareSyncRefresher;
using DebugData = UBM.MiddlewareSyncMonitor.ClientSyncService.DebugData;
using ISynchroniseCallback = UBM.MiddlewareSyncMonitor.ClientSyncService.ISynchroniseCallback;
using Lock = UBM.MiddlewareSyncMonitor.ClientSyncService.Lock;
using SyncData = UBM.MiddlewareSyncMonitor.ClientSyncService.SyncData;
using SynchroniseClient = UBM.MiddlewareSyncMonitor.ClientSyncService.SynchroniseClient;
using Viewing = UBM.MiddlewareSyncMonitor.ClientSyncService.Viewing;


namespace UBM.MiddlewareSyncMonitor
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
     CallbackBehavior(UseSynchronizationContext = false)]
    public partial class Monitor : Form, ISynchroniseCallback
    {
        private SynchroniseClient client;
        private SynchronizationContext uiContext;

        public Monitor()
        {
            InitializeComponent();
        }

        #region ISynchroniseCallback Members

        public void SynchroniseNotification()
        {
            SendOrPostCallback callback = delegate { Program.form.RefreshScreen(""); };

            uiContext.Post(callback, null);
        }

        public void RefreshFromDatabaseNotification()
        {
            SendOrPostCallback callback = delegate { AppendFeed("Client: Refresh Notification Received" + "\r\n"); };

            uiContext.Post(callback, null);
        }

        public void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
            SendOrPostCallback callback = delegate
            {
                AppendProgress(jobName + ": " + stepDetail + " " + percentageComplete + "% complete");                
            };

            uiContext.Post(callback, null);
        }

        public void DebugNotification(string message)
        {
            SendOrPostCallback callback = delegate { Program.form.RefreshScreen(DateTime.Now + ": " + message); };

            uiContext.Post(callback, null);
        }

        #endregion

        private void DisableButtons()
        {
            btnConnect.Enabled = true;
            btnDisconnect.Enabled = false;
            btnLock.Enabled = false;
            btnUnlock.Enabled = false;
            btnView.Enabled = false;
            btnUnview.Enabled = false;
            btnRefreshClients.Enabled = false;
            btnUnlockAll.Enabled = false;
            btnProgressReport.Enabled = false;
            btnUnlockRange.Enabled = false;
            btnLockRange.Enabled = false;
        }

        private void EnableButtons()
        {
            btnConnect.Enabled = false;
            btnDisconnect.Enabled = true;
            btnLock.Enabled = true;
            btnUnlock.Enabled = true;
            btnView.Enabled = true;
            btnUnview.Enabled = true;
            btnRefreshClients.Enabled = true;
            btnUnlockAll.Enabled = true;
            btnProgressReport.Enabled = true;
            btnUnlockRange.Enabled = true;
            btnLockRange.Enabled = true;
        }

        private void AppendFeed(string feedText)
        {
            txtFeed.Text += feedText + "\r\n";
            txtFeed.SelectionStart = txtFeed.Text.Length;
            txtFeed.ScrollToCaret();
        }

        private void AppendProgress(string progressText)
        {
            txtProgress.Text += progressText + "\r\n";
            txtProgress.SelectionStart = txtProgress.Text.Length;
            txtProgress.ScrollToCaret();
        }

        private void Monitor_Load(object sender, EventArgs e)
        {
            DisableButtons();

            uiContext = SynchronizationContext.Current;
            client = new SynchroniseClient(new InstanceContext(this));
            client.Open();
        }

        private void Monitor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (client.State == CommunicationState.Faulted)
            {
                client.Abort();
            }
            else
            {
                client.Disconnect(txtConnectUser.Text);
                client.Close();
                AppendFeed("Disconnected");
            }
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            try
            {
                client.Lock(new Lock
                                        {
                                            ID = int.Parse(txtId.Text),
                                            SystemName = txtSystem.Text,
                                            LockTime = DateTime.Now,
                                            UserName = txtConnectUser.Text,
                                            ObjectType = cboSyncType.Text
                                        });
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {
            try
            {
                client.Unlock(new Lock
                                          {
                                              ID = int.Parse(txtId.Text),
                                              SystemName = txtSystem.Text,
                                              UserName = txtConnectUser.Text,
                                              ObjectType = cboSyncType.Text
                                          });
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnUnlockAll_Click(object sender, EventArgs e)
        {
            try
            {
                client.UnlockAllForUser(txtConnectUser.Text, cboSyncType.Text);
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                client.View(new Viewing
                                        {
                                            ID = int.Parse(txtId.Text),
                                            SystemName = txtSystem.Text,
                                            UserName = txtConnectUser.Text,
                                            ObjectType = cboSyncType.Text
                                        });
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnUnview_Click(object sender, EventArgs e)
        {
            try
            {
                client.Unview(new Viewing
                                          {
                                              ID = int.Parse(txtId.Text),
                                              SystemName = txtSystem.Text,
                                              UserName = txtConnectUser.Text,
                                              ObjectType = cboSyncType.Text
                                          });
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        public void RefreshScreen(string message)
        {
            if (message != "") 
                AppendFeed(message);

            try
            {
                SyncData s = client.GetSyncData(null);

                if (s != null)
                {
                    List<Lock> locked = s.LockedList.ToList();
                    List<Viewing> viewed = s.ViewingList.ToList();

                    txtLocked.Clear();
                    txtViewed.Clear();

                    foreach (Lock l in locked)
                    {
                        txtLocked.Text += l.ObjectType.ToString() + " " + l.ID + " " + l.SystemName + " " + l.UserName + " " + l.LockTime + "\r\n";
                    }

                    foreach (Viewing v in viewed)
                    {
                        txtViewed.Text += v.ObjectType.ToString() + " " +  v.ID + " " + v.SystemName + " " + v.UserName + "\r\n";
                    }
                }

                DebugData d = client.GetDebugData();

                if (d != null)
                {
                    string[] users = d.UserList;
                    txtConnectedUsers.Clear();

                    foreach (string user in users)
                    {
                        txtConnectedUsers.Text += user + "\r\n";
                    }
                }
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                client.Connect(txtConnectUser.Text);
                txtConnectUser.ReadOnly = true;
                RefreshScreen("");
                EnableButtons();
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                client.Disconnect(txtConnectUser.Text);
                txtConnectUser.ReadOnly = false;
                DisableButtons();

                txtConnectedUsers.Clear();
                txtFeed.Clear();
                txtLocked.Clear();
                txtViewed.Clear();
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtFeed.Clear();
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnRefreshClients_Click(object sender, EventArgs e)
        {
            SyncRefresher.RefreshClients();
        }

        private void btnProgressReport_Click(object sender, EventArgs e)
        {
            try
            {
                SyncRefresher.SendProgressReport(txtJobName.Text, txtStepDetail.Text, double.Parse(txtPercentComplete.Text));
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }

        }

        private void btnLockRange_Click(object sender, EventArgs e)
        {
            try
            {
                List<Lock> locks = new List<Lock>();

                for (int i = 0; i < 11; i++)
                {
                    locks.Add(new Lock
                    {
                        ID = i,
                        LockTime = DateTime.Now,
                        ObjectType = cboSyncType.Text,
                        SystemName = "Test",
                        UserName = txtConnectUser.Text
                    });
                }

                client.LockRange(locks.ToArray());
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        
        }

        private void btnUnlockRange_Click(object sender, EventArgs e)
        {
            try
            {
                List<Lock> locks = new List<Lock>();

                for (int i = 0; i < 11; i++)
                {
                    locks.Add(new Lock
                    {
                        ID = i,
                        LockTime = DateTime.Now,
                        ObjectType = cboSyncType.Text,
                        SystemName = "Test",
                        UserName = txtConnectUser.Text
                    });
                }

                client.UnlockRange(locks.ToArray());
            }
            catch (Exception exception)
            {
                AppendFeed("Client: " + exception);
            }
        }

        private void btnClearProgress_Click(object sender, EventArgs e)
        {
            txtProgress.Clear();
        }

    
    }
}