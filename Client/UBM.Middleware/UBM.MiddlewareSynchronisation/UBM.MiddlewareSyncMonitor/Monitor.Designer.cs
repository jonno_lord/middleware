﻿namespace UBM.MiddlewareSyncMonitor
{
    partial class Monitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLock = new System.Windows.Forms.Button();
            this.btnUnlock = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtSystem = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnView = new System.Windows.Forms.Button();
            this.btnUnview = new System.Windows.Forms.Button();
            this.txtLocked = new System.Windows.Forms.TextBox();
            this.txtViewed = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtConnectedUsers = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.txtConnectUser = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProgress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFeed = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnUnlockRange = new System.Windows.Forms.Button();
            this.btnLockRange = new System.Windows.Forms.Button();
            this.btnUnlockAll = new System.Windows.Forms.Button();
            this.btnProgressReport = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPercentComplete = new System.Windows.Forms.TextBox();
            this.txtStepDetail = new System.Windows.Forms.TextBox();
            this.txtJobName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboSyncType = new System.Windows.Forms.ComboBox();
            this.btnRefreshClients = new System.Windows.Forms.Button();
            this.btnClearProgress = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLock
            // 
            this.btnLock.Location = new System.Drawing.Point(190, 66);
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(75, 23);
            this.btnLock.TabIndex = 0;
            this.btnLock.Text = "Lock";
            this.btnLock.UseVisualStyleBackColor = true;
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // btnUnlock
            // 
            this.btnUnlock.Location = new System.Drawing.Point(270, 66);
            this.btnUnlock.Name = "btnUnlock";
            this.btnUnlock.Size = new System.Drawing.Size(75, 23);
            this.btnUnlock.TabIndex = 1;
            this.btnUnlock.Text = "Unlock";
            this.btnUnlock.UseVisualStyleBackColor = true;
            this.btnUnlock.Click += new System.EventHandler(this.btnUnlock_Click);
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(75, 69);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 2;
            // 
            // txtSystem
            // 
            this.txtSystem.Location = new System.Drawing.Point(75, 95);
            this.txtSystem.Name = "txtSystem";
            this.txtSystem.Size = new System.Drawing.Size(100, 20);
            this.txtSystem.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "System:";
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(190, 92);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 8;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnUnview
            // 
            this.btnUnview.Location = new System.Drawing.Point(271, 92);
            this.btnUnview.Name = "btnUnview";
            this.btnUnview.Size = new System.Drawing.Size(75, 23);
            this.btnUnview.TabIndex = 9;
            this.btnUnview.Text = "Unview";
            this.btnUnview.UseVisualStyleBackColor = true;
            this.btnUnview.Click += new System.EventHandler(this.btnUnview_Click);
            // 
            // txtLocked
            // 
            this.txtLocked.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtLocked.Location = new System.Drawing.Point(144, 32);
            this.txtLocked.Multiline = true;
            this.txtLocked.Name = "txtLocked";
            this.txtLocked.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLocked.Size = new System.Drawing.Size(271, 374);
            this.txtLocked.TabIndex = 10;
            // 
            // txtViewed
            // 
            this.txtViewed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtViewed.Location = new System.Drawing.Point(421, 32);
            this.txtViewed.Multiline = true;
            this.txtViewed.Name = "txtViewed";
            this.txtViewed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtViewed.Size = new System.Drawing.Size(240, 374);
            this.txtViewed.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(141, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Locked:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(425, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Viewed:";
            // 
            // txtConnectedUsers
            // 
            this.txtConnectedUsers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtConnectedUsers.Location = new System.Drawing.Point(9, 32);
            this.txtConnectedUsers.Multiline = true;
            this.txtConnectedUsers.Name = "txtConnectedUsers";
            this.txtConnectedUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtConnectedUsers.Size = new System.Drawing.Size(129, 374);
            this.txtConnectedUsers.TabIndex = 19;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(190, 11);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 20;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(271, 11);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnDisconnect.TabIndex = 21;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // txtConnectUser
            // 
            this.txtConnectUser.Location = new System.Drawing.Point(75, 14);
            this.txtConnectUser.Name = "txtConnectUser";
            this.txtConnectUser.Size = new System.Drawing.Size(100, 20);
            this.txtConnectUser.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Users Online:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnClearProgress);
            this.groupBox1.Controls.Add(this.txtProgress);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtFeed);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Location = new System.Drawing.Point(674, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(405, 546);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Debug";
            // 
            // txtProgress
            // 
            this.txtProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProgress.Location = new System.Drawing.Point(6, 323);
            this.txtProgress.Multiline = true;
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProgress.Size = new System.Drawing.Size(393, 188);
            this.txtProgress.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Feed:";
            // 
            // txtFeed
            // 
            this.txtFeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFeed.Location = new System.Drawing.Point(6, 32);
            this.txtFeed.Multiline = true;
            this.txtFeed.Name = "txtFeed";
            this.txtFeed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFeed.Size = new System.Drawing.Size(393, 256);
            this.txtFeed.TabIndex = 2;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(6, 294);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 25;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "User Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.txtLocked);
            this.groupBox2.Controls.Add(this.txtViewed);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtConnectedUsers);
            this.groupBox2.Location = new System.Drawing.Point(3, 140);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(665, 412);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Monitoring";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnUnlockRange);
            this.groupBox3.Controls.Add(this.btnLockRange);
            this.groupBox3.Controls.Add(this.btnUnlockAll);
            this.groupBox3.Controls.Add(this.btnProgressReport);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtPercentComplete);
            this.groupBox3.Controls.Add(this.txtStepDetail);
            this.groupBox3.Controls.Add(this.txtJobName);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.cboSyncType);
            this.groupBox3.Controls.Add(this.btnRefreshClients);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnLock);
            this.groupBox3.Controls.Add(this.btnUnlock);
            this.groupBox3.Controls.Add(this.txtId);
            this.groupBox3.Controls.Add(this.txtSystem);
            this.groupBox3.Controls.Add(this.txtConnectUser);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnDisconnect);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btnConnect);
            this.groupBox3.Controls.Add(this.btnView);
            this.groupBox3.Controls.Add(this.btnUnview);
            this.groupBox3.Location = new System.Drawing.Point(3, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(665, 128);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Control";
            // 
            // btnUnlockRange
            // 
            this.btnUnlockRange.Location = new System.Drawing.Point(271, 40);
            this.btnUnlockRange.Name = "btnUnlockRange";
            this.btnUnlockRange.Size = new System.Drawing.Size(83, 23);
            this.btnUnlockRange.TabIndex = 41;
            this.btnUnlockRange.Text = "UnlockRange";
            this.btnUnlockRange.UseVisualStyleBackColor = true;
            this.btnUnlockRange.Click += new System.EventHandler(this.btnUnlockRange_Click);
            // 
            // btnLockRange
            // 
            this.btnLockRange.Location = new System.Drawing.Point(190, 39);
            this.btnLockRange.Name = "btnLockRange";
            this.btnLockRange.Size = new System.Drawing.Size(75, 23);
            this.btnLockRange.TabIndex = 40;
            this.btnLockRange.Text = "LockRange";
            this.btnLockRange.UseVisualStyleBackColor = true;
            this.btnLockRange.Click += new System.EventHandler(this.btnLockRange_Click);
            // 
            // btnUnlockAll
            // 
            this.btnUnlockAll.Location = new System.Drawing.Point(351, 67);
            this.btnUnlockAll.Name = "btnUnlockAll";
            this.btnUnlockAll.Size = new System.Drawing.Size(75, 23);
            this.btnUnlockAll.TabIndex = 39;
            this.btnUnlockAll.Text = "Unlock All";
            this.btnUnlockAll.UseVisualStyleBackColor = true;
            this.btnUnlockAll.Click += new System.EventHandler(this.btnUnlockAll_Click);
            // 
            // btnProgressReport
            // 
            this.btnProgressReport.Location = new System.Drawing.Point(498, 95);
            this.btnProgressReport.Name = "btnProgressReport";
            this.btnProgressReport.Size = new System.Drawing.Size(161, 23);
            this.btnProgressReport.TabIndex = 38;
            this.btnProgressReport.Text = "Send Progress Report";
            this.btnProgressReport.UseVisualStyleBackColor = true;
            this.btnProgressReport.Click += new System.EventHandler(this.btnProgressReport_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(492, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "% Complete:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(495, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "Step Detail:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(499, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Job Name:";
            // 
            // txtPercentComplete
            // 
            this.txtPercentComplete.Location = new System.Drawing.Point(559, 68);
            this.txtPercentComplete.Name = "txtPercentComplete";
            this.txtPercentComplete.Size = new System.Drawing.Size(100, 20);
            this.txtPercentComplete.TabIndex = 34;
            // 
            // txtStepDetail
            // 
            this.txtStepDetail.Location = new System.Drawing.Point(559, 42);
            this.txtStepDetail.Name = "txtStepDetail";
            this.txtStepDetail.Size = new System.Drawing.Size(100, 20);
            this.txtStepDetail.TabIndex = 33;
            // 
            // txtJobName
            // 
            this.txtJobName.Location = new System.Drawing.Point(559, 16);
            this.txtJobName.Name = "txtJobName";
            this.txtJobName.Size = new System.Drawing.Size(100, 20);
            this.txtJobName.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Sync Object:";
            // 
            // cboSyncType
            // 
            this.cboSyncType.FormattingEnabled = true;
            this.cboSyncType.Items.AddRange(new object[] {
            "Customer",
            "Invoice"});
            this.cboSyncType.Location = new System.Drawing.Point(75, 42);
            this.cboSyncType.Name = "cboSyncType";
            this.cboSyncType.Size = new System.Drawing.Size(100, 21);
            this.cboSyncType.TabIndex = 30;
            this.cboSyncType.Text = "Customer";
            // 
            // btnRefreshClients
            // 
            this.btnRefreshClients.Location = new System.Drawing.Point(352, 11);
            this.btnRefreshClients.Name = "btnRefreshClients";
            this.btnRefreshClients.Size = new System.Drawing.Size(108, 23);
            this.btnRefreshClients.TabIndex = 29;
            this.btnRefreshClients.Text = "Refresh Clients";
            this.btnRefreshClients.UseVisualStyleBackColor = true;
            this.btnRefreshClients.Click += new System.EventHandler(this.btnRefreshClients_Click);
            // 
            // btnClearProgress
            // 
            this.btnClearProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearProgress.Location = new System.Drawing.Point(6, 517);
            this.btnClearProgress.Name = "btnClearProgress";
            this.btnClearProgress.Size = new System.Drawing.Size(75, 23);
            this.btnClearProgress.TabIndex = 27;
            this.btnClearProgress.Text = "Clear";
            this.btnClearProgress.UseVisualStyleBackColor = true;
            this.btnClearProgress.Click += new System.EventHandler(this.btnClearProgress_Click);
            // 
            // Monitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 564);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Monitor";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Monitor_FormClosing);
            this.Load += new System.EventHandler(this.Monitor_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLock;
        private System.Windows.Forms.Button btnUnlock;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtSystem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnUnview;
        private System.Windows.Forms.TextBox txtLocked;
        private System.Windows.Forms.TextBox txtViewed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtConnectedUsers;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.TextBox txtConnectUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFeed;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRefreshClients;
        private System.Windows.Forms.ComboBox cboSyncType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnProgressReport;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPercentComplete;
        private System.Windows.Forms.TextBox txtStepDetail;
        private System.Windows.Forms.TextBox txtJobName;
        private System.Windows.Forms.Button btnUnlockAll;
        private System.Windows.Forms.TextBox txtProgress;
        private System.Windows.Forms.Button btnUnlockRange;
        private System.Windows.Forms.Button btnLockRange;
        private System.Windows.Forms.Button btnClearProgress;
    }
}

