﻿using System;
using System.Windows.Forms;

namespace UBM.MiddlewareSyncMonitor
{
    internal static class Program
    {
        public static Monitor form;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            form = new Monitor();

            Application.Run(form);
        }
    }
}