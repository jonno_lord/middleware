﻿using System.ServiceModel;
using UBM.MiddlewareSyncLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UBM.MiddlewareSyncLibrary.Test.SyncClient;
using System.Threading;

namespace UBM.MiddlewareSyncLibrary.Test
{
    
    
    /// <summary>
    ///This is a test class for SynchroniseTest and is intended
    ///to contain all SynchroniseTest Unit Tests
    ///</summary>
    ///    
     [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
     CallbackBehavior(UseSynchronizationContext = false)]
    [TestClass()]
    public class SynchroniseTest : ISynchroniseCallback
    {


        private TestContext testContextInstance;


        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Synchronise Constructor
        ///</summary>
        [TestMethod()]
        public void SynchroniseConstructorTest()
        {
            Synchronise target = new Synchronise();
            Assert.IsInstanceOfType(target, typeof(Synchronise));
        }


        /// <summary>
        ///A test for GetDebugInfo
        ///</summary>
        [TestMethod()]
        public void GetDebugInfoTest()
        {
            Synchronise target = new Synchronise(); // TODO: Initialize to an appropriate value
            Debug expected = null; // TODO: Initialize to an appropriate value
            Debug actual;
            actual = target.GetDebugInfo();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetSyncInfo
        ///</summary>
        [TestMethod()]
        public void GetSyncInfoTest()
        {
            Synchronise target = new Synchronise(); // TODO: Initialize to an appropriate value
            Sync expected = null; // TODO: Initialize to an appropriate value
            Sync actual;
            actual = target.GetSyncInfo();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }


        /// <summary>
        ///A test for LockCustomer
        ///</summary>
        [TestMethod()]
        public void LockCustomerTest()
        {
            SynchroniseClient synchronise = new SynchroniseClient(new InstanceContext(this));
            ksynchronise.Connect(Environment.UserName);
            Locked locked = new Locked() {ID = 1, SystemName = "Test", LockUserName = Environment.UserName};
            Assert.IsTrue(synchronise.LockCustomer(locked));
        }


        /// <summary>
        ///A test for UnlockCustomer
        ///</summary>
        [TestMethod()]
        public void UnlockCustomerTest()
        {
            SynchroniseClient synchronise = new SynchroniseClient(new InstanceContext(this));
            Locked locked = new Locked() { ID = 1, SystemName = "Test", LockUserName = Environment.UserName };
            synchronise.LockCustomer(locked);
            Assert.IsTrue(synchronise.UnlockCustomer(locked));
        }

        /// <summary>
        ///A test for UnviewCustomer
        ///</summary>
        [TestMethod()]
        public void UnviewCustomerTest()
        {
            SynchroniseClient synchronise = new SynchroniseClient(new InstanceContext(this));
            Viewing viewing = new Viewing() { ID = 1, SystemName = "Test", ViewUserName = Environment.UserName };
            synchronise.ViewCustomer(viewing);
            Assert.IsTrue(synchronise.UnviewCustomer(viewing));
        }


        /// <summary>
        ///A test for ViewCustomer
        ///</summary>
        [TestMethod()]
        public void ViewCustomerTest()
        {
            SynchroniseClient synchronise = new SynchroniseClient(new InstanceContext(this));
            Viewing viewing = new Viewing() { ID = 1, SystemName = "Test", ViewUserName = Environment.UserName };
            Assert.IsTrue(synchronise.ViewCustomer(viewing));
        }

        public void UpdateNotification()
        {
 
        }

        public void RefreshNotification()
        {

        }

        public void DebugNotification(string message)
        {

        }
    }
}
