﻿using UBM.MiddlewareSyncClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UBM.MiddlewareSyncClient.SyncClient;

namespace UBM.MiddlewareSyncClient.Test
{
    
    
    /// <summary>
    ///This is a test class for ClientTest and is intended
    ///to contain all ClientTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ClientTest
    {


        private TestContext testContextInstance;

        private Client client = new Client();

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            client.Connect();
        }
        
        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            client.Disconnect();
        }
        
        #endregion


        /// <summary>
        ///A test for Client Constructor
        ///</summary>
        [TestMethod()]
        public void ClientConstructorTest()
        {
            Client target = new Client();
            Assert.IsInstanceOfType(target, typeof(Client));
        }

   
        /// <summary>
        ///A test for GetSyncInfo
        ///</summary>
        [TestMethod()]
        public void GetSyncInfoTest()
        {
            Locked locked = new Locked() { ID = 1, SystemName = "Test", LockTime = DateTime.Now, LockUserName = Environment.UserName };
            client.LockCustomer(locked);
            Sync actual = client.GetSyncInfo();
            Assert.AreEqual(locked.ID, actual.LockedList[0].ID);
            Assert.AreEqual(locked.SystemName, actual.LockedList[0].SystemName);
            client.UnlockCustomer(locked);
        }

        /// <summary>
        /// A test for LockCustomer
        /// </summary>
        [TestMethod()]
        public void LockCustomerTest()
        {
            Locked locked = new Locked() {ID = 1, SystemName = "Test", LockTime = DateTime.Now, LockUserName = Environment.UserName };
            bool expected = true;
            bool actual;
            actual = client.LockCustomer(locked);
            Assert.AreEqual(expected, actual);
            client.UnlockCustomer(locked);
        }

        /// <summary>
        ///A test for UnlockCustomer
        ///</summary>
        [TestMethod()]
        public void UnlockCustomerTest()
        {
            Locked locked = new Locked() { ID = 1, SystemName = "Test", LockTime = DateTime.Now, LockUserName = Environment.UserName };
            bool expected = true;
            bool actual;
            client.LockCustomer(locked);
            actual = client.UnlockCustomer(locked);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ViewCustomer
        ///</summary>
        [TestMethod()]
        public void ViewCustomerTest()
        {
            Viewing viewing = new Viewing { ID = 1, SystemName = "Test", ViewUserName = Environment.UserName };
            bool expected = true;
            bool actual;
            actual = client.ViewCustomer(viewing);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UnViewCustomer
        ///</summary>
        [TestMethod()]
        public void UnviewCustomerTest()
        {
            Viewing viewing = new Viewing { ID = 1, SystemName = "Test", ViewUserName = Environment.UserName };
            bool expected = true;
            bool actual;
            client.ViewCustomer(viewing);
            actual = client.UnviewCustomer(viewing);
            Assert.AreEqual(expected, actual);
        }
    }
}
