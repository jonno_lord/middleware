﻿using System;
using System.ServiceModel;
using UBM.MiddlewareSyncRefresher.ClientSyncService;


namespace UBM.MiddlewareSyncRefresher
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single),
     CallbackBehavior(UseSynchronizationContext = false)]
    public class SyncRefresher : ISynchroniseCallback
    {
        private SynchroniseClient synchroniseClient;

        #region Private Methods

        private void Open()
        {
            try
            {
                synchroniseClient = new SynchroniseClient(new InstanceContext(new SyncRefresher()));
                synchroniseClient.Open();
            }
            catch (Exception)
            {
                SyncError();
            }
        }

        private void Close()
        {
            try
            {
                synchroniseClient.Close();
            }
            catch (Exception)
            {
                SyncError();
            }
        }

        private void SyncError()
        {
            if (synchroniseClient != null && synchroniseClient.State == CommunicationState.Faulted)
            {
                synchroniseClient.Abort();
            }
        }

        #endregion

        public static void RefreshClients()
        {
            SyncRefresher refresh = new SyncRefresher();
            refresh.Open();

            if(refresh.synchroniseClient.State == CommunicationState.Opened)
            {
                refresh.synchroniseClient.RefreshClients();
                refresh.Close();
            }

        }
        
        public static void SendProgressReport(string jobName, string stepDetail, double percentageComplete)
        {
            SyncRefresher refresh = new SyncRefresher();
            refresh.Open();

            if (refresh.synchroniseClient.State == CommunicationState.Opened)
            {
                refresh.synchroniseClient.SendProgressReport(jobName, stepDetail, percentageComplete);
                refresh.Close();
            }
        }

        #region Callbacks

        public void SynchroniseNotification()
        {
        }

        public void RefreshFromDatabaseNotification()
        {
        }

        public void ProgressReportNotification(string jobName, string stepDetail, double percentageComplete)
        {
        }

        public void DebugNotification(string message)
        {
        }

        #endregion
    }
}
