USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_F0101z2]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_F0101z2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_F0101z2] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_INS_F0101z2]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F0101Z2 f
		INNER JOIN Customers c ON c.id = f.szedtn
		WHERE Stage = 1	

	INSERT INTO dbo.F0101Z2
	(CustomerId,SZAC05,SZAC08,SZADD1,SZADD2,SZADD3,SZADDZ,SZALKY,SZAR1,SZAR2,SZAT1,
	SZCOUN,SZCTR,SZCTY1,SZEDBT,SZEDTN,SZEDUS,SZMLNM,SZPH1,SZPH2,SZRMK,SZTAX,SZURRF,SZTNAC)
	
	SELECT	
			c.id															AS CustomerId,
			LEFT(dbo.fn_AgencyCode(), 2)									AS SZAC05,
			LEFT(dbo.fn_IsoCountry(), 3)									AS SZAC08,
			LEFT(dbo.fn_AddressLine1(), 16)									AS SZADD1,
			c.AddressLine1													AS SZADD2,
			c.AddressLine2													AS SZADD3,
			c.Postcode														AS SZADDZ,
			LEFT(dbo.fn_LongAddressNumber(c.ClientNo), 16)					AS SZALKY,
			LEFT(dbo.fn_TelephoneType(c.phone), 3)							AS SZAR1,
			LEFT(dbo.fn_FaxType(c.Fax), 3)									AS SZAR2,
			LEFT(dbo.fn_SearchType(), 2)									AS SZAT1,
			c.County														AS SZCOUN,
			c.Country														AS SZCTR,
			c.City															AS SZCTY1,
			@BatchNumber													AS SZEDBT,
			c.id															AS SZEDTN,
			LEFT(dbo.fn_MiddlewareUser(), 7)								AS SZEDUS,
			c.CompanyName													AS SZMLNM,
			c.phone															AS SZPH1,
			c.fax															AS SZPH2,
			c.Email															AS SZRMK,
			c.TaxRegNo														AS SZTAX,
			c.ClientNo														AS SZURRF,
			dbo.fn_TransactionSetting()										AS SZTNAC
	FROM Customers c	 
	WHERE Stage = 1		
END
GO
