USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_F0911Z1]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SEL_F0911Z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_F0911Z1]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SEL_F0911Z1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SEL_F0911Z1] AS' 
END
GO
-- =============================================
-- Author:		<Author,,BMPatterson>
-- Create date: <Create Date,,23/05/2011>
-- Description:	<Description,,This stored proc gets the all the data from table F0911Z1 based on month and year>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SEL_F0911Z1]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Revenue_Period_Summary_Id, VNPN, VNFY  FROM CARSRR_F0911Z1
	WHERE VNPN = @month AND VNFY = @year
END
GO
