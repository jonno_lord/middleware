USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJDEFormat]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_TRA_AccountsReceivableToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJDEFormat]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TRA_AccountsReceivableToJDEFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJDEFormat] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	
SET NOCOUNT ON
      
      BEGIN TRANSACTION
      
			SELECT @BatchNumber = MAX(Batch_Number) + 1 FROM Invoices
			
			UPDATE Invoices
			SET Batch_number = @BatchNumber
			WHERE Stage = 1

            EXECUTE dbo.usp_CRE_TemporaryInvoices
            IF (@@ERROR <> 0) GOTO Failure
            
            EXECUTE dbo.usp_INS_TemporaryInvoices @BatchNumber
            IF (@@ERROR <> 0) GOTO Failure    
                        
            EXECUTE dbo.usp_UPD_TemporaryInvoices
            IF (@@ERROR <> 0) GOTO Failure
            
            -- Insert an additional row for any that have a payment type of 80.
            EXECUTE dbo.usp_INS_TemporaryInvoicesType80
            IF (@@ERROR <> 0) GOTO Failure         
            
            EXECUTE dbo.usp_UPD_TemporaryInvoicesAmounts
            IF (@@ERROR <> 0) GOTO Failure 
            
			EXECUTE dbo.usp_INS_F03B11z1 @BatchNumber 
            IF(@@ERROR <> 0) GOTO Failure
            
            EXECUTE dbo.usp_UPD_F03PaymentTerms @BatchNumber
            IF (@@ERROR <> 0) GOTO Failure 
            
            EXECUTE dbo.usp_UPD_F03LineNumbers  @BatchNumber
            IF (@@ERROR <> 0) GOTO Failure 
            
            EXECUTE dbo.usp_INS_F0911z1 @BatchNumber 
            IF(@@ERROR <> 0) GOTO Failure
                        
            EXECUTE dbo.usp_UPD_F09LineNumbers  @BatchNumber
            IF (@@ERROR <> 0) GOTO Failure 
             
            UPDATE AccountsReceivable 
            SET stage = 2, MiddlewareToMiddlewareIn = GETDATE()
            FROM AccountsReceivable ar
			INNER JOIN F03b11z1 f03 ON ( ar.F03b11z1id = f03.id)
			WHERE stage = 1
            
            UPDATE GeneralLedger 
            SET stage = 2, MiddlewareToMiddlewareIn = GETDATE()
            FROM GeneralLedger gl
			INNER JOIN F0911z1 f09 ON (gl.f0911z1id = f09.Id)
			WHERE stage = 1  
				
			EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoicesPaymentTerms 'MiddlewareCARS'
            IF (@@ERROR <> 0) GOTO Failure 
            
            EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoiceTaxCode @SystemName='MiddlewareCARS'
            IF (@@ERROR <> 0) GOTO Failure
				
			UPDATE Invoices SET STAGE = 3
				WHERE Stage = 1

      COMMIT TRANSACTION
      
      GOTO ENDFunction

Failure:
      ROLLBACK TRANSACTION

ENDFunction:
      -- return the amount of created rows for this batch
      SELECT COUNT(*) FROM F0911z1 WHERE VNEDBT = @BatchNumber

END

GO
