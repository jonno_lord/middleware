USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03LineNumbers]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_UPD_F03LineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03LineNumbers]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UPD_F03LineNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UPD_F03LineNumbers] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UPD_F03LineNumbers]
(@BatchNumber as varchar(10))
AS
BEGIN

	-- Used for creating the line numbers thru the cursor
	DECLARE @Line_Number AS INTEGER
	DECLARE @VJDOC AS FLOAT
	DECLARE @ID AS INTEGER

	-- Stores the tax area rate
	DECLARE @Last_VJDOC AS FLOAT

	-- Set up the last tax area rate
	SET @Last_VJDOC = ''
	SET @Line_Number = 0

	-- Create a cursor based from the temporary table loaded 
	DECLARE csrF03b11z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VJDOC, f03.[ID] FROM F03b11z1 f03
		INNER JOIN AccountsReceivable ar ON f03.AccountsReceivableId = ar.id
		WHERE ar.stage = 1
		ORDER BY VJDOC, VJSFX

	SET @Line_Number = 1

	-- Loop through this created cursor and update the transaction numbers
	OPEN csrF03b11z1
	FETCH NEXT FROM csrF03b11z1 INTO @VJDOC,@ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Increment the transaction number if the batch no, account or currency changed
		IF 	@Last_VJDOC <> @VJDOC 
			SET @Line_Number = 1
		
		ELSE
			SET @Line_Number = @Line_Number + 1

		-- Update the table with the next value
		UPDATE F03b11z1 SET VJEDLN = @Line_Number WHERE [ID] = @ID 

		-- store the previous batch details...
		SET @Last_VJDOC = @VJDOC

		-- Get the next entry
		FETCH NEXT FROM csrF03b11z1 INTO @VJDOC,@ID
	END

	CLOSE csrF03b11z1
	DEALLOCATE csrF03b11z1	
END


GO
