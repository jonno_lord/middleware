USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TemporaryInvoices]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_UPD_TemporaryInvoices]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TemporaryInvoices]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UPD_TemporaryInvoices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UPD_TemporaryInvoices] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UPD_TemporaryInvoices]
AS
BEGIN

	UPDATE inv
		SET [Sequence] = [Sequence] + 1
		FROM ##Invoices inv
		WHERE Payment_Terms = 80

END

GO
