USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ConfigurationSettings]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_DEL_ConfigurationSettings]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ConfigurationSettings]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DEL_ConfigurationSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DEL_ConfigurationSettings] AS' 
END
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Deletes configuration settings by keycategory and keyname match (deleting for each environment)
-- =============================================
ALTER PROCEDURE [dbo].[usp_DEL_ConfigurationSettings]
	-- Add the parameters for the stored procedure here
	@category AS NVARCHAR(50), 
	@name AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM ConfigurationSettings
	WHERE keyCategory = @category AND keyName = @name
END
GO
