USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_F0911z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_F0911z1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_F0911z1] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_INS_F0911z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN
   
    
INSERT INTO  F0911Z1
(VNEDUS,VNEDTN,VNEDLN,VNEDBT,VNDCT,VNDOC,VNDGJ,VNCO,VNOBJ,VNSBL,VNEXA,/*VNTXITM,VNEXR1,VNDLNID,*/VNDKC,VNCRRM,VNAA,VNSFX,VNSTAM,VNCTAM)
	
SELECT	'MW_CARS'																														AS VNEDUS,
		inv.Document_Number																												AS VNEDTN,
		0																																AS VNEDLN,
		'MW_CARS' + CONVERT(VARCHAR(8),ISNULL(inv.Batch_Number,0))																		AS VNEDBT,
		inv.Document_Type																												AS VNDCT,
		inv.Document_Number																												AS VNDOC,
		dbo.fn_DateToOracleJulian(GETDATE())																							AS VNDGJ,						
		CASE WHEN inv.Business_Unit like '%5000275%' THEN '16221' ELSE '10121' END														AS VNCO,
		CASE WHEN inv.Object_account IS NOT NULL THEN dbo.fn_Split(inv.Object_account,'.',1) ELSE NULL END								AS VNOBJ,
		RTRIM(REPLACE(CONVERT(varchar(6), inv.Plan_Date, 112), '.', '') )																AS VNSBL,  
		SUBSTRING(inv.Company_name,1,30)																								AS VNEXA,
		--CASE WHEN inv.Document_Type = 'C1' THEN dbo.fn_OriginalInvoiceNumber(MAX(inv.Order_Line_No)) ELSE NULL END						AS VNTXITM,
		--CASE WHEN inv.Document_Type = 'C1' THEN 'B1' ELSE NULL END																		AS VNEXR1,
		--CASE WHEN inv.Document_Type = 'C1' THEN dbo.fn_OriginalInvoiceLineNumber(MAX(inv.Order_Line_No)) ELSE NULL END					AS VNDLNID,
		dbo.fn_PlanDuration(inv.Plan_Duration)																							AS VNDKC,
		'D'																																AS VNCRRM,
		SUM (inv.Net_Amount * -1)																										AS VNAA,
		inv.SFX																															AS VNSFX,
		CASE WHEN inv.Currency = 'GBP' THEN SUM(inv.Gross_Amount * -1) - SUM(inv.Net_Amount * -1) ELSE 0 END							AS VNSTAM,
		CASE WHEN inv.Currency <> 'GBP' THEN SUM(inv.Gross_Amount * -1) - SUM(inv.Net_Amount * -1) ELSE 0 END							AS VNCTAM
		FROM ##Invoices inv
	INNER JOIN Invoices cinv ON (inv.Document_number = cinv.Document_Number and inv.Order_Line_No = cinv.Order_Line_No)
	INNER JOIN Customers c ON inv.JDE_account_no	= c.JdeAccountNo
GROUP BY inv.JDE_Account_No,inv.Company_name,inv.Currency,inv.JDE_tax_code,inv.Document_Number,inv.Order_No, inv.Batch_Number, inv.Document_Type,
inv.Invoice_Date,inv.Business_Unit,inv.Payment_Instrument,inv.Remark,inv.[Sequence],c.ShipTo,inv.SFX,inv.Object_Account,inv.Plan_Date,cinv.Plan_Duration,inv.Plan_Duration
ORDER BY inv.JDE_Account_No,inv.Company_name,inv.Currency,inv.JDE_tax_code,inv.Document_Number,inv.Order_No, inv.Batch_Number, inv.Document_Type,
inv.Invoice_Date,inv.Business_Unit,inv.Payment_Instrument,inv.Remark,inv.[Sequence],c.ShipTo,inv.SFX,inv.Object_Account,inv.Plan_Date,cinv.Plan_Duration,inv.Plan_Duration

INSERT INTO GeneralLedger (f0911z1id,stage)
SELECT f09.id AS f0911z1id,	1 AS stage FROM f0911z1 f09
INNER JOIN ##Invoices inv ON f09.VNEDTN = inv.document_number AND f09.VNSFX = inv.SFX
	
UPDATE F09 SET GeneralLedgerId = gl.id FROM
        F0911z1 f09
              INNER JOIN GeneralLedger gl on gl.F0911z1id = f09.id
              WHERE gl.Stage = 1

END

GO
