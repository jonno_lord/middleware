USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_InvoiceData]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_DEL_InvoiceData]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_InvoiceData]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DEL_InvoiceData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DEL_InvoiceData] AS' 
END
GO
-- =============================================
-- Author:		<Author,,BMPatterson>
-- Create date: <Create Date,,26/09/2011>
-- Description:	<Description,,Deletes invoices from the CARSRR_F0911Z1 tables based on date selected>
-- =============================================
ALTER PROCEDURE [dbo].[usp_DEL_InvoiceData]
	-- Add the parameters for the stored procedure here
	@month as int,
	@firstyearpart as int,
	@secondyearpart as int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM CARSRR_F0911Z1
	WHERE VNPN = @month AND VNCTRY = @firstyearpart AND VNFY = @secondyearpart
	
	
END
GO
