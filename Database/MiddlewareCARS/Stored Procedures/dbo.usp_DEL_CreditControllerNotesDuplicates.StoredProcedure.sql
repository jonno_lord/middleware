USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CreditControllerNotesDuplicates]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_DEL_CreditControllerNotesDuplicates]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CreditControllerNotesDuplicates]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DEL_CreditControllerNotesDuplicates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DEL_CreditControllerNotesDuplicates] AS' 
END
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 1st April 2011
-- Description:	Removes dupes based on the fact that the SYS_C0083316 constraint
-- in oracle will only allow one client number per date (update date). Multiple
-- notes cannot be backfed and will be destroyed.
-- =============================================
ALTER PROCEDURE [dbo].[usp_DEL_CreditControllerNotesDuplicates]
	-- Add the parameters for the stored procedure here
	@BatchNumber AS INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Remove duplicates from the Credit Controller notes table.
	DELETE ccn from CreditControllerNotes ccn
		INNER JOIN
		(
		-- Find entries duplicated based on CustomerNumber, JDEAccountNumber, UpdateDate
		SELECT CustomerNumber, JDEAccountNumber, UpdateDate from 
		CreditControllerNotes WHERE Stage = 1
		GROUP BY CustomerNumber, JDEAccountNumber, UpdateDate HAVING COUNT(*) > 1
		) qryDupes on qryDupes.CustomerNumber = ccn.CustomerNumber AND
					  qryDupes.JDEAccountNumber = ccn.JDEAccountNumber AND
					  qryDupes.UpdateDate = ccn.UpdateDate 

		-- Now find the entries that we want to keep, and join onto the listing of duplicates
		LEFT OUTER JOIN
			(SELECT MAX(ID) as Id from CreditControllerNotes ccn
			inner join 
			(
			SELECT CustomerNumber, JDEAccountNumber, UpdateDate from 
			CreditControllerNotes WHERE Stage = 1
			GROUP BY CustomerNumber, JDEAccountNumber, UpdateDate HAVING COUNT(*) > 1
			) qryDupes on qryDupes.CustomerNumber = ccn.CustomerNumber AND
						  qryDupes.JDEAccountNumber = ccn.JDEAccountNumber AND
						  qryDupes.UpdateDate = ccn.UpdateDate 
			GROUP BY ccn.CustomerNumber, ccn.JDEAccountNumber, ccn.UpdateDate
			) qryDontKeep On ccn.id = qryDontKeep.Id
		WHERE qryDontKeep.Id IS NULL OR ccn.CustomerNumber IS NULL

END
GO
