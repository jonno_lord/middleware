USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_InvoiceDataold]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_DEL_InvoiceDataold]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_InvoiceDataold]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DEL_InvoiceDataold]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DEL_InvoiceDataold] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_DEL_InvoiceDataold] 
	-- Add the parameters for the stored procedure here
	@month int,
	@year as int,
	@sysname as varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @sysname = 'CARS'
        -- Delete all previously split invoice entries from [F0911Z1] table in MiddlewareIPSOP
        -- Delete the annual supscription records from the Revenue_Period_Summary table that correspond
        -- to the Order_Number. 
		Begin
			declare @DeletedInvoiceIDs table (id int);
		     
			DELETE t1
			output deleted.VNDOC into @DeletedInvoiceIDs  
			FROM CARSRR_F0911Z1 as t1 
			INNER JOIN Revenue_Period_Summary as t2 ON Document_Number = t1.VNDOC
			WHERE Period_Month = @month AND Period_Year = @year;
			
			DELETE t2
			FROM Revenue_Period_Summary as t2 
			JOIN @DeletedInvoiceIDs del
			on del.id = t2.Document_Number
		end
	ELSE
	    -- Delete all previous invoice entries from JDE [F0911Z1] table.
		Begin
			DELETE t1 FROM CARSRR_F0911Z1 t1
			INNER JOIN Revenue_Period_Summary ON Document_Number = t1.VNDOC
			WHERE Period_Month = @month AND Period_Year = @year
		end
END
GO
