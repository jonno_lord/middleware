USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToCARSFormat]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_TRA_CustomersToCARSFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToCARSFormat]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TRA_CustomersToCARSFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_TRA_CustomersToCARSFormat] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_TRA_CustomersToCARSFormat]
	@BatchNumber as int = 0
AS
BEGIN

	SET NOCOUNT ON

	;with cte_dupes as
	(
	select ROW_NUMBER() over (partition by q1alky order by q1alky) as dupe, * from F550101
	)
	delete from cte_dupes 
	where dupe > 1
	
	UPDATE Customers SET Stage = 5, MiddlewareToMiddlewareOut = GETDATE() 
		FROM F550101 F55
		INNER JOIN Customers C ON F55.Q1URRF = C.ClientNo
		WHERE Stage > 3

END

GO
