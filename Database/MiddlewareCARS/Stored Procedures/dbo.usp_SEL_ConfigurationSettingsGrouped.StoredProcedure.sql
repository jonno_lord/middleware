USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ConfigurationSettingsGrouped]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SEL_ConfigurationSettingsGrouped]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ConfigurationSettingsGrouped]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SEL_ConfigurationSettingsGrouped]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SEL_ConfigurationSettingsGrouped] AS' 
END
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
ALTER PROCEDURE [dbo].[usp_SEL_ConfigurationSettingsGrouped] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT keyCategory, keyName FROM ConfigurationSettings
END
GO
