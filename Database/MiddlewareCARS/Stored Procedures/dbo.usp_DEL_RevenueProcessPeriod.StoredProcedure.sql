USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_RevenueProcessPeriod]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_DEL_RevenueProcessPeriod]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_RevenueProcessPeriod]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DEL_RevenueProcessPeriod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DEL_RevenueProcessPeriod] AS' 
END
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,29/06/2011>
-- Description:	<Description,,Deletes processed records based on month and year>
-- =============================================
ALTER PROCEDURE [dbo].[usp_DEL_RevenueProcessPeriod]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int,
	@sysname as varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Revenue_Process_Period
	WHERE Period_Month = @month AND period_year = @year;
END
GO
