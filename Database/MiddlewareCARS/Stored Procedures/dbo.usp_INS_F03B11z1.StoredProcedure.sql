USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03B11z1]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_F03B11z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03B11z1]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_F03B11z1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_F03B11z1] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_INS_F03B11z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	INSERT INTO F03b11z1 
	(VJEDUS,VJEDTN,VJEDBT,VJDOC,VJDCT,VJAN8,VJDIVJ,VJCO,VJAG,VJSTAM,VJCRCD,VJTXA1,VJMCU,VJPTC,VJVINV,VJRMK,VJALPH,VJRYIN,VJTORG,VJEDLN,VJCRRM,VJODOC,VJSFX)

	SELECT  'MW_CARS'																													AS VJEDUS,
			inv.Document_Number																											AS VJEDTN,
			'MW_CARS' + CONVERT(VARCHAR(8),inv.Batch_Number)																			AS VJEDBT,
			inv.Document_Number																											AS VJDOC,
			inv.Document_Type																											AS VJDCT,
			CONVERT(VARCHAR,inv.JDE_account_no )+ '-' + ISNULL(c.ShipTo,'')															    AS VJAN8,
			dbo.fn_DateToOracleJulian(inv.Invoice_Date)																					AS VJDIVJ,
			CASE WHEN inv.Business_Unit like '%5000275%' THEN '16221' ELSE '10121' END													AS VJCO,
			SUM (inv.Gross_Amount)																									    AS VJAG,	
			SUM(inv.Tax_Amount)																										    AS VJSTAM,	
			inv.Currency																												AS VJCRCD,
			CASE WHEN inv.JDE_tax_code = 'OUTSIDE' THEN 'GBOUTSIDE' ELSE inv.JDE_tax_code END											AS VJTXA1,	
			inv.Business_Unit																											AS VJMCU,
			CASE WHEN inv.[Sequence] = 1 THEN 'IMMEDIATE' ELSE 'XXX' END																AS VJPTC,
			inv.Batch_Number																											AS VJVINV,
			inv.Remark																													AS VJRMK,
			inv.Company_name																											AS VJALPH,	
			inv.Payment_Instrument																										AS VJRYIN,	
			'MW_CARS'																													AS VJTORG,
			1																															AS VJEDLN,
			'D'																															AS VJEDLN,
			inv.Order_No																												AS VJODOC,
			inv.SFX																														AS VJSFX
FROM ##Invoices inv
INNER JOIN Invoices cinv ON (inv.Document_number = cinv.Document_Number and inv.Order_Line_No = cinv.Order_Line_No)
INNER JOIN Customers c ON inv.JDE_account_no	= c.JdeAccountNo
GROUP BY inv.JDE_Account_No,inv.Company_name,inv.Currency,inv.JDE_tax_code,inv.Document_Number,inv.Order_No, inv.Batch_Number, inv.Document_Type,
inv.Invoice_Date,inv.Business_Unit,inv.Payment_Instrument,inv.Remark,inv.[Sequence],c.ShipTo,inv.SFX,inv.Object_Account,inv.Plan_Date,cinv.Plan_Duration,inv.Plan_Duration
ORDER BY inv.JDE_Account_No,inv.Company_name,inv.Currency,inv.JDE_tax_code,inv.Document_Number,inv.Order_No, inv.Batch_Number, inv.Document_Type,
inv.Invoice_Date,inv.Business_Unit,inv.Payment_Instrument,inv.Remark,inv.[Sequence],c.ShipTo,inv.SFX,inv.Object_Account,inv.Plan_Date,cinv.Plan_Duration,inv.Plan_Duration

INSERT INTO AccountsReceivable (F03b11z1id,stage)
SELECT DISTINCT f03.id AS F03b11z1id,1 AS Stage FROM F03b11z1 f03
INNER JOIN ##Invoices inv ON f03.VJEDTN = inv.Document_Number AND f03.VJODOC = inv.order_no AND CONVERT(INT,f03.VJMCU) = CONVERT(INT,inv.Business_Unit)
	
UPDATE F03 SET AccountsReceivableId = ar.id FROM
	  F03B11Z1 f03
			INNER JOIN AccountsReceivable ar on ar.F03b11z1id = f03.id
			WHERE ar.Stage = 1

END


GO
