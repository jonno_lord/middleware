USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ConfigurationSettingsEnvironment]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SAV_ConfigurationSettingsEnvironment]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ConfigurationSettingsEnvironment]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SAV_ConfigurationSettingsEnvironment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SAV_ConfigurationSettingsEnvironment] AS' 
END
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Inserts or Updates
-- =============================================
ALTER PROCEDURE [dbo].[usp_SAV_ConfigurationSettingsEnvironment]
	-- Add the parameters for the stored procedure here
	@idDev AS INT,
	@idUat AS INT,
	@idProd AS INT,
	@Category AS NVARCHAR(50),
	@Name AS NVARCHAR(50),
	
	@keyValueDev AS NVARCHAR(2048),
	@keyValueUat AS NVARCHAR(2048),
	@keyValueProd AS NVARCHAR(2048),
	@keyDataTypeDev AS NVARCHAR(50),
	@keyDataTypeUat AS NVARCHAR(50),
	@keyDataTypeProd AS NVARCHAR(50),
	@EnvironmentNameDev AS NVARCHAR(50),
	@EnvironmentNameUat AS NVARCHAR(50),
	@EnvironmentNameProd AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM ConfigurationSettings WHERE id = @idDev)
	BEGIN
		UPDATE ConfigurationSettings SET keyValue = @keyValueDev, keyDataType = @keyDataTypeDev
		WHERE id = @idDev
	END
	
	IF EXISTS (SELECT * FROM ConfigurationSettings WHERE id = @idUat)
	BEGIN	
		UPDATE ConfigurationSettings SET keyValue = @keyValueUat, keyDataType = @keyDataTypeUat
		WHERE id = @idUat
	END
	
	IF EXISTS (SELECT * FROM ConfigurationSettings WHERE id = @idProd)
	BEGIN
		UPDATE ConfigurationSettings SET keyValue = @keyValueProd, keyDataType = @keyDataTypeProd
		WHERE id = @idProd
	END
	
	ELSE
	
	BEGIN
		INSERT INTO ConfigurationSettings(keyCategory, keyName, keyValue, keyDataType, EnvironmentName)
		VALUES (@category, @Name, @keyValueDev, @keyDataTypeDev, @EnvironmentNameDev)
		
		INSERT INTO ConfigurationSettings(keyCategory, keyName, keyValue, keyDataType, EnvironmentName)
		VALUES (@category, @Name, @keyValueUat, @keyDataTypeUat, @EnvironmentNameUat)
		
		INSERT INTO ConfigurationSettings(keyCategory, keyName, keyValue, keyDataType, EnvironmentName)
		VALUES (@category, @Name, @keyValueProd, @keyDataTypeProd, @EnvironmentNameProd)
	END
END
GO
