USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_TemporaryInvoicesType80]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_TemporaryInvoicesType80]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_TemporaryInvoicesType80]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_TemporaryInvoicesType80]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_TemporaryInvoicesType80] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_INS_TemporaryInvoicesType80]
AS
BEGIN

	DECLARE @BatchNo AS VARCHAR(10)
	SELECT @BatchNo = MAX(Batch_Number) + 1 FROM Invoices

	--Insert one more row for Inovices of Payment Terms 80
	INSERT INTO ##Invoices
	( Batch_number, Document_Type, Document_Number,	Order_Line_No, JDE_Account_No, Invoice_date,
	  Gross_Amount, Net_Amount, Tax_Amount, Company_name, Order_No, Payment_terms, Payment_Instrument,
	  Business_Unit, Object_account, JDE_tax_code, Currency, Remark, Plan_Date, Plan_Duration,
	  Product, Tax_Rate_Description, [Sequence]	)
	SELECT	ISNULL(Batch_Number,@BatchNo),
			Document_Type,
			Document_Number,
			Order_Line_No,
			JDE_Account_No,
			Invoice_date,
			Gross_Amount,
			Net_Amount,
			Tax_Amount,
			Company_name,
			Order_No,
			Payment_terms,
			Payment_Instrument,
			Business_Unit,
			Object_account,
			JDE_tax_code,
			Currency,
			Remark,
			Plan_Date,
			Plan_Duration,
			Product,
			Tax_Rate_Description,
			[Sequence] + 1 'Sequence'
	FROM ##Invoices
	WHERE Payment_Terms = 80

END

GO
