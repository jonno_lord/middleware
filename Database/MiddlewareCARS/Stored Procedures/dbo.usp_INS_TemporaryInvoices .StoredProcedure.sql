USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_TemporaryInvoices ]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_TemporaryInvoices ]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_TemporaryInvoices ]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_TemporaryInvoices ]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_TemporaryInvoices ] AS' 
END
GO

ALTER PROCEDURE [dbo].[usp_INS_TemporaryInvoices ]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	--Time to saddle up boys, there's a new coboy in town. Yeeeeehaaaaaa.
	UPDATE i
	SET i.SFX = qrySFX.SFX
	FROM Invoices i
	INNER JOIN Invoices cinv ON (i.Document_number = cinv.Document_Number and i.Order_Line_No = cinv.Order_Line_No)
	LEFT JOIN Customers c on c.JdeAccountNo = i.JDE_Account_No
	INNER JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY inv.Document_Number ORDER BY inv.Document_Number) as SFX,inv.JDE_Account_No,inv.Company_name,inv.Currency,inv.JDE_tax_code,
	inv.Document_Number,inv.Order_No,inv.Batch_Number,inv.Document_Type,inv.Invoice_Date,inv.Business_Unit,inv.Payment_Instrument,inv.Remark,c.ShipTo,
	inv.Object_Account,inv.Plan_Date,cinv.Plan_Duration as cPlanDuration,inv.Plan_Duration FROM Invoices inv
	INNER JOIN Invoices cinv ON (inv.Document_number = cinv.Document_Number and inv.Order_Line_No = cinv.Order_Line_No)
	LEFT JOIN Customers c ON inv.JDE_account_no	= c.JdeAccountNo
	WHERE inv.Net_Amount <> 0 AND inv.Stage = 1	
	GROUP BY inv.JDE_Account_No,inv.Company_name,inv.Currency,inv.JDE_tax_code,inv.Document_Number,inv.Order_No, inv.Batch_Number, inv.Document_Type,
	inv.Invoice_Date,inv.Business_Unit,inv.Payment_Instrument,inv.Remark,c.ShipTo,inv.Object_Account,inv.Plan_Date,
	cinv.Plan_Duration,inv.Plan_Duration) as qrySFX
	ON ISNULL(i.JDE_Account_No,'') = ISNULL(qrySFX.JDE_Account_No,'') and
	ISNULL(i.Company_Name,'') = ISNULL(qrySFX.Company_Name,'') and 
	ISNULL(i.Currency,'') = ISNULL(qrySFX.Currency,'') and 
	ISNULL(i.JDE_Tax_Code,'') = ISNULL(qrySFX.JDE_Tax_Code,'') and 
	ISNULL(i.Document_Number,'') = ISNULL(qrySFX.Document_Number,'') and 
	ISNULL(i.Order_No,'') = ISNULL(qrySFX.Order_No,'') and
	ISNULL(i.Batch_Number,'') = ISNULL(qrySFX.Batch_number,'') and
	ISNULL(i.Document_Type,'') = ISNULL(qrySFX.Document_Type,'') and 
	ISNULL(i.Invoice_date,'') = ISNULL(qrySFX.Invoice_date,'') and 
	ISNULL(i.Business_Unit,'') = ISNULL(qrySFX.Business_Unit,'') and
	ISNULL(i.Payment_Instrument,'') = ISNULL(qrySFX.Payment_Instrument,'') and
	ISNULL(i.Remark,'') = ISNULL(qrySFX.Remark,'') and 
	ISNULL(c.ShipTo,'') = ISNULL(qrySFX.ShipTo,'') and
	ISNULL(i.Object_Account,'') = ISNULL(qrySFX.Object_Account,'') and 
	ISNULL(i.Plan_Date,'') = ISNULL(qrySFX.Plan_Date,'') and 
	ISNULL(cinv.Plan_Duration,'') = ISNULL(qrySFX.cPlanDuration,'') and 
	ISNULL(i.Plan_Duration,'') = ISNULL(qrySFX.Plan_Duration,'') 
	WHERE i.Net_Amount <> 0 AND i.Stage = 1
	
	--Format the SFX (e.g. 001)
	UPDATE i
	SET SFX = REPLICATE('0', 3 - LEN(SFX)) + SFX
	FROM Invoices i
	WHERE i.Net_Amount <> 0 
	AND i.Stage = 1
	
	
	INSERT INTO ##Invoices 
	( Id, Batch_Number, Document_Type, Document_Number, Order_Line_No, JDE_Account_No, Invoice_date,
	  Gross_Amount, Net_Amount, Tax_Amount, Company_Name, Order_No, Payment_Terms, Payment_Instrument,
	  Business_Unit, Object_Account, JDE_tax_Code, Currency, Remark, Plan_Date, Plan_Duration, Product,
	  Tax_Rate_Description, [Sequence], [SFX])
	SELECT	Id,
			--@BatchNumber AS 
			ISNULL(Batch_number,@BatchNumber),
			Document_Type,
			Document_Number,
			Order_Line_No,
			JDE_Account_No,
			Invoice_date,
			Gross_Amount,
			Net_Amount,
			Tax_Amount,
			Company_Name,
			Order_No,
			Payment_Terms,
			Payment_Instrument,
			Business_Unit,
			Object_Account,
			JDE_tax_Code,
			Currency,
			Remark,
			Plan_Date,
			Plan_Duration,
			Product,
			Tax_Rate_Description,
			0 AS [Sequence],
			SFX
	FROM Invoices
	WHERE Net_Amount <> 0
	AND stage = 1
END
GO
