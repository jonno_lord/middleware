USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TemporaryInvoicesAmounts]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_UPD_TemporaryInvoicesAmounts]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TemporaryInvoicesAmounts]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UPD_TemporaryInvoicesAmounts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UPD_TemporaryInvoicesAmounts] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UPD_TemporaryInvoicesAmounts]
AS
BEGIN

	-- Update ##Invoices table for Invoices with Payment terms of 80.
	UPDATE ##Invoices
		SET Gross_Amount = Gross_Amount / CONVERT(INT,Plan_Duration) * 2 * 100,
			Net_Amount = Net_Amount / CONVERT(INT,Plan_Duration) * 2 * 100,
			Tax_Amount = Tax_Amount / CONVERT(INT,Plan_Duration) * 2 * 100,
			Plan_Duration = '0100'
		  WHERE Payment_terms = 80 AND [Sequence] = 1

	UPDATE ##Invoices
		SET Gross_Amount = Gross_Amount - (Gross_Amount / CONVERT(INT,Plan_Duration) * 2 * 100),
			Net_Amount = Net_Amount - (Net_Amount / CONVERT(INT,Plan_Duration) * 2 * 100),
			Tax_Amount = Tax_Amount - (Tax_Amount / CONVERT(INT,Plan_Duration) * 2 * 100),
			Plan_Duration = CONVERT(INT,Plan_Duration) - 200
		  WHERE Payment_terms = 80 AND [Sequence] = 2

	UPDATE ##Invoices
	SET Plan_Duration = CASE WHEN LEN(Plan_Duration) = 3 
					 THEN '0'+Plan_Duration
					 ELSE Plan_Duration END
	WHERE [Sequence] <> -1
	
END

GO
