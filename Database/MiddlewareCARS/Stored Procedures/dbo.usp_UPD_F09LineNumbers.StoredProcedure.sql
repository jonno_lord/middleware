USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F09LineNumbers]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_UPD_F09LineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F09LineNumbers]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UPD_F09LineNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UPD_F09LineNumbers] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UPD_F09LineNumbers]
(@BatchNumber as varchar(10))
AS
BEGIN

	-- Used for creating the transaction and line numbering details in the cursor
	DECLARE @VNEDTN AS INTEGER
	DECLARE @Line_Number AS INTEGER
	DECLARE @ID AS INTEGER

	-- Stores the previous batch details
	DECLARE @last_VNEDTN AS VARCHAR(100)

	-- Set up the last details so that
	-- they will break on the first occurence of entering the
	-- loop below...
	SET @last_VNEDTN = ''

	-- begin the line number
	SET @Line_Number = 0

	-- Create a cursor based from the temporary table loaded  
	DECLARE csrF0911z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VNEDTN, f09.[ID] FROM F0911z1 f09
		INNER JOIN GeneralLedger gl ON f09.GeneralLedgerId = gl.id
		WHERE gl.stage = 1
		ORDER BY VNEDTN, VNSFX

	-- Loop through this created cursor and update the transaction numbers
	OPEN csrF0911z1
	FETCH NEXT FROM csrF0911z1 INTO @VNEDTN, @ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Increment the transaction number if the batch no got changed out
		IF 	@last_VNEDTN <> @VNEDTN 
			SET @Line_Number = 1
		
		ELSE
			SET @Line_Number = @Line_Number + 1

		-- Update the table with the next value
		UPDATE F0911z1 SET VNEDLN = @Line_Number WHERE [ID] = @ID

		-- store the previous batch details...
		SET @last_VNEDTN = @VNEDTN

		-- Get the next entry
		FETCH NEXT FROM csrF0911z1 INTO @VNEDTN, @ID
	END

	CLOSE csrF0911z1
	DEALLOCATE csrF0911z1

END

GO
