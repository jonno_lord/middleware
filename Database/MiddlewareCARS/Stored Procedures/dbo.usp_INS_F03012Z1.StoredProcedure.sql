USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012Z1]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_F03012Z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012Z1]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_F03012Z1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_F03012Z1] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_INS_F03012Z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F03012Z1 f
		INNER JOIN Customers c ON c.id = f.VOEDTN
		WHERE Stage = 1		

	INSERT INTO dbo.F03012Z1
	(CustomerId,VOACL,VOCRCD,VOEDBT,VOEDLN,VOEDTN,VOEDUS,VOPOPN,VOSTMT,VOTNAC,VOTRAR,VOTXA1,VOALKY,VOBADT)
	SELECT	Id															AS CustomerId,
			LEFT(dbo.fn_CreditLimit(),5)								AS VOACL,
			LEFT(dbo.fn_PreferredBillingCurrency(),3)					AS VOCRCD,
			LEFT(@BatchNumber, 15)										AS VOEDBT,
			LEFT(dbo.fn_LineNumber(),1)									AS VOEDLN,
			c.id														AS VOEDTN,
			LEFT(dbo.fn_MiddlewareUser(), 7)							AS VOEDUS,
			c.AccountManager											AS VOPOPN,
			LEFT(dbo.fn_DelinqencyNotice(),1)							AS VOSTMT,
			LEFT(dbo.fn_TransactionSetting(), 2)						AS VOTNAC,
			c.PayPolNo													AS VOTRAR,
			LEFT(dbo.fn_TaxExemptCodeDesc(), 5)							AS VOTXA1,
			LEFT(dbo.fn_LongAddressNumber(c.ClientNo), 16)				AS VOALKY,
			'X'															AS VOBADT
	FROM Customers c	 
	WHERE Stage = 1		
END
GO
