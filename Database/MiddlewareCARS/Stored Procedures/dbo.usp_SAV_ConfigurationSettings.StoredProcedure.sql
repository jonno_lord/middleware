USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ConfigurationSettings]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SAV_ConfigurationSettings]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ConfigurationSettings]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SAV_ConfigurationSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SAV_ConfigurationSettings] AS' 
END
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Inserts or Updates a specified configuration setting
-- =============================================
ALTER PROCEDURE [dbo].[usp_SAV_ConfigurationSettings] 
	-- Add the parameters for the stored procedure here
	@id AS INT,
	@keyName AS NVARCHAR(50),
	@keyValue AS NVARCHAR(2048),
	@keyDataType AS NVARCHAR(50),
	@EnvironmentName AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM ConfigurationSettings WHERE id = @id)
	BEGIN
		UPDATE ConfigurationSettings SET keyName = @keyName, keyValue = @keyValue, keyDataType = @keyDataType, EnvironmentName = @EnvironmentName
		WHERE id = @id 
	END
	ELSE
	BEGIN
		INSERT INTO ConfigurationSettings(keyName, keyValue, keyDataType, EnvironmentName)
		VALUES (@keyName, @keyValue, @keyDataType, @EnvironmentName)
	END
END
GO
