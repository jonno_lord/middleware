USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenueRecognition]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_RevenueRecognition]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenueRecognition]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_RevenueRecognition]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_RevenueRecognition] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_INS_RevenueRecognition]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int,
	@username as varchar(50),
	@Committed_Date as datetime
	
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert INTO Revenue_Recognition  (Period_Month,Period_Year,Run_Date,Run_By, Committed_Date)
	VALUES (@month, @year,GETDATE(),@username,@Committed_Date)
	
END
GO
