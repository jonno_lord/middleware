USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditControllerNotes]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_INS_CreditControllerNotes]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditControllerNotes]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_INS_CreditControllerNotes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_INS_CreditControllerNotes] AS' 
END
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_INS_CreditControllerNotes] 
	-- Add the parameters for the stored procedure here
	(@BatchNumber AS INT =0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.CreditControllerNotes
		(Stage, MiddlewareToMiddlewareOut, BatchNumber, SearchType,JDEAccountNumber,CreatedDate,CustomerNumber,JDEActivtyType,JDEActivtyTypeDescription,NoteNumber,JDEUserName,NoteCategory,NoteText,UpdateDate,UpdateTime, OrderNumber)
		SELECT		
			1 AS Stage,
			GETDATE() as MiddlewareToMiddlewareOut,
			@BatchNumber as BatchNumber,
			DNAT1 AS SearchType, 
			DNAN8 AS JDEAccountNumber,
			dbo.fn_JuliantoDate(DNDTI) AS CreatedDate,
			LTRIM(RTRIM(REPLACE(c.ClientNo,'#','-'))) AS CustomerNumber,
			DNAIT AS JDEActivtyType,
			DNDL01 AS JDEActivtyTypeDescription,
			DNAVID AS NoteNumber,
			DNEUSR AS JDEUserName,
			CASE WHEN ISNUMERIC(DNRMK) <> 1 THEN SUBSTRING(DNRMK,1,CHARINDEX('_',DNRMK,1)-1) ELSE DNRMK END AS NoteCategory,
			DNVAR1 AS NoteText,
			dbo.fn_JuliantoDate(DNUPMJ) as UpdateDate, 
			DNUPMT AS UpdateTime ,
			
			/*CASE WHEN ISNUMERIC(SUBSTRING(DNRMK,1,1)) = 1 THEN 
			
				CASE WHEN CHARINDEX('/', DNRMK) > 0 AND CHARINDEX('/', DNRMK) < 30 THEN
					SUBSTRING(DNRMK, CHARINDEX('/', DNRMK) + 1, 
					CHARINDEX('/', DNRMK) + 1 - CHARINDEX(' ', SUBSTRING(DNRMK, CHARINDEX('/', DNRMK) + 1,255))) 
				ELSE '' END 
			ELSE
			
				''
			END AS OrderNumber*/
			DNRMK AS OrderNumber
			
			FROM F56008 f56
			
			
		LEFT OUTER JOIN dbo.CreditControllerNotes CCN 
		ON (f56.DNAVID = CCN.NoteNumber AND dbo.fn_JuliantoDate(DNUPMJ) = UpdateDate)
		INNER JOIN Customers c On f56.DNAN8 = c.JdeAccountNo
		WHERE CCN.id IS NULL
		
	-- Remove duplicates
	--EXECUTE usp_DEL_CreditControllerNotesDuplicates
		
	SELECT COUNT(*) FROM dbo.CreditControllerNotes WHERE Stage = 1 AND BatchNumber = @BatchNumber 
	
END

GO
