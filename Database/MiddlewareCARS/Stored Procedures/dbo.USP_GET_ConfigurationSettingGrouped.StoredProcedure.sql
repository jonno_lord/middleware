USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ConfigurationSettingGrouped]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[USP_GET_ConfigurationSettingGrouped]
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ConfigurationSettingGrouped]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GET_ConfigurationSettingGrouped]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[USP_GET_ConfigurationSettingGrouped] AS' 
END
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 4th April 2011
-- Description:	Selects configuration settings across multiple environments (DEV, UAT, Production)
-- =============================================
ALTER PROCEDURE [dbo].[USP_GET_ConfigurationSettingGrouped] 
	-- Add the parameters for the stored procedure here
	@category AS NVARCHAR(50),
	@name AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ConfigurationSettings
	WHERE keyCategory = @category AND keyName = @name
END
GO
