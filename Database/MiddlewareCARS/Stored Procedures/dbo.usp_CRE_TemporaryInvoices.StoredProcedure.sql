USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_TemporaryInvoices]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_CRE_TemporaryInvoices]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_TemporaryInvoices]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CRE_TemporaryInvoices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_CRE_TemporaryInvoices] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_CRE_TemporaryInvoices]
AS
BEGIN
     
IF OBJECT_ID('tempdb..##Invoices') IS NOT NULL
	DROP TABLE ##Invoices

CREATE TABLE [dbo].[##Invoices](	
									[Id] [int] NULL,
									[Batch_number] [int] NULL,
									[Document_Type] [varchar](50) NULL,
									[Document_Number] [varchar](50) NULL,
									[Order_Line_No] [varchar](50) NULL,
									[JDE_Account_No] [int] NULL,
									[Invoice_date] [datetime] NULL,
									[Gross_Amount] [numeric](19, 2) NULL,
									[Net_Amount] [numeric](19, 2) NULL,
									[Tax_Amount] [numeric](19, 2) NULL,
									[Company_name] [varchar](50) NULL,
									[Order_No] [varchar](50) NULL,
									[Payment_terms] [varchar](2) NULL,
									[Payment_Instrument] [varchar](50) NULL,
									[Business_Unit] [varchar](250) NULL,
									[Object_account] [varchar](50) NULL,
									[JDE_tax_code] [varchar](50) NULL,
									[Currency] [varchar](50) NULL,
									[Remark] [varchar](50) NULL,
									[Plan_Date] [datetime] NULL,
									[Plan_Duration] [varchar](50) NULL,
									[Product] [varchar](50) NULL,
									[Tax_Rate_Description] [varchar](50) NULL,
									[Sequence][int] NULL,
									[SFX] [char](3) NULL
								)     
END

GO
