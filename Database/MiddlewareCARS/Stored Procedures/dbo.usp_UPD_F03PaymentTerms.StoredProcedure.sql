USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03PaymentTerms]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_UPD_F03PaymentTerms]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03PaymentTerms]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UPD_F03PaymentTerms]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UPD_F03PaymentTerms] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UPD_F03PaymentTerms]
(@BatchNumber as varchar(10))
AS
BEGIN

		UPDATE F03
			SET VJPTC =  CPay.JDEPaymentTerms
		  FROM ##Invoices inv
			INNER JOIN F03b11z1 F03 ON (  F03.VJDOC=inv.Document_Number AND F03.VJDCT = inv.Document_Type   AND SUBSTRING(F03.VJAN8,1,8) = inv.JDE_account_no  AND  F03.VJODOC = inv.Order_No AND F03.VJVINV = @BatchNumber)
			INNER JOIN CARS_Payment_Terms CPay ON ( CPay.PayPolNo=inv.Payment_terms  COLLATE SQL_Latin1_General_CP1_CI_AS  AND CPay.PlanDuration=CONVERT(INT,inv.Plan_Duration COLLATE SQL_Latin1_General_CP1_CI_AS)/100 )
		  WHERE VJPTC <> 'AIM' 
	
END

GO
