USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Revenue_Recognition_Summary]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SEL_Revenue_Recognition_Summary]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Revenue_Recognition_Summary]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SEL_Revenue_Recognition_Summary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SEL_Revenue_Recognition_Summary] AS' 
END
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,18/05/2011>
-- Description:	<Description,,Selects all the records from Revenue Recognition Summary and display >
-- =============================================
ALTER PROCEDURE [dbo].[usp_SEL_Revenue_Recognition_Summary]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Revenue_Period_Summary;
END
GO
