USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJDEFormat]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_TRA_CustomersToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJDEFormat]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TRA_CustomersToJDEFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_TRA_CustomersToJDEFormat] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_TRA_CustomersToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	
SET NOCOUNT ON
      
      BEGIN TRANSACTION

        EXECUTE dbo.usp_INS_F0101z2 @BatchNumber
        IF(@@ERROR <> 0) GOTO Failure
        
        EXECUTE dbo.usp_INS_F03012z1 @BatchNumber     
        IF(@@ERROR <> 0) GOTO Failure
        
        --EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerPaymentTerms @SystemName = 'MiddlewareCARS'
        --IF(@@ERROR <> 0) GOTO Failure     
        
        --EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerTaxCode @SystemName = 'MiddlewareCARS'
        --IF(@@ERROR <> 0) GOTO Failure
        
        -- Mark all customers as ready for the next stage
		UPDATE C SET Stage = 2, MiddlewaretoMiddlewareIn = GETDATE() 
			FROM Customers C INNER JOIN F0101Z2 F ON (F.SZEDTN = C.ID)
			WHERE Stage = 1 

		IF(@@ERROR <> 0) GOTO Failure
		
      COMMIT TRANSACTION
      
      GOTO ENDFunction

Failure:
      ROLLBACK TRANSACTION

ENDFunction:
      -- return the amount of created rows for this batch
      SELECT COUNT(*) FROM F0911z1 WHERE VNEDBT = @BatchNumber

END

GO
