USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_RevenuePeriodSummary]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SEL_RevenuePeriodSummary]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_RevenuePeriodSummary]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SEL_RevenuePeriodSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SEL_RevenuePeriodSummary] AS' 
END
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,19/05/2011>
-- Description:	<Description,,Select all recors from Revenu_period_Summary table based on month and year>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SEL_RevenuePeriodSummary]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Revenue_Period_Summary
	WHERE Period_Month = @month AND Period_Year = @year;
END
GO
