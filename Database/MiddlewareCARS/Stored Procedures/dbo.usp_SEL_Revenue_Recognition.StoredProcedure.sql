USE [MiddlewareCARS]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Revenue_Recognition]    Script Date: 09/04/2018 16:33:11 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_SEL_Revenue_Recognition]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Revenue_Recognition]    Script Date: 09/04/2018 16:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SEL_Revenue_Recognition]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SEL_Revenue_Recognition] AS' 
END
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,14/07/2011>
-- Description:	<Description,,Select the top 1 Revenue_Process_Period id, in order to check that a record
                              --exists for the month and year chosen by the user>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SEL_Revenue_Recognition]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top 1 
	   [Run_Date]
      ,[Run_By]
	FROM Revenue_Recognition
	WHERE [Period_Month] = @month AND [period_year] = @year
	
END
GO
