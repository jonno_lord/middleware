USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TelephoneType]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_TelephoneType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TelephoneType]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_TelephoneType](@PHONE AS VARCHAR(20))
RETURNS VARCHAR(3)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(3)
	SET @RET = ''
	
	IF (ISNULL(@PHONE,'') <> '') SET @RET = 'TEL'
					  
	RETURN @RET
	
END

GO
