USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceLineNumber]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_OriginalInvoiceLineNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceLineNumber]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[fn_OriginalInvoiceLineNumber]
(
@OrderLineNumber AS VARCHAR(50)
)

RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @ret AS VARCHAR(10)
	DECLARE @OriginalInvoiceNumber AS VARCHAR(10)
	
	SELECT @ret = SFX FROM Invoices
	WHERE Order_Line_No = @OrderLineNumber
	AND Document_Type = 'B1'
	
	RETURN @ret
	
END
GO
