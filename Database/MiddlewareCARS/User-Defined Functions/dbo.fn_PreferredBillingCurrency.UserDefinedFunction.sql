USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PreferredBillingCurrency]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_PreferredBillingCurrency]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PreferredBillingCurrency]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PreferredBillingCurrency]()
returns varchar(3)
as
begin
	
	declare @ret as VARCHAR(3)
	
	SET @ret = 'GBP'
	
				  
	RETURN @ret
	
END

GO
