USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_DunningCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DunningCode]()
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = '17'
	
				  
	RETURN @ret
	
END

GO
