USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_StatementCycle]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_StatementCycle]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_StatementCycle]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_StatementCycle]()
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = '01'
	
				  
	RETURN @ret
	
END

GO
