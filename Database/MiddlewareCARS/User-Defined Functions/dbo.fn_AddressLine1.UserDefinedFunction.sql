USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AddressLine1]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_AddressLine1]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AddressLine1]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_AddressLine1]()
returns varchar(16)
as
begin
	
	declare @ret as VARCHAR(16)
	
	SET @ret = 'Accounts Payable'
					  
	RETURN @ret
	
END

GO
