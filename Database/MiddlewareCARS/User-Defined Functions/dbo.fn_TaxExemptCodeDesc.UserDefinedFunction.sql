USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCodeDesc]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_TaxExemptCodeDesc]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCodeDesc]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_TaxExemptCodeDesc]()
returns varchar(5)
as
begin
	
	declare @ret as VARCHAR(5)
	
	SET @ret = 'GBSTD'
	
				  
	RETURN @ret
	
END

GO
