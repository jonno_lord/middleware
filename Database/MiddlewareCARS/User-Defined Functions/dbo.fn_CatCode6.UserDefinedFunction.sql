USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode6]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_CatCode6]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode6]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CatCode6]()
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = 'D1'
	
				  
	RETURN @ret
	
END

GO
