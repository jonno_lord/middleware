USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyCode]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_AgencyCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyCode]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_AgencyCode]()
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = 'BR'
	
				  
	RETURN @ret
	
END

GO
