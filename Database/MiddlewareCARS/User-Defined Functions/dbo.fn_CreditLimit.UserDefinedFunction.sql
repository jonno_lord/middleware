USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_CreditLimit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CreditLimit]()
returns varchar(5)
as
begin
	
	declare @ret as VARCHAR(5)
	
	SET @ret = '10000'
	
				  
	RETURN @ret
	
END

GO
