USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FaxType]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_FaxType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FaxType]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_FaxType](@FAXNO AS VARCHAR(20))
RETURNS VARCHAR(3)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(3)
	SET @RET = ''
	
	IF (ISNULL(@FAXNO, '') <> '') SET @RET = 'FAX'
					  
	RETURN @RET
	
END

GO
