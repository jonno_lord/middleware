USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MandateCode]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_MandateCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MandateCode]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_MandateCode](@BankAccountNo as varchar(8))
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = CASE WHEN LEN(@BankAccountNo ) >0 THEN '0N' ELSE '' END
					  
	RETURN @ret
	
END

GO
