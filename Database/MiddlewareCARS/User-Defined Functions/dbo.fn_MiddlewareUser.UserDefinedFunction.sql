USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MiddlewareUser]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_MiddlewareUser]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MiddlewareUser]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_MiddlewareUser]()
returns varchar(7)
as
begin
	
	declare @ret as VARCHAR(7)
	
	SET @ret = 'CARS'
	
				  
	RETURN @ret
	
END

GO
