USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_NumberOfDunningLetters]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_NumberOfDunningLetters]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_NumberOfDunningLetters]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_NumberOfDunningLetters]()
returns varchar(1)
as
begin
	
	declare @ret as VARCHAR(1)
	
	SET @ret = '2'
	
				  
	RETURN @ret
	
END

GO
