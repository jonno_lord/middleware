USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PlanDuration]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_PlanDuration]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PlanDuration]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_PlanDuration](@PlanDuration AS VARCHAR(50))
RETURNS VARCHAR(5)
AS 
BEGIN 
	
	RETURN REPLACE(CONVERT(INT,@PlanDuration),'0','')			  
	
END

GO
