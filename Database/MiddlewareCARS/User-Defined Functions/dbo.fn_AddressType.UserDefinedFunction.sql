USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AddressType]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_AddressType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AddressType]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_AddressType]()
returns varchar(1)
as
begin
	
	declare @ret as VARCHAR(1)
	
	SET @ret = 'Y'
					  
	RETURN @ret
	
END

GO
