USE [MiddlewareCARS]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyNumber]    Script Date: 09/04/2018 16:31:19 ******/
DROP FUNCTION [dbo].[fn_CompanyNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyNumber]    Script Date: 09/04/2018 16:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CompanyNumber]()
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	
	SET @ret = '00000'
	
				  
	RETURN @ret
	
END

GO
