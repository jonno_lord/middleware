USE [MiddlewareCARS]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerBackFeed'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerBackFeed'
GO
/****** Object:  View [dbo].[vw_CustomerBackFeed]    Script Date: 09/04/2018 16:30:07 ******/
DROP VIEW [dbo].[vw_CustomerBackFeed]
GO
/****** Object:  View [dbo].[vw_CustomerBackFeed]    Script Date: 09/04/2018 16:30:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_CustomerBackFeed]
AS
SELECT DISTINCT 
                      LTRIM(RTRIM(f55.Q1URRF)) AS ClientNo, LEFT(f55.Q1MLNM, 40) AS CompanyName, LEFT(f55.Q1ADD1, 40) AS Addr1, LEFT(f55.Q1ADD2, 40) AS Addr2, LEFT(f55.Q1ADDZ, 25) AS PCode, 
                      LEFT(f55.Q1CTY1, 25) AS City, LEFT(f55.Q1CTR, 3) AS IsoCountry, LEFT(f55.Q1COUN, 25) AS County, LEFT(f55.Q1PH1, 20) AS Phone, LEFT(f55.Q1PH2, 20) AS Fax, LEFT(f55.Q1RMK, 30) AS Email, 
                      LEFT(f55.Q1TAX, 20) AS TaxRegNo, f55.Q1AN8 AS JDEAccountNo, LEFT(f55.Q1AC02, 20) AS CreditController, jcs.CARS_Value AS ISSTOPPED, LEFT(f55.Q1CM, 2) AS CreditMessage, 
                      f55.Q1AMCR AS AvailableCredit, f55.Q1ACL AS CreditLimit, LEFT(f55.Q1PORQ, 1) AS PurchasedOrderRequired, 'N30' AS PaymentTerms
FROM         dbo.F550101 AS f55 INNER JOIN
                      dbo.JDECreditStatus AS jcs ON LTRIM(RTRIM(jcs.JDE_Credit_Status_Code)) = ISNULL(f55.Q1CM, '') INNER JOIN
                      dbo.Customers AS cus ON LTRIM(RTRIM(cus.ClientNo)) = LTRIM(RTRIM(f55.Q1URRF))
WHERE     (cus.Stage = 5)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "f55"
            Begin Extent = 
               Top = 6
               Left = 303
               Bottom = 125
               Right = 505
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "jcs"
            Begin Extent = 
               Top = 6
               Left = 543
               Bottom = 125
               Right = 781
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cus"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1365
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerBackFeed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerBackFeed'
GO
