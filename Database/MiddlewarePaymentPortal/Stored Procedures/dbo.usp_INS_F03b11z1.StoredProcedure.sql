USE [MiddlewarePaymentPortal]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03b11z1]    Script Date: 10/04/2018 10:50:51 ******/
DROP PROCEDURE [dbo].[usp_INS_F03b11z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03b11z1]    Script Date: 10/04/2018 10:50:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F03b11z1]
@BatchNumber AS INT = 0 AS
BEGIN

INSERT INTO F03b11z1
(VJEDTN,VJEDUS,VJEDBT,VJDCT,VJAN8,VJDIVJ,VJCO,VJCRRM,VJCRCD,VJTXA1,VJEXR1,VJVINV,VJRMK,VJTORG,VJDOC,VJAG,VJSTAM,VJACR,VJCTAM,VJEDLN,VJMCU,VJSFX)

SELECT  pcco.Receipt_Number																	AS VJEDTN, 
	'CRCDPORTAL'																			AS VJEDUS,
	'CRPT' + REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '')							AS VJEDBT,
	'G7'																					AS VJDCT, 
	CONVERT(VARCHAR,Jde_Account_no)+ '-' + ISNULL(ba.ShipTo,'')								AS VJAN8,
	--CONVERT(VARCHAR,Jde_Account_no) as VJAN8,
	dbo.fn_DateToOracleJulian(GETDATE())													AS VJDIVJ,
	Hub_Company																				AS VJCO,
	CASE WHEN pcco.Currency = 'GBP' THEN 'D' ELSE 'F' END									AS VJCRRM,
	 pcco.Currency																			AS VJCRCD,
	VAT_CODE																				AS VJTXA1,
	CASE WHEN VAT_Code LIKE '%EXEMPT%' THEN 'E' ELSE 'V' END								AS VJEXR1,
	pcco.Batch_Number																		AS VJVINV,
	 pcco.Receipt_Number																	AS VJRMK,  
	'PORTALCC'																				AS VJTORG,
	pcco.Receipt_Number																		AS VJDOC,
	CASE WHEN pcco.Currency = 'GBP' THEN SUM(Gross_Price)  ELSE 0 END						AS VJAG,
	CASE WHEN pcco.Currency = 'GBP' THEN (SUM(Gross_price) - SUM(Net_price)) ELSE 0 END		AS VJSTAM,
	CASE WHEN pcco.Currency <> 'GBP' THEN SUM(Gross_Price) ELSE 0 END						AS VJACR,
	CASE WHEN pcco.Currency <> 'GBP' THEN (SUM(gross_price) - SUM(net_price)) ELSE 0 END	AS VJCTAM,
	0																						AS VJEDLN,
	pcco.Business_Unit																		AS VJMCU,
	pcco.SFX																				AS VJSFX
	FROM PORTAL_Credit_Card_Orders pcco
	LEFT JOIN MiddlewareCommon.dbo.BucketAccounts ba ON pcco.Currency = ba.Currency AND pcco.JDE_Account_No = ba.AccountNumber AND ba.SystemName = 'PaymentPortal'
	WHERE Stage = 1 and Jde_Account_No IS NOT NULL
	GROUP BY  pcco.Receipt_Number, JDE_Account_No, pcco.Batch_Number,  pcco.Currency, Hub_Company,pcco.Old_VAT, VAT_CODE, 
	VAT_RATE, Business_Unit, Object_Account, Subsidiary_Account,ba.ShipTo,pcco.SFX, pcco.Website_Name,Customer_ID,
	'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), Delivery_Date, 3), '/', ''),5,2)+SUBSTRING(REPLACE(CONVERT(VARCHAR(10), Delivery_Date, 3), '/', ''),3,2)
	ORDER BY JDE_Account_No, pcco.Receipt_Number, pcco.Batch_Number,  pcco.Currency, Hub_Company,pcco.Old_VAT, VAT_CODE, 
	VAT_RATE,Business_Unit, Object_Account, Subsidiary_Account, ba.ShipTo
	
	UPDATE F03b11z1
	SET VJTXA1 = 'GBOUTSIDE'
	WHERE VJTXA1 = 'OUTSIDE'
	

--** Used for creating the transaction and line numbering details in the cursor
DECLARE @VJSFX AS VARCHAR(3)
DECLARE @Line_Number AS INTEGER
DECLARE @VJDOC AS VARCHAR(10)
DECLARE @ID AS INTEGER

--** Stores the Document number
DECLARE @Last_VJDOC AS VARCHAR(10)

--** setup the last document number in temp variable
SET @VJDOC =''
SET @Line_Number = 0

--** Create a cursor based from the temporary table loaded  (grouped
--** by the key fields of batch number, bucket account and order currency)
DECLARE csrF03b11z1 CURSOR LOCAL FORWARD_ONLY FOR 
	SELECT VJDOC, f03.[ID] FROM F03b11z1 f03
	INNER JOIN Portal_Credit_Card_Orders pcco on f03.VJEDTN = pcco.Receipt_Number 
	WHERE pcco.Stage = 1
	ORDER BY VJDOC, VJSFX

--SET @Line_Number = 1

--** Loop through this created cursor and update the transaction numbers
OPEN csrF03b11z1
FETCH NEXT FROM csrF03b11z1 INTO @VJDOC, @ID
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Increment the line number by 1 and reset to 1 again for every new document number
	IF 	@Last_VJDOC <> @VJDOC
		SET @Line_Number = 1
	ELSE
		SET @Line_Number = @Line_Number + 1

	--** Create a justified version of this data
	--SET @VJSFX = LTRIM(RTRIM(CONVERT(VARCHAR(3), @Line_Number)))
	--SET @VJSFX = REPLICATE('0', 3 - LEN(@VJSFX)) + @VJSFX

	--** Update the table with the next value
	UPDATE F03b11z1 
	SET VJEDLN = @Line_Number 
	WHERE [ID] = @ID

	--** store the previous batch details...
	SET @Last_VJDOC=@VJDOC

	--** Get the next entry
	FETCH NEXT FROM csrF03b11z1 INTO @VJDOC, @ID
END

CLOSE csrF03b11z1
DEALLOCATE csrF03b11z1


	INSERT INTO AccountsReceivable ( F03b11z1id, stage )
	SELECT f03.id AS F03b11z1id, 1 AS Stage FROM F03b11z1 f03
	INNER JOIN Portal_Credit_Card_Orders pcco ON f03.VJEDTN = pcco.Receipt_Number
	WHERE pcco.Stage = 1

	UPDATE F03 
	SET AccountsReceivableId = ar.id 
	FROM F03B11Z1 f03
	INNER JOIN AccountsReceivable ar on ar.F03b11z1id = f03.id
	WHERE ar.Stage = 1


END
GO
