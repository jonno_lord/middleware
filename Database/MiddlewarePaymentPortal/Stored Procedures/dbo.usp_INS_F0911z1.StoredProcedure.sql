USE [MiddlewarePaymentPortal]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 10/04/2018 10:50:51 ******/
DROP PROCEDURE [dbo].[usp_INS_F0911z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 10/04/2018 10:50:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F0911z1]
@BatchNumber AS INT = 0
AS
BEGIN

	INSERT INTO F0911z1
	(VNEDTN,VNEDLN,VNEDUS,VNEDBT,VNDCT,VNDOC,VNDGJ,VNOBJ,VNSBL,VNAA,VNEXA,VNEXR,VNACR,VNCRRM,VNSFX,VNSTAM,VNCTAM)
	
	SELECT   
		pcco.Receipt_Number																						AS VNEDTN, 
		0																										AS VNEDLN, 
		'CRCDPORTAL'																							AS VNEDUS,
		'CRPT' +  REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '')											AS VNEDBT,
		'G7'																									AS VNDCT,
		 pcco.Receipt_Number																					AS VNDOC, 
		dbo.fn_DateToOracleJulian(GETDATE())																	AS VNDGJ,
		Object_Account																							AS VNOBJ,
		'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), Delivery_Date, 3), '/', ''),5,2) + 
		SUBSTRING(REPLACE(CONVERT(VARCHAR(10), Delivery_Date, 3), '/', ''),3,2)									AS VNSBL,
		CASE WHEN pcco.CURRENCY='GBP' THEN SUM(NET_PRICE)*-1 ELSE 0 END											AS VNAA,
		'CRCDPORTAL/' + REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '')									AS VNEXA,
		SUBSTRING(Website_Name,1,30)																			AS VNEXR, 
		CASE WHEN pcco.CURRENCY <> 'GBP' THEN SUM(NET_PRICE)*-1 ELSE 0 END										AS VNACR,
		CASE WHEN pcco.Currency = 'GBP' THEN 'D' ELSE 'F' END													AS VJCRRM,
		pcco.SFX																								AS VNSFX,
		CASE WHEN pcco.CURRENCY = 'GBP' THEN SUM(Gross_price * -1) - SUM(Net_Price * -1) ELSE 0 END				AS VNSTAM,
		CASE WHEN pcco.CURRENCY <> 'GBP' THEN SUM(Gross_price * -1) - SUM(Net_Price * -1) ELSE 0 END			AS VNCTAM
		--CASE WHEN SUM(Gross_price) < 0 THEN dbo.fn_OriginalInvoiceNumber(Customer_ID) ELSE NULL END				AS VNTXITM,
		--CASE WHEN SUM(Gross_price) < 0 THEN 'G7' ELSE NULL END													AS VNEXR1,
		--CASE WHEN SUM(Gross_price) < 0 THEN dbo.fn_OriginalInvoiceLineNumber(Customer_ID) ELSE NULL END			AS VNLNID
		FROM PORTAL_Credit_Card_Orders pcco
		LEFT JOIN MiddlewareCommon.dbo.BucketAccounts ba ON pcco.Currency = ba.Currency AND pcco.JDE_Account_No = ba.AccountNumber AND ba.SystemName = 'PaymentPortal'
		WHERE Stage = 1 and Jde_Account_No IS NOT NULL and net_price <> 0 
		GROUP BY  pcco.Receipt_Number, JDE_Account_No, pcco.Batch_Number,  pcco.Currency, Hub_Company,pcco.Old_VAT, VAT_CODE, 
		VAT_RATE, Business_Unit, Object_Account, Subsidiary_Account,ba.ShipTo,pcco.SFX, pcco.Website_Name,Customer_ID,
		'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), Delivery_Date, 3), '/', ''),5,2)+SUBSTRING(REPLACE(CONVERT(VARCHAR(10), Delivery_Date, 3), '/', ''),3,2)
		ORDER BY JDE_Account_No, pcco.Receipt_Number, pcco.Batch_Number,  pcco.Currency, Hub_Company,pcco.Old_VAT, VAT_CODE, 
		VAT_RATE,Business_Unit, Object_Account, Subsidiary_Account, ba.ShipTo
	
		

	--** Used for creating the transaction and line numbering details in the cursor
	DECLARE @VNEDTN AS INTEGER
	DECLARE @VNSFX AS VARCHAR(3)
	DECLARE @Line_Number AS INTEGER
	DECLARE @ID AS INTEGER

	--** Stores the previous batch details
	DECLARE @last_VNEDTN AS VARCHAR(10)
	DECLARE @last_VNSFX AS VARCHAR(3)

	--** Set up the last details so that
	--** they will break on the first occurence of entering the
	--** loop below...
	SET @last_VNEDTN = ''
	SET @last_VNSFX = ''

	--** begin the line number
	SET @Line_Number = 0

	--** Create a cursor based from the temporary table loaded  (grouped
	--** by the key fields of batch number, bucket account and order currency)
	DECLARE csrF0911z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VNEDTN, VNSFX, f09.[ID] FROM F0911z1 f09
		INNER JOIN Portal_Credit_Card_Orders pcco ON f09.VNEDTN = pcco.Receipt_Number
		WHERE pcco.Stage = 1
		ORDER BY VNEDTN, VNSFX

	--** Loop through this created cursor and update the transaction numbers
	OPEN csrF0911z1
	FETCH NEXT FROM csrF0911z1 INTO @VNEDTN, @VNSFX, @ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Increment the transaction number if the batch no, account or currency changed
		IF 	@last_VNEDTN <> @VNEDTN 
			SET @Line_Number = 1
			
		ELSE IF @last_VNEDTN = @VNEDTN AND @last_VNSFX <> @VNSFX
			SET @Line_Number = 1
			
		ELSE
			SET @Line_Number = @Line_Number + 1

		--** Update the table with the next value
		UPDATE F0911z1 SET VNEDLN = @Line_Number WHERE [ID] = @ID

		--** store the previous batch details...
		SET @last_VNEDTN = @VNEDTN
		SET @last_VNSFX = @VNSFX

		--** Get the next entry
		FETCH NEXT FROM csrF0911z1 INTO @VNEDTN, @VNSFX, @ID
	END

	CLOSE csrF0911z1
	DEALLOCATE csrF0911z1

	INSERT INTO GeneralLedger (f0911z1id, stage)
	SELECT f09.id AS f0911z1id, 1 AS stage FROM f0911z1 f09
	INNER JOIN Portal_Credit_Card_Orders pcco ON f09.VNEDTN = pcco.Receipt_Number
	WHERE pcco.Stage = 1

	UPDATE F09 
	SET GeneralLedgerId = gl.id 
	FROM F0911z1 f09
	INNER JOIN GeneralLedger gl on gl.F0911z1id = f09.id
	WHERE gl.Stage = 1

END

GO
