USE [MiddlewarePaymentPortal]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJDEFormat]    Script Date: 10/04/2018 10:50:51 ******/
DROP PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJDEFormat]    Script Date: 10/04/2018 10:50:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION
	
		DELETE FROM AccountsReceivable
		DELETE FROM GeneralLedger
	
		EXECUTE dbo.usp_UPD_InvoiceLineNumbers
		IF(@@ERROR <> 0) GOTO Failure

		EXECUTE dbo.usp_INS_F0911z1 @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE dbo.usp_INS_F03B11z1 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
		
		UPDATE Portal_Credit_Card_Orders SET Stage = 2, MiddlewareToMiddlewareIn=GETDATE() WHERE Stage = 1
		
        UPDATE AccountsReceivable 
        SET stage = 2, MiddlewareToMiddlewareIn = GETDATE()
        FROM AccountsReceivable ar
        INNER JOIN F03b11z1 f03 ON (ar.F03b11z1id = f03.id)
        WHERE ar.stage = 1        
        
        UPDATE GeneralLedger 
        SET stage = 2, MiddlewareToMiddlewareIn = GETDATE()
        FROM GeneralLedger gl
        INNER JOIN F0911z1 f09 ON (gl.f0911z1id = f09.Id)  
        WHERE gl.stage = 1
        
        EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoiceTaxCode @SystemName = 'MiddlewarePaymentPortal'
        IF(@@ERROR <> 0) GOTO Failure
        
        EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoicesPaymentTerms @SystemName = 'MiddlewarePaymentPortal'
        IF(@@ERROR <> 0) GOTO Failure

	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F0911z1 WHERE VNEDBT = @BatchNumber

END
GO
