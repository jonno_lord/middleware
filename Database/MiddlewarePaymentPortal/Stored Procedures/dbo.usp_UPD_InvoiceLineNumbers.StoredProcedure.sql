USE [MiddlewarePaymentPortal]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceLineNumbers]    Script Date: 10/04/2018 10:50:51 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoiceLineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceLineNumbers]    Script Date: 10/04/2018 10:50:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoiceLineNumbers]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE pcco
	SET pcco.SFX = qrySFX.SFX
	FROM PORTAL_Credit_Card_Orders pcco
	LEFT JOIN MiddlewareCommon.dbo.BucketAccounts ba ON pcco.Currency = ba.Currency 
	AND pcco.JDE_Account_No = ba.AccountNumber 
	AND ba.SystemName = 'PaymentPortal'
	INNER JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY Receipt_Number ORDER BY Receipt_Number) as SFX,pcco.Receipt_Number, JDE_Account_No, pcco.Batch_Number,  pcco.Currency, Hub_Company,pcco.Old_VAT,				VAT_CODE, VAT_RATE, Business_Unit, Object_Account, Subsidiary_Account,ba.ShipTo
				FROM PORTAL_Credit_Card_Orders pcco
				LEFT JOIN MiddlewareCommon.dbo.BucketAccounts ba ON pcco.Currency = ba.Currency 
				AND pcco.JDE_Account_No = ba.AccountNumber 
				AND ba.SystemName = 'PaymentPortal'
				WHERE Stage = 1 and Jde_Account_No IS NOT NULL
				GROUP BY pcco.Receipt_Number, JDE_Account_No, pcco.Batch_Number,pcco.Currency,Hub_Company,pcco.Old_VAT,VAT_CODE, 
				VAT_RATE,Business_Unit,Object_Account,Subsidiary_Account,ba.ShipTo) as qrySFX
	ON pcco.Receipt_Number = qrySFX.Receipt_Number and 
	pcco.JDE_Account_No = qrySFX.JDE_Account_No and 
	pcco.Batch_number = qrySFX.Batch_number and 
	pcco.Currency = qrySFX.Currency and 
	pcco.Hub_Company = qrySFX.Hub_Company and 
	pcco.Old_VAT = qrySFX.Old_VAT and 
	pcco.VAT_Code = qrySFX.VAT_Code and 
	pcco.VAT_Rate = qrySFX.VAT_Rate and
	pcco.Business_Unit= qrySFX.Business_Unit and 
	pcco.Object_Account = qrySFX.Object_Account and
	pcco.Subsidiary_Account = qrySFX.Subsidiary_Account and
	ba.ShipTo = qrySFX.ShipTo	
	WHERE Stage = 1 
	AND pcco.Jde_Account_No IS NOT NULL
	
	UPDATE pcco
	SET SFX = REPLICATE('0', 3 - LEN(SFX)) + SFX
	FROM Portal_Credit_Card_Orders pcco
	LEFT JOIN MiddlewareCommon.dbo.BucketAccounts ba ON pcco.Currency = ba.Currency 
	AND pcco.JDE_Account_No = ba.AccountNumber 
	AND ba.SystemName = 'PaymentPortal'
	WHERE Stage = 1 
	AND pcco.Jde_Account_No IS NOT NULL
		
END



		


GO
