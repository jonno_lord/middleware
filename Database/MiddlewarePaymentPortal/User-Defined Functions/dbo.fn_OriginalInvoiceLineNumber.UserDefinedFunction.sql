USE [MiddlewarePaymentPortal]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceLineNumber]    Script Date: 10/04/2018 10:51:27 ******/
DROP FUNCTION [dbo].[fn_OriginalInvoiceLineNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceLineNumber]    Script Date: 10/04/2018 10:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OriginalInvoiceLineNumber]
(
	@CustomerId AS INT
) 
RETURNS INT
AS
BEGIN
        DECLARE @ret AS INT
        SELECT @ret = SFX FROM Portal_Credit_Card_Orders
        WHERE Stage = 2
        AND Customer_ID = @CustomerId
        
        RETURN @ret
END
GO
