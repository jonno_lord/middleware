USE [MiddlewarePaymentPortal]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateToJulian]    Script Date: 10/04/2018 10:51:27 ******/
DROP FUNCTION [dbo].[fn_DateToJulian]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateToJulian]    Script Date: 10/04/2018 10:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[fn_DateToJulian](@date datetime) RETURNS char(8) AS 
BEGIN 

	DECLARE @JulianYear AS INT
	SET @JulianYear = YEAR(@date) - 1900

	RETURN (SELECT CONVERT(VARCHAR(10), @JulianYear) + 	
	REPLICATE('0', 3 -LEN(LTRIM(CAST(DATEPART(dy, @date) AS varchar(3)))))
	+
	LTRIM(CAST(DATEPART(dy, @date) AS varchar(3))))


    --RETURN (SELECT RIGHT(CAST(YEAR(@date) AS CHAR(4)),2) + RIGHT('000' + CAST(DATEPART(dy, @date) AS varchar(3)),4)) 
END
GO
