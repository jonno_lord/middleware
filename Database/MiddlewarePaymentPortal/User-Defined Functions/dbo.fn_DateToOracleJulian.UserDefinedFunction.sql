USE [MiddlewarePaymentPortal]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateToOracleJulian]    Script Date: 10/04/2018 10:51:27 ******/
DROP FUNCTION [dbo].[fn_DateToOracleJulian]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateToOracleJulian]    Script Date: 10/04/2018 10:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_DateToOracleJulian]
(
        @date datetime
) RETURNS int
AS
BEGIN
        RETURN DATEDIFF(dd, 0, @date) + 2415021
END
GO
