USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ItemMaster]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_ItemMaster]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ItemMaster]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_ItemMaster](@ItemMaster AS VARCHAR(25), @AccountNumber AS VARCHAR(25), @DocumentType AS VARCHAR(2))
RETURNS VARCHAR(25)
AS
BEGIN
	
	DECLARE @ret AS VARCHAR(25)
	DECLARE @CatCode28 AS VARCHAR(10)
	
	SELECT @CatCode28 = Cat_Code_28 FROM Customers
	WHERE JDE_Account_No = @AccountNumber
	
	IF (@CatCode28 = 'I')
		SET @ret = UPPER(@ItemMaster) + 'IC'
	ELSE IF (@DocumentType LIKE '%2' OR @DocumentType LIKE '%7' OR @DocumentType LIKE '%9')
		SET @ret = UPPER(@ItemMaster) + 'CN'
	ELSE
		SET @ret = UPPER(@ItemMaster)
				  
	RETURN @ret
	
END
GO
