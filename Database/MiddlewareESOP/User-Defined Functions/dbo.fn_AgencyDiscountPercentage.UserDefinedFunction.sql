USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyDiscountPercentage]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_AgencyDiscountPercentage]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyDiscountPercentage]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_AgencyDiscountPercentage](@Payment_Percent AS MONEY, @Invoiced_by_ESOP AS FLOAT, @COL_Value AS FLOAT )
RETURNS MONEY
AS
BEGIN

	DECLARE @percentage AS MONEY

	SELECT @percentage = CASE WHEN @COL_VALUE <> 0 THEN CONVERT(MONEY, ABS(@Invoiced_by_ESOP)) / CONVERT(MONEY, ABS(@COL_Value)) * 100  
        ELSE 
                @Payment_Percent      
        END 

	IF (@percentage > 100)
		SET @percentage = @Payment_Percent

	RETURN CONVERT(DECIMAL(10,2),@percentage)
	
END

GO
