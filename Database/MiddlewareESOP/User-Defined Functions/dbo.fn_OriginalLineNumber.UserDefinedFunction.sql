USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalLineNumber]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_OriginalLineNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalLineNumber]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OriginalLineNumber]
(
@DocumentType AS VARCHAR(2),
@COLAPId AS BINARY(8),
@InvoicedByESOP FLOAT,
@LineNo FLOAT
)
RETURNS INT
AS
BEGIN

	DECLARE @ret AS INT
	
	IF (@DocumentType like 'F%')
		BEGIN
			SELECT @ret = MAX(CONVERT(int, ord.Detail_Line_No * 1000000)) FROM Orders ord
			WHERE ord.Document_Type = REPLACE(@DocumentType,'F','E') 
			AND ord.Contract_Order_Line_Ac_Pay_Id = @COLAPId
			AND CONVERT(MONEY,ord.Invoiced_by_ESOP) = (@InvoicedByESOP * -1)
			AND ord.Invoiced_Date > '2014-02-01'
		END
				
	RETURN @ret	
	
END


















GO
