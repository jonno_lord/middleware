USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MailingName]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_MailingName]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MailingName]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_MailingName](@BillingContactName AS VARCHAR(25), @SearchType AS VARCHAR(3))
RETURNS VARCHAR(40)
AS
BEGIN
	
	DECLARE @ret AS VARCHAR(40)
	
	IF(@SearchType = 'CT')
		SET @ret = NULL
	ELSE 
		SET @ret = @BillingContactName	
		
	RETURN @ret
	
END


GO
