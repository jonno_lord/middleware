USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode1]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_CatCode1]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode1]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CatCode1](@searchType as varchar(10))
returns varchar(1)
as
begin
	
	declare @ret as VARCHAR(1)
	SELECT @ret = CASE 
				  WHEN @searchType = 'CH' THEN 'H'
				  WHEN @searchType = 'CI' THEN 'U'
				  WHEN @searchType = 'CE' THEN 'E'
				  WHEN @searchType = 'CS' THEN 'E'
				  ELSE 'x' END
				  
	RETURN @ret
	
END
GO
