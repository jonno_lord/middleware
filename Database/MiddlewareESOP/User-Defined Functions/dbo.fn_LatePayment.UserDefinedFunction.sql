USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LatePayment]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_LatePayment]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LatePayment]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_LatePayment]
(
	@SearchType AS VARCHAR(10)
)
RETURNS VARCHAR(20)
AS
BEGIN
	
	IF (@SearchType = 'CH')
		RETURN 'Y'
	
	RETURN NULL

END
GO
