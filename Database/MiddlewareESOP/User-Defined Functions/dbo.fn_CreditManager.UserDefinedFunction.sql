USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditManager]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_CreditManager]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditManager]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_CreditManager]
(
	-- Add the parameters for the function here
	@SearchType AS VARCHAR(5)
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @CreditManager AS VARCHAR(20)
	
	SET @CreditManager = 'DDEEDMAN'
	
	IF (@SearchType = 'CI') SET @CreditManager = 'GBELLAVIA'

	RETURN @CreditManager

END
GO
