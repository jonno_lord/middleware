USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultTelephoneCode]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DefaultTelephoneCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultTelephoneCode]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_DefaultTelephoneCode](@PHONE AS VARCHAR(100))
RETURNS VARCHAR(10)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(10)
	SET @RET = ''
	
	IF(ISNULL(@PHONE, '') <> '') SET @RET = 'TEL'
				  
	RETURN @RET
	
END
GO
