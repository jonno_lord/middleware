USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Quantity]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_Quantity]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Quantity]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_Quantity](@Quantity AS INT, @DocumentType AS VARCHAR(2))
RETURNS VARCHAR(10)
AS 
BEGIN
	
	DECLARE @ret AS FLOAT
	
	SET @ret = @Quantity
	
	IF(@DocumentType LIKE 'F%')
		SET @ret = @Quantity * -1
	
	RETURN @ret

END
GO
