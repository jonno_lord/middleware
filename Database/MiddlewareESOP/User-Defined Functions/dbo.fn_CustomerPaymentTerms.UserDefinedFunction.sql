USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerPaymentTerms]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_CustomerPaymentTerms]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerPaymentTerms]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_CustomerPaymentTerms](@searchType as varchar(3))
returns varchar(3)
as
begin

	DECLARE @Ret AS VARCHAR(3)
	SET @Ret = 'N30'
	IF(@searchType = 'CI') SET @Ret = 'N01'

	RETURN @Ret


end
GO
