USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProcessingMode]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_ProcessingMode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProcessingMode]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_ProcessingMode]()
returns varchar(1)
as
begin
	
	RETURN 'P'
	
END
GO
