USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCode]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_TaxExemptCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCode]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_TaxExemptCode](@TaxExemptCertificateNumber as varchar(20))
returns varchar(1)
as
begin
	
	DECLARE @ret AS VARCHAR(1)
	
	IF(LTRIM(@TaxExemptCertificateNumber) <> '')
		SET @ret = 'E'
	ELSE
		SET @ret = 'V'
	
	RETURN @ret
	
END
GO
