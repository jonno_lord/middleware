USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_County]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_County]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_County]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_County](@County AS VARCHAR(25), @CountryCode AS VARCHAR(3))
RETURNS VARCHAR(25)
AS
BEGIN
	
	DECLARE @ret AS VARCHAR(25)
	
	IF(@CountryCode = 'US')
		SET @ret = NULL
	ELSE 
		SET @ret = @County	
		
	RETURN @ret
	
END

GO
