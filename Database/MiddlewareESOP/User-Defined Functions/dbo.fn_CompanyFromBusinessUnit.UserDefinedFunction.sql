USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyFromBusinessUnit]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_CompanyFromBusinessUnit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyFromBusinessUnit]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CompanyFromBusinessUnit](@JdeBusinessUnit as varchar(10))
returns varchar(12)
as
begin
	
	declare @ret as VARCHAR(12)
	
	SET @ret = 'XXXX'
	SELECT @ret = ISNULL(company, 'XXXX') from MiddlewareJDE.dbo.vw_JdeBusinessUnits  WHERE BusinessUnit = LTRIM(RTRIM(@JdeBusinessUnit))
	
	RETURN @ret
	
END


GO
