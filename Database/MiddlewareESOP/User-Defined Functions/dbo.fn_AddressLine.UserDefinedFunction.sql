USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AddressLine]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_AddressLine]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AddressLine]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_AddressLine](@InputOne AS VARCHAR(40), @InputTwo AS VARCHAR(40), @SearchType AS VARCHAR(2))
RETURNS VARCHAR(40)
AS 
BEGIN

	DECLARE @ret AS VARCHAR(40)
	
	IF (@SearchType = 'CT')
		BEGIN
			SET @ret = @InputTwo
		END
	ELSE
		BEGIN
			SET @ret = @InputOne
		END

	RETURN @ret

END



GO
