USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProjectCode]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_ProjectCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProjectCode]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_ProjectCode](@ProjectCode AS VARCHAR(20))
RETURNS VARCHAR(10)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(20)
	
	IF(ISNULL(@ProjectCode,0) = 0)
		SET @RET = NULL
	ELSE 
		SET @RET = @ProjectCode
	
	RETURN @RET
	
END
GO
