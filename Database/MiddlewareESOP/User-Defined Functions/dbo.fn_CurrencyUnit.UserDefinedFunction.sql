USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CurrencyUnit]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_CurrencyUnit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CurrencyUnit]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_CurrencyUnit] (@SearchType as VARCHAR(3), @CurrencyUnit AS VARCHAR(10) )
RETURNS char(1) AS 
BEGIN 

	DECLARE @Default AS CHAR(1)
	SET @Default = 'F'
	
	IF((@SearchType = 'CE' OR @SearchType = 'CS') AND @CurrencyUnit = 'GBP')
		SET @Default = 'D'

	IF(@SearchType = 'CH' AND @CurrencyUnit = 'EUR')
		SET @Default = 'D'

	IF(@SearchType = 'CI' AND @CurrencyUnit = 'USD')
		SET @Default = 'D'

	Return @Default
	
END
GO
