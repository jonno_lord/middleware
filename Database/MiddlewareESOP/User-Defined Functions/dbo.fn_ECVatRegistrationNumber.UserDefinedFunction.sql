USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ECVatRegistrationNumber]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_ECVatRegistrationNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ECVatRegistrationNumber]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ECVatRegistrationNumber]
(
	@UkVatRegistrationNumber as varchar(20), @ECVatRegistrationNumber as varchar(20)
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @ret AS VARCHAR(20)
	
	SET @ret = ''
	
	SELECT @ret = CASE 
			WHEN @UkVatRegistrationNumber = '' and @ECVatRegistrationNumber <> '' THEN 
				ISNULL(@UkVatRegistrationNumber, '')
			ELSE 
				ISNULL(@ECVatRegistrationNumber, '')
			END 

	RETURN @ret
	
END
GO
