USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTerms]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_PaymentTerms]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTerms]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PaymentTerms](@SearchType AS VARCHAR(8), @PaymentTerms as VARCHAR(10), @DocumentType AS VARCHAR(5))
RETURNS CHAR(10)
AS 
BEGIN
	
	DECLARE @ret AS VARCHAR(10)
	
	SET @ret = @PaymentTerms
	
	IF(@SearchType = 'CH' AND @PaymentTerms <> 'R') 
		SET @ret = 'N30'
	IF(@DocumentType LIKE '%3') 
		SET @ret = 'PRE'
	
	RETURN @ret
	
END
GO
