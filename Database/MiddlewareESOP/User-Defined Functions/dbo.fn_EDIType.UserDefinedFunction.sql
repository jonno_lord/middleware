USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EDIType]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_EDIType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EDIType]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_EDIType]()
returns varchar(1)
as
begin
	return 'M'
end
GO
