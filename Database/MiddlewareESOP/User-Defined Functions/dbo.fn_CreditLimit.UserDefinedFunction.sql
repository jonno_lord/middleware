USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_CreditLimit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_CreditLimit](@searchType as varchar(10))
returns float
as
begin
	
	declare @ret as float
	
	SET @ret = 25000

	IF(@searchType = 'CT') 
		SET @ret = 100000
	
	IF(@searchType = 'CH') 
		SET @ret = 35000
	
	IF(@searchType = 'CI')	
		SET @ret = 50000
				  
	RETURN @ret
	
END


GO
