USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptDescription]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_TaxExemptDescription]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptDescription]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_TaxExemptDescription](@TaxExemptCertificateNumber as varchar(20), @searchType as varchar(10), @EcVatNumber AS VARCHAR(100))
returns varchar(10)
as
begin
	
	DECLARE @ret AS VARCHAR(10)
	
	--UK
	IF ((@searchType = 'CE' OR @searchType = 'CS') AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) <> '')
		SET @ret = 'GBEXMEPT'

	IF ((@searchType = 'CE' OR @searchType = 'CS') AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) = '')
		SET @ret = 'GBSTD'

	IF (@searchType ='CE' AND LTRIM(RTRIM(ISNULL(@ECVatNumber, ''))) <> '')
		SET @ret = 'GBREVCHRGE'		

	--NL
	IF (@searchType = 'CH' AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) <> '') 
		SET @ret = 'NLEXEMPT'

	IF (@searchType = 'CH' AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) = '') 
		SET @ret = 'NLSTD'
	
	IF (@searchType ='CH' AND LTRIM(RTRIM(ISNULL(@ECVatNumber, ''))) <> '')
		SET @ret = 'NLREVCHRGE'		

	--US
	IF (@searchType = 'CI') 
		SET @ret = 'USOUTSIDE'

	--TURKEY
	IF (@searchType = 'CT')
		SET @ret = 'TRSTD'
	
	RETURN @ret
	
END


GO
