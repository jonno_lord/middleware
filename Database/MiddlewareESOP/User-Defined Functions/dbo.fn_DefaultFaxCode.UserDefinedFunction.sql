USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultFaxCode]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DefaultFaxCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultFaxCode]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_DefaultFaxCode](@FAX AS VARCHAR(100))
RETURNS VARCHAR(10)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(10)
	SET @RET = ''
	
	IF(ISNULL(@FAX, '') <> '') SET @RET = 'FAX'
				  
	RETURN @RET
	
END
GO
