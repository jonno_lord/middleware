USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Percentage]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_Percentage]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Percentage]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_Percentage](@Payment_Percent AS MONEY, @Invoiced_by_ESOP AS FLOAT, @COL_Value AS FLOAT )
returns money
as
begin

	DECLARE @percentage as MONEY

	SELECT @percentage = CASE WHEN @COL_VALUE <> 0 THEN 
                CONVERT(MONEY, ABS(@Invoiced_by_ESOP)) / CONVERT(MONEY, ABS(@COL_Value)) * 100  
        ELSE 
                0       
        END 

	return CONVERT(DECIMAL(10,2),@percentage)
	
end
GO
