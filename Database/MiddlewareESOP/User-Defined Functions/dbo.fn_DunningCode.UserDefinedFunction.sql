USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DunningCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DunningCode](@searchType as varchar(10), @countryCode as varchar(10))
returns varchar(10)
as
begin

	DECLARE @ret AS VARCHAR(10)
	
	SET @ret = 'xx'
	
	IF(@searchType = 'CI') SET @ret = '13'

	IF(@searchType = 'CH') SET @ret = '14'
	
	IF((@searchType = 'CE' OR @SearchType ='CS') AND @CountryCode = 'GB')
		SELECT @ret = RFPLY FROM MiddlewareJDE.dbo.F03B25  WHERE RTRIM(RFPLYN) = 'ES' AND RFPLY <> 0
	
	IF((@searchType = 'CE' OR @SearchType ='CS') AND @CountryCode <> 'GB')
		SELECT @ret = RFPLY FROM MiddlewareJDE.dbo.F03B25  WHERE RTRIM(RFPLYN) = 'EF' AND RFPLY <> 0

	RETURN @ret
	
END
GO
