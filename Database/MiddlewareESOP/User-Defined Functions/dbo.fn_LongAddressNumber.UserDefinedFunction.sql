USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_LongAddressNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_LongAddressNumber](@searchType as varchar(10), @ProspectNo as varchar(20))
returns varchar(20)
as
begin
	
	declare @ret as VARCHAR(20)
	
	SET @ret = LTRIM(RTRIM(@ProspectNo)) + 'x'

	IF (@searchType = 'CI')
		SET @ret = LTRIM(RTRIM(@ProspectNo)) + 'U'

	IF (@searchType = 'CH')
		SET @ret = LTRIM(RTRIM(@ProspectNo)) + 'H'
	
	IF (@searchType = 'CE')
		SET @ret = LTRIM(RTRIM(@ProspectNo)) + 'E'
		
	IF (@searchType = 'CT')
		SET @ret = LTRIM(RTRIM(@ProspectNo)) + 'T'	
	
	RETURN @ret
	
END


GO
