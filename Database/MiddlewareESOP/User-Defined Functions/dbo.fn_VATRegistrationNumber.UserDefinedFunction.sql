USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_VATRegistrationNumber]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_VATRegistrationNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_VATRegistrationNumber]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_VATRegistrationNumber]
(
	@UkVatRegistrationNumber as varchar(20), @ECVatRegistrationNumber as varchar(20)
)
RETURNS VARCHAR(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ret AS VARCHAR(20)
	
	SET @ret = ''
	
	IF(LTRIM(@ECVatRegistrationNumber) <> '')
		SET @ret = @ECVatRegistrationNumber
	
	IF(LTRIM(@UkVatRegistrationNumber) <> '')
		SET @ret = @UkVatRegistrationNumber
	
	-- Return the result of the function
	RETURN @ret

END
GO
