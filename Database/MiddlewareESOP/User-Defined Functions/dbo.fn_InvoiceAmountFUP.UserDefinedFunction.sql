USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmountFUP]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_InvoiceAmountFUP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmountFUP]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_InvoiceAmountFUP](@SearchType AS VARCHAR(3), @CurrencyUnit AS VARCHAR(5), @InvoiceAmount AS MONEY, @DocumentType AS VARCHAR(2))
RETURNS MONEY
AS 
BEGIN

	DECLARE @ReturnedAmount as MONEY
	
	IF(@DocumentType LIKE 'F%')
		SET @ReturnedAmount = @InvoiceAmount * -1
	ELSE
		SET @ReturnedAmount = @InvoiceAmount 

	IF((@SearchType = 'CE' OR @SearchType='CS') AND @CurrencyUnit = 'GBP')
		SET @ReturnedAmount = 0

	IF(@SearchType = 'CH' AND @CurrencyUnit = 'EUR')
		SET @ReturnedAmount = 0

	IF(@SearchType = 'CI' AND @CurrencyUnit = 'USD')
		SET @ReturnedAmount = 0
	
	RETURN @ReturnedAmount
END
GO
