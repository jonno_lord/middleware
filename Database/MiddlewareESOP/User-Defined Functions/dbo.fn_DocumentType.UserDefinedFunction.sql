USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DocumentType]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DocumentType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DocumentType]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_DocumentType](@AccountNo AS VARCHAR(20), @SearchType AS VARCHAR(2), @DocumentType AS VARCHAR(2))
RETURNS VARCHAR(2)
AS 
BEGIN

	DECLARE @ret AS VARCHAR(2)
	DECLARE @CatCode28 AS VARCHAR(10)	
	
	SELECT @CatCode28 = cat_code_28 FROM Customers 
	WHERE JDE_Account_No = @AccountNo
	
	IF (ISNULL(@CatCode28, '')) = 'I'
		BEGIN
			SELECT @ret = CASE WHEN @SearchType = 'CE' THEN SUBSTRING(@DocumentType,1,1) + '5'
							WHEN @SearchType = 'CH' THEN @DocumentType
							WHEN @SearchType = 'CI' THEN @DocumentType END
		END
	ELSE 
		BEGIN
			SELECT @ret = @DocumentType
		END		
	
	RETURN @ret
		
END

GO
