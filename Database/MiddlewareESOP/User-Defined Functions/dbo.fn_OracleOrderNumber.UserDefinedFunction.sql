USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleOrderNumber]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_OracleOrderNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleOrderNumber]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[fn_OracleOrderNumber]
(
 @Acc_Payer_Contract_No AS VARCHAR(8),
@Detail_Line_No AS FLOAT
)
RETURNS VARCHAR(12)
AS
BEGIN

	RETURN CONVERT(VARCHAR(10),@Acc_Payer_Contract_No) + '_' + CONVERT(VARCHAR(10),@Detail_Line_No * 1000)

END







GO
