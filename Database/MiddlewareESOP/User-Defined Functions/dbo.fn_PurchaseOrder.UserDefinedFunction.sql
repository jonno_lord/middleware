USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PurchaseOrder]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_PurchaseOrder]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PurchaseOrder]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_PurchaseOrder](@ContractNo AS VARCHAR(8))
RETURNS VARCHAR(25)
AS 
BEGIN
	
	DECLARE @ret AS VARCHAR(10)
	
	SELECT @ret = Cust_PO_No FROM Orders o
	WHERE o.Acc_Payer_Contract_No = @ContractNo
	AND o.Stage = 1
	AND ISNULL(o.Cust_PO_No,'') <> ''
	
	SET @Ret = dbo.fn_RemoveSpecialChars(@ret)
	
	RETURN @ret
	
END

GO
