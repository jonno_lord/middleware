USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DirectDebit]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DirectDebit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DirectDebit]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_DirectDebit](@DirectDebit AS VARCHAR(10))
RETURNS VARCHAR(10)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(10)
	SET @RET = '0'
	
	IF(ISNULL(@DirectDebit,0) = 0) 
		SET @RET = '0'
	ELSE 
		SET @RET = '1'
				  
	RETURN @RET
	
END
GO
