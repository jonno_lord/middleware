USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningLetters]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DunningLetters]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningLetters]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DunningLetters]
(
	@SearchType as varchar(4), @CountryCode as varchar(10)
)
RETURNS VARCHAR(2)
AS
BEGIN

	DECLARE @ret AS VARCHAR(2)
	SET @ret = 'EF'
	
	IF(@SearchType = 'CH') SET @ret = 'NL'
	IF(@SearchType = 'CI') SET @ret = 'US'
	IF(@CountryCode = 'GB' AND (@searchType = 'CE' OR @searchType='CS')) SET @ret = 'ES'

	-- unknown search types will not post. SP 11/10/10
	IF(@SearchType <> 'CH' AND @SearchType <> 'CS' AND @SearchType <> 'CE' AND @SearchType <> 'CI')
		SET @ret = 'xx'

	RETURN @ret

END
GO
