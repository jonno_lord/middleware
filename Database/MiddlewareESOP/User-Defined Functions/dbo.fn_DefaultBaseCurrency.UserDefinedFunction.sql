USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultBaseCurrency]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_DefaultBaseCurrency]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultBaseCurrency]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DefaultBaseCurrency](@searchType as varchar(10))
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	
	SET @ret = 'xxx'
	
	IF(@searchType = 'CE' OR @searchType = 'CS') SET @ret = 'GBP'
	
	IF(@searchType = 'CH') SET @ret = 'EUR'
	
	IF(@searchType = 'CI')	SET @ret = 'USD'
				  
	RETURN @ret
	
END
GO
