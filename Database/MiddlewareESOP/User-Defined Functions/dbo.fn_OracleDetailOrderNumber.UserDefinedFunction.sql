USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleDetailOrderNumber]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_OracleDetailOrderNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleDetailOrderNumber]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OracleDetailOrderNumber]
(
 @Acc_Payer_Contract_No AS VARCHAR(8),
 @Batch_No AS INT,
 @Document_Type AS VARCHAR(10),
 @IsInsurance AS BIT
)
RETURNS VARCHAR(12)
AS
BEGIN

	DECLARE @LineNumber AS FLOAT
	
	IF (@IsInsurance = 1)
		BEGIN
			SELECT @LineNumber = MAX(ord.Detail_Line_No) FROM Orders ord
			WHERE ord.Acc_Payer_Contract_No = @Acc_Payer_Contract_No
			AND ord.Batch_No = @Batch_No
			AND ord.Document_Type = @Document_Type
			AND ord.Item_Master_No = 'INSURANCE'
			AND ord.Stage = 1
		END
	ELSE
		BEGIN 		
			SELECT @LineNumber = MAX(ord.Detail_Line_No) FROM Orders ord
			WHERE ord.Acc_Payer_Contract_No = @Acc_Payer_Contract_No
			AND ord.Batch_No = @Batch_No
			AND ord.Document_Type = @Document_Type
			AND ord.Item_Master_No <> 'INSURANCE'
			AND ord.Stage = 1
		END
		
	RETURN CONVERT(VARCHAR(10),@Acc_Payer_Contract_No) + '_' + CONVERT(VARCHAR(10),@LineNumber * 1000)

END









GO
