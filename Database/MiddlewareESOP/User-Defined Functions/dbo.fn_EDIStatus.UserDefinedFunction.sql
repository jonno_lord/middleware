USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EDIStatus]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_EDIStatus]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EDIStatus]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_EDIStatus]()
returns varchar(3)
as
begin
	return '850'
end
GO
