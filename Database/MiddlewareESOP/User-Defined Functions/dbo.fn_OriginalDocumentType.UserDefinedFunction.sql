USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalDocumentType]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[fn_OriginalDocumentType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalDocumentType]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_OriginalDocumentType]
(
@DocumentType AS VARCHAR(2)
)
RETURNS VARCHAR(2)
AS
BEGIN

	DECLARE @ret AS VARCHAR(2)
	
	IF (@DocumentType like 'F%')
		BEGIN
			SELECT @ret = REPLACE(@DocumentType,'F','E')
		END
		
	RETURN @ret	
	
END

GO
