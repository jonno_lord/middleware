USE [MiddlewareESOP]
GO
/****** Object:  UserDefinedFunction [dbo].[Julian_to_Date]    Script Date: 10/04/2018 10:38:18 ******/
DROP FUNCTION [dbo].[Julian_to_Date]
GO
/****** Object:  UserDefinedFunction [dbo].[Julian_to_Date]    Script Date: 10/04/2018 10:38:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Julian_to_Date] (@JulianDate AS INT) 
 RETURNS DATETIME AS  
BEGIN 
	DECLARE @JD AS VARCHAR(10)
	DECLARE @STRYEAR AS VARCHAR(4)
	DECLARE @DAYS AS VARCHAR(3)	
SET @JD = CONVERT(VARCHAR(10), @JulianDate)
	IF LEFT(@JD,1) = '0' 	
	SET @STRYEAR = '19' + SUBSTRING(@JD, 2, 2)
	ELSE
		SET @STRYEAR = '20' + SUBSTRING(@JD, 2, 2)	
SET @DAYS = RIGHT(@JD, 3)	
RETURN(DATEADD(DD, CONVERT(INT, @DAYS)-1, CONVERT(DATETIME, @STRYEAR + '/01/01')))
	RETURN @JD
END
GO
