USE [MiddlewareESOP]
GO
/****** Object:  View [dbo].[vw_CustomersURN]    Script Date: 10/04/2018 10:37:08 ******/
DROP VIEW [dbo].[vw_CustomersURN]
GO
/****** Object:  View [dbo].[vw_CustomersURN]    Script Date: 10/04/2018 10:37:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_CustomersURN] AS
SELECT Esop_Prospect_No AS URN, id FROM Customers
GO
