USE [MiddlewareESOP]
GO
/****** Object:  View [dbo].[vw_orders]    Script Date: 10/04/2018 10:37:08 ******/
DROP VIEW [dbo].[vw_orders]
GO
/****** Object:  View [dbo].[vw_orders]    Script Date: 10/04/2018 10:37:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_orders] as
select id,Stage,
	SourceToMiddleware,
		MiddlewareToMiddlewareIN,
			MiddlewareToJDE,JDEToMiddleware,MiddlewareToMiddlewareOut,MiddlewareToSource,Contract_Order_Line_Detail_Id,Rn_Descriptor,Rn_Create_Date,Rn_Create_User,Rn_Edit_Date,Rn_Edit_User,Contract_Order_Line_Id,Contract_Order_Line_Ac_Pay_Id,Acc_Payer_Contract_No,Document_Type,Account_Code,JDE_Business_Unit,Currency_Unit,Status_Date,Event_Name,Venue_Name,Event_Start_Date,Event_End_Date,Action_Code,Detail_Line_No,Payment_Percent,COL_Value,Invoiced_by_ESOP,Item_Master_No,Event_Product_Name,Batch_Run_Date,Payment_Terms,Payment_Instrument,Tax_Code,Issue_Date,Hall_Prefix,Stand_Number,Stand_Size,Cust_PO_No,Person_Placing_Order,Contract_No,Batch_No,Sub_Ledger_Type,Units_Order_Quantity,Invoice_No,Invoiced_By_JDE,Invoiced_Date,Invoice_Rejected_By_JDE,Invoice_On_Hold_By_JDE,Tax_Exempt_Cert_No,Passed_from_ESOP_to_MW,Contract_Id,Search_Type,msrepl_tran_version from orders
GO
