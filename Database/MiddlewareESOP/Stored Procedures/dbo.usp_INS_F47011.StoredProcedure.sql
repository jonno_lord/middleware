USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F47011]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_INS_F47011]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- verify the data is valid, and perform fixes
	EXECUTE dbo.usp_VER_OrderHeaders

	--DELETE F47 FROM F47011 F47 
	--	INNER JOIN Orders ON F47.SYDCTO = Orders.Document_Type AND F47.SYDOCO = Orders.Acc_Payer_Contract_No 
	--	WHERE Orders.Stage = 1
	
	-- Oracle needs to process Insurance lines separate.
	
	-- Create header for non insurance lines
	
	INSERT INTO F47011
		(SYKCOO,SYAN8,SYCRCD,SYDCTO,SYDEL1,SYDEL2,SYDOCO,SYMCU,SYOPDJ,SYPDDJ,SYTPUR,SYTRDJ,SYVR01,SYTORG,SYEDOC,SYEDCT,SYRYIN)
		SELECT DISTINCT
		dbo.fn_CompanyFromBusinessUnit(JDE_Business_Unit)								AS SYKCOO,
		qryAccount.JDE_Account_No														AS SYAN8,
		LEFT(ISNULL(Currency_Unit, 'xxx'),3)											AS SYCRCD,
		dbo.fn_DocumentType(qryAccount.JDE_Account_No,o.Search_Type,o.Document_Type)	AS SYDCTO,
		dbo.fn_RemoveSpecialChars(LEFT(o.Event_Name, 50))								AS SYDEL1,
		dbo.fn_RemoveSpecialChars(LEFT(o.Venue_Name, 30))								AS SYDEL2,
		dbo.fn_OracleOrderNumber(o.Acc_Payer_Contract_No,MAX(o.Detail_Line_No))			AS SYDOCO,
		dbo.fn_BusinessUnit(Jde_Business_Unit)											AS SYMCU,
		dbo.fn_DateToOracleJulian(MIN(o.Event_Start_Date))								AS SYOPDJ,
		dbo.fn_DateToOracleJulian(MAX(Event_End_Date))									AS SYPDDJ,
		dbo.fn_ActionCode()																AS SYTPUR,
		dbo.fn_DateToOracleJulian(MIN(Status_Date))										AS SYTRDJ,
		dbo.fn_PurchaseOrder(o.Acc_Payer_Contract_No)									AS SYVR01,
		dbo.fn_MWUserName()																AS SYTORG,
		o.Acc_Payer_Contract_No															AS SYEDOC,
		o.Document_Type																	AS SYEDCT,
		O.Payment_Instrument															AS SYRYIN
	FROM Orders as O
	INNER JOIN Customers c ON o.Account_Code = c.JDE_Account_No
		INNER JOIN
			(SELECT MAX(Account_Code) as JDE_Account_No, Acc_Payer_Contract_No, Document_Type, MIN(Detail_Line_No) as Detail_Line_No  
				FROM Orders o1 
				WHERE Stage = 1 
				GROUP BY  Acc_Payer_Contract_No, Document_Type) 
		as qryAccount
				ON qryAccount.Acc_Payer_Contract_No = O.Acc_Payer_Contract_No AND
				qryAccount.Document_Type = O.Document_Type 

	WHERE o.Stage = 1 
		AND o.Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
		AND o.Detail_Line_No IS NOT NULL AND o.Item_Master_No <> 'INSURANCE'
	GROUP BY qryAccount.JDE_Account_No, qryAccount.Detail_Line_No, Currency_Unit, o.Search_Type, O.Document_Type, 
		o.Batch_No, o.Acc_Payer_Contract_No, o.Event_Name, o.Venue_Name, o.JDE_Business_Unit, O.Payment_Instrument
		
		
		-- Create header for insurance lines
		
		INSERT INTO F47011
		(SYKCOO,SYAN8,SYCRCD,SYDCTO,SYDEL1,SYDEL2,SYDOCO,SYMCU,SYOPDJ,SYPDDJ,SYTPUR,SYTRDJ,SYVR01,SYTORG,SYEDOC,SYEDCT)
		SELECT DISTINCT
		dbo.fn_CompanyFromBusinessUnit(JDE_Business_Unit)								AS SYKCOO,
		qryAccount.JDE_Account_No														AS SYAN8,
		LEFT(ISNULL(Currency_Unit, 'xxx'),3)											AS SYCRCD,
		dbo.fn_DocumentType(qryAccount.JDE_Account_No,o.Search_Type,o.Document_Type)	AS SYDCTO,
		dbo.fn_RemoveSpecialChars(LEFT(o.Event_Name, 50))								AS SYDEL1,
		dbo.fn_RemoveSpecialChars(LEFT(o.Venue_Name, 30))								AS SYDEL2,
		dbo.fn_OracleOrderNumber(o.Acc_Payer_Contract_No,MAX(o.Detail_Line_No))			AS SYDOCO,
		dbo.fn_BusinessUnit(Jde_Business_Unit)											AS SYMCU,
		dbo.fn_DateToOracleJulian(MIN(o.Event_Start_Date))								AS SYOPDJ,
		dbo.fn_DateToOracleJulian(MAX(Event_End_Date))									AS SYPDDJ,
		dbo.fn_ActionCode()																AS SYTPUR,
		dbo.fn_DateToOracleJulian(MIN(Status_Date))										AS SYTRDJ,
		dbo.fn_PurchaseOrder(o.Acc_Payer_Contract_No)									AS SYVR01,
		dbo.fn_MWUserName()																AS SYTORG,
		o.Acc_Payer_Contract_No															AS SYEDOC,
		o.Document_Type																	AS SYEDCT
	FROM Orders as O
	INNER JOIN Customers c ON o.Account_Code = c.JDE_Account_No
		INNER JOIN
			(SELECT MAX(Account_Code) as JDE_Account_No, Acc_Payer_Contract_No, Document_Type, MIN(Detail_Line_No) as Detail_Line_No  
				FROM Orders o1 
				WHERE Stage = 1 
				GROUP BY  Acc_Payer_Contract_No, Document_Type) 
		as qryAccount
				ON qryAccount.Acc_Payer_Contract_No = O.Acc_Payer_Contract_No AND
				qryAccount.Document_Type = O.Document_Type 

	WHERE o.Stage = 1 
		AND o.Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
		AND o.Detail_Line_No IS NOT NULL AND o.Item_Master_No = 'INSURANCE'
	GROUP BY qryAccount.JDE_Account_No, qryAccount.Detail_Line_No, Currency_Unit, o.Search_Type, O.Document_Type, 
		o.Batch_No, o.Acc_Payer_Contract_No, o.Event_Name, o.Venue_Name, o.JDE_Business_Unit


	UPDATE F47 Set f47.OrdersId = Ord.id FROM F47011 F47
	INNER JOIN Orders Ord ON (F47.SYEDCT = Ord.Document_Type AND F47.SYEDOC = Ord.Acc_Payer_Contract_No)
	WHERE Stage = 1
	AND f47.OrdersId IS NULL
	
END
GO
