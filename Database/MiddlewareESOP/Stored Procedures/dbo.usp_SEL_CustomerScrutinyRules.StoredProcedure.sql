USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerScrutinyRules]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_CustomerScrutinyRules]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerScrutinyRules]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizbateh Hamlet
-- Create date: 13th August 2010
-- Description:	Selects all from the Customer Scrutiny Rules Table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CustomerScrutinyRules] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM CustomerScrutinyRules AS csr
		INNER JOIN MiddlewareSystem.dbo.Systems AS sy
			ON sy.id = csr.QuerySystemId

END
GO
