USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012_OldCopy20110606]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F47012_OldCopy20110606]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012_OldCopy20110606]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_INS_F47012_OldCopy20110606]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- verify the data is valid, and perform fixes
		DELETE F12 
			FROM F47012 F12
				INNER JOIN Orders ON F12.szedoc = Orders.Acc_Payer_Contract_No AND
				f12.szedct = orders.Document_Type and f12.szedln = Detail_Line_No * 1000000
			WHERE Stage = 1

		INSERT INTO F47012
			(OrdersId, SZKCO,SZKCOO, SZAN8,SZCRCD,SZDCTO,SZDOCO,SZDSC1,SZDSC2,SZEDBT,SZEDCT,SZEDLN,SZEDOC,SZEDST,SZEDTY,SZEKCO,SZFUN2,SZFUP,SZJOBN,SZLITM,SZLNID,SZLNTY,SZLOCN,SZLOTN,SZMCU,SZPID,SZPROV,SZPTC,SZRLLN,SZRYIN,SZSBL,SZSBLT,SZSERN,SZSHAN,SZTDAY,SZTORG,SZTXA1,SZUORG,SZUPMJ,SZUPRC,SZURAB,SZURAT,SZURDT,SZURRF,SZUSER,SZVR01,SZVR02, SZEDSP)
		SELECT DISTINCT
				Id													as OrdersId,
				''													as SZKCO,
				dbo.fn_CompanyFromBusinessUnit(JDE_Business_Unit)	as SZKCOO,								
				Account_Code										as SZAN8,
				Currency_Unit										as SZCRCD,
				Document_Type										as SZDCTO,
				Acc_Payer_Contract_No								as SZDOCO,
				Event_Product_Name									as SZDSC1,
				Stand_Size											as SZDSC2,
				@BatchNumber										as SZEDBT,
				Document_Type										as SZEDCT,
				CONVERT(int,Detail_Line_No * 1000000)				as SZEDLN,
				Acc_Payer_Contract_No								as SZEDOC,
				dbo.fn_EDIStatus()									as SZEDST,
				dbo.fn_EdiType()									as SZEDTY,
				dbo.fn_CompanyNumber()								as SZEKCO,
				dbo.fn_AgencyDiscountPercentage(Payment_Percent)	as SZFUN2,
				dbo.fn_InvoiceAmountFUP(Ord.Search_Type, Currency_Unit, Invoiced_by_ESOP)	as SZFUP,
				Ord.id												as SZJOBN,
				Item_Master_No										as SZLITM,
				CONVERT(int,Detail_Line_No * 1000000)				as SZLNID,
				dbo.fn_SZLNTY()										as SZLNTY,
				Hall_Prefix											as SZLOCN,
				Stand_Number										as SZLOTN,
				dbo.fn_BusinessUnit(Jde_Business_Unit)				as SZMCU,
				dbo.fn_MWUserName()									as SZPID,
				dbo.fn_Prov()										as SZPROV,
				dbo.fn_PaymentTerms(ord.Search_Type, Payment_Terms) as SZPTC,
				CONVERT(int,Detail_Line_No * 1000000)				as SZRLLN,
				Payment_Instrument									as SZRYIN,
				Issue_Date											as SZSBL,
				Sub_Ledger_Type										as SZSBLT,
				Cust_PO_No											as SZSERN,
				Account_Code										as SZSHAN,
				REPLACE(SUBSTRING(CONVERT(VARCHAR(20), GETDATE(), 113), 13, 10), ':', '') as SZTDAY,
				dbo.fn_MWUserName()									as SZTORG,
				ord.Tax_Code										as SZTXA1,
				Units_Order_Quantity								as SZUORG,
				dbo.fn_DateToJulian(GETDATE())						as SZUPMJ,
				dbo.fn_InvoiceAmountUPRC(Ord.Search_Type, Currency_Unit, Invoiced_by_ESOP) AS SZUPRC,
				Contract_No											as SZURAB,
				dbo.fn_Col(Col_Value)								as SZURAT,
				dbo.fn_DateToJulian(Batch_Run_Date)					as SZURDT,
				Ord.id												as SZURRF,
				dbo.fn_MWUsername()									as SZUSER,
				Cust_PO_No											as SZVR01,
				Person_Placing_Order								as SZVR02,
				'N'													as SZEDSP
								
			FROM Orders Ord
				WHERE Stage = 1 		AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
		AND Detail_Line_No IS NOT NULL

	
	
		UPDATE F12 SET SZTXA1 = Tax_Override_Code 
			FROM F47012 AS F12
				INNER JOIN Orders Ord ON f12.SZURRF = Ord.id
				INNER JOIN Customers Cust ON Cust.JDE_Account_No = Ord.Account_Code
				INNER JOIN TaxOverride Tx ON (Cust.Country_Code = tx.Country_Code AND Ord.Tax_Code = tx.Tax_Code AND tx.Business_Unit = LTRIM(RTRIM(ord.JDE_Business_unit)) AND Enabled = 1)
				WHERE Ord.Stage = 1 


	
END

GO
