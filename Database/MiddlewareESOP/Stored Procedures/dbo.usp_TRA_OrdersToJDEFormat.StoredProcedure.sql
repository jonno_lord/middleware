USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToJDEFormat]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_OrdersToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToJDEFormat]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_OrdersToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION
	
		EXECUTE dbo.usp_INS_F47011  @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE dbo.usp_INS_F47012 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
		         
        EXECUTE MiddlewareCommon.[dbo].[usp_UPD_OrderPaymentTerms] @SystemName='MiddlewareESOP'
        IF(@@ERROR <> 0) GOTO Failure
        
        EXECUTE MiddlewareCommon.[dbo].[usp_UPD_OrderTaxCode] @SystemName='MiddlewareESOP'
        IF(@@ERROR <> 0) GOTO Failure
        
		-- Mark all customers as ready for the next stage
		UPDATE Orders SET Stage = 2, MiddlewaretoMiddlewareIn = GETDATE() WHERE Stage = 1
		IF(@@ERROR <> 0) GOTO Failure
		
		UPDATE o
		SET o.Oracle_Document_Type = f.SZDCTO
		FROM Orders o 
		INNER JOIN F47012 f ON o.id = f.OrdersId
		WHERE o.Stage = 2
		IF(@@ERROR <> 0) GOTO Failure
		
		--Another bodge
		UPDATE F47012
		SET SZDOC = NULL, SZRLLN = NULL
		WHERE SZDOCO IN (SELECT DISTINCT SZDOCO FROM f47012 f 
						INNER JOIN Orders o ON f.OrdersId = o.id 
						WHERE Stage = 2 
						AND (SZDOC IS NULL OR SZRLLN IS NULL) 
						AND SZDCTO LIKE 'F%')
		IF(@@ERROR <> 0) GOTO Failure
		
	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F47012 WHERE SZEDBT = @BatchNumber

END

GO
