USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditControllerNotes]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_CreditControllerNotes]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditControllerNotes]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_CreditControllerNotes] 
	-- Add the parameters for the stored procedure here
	(@BatchNumber AS INT =0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM F56008
	WHERE LEN(CONVERT(BIGINT,DNAN8)) > 8

	INSERT INTO dbo.CreditControllerNotes
		(Stage, MiddlewareToMiddlewareOut, BatchNumber, SearchType,JDEAccountNumber,CreatedDate,CustomerNumber,JDEActivtyType,JDEActivtyTypeDescription,NoteNumber,JDEUserName,NoteCategory,NoteText,UpdateDate,UpdateTime)
		SELECT		
			1 AS Stage,
			GETDATE() as MiddlewareToMiddlewareOut,
			@BatchNumber as BatchNumber,
			DNAT1 AS SearchType, 
			DNAN8 AS JDEAccountNumber,
			dbo.Julian_to_Date(DNDTI) AS CreatedDate,
			DNURRF AS CustomerNumber,
			DNAIT AS JDEActivtyType,
			DNDL01 AS JDEActivtyTypeDescription,
			DNAVID AS NoteNumber,
			DNEUSR AS JDEUserName,
			CASE WHEN ISNUMERIC(DNRMK) <> 1 THEN SUBSTRING(DNRMK,1,CHARINDEX('_',DNRMK,1)-1) ELSE DNRMK END AS NoteCategory,
			DNVAR1 AS NoteText,
			dbo.Julian_to_Date(DNUPMJ) as UpdateDate, 
			DNUPMT AS UpdateTime from F56008 f56
		LEFT OUTER JOIN dbo.CreditControllerNotes CCN 
		ON (f56.DNAVID = CCN.NoteNumber AND dbo.Julian_to_Date(DNUPMJ) = UpdateDate)
		WHERE CCN.id IS NULL
		
	SELECT COUNT(*) FROM dbo.CreditControllerNotes WHERE Stage = 1 AND BatchNumber = @BatchNumber 
	
END
GO
