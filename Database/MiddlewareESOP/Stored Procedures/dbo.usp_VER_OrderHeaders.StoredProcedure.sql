USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_VER_OrderHeaders]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_VER_OrderHeaders]
GO
/****** Object:  StoredProcedure [dbo].[usp_VER_OrderHeaders]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_VER_OrderHeaders] AS
BEGIN



DELETE Orders FROM Orders 
	INNER JOIN 
(select Acc_Payer_Contract_No, Document_Type, Detail_Line_No  From Orders
	Group by Acc_Payer_Contract_No, Document_Type, Detail_Line_No 
		having COUNT(*) > 1) as Dupes ON 
		Dupes.Acc_Payer_Contract_No = Orders.Acc_Payer_Contract_No AND
		Dupes.Document_Type = Orders.Document_Type AND
		Dupes.Detail_Line_No = Dupes.Detail_Line_No
	WHERE Stage = 1

-- Check to see if there are items in the Orders table that cannot be processed as ESOP
-- has created invalid detail, document and account payer duplicates. This means we 
-- cannot send them to JDE as they will break the PK. At the moment, we just check for
-- this and fail the job - this should be checked with the business - if they wish to
-- ignore the data and try and process what they have - we should uncomment the delete
-- statement. The job would fail, but then remove the offending articles from MW
-- allowing it to continue...
IF EXISTS(SELECT Acc_Payer_Contract_No, Document_Type, Detail_Line_No FROM orders
	WHERE 
	Stage = 1 
	AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
	AND Detail_Line_No IS NOT NULL
	GROUP BY Acc_Payer_Contract_No, Document_Type, Detail_Line_No
	HAVING COUNT(*) > 1)
BEGIN

	DECLARE @AccPayer VARCHAR(20)
	DECLARE @BadAccounts AS VARCHAR(8000)
	
	SET @BadAccounts = 'ESOP has generated Duplicate Detail line numbers for the following account payer contract numbers:' + CHAR(13)
	
	-- create a list of the bad accounts
	DECLARE faults CURSOR FOR
		SELECT Acc_Payer_Contract_No FROM orders
			GROUP BY Acc_Payer_Contract_No, Document_Type, Detail_Line_No
				HAVING COUNT(*) > 1
	OPEN faults
	FETCH NEXT FROM faults INTO @AccPayer
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @BadAccounts = @BadAccounts + @AccPayer + ','
		FETCH NEXT FROM faults INTO @AccPayer
	
	END
	CLOSE faults
	DEALLOCATE faults

	-- report on the bad accounts, provide the SQL so when debugging we can execute the
	-- query to find out what is going on.
	SET @BadAccounts = @BadAccounts + + CHAR(13) + 'To Generate the Query Run ' + CHAR(13)
	SET @BadAccounts = @BadAccounts + 'SELECT Acc_Payer_Contract_No, Document_Type, Detail_Line_No FROM orders WHERE Stage = 1 GROUP BY Acc_Payer_Contract_No, Document_Type, Detail_Line_No HAVING COUNT(*) > 1 AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL AND Detail_Line_No IS NOT NULL'
	RAISERROR(@BadAccounts, 16, -1)

/*
	This DELETE will remove the duplicates...
	
	DELETE o FROM Orders o
	INNER JOIN (SELECT Acc_Payer_Contract_No, Document_Type, Detail_Line_No FROM orders
	WHERE Stage = 1
	AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
	AND Detail_Line_No IS NOT NULL
	GROUP BY Acc_Payer_Contract_No, Document_Type, Detail_Line_No
	HAVING COUNT(*) > 1) bad ON
		o.Acc_Payer_Contract_No = bad.Acc_Payer_Contract_No AND
		o.Document_Type = bad.Document_Type AND
		o.Detail_Line_No = bad.Detail_Line_No 
*/
	
END

--- Next deal with venuenames that have changed half way through processing...
-- this will break the PK
DECLARE csrVenueChanged CURSOR FOR
SELECT DISTINCT m.Acc_Payer_Contract_No, m.Document_Type FROM
(
SELECT DISTINCT
	o.Venue_Name,
	o.Acc_Payer_Contract_No,
	o.Document_Type 										
FROM Orders as O
	INNER JOIN
		-- join on the Maximum account codes, as the contract can change the account
		-- number during the invoice process...
		(SELECT MAX(Account_Code) as JDE_Account_No, Acc_Payer_Contract_No, Document_Type, MIN(Detail_Line_No) as Detail_Line_No  
			FROM Orders o1 
			WHERE Stage = 1 
			GROUP BY  Acc_Payer_Contract_No, Document_Type) 
	as qryAccount
			ON qryAccount.Acc_Payer_Contract_No = O.Acc_Payer_Contract_No AND
			qryAccount.Document_Type = O.Document_Type 

		-- filter to exclude bad data
WHERE Stage = 1 
	AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
	AND o.Detail_Line_No IS NOT NULL AND o.Event_Name IS NOT NULL AND o.Document_Type IS NOT NULL
GROUP BY qryAccount.JDE_Account_No, qryAccount.Detail_Line_No, Currency_Unit, Search_Type, O.Document_Type, 
	O.Acc_Payer_Contract_No, o.Venue_Name

) AS M
GROUP BY m.Acc_Payer_Contract_No, m.Document_Type 
HAVING COUNT(*) > 1


-- loop through and update all bad references of venue names to the first instance
DECLARE @acc AS VARCHAR(20)
DECLARE @doc as VARCHAR(10)

OPEN csrVenueChanged
FETCH NEXT FROM csrVenueChanged INTO @acc, @doc
WHILE @@FETCH_STATUS = 0
BEGIN

	DECLARE @VenueName as VARCHAR(250)
	SELECT @VenueName = Venue_Name FROM Orders WHERE Acc_Payer_Contract_No = @acc AND Document_Type = @doc
	-- update all instances of this venue name to the first one. (ie. when they change
	-- excl-docklands to "excel docklands"
	UPDATE Orders SET Venue_Name = @VenueName WHERE Acc_Payer_Contract_No = @acc AND Document_Type = @doc

	FETCH NEXT FROM csrVenueChanged INTO @acc, @doc
END

CLOSE csrVenueChanged
DEALLOCATE csrVenueChanged




--- now do the same procedure for Event Names that have changed
DECLARE csrEventChanged CURSOR FOR
SELECT DISTINCT m.Acc_Payer_Contract_No, m.Document_Type FROM
(
SELECT DISTINCT
	o.Event_Name,
	o.Acc_Payer_Contract_No,
	o.Document_Type 										
FROM Orders as O
	INNER JOIN
		(SELECT MAX(Account_Code) as JDE_Account_No, Acc_Payer_Contract_No, Document_Type, MIN(Detail_Line_No) as Detail_Line_No  
			FROM Orders o1 
			WHERE Stage = 1 
			GROUP BY  Acc_Payer_Contract_No, Document_Type) 
	as qryAccount
			ON qryAccount.Acc_Payer_Contract_No = O.Acc_Payer_Contract_No AND
			qryAccount.Document_Type = O.Document_Type 

WHERE Stage = 1 
	AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
	AND o.Detail_Line_No IS NOT NULL AND o.Event_Name IS NOT NULL AND o.Document_Type IS NOT NULL
GROUP BY qryAccount.JDE_Account_No, qryAccount.Detail_Line_No, Currency_Unit, Search_Type, O.Document_Type, 
	O.Acc_Payer_Contract_No, o.Event_Name

) AS M
GROUP BY m.Acc_Payer_Contract_No, m.Document_Type 
HAVING COUNT(*) > 1

OPEN csrEventChanged 
FETCH NEXT FROM csrEventChanged INTO @acc, @doc
WHILE @@FETCH_STATUS = 0
BEGIN

	DECLARE @EventName as VARCHAR(50)
	SELECT @EventName = Event_Name FROM Orders WHERE Acc_Payer_Contract_No = @acc AND Document_Type = @doc

	UPDATE Orders SET Event_Name = @EventName WHERE Acc_Payer_Contract_No = @acc AND Document_Type = @doc

	FETCH NEXT FROM csrEventChanged INTO @acc, @doc
END

CLOSE csrEventChanged 
DEALLOCATE csrEventChanged 



END
GO
