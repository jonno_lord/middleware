USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F47012]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_INS_F47012]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- verify the data is valid, and perform fixes

		--DELETE F12 
		--	FROM F47012 F12
		--		INNER JOIN Orders ON F12.szedoc = Orders.Acc_Payer_Contract_No AND
		--		f12.szedct = orders.Document_Type and f12.szedln = Detail_Line_No * 1000000
		--	WHERE Stage = 1

		INSERT INTO F47012
		(OrdersId,SZKCOO,SZAN8,SZDCTO,SZDOCO,SZDSC1,SZDSC2,SZFUN2,SZFUP,SZLITM,SZLNID,SZLOCN,SZLOTN,SZMCU,
		SZPTC,/*SZRLLN,*/SZSBL,SZSERN,SZTXA1,SZUORG,SZUPMJ,SZUPRC,SZVR02,/*SZDOC,*/SZEDOC)
				
				SELECT DISTINCT
				Ord.id																							as OrdersId,
				dbo.fn_CompanyFromBusinessUnit(JDE_Business_Unit)												as SZKCOO,								
				Account_Code																					as SZAN8,
				Document_Type																					as SZDCTO,
				dbo.fn_OracleDetailOrderNumber(Acc_Payer_Contract_No,Batch_No,Document_Type,0)					as SZDOCO,
				dbo.fn_RemoveSpecialChars(Event_Product_Name)													as SZDSC1,
				Stand_Size																						as SZDSC2,
				dbo.fn_AgencyDiscountPercentage(Payment_Percent, Invoiced_By_ESOP, Col_Value)					as SZFUN2,
				dbo.fn_InvoiceAmountFUP(Ord.Search_Type, Currency_Unit, Invoiced_by_ESOP, Document_Type)		as SZFUP,
				dbo.fn_ItemMaster(ord.Item_Master_No, ord.Account_Code, ord.Document_Type)						as SZLITM,
				CONVERT(int, Detail_Line_No * 1000000)															as SZLNID,
				Hall_Prefix																						as SZLOCN,
				Stand_Number																					as SZLOTN,
				dbo.fn_BusinessUnit(Jde_Business_Unit)															as SZMCU,
				dbo.fn_PaymentTerms(ord.Search_Type, Payment_Terms,Document_Type)								as SZPTC,
				--dbo.fn_OriginalLineNumber(ord.Document_Type,ord.Contract_Order_Line_Ac_Pay_Id,
				--							ord.Invoiced_by_ESOP,ord.Detail_Line_No)							AS SZRLLN,
				'20' + Issue_Date																				as SZSBL,
				dbo.fn_RemoveSpecialChars(Invoice_Reference)													as SZSERN,
				ord.Tax_Code																					as SZTXA1,
				dbo.fn_Quantity(Units_Order_Quantity,Document_Type)												as SZUORG,
				dbo.fn_ProjectCode(Ord.Project_Code)															as SZUPMJ,
				dbo.fn_InvoiceAmountUPRC(Ord.Search_Type, Currency_Unit, Invoiced_by_ESOP, Document_Type)		as SZUPRC,
				dbo.fn_RemoveSpecialChars(Person_Placing_Order)													as SZVR02,
				--dbo.fn_OriginalInvoiceNumber(ord.Document_Type,ord.Contract_Order_Line_Ac_Pay_Id,
				--								ord.Invoiced_by_ESOP)											as SZDOC,
				ord.Acc_Payer_Contract_No																		as SZEDOC
			FROM Orders Ord
			INNER JOIN Customers c ON ord.Account_Code = c.JDE_Account_No
			WHERE Ord.Stage = 1 AND Ord.Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
			AND Detail_Line_No IS NOT NULL AND ord.Item_Master_No <> 'INSURANCE'
		
		
		INSERT INTO F47012
		(OrdersId,SZKCOO,SZAN8,SZDCTO,SZDOCO,SZDSC1,SZDSC2,SZFUN2,SZFUP,SZLITM,SZLNID,SZLOCN,SZLOTN,SZMCU,
		SZPTC,/*SZRLLN,*/SZSBL,SZSERN,SZTXA1,SZUORG,SZUPMJ,SZUPRC,SZVR02,/*SZDOC,*/SZEDOC)
				
				SELECT DISTINCT
				Ord.id																							as OrdersId,
				dbo.fn_CompanyFromBusinessUnit(JDE_Business_Unit)												as SZKCOO,								
				Account_Code																					as SZAN8,
				Document_Type																					as SZDCTO,
				dbo.fn_OracleDetailOrderNumber(Acc_Payer_Contract_No,Batch_No,Document_Type,1)					as SZDOCO,
				dbo.fn_RemoveSpecialChars(Event_Product_Name)													as SZDSC1,
				Stand_Size																						as SZDSC2,
				dbo.fn_AgencyDiscountPercentage(Payment_Percent, Invoiced_By_ESOP, Col_Value)					as SZFUN2,
				dbo.fn_InvoiceAmountFUP(Ord.Search_Type, Currency_Unit, Invoiced_by_ESOP, Document_Type)		as SZFUP,
				dbo.fn_ItemMaster(ord.Item_Master_No, ord.Account_Code, ord.Document_Type)						as SZLITM,
				CONVERT(int, Detail_Line_No * 1000000)															as SZLNID,
				Hall_Prefix																						as SZLOCN,
				Stand_Number																					as SZLOTN,
				dbo.fn_BusinessUnit(Jde_Business_Unit)															as SZMCU,
				dbo.fn_PaymentTerms(ord.Search_Type, Payment_Terms,Document_Type)								as SZPTC,
				--dbo.fn_OriginalLineNumber(ord.Document_Type,ord.Contract_Order_Line_Ac_Pay_Id,
				--							ord.Invoiced_by_ESOP,ord.Detail_Line_No)							AS SZRLLN,
				'20' + Issue_Date																				as SZSBL,
				dbo.fn_RemoveSpecialChars(Invoice_Reference)													as SZSERN,
				ord.Tax_Code																					as SZTXA1,
				dbo.fn_Quantity(Units_Order_Quantity,Document_Type)												as SZUORG,
				dbo.fn_ProjectCode(Ord.Project_Code)															as SZUPMJ,
				dbo.fn_InvoiceAmountUPRC(Ord.Search_Type, Currency_Unit, Invoiced_by_ESOP, Document_Type)		as SZUPRC,
				dbo.fn_RemoveSpecialChars(Person_Placing_Order)													as SZVR02,
				--dbo.fn_OriginalInvoiceNumber(ord.Document_Type,ord.Contract_Order_Line_Ac_Pay_Id,
				--								ord.Invoiced_by_ESOP)											as SZDOC,
				ord.Acc_Payer_Contract_No																		as SZEDOC
			FROM Orders Ord
			INNER JOIN Customers c ON ord.Account_Code = c.JDE_Account_No
			WHERE Ord.Stage = 1 AND Ord.Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
			AND Detail_Line_No IS NOT NULL AND ord.Item_Master_No = 'INSURANCE'
		
		--Prime OUTSIDE tax code prior to lookup
		--UPDATE F47
		--SET SZTXA1 = CASE WHEN Search_Type = 'CI' THEN 'USOUTSIDE'
		--			 WHEN Search_Type = 'CH' THEN 'NLOUTSIDE'
		--			 ELSE 'GBOUTSIDE' END
		--FROM F47012 F47
		--INNER JOIN Orders o ON F47.OrdersId = o.id
		--WHERE o.Stage = 1 AND SZTXA1 = 'OUTSIDE'
		
	
END
GO
