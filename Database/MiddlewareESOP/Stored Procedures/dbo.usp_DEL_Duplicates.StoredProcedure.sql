USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_Duplicates]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_Duplicates]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_Duplicates]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DEL_Duplicates]
	@BatchNumber AS INTEGER = 0
AS
BEGIN

BEGIN TRANSACTION

-- Dedupe accounts, used for when a scrutiny rule determines the same customer has violated multiple rules
DECLARE @CustomerID as INT
DECLARE @SystemNameId as Int
DECLARE @CustomerReferenceNumber AS VARCHAR(50)
DECLARE @JdeAccountNumber AS VARCHAR(50)

-- retrieve a list of customer duplicates that appear more than once in the duplicate listing... 
DECLARE csrDupes CURSOR FOR 
      SELECT CustomerId, SystemNameid, CustomerReferenceNumber, JdeAccountNumber FROM
            CustomerDuplicates group by CustomerId, SystemNameid,CustomerReferenceNumber,JdeAccountNumber  
            having COUNT(*) > 1

-- retrieve the customers to process
OPEN csrDupes
FETCH NEXT FROM csrDupes INTO @CustomerId, @SystemNameId, @CustomerReferenceNumber, @JdeAccountNumber

-- only keep the first customer in the list loop.
WHILE @@FETCH_STATUS = 0
BEGIN

      -- find the first entry that we wish to keep
      DECLARE @id AS INTEGER
      SELECT @id = id FROM CustomerDuplicates WHERE CustomerId = @CustomerID AND SystemNameid = @SystemNameId AND 
                  CustomerReferenceNumber = @CustomerReferenceNumber AND JdeAccountNumber = @JdeAccountNumber

      -- remove the entries that are not the one we want to keep, but match of all other fields..
      DELETE cd FROM CustomerDuplicates cd WHERE CustomerId = @CustomerID AND SystemNameid = @SystemNameId AND 
                  CustomerReferenceNumber = @CustomerReferenceNumber AND JdeAccountNumber = @JdeAccountNumber AND id <> @id
      
      -- move onto the next duplicate
      FETCH NEXT FROM csrDupes INTO @CustomerId, @SystemNameId, @CustomerReferenceNumber, @JdeAccountNumber

END

-- dealloc and release
CLOSE csrDupes
DEALLOCATE csrDupes

            
COMMIT TRANSACTION

END

GO
