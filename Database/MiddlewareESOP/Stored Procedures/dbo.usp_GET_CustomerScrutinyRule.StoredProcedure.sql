USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerScrutinyRule]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_GET_CustomerScrutinyRule]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerScrutinyRule]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 13th August 2010
-- Description:	Selects specified Customer Scrutiny Rule
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_CustomerScrutinyRule] 
	-- Add the parameters for the stored procedure here
	@customerScrutinyRuleid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM CustomerScrutinyRules
	WHERE id = @customerScrutinyRuleid
END
GO
