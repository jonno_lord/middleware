USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_TaxOverride]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_TaxOverride]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_TaxOverride]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:B.M.Patterson
-- Create date: 20th September 2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_TaxOverride]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM TaxOverride
	INNER JOIN MiddlewareJDE.dbo.vw_JdeBusinessUnits ON TaxOverride.Business_Unit =  MiddlewareJDE.dbo.vw_JdeBusinessUnits.BusinessUnit
END
GO
