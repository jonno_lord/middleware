USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_TaxOverride]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_TaxOverride]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_TaxOverride]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th September 2010
-- Description:	Deletes record fromTax Override
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_TaxOverride] 
	-- Add the parameters for the stored procedure here
	@taxId AS INTEGER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM TaxOverride WHERE Tax_Override_Id = @taxId
END
GO
