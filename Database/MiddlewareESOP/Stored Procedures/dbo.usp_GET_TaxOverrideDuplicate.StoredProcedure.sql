USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TaxOverrideDuplicate]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_GET_TaxOverrideDuplicate]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TaxOverrideDuplicate]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th September 2010
-- Description:	This Stored Procedure attempts to find a duplicate in the TaxOverride table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_TaxOverrideDuplicate]
	-- Add the parameters for the stored procedure here
	@CountryCode AS VARCHAR(10),
	@BusinessUnit AS VARCHAR(12),
	@TaxCode AS VARCHAR(20),
	@taxid AS INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM TaxOverride
	WHERE Country_Code = @CountryCode AND Business_Unit = @BusinessUnit AND Tax_Code = @TaxCode AND Tax_Override_Id <> @taxid
END
GO
