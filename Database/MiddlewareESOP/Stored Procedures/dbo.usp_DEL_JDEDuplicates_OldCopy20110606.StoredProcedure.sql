USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_JDEDuplicates_OldCopy20110606]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_JDEDuplicates_OldCopy20110606]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_JDEDuplicates_OldCopy20110606]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DEL_JDEDuplicates_OldCopy20110606]
	@BatchNumber AS INTEGER = 0
AS
BEGIN

BEGIN TRANSACTION

      DELETE JDECus FROM CustomerDuplicates JDECus
            INNER JOIN
                  (
                  -- retrieve the ESOP customers that are at stage 1
                  SELECT JDEAccountNumber FROM CustomerDuplicates 
                        INNER JOIN Customers on Customers.id = CustomerDuplicates.CustomerId 
                        WHERE SystemNameid = 4 AND Customers.Stage = 1
                  ) 
            -- Find ESOP customers that exist as JDE versions
            as qryEsopMatches ON qryEsopMatches.JDEAccountNumber = JDECus.JDEAccountNumber 
            
            INNER JOIN Customers on Customers.id = JDECus.CustomerId 
            WHERE SystemNameid = 5 and Customers.Stage = 1
            
COMMIT TRANSACTION

END

GO
