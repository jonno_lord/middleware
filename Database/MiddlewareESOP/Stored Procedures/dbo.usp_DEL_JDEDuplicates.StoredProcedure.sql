USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_JDEDuplicates]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_JDEDuplicates]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_JDEDuplicates]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DEL_JDEDuplicates]
	@BatchNumber AS INTEGER = 0
AS
BEGIN

BEGIN TRANSACTION

      DELETE JDECus FROM CustomerDuplicates JDECus
            INNER JOIN
                  (
                  -- retrieve the ESOP customers that are at stage 1
                  SELECT JDEAccountNumber FROM CustomerDuplicates 
                        INNER JOIN Customers on Customers.id = CustomerDuplicates.CustomerId 
                        WHERE SystemNameid = 4 AND Customers.Stage = 1
                  ) 
            -- Find ESOP customers that exist as JDE versions
            as qryEsopMatches ON qryEsopMatches.JDEAccountNumber = JDECus.JDEAccountNumber 
            
            INNER JOIN Customers on Customers.id = JDECus.CustomerId 
            WHERE SystemNameid = 5 and Customers.Stage = 1
            
            EXECUTE usp_DEL_Duplicates
			IF(@@ERROR <> 0) GOTO Failure
            
            
COMMIT TRANSACTION


Failure:
	ROLLBACK TRANSACTION

END

GO
