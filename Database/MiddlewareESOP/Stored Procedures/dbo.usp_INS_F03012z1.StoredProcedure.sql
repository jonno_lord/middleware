USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012z1]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F03012z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012z1]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F03012z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F03012Z1 f
		INNER JOIN Customers c ON c.id = f.voedtn
		WHERE Stage = 1 AND Accepted = 1 AND ISNULL(JDE_Account_No,0) = 0	

	INSERT INTO dbo.F03012Z1
	(CustomerId,VOACL,VOASN,VOCRCD,VOEDBT,VOEDLN,VOEDTN,VOEDUS,VOPOPN,VOPORQ,VOSTMT,VOTNAC,VOTRAR,VOALKY,VOTXA1,VOBADT)
	SELECT 
	Id																									AS Customer, 
	CONVERT(FLOAT, dbo.fn_CreditLimit(search_Type))														AS VOACL,
	LEFT(dbo.fn_LatePayment(Search_Type), 8)															AS VOASN,
	LEFT(Preferred_Billing_Currency, 3)																	AS VOCRCD,
	LEFT(@BatchNumber, 15)																				AS VOEDBT,
	dbo.fn_LineNumber()																					AS VOEDLN,
	LEFT(CONVERT(VARCHAR(10), c.id), 22)																AS VOEDTN,
	LEFT(dbo.fn_UserName(), 10)																			AS VOEDUS,
	LEFT(Person_Entering_Contract, 10)																	AS VOPOPN,
	LEFT(Purchase_Order_Required, 1)																	AS VOPORQ,
	LEFT(dbo.fn_PrintStatement(), 1)																	AS VOSTMT,
	LEFT(dbo.fn_TransactionSetting(), 2)																AS VOTNAC,
	LEFT(dbo.fn_CustomerPaymentTerms(Search_Type), 3)													AS VOTRAR,
	LEFT(dbo.fn_LongAddressNumber(Search_Type, Esop_Prospect_No), 20)									AS VOALKY,
	LEFT(dbo.fn_TaxExemptDescription(c.Tax_Exempt_Cert_No, Search_Type, c.EC_VAT_Registration_No),10)	AS VOTXA1,
	LEFT(dbo.fn_BillingType(),1)																		AS VOBADT
	FROM Customers C
	WHERE Stage = 1 
	AND Accepted = 1 
	AND ISNULL(JDE_Account_No,0) = 0	
	
END
GO
