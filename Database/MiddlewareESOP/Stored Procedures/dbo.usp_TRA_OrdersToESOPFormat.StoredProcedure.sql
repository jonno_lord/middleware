USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToESOPFormat]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_OrdersToESOPFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToESOPFormat]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_OrdersToESOPFormat](@BatchNumber as INT) AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Rows AS INTEGER
	
	SELECT @Rows = COUNT(*) FROM Orders WHERE Stage = 4

	Update Ord SET
	Stage = 5,
	MiddlewareToMiddlewareOut = GETDATE(),
	Invoice_No = CONVERT(INT, SDDOC),
	Invoiced_By_Jde = CASE 
						WHEN Search_Type = 'CE' THEN
							CASE WHEN SDCRCD = 'GBP' THEN SDAEXP / 100 ELSE SDFEA / 100 END
						WHEN Search_Type = 'CH' THEN
							CASE WHEN SDCRCD = 'EUR' THEN SDAEXP / 100 ELSE SDFEA / 100 END
						WHEN Search_Type = 'CI' THEN
							CASE WHEN SDCRCD = 'USD' THEN SDAEXP / 100 ELSE SDFEA / 100 END
						WHEN Search_Type = 'CT' THEN
							CASE WHEN SDCRCD = 'TRY' THEN SDAEXP / 100 ELSE SDFEA / 100 END
					  END,
	Invoiced_Date = CONVERT(VARCHAR(10), dbo.Julian_to_Date(SDIVD), 102),
	--Invoice_Rejected_By_Jde = CASE WHEN SDSTTS IN ('01', '02') THEN 1 ELSE 0 END	                       
	-- There is NO SDSTTS!
	Invoice_Rejected_By_Jde = 0
	FROM Orders Ord
		INNER JOIN F42119 ON F42119.OrdersId = Ord.id
		WHERE Stage = 4                


	SELECT @Rows 

END

GO
