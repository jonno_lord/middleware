USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F0101z2]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F0101z2]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F0101Z2 f
		INNER JOIN Customers c ON c.id = f.szedtn
		WHERE Stage = 1 AND Accepted = 1 and ISNULL(JDE_Account_No,0) = 0

	INSERT INTO F0101Z2
	(CustomerId,SZAC05,SZAC08,SZADD1,SZADD2,SZADD3,SZADD4,SZADDZ,SZALKY,SZAR1,SZAR2,SZAT1,SZCM,SZCOUN,SZCTR,
	SZCTY1,SZEDBT,SZEDTN,SZEDUS,SZMLN1,SZMLNM,SZPH1,SZPH2,SZRMK,SZTAX,SZTNAC,SZTXCT,SZURAB,SZURRF)		
	SELECT 
		id																							AS CustomerId,
		LEFT(dbo.fn_CustomerType(),2)																AS SZAC05,
		LEFT(dbo.fn_CountryCode(),3)																AS SZAC08,
		LEFT(dbo.fn_RemoveSpecialChars(dbo.fn_AddressLine('Accounts Payable', Address_1, Search_Type)), 40)	AS SZADD1,
		LEFT(dbo.fn_RemoveSpecialChars(dbo.fn_AddressLine(Address_1, Address_2, Search_Type)), 40)	AS SZADD2,
		LEFT(dbo.fn_RemoveSpecialChars(dbo.fn_AddressLine(Address_2, Address_3, Search_Type)), 40)	AS SZADD3,
		LEFT(dbo.fn_AddressLine(Address_3, '', Search_Type), 40)									AS SZADD4,
		LEFT(Post_Code, 12)																			AS SZADDZ,
		LEFT(dbo.fn_LongAddressNumber(Search_Type, Esop_Prospect_No), 20)							AS SZALKY,
		LEFT(dbo.fn_DefaultTelephoneCode(phone), 6)													AS SZAR1,
		LEFT(dbo.fn_DefaultFaxCode(Fax), 6)															AS SZAR2,
		LEFT(Search_Type, 3)																		AS SZAT1,
		LEFT(dbo.fn_CreditMessage(), 2)																AS SZCM,
		LEFT(dbo.fn_County(c.County, c.Country_Code), 25)											AS SZCOUN,
		LEFT(Country_Code, 3)																		AS SZCTR,
		LEFT(City, 25)																				AS SZCTY1,
		LEFT(@BatchNumber, 15)																		AS SZEDBT,
		LEFT(CONVERT(VARCHAR(10), id), 22)															AS SZEDTN,
		LEFT(dbo.fn_UserName(), 10)																	AS SZEDUS,
		LEFT(dbo.fn_MailingName(c.Billing_Contact_Name, c.Search_Type),40)							AS SZMLN1,
		LEFT(dbo.fn_RemoveSpecialChars(Company_Name),40)											AS SZMLNM,
		LEFT(dbo.fn_RemoveSpecialChars(Phone), 20)													AS SZPH1,
		LEFT(dbo.fn_RemoveSpecialChars(Fax), 20)													AS SZPH2,
		LEFT(Email,50)																				AS SZRMK,
		LEFT(dbo.fn_VATRegistrationNumber(c.UK_Vat_Registration_No, c.EC_VAT_Registration_No), 20)	AS SZTAX,
		LEFT(dbo.fn_TransactionSetting(), 20)														AS SZTNAC,
		LEFT(Tax_Exempt_Cert_No, 20)																AS SZTXCT,
		JDE_Parent_Account_No																		AS SZURAB,
		LEFT(ESOP_Prospect_No, 15)																	AS SZURRF
		FROM Customers c
		WHERE Stage = 1 AND Accepted = 1 and ISNULL(JDE_Account_No,0) = 0

	
END

GO
