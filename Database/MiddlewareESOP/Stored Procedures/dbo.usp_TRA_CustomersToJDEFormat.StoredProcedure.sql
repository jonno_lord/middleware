USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJDEFormat]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJDEFormat]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_CustomersToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION
	
		EXECUTE dbo.usp_INS_F0101z2  @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE dbo.usp_INS_F03012z1 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
		
		--EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerPaymentTerms @SystemName = 'MiddlewareESOP'
		--IF(@@ERROR <> 0) GOTO Failure
		
		--EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerTaxCode @SystemName = 'MiddlewareESOP'
		--IF(@@ERROR <> 0) GOTO Failure

		-- Mark all customers as ready for the next stage
		UPDATE Customers SET Stage = 2, MiddlewaretoMiddlewareIn = GETDATE() 
		WHERE Stage = 1 AND Accepted = 1
		IF(@@ERROR <> 0) GOTO Failure

	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F0101z2 WHERE SZEDBT = @BatchNumber

END
GO
