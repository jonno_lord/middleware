USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JDEBusinessUnits]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_JDEBusinessUnits]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JDEBusinessUnits]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author: Elizabeth Hamlet and Jonno Lord
-- Create date: 13th May 2011
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_JDEBusinessUnits]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM MiddlewareJDE.dbo.vw_JdeBusinessUnits
	WHERE ISNULL(MCPECC,'') <> 'N'
END


GO
