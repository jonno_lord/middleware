USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToEsopFormat]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToEsopFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToEsopFormat]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_CustomersToEsopFormat]
	-- Add the parameters for the stored procedure here
AS
BEGIN

	SET NOCOUNT ON
	
	UPDATE Customers SET Stage = 5, MiddlewareToMiddlewareOut = GETDATE(), Cat_Code_28 = f55.Q1AC28
		FROM F550101 F55
		INNER JOIN Customers C ON F55.Q1URRF = C.Esop_Prospect_No
		WHERE Stage > 3

END
GO
