USE [MiddlewareESOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011_OldCopy20110606]    Script Date: 10/04/2018 10:37:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F47011_OldCopy20110606]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011_OldCopy20110606]    Script Date: 10/04/2018 10:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_INS_F47011_OldCopy20110606]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- verify the data is valid, and perform fixes
	EXECUTE dbo.usp_VER_OrderHeaders

	DELETE F47 FROM F47011 F47 
		INNER JOIN Orders ON F47.SYDCTO = Orders.Document_Type AND F47.SYDOCO = Orders.Acc_Payer_Contract_No 
		WHERE Orders.Stage = 1
	
	INSERT INTO F47011
		(SYKCOO, SYAN8,SYASN,SYCRCD,SYCRRM,SYDCTO,SYDEL1,SYDEL2,SYDOCO,SYEDBT,SYEDCT,SYEDLN,SYEDOC,SYEDST,SYEDTY,SYEKCO,SYMCU,SYOPDJ,SYPDDJ,SYSHAN,SYTPUR,SYTRDJ,SYURAB,SYVR01,SYEDSP)
		SELECT DISTINCT
		dbo.fn_CompanyFromBusinessUnit(JDE_Business_Unit)				as SYKCOO,
		qryAccount.JDE_Account_No										as SYAN8,
		LEFT(dbo.fn_LatePayment(),8)									as SYASN,
		LEFT(ISNULL(Currency_Unit, 'xxx'),3)							as SYCRCD,
		LEFT(dbo.fn_CurrencyUnit(Search_Type, Currency_Unit),1)			as SYCRRM,
		LEFT(o.Document_Type,2)											as SYDCTO,
		LEFT(o.Event_Name, 30)											as SYDEL1,
		LEFT(o.Venue_Name, 30)											as SYDEL2,
		o.Acc_Payer_Contract_No											AS SYDOCO,
		@BatchNumber													AS SYEDBT,
		LEFT(o.Document_Type,2)											as SYEDCT,
		qryAccount.Detail_Line_No * 1000000								AS SYEDLN,
		o.Acc_Payer_Contract_No											AS SYEDOC,
		dbo.fn_EDST()													as SYEDST,
		dbo.fn_EDIType()												AS SYEDTY,
		dbo.fn_CompanyNumber()											AS SYEKCO,
		dbo.fn_BusinessUnit(Jde_Business_Unit)							AS SYMCU,
		dbo.fn_DateToJulian(MIN(Event_Start_Date))						as SYOPDJ,
		dbo.fn_DateToJulian(MAX(Event_End_Date))						as SYPDDJ,
		qryAccount.JDE_Account_No										AS SYSHAN,
		dbo.fn_ActionCode()												AS SYTPUR,
		dbo.fn_DateToJulian(MIN(Status_Date))							as SYTRDJ,
		NULL															as SYURAB,
		o.Acc_Payer_Contract_No											as SYVR01,
		'N'																as SYEDSP
	FROM Orders as O
		INNER JOIN
			(SELECT MAX(Account_Code) as JDE_Account_No, Acc_Payer_Contract_No, Document_Type, MIN(Detail_Line_No) as Detail_Line_No  
				FROM Orders o1 
				WHERE Stage = 1 
				GROUP BY  Acc_Payer_Contract_No, Document_Type) 
		as qryAccount
				ON qryAccount.Acc_Payer_Contract_No = O.Acc_Payer_Contract_No AND
				qryAccount.Document_Type = O.Document_Type 

	WHERE Stage = 1 
		AND Search_Type IS NOT NULL AND Currency_Unit IS NOT NULL 
		AND o.Detail_Line_No IS NOT NULL
	GROUP BY qryAccount.JDE_Account_No, qryAccount.Detail_Line_No, Currency_Unit, Search_Type, O.Document_Type, 
		O.Acc_Payer_Contract_No, o.Event_Name, o.Venue_Name, o.JDE_Business_Unit

	UPDATE F47 Set f47.OrdersId = Ord.id FROM F47011 F47
		INNER JOIN Orders Ord ON (F47.SYEDCT = Ord.Document_Type AND F47.SYEDOC = Ord.Acc_Payer_Contract_No)
		WHERE Stage = 1


	
END

GO
