USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_ValidVatNumber]    Script Date: 10/04/2018 10:33:57 ******/
DROP PROCEDURE [dbo].[usp_INS_ValidVatNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_ValidVatNumber]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 2nd October 2010
-- Description:	Creates an entry in ValidVatNumbers table
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_ValidVatNumber] 
	-- Add the parameters for the stored procedure here
	(@CountryCode as varchar(2), @VatNumber as varchar(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT * FROM ValidVatNumbers WHERE
		CountryCode = @CountryCode AND VatNumber = @VatNumber)
		INSERT INTO dbo.ValidVatNumbers	 (CountryCode, VatNumber) VALUES (@CountryCode, @VatNumber)
END
GO
