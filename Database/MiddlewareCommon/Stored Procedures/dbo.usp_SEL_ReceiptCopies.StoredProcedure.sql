USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ReceiptCopies]    Script Date: 10/04/2018 10:33:57 ******/
DROP PROCEDURE [dbo].[usp_SEL_ReceiptCopies]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ReceiptCopies]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ReceiptCopies] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT * FROM ReceiptCopies WHERE ProcessedDate IS NULL
    
END

GO
