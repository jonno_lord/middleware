USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_SystemAccountNumber]    Script Date: 10/04/2018 10:33:57 ******/
DROP PROCEDURE [dbo].[usp_SET_SystemAccountNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_SystemAccountNumber]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Steve Parker
-- Create date: 25th February 2010
-- Description:	Sets account numbers due to a fault with LiNQ and Float numbers
-- =============================================
CREATE PROCEDURE [dbo].[usp_SET_SystemAccountNumber]
	--
	@CustomerId INT, @JdeAccountNumber AS VARCHAR(20) = NULL, @SystemName AS VARCHAR(50)
AS
BEGIN

	-- Ignore null and empty account numbers
	IF (@JdeAccountNumber IS NULL) RETURN
	IF (LEN(LTRIM(@JdeAccountNumber)) = 0) RETURN 
	
	SET NOCOUNT ON;
	IF (@SystemName = 'ESOP') UPDATE MiddlewareESOP.dbo.Customers SET Jde_Account_No = @JdeAccountNumber WHERE Id = @CustomerId
	
END


GO
