USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_OrderPaymentTerms]    Script Date: 10/04/2018 10:33:56 ******/
DROP PROCEDURE [dbo].[usp_UPD_OrderPaymentTerms]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_OrderPaymentTerms]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,B.M.Patterson>
-- Create date: <Create Date,,01/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_OrderPaymentTerms]
	-- Add the parameters for the stored procedure here
	@SystemName AS VARCHAR(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @SqlQuery AS NVARCHAR(1000)

    -- Insert statements for procedure here
    SET @SqlQuery = 'UPDATE F47 SET F47.SZPTC = PNPID 
					FROM ' + @SystemName + '.[dbo].[F47012] F47
					INNER JOIN ' +  @SystemName + '.[dbo].Orders o on F47.OrdersId = o.id
					LEFT JOIN MiddlewareJDE.dbo.F0014 ON SZPTC = PNPTC
					WHERE o.Stage = 1'
					
	EXEC dbo.sp_executesql @SqlQuery
			
END






GO
