USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_CustomerPaymentTerms]    Script Date: 10/04/2018 10:33:57 ******/
DROP PROCEDURE [dbo].[usp_UPD_CustomerPaymentTerms]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_CustomerPaymentTerms]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,B.M.Patterson>
-- Create date: <Create Date,,01/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_CustomerPaymentTerms]
	-- Add the parameters for the stored procedure here
	@SystemName AS VARCHAR(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @SqlQuery AS NVARCHAR(1000)

    -- Insert statements for procedure here
    SET @SqlQuery = 'UPDATE F03 SET F03.VOTRAR = PNPID 
					FROM ' + @SystemName + '.[dbo].[F03012Z1] F03
					INNER JOIN ' + @SystemName + '.[dbo].[Customers] c ON f03.CustomerId = c.id
					LEFT JOIN MiddlewareJDE.dbo.F0014 f14 ON f03.VOTRAR = f14.PNPTC
					WHERE c.Stage = 1'
					
	EXEC dbo.sp_executesql @SqlQuery
			
END

GO
