USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceTaxCode]    Script Date: 10/04/2018 10:33:56 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoiceTaxCode]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceTaxCode]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,B.M.Patterson>
-- Create date: <Create Date,,01/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoiceTaxCode]
	-- Add the parameters for the stored procedure here
	@SystemName AS VARCHAR(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @SqlQuery AS NVARCHAR(1000)
	DECLARE @Hack AS VARCHAR(20)
	DECLARE @Bodge AS VARCHAR(20)
	
    -- Insert statements for procedure here
	SET @SqlQuery = 'UPDATE f03 SET f03.VJTXA1 = t.TALITM
					FROM ' + @SystemName + '.[dbo].[F03b11z1] f03
					INNER JOIN  ' + @SystemName + '.[dbo].[AccountsReceivable] ar ON f03.Id = ar.F03b11z1id
					INNER JOIN  MiddlewareJDE.dbo.F4008 t ON f03.VJTXA1 = t.TATXA1
					WHERE ar.stage = 2'
					
	EXEC dbo.sp_executesql @SqlQuery
	
END


GO
