USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ValidVatNumber]    Script Date: 10/04/2018 10:33:57 ******/
DROP PROCEDURE [dbo].[usp_GET_ValidVatNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ValidVatNumber]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 2nd October 2010
-- Description:	Checks for a valid vat number
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ValidVatNumber]
	-- Add the parameters for the stored procedure here
	(@CountryCode as VARCHAR(3), @VatNumber AS VARCHAR(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM dbo.ValidVatNumbers WHERE
		CountryCode = @CountryCode AND VatNumber = @VatNumber 
END
GO
