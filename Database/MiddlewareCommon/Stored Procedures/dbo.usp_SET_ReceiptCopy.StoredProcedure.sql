USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_ReceiptCopy]    Script Date: 10/04/2018 10:33:57 ******/
DROP PROCEDURE [dbo].[usp_SET_ReceiptCopy]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_ReceiptCopy]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SET_ReceiptCopy]
	-- Add the parameters for the stored procedure here
	(@ID AS INTEGER)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE ReceiptCopies SET ProcessedDate = GETDATE() WHERE id = @ID
    
END

GO
