USE [MiddlewareCommon]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoicesPaymentTerms]    Script Date: 10/04/2018 10:33:56 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoicesPaymentTerms]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoicesPaymentTerms]    Script Date: 10/04/2018 10:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Nishi Reddy>
-- Create date: <14/01/2013>
-- Description:   <replacing VJPTC values in accordance to Oracle>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoicesPaymentTerms]
	@SystemName AS VARCHAR(25)
AS
BEGIN

	DECLARE @SqlQuery AS NVARCHAR(1000)

    -- Insert statements for procedure here
    SET @SqlQuery = 'UPDATE F03 SET F03.VJPTC = PNPID 
					FROM ' + @SystemName + '.[dbo].[F03B11Z1] F03
					INNER JOIN ' +  @SystemName + '.[dbo].AccountsReceivable ar on f03.id = ar.f03b11z1id
					LEFT JOIN MiddlewareJDE.dbo.F0014 ON VJPTC = PNPTC
					WHERE ar.Stage = 2'
					
	EXEC dbo.sp_executesql @SqlQuery
	
END



			
GO
