USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwOrdersIPSOP]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwOrdersIPSOP]
GO
/****** Object:  View [dbo].[vwOrdersIPSOP]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwOrdersIPSOP]
AS
SELECT     id, Stage, Rn_Descriptor AS Descriptor, CONVERT(Varchar(10), Rn_Create_Date, 103) AS [Create Date], CONVERT(Varchar(10), Rn_Edit_Date, 103) AS [Edit Date], 
                      Sub_No AS [Order Number], JDE_Number AS [JDE Number], Company_Text_Id AS [Company Text ID], JDE_Payment_Type AS [JDE Payment Type], 
                      JDE_Payment_Terms_Code AS [JDE Payment Terms Code], JDE_Payment_Terms_Description AS [JDE Payment Terms Description], Full_Name AS [Full Name], 
                      Currency_Code AS [Currency Code], Customer_Ref AS [Customer Ref], Client_Name AS [Client Name], CONVERT(Varchar(10), Transfer_Date, 103) AS [Transfer Date], 
                      IPSOP_Invoice_Value AS [IPSOP Invoice Value], Price, Batch_No AS [Batch Number], Status, Line_No AS [Line Number], Quantity, 
                      Invoice_Description AS [Invoice Description], Business_Unit_Code AS [Business Unit Code], Business_Unit_Description AS [Business Unit Description#], 
                      JDE_Item_Code AS [JDE Item Code], CONVERT(Varchar(10), Start_Date, 103) AS [Start Date], CONVERT(Varchar(10), End_Date, 103) AS [End Date], Duration, 
                      Issue_Date AS [Issue Date], Cover_Date AS [Cover Date], Collected_by_MW AS [Collected By MW], JDE_Invoice_Value AS [JDE Invoice Value], 
                      JDE_Invoice_Number AS [JDE Invoice Number], JDE_Invoice_Date AS [JDE Invoice Date], MW_Date AS [MW Date], Date_Check AS [Date Check], Type, 
                      Excl_VAT AS [Excl VAT], Item_Code_Type AS [Item Code Type], Invoice_Immediately AS [Invoice Immediately], Do_Not_Print_Invoice AS [Do Not Print Invoice]
FROM         MiddlewareIPSOP.dbo.Orders
GO
