USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwOrdersASOPDaltons]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwOrdersASOPDaltons]
GO
/****** Object:  View [dbo].[vwOrdersASOPDaltons]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwOrdersASOPDaltons]
AS
SELECT     id, Stage, urn_number AS [Order Number], Advertisers_Reference AS [Advertisers Reference], Agency_Discount AS [Agency Discount], Client_Name AS [Client Name], 
                      Contact_forenames AS [Contact Forenames], Contact_Surname AS [Contact Surname], Contact_Title AS [Contact Title], CONVERT(Varchar(10), Create_Time, 103) 
                      AS [Create Time], customer_name AS [Customer Name], batch_number AS [Batch Number], negotiation_currency AS [Negotiation Currency], 
                      payment_setting_short AS [Payment Setting Short], Payment_Type_short AS [Payment Type Short], Salesperson_Forenames AS [Salesperson Forenames], 
                      Salesperson_Surname AS [Salesperson Surname], urn_number AS URN, Class_card AS [Class Card], Colour_Card AS [Colour Card], Feature_Type AS [Feature Type], 
                      CONVERT(Varchar(10), insert_date, 103) AS [Insert Date], Document_Type AS [Document Type], Order_Insert_Breakdown AS [Order Insert Breakdown], 
                      Order_Insert_Breakdown_cy AS [Order Insert Breakdown cy], Page_Number AS [Page Number], Size_Card AS [Size Card], Style_Card AS [Style Card], 
                      Summary_Card AS [Summary Card], Summary_Card_short AS [Summary Card short], Text_Sample AS [Text Sample], Title_Card AS [Title Card], 
                      Title_Card_short AS [Title Card short], Line_Number AS [Line Number], Sequence_Number AS [Sequence Number]
FROM         MiddlewareASOPDaltons.dbo.Orders
GO
