USE [MiddlewareCommon]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerDuplicates'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerDuplicates'
GO
/****** Object:  View [dbo].[vwCustomerDuplicates]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwCustomerDuplicates]
GO
/****** Object:  View [dbo].[vwCustomerDuplicates]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwCustomerDuplicates]
AS
SELECT     MiddlewareESOP.dbo.CustomerDuplicates.CustomerId AS ID, MiddlewareESOP.dbo.CustomerDuplicates.CustomerScrutinyRuleId, 
                      MiddlewareESOP.dbo.CustomerDuplicates.CustomerReferenceNumber AS URN, MiddlewareESOP.dbo.CustomerDuplicates.CustomerName, 
                      MiddlewareESOP.dbo.CustomerDuplicates.AddressLine1, MiddlewareESOP.dbo.CustomerDuplicates.AddressLine2, 
                      MiddlewareESOP.dbo.CustomerDuplicates.AddressLine3, MiddlewareESOP.dbo.CustomerDuplicates.AddressLine4, '' AS City, '' AS County, 
                      MiddlewareESOP.dbo.CustomerDuplicates.Postcode, MiddlewareESOP.dbo.CustomerDuplicates.Country, MiddlewareESOP.dbo.CustomerDuplicates.Telephone, 
                      MiddlewareESOP.dbo.CustomerDuplicates.Email, MiddlewareESOP.dbo.CustomerDuplicates.URL, 'ESOP' AS DuplicateSource, 
                      MiddlewareESOP.dbo.CustomerDuplicates.JDEAccountExpired, MiddlewareESOP.dbo.CustomerDuplicates.JDEAccountNumber, 
                      MiddlewareSystem.dbo.SystemNames.SystemName
FROM         MiddlewareESOP.dbo.CustomerDuplicates INNER JOIN
                      MiddlewareSystem.dbo.SystemNames ON MiddlewareESOP.dbo.CustomerDuplicates.SystemNameid = MiddlewareSystem.dbo.SystemNames.SystemId
UNION
SELECT     MiddlewareIPSOP.dbo.CustomerDuplicates.CustomerId AS ID, MiddlewareIPSOP.dbo.CustomerDuplicates.CustomerScrutinyRuleId, 
                      MiddlewareIPSOP.dbo.CustomerDuplicates.CustomerReferenceNumber AS URN, MiddlewareIPSOP.dbo.CustomerDuplicates.CustomerName, 
                      MiddlewareIPSOP.dbo.CustomerDuplicates.AddressLine1, MiddlewareIPSOP.dbo.CustomerDuplicates.AddressLine2, 
                      MiddlewareIPSOP.dbo.CustomerDuplicates.AddressLine3, MiddlewareIPSOP.dbo.CustomerDuplicates.AddressLine4, '' AS City, '' AS County, 
                      MiddlewareIPSOP.dbo.CustomerDuplicates.Postcode, MiddlewareIPSOP.dbo.CustomerDuplicates.Country, MiddlewareIPSOP.dbo.CustomerDuplicates.Telephone, 
                      MiddlewareIPSOP.dbo.CustomerDuplicates.Email, MiddlewareIPSOP.dbo.CustomerDuplicates.URL, 'IPSOP' AS DuplicateSource, 
                      MiddlewareIPSOP.dbo.CustomerDuplicates.JDEAccountExpired, MiddlewareIPSOP.dbo.CustomerDuplicates.JDEAccountNumber, 
                      MiddlewareSystem.dbo.SystemNames.SystemName
FROM         MiddlewareIPSOP.dbo.CustomerDuplicates INNER JOIN
                      MiddlewareSystem.dbo.SystemNames ON MiddlewareIPSOP.dbo.CustomerDuplicates.SystemNameid = MiddlewareSystem.dbo.SystemNames.SystemId
UNION
SELECT     MiddlewareASOPUK.dbo.CustomerDuplicates.CustomerId AS ID, MiddlewareASOPUK.dbo.CustomerDuplicates.CustomerScrutinyRuleId, 
                      MiddlewareASOPUK.dbo.CustomerDuplicates.CustomerReferenceNumber AS URN, MiddlewareASOPUK.dbo.CustomerDuplicates.CustomerName, 
                      MiddlewareASOPUK.dbo.CustomerDuplicates.AddressLine1, MiddlewareASOPUK.dbo.CustomerDuplicates.AddressLine2, 
                      MiddlewareASOPUK.dbo.CustomerDuplicates.AddressLine3, MiddlewareASOPUK.dbo.CustomerDuplicates.AddressLine4, '' AS City, '' AS County, 
                      MiddlewareASOPUK.dbo.CustomerDuplicates.Postcode, MiddlewareASOPUK.dbo.CustomerDuplicates.Country, 
                      MiddlewareASOPUK.dbo.CustomerDuplicates.Telephone, MiddlewareASOPUK.dbo.CustomerDuplicates.Email, MiddlewareASOPUK.dbo.CustomerDuplicates.URL, 
                      'ASOPUK' AS DuplicateSource, MiddlewareASOPUK.dbo.CustomerDuplicates.JDEAccountExpired, MiddlewareASOPUK.dbo.CustomerDuplicates.JDEAccountNumber, 
                      MiddlewareSystem.dbo.SystemNames.SystemName
FROM         MiddlewareASOPUK.dbo.CustomerDuplicates INNER JOIN
                      MiddlewareSystem.dbo.SystemNames ON MiddlewareASOPUK.dbo.CustomerDuplicates.SystemNameid = MiddlewareSystem.dbo.SystemNames.SystemId
UNION
SELECT     MiddlewareASOPDaltons.dbo.CustomerDuplicates.CustomerId AS ID, MiddlewareASOPDaltons.dbo.CustomerDuplicates.CustomerScrutinyRuleId, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.CustomerReferenceNumber AS URN, MiddlewareASOPDaltons.dbo.CustomerDuplicates.CustomerName, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.AddressLine1, MiddlewareASOPDaltons.dbo.CustomerDuplicates.AddressLine2, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.AddressLine3, MiddlewareASOPDaltons.dbo.CustomerDuplicates.AddressLine4, '' AS City, '' AS County, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.Postcode, MiddlewareASOPDaltons.dbo.CustomerDuplicates.Country, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.Telephone, MiddlewareASOPDaltons.dbo.CustomerDuplicates.Email, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.URL, 'ASOPDaltons' AS DuplicateSource, MiddlewareASOPDaltons.dbo.CustomerDuplicates.JDEAccountExpired, 
                      MiddlewareASOPDaltons.dbo.CustomerDuplicates.JDEAccountNumber, MiddlewareSystem.dbo.SystemNames.SystemName
FROM         MiddlewareASOPDaltons.dbo.CustomerDuplicates INNER JOIN
                      MiddlewareSystem.dbo.SystemNames ON MiddlewareASOPDaltons.dbo.CustomerDuplicates.SystemNameid = MiddlewareSystem.dbo.SystemNames.SystemId
UNION
SELECT     MiddlewareASOPHolland.dbo.CustomerDuplicates.CustomerId AS ID, MiddlewareASOPHolland.dbo.CustomerDuplicates.CustomerScrutinyRuleId, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.CustomerReferenceNumber AS URN, MiddlewareASOPHolland.dbo.CustomerDuplicates.CustomerName, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.AddressLine1, MiddlewareASOPHolland.dbo.CustomerDuplicates.AddressLine2, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.AddressLine3, MiddlewareASOPHolland.dbo.CustomerDuplicates.AddressLine4, '' AS City, '' AS County, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.Postcode, MiddlewareASOPHolland.dbo.CustomerDuplicates.Country, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.Telephone, MiddlewareASOPHolland.dbo.CustomerDuplicates.Email, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.URL, 'ASOPHolland' AS DuplicateSource, MiddlewareASOPHolland.dbo.CustomerDuplicates.JDEAccountExpired, 
                      MiddlewareASOPHolland.dbo.CustomerDuplicates.JDEAccountNumber, MiddlewareSystem.dbo.SystemNames.SystemName
FROM         MiddlewareASOPHolland.dbo.CustomerDuplicates INNER JOIN
                      MiddlewareSystem.dbo.SystemNames ON MiddlewareASOPHolland.dbo.CustomerDuplicates.SystemNameid = MiddlewareSystem.dbo.SystemNames.SystemId

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[22] 4[39] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerDuplicates (MiddlewareESOP.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 315
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "SystemNames (MiddlewareSystem.dbo)"
            Begin Extent = 
               Top = 6
               Left = 298
               Bottom = 110
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2970
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerDuplicates'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerDuplicates'
GO
