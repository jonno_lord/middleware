USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwOrdersEventsForce]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwOrdersEventsForce]
GO
/****** Object:  View [dbo].[vwOrdersEventsForce]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwOrdersEventsForce]
AS
SELECT     Invoice_Orders_Id AS id, Line_Number AS [Line Number], Customer_Urn AS [Customer URN], Customer_name AS [Customer Name], Address, Town, County, Postcode, 
                      Country, Country_Code AS [Country Code], Customer_Reference AS [Customer Reference], Order_Date AS [Order Date], Customer_Type AS [Customer Type], 
                      VAT_Registration_No AS [VAT Registration Number], Charity_Number AS [Charity Number], Payment_Method AS [Payment Method], Currency, Net_price AS [Net Price], 
                      Gross_price AS [Gross Price], Net_price_sterling AS [Net Price Sterling], Gross_price_sterling AS [Gross Price Sterling], 
                      Credit_Controller_name AS [Credit Controller Name], Credit_Controller_phone AS [Credit Controller Phone], Batch_number AS [Batch Number], 
                      JDE_Account_No AS [JDE Account Number], Tax_Rate_Code AS [Tax Rate Code], Business_Unit AS [Business Unit], Object_account AS [Object Account], 
                      Subsidiary_account AS [Subsidiary Account], subledger, Document_Type AS [Document Type], Document_Number AS [Document Number], 
                      Original_Invoice_Number AS [Original Invoice Number], Event_Name AS [Event Name], Event_Start_Date AS [Event Start Date], CodID, 
                      MW_Batch_File_Name AS [MW Batch File Name], PurchaseID AS [Order Number], CONVERT(Varchar(10), Invoice_date, 103) AS [Invoice Date], 
                      Booking_Reference AS [Booking Reference], Tax_rate_Description AS [Tax Rate Description]
FROM         MiddlewareEventsForce.dbo.Invoices

GO
