USE [MiddlewareCommon]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerViolations'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerViolations'
GO
/****** Object:  View [dbo].[vwCustomerViolations]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwCustomerViolations]
GO
/****** Object:  View [dbo].[vwCustomerViolations]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwCustomerViolations]
AS
SELECT     MiddlewareESOP.dbo.CustomerViolations.customerId AS ID, 'ESOP' AS SystemName, MiddlewareESOP.dbo.CustomerViolations.CustomerScrutinyRuleid, 
                      MiddlewareESOP.dbo.CustomerViolations.Violation, MiddlewareESOP.dbo.CustomerScrutinyRules.Description
FROM         MiddlewareESOP.dbo.CustomerViolations INNER JOIN
                      MiddlewareESOP.dbo.CustomerScrutinyRules ON 
                      MiddlewareESOP.dbo.CustomerViolations.CustomerScrutinyRuleid = MiddlewareESOP.dbo.CustomerScrutinyRules.id
UNION
SELECT     MiddlewareIPSOP.dbo.CustomerViolations.customerId AS ID, 'IPSOP' AS SystemName, MiddlewareIPSOP.dbo.CustomerViolations.CustomerScrutinyRuleid, 
                      MiddlewareIPSOP.dbo.CustomerViolations.Violation, MiddlewareIPSOP.dbo.CustomerScrutinyRules.Description
FROM         MiddlewareIPSOP.dbo.CustomerViolations INNER JOIN
                      MiddlewareIPSOP.dbo.CustomerScrutinyRules ON 
                      MiddlewareIPSOP.dbo.CustomerViolations.CustomerScrutinyRuleid = MiddlewareIPSOP.dbo.CustomerScrutinyRules.id
UNION
SELECT     MiddlewareASOPUK.dbo.CustomerViolations.customerId AS ID, 'ASOPUK' AS SystemName, MiddlewareASOPUK.dbo.CustomerViolations.CustomerScrutinyRuleid, 
                      MiddlewareASOPUK.dbo.CustomerViolations.Violation, MiddlewareASOPUK.dbo.CustomerScrutinyRules.Description
FROM         MiddlewareASOPUK.dbo.CustomerViolations INNER JOIN
                      MiddlewareASOPUK.dbo.CustomerScrutinyRules ON 
                      MiddlewareASOPUK.dbo.CustomerViolations.CustomerScrutinyRuleid = MiddlewareASOPUK.dbo.CustomerScrutinyRules.id
UNION
SELECT     MiddlewareASOPDaltons.dbo.CustomerViolations.customerId AS ID, 'ASOPDaltons' AS SystemName, 
                      MiddlewareASOPDaltons.dbo.CustomerViolations.CustomerScrutinyRuleid, MiddlewareASOPDaltons.dbo.CustomerViolations.Violation, 
                      MiddlewareASOPDaltons.dbo.CustomerScrutinyRules.Description
FROM         MiddlewareASOPDaltons.dbo.CustomerViolations INNER JOIN
                      MiddlewareASOPDaltons.dbo.CustomerScrutinyRules ON 
                      MiddlewareASOPDaltons.dbo.CustomerViolations.CustomerScrutinyRuleid = MiddlewareASOPDaltons.dbo.CustomerScrutinyRules.id
UNION
SELECT     MiddlewareASOPHolland.dbo.CustomerViolations.customerId AS ID, 'ASOPHolland' AS SystemName, 
                      MiddlewareASOPHolland.dbo.CustomerViolations.CustomerScrutinyRuleid, MiddlewareASOPHolland.dbo.CustomerViolations.Violation, 
                      MiddlewareASOPHolland.dbo.CustomerScrutinyRules.Description
FROM         MiddlewareASOPHolland.dbo.CustomerViolations INNER JOIN
                      MiddlewareASOPHolland.dbo.CustomerScrutinyRules ON 
                      MiddlewareASOPHolland.dbo.CustomerViolations.CustomerScrutinyRuleid = MiddlewareASOPHolland.dbo.CustomerScrutinyRules.id

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerViolations (MiddlewareESOP.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 193
               Right = 241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CustomerScrutinyRules (MiddlewareESOP.dbo)"
            Begin Extent = 
               Top = 6
               Left = 279
               Bottom = 186
               Right = 553
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2025
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2295
         Alias = 1905
         Table = 3510
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerViolations'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerViolations'
GO
