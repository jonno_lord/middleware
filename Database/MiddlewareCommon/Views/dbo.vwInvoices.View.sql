USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwInvoices]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwInvoices]
GO
/****** Object:  View [dbo].[vwInvoices]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwInvoices]
AS
SELECT     ID, insert_date AS IssueDate, urn_number AS URN, customer_name AS CustomerName, Title_Card_short AS Code, Title_Card AS Description, 
                      batch_number AS BatchNumber, Passed_To_MW, Client_Name, Order_Status_Code
FROM         MiddlewareASOPUK.dbo.ASOP_Invoice_Extraction
GO
