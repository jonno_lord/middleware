USE [MiddlewareCommon]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomers'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomers'
GO
/****** Object:  View [dbo].[vwCustomers]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwCustomers]
GO
/****** Object:  View [dbo].[vwCustomers]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwCustomers]
AS
SELECT     id, 'ESOP' AS SystemName, ESOP_Prospect_No AS URN, STR(JDE_Account_No) AS JDEAccountNumber, 'Company' AS CustomerType, 
                      Company_Name AS CustomerName, Address_1 AS AddressLine1, Address_2 AS AddressLine2, Address_3 AS AddressLine3, '' AS AddressLine4, Stage, 
                      SourceToMiddleware, MiddlewareScrutiny, Accepted, Rejected, AcceptedInScrutiny, RejectedInScrutiny, MiddlewaretoMiddlewareIn, MiddlewareToJDE, 
                      RejectedFromJDE, JDEToMiddleware, MiddlewareToMiddlewareOut, MiddlewareToSource, NumberOfScrutinyRulesBroken, Email, Phone, Post_Code AS PostCode, 
                      dbo.fn_GetStageDescription(Stage) AS EntryStatus, County, City, Country_Code AS Country, ISNULL(Url, '') AS URL, '' AS BatchNumber, 
                      Contract_number AS ContractNumber, Contract_Value AS OrderAmount, '' AS PaymentSetting, Event_Name AS ProductName, Salesperson_Name AS SalesPerson, 
                      Tax_Exempt_Cert_No AS TaxExemptNumber, '' AS TradingStyle, EC_VAT_Registration_No AS VATNumber, Contract_Currency AS Currency, 
                      Search_Type AS SearchType, Event_Start_Date AS EventStartDate
FROM         MiddlewareESOP.dbo.Customers
UNION
SELECT     cus.id, 'IPSOP' AS SystemName, Company_Text_ID AS URN, STR(Jde_Account_Number) AS JDEAccountNumber, 'Company' AS CustomerType, 
                      Company_Name AS CustomerName, Address_1 AS AddressLine1, Address_2 AS AddressLine2, Address_3 AS AddressLine3, Address_4 AS AddressLine4, Stage, 
                      SourceToMiddleware, MiddlewareScrutiny, Accepted, Rejected, AcceptedInScrutiny, RejectedInScrutiny, MiddlewaretoMiddlewareIn, MiddlewareToJDE, 
                      RejectedFromJDE, JDEToMiddleware, MiddlewareToMiddlewareOut, MiddlewareToSource, NumberOfScrutinyRulesBroken, '' AS Email, Phone, 
                      Post_Code AS PostCode, dbo.fn_GetStageDescription(Stage) AS EntryStatus, County, City, Country, '' AS URL, '' AS BatchNumber, '' AS ContractNumber, 
                      Quote_Total AS OrderAmount, '' AS PaymentSetting, bum.Business_unit + ' - ' + f06.MCDL01 AS ProductName, bum.Admin_Contact_Name AS SalesPerson, 
                      Tax_Exempt_Cert_No AS TaxExemptNumber, '' AS TradingStyle, VAT_Number AS VATNumber, 'GBP' AS Currency, 'C' AS SearchType, '' AS EventStartDate
FROM         MiddlewareIPSOP.dbo.Customers cus LEFT JOIN
                      MiddlewareIPSOP.dbo.BusinessUnitMappings bum ON cus.Business_Unit_Code = bum.Business_Unit LEFT JOIN
                      MiddlewareJDE.dbo.F0006 f06 ON bum.business_unit = LTRIM(f06.mcmcu)
UNION
SELECT     id, 'ASOPUK' AS SystemName, CONVERT(varchar(50), Urn_Number) AS URN, STR(JDE_Account_Number) AS JDEAccountNumber, Customer_Type AS CustomerType, 
                      Company_Name AS CustomerName, Address1 AS AddressLine1, Address2 AS AddressLine2, Address3 AS AddressLine3, Address4 AS AddressLine4, Stage, 
                      SourceToMiddleware, MiddlewareScrutiny, Accepted, Rejected, AcceptedInScrutiny, RejectedInScrutiny, MiddlewaretoMiddlewareIn, MiddlewareToJDE, 
                      RejectedFromJDE, JDEToMiddleware, MiddlewareToMiddlewareOut, MiddlewareToSource, NumberOfScrutinyRulesBroken, Email_Address AS Email, 
                      dbo.fn_GetTelephoneFormat(Telephone_Prefix, Telephone_Std, Telephone_Number) AS Phone, PostCode, dbo.fn_GetStageDescription(Stage) AS EntryStatus, County, 
                      Town AS City, Country, '' AS URL, CONVERT(varchar(10), Batch_Number) AS BatchNumber, CONVERT(varchar(50), order_urn) AS ContractNumber, 
                      Negotiation_Gross_Price AS OrderAmount, Payment_Setting AS PaymentSetting, TitleCard_Name AS ProductName, Operator_Name AS SalesPerson, 
                      Tax_Exempt_Number AS TaxExemptNumber, Trading_Style AS TradingStyle, VAT_Reg_No AS VATNumber, Negotiation_Currency AS Currency, 'C' AS SearchType, 
                      '' AS EventStartDate
FROM         MiddlewareASOPUK.dbo.Customers
UNION
SELECT     id, 'ASOPDaltons' AS SystemName, CONVERT(varchar(50), Urn_Number) AS URN, STR(JDE_Account_Number) AS JDEAccountNumber, 
                      Customer_Type AS CustomerType, Company_Name AS CustomerName, Address1 AS AddressLine1, Address2 AS AddressLine2, Address3 AS AddressLine3, 
                      Address4 AS AddressLine4, Stage, SourceToMiddleware, MiddlewareScrutiny, Accepted, Rejected, AcceptedInScrutiny, RejectedInScrutiny, MiddlewaretoMiddlewareIn, 
                      MiddlewareToJDE, RejectedFromJDE, JDEToMiddleware, MiddlewareToMiddlewareOut, MiddlewareToSource, NumberOfScrutinyRulesBroken, 
                      Email_Address AS Email, dbo.fn_GetTelephoneFormat(Telephone_Prefix, Telephone_Std, Telephone_Number) AS Phone, PostCode, 
                      dbo.fn_GetStageDescription(Stage) AS EntryStatus, County, Town AS City, Country, '' AS URL, CONVERT(varchar(10), Batch_Number) AS BatchNumber, 
                      CONVERT(varchar(50), Order_urn) AS ContractNumber, Negotiation_Gross_Price AS OrderAmount, Payment_Setting AS PaymentSetting, 
                      TitleCard_Name AS ProductName, Operator_Name AS SalesPerson, Tax_Exempt_Number AS TaxExemptNumber, Trading_Style AS TradingStyle, 
                      VAT_Reg_No AS VATNumber, Negotiation_Currency AS Currency, 'CC' AS SearchType, '' AS EventStartDate
FROM         MiddlewareASOPDaltons.dbo.Customers
UNION
SELECT     id, 'EventsForce' AS SystemName, CONVERT(varchar(50), Urn_Number) AS URN, STR(JDE_Account_Number) AS JDEAccountNumber, 'Company' AS CustomerType, 
                      Company_Name AS CustomerName, Address_1 AS AddressLine1, Address_2 AS AddressLine2, Address_3 AS AddressLine3, Address_4 AS AddressLine4, 0 AS Stage, 
                      SourceToMiddleware, MiddlewareScrutiny, NULL AS Accepted, NULL AS Rejected, NULL AS AcceptedInScrutiny, NULL AS RejectedInScrutiny, MiddlewaretoMiddlewareIn, 
                      MiddlewareToJDE, RejectedFromJDE, JDEToMiddleware, MiddlewareToMiddlewareOut, MiddlewareToSource, NULL AS NumberOfScrutinyRulesBroken, 
                      Email_address AS Email, Telephone_Number AS Phone, Postcode, dbo.fn_GetStageDescription(Stage) AS EntryStatus, County, Town AS City, Country, '' AS URL, 
                      CONVERT(varchar(10), Batch_Number) AS BatchNumber, '' AS ContractNumber, Negotiation_Gross_Price AS OrderAmount, '' AS PaymentSetting, 
                      Event_Name AS ProductName, (First_Name + ' ' + Last_Name) AS SalesPerson, Tax_Exempt_Number AS TaxExemptNumber, '' AS TradingStyle, 
                      VAT_Reg_No AS VATNumber, Negotiation_Currency AS Currency, 'CN' AS SearchType, Event_Start_Date AS EventStartDate
FROM         MiddlewareEventsForce.dbo.Customers
UNION
SELECT     id, 'ASOPHolland' AS SystemName, CONVERT(varchar(50), Urn_Number) AS URN, STR(JDE_Account_Number) AS JDEAccountNumber, 
                      Customer_Type AS CustomerType, Company_Name AS CustomerName, Address1 AS AddressLine1, Address2 AS AddressLine2, Address3 AS AddressLine3, 
                      Address4 AS AddressLine4, Stage, SourceToMiddleware, MiddlewareScrutiny, Accepted, Rejected, AcceptedInScrutiny, RejectedInScrutiny, MiddlewaretoMiddlewareIn, 
                      MiddlewareToJDE, RejectedFromJDE, JDEToMiddleware, MiddlewareToMiddlewareOut, MiddlewareToSource, NumberOfScrutinyRulesBroken, 
                      Email_Address AS Email, dbo.fn_GetTelephoneFormat(Telephone_Prefix, Telephone_Std, Telephone_Number) AS Phone, PostCode, 
                      dbo.fn_GetStageDescription(Stage) AS EntryStatus, County, Town AS City, Country, '' AS URL, CONVERT(varchar(10), Batch_Number) AS BatchNumber, 
                      CONVERT(varchar(50), order_urn) AS ContractNumber, Negotiation_Gross_Price AS OrderAmount, Payment_Setting AS PaymentSetting, 
                      TitleCard_Name AS ProductName, Operator_Name AS SalesPerson, Tax_Exempt_Number AS TaxExemptNumber, Trading_Style AS TradingStyle, 
                      VAT_Reg_No AS VATNumber, Negotiation_Currency AS Currency, 'CF' AS SearchType, '' AS EventStartDate
FROM         MiddlewareASOPHolland.dbo.Customers
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[24] 4[22] 2[45] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 46
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1905
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 7905
         Alias = 3705
         Table = 3420
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomers'
GO
