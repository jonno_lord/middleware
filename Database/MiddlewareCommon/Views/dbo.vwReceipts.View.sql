USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwReceipts]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwReceipts]
GO
/****** Object:  View [dbo].[vwReceipts]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwReceipts]
AS
SELECT     'ASOPUK' AS SystemName, Credit_Card_Orders_Id, Source_to_MW, MW_to_MW_Conversion, MW_to_MW_Out, MW_to_JDE, Order_Urn, Line_Number, 
                      Customer_Urn, Customer_name, Address, Town, County, Postcode, Country, Country_Code, Country_EC, Placed_By, Customer_Reference, 
                      Receipt_Number AS ReceiptNumber, Receipt_Date, Publication, Publication_Code, Size, Colour, Summary, Classification, Order_Date, Customer_Type, 
                      VAT_Registration_No, Charity_Number, Payment_Method, Currency, Net_price, Gross_price, Net_price_sterling, Gross_price_sterling, Text_Sample, Issue_date, 
                      Subledger_Issue_date, Original_Issue_Date, Credit_Controller_name, Credit_Controller_phone, Credit_Controller_fax, Credit_Controller_email, Sent_To_JDE, 
                      Batch_number, JDE_Account_No, Receipt_required, Jersey, Tax_Rate_Code, Tax_Rate_Description, Output_net_price, Output_VAT_amount, Output_gross_price, 
                      Output_net_sterling, Output_VAT_sterling, Output_gross_sterling, Business_Unit, Object_account, Subsidiary_account, subledger, Logo_Reference_ID, Produce_Copy, 
                      Actual_Insert_Description, Page_Number, of_Page_Number, Document_Type, Document_Number, Invalid_Tax_From_ASOP, Hub_Company, Old_VAT
FROM         MiddlewareASOPUK.dbo.vw_Credit_Card_Orders
UNION
SELECT     'ASOPDaltons' AS SystemName, Credit_Card_Orders_Id, Source_to_MW, MW_to_MW_Conversion, MW_to_MW_Out, MW_to_JDE, Order_Urn, Line_Number, 
                      Customer_Urn, Customer_name, Address, Town, County, Postcode, Country, Country_Code, Country_EC, Placed_By, Customer_Reference, 
                      Receipt_Number AS ReceiptNumber, Receipt_Date, Publication, Publication_Code, Size, Colour, Summary, Classification, Order_Date, Customer_Type, 
                      VAT_Registration_No, Charity_Number, Payment_Method, Currency, Net_price, Gross_price, Net_price_sterling, Gross_price_sterling, Text_Sample, Issue_date, 
                      Subledger_Issue_date, Original_Issue_Date, Credit_Controller_name, Credit_Controller_phone, Credit_Controller_fax, Credit_Controller_email, Sent_To_JDE, 
                      Batch_number, JDE_Account_No, Receipt_required, Jersey, Tax_Rate_Code, Tax_Rate_Description, Output_net_price, Output_VAT_amount, Output_gross_price, 
                      Output_net_sterling, Output_VAT_sterling, Output_gross_sterling, Business_Unit, Object_account, Subsidiary_account, subledger, Logo_Reference_ID, Produce_Copy, 
                      Actual_Insert_Description, Page_Number, of_Page_Number, Document_Type, Document_Number, Invalid_Tax_From_ASOP, Hub_Company, Old_VAT
FROM         MiddlewareASOPDaltons.dbo.vw_Credit_Card_Orders

UNION
SELECT     'ASOPHolland' AS SystemName, Credit_Card_Orders_Id, Source_to_MW, MW_to_MW_Conversion, MW_to_MW_Out, MW_to_JDE, Order_Urn, Line_Number, 
                      Customer_Urn, Customer_name, Address, Town, County, Postcode, Country, Country_Code, Country_EC, Placed_By, Customer_Reference, 
                      Receipt_Number AS ReceiptNumber, Receipt_Date, Publication, Publication_Code, Size, Colour, Summary, Classification, Order_Date, Customer_Type, 
                      VAT_Registration_No, Charity_Number, Payment_Method, Currency, Net_price, Gross_price, Net_price_sterling, Gross_price_sterling, Text_Sample, Issue_date, 
                      Subledger_Issue_date, Original_Issue_Date, Credit_Controller_name, Credit_Controller_phone, Credit_Controller_fax, Credit_Controller_email, Sent_To_JDE, 
                      Batch_number, JDE_Account_No, Receipt_required, Jersey, Tax_Rate_Code, Tax_Rate_Description, Output_net_price, Output_VAT_amount, Output_gross_price, 
                      Output_net_sterling, Output_VAT_sterling, Output_gross_sterling, Business_Unit, Object_account, Subsidiary_account, subledger, Logo_Reference_ID, Produce_Copy, 
                      Actual_Insert_Description, Page_Number, of_Page_Number, Document_Type, Document_Number, Invalid_Tax_From_ASOP, Hub_Company, Old_VAT
FROM         MiddlewareASOPHolland.dbo.vw_Credit_Card_Orders

GO
