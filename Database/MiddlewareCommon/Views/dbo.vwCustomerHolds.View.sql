USE [MiddlewareCommon]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerHolds'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerHolds'
GO
/****** Object:  View [dbo].[vwCustomerHolds]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwCustomerHolds]
GO
/****** Object:  View [dbo].[vwCustomerHolds]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwCustomerHolds]
AS
SELECT     MiddlewareESOP.dbo.CustomerHolds.CustomerId AS ID, 'ESOP' AS SystemName, MiddlewareESOP.dbo.Customers.Company_Name AS CustomerName, 
                      MiddlewareESOP.dbo.CustomerHolds.Hold, MiddlewareESOP.dbo.CustomerHolds.HoldDate, MiddlewareESOP.dbo.CustomerHolds.Reason, 
                      MiddlewareESOP.dbo.CustomerHolds.UserName, '' AS EntityNumber
FROM         MiddlewareESOP.dbo.CustomerHolds INNER JOIN
                      MiddlewareESOP.dbo.Customers ON MiddlewareESOP.dbo.CustomerHolds.CustomerId = MiddlewareESOP.dbo.Customers.id
UNION
SELECT     MiddlewareIPSOP.dbo.CustomerHolds.CustomerId AS ID, 'IPSOP' AS SystemName, MiddlewareIPSOP.dbo.Customers.Company_Name AS CustomerName, 
                      MiddlewareIPSOP.dbo.CustomerHolds.Hold, MiddlewareIPSOP.dbo.CustomerHolds.HoldDate, MiddlewareIPSOP.dbo.CustomerHolds.Reason, 
                      MiddlewareIPSOP.dbo.CustomerHolds.UserName, '' AS EntityNumber
FROM         MiddlewareIPSOP.dbo.CustomerHolds INNER JOIN
                      MiddlewareIPSOP.dbo.Customers ON MiddlewareIPSOP.dbo.CustomerHolds.CustomerId = MiddlewareIPSOP.dbo.Customers.id
UNION
SELECT     MiddlewareASOPUK.dbo.CustomerHolds.CustomerId AS ID, 'ASOPUK' AS SystemName, MiddlewareASOPUK.dbo.Customers.Company_Name AS CustomerName, 
                      MiddlewareASOPUK.dbo.CustomerHolds.Hold, MiddlewareASOPUK.dbo.CustomerHolds.HoldDate, MiddlewareASOPUK.dbo.CustomerHolds.Reason, 
                      MiddlewareASOPUK.dbo.CustomerHolds.UserName, '' AS EntityNumber
FROM         MiddlewareASOPUK.dbo.CustomerHolds INNER JOIN
                      MiddlewareASOPUK.dbo.Customers ON MiddlewareASOPUK.dbo.CustomerHolds.CustomerId = MiddlewareASOPUK.dbo.Customers.id
UNION
SELECT     MiddlewareASOPDaltons.dbo.CustomerHolds.CustomerId AS ID, 'ASOPDaltons' AS SystemName, 
                      MiddlewareASOPDaltons.dbo.Customers.Company_Name AS CustomerName, MiddlewareASOPDaltons.dbo.CustomerHolds.Hold, 
                      MiddlewareASOPDaltons.dbo.CustomerHolds.HoldDate, MiddlewareASOPDaltons.dbo.CustomerHolds.Reason, 
                      MiddlewareASOPDaltons.dbo.CustomerHolds.UserName, '' AS EntityNumber
FROM         MiddlewareASOPDaltons.dbo.CustomerHolds INNER JOIN
                      MiddlewareASOPDaltons.dbo.Customers ON MiddlewareASOPDaltons.dbo.CustomerHolds.CustomerId = MiddlewareASOPDaltons.dbo.Customers.id
UNION
SELECT     MiddlewareASOPHolland.dbo.CustomerHolds.CustomerId AS ID, 'ASOPHolland' AS SystemName, 
                      MiddlewareASOPHolland.dbo.Customers.Company_Name AS CustomerName, MiddlewareASOPHolland.dbo.CustomerHolds.Hold, 
                      MiddlewareASOPHolland.dbo.CustomerHolds.HoldDate, MiddlewareASOPHolland.dbo.CustomerHolds.Reason, 
                      MiddlewareASOPHolland.dbo.CustomerHolds.UserName, '' AS EntityNumber
FROM         MiddlewareASOPHolland.dbo.CustomerHolds INNER JOIN
                      MiddlewareASOPHolland.dbo.Customers ON MiddlewareASOPHolland.dbo.CustomerHolds.CustomerId = MiddlewareASOPHolland.dbo.Customers.id

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[22] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerHolds (MiddlewareESOP.dbo)"
            Begin Extent = 
               Top = 9
               Left = 77
               Bottom = 246
               Right = 375
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwCustomers"
            Begin Extent = 
               Top = 6
               Left = 413
               Bottom = 125
               Right = 649
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3435
         Alias = 1380
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerHolds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerHolds'
GO
