USE [MiddlewareCommon]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOrdersESOP'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOrdersESOP'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOrdersESOP'
GO
/****** Object:  View [dbo].[vwOrdersESOP]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwOrdersESOP]
GO
/****** Object:  View [dbo].[vwOrdersESOP]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwOrdersESOP]
AS
SELECT     id, Stage, Rn_Descriptor AS Descriptor, CONVERT(Varchar(10), Rn_Create_Date, 103) AS [Create Date], CONVERT(Varchar(10), Rn_Edit_Date, 103) AS [Edit Date], 
                      Acc_Payer_Contract_No AS [Order Number], Document_Type AS [Document Type], Account_Code AS [Account Code], JDE_Business_Unit AS [JDE Business Unit], 
                      Currency_Unit AS [Currency Unit], CONVERT(Varchar(10), Status_Date, 103) AS [Status Date], Event_Name AS [Event Name], Venue_Name AS [Venue Name], 
                      CONVERT(Varchar(10), Event_Start_Date, 103) AS [Event Start Date], CONVERT(Varchar(10), Event_End_Date, 103) AS [Event End Date], Action_Code AS [Action Code], 
                      Detail_Line_No * 1000 AS [Detail Line No], Payment_Percent AS [Payment Percent], COL_Value AS Value, Invoiced_by_ESOP AS [Invoiced by ESOP], 
                      Item_Master_No AS [Item Master No], Event_Product_Name AS [Event Product Name], CONVERT(Varchar(10), Batch_Run_Date, 103) AS [Batch Run Date], 
                      Payment_Terms AS [Payment Terms], Payment_Instrument AS [Payment Instrument], Tax_Code AS [Tax Code], Issue_Date AS [Issue Date], Hall_Prefix AS [Hall Prefix],
                       Stand_Number AS [Stand Number], Stand_Size AS [Stand Size], Cust_PO_No AS [Cust PO No], Person_Placing_Order AS [Person Placing Order], 
                      Contract_No AS [Contract No], Batch_No AS [Batch Number], Sub_Ledger_Type AS [Sub Ledger Type], Units_Order_Quantity AS [Units Order Quantity], 
                      Invoice_No AS [Invoice No], Invoiced_By_JDE AS [Invoiced By JDE], CONVERT(Varchar(10), Invoiced_Date, 103) AS [Invoiced Date], 
                      Invoice_Rejected_By_JDE AS [Invoice Rejected By JDE], Invoice_On_Hold_By_JDE AS [Invoice On Hold By JDE], Tax_Exempt_Cert_No AS [Tax Exempt Cert No], 
                      Passed_from_ESOP_to_MW AS [Passed from ESOP to MW], Search_Type AS [Search Type]
FROM         MiddlewareESOP.dbo.Orders
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[17] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Orders (MiddlewareESOP.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 286
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 45
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4695
         Alias = 1995
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOrdersESOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOrdersESOP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOrdersESOP'
GO
