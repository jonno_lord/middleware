USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwReceiptsAggregate]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwReceiptsAggregate]
GO
/****** Object:  View [dbo].[vwReceiptsAggregate]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwReceiptsAggregate]
AS
SELECT     TOP (100) PERCENT Currency, SystemName, CONVERT(varchar(50), Customer_Urn) AS URN, Customer_name AS CustomerName, SUM(Gross_price_sterling) 
                      AS Amount, CONVERT(varchar(50), Order_Urn) AS OrderNumber, Postcode, MW_to_JDE AS Processed, Address, Town, County, Country, CONVERT(varchar(50), 
                      ReceiptNumber) AS ReceiptNumber
FROM         dbo.vwReceipts
WHERE     (ReceiptNumber IS NOT NULL)
GROUP BY ReceiptNumber, Customer_Urn, Customer_name, Currency, Order_Urn, Postcode, MW_to_JDE, Address, Town, County, Country, SystemName
ORDER BY CustomerName
GO
