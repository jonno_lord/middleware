USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwBatchReceipts]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwBatchReceipts]
GO
/****** Object:  View [dbo].[vwBatchReceipts]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwBatchReceipts]
AS
SELECT     TOP (100) PERCENT Batch_number AS ID, Source_to_MW AS Extracted, SystemName, COUNT(Line_Number) AS LinesTotal, Currency, SUM(Gross_price) AS Amount, 
                      COUNT(DISTINCT Order_Urn) AS ReceiptCount
FROM         dbo.vwReceipts AS rec
WHERE     (Receipt_required = 1) AND (MW_to_JDE IS NULL) AND (Batch_number IS NOT NULL)
GROUP BY Batch_number, SystemName, Source_to_MW, MW_to_JDE, Currency
GO
