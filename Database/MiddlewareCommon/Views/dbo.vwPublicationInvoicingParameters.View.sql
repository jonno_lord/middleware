USE [MiddlewareCommon]
GO
/****** Object:  View [dbo].[vwPublicationInvoicingParameters]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwPublicationInvoicingParameters]
GO
/****** Object:  View [dbo].[vwPublicationInvoicingParameters]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwPublicationInvoicingParameters]
AS
SELECT     'ASOPUK' AS SystemName, SelectedBy, TitleCard, InsertDate, RunDate, Extracted, BatchNumber
FROM         MiddlewareASOPUK.dbo.PublicationInvoicingParameters
UNION
SELECT     'ASOPDaltons' AS SystemName, SelectedBy, TitleCard, InsertDate, RunDate, Extracted, BatchNumber
FROM         MiddlewareASOPDaltons.dbo.PublicationInvoicingParameters

UNION
SELECT     'ASOPHolland' AS SystemName, SelectedBy, TitleCard, InsertDate, RunDate, Extracted, BatchNumber
FROM         MiddlewareASOPHolland.dbo.PublicationInvoicingParameters

GO
