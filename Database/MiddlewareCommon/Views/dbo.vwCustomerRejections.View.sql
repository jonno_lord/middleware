USE [MiddlewareCommon]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerRejections'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerRejections'
GO
/****** Object:  View [dbo].[vwCustomerRejections]    Script Date: 10/04/2018 10:33:09 ******/
DROP VIEW [dbo].[vwCustomerRejections]
GO
/****** Object:  View [dbo].[vwCustomerRejections]    Script Date: 10/04/2018 10:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwCustomerRejections]
AS
SELECT     MiddlewareESOP.dbo.CustomerRejections.CustomerId AS ID, 'ESOP' AS SystemName, MiddlewareESOP.dbo.Customers.Company_Name AS CustomerName, 
                      MiddlewareESOP.dbo.CustomerRejections.Sender, MiddlewareESOP.dbo.CustomerRejections.Recipient, MiddlewareESOP.dbo.CustomerRejections.CC, 
                      MiddlewareESOP.dbo.CustomerRejections.DateRejected, MiddlewareESOP.dbo.CustomerRejections.ReasonRejected, 
                      MiddlewareESOP.dbo.CustomerRejections.Body, MiddlewareESOP.dbo.CustomerRejections.NotificationSent
FROM         MiddlewareESOP.dbo.CustomerRejections INNER JOIN
                      MiddlewareESOP.dbo.Customers ON MiddlewareESOP.dbo.CustomerRejections.CustomerId = MiddlewareESOP.dbo.Customers.id
UNION
SELECT     MiddlewareIPSOP.dbo.CustomerRejections.CustomerId AS ID, 'IPSOP' AS SystemName, MiddlewareIPSOP.dbo.Customers.Company_Name AS CustomerName, 
                      MiddlewareIPSOP.dbo.CustomerRejections.Sender, MiddlewareIPSOP.dbo.CustomerRejections.Recipient, MiddlewareIPSOP.dbo.CustomerRejections.CC, 
                      MiddlewareIPSOP.dbo.CustomerRejections.DateRejected, MiddlewareIPSOP.dbo.CustomerRejections.ReasonRejected, 
                      MiddlewareIPSOP.dbo.CustomerRejections.Body, MiddlewareIPSOP.dbo.CustomerRejections.NotificationSent
FROM         MiddlewareIPSOP.dbo.CustomerRejections INNER JOIN
                      MiddlewareIPSOP.dbo.Customers ON MiddlewareIPSOP.dbo.CustomerRejections.CustomerId = MiddlewareIPSOP.dbo.Customers.id
UNION
SELECT     MiddlewareASOPUK.dbo.CustomerRejections.CustomerId AS ID, 'ASOPUK' AS SystemName, MiddlewareASOPUK.dbo.Customers.Company_Name AS CustomerName, 
                      MiddlewareASOPUK.dbo.CustomerRejections.Sender, MiddlewareASOPUK.dbo.CustomerRejections.Recipient, MiddlewareASOPUK.dbo.CustomerRejections.CC, 
                      MiddlewareASOPUK.dbo.CustomerRejections.DateRejected, MiddlewareASOPUK.dbo.CustomerRejections.ReasonRejected, 
                      MiddlewareASOPUK.dbo.CustomerRejections.Body, MiddlewareASOPUK.dbo.CustomerRejections.NotificationSent
FROM         MiddlewareASOPUK.dbo.CustomerRejections INNER JOIN
                      MiddlewareASOPUK.dbo.Customers ON MiddlewareASOPUK.dbo.CustomerRejections.CustomerId = MiddlewareASOPUK.dbo.Customers.id
UNION
SELECT     MiddlewareASOPDaltons.dbo.CustomerRejections.CustomerId AS ID, 'ASOPDaltons' AS SystemName, 
                      MiddlewareASOPDaltons.dbo.Customers.Company_Name AS CustomerName, MiddlewareASOPDaltons.dbo.CustomerRejections.Sender, 
                      MiddlewareASOPDaltons.dbo.CustomerRejections.Recipient, MiddlewareASOPDaltons.dbo.CustomerRejections.CC, 
                      MiddlewareASOPDaltons.dbo.CustomerRejections.DateRejected, MiddlewareASOPDaltons.dbo.CustomerRejections.ReasonRejected, 
                      MiddlewareASOPDaltons.dbo.CustomerRejections.Body, MiddlewareASOPDaltons.dbo.CustomerRejections.NotificationSent
FROM         MiddlewareASOPDaltons.dbo.CustomerRejections INNER JOIN
                      MiddlewareASOPDaltons.dbo.Customers ON MiddlewareASOPDaltons.dbo.CustomerRejections.CustomerId = MiddlewareASOPDaltons.dbo.Customers.id
UNION
SELECT     MiddlewareASOPHolland.dbo.CustomerRejections.CustomerId AS ID, 'ASOPHolland' AS SystemName, 
                      MiddlewareASOPHolland.dbo.Customers.Company_Name AS CustomerName, MiddlewareASOPHolland.dbo.CustomerRejections.Sender, 
                      MiddlewareASOPHolland.dbo.CustomerRejections.Recipient, MiddlewareASOPHolland.dbo.CustomerRejections.CC, 
                      MiddlewareASOPHolland.dbo.CustomerRejections.DateRejected, MiddlewareASOPHolland.dbo.CustomerRejections.ReasonRejected, 
                      MiddlewareASOPHolland.dbo.CustomerRejections.Body, MiddlewareASOPHolland.dbo.CustomerRejections.NotificationSent
FROM         MiddlewareASOPHolland.dbo.CustomerRejections INNER JOIN
                      MiddlewareASOPHolland.dbo.Customers ON MiddlewareASOPHolland.dbo.CustomerRejections.CustomerId = MiddlewareASOPHolland.dbo.Customers.id

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[30] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerRejections (MiddlewareESOP.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 229
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwCustomers"
            Begin Extent = 
               Top = 6
               Left = 244
               Bottom = 182
               Right = 480
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3540
         Alias = 1215
         Table = 4455
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerRejections'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwCustomerRejections'
GO
