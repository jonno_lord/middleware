USE [MiddlewareCommon]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetStageDescription]    Script Date: 10/04/2018 10:35:40 ******/
DROP FUNCTION [dbo].[fn_GetStageDescription]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetStageDescription]    Script Date: 10/04/2018 10:35:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 12th August 2010
-- Description:	Calculate stage description
-- =============================================
CREATE FUNCTION [dbo].[fn_GetStageDescription]
(
	-- Add the parameters for the function here
	@stageId as INT
)
RETURNS VARCHAR(40)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @StageDescription AS VARCHAR(40)
	
	SELECT @StageDescription = CASE WHEN @stageId =1 THEN 'Source to Middleware'
			WHEN @stageId = 2 THEN 'Accepted in Scrutiny'
			WHEN @stageId = 3 THEN 'Middleware to Middleware In'
			WHEN @stageId = 4 THEN 'Middleware to JDE'
			WHEN @stageId = 5 THEN 'JDE to Middleware'
			WHEN @stageId = 6 THEN 'Middleware to Middleware Out'
			WHEN @stageId = 7 THEN 'Middleware to Source'
			ELSE 'Unknown'
			END
	
	RETURN @StageDescription
	
END
GO
