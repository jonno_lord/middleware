USE [MiddlewareCommon]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTelephoneFormat]    Script Date: 10/04/2018 10:35:40 ******/
DROP FUNCTION [dbo].[fn_GetTelephoneFormat]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTelephoneFormat]    Script Date: 10/04/2018 10:35:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_GetTelephoneFormat]
(
	@Prefix as Varchar(10),
	@Std as varchar(10),
	@Number as Varchar(15)
)
RETURNS VARCHAR(40)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @PhoneFormat AS VARCHAR(40)
	
	IF @Prefix = '44' OR @Prefix = '+44'
		SET @PhoneFormat = '+' + LTRIM(RTRIM(@Prefix)) +  ' (' + SUBSTRING(LTRIM(RTRIM(@STD)), 1, 1) + ')' + SUBSTRING(LTRIM(RTRIM(@Std)),2,9) + ' ' + LTRIM(RTRIM(@Number))
	ELSE 
		SET @PhoneFormat = '+' + LTRIM(RTRIM(@Prefix)) +  ' ' + LTRIM(RTRIM(@Std)) + ' ' + LTRIM(RTRIM(@Number))
	
	RETURN @PhoneFormat
	
END

GO
