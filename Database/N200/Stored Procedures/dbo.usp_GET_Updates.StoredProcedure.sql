USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Updates]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_GET_Updates]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Updates]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_Updates] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT ExhibitorId,ExhibitingName,dbo.fn_FormatAddress(ISNULL(Address1,''),ISNULL(Address2,''),ISNULL(Address3,'')) as Address,
	City,County,PostCode,Country,Title,FirstName,LastName,JobTitle,Username,[Password],StandNumber,StandArea,PartnerCode,ContactCode, 
	N200EventCode,N200RegistrationCode as RegistrationType,N200PersonnelTypeCode as PersonnelType,NumberOfBadges,Website,Email,Type,
	BarcodeScanner,MobileApp
	FROM Staging
	WHERE [Action] = 'U'
	AND ContactCode IS NOT NULL
	AND PartnerCode IS NOT NULL
END

GO
