USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_PartnerCode]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_UPD_PartnerCode]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_PartnerCode]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_PartnerCode] 
(@PartnerCode AS VARCHAR(20), @ESOPEventCode AS VARCHAR(10), @ExhibitorId AS VARCHAR(10), @Type AS VARCHAR(12))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Staging
	SET PartnerCode = @PartnerCode, [Action] = NULL , DateUpdated = GETDATE(), Error = NULL
	WHERE ExhibitorId = @ExhibitorId
	AND ESOPEventCode = @ESOPEventCode
	AND [Type] = @Type
END

GO
