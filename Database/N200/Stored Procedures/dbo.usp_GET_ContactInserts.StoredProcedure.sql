USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ContactInserts]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_GET_ContactInserts]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ContactInserts]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ContactInserts] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT ExhibitorId,ExhibitingName,dbo.fn_FormatAddress(ISNULL(Address1,'') ,ISNULL(Address2,''),ISNULL(Address3,'')) as Address,
	City,County,PostCode,Country,Title,FirstName,LastName,JobTitle,Username,Password,Website,Email,[Type]
	FROM Staging
	WHERE [Action] = 'I'
	AND ContactCode IS NULL
END

GO
