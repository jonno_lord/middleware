USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Event]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_INS_Event]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Event]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_Event] 
(@ESOPEventName AS VARCHAR(50), @N200EventCode AS VARCHAR(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.Events (ESOPEventName, N200EventCode)
	VALUES (@ESOPEventName, @N200EventCode)
	
END

GO
