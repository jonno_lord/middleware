USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Events]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_SEL_Events]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Events]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_Events] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ESOPEventName, N200EventCode FROM Events
	ORDER BY ESOPEventName 
END

GO
