USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ResetUpdates]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_UPD_ResetUpdates]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ResetUpdates]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_ResetUpdates]
(@ContactCode AS VARCHAR(20), @PartnerCode AS VARCHAR(20)) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Staging
	SET [Action] = NULL, DateUpdated = GETDATE(), Error = NULL
	WHERE ContactCode = @ContactCode
	AND PartnerCode = @PartnerCode

END
GO
