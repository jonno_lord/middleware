USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_LogError]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_UPD_LogError]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_LogError]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_LogError]
(@ExhibitorId AS INT, @N200EventCode AS VARCHAR(20) = NULL, @Type AS VARCHAR(10), @Error AS VARCHAR(250)) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@N200EventCode IS NULL)
	BEGIN
		UPDATE Staging
		SET Error = @Error, DateUpdated = GETDATE()
		WHERE ExhibitorId = @ExhibitorId
		AND [Type] = @Type
		AND Action IS NOT NULL
	END
		ELSE
	BEGIN
		UPDATE Staging
		SET Error = @Error, DateUpdated = GETDATE()
		WHERE ExhibitorId = @ExhibitorId
		AND N200EventCode = @N200EventCode
		AND [Type] = @Type
		AND Action IS NOT NULL
	END

END

GO
