USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PartnerInserts]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_GET_PartnerInserts]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PartnerInserts]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_PartnerInserts] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT StandNumber,StandArea,N200EventCode,N200RegistrationCode as RegistrationType,
	ContactCode,ESOPEventCode,N200PersonnelTypeCode as PersonnelType, NumberOfBadges, ExhibitorId, [Type],
	BarcodeScanner, MobileApp
    FROM Staging
	WHERE [Action] = 'I'
	AND PartnerCode IS NULL
	AND ContactCode IS NOT NULL
END






GO
