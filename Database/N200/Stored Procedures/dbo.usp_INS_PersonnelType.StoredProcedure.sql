USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_PersonnelType]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_INS_PersonnelType]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_PersonnelType]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_PersonnelType] 
(@N200EventCode AS VARCHAR(20), @RegistrationTypeCode AS VARCHAR(20), @PersonnelTypeCode AS VARCHAR(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.PersonnelTypes (N200EventCode, RegistrationTypeCode, PersonnelTypeCode)
	VALUES (@N200EventCode, @RegistrationTypeCode , @PersonnelTypeCode)
	
END

GO
