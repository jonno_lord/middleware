USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_BadgeAllocation]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_SAV_BadgeAllocation]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_BadgeAllocation]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th July 2011
-- Description:	Inserts or Updates tax company table entry
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_BadgeAllocation] 
	-- Add the parameters for the stored procedure here
	@Id int,
	@EventCode AS VARCHAR(10),
	@StandSizeFrom AS DECIMAL(18,2),
	@StandSizeTo AS DECIMAL(18,2),
	@Main AS INT,
	@Sharer AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		IF EXISTS (SELECT * FROM BadgeAllocations WHERE Id = @Id)
	BEGIN
		UPDATE BadgeAllocations 
		SET EventCode = @EventCode, StandSizeFrom = @StandSizeFrom, StandSizeTo = @StandSizeTo, Main = @Main, Sharer = @Sharer
		WHERE Id = @Id
	END
	ELSE
	BEGIN
		INSERT INTO BadgeAllocations(EventCode,StandSizeFrom,StandSizeTo,Main,Sharer)
		VALUES (@EventCode,@StandSizeFrom,@StandSizeTo,@Main,@Sharer)
	END
END

GO
