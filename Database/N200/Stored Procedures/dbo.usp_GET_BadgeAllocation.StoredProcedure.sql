USE [N200]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BadgeAllocation]    Script Date: 10/04/2018 10:55:32 ******/
DROP PROCEDURE [dbo].[usp_GET_BadgeAllocation]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BadgeAllocation]    Script Date: 10/04/2018 10:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th July 2011
-- Description:	gets tax company by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_BadgeAllocation] 
	-- Add the parameters for the stored procedure here
	@id int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM BadgeAllocations
	WHERE Id = @id
END

GO
