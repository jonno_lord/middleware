USE [N200]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatAddress]    Script Date: 10/04/2018 10:56:19 ******/
DROP FUNCTION [dbo].[fn_FormatAddress]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatAddress]    Script Date: 10/04/2018 10:56:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_FormatAddress]
(
	@Address1 AS VARCHAR(50), @Address2 AS VARCHAR(50), @Address3 AS VARCHAR(50)
)
RETURNS VARCHAR(150)
AS
BEGIN

	
	-- Declare the return variable here
	IF (@Address3 = '' AND @Address2 = '' AND @Address1 = '')
		RETURN ''

	IF (@Address3 = '' AND @Address2 <> '' AND @Address1 <> '')
		RETURN @Address1 + ', ' + @Address2

	IF (@Address3 = '' AND @Address2 = '' AND @Address1 <> '')
		RETURN @Address1

	IF (@Address3 <> '' AND @Address2 <> '' AND @Address1 <> '')
		RETURN @Address1 + ', ' + @Address2 + ', ' + @Address3

	IF (@Address3 <> '' AND @Address2 = '' AND @Address1 <> '')
		RETURN @Address1 + ', ' + @Address3

	RETURN ''

END

GO
