USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyCode]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_AgencyCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyCode]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_AgencyCode](@PaymentSetting VARCHAR(5))
RETURNS VARCHAR(2)
AS
BEGIN 
	DECLARE @Ret VARCHAR(2)
	
	IF @PaymentSetting = 'CSH'
		SET @Ret = 'P'
	ELSE 
		SET @Ret = 'BR'	
	
	return @Ret
END

GO
