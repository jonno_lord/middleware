USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedTelephoneNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_FormatedTelephoneNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedTelephoneNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_FormatedTelephoneNumber](@TelephoneNum as varchar(15), @TelephonePre as varchar(10), @TelephoneStd as varchar(10))
returns varchar(40)
as
begin
	
	
	DECLARE @ret AS VARCHAR(40)
	SET @ret = ''
	
	IF (ISNULL(@TelephoneNum, '') <> '')
	BEGIN 
		SET @ret = 	'(' + RTRIM(ISNULL(@TelephonePre, '')) + ')' + RTRIM(ISNULL(@TelephoneStd, '')) +
					RTRIM(@TelephoneNum)
	END
	

	RETURN @ret
	
END



GO
