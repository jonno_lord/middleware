USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyDiscountFeature]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_AgencyDiscountFeature]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyDiscountFeature]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_AgencyDiscountFeature]
(
	@FeatureType AS VARCHAR(3),
	@ClassCard AS VARCHAR(40)
)
RETURNs VARCHAR(40)
AS
BEGIN	
	
	IF @FeatureType = 'AGY' 
		return 'Agency Discount'


		return @ClassCard
	
END

GO
