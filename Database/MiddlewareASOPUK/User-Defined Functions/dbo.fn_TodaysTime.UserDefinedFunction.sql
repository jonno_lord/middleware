USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TodaysTime]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_TodaysTime]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TodaysTime]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_TodaysTime]()
RETURNS VARCHAR(8)
AS
BEGIN	
	DECLARE @ret AS VARCHAR(6)
	SELECT @ret = REPLACE(CONVERT(VARCHAR, GETDATE(), 108), ':', '')
	
	RETURN @ret
END
GO
