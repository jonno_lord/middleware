USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedSubledgerDate]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_FormatedSubledgerDate]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedSubledgerDate]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_FormatedSubledgerDate](@InsertDate AS DATETIME)
RETURNS VARCHAR(6)
AS
BEGIN
	
	DECLARE @ret AS VARCHAR(6)
	
	--SET @ret = SUBSTRING(CONVERT(VARCHAR(10),@InsertDate, 103),9,2) + SUBSTRING(CONVERT(VARCHAR(10),@InsertDate, 103),4,2) 
	
	SET @ret = CONVERT(CHAR(6), @InsertDate, 112)
				  
	RETURN @ret
	
END
GO
