USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentInstrumentCustomerLoad]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_PaymentInstrumentCustomerLoad]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentInstrumentCustomerLoad]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PaymentInstrumentCustomerLoad](@PaymentSetting as varchar(20))
returns varchar(1)
as
begin

	DECLARE @ret AS VARCHAR(1)
	
	SET @ret = ''
	
	IF(@PaymentSetting= 'CC') SET @ret = 'V'

	IF(@PaymentSetting = 'CSH') SET @ret = 'C'
	
	
	RETURN @ret
	
END


GO
