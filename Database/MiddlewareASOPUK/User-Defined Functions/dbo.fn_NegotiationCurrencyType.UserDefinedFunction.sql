USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_NegotiationCurrencyType]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_NegotiationCurrencyType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_NegotiationCurrencyType]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_NegotiationCurrencyType](@NegotiationCurrency as varchar(3))
returns varchar(1)
as
begin	
	
	IF @NegotiationCurrency <> 'GBP'
		return 'F'


		return 'D'
	
END

GO
