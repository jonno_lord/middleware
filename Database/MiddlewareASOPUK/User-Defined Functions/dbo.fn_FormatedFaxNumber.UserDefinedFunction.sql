USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedFaxNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_FormatedFaxNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedFaxNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE function [dbo].[fn_FormatedFaxNumber](@FaxNum as varchar(15), @FaxPre as varchar(10), @FaxStd as varchar(10))
returns varchar(40)
as
begin
	
	
	DECLARE @ret AS VARCHAR(40)
	SET @ret = ''
	
	IF (ISNULL(@FaxNum, '') <> '')
	BEGIN 
		SET @ret = 	'(' + RTRIM(@FaxPre) + ')' + RTRIM(@FaxStd) +
					RTRIM(@FaxNum)
	END
	

	RETURN @ret
	
END




GO
