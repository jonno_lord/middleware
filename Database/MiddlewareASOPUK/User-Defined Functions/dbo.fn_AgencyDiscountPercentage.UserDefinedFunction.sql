USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyDiscountPercentage]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_AgencyDiscountPercentage]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyDiscountPercentage]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_AgencyDiscountPercentage]
(
	@FeatureType AS VARCHAR(3),
	@AgencyDiscount AS MONEY
)
returns Money
as
begin	
	
	IF @FeatureType = 'AGY' AND @AgencyDiscount > 0
		return @AgencyDiscount*10


		return 0
	
END

GO
