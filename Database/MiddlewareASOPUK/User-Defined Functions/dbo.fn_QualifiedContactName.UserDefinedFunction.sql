USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_QualifiedContactName]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_QualifiedContactName]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_QualifiedContactName]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_QualifiedContactName](@ContactTitle as varchar(3), @Forename as varchar(40), @Surname as varchar(20))
returns varchar(30)
as
begin	
	
	return LTRIM(@ContactTitle) + ' '+ LEFT(@Forename,1) + ' ' + LTRIM(@Surname)
	
END

GO
