USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditControllerLogin]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CreditControllerLogin]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditControllerLogin]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CreditControllerLogin](@DescribesAgency as bit, @TitleCard as Varchar(20))
returns varchar(50)
as
begin
	
	
	DECLARE @ret AS VARCHAR(50)

	

	-- look up the credit team
	IF(@DescribesAgency <> 1)
	BEGIN

		SELECT @ret = Collection_Manager_Client FROM
			JDE_Publication_To_Business_Unit 
			WHERE Publication = @TitleCard
	END
	ELSE
	BEGIN
	
		SELECT @ret = Collection_Manager_Agent FROM
			JDE_Publication_To_Business_Unit 
			WHERE Publication = @TitleCard
	END

	-- if we could not find this default - put it to X to mark the failure
	-- for JDE to resolve by hand
	IF (@ret IS NULL) SET @ret = 'X'

	RETURN @ret
	
END

GO
