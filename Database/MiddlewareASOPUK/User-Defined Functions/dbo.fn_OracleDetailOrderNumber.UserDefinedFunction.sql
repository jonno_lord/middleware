USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleDetailOrderNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_OracleDetailOrderNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleDetailOrderNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_OracleDetailOrderNumber]
(
@OrderURN AS INT,
@BatchNo AS INT,
@TitleCardShort AS VARCHAR(10)
)
RETURNS VARCHAR(12)
AS
BEGIN

	DECLARE @LineNumber AS INT
	
	SELECT @LineNumber = MAX(Line_Number) FROM Orders o
	WHERE o.Stage = 1
	AND o.urn_number = @OrderURN
	AND o.batch_number = @BatchNo
	AND o.Title_Card_short = @TitleCardShort

	RETURN CONVERT(VARCHAR(10),@OrderURN) + '_' + CONVERT(VARCHAR(10),@LineNumber)

END




GO
