USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnitHeader]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_BusinessUnitHeader]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnitHeader]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_BusinessUnitHeader](@URNNUmber AS VARCHAR(10), @TitleCardShort AS VARCHAR(10))
returns varchar(12)
as
begin	
	
	DECLARE @ret AS VARCHAR(12)
	DECLARE @count AS INT
	
	SET @ret = ''
	SET @count = 0
	
	SELECT @count = COUNT(DISTINCT title_card) FROM dbo.Orders o
		WHERE urn_number = @URNNumber
		AND o.Stage = 1
		
	IF @Count > 1
	SET @ret = 'ASOP'
	ELSE
	SELECT @ret = LTRIM(RTRIM(Business_Unit)) FROM JDE_Publication_to_Business_Unit
			WHERE Publication = @TitleCardShort

	RETURN @ret
	
END
	
GO
