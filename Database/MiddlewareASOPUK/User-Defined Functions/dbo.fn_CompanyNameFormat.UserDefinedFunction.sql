USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyNameFormat]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CompanyNameFormat]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyNameFormat]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_CompanyNameFormat](@IsCompany as bit, @ComapnyName as Varchar(50))
returns varchar(50)
as
begin
	
	
	DECLARE @ret AS VARCHAR(50)

	

	-- look up the credit team
	IF(@IsCompany <> 1)
	BEGIN

		SELECT @ret = LTRIM(RTRIM(SubString(RTRIM(@ComapnyName), charindex(' ', RTRIM(@ComapnyName))+1, len(RTRIM(@ComapnyName)) - charindex(' ', RTRIM(@ComapnyName)))
					  + ' ' + LEFT(RTRIM(@ComapnyName), charindex(' ', RTRIM(@ComapnyName)))))
	END
	ELSE
		SET @ret = @ComapnyName

	-- if we could not find this default - put it to X to mark the failure
	-- for JDE to resolve by hand
	IF (@ret IS NULL) SET @ret = 'X'

	RETURN @ret
	
END


GO
