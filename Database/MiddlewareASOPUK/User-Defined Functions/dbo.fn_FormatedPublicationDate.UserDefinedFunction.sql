USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedPublicationDate]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_FormatedPublicationDate]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FormatedPublicationDate]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_FormatedPublicationDate]
(
	@TitleCardShort AS VARCHAR(5),
	@InsertDate AS DATETIME,
	@ColourCard AS VARCHAR(15)
)
RETURNS VARCHAR(46)
AS
BEGIN	
	DECLARE @ret AS VARCHAR(46)
	SET @ret = 'X'
	
	-- (2011) Year is not relevant as the SQL is only interested in months...
	IF EXISTS (SELECT ID FROM JDE_Publication_to_Business_Unit
				WHERE (Monthly_Weekly = 'M') AND (Publication = RTRIM(@TitleCardShort)))				
		BEGIN 
			SELECT @ret = '01' + SUBSTRING(CONVERT(VARCHAR(8) , @InsertDate, 3),3, LEN(CONVERT(VARCHAR(8) , @InsertDate, 3))) + @ColourCard 
		END
	ELSE
		BEGIN
			SELECT @ret = CONVERT(VARCHAR(8) , @InsertDate, 3) + @ColourCard 
		END

	RETURN @ret
	
END







GO
