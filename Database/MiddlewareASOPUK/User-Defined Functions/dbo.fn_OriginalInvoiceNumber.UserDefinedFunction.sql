USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_OriginalInvoiceNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OriginalInvoiceNumber](@UrnNumber AS INT, @GrossPrice AS MONEY, @IssueDate AS DATETIME, @PublicationCode as VARCHAR(50))
RETURNS FLOAT
AS
BEGIN

	DECLARE @ret AS FLOAT
	
	SELECT @ret = Document_Number FROM ASOP_Credit_Card_Orders
	WHERE MW_to_MW_Out IS NOT NULL
	AND Order_Urn = @UrnNumber
	AND ABS(Gross_price) = ABS(@GrossPrice)
	AND Issue_date = @IssueDate
	AND Publication_Code = @PublicationCode
	
	RETURN @ret
	
END



GO
