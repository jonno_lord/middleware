USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ReportingCurrency]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_ReportingCurrency]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ReportingCurrency]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ReportingCurrency]()
returns varchar(3)
as
begin
	return 'GBP'
end
GO
