USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode14]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CatCode14]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode14]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE function [dbo].[fn_CatCode14](@CutomerType as varchar(20))
returns varchar(3)
as
begin

	DECLARE @ret AS VARCHAR(3)
	
	SELECT @ret = Jde_Code 
			   FROM dbo.JDE_Catcode_Map jcm
					INNER JOIN ASOP_Customer_Type act ON jcm.Short_name = act.short_name
			   WHERE act.Long_name = @CutomerType 		
	
	
	return @ret
end



GO
