USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Address2]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_Address2]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Address2]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_Address2](@Company as bit, @AddressLine1 as varchar(50), @AddressLine2 as varchar(50))
returns varchar(50)
as
begin

	DECLARE @ret as varchar(50)

	IF(@Company = 1)
		SET @ret = @AddressLine1
	Else
		SET @ret = @AddressLine2
	
	return @ret
end

GO
