USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_VATRateCodeCustomerLoad]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_VATRateCodeCustomerLoad]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_VATRateCodeCustomerLoad]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_VATRateCodeCustomerLoad]
(
	@DescribesHouseCustomer as bit, @VatRegNumber as varchar(20), @TaxExemptNumber as varchar(20), @CountryCode as varchar(3)
)
RETURNS VARCHAR(10)
AS
BEGIN

       DECLARE @Code AS VARCHAR(10)
       DECLARE @InEC AS BIT 
       
       SET @Code = 'GBSTD'               -- Default vat code
       SET @InEC = 0

       IF(@DescribesHouseCustomer = 1 )
       BEGIN
              SET @Code = 'GBOUTSIDE'
              RETURN @Code
       END

       -- check to see if country is in EC
       IF EXISTS(SELECT * FROM MiddlewareJDE.dbo.vw_ECVatGroups jevg WHERE jevg.CountryCode = @CountryCode)
       BEGIN
              SET @InEC = 1
       END

       IF(ISNULL(@VatRegNumber, '') <> '' AND @InEC = 1 AND @CountryCode <> 'GB')
       BEGIN
              SET @Code = 'GBREVCHRGE'
              RETURN @Code
       END

       IF(@CountryCode <> 'GB' AND @InEC = 0)
       BEGIN
              SET @Code = 'GBOUTSIDE'
              RETURN @Code
       END

       IF (ISNULL(@TaxExemptNumber,'') <> '' AND @CountryCode = 'GB')
       BEGIN
              SET @Code = 'GBZERO'
              RETURN @Code
       END

       RETURN @Code

	
END



GO
