USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyFromBusinessUnit]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CompanyFromBusinessUnit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyFromBusinessUnit]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CompanyFromBusinessUnit](@TitleCard as varchar(10))
returns varchar(10)
as
begin	
	
	DECLARE @ret AS VARCHAR(10)
	
	SELECT @ret = Company FROM 
	MiddlewareJDE.dbo.vw_JdeBusinessUnits JBU 
	INNER JOIN JDE_Publication_To_Business_Unit JPTBU ON
	(JPTBU.Business_Unit = JBU.BusinessUnit)
	WHERE JPTBU.Publication = @TitleCard
	
	RETURN @ret
	
END

GO
