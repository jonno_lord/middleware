USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DelinquencyNotice]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_DelinquencyNotice]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DelinquencyNotice]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DelinquencyNotice](@PaymentSetting as varchar(3))
returns varchar(2)
as
begin

	DECLARE @ret as varchar(2)
	
	SELECT @ret = Delinquency_Notice FROM
	ASOP_Defaults_Based_On_Payment_setting
	WHERE Payment_Setting = @PaymentSetting
	
	RETURN @ret
END

GO
