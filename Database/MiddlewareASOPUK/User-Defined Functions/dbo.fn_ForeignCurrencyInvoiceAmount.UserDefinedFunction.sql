USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ForeignCurrencyInvoiceAmount]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_ForeignCurrencyInvoiceAmount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ForeignCurrencyInvoiceAmount]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_ForeignCurrencyInvoiceAmount]
(
	@NegotiationCurrency AS VARCHAR(3),
	@OrderInsertBreakdownCY AS Money
)
RETURNS FLOAT
AS
BEGIN	
	
	DECLARE @ret AS DECIMAL(10,2)
	
	IF @NegotiationCurrency <> 'GBP'
	
	--SELECT @ret = CONVERT(FLOAT,(@OrderInsertBreakdownCY)) * CONVERT(FLOAT, (POWER(10, Decimals)) * 100) FROM 	MiddlewareJDE.dbo.[vw_Currencies]
	--	WHERE CurrencyCode = @NegotiationCurrency
	
	SELECT @ret = CONVERT(DECIMAL(10,2),(@OrderInsertBreakdownCY)) 	
	RETURN @Ret
	
END



	

GO
