USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LegalEntity]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_LegalEntity]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LegalEntity]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_LegalEntity](@House as bit)
returns varchar(3)
as
begin
	
	
	DECLARE @ret AS VARCHAR(3)

	

	-- look up the credit team
	IF(@House = 1)
		SET @ret = '999'

	-- if we could not find this default - put it to X to mark the failure
	-- for JDE to resolve by hand
	IF (@ret IS NULL) SET @ret = ''

	RETURN @ret
	
END



GO
