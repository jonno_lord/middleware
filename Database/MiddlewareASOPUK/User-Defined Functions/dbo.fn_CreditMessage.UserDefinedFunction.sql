USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditMessage]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CreditMessage]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditMessage]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_CreditMessage]
(@PaymentSetting AS VARCHAR(5))
returns varchar(1)
as
begin
	
	DECLARE @Setting AS VARCHAR(1)
	SET @Setting = ''
	
	IF(@PaymentSetting = 'CSH' OR @PaymentSetting = 'CC')
		SET @Setting = 'D'
		
	RETURN @Setting
	
END



GO
