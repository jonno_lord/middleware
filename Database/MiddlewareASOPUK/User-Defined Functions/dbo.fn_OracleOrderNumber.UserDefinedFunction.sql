USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleOrderNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_OracleOrderNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleOrderNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_OracleOrderNumber]
(
@OrderURN AS INT,
@LineNumber AS INT
)
RETURNS VARCHAR(12)
AS
BEGIN

	RETURN CONVERT(VARCHAR(10),@OrderURN) + '_' + CONVERT(VARCHAR(10),@LineNumber)

END




GO
