USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExplanationCodeCustomer]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_TaxExplanationCodeCustomer]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExplanationCodeCustomer]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_TaxExplanationCodeCustomer](@TaxExemptNumber as varchar(30))
returns varchar(1)
as
begin

	if( ISNULL(@TaxExemptNumber,'') <> '' AND LEN(@TaxExemptNumber)  > 0)
		return 'E'
	
	return 'V'
end



GO
