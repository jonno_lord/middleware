USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PreferredBillingCurrency]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_PreferredBillingCurrency]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PreferredBillingCurrency]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_PreferredBillingCurrency](@CountryCode as varchar(3))
returns varchar(3)
as
begin
	DECLARE @ret as varchar(3)
	SET @ret = 'GBP'
	
	IF EXISTS(SELECT Currency_Code
				FROM JDE_Preferred_Billing_Currency
				WHERE Country_Code = @CountryCode)
		SELECT @ret = Currency_Code
		FROM JDE_Preferred_Billing_Currency
		WHERE Country_Code = @CountryCode
	
	RETURN @ret
	
end

GO
