USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnit]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_BusinessUnit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnit]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_BusinessUnit]
(
	@TitleCardShort AS VARCHAR(5)
)
returns VARCHAR(12)
as
begin
	
	declare @ret AS VARCHAR(12)
	
	SELECT @ret = LTRIM(RTRIM(Business_Unit)) 
	  FROM JDE_Publication_to_Business_Unit
		WHERE Publication = @TitleCardShort
				  
	RETURN @ret
	
END

GO
