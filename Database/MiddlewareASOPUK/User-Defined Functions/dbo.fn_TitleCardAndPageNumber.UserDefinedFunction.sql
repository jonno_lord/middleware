USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TitleCardAndPageNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_TitleCardAndPageNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TitleCardAndPageNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_TitleCardAndPageNumber]
(
	@TitleCard AS VARCHAR(30),
	@PageNumber AS VARCHAR(3)
)
RETURNS VARCHAR(30)
AS
BEGIN
	DECLARE @res AS VARCHAR(30)
	DECLARE @TitleLength INT
	DECLARE @MaxLength INT
	DECLARE @BlankSpaces INT
	
	SET @MaxLength = 26
		
	SELECT @TitleLength = LEN(@TitleCard) 
	
	SELECT @BlankSpaces = @MaxLength - @TitleLength 
	
	--Check with ED about what title card it is. 
	SELECT @res = @TitleCard + SPACE(@BlankSpaces) + '~' + LTRIM(RTRIM(ISNULL(@PageNumber,'')))
	
	RETURN @res
	
END
GO
