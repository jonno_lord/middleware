USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerCategory]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CustomerCategory]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerCategory]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CustomerCategory](@Agency as bit, @InHouse as bit)
returns varchar(2)
as
begin
	
	DECLARE @ret AS VARCHAR(2)
	SET @ret = 'XD'
	
	IF(@Agency = 1)
		SET @ret = 'XO'
		
	IF(@InHouse = 1)
		SET @ret = 'IC'

	RETURN @ret

END

GO
