USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsCustomersLoad]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_PaymentTermsCustomersLoad]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsCustomersLoad]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PaymentTermsCustomersLoad](@DescribesAgency as bit, @PaymentSetting as varchar(20), @TitleCard as varchar(4), @SummaryCard as varchar(30))
returns varchar(3)
as
begin

	DECLARE @ret AS VARCHAR(3)
	
	SET @ret = 'N30'
	
	IF(@DescribesAgency = 1 AND @PaymentSetting = 'OTH') SET @ret = 'N'

	IF(@DescribesAgency = 1 AND @PaymentSetting = 'INV') SET @ret = 'N'

	IF(@PaymentSetting = 'CSH' OR @PaymentSetting = 'CC') SET @ret = 'R'
	
	IF(@DescribesAgency <> 1 AND @PaymentSetting = 'INV' AND (@TitleCard = 'FG' OR @TitleCard = 'CW') AND (@SummaryCard = 'Classified' OR  @SummaryCard = 'Recruitment' ) ) SET @ret = 'N14'
	

	
	
	RETURN @ret
	
END

GO
