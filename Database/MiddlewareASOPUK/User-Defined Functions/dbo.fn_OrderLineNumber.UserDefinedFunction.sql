USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OrderLineNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_OrderLineNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OrderLineNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OrderLineNumber]
(
@LineNumber AS FLOAT(10)
)
RETURNS FLOAT(10)
AS
BEGIN

	RETURN @LineNumber * 1000

END

GO
