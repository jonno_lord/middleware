USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProcessedFlag]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_ProcessedFlag]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProcessedFlag]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_ProcessedFlag]()
RETURNS VARCHAR(1)
AS
BEGIN
	RETURN 'N'

END
GO
