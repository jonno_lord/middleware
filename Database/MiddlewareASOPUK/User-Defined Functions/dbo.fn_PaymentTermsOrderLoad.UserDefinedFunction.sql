USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsOrderLoad]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_PaymentTermsOrderLoad]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsOrderLoad]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_PaymentTermsOrderLoad]
(
	@AccountNumber AS VARCHAR(12)	
)
returns VARCHAR(3)
as
begin	
	
	DECLARE @ret AS VARCHAR(20)
	
	SELECT @ret = Q1TRAR FROM Customers
	 WHERE Jde_Account_number = @AccountNumber
	 
	 RETURN @ret
END

GO
