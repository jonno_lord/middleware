USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_VATRateCodeOrder]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_VATRateCodeOrder]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_VATRateCodeOrder]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_VATRateCodeOrder]
(
	@AccountNo AS VARCHAR(10),
	@TitleCard AS VARCHAR(10),
	@OrderInsertBreakdown AS MONEY,
	@OrderInsertBreakdownCY AS MONEY,
	@CatCode28 AS VARCHAR(50),
	@Q1TXA1 AS VARCHAR(10)
)
RETURNS VARCHAR(10)

AS
BEGIN
	DECLARE @catcode30 as VARCHAR(3)
	DECLARE @res AS VARCHAR(10)
	SET @res = 'GBSTD'

	IF (@catcode28 <> '' AND ((CONVERT(MONEY, @OrderInsertBreakdown +'0') >= 0 OR
					  CONVERT(MONEY, @OrderInsertBreakdownCY +'0') >= 0)))
	BEGIN					  
		
		SELECT @catcode30 = comp.catcode30 from MiddlewareJDE.dbo.vw_Companies as Comp
			INNER JOIN Customers cus ON CONVERT(INT, (cus.Cat_Code_28 + cus.Cat_Code_29)) = CONVERT(INT, Comp.CompanyCode)
			WHERE jde_account_number = @AccountNo


		IF EXISTS(SELECT * FROM MiddlewareJDE.dbo.vw_JdeBusinessUnits bus
				INNER JOIN Jde_Publication_To_Business_Unit pub ON (bus.BusinessUnit = Pub.Business_Unit)
				INNER JOIN MiddlewareJDE.dbo.vw_Companies Comp ON (comp.CompanyCode = Bus.Company)
				WHERE pub.Publication = @TitleCard AND CatCode30 = @catcode30)		  
			SET @res = 'GBEXEMPT'
			
	END		
	ELSE 
	BEGIN	
	
		SELECT @res = @Q1TXA1 
		
	END
	
	IF (LTRIM(RTRIM(@TitleCard)) = 'TBC') SET @res = 'GBZERO'
						
	RETURN @res
END
GO
