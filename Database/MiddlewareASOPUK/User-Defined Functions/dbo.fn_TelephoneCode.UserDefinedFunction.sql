USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TelephoneCode]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_TelephoneCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TelephoneCode]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION[dbo].[fn_TelephoneCode]()
RETURNS VARCHAR(10)
AS
BEGIN
	
	RETURN 'TEL'
	
END

GO
