USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PersonOpeningAccount]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_PersonOpeningAccount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PersonOpeningAccount]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PersonOpeningAccount]()
returns varchar(5)
as
begin
	return 'ASOP'
end
GO
