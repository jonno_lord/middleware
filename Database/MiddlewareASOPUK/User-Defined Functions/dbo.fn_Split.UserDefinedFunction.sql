USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Split]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_Split]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Split]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_Split]
(
	@DelimitedString as nvarchar(MAX), @Delimiter as varchar(2), @SubscriptPosition as int
)
RETURNS nvarchar(MAX)
AS
BEGIN
	
	DECLARE @ReturnValue as nvarchar(MAX)
	SET @ReturnValue = ''

	DECLARE @NextString NVARCHAR(MAX)
	DECLARE @Pos INT
	DECLARE @NextPos INT
	DECLARE @Counter INT 
	
	SET @Counter = 0
	SET @DelimitedString = @DelimitedString + @Delimiter
	SET @Pos = charindex(@Delimiter,@DelimitedString)

	WHILE (@pos <> 0)
	BEGIN
		SET @Counter = @Counter + 1
		SET @NextString = substring(@DelimitedString,1,@Pos - 1)
		
		IF (@Counter = @SubscriptPosition ) 
		BEGIN
			SET @ReturnValue = @NextString
			BREAK
		END
		
		SET @DelimitedString = substring(@DelimitedString,@pos+1,len(@DelimitedString))
		SET @pos = charindex(@Delimiter,@DelimitedString)
	END 


	RETURN @ReturnValue

END
GO
