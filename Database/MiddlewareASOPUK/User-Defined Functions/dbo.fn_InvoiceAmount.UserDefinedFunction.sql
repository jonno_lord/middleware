USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmount]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_InvoiceAmount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmount]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_InvoiceAmount]
(
	@NegotiationCurrency AS VARCHAR(3),
	@OrderInsertBreakdown AS MONEY,
	@FeatureType AS VARCHAR(3)
)
returns DECIMAL(10,2)
as
begin	
	
	DECLARE @ret AS DECIMAL(10,2)
	
	SELECT @ret = CASE WHEN @NegotiationCurrency = 'GBP' THEN
					CONVERT(DECIMAL(10,2), (@OrderInsertBreakdown)) /* * CONVERT(FLOAT, (POWER(10, Decimals))* 100 )*/
						* CASE WHEN @FeatureType = 'AGY' OR @OrderInsertBreakdown < 0 THEN -1 ELSE 1 END 
				  ELSE 0 END
			
	RETURN @ret
	
END



GO
