USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ForeignCurrencyFUPAmount]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_ForeignCurrencyFUPAmount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ForeignCurrencyFUPAmount]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_ForeignCurrencyFUPAmount]
(
	@NegotiationCurrency AS VARCHAR(3),	
	@OrderInsertBreakdownCY AS MONEY,
	@FeatureType AS VARCHAR(3)
)
RETURNS FLOAT
AS
BEGIN	
	
	DECLARE @ret AS DECIMAL(10,2)
	
	IF @NegotiationCurrency <> 'GBP' 
		SELECT @ret = CONVERT(DECIMAL(10,2), (@OrderInsertBreakdownCY)) /* * CONVERT(FLOAT,(POWER(10, Decimals)) * 100) */ * 
			CASE WHEN RTRIM(@FeatureType) = 'AGY' OR @OrderInsertBreakdownCY < 0 THEN
				-1 
			ELSE 
				1 
			END 
		
		RETURN @ret
END

GO
