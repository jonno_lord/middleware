USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TeamSetting]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_TeamSetting]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TeamSetting]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_TeamSetting](@Agency as bit, @TitleCard as Varchar(4))
returns varchar(1)
as
begin
	
	
	DECLARE @ret AS VARCHAR(1)

	

	-- look up the credit team
	IF(@Agency = 1)
	BEGIN

		SELECT @ret = CreditControllerTeam from JDE_Publication_to_Business_unit pbu
			INNER JOIN MiddlewareJDE.dbo.vw_CreditControllers vcc
			ON (pbu.Collection_Manager_Agent = vcc.LogonName)
			WHERE LTRIM(RTRIM(pbu.Publication)) = LTRIM(RTRIM(@TitleCard))
	
	END
	ELSE
	BEGIN
	
		SELECT @ret = CreditControllerTeam from JDE_Publication_to_Business_unit pbu
			INNER JOIN MiddlewareJDE.dbo.vw_CreditControllers vcc
			ON (pbu.Collection_Manager_Client = vcc.LogonName)
			WHERE LTRIM(RTRIM(pbu.Publication)) = LTRIM(RTRIM(@TitleCard))
	END

	-- if we could not find this default - put it to X to mark the failure
	-- for JDE to resolve by hand
	IF (@ret IS NULL) SET @ret = 'X'

	RETURN @ret
	
END



GO
