USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_LongAddressNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_LongAddressNumber](@URN as varchar(20))
returns varchar(20)
as
begin
	
	declare @ret as VARCHAR(20)
	
	SELECT @ret = LTRIM(RTRIM(@URN)) + 'A'
	
	RETURN @ret
	
END

GO
