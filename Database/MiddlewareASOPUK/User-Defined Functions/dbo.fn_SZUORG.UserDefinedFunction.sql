USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SZUORG]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_SZUORG]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SZUORG]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_SZUORG](

	@InvoiceAmountDomestic AS MONEY,
	@InvoiceAmountForeign AS MONEY
)
RETURNS VARCHAR(2)
AS 
BEGIN
	
	DECLARE @ret AS INT
	
	IF(@InvoiceAmountDomestic < 0 OR @InvoiceAmountForeign < 0)
		SET @ret = '-1'
	ELSE
		SET @ret = '1'
		
	RETURN @ret
	
END


GO
