USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultFaxCode]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_DefaultFaxCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultFaxCode]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_DefaultFaxCode](@FaxNumber as VARCHAR(15))
RETURNS VARCHAR(3)
AS
BEGIN
	
	DECLARE @RET AS VARCHAR(3)
	SET @RET = ''
	
	IF (ISNULL(@FaxNumber,'') <> '') SET @RET = 'FAX'
	RETURN @RET
	
END

GO
