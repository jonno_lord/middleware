USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Address4]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_Address4]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Address4]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_Address4](@Company as bit, @AddressLine3 as varchar(50), @AddressLine4 as varchar(50))
returns varchar(50)
as
begin

	DECLARE @ret as varchar(50)

	IF(@Company = 1)
		SET @ret = @AddressLine3
	Else
		SET @ret = @AddressLine4
	
	return @ret
end

GO
