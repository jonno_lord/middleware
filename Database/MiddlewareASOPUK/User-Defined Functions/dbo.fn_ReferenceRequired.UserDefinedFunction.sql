USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ReferenceRequired]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_ReferenceRequired]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ReferenceRequired]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ReferenceRequired](@ReferenceReqSetting as varchar(2), @DescribesHouse as bit)
returns varchar(1)
as
begin
	
	IF (@DescribesHouse = 1)
		RETURN 'Y'
		
	IF(@ReferenceReqSetting = 'NO')
		RETURN 'N'
	
	RETURN 'Y'
end
GO
