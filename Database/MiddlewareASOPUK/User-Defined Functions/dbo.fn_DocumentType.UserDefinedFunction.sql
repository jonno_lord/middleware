USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DocumentType]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_DocumentType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DocumentType]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_DocumentType](@AccountNo as VARCHAR(20), @OrderInsertBreakdown AS MONEY, @OrderInsertBreakdownCY as MONEY,
										@PaymentType AS VARCHAR(4), @PaymentSetting AS VARCHAR(3))
returns varchar(2)
as
begin

	DECLARE @ret AS VARCHAR(2)
	DECLARE @CatCode28 AS VARCHAR(50)	
	
	SELECT @CatCode28 = cat_code_28 FROM Customers 
	WHERE JDE_Account_Number = @AccountNo
	
	IF (ISNULL(@CatCode28, '') = 'I' AND((@OrderInsertBreakdown >= 0 OR @OrderInsertBreakdownCY >= 0)))
		RETURN 'G5'
	ELSE IF (ISNULL(@Catcode28, '') = 'I' AND((@OrderInsertBreakdown < 0 OR @OrderInsertBreakdownCY < 0)))
		RETURN 'K5'
	
	ELSE 
	
	SELECT @ret = CASE WHEN ((@OrderInsertBreakdown < 0) OR (@OrderInsertBreakdownCY < 0)) 
		THEN 
			Credit_Document_Type
		ELSE
			Document_Type
		END 
	FROM dbo.Asop_Payment_Types WHERE 
	Payment_Type = @paymentType AND 
	Payment_Setting = @PaymentSetting 
	
	RETURN @ret
	
END

GO
