USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OrderDocumentType]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_OrderDocumentType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OrderDocumentType]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_OrderDocumentType]
(
	-- Add the parameters for the function here
	@AccountNumber AS VARCHAR(30),
	@OrderInsertBreakdown AS MONEY,
	@OrderInsertBreakdownCy AS MONEY,
	@PaymentType AS VARCHAR(4),
	@PaymentSetting AS VARCHAR(3)
)
RETURNS VARCHAR(2)
AS
BEGIN

	DECLARE @DocumentType AS VARCHAR(2)
	SET @DocumentType = 'G1'
	
	IF EXISTS(SELECT Cat_Code_28 
				FROM Customers 
				WHERE Jde_Account_Number = @AccountNumber
				AND LTRIM(Cat_Code_28) <> '' 
				AND ((CONVERT(MONEY, @OrderInsertBreakdown +'0') >= 0 OR
					  CONVERT(MONEY, @OrderInsertBreakdownCy +'0') >= 0)))
		SET @DocumentType = 'G5'
	ElSE IF EXISTS(SELECT Cat_Code_28 
				FROM Customers 
				WHERE Jde_Account_Number = @AccountNumber
				AND LTRIM(Cat_Code_28) <> '' 
				AND ((CONVERT(MONEY, @OrderInsertBreakdown +'0') <= 0 OR
					  CONVERT(MONEY, @OrderInsertBreakdownCy +'0') <= 0)))
		SET @DocumentType = 'K5'

	SELECT @DocumentType = CASE WHEN (CONVERT(MONEY, @OrderInsertBreakdown +'0') >= 0 OR 
									  CONVERT(MONEY, @OrderInsertBreakdownCy +'0') >= 0) 
			THEN
				Document_Type 
			ELSE
				Credit_Document_Type
			END
		FROM Asop_Payment_Types 
		WHERE Payment_Type = @PaymentType 
		AND	Payment_Setting = @PaymentSetting
	
	RETURN @DocumentType
END
GO
