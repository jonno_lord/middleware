USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FeatureTypeProductionCharge]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_FeatureTypeProductionCharge]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FeatureTypeProductionCharge]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_FeatureTypeProductionCharge](@FeatureType AS VARCHAR(10), @SummaryCard AS VARCHAR(40), @AccountNo AS VARCHAR(20), @PaymentType AS VARCHAR(10))
RETURNS VARCHAR(40)
AS
BEGIN

	DECLARE @ret AS VARCHAR(40)
	DECLARE @CatCode28 AS VARCHAR(50)
	
	SELECT @CatCode28 = Cat_Code_28 FROM Customers
	WHERE JDE_Account_Number = @AccountNo            
	
	IF (@FeatureType = 'PRC')
		SET @ret = 'PRODUCTION CHARGE'
	ELSE IF (@CatCode28 = 'I')
		SET @ret = UPPER(@SummaryCard) + 'IC'
	ELSE IF (@PaymentType = 'CD')
		SET @ret = UPPER(@SummaryCard) + 'CN'
	ELSE
		SET @ret = UPPER(@SummaryCard)
	
	RETURN @ret
END

GO
