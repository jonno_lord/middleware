USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RightJustify]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_RightJustify]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_RightJustify]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_RightJustify](@content AS varchar(200), @length as int)
returns varchar(200)
as
begin

	declare @ret as varchar(200)
	set @ret = ltrim(rtrim(substring(@content, 1, @length)))

	declare @padding as int
	set @padding = @length - LEN(@ret) 
	
	if(@padding > 0)
		set @ret = REPLICATE(' ', @padding) + @ret

	return @ret
	
end
GO
