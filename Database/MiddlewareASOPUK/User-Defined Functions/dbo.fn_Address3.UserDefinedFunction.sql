USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Address3]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_Address3]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Address3]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_Address3](@Company as bit, @AddressLine2 as varchar(50), @AddressLine3 as varchar(50))
returns varchar(50)
as
begin

	DECLARE @ret as varchar(50)

	IF(@Company = 1)
		SET @ret = @AddressLine2
	Else
		SET @ret = @AddressLine3
	
	return @ret
end

GO
