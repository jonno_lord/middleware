USE [MiddlewareASOPUK]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 09/04/2018 16:13:33 ******/
DROP FUNCTION [dbo].[fn_CreditLimit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 09/04/2018 16:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CreditLimit](@describesAgency as bit, @TitleCard as Varchar(50))
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	
	IF (@describesAgency <> 1 AND @TitleCard = 'FG')
		SET @ret = '250'
	ELSE
		SET @ret = '10000'
				  
	RETURN @ret
	
END


GO
