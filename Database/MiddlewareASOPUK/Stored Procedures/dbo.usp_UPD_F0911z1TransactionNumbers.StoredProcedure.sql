USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F0911z1TransactionNumbers]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_F0911z1TransactionNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F0911z1TransactionNumbers]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UPD_F0911z1TransactionNumbers] 
	-- Add the parameters for the stored procedure here
AS

BEGIN
	--** Used for creating the transaction and line numbering details in the cursor
	DECLARE @VNEDTN AS INTEGER
	DECLARE @Transaction_Id AS INTEGER
	DECLARE @VNODOC AS FLOAT

	DECLARE @ID AS INTEGER

	--** Stores the previous batch details
	DECLARE @Last_VNODOC AS FLOAT

	--** Set up the last details so that
	--** they will break on the first occurence of entering the
	SET @Last_VNODOC = 0
	
	--** Check to make sure the asop line number has been defined correctly
	IF NOT EXISTS(select Guid_Id from MiddlewareCommon.dbo.ASOPGuids WHERE System_Description = 'MW_ASOP_LINE_NO')
		RAISERROR ('Unable to find the next asop transaction line number', 16, 1)

	--** begin the transaction accross the GUIDs table... 
	SELECT @Transaction_Id = Guid_Id FROM MiddlewareCommon.dbo.ASOPGuids WHERE System_Description = 'MW_ASOP_LINE_NO'

	--** Create a cursor based from the temporary table loaded  (grouped
	--** by the key fields of batch number, bucket account and order currency)
	DECLARE csrF0911z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VNODOC FROM F0911z1 
		WHERE GeneralLedgerId IS NULL
		ORDER BY VNODOC
		
	--** Loop through this created cursor and update the transaction numbers
	OPEN csrF0911z1
	FETCH NEXT FROM csrF0911z1 INTO @VNODOC
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Increment the transaction number if the batch no, account or currency changed
		IF 	@VNODOC <> @Last_VNODOC
		
			BEGIN 
				SET @Transaction_Id = @Transaction_Id + 1
			END
		
				--** Update the table with the next value
		UPDATE F0911z1 SET VNEDTN = @Transaction_Id WHERE VNODOC = @VNODOC
		UPDATE F0911z1 SET VNDOC = @Transaction_Id WHERE VNODOC = @VNODOC

		--** store the previous batch details...
		SET @Last_VNODOC = @VNODOC
		
		--** Get the next entry
	FETCH NEXT FROM csrF0911z1 INTO @VNODOC

	END

	CLOSE csrF0911z1
	DEALLOCATE csrF0911z1


	UPDATE MiddlewareCommon.dbo.ASOPGuids
	SET Guid_ID = @Transaction_Id
	WHERE System_Description = 'MW_ASOP_LINE_NO'

END
GO
