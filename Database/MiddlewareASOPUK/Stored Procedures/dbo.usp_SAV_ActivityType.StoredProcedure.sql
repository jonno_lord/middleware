USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ActivityType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SAV_ActivityType]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ActivityType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Brenda Patterson>
-- Create date: <Create Date,,02/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_ActivityType]
	-- Add the parameters for the stored procedure here
	@Id AS Integer,
	@ActivityID AS Varchar(50),
	@ReasonID AS Integer
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	IF EXISTS (SELECT * FROM ASOP_Activity_Types WHERE id = @Id)
	BEGIN
		UPDATE ASOP_Activity_Types
		SET  ActivityID = @ActivityID, ReasonID = @ReasonID
		WHERE Id = @Id
	END
	ELSE
	BEGIN
		INSERT INTO ASOP_Activity_Types(ActivityID, ReasonID)
		VALUES (@ActivityID, @ReasonID)
	END	
			
	
END
GO
