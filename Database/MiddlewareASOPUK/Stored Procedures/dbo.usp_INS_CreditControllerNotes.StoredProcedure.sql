USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditControllerNotes]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_CreditControllerNotes]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditControllerNotes]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_CreditControllerNotes] 
	-- Add the parameters for the stored procedure here
	(@BatchNumber AS INT =0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.CreditControllerNotes
		(Stage, MiddlewareToMiddlewareOut, BatchNumber, SearchType,JDEAccountNumber,CreatedDate,CustomerNumber,JDEActivtyType,JDEActivtyTypeDescription,NoteNumber,JDEUserName,NoteCategory,NoteText,UpdateDate,UpdateTime, OrderNumber)
		SELECT		
			1 AS Stage,
			GETDATE() as MiddlewareToMiddlewareOut,
			@BatchNumber as BatchNumber,
			DNAT1 AS SearchType, 
			DNAN8 AS JDEAccountNumber,
			dbo.Julian_to_Date(DNDTI) AS CreatedDate,
			DNURRF AS CustomerNumber,
			DNAIT AS JDEActivtyType,
			DNDL01 AS JDEActivtyTypeDescription,
			DNAVID AS NoteNumber,
			DNEUSR AS JDEUserName,
			CASE WHEN ISNUMERIC(DNRMK) <> 1 THEN SUBSTRING(DNRMK,1,CHARINDEX('_',DNRMK,1)-1) ELSE DNRMK END AS NoteCategory,
			DNVAR1 AS NoteText,
			dbo.Julian_to_Date(DNUPMJ) as UpdateDate, 
			DNUPMT AS UpdateTime ,
			
			/*CASE WHEN ISNUMERIC(SUBSTRING(DNRMK,1,1)) = 1 THEN 
			
				CASE WHEN CHARINDEX('/', DNRMK) > 0 AND CHARINDEX('/', DNRMK) < 30 THEN
					SUBSTRING(DNRMK, CHARINDEX('/', DNRMK) + 1, 
					CHARINDEX('/', DNRMK) + 1 - CHARINDEX(' ', SUBSTRING(DNRMK, CHARINDEX('/', DNRMK) + 1,255))) 
				ELSE '' END 
			ELSE
			
				''
			END AS OrderNumber*/
			DNRMK AS OrderNumber
			
			FROM F56008 f56
			
			
		LEFT OUTER JOIN dbo.CreditControllerNotes CCN ON (f56.DNAVID = CCN.NoteNumber AND dbo.Julian_to_Date(DNUPMJ) = UpdateDate)
		INNER JOIN dbo.Customers cus ON (f56.DNAN8 = cus.Jde_Account_Number)
		WHERE CCN.id IS NULL
		
	SELECT COUNT(*) FROM dbo.CreditControllerNotes WHERE Stage = 1 AND BatchNumber = @BatchNumber 
	
END
GO
