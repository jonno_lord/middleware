USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_PublicationToBusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_PublicationToBusinessUnit]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_PublicationToBusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st February 2011
-- Description:	Removes publication to business unit mapping by specified id
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_PublicationToBusinessUnit] 
	-- Add the parameters for the stored procedure here
	@publicationToBusinessUnitid AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM JDE_Publication_to_Business_Unit
    WHERE id = @publicationToBusinessUnitid
END
GO
