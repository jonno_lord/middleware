USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerTypePayments]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_CustomerTypePayments]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerTypePayments]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 4th February 2011
-- Description:	Selects all customer type payment details from customer type payment and payment types
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CustomerTypePayments] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM 
      (SELECT apt.*, actg.Group_Code, actg.Group_Description 
      FROM asop_Payment_types apt, asop_Customer_Type_Group actg) AS apt
      LEFT OUTER JOIN asop_Customer_Type_payment actp ON actp.Group_Code = apt.Group_Code 
      AND actp.payment_type = apt.Payment_Type AND actp.Payment_Setting = apt.Payment_setting
      
      LEFT OUTER JOIN ASOP_Invoice_Types ait ON ait.Invoice_Type = apt.Invoice_Type
      ORDER BY Name
END
GO
