USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerTypes]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_CustomerTypes]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerTypes]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st January 2011
-- Description:	Selects all customer types and customer groups
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CustomerTypes] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT act.[Customer_Type_ID], act.[Short_name],act.[Long_name],act.[Group_code],actg.Group_Description
	FROM ASOP_Customer_Type AS act
	LEFT JOIN [dbo].[ASOP_Customer_Type_Group] actg ON actg.Group_Code = act.Group_Code
	ORDER BY Long_Name
  
END
GO
