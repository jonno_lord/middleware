USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_TitleCard]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_TitleCard]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_TitleCard]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Elizabeth Hamlet
-- Create date: 14th February 2011
-- Description:	Selects publication to business units as well as joining to retrieve title card and business unit names and decriptions
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_TitleCard] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SHORT_NAME + ' - ' + LONG_NAME AS TITLECARDDESCRIPTION, SHORT_NAME AS SHORTNAME FROM ASOP_TITLE_CARD ATC 
	LEFT JOIN DBO.JDE_PUBLICATION_TO_BUSINESS_UNIT JPBU ON ATC.SHORT_NAME = JPBU.PUBLICATION
	WHERE JPBU.PUBLICATION IS NULL AND ATC.DISCONTINUE_DATE IS NULL
	ORDER BY TITLECARDDESCRIPTION
END

GO
