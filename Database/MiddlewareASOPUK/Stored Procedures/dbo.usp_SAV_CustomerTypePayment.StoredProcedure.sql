USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CustomerTypePayment]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SAV_CustomerTypePayment]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CustomerTypePayment]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 7th February 2011
-- Description:	Either Updates or Inserts based on whether mapping exists in customer type payment
-- Updates to Payment Types 
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_CustomerTypePayment] 
	-- Add the parameters for the stored procedure here

	@GroupCode AS NVARCHAR(50),
	@PaymentType AS NVARCHAR(50),
	@PaymentSetting AS NVARCHAR(50),
	@Receipt AS BIT,
	@Jersey AS BIT,
	@JdeAccountNo AS VARCHAR(20),
	@DocumentType AS VARCHAR(50),
	@CreditDocumentType AS VARCHAR(50),
	@InvoiceType AS VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM ASOP_Customer_Type_Payment WHERE Group_Code = @GroupCode AND Payment_Type = @PaymentType AND Payment_Setting = @PaymentSetting)
	BEGIN
		UPDATE ASOP_Customer_Type_Payment SET Receipt = @Receipt, Jersey = @Jersey, Jde_Account_No = @JdeAccountNo
		WHERE Group_Code = @GroupCode AND Payment_Type = @PaymentType AND Payment_Setting = @PaymentSetting
	END
	ELSE
	BEGIN
		INSERT INTO  ASOP_Customer_Type_Payment(Group_Code, Payment_Type, Payment_Setting, Receipt, Jersey, Jde_Account_No)
		VALUES (@GroupCode, @PaymentType, @PaymentSetting, @Receipt, @Jersey, @JdeAccountNo)
	END
	
	--updates payment types table
	UPDATE ASOP_Payment_Types SET Document_Type = @DocumentType, Credit_Document_Type = @CreditDocumentType, Invoice_Type = @InvoiceType
	WHERE Payment_Type = @PaymentType AND Payment_setting = @PaymentSetting
END
GO
