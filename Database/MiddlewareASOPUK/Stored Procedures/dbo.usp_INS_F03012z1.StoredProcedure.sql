USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012z1]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F03012z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012z1]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F03012z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F03012Z1 f
		INNER JOIN Customers c ON c.id = f.voedtn
		WHERE Stage = 1 AND Accepted = 1 AND ISNULL(JDE_Account_Number,0) = 0	

	INSERT INTO dbo.F03012Z1
	(CustomerId, VOACL, VOCRCD, VOEDBT, VOEDLN, VOEDTN, VOEDUS, VOPOPN, VOPORQ, VORYIN, VOSTMT, VOTNAC, VOTRAR, VOTXA1, VOALKY, VOBADT)
	SELECT 
	Id																												AS CustomerId,
	CONVERT(FLOAT, dbo.fn_CreditLimit(Describes_Agency, Title_Card))												AS VOACL,
	LEFT(dbo.fn_PreferredBillingCurrency(Country_Code), 3 )															AS VOCRCD,
	Batch_Number																									AS VOEDBT,
	LEFT(dbo.fn_LineNumber(), 1)																					AS VOEDLN,
	id																												AS VOEDTN,
	LEFT(dbo.fn_MWUserName(), 4)																					AS VOEDUS,
	LEFT(dbo.fn_PersonOpeningAccount(), 4)																			AS VOPOPN,
	LEFT(dbo.fn_ReferenceRequired(Reference_Required_Setting, Describes_House_Customer), 1)							AS VOPORQ,
	LEFT(dbo.fn_PaymentInstrumentCustomerLoad(Payment_Setting), 1)													AS VORYIN,
	LEFT(dbo.fn_DelinquencyNotice(Payment_Setting), 3)																AS VOSTMT,
	LEFT(dbo.fn_TransactionSetting(), 2)																			AS VOTNAC,
	LEFT(dbo.fn_PaymentTermsCustomersLoad(Describes_Agency, Payment_Setting, Title_Card, Summary_Card ), 3)			AS VOTRAR,
	LEFT(dbo.fn_VATRateCodeCustomerLoad(Describes_House_Customer, Vat_Reg_No, Tax_Exempt_Number, Country_Code), 10) AS VOTXA1,
	LEFT(dbo.fn_LongAddressNumber(c.Urn_Number), 20)																AS VOALKY,
	LEFT(dbo.fn_BillingType(),1)																					AS VOBADT
	FROM Customers C
	WHERE Stage = 1 AND Accepted = 1 AND ISNULL(JDE_Account_Number,0) = 0	
	
END
GO
