USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F47011]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F47011]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN
	
	-- @JL Don't think we need this anymore as SYDOCO will be unique going forward
	-- DELETE F47 FROM F47011 F47 
	--	INNER JOIN Orders ON F47.SYDCTO = Orders.Document_Type AND F47.SYDOCO = Orders.Urn_Number 
	--	WHERE Orders.Stage = 1
	
	INSERT INTO F47011
		(SYAN8,SYCRCD,SYDCTO,SYDEL1,SYDEL2,SYDOCO,SYKCOO,SYMCU,SYTORG,SYTPUR,SYTRDJ,SYVR01,SYVR02,SYEDOC)
		SELECT DISTINCT  
			o.Account_No																							AS SYAN8,
			LEFT(o.negotiation_currency, 3)																			AS SYCRCD,
			LEFT(dbo.fn_DocumentType(Account_No,SUM(Order_Insert_Breakdown),SUM(Order_Insert_Breakdown_cy),
										Payment_Type_short,payment_setting_short),2)								AS SYDCTO,
			dbo.fn_RemoveSpecialChars(LEFT(o.Client_Name,  30))														AS SYDEL1,
			dbo.fn_RemoveSpecialChars(LEFT(dbo.[fn_QualifiedContactName](o.Contact_Title, 
										o.Contact_forenames, o.Contact_Surname), 30))								AS SYDEL2,
			dbo.fn_OracleOrderNumber(o.urn_number,MAX(o.Line_Number))												AS SYDOCO,
			LEFT(dbo.[fn_CompanyFromBusinessUnit](o.Title_Card_short), 5)											AS SYKCOO,
			LEFT(dbo.[fn_BusinessUnit](o.Title_Card_Short), 12)														AS SYMCU,
			LEFT(dbo.fn_MWUserName(), 4)																			AS SYTORG,
			LEFT(dbo.fn_TransactionSetting(), 2)																	AS SYTPUR,
			dbo.fn_DateToOracleJulian(MIN(o.Create_Time))															AS SYTRDJ,
			dbo.fn_RemoveSpecialChars(LEFT(o.advertisers_reference, 25))											AS SYVR01,
			LEFT(dbo.fn_QualifiedSalespersonName(o.Salesperson_Forenames, o.Salesperson_Surname),30)				AS SYVR02,
			o.urn_number																							AS SYEDOC
		FROM Orders as O
		INNER JOIN Customers c ON o.Account_No = c.JDE_Account_Number
		WHERE o.Stage = 1
		GROUP BY Account_No, o.URN_Number, O.Document_Type, O.negotiation_currency, o.Client_Name, o.Contact_Title,
			o.Contact_forenames, o.Contact_Surname, o.batch_number, o.Title_Card_short,c.ShipTo, o.Title_Card, o.Advertisers_Reference,
			o.Salesperson_Forenames, o.Salesperson_Surname, Payment_Type_short, payment_setting_short
		
		
		UPDATE F47 Set f47.OrdersId = Ord.id FROM F47011 F47
		INNER JOIN Orders Ord ON (F47.SYDCTO = LEFT(dbo.fn_DocumentType(Account_No,Order_Insert_Breakdown,Order_Insert_Breakdown_cy,
										Payment_Type_short,payment_setting_short),2) AND F47.SYEDOC = Ord.urn_number)
				WHERE Stage = 1 AND f47.OrdersId IS NULL
	
END

GO
