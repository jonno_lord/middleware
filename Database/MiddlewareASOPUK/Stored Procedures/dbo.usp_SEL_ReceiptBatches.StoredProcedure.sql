USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ReceiptBatches]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_ReceiptBatches]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ReceiptBatches]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SEL_ReceiptBatches]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @order_URN AS VARCHAR(100)
	DECLARE  csrOrders CURSOR FOR 
		select DISTINCT ORDER_URN from [vw_Credit_Card_Orders] where mw_to_jde is null 
			and jde_account_no is not null and receipt_required = 1 and batch_selected = 1

	OPEN csrOrders
	FETCH NEXT FROM csrOrders INTO @order_URN
	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXECUTE dbo.usp_SET_PageNumbers @order_URN

		FETCH NEXT FROM csrOrders INTO @order_URN

	END

	CLOSE csrOrders
	DEALLOCATE csrOrders


	select DISTINCT BATCH_NUMBER from [vw_Credit_Card_Orders] where mw_to_jde is null 
		and jde_account_no is not null and receipt_required = 1 and batch_selected = 1
	
END
GO
