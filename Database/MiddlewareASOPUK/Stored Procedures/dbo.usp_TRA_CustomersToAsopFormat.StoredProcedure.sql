USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToAsopFormat]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToAsopFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToAsopFormat]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TRA_CustomersToAsopFormat]
	-- Add the parameters for the stored procedure here
	@BatchNumber As VARCHAR(10)
AS
BEGIN

	SET NOCOUNT ON
	
	select * into #cco from MiddlewareJDE.dbo.vw_CreditControllers 	
	
	select * into #pay from MiddlewareJDE.dbo.vw_paymentterms pay
	
	select * into #jevg from MiddlewareJDE.dbo.vw_ECVatGroups 
	
	DECLARE @Account_Status_ID AS INT
	select @Account_Status_ID = Account_Status_ID  from ASOP_Account_Status where Name LIKE 'Satisfactory%'

	
	UPDATE Cus SET Stage = 5, MiddlewareToMiddlewareOut = GETDATE(),
		cus.Q1ACL = f55.Q1ACL,
		jde_account_number = LTRIM(RTRIM(STR(Q1AN8,50))),
		town = REPLACE(LTRIM(RTRIM(f55.Q1CTY1)),'''',''),
		county = REPLACE(LTRIM(RTRIM(f55.Q1COUN)),'''',''),
		postcode = LEFT(REPLACE(LTRIM(RTRIM(f55.Q1ADDZ)),'''',''),10),
		credit_limit = CAST(f55.Q1ACL AS MONEY),
		vat_reg_no = LTRIM(RTRIM(f55.Q1TAX)),
		--vat_reg_no = CASE WHEN LTRIM(RTRIM(f55.Q1TAX)) <> '' THEN
		--	CASE WHEN JDEEC.Country_Code IS NOT NULL THEN
		--		'999'
		--	ELSE
		--		REPLACE(LTRIM(RTRIM(f55.Q1TAX)),'''','')
		--	END
		--ELSE
		--	''
		--END,
		tax_exempt_number = LTRIM(RTRIM(f55.Q1TXCT)),
		negotiation_currency = CASE WHEN LTRIM(RTRIM(f55.Q1CRCD)) = '' THEN
				'GBP'
			ELSE
				REPLACE(LTRIM(RTRIM(f55.Q1CRCD)),'''','')
			END,
		country_code = REPLACE(LTRIM(RTRIM(f55.Q1CTR)),'''',''),
		country = ctr.long_name,
		payment_terms = #pay.PaymentTerms,

		credit_controller_notes = 'Credit Controller ' + REPLACE(LTRIM(RTRIM(f55.CreditController)),'''',''),
		

		account_status_id = ISNULL(acc.account_status_id,@Account_Status_ID),

		-- Address
		Address = LEFT(CASE WHEN UPPER(LTRIM(RTRIM(F55.Q1ADD1))) <> 'ACCOUNTS PAYABLE' THEN 
			LTRIM(RTRIM(F55.Q1ADD1)) + CHAR(13) + CHAR(10) 
		ELSE
			''
		END +

		CASE WHEN F55.Q1ADD2 IS NOT NULL THEN
			LTRIM(RTRIM(F55.Q1ADD2)) + CHAR(13) + CHAR(10)
		ELSE
			''
		END +

		CASE WHEN F55.Q1ADD3 IS NOT NULL THEN
			LTRIM(RTRIM(F55.Q1ADD3)) + CHAR(13) + CHAR(10)
		ELSE
			''
		END +

		CASE WHEN F55.Q1ADD4 IS NOT NULL THEN
			LTRIM(RTRIM(F55.Q1ADD4)) + CHAR(13) + CHAR(10)
		ELSE
			''
		END,100),


		--Cat_Code_28
		cat_code_28 = CASE WHEN LTRIM(f55.Q1AC28) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1AC28)),'''','')
		END,


		--Cat_Code_29
		cat_code_29 = CASE WHEN LTRIM(f55.Q1AC29) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1AC29)),'''','')
		END,


		--Cat_Code_30
		cat_code_30 = CASE WHEN LTRIM(f55.Q1AC30) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1AC30)),'''','')
		END,


		--reference_required_setting
		reference_required_setting = CASE WHEN LTRIM(RTRIM(f55.Q1PORQ)) = 'N' THEN
			'NO'
		ELSE
			'MHS'
		END,


		--Q1AC05
		Q1AC05 = CASE WHEN LTRIM(RTRIM(f55.Q1AC05)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1AC05)),'''','')
		END,


		--Q1AC06
		Q1AC06 = CASE WHEN LTRIM(RTRIM(f55.Q1AC06)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1AC06)),'''','')
		END,


		Q1AN81 = '' + LTRIM(RTRIM(f55.Q1AN81)),
		Q1AMCR = '' + LTRIM(RTRIM(f55.Q1AMCR)),


		--Q1CM
		Q1CM = CASE WHEN LTRIM(RTRIM(f55.Q1CM)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1CM)),'''','')
		END,


		--Q1CMGR
		Q1CMGR = CASE WHEN LTRIM(RTRIM(f55.Q1CMGR)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1CMGR)),'''','')
		END,


		--Q1EXR1
		Q1EXR1 = CASE WHEN LTRIM(RTRIM(f55.Q1EXR1)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1EXR1)),'''','')
		END,


		--Q1HOLD
		Q1HOLD = CASE WHEN LTRIM(RTRIM(f55.Q1HOLD)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1HOLD)),'''','')
		END,


		--Q1LNGP
		Q1LNGP = CASE WHEN LTRIM(RTRIM(f55.Q1LNGP)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1LNGP)),'''','')
		END,


		--Q1MLN1
		Q1MLN1 = CASE WHEN LTRIM(RTRIM(f55.Q1MLN1)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1MLN1)),'''','')
		END,


		--Q1TXA1
		Q1TXA1 = CASE WHEN LTRIM(RTRIM(f55.Q1TXA1)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1TXA1)),'''','')
		END,


		--Q1AC02
		Q1AC02 = CASE WHEN LTRIM(RTRIM(f55.Q1AC02)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1AC02)),'''','')
		END,


		--Q1TRAR
		Q1TRAR = CASE WHEN LTRIM(RTRIM(f55.Q1TRAR)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1TRAR)),'''','')
		END,


		--Q1PORQ
		Q1PORQ = CASE WHEN LTRIM(RTRIM(f55.Q1PORQ)) = '' THEN
			NULL
		ELSE
			REPLACE(LTRIM(RTRIM(f55.Q1PORQ)),'''','')
		END
		FROM Customers cus
			INNER JOIN F550101 f55 ON cus.URN_number = f55.Q1URRF AND cus.JDE_Account_Number = F55.Q1AN8
			LEFT OUTER JOIN #pay ON f55.Q1TRAR = #pay.OracleCode
			LEFT OUTER JOIN #cco ON f55.Q1AC02 = #cco.Initials 
			LEFT OUTER JOIN ASOP_country ctr ON f55.Q1CTR = LEFT(ctr.short_name,2)
			LEFT OUTER JOIN ASOP_account_status acc ON f55.Q1CM = acc.import_code
			LEFT OUTER JOIN  #jevg ON F55.q1ctr = #jevg.CountryCode
		WHERE f55.Q1URRF <> ''

END

GO
