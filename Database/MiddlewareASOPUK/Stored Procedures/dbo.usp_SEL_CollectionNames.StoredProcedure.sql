USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CollectionNames]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_CollectionNames]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CollectionNames]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 4th August 2011
-- Description:	Selects logon names from jde credit controllers view
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CollectionNames] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LogonName FROM  MiddlewareJDE.dbo.vw_CreditControllers
END
GO
