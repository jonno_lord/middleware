USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F0911z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F0911z1] 
	@BatchNumber AS INT
AS
BEGIN

-- remove invalid and incomplete entries
DELETE FROM F0911z1 WHERE Processed IS NULL

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	INSERT INTO F0911z1
	(VNEDTN,VNEDLN,VNEDUS,VNEDBT,VNDCT,VNDOC,VNDGJ,VNOBJ,VNSBL,VNAA,VNEXA,VNEXR,VNACR,VNDKC,VNODOC,VNMCU,VNCRCD,VNSFX,VNSTAM,VNCTAM)
	SELECT  0																																	AS VNEDTN,
	0																																			AS VNEDLN,
	'CRCDASOP1'																																	AS VNEDUS,
	'CRCD' + REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '')																				AS VNEDBT,
	'G6'																																		AS VNDCT,
	0																																			AS VNDOC,
	dbo.fn_DateToOracleJulian(GETDATE())																										AS VNDGJ,
	Object_Account																																AS VNOBJ,
	CONVERT(varchar(6),Issue_date,112)																											AS VNSBL,
	CASE WHEN acco.Currency = 'GBP' THEN SUM(Output_Net_Price) * -1 ELSE 0 END																	AS VNAA,
	'CRCDASOP01/' + REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '')																		AS VNEXA,
	CASE WHEN jpbu.Monthly_Weekly = 'W' THEN CONVERT(VARCHAR(10), issue_date, 103) +  ' ' + LEFT(acco.publication, 18) 
											ELSE SUBSTRING(CONVERT(VARCHAR(12), issue_date, 6), 4, 6) +  ' ' + LEFT(acco.Publication, 18) END	AS VNEXR,
	CASE WHEN acco.Currency <> 'GBP' THEN Sum(Output_Net_Price) * -1 ELSE 0 END																	AS VNACR,
	--CASE WHEN Gross_Price < 0 THEN dbo.fn_OriginalInvoiceNumber(Order_Urn,Gross_price,Issue_date,Publication_Code) ELSE NULL END				AS VNTXITM,
	--CASE WHEN Gross_price < 0 THEN 'G6' ELSE NULL END																							AS VNEXR1,
	--CASE WHEN Gross_Price < 0 THEN dbo.fn_OriginalInvoiceLineNumber(Order_Urn,Gross_price,Issue_date,Publication_Code) ELSE NULL END			AS VNDLNID,
	1																																			AS VNDKC,
	acco.Order_Urn																																AS VNODOC,
	acco.Business_Unit																															AS VNMCU,
	acco.Currency																																AS VNCRCD,
	acco.SFX																																	AS VNSFX,
	CASE WHEN acco.Currency = 'GBP' THEN SUM(acco.Output_gross_price * -1) - SUM(acco.Output_net_price * -1) ELSE 0 END							AS VNSTAM,
	CASE WHEN acco.Currency <> 'GBP' THEN SUM(acco.Output_gross_price * -1) - SUM(acco.Output_net_price * -1) ELSE 0 END						AS VNCTAM
	FROM ASOP_Credit_Card_Orders acco
	INNER JOIN JDE_Publication_to_Business_Unit jpbu on (jpbu.Publication = acco.Publication_Code)
	WHERE mw_to_mw_out IS NULL and Jde_Account_No IS NOT NULL AND net_price <> 0 and Batch_Selected = 1
	GROUP BY Order_URN, acco.JDE_Account_No,acco.Batch_Number,acco.Currency,acco.Hub_Company,acco.Document_Number,acco.Old_VAT,acco.Business_Unit,acco.SFX,acco.Line_Number,
	acco.Publication,Object_Account, Subsidiary_Account, Issue_Date,Monthly_Weekly,Gross_price,Publication_Code,TransactionPOSID,Tax_Rate_Code,acco.Tax_Rate_Description
    ORDER BY Order_URN, acco.JDE_Account_No,acco.Batch_Number,acco.Currency,acco.Hub_Company,acco.Document_Number,acco.Old_VAT,acco.Business_Unit,acco.SFX,acco.Line_Number,
	acco.Publication,Object_Account, Subsidiary_Account, Issue_Date,Monthly_Weekly,Gross_price,Publication_Code,TransactionPOSID,Tax_Rate_Code,acco.Tax_Rate_Description

	-- set the Transaction numbers on the F0911z1
	EXECUTE dbo.usp_UPD_F0911z1TransactionNumbers 

	-- Set the line numbers on the F0911z1
	EXECUTE dbo.usp_UPD_F0911z1LineNumbers

	-- now put the entries to load in the GL master table, this is used to
	-- send to JDE in the next step.
	INSERT INTO GeneralLedger (MiddlewareToMiddlewareIn,F0911z1id, stage)
		SELECT GETDATE(), id, 2 from f0911z1
		WHERE Processed IS NULL

	UPDATE F09 SET GeneralLedgerId = gl.id
		FROM F0911z1 F09
		INNER JOIN GeneralLedger GL ON (F09.id = gl.F0911z1id)
		WHERE Processed IS NULL
END

GO
