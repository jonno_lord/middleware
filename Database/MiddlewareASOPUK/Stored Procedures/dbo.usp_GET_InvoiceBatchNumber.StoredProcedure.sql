USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_InvoiceBatchNumber]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_InvoiceBatchNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_InvoiceBatchNumber]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_InvoiceBatchNumber]
	-- Add the parameters for the stored procedure here
	@MiddlewareBatchNumber AS INT = 0, @BatchNumber AS INT OUTPUT
AS
BEGIN

	INSERT INTO InvoiceBatches(MiddlewareBatchNumber) VALUES (@MiddlewareBatchNumber)
	SELECT @BatchNumber = @@IDENTITY 
	
	SELECT @BatchNumber
	 
	RETURN @BatchNumber

END
GO
