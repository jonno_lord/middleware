USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_PublicationToBusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SAV_PublicationToBusinessUnit]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_PublicationToBusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st February 2011
-- Description:	updates specified publication to business unit mappings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_PublicationToBusinessUnit] 
	-- Add the parameters for the stored procedure here
	@id AS INT = 0,
	@businessUnit AS VARCHAR(8),
	@publication AS VARCHAR(4),
	@collectionManager AS VARCHAR(50),
	@collectionAgent AS VARCHAR(50),
	@monthlyWeekly AS CHAR(1),
	@taxCompany AS VARCHAR(4),
	@salesAdminName AS VARCHAR(50),
	@salesAdminPhone AS VARCHAR(50),
	@adminFax AS VARCHAR(50),
	@adminEmail AS VARCHAR(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM JDE_Publication_to_Business_Unit WHERE id = @id)
	BEGIN
		UPDATE JDE_Publication_to_Business_Unit SET Business_Unit = @businessUnit, Publication = @publication, Collection_Manager_Client = @collectionManager, Collection_Manager_Agent = @collectionAgent, Monthly_Weekly = @monthlyWeekly, Tax_Company_Code = @taxCompany, Sales_admin_name = @salesAdminName, Sales_admin_tel =  @salesAdminPhone, Sales_admin_fax = @adminFax, Sales_admin_email = @adminEmail
		WHERE id = @id 
	END
	ELSE
	BEGIN
		INSERT INTO JDE_Publication_to_Business_Unit (Business_Unit, Publication, Collection_Manager_Client, Collection_Manager_Agent, Monthly_Weekly, Tax_Company_Code, Sales_admin_name, Sales_admin_tel, Sales_admin_fax, Sales_admin_email)
		VALUES (@businessUnit, @publication, @collectionManager, @collectionAgent, @monthlyWeekly, @taxCompany, @salesAdminName, @salesAdminPhone, @adminFax, @adminEmail)
	END
END
GO
