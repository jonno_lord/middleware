USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03B11z1LineNumbers]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_F03B11z1LineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03B11z1LineNumbers]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UPD_F03B11z1LineNumbers] 
	-- Add the parameters for the stored procedure here
AS

BEGIN

	--** Used for creating the transaction and line numbering details in the cursor
	DECLARE @VJEDTN AS FLOAT
	DECLARE @Line_Number AS INT
	DECLARE @VJSFX AS CHAR(3)
	DECLARE @ID AS INT

	--** Stores the tax area rate
	DECLARE @Last_VJEDTN AS VARCHAR(10)

	--** Set up the last tax area rate
	SET @Last_VJEDTN = ''
	SET @Line_Number = 0

	--** Create a cursor based from the temporary table loaded  (grouped
	--** by the key fields of batch number, bucket account and order currency)
	DECLARE csrF03b11z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VJEDTN, ID FROM F03b11z1 
		WHERE Processed IS NULL
		ORDER BY VJEDTN,VJATXA DESC--Endorsed by SP

	SET @Line_Number = 0

	--** Loop through this created cursor and update the transaction numbers
	OPEN csrF03b11z1
	FETCH NEXT FROM csrF03b11z1 INTO @VJEDTN, @ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF @Last_VJEDTN = @VJEDTN
			SET @Line_Number = @Line_Number + 1
		ELSE 
			SET @Line_Number = 1
		
		SET @VJSFX = LTRIM(RTRIM(CONVERT(VARCHAR(3), @Line_Number)))
		SET @VJSFX = REPLICATE('0', 3 - LEN(@VJSFX)) + @VJSFX

		--** Update the table with the next value
		UPDATE F03b11z1 SET VJSFX = @VJSFX WHERE [ID] = @ID
		UPDATE F03b11z1 SET VJEDLN = @Line_Number WHERE [ID] = @ID

		--** store the previous batch details...
		SET @Last_VJEDTN = @VJEDTN

		--** Get the next entry
		FETCH NEXT FROM csrF03b11z1 INTO @VJEDTN, @ID
	END

	CLOSE csrF03b11z1
	DEALLOCATE csrF03b11z1


END

GO
