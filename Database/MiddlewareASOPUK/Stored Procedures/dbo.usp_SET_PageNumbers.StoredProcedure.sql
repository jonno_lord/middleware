USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_PageNumbers]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SET_PageNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_PageNumbers]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SET_PageNumbers] 
	@URN AS VARCHAR(100)
AS
BEGIN

	DECLARE @entriesPerPage AS INT
	SET @entriesPerPage = 7
	DECLARE @enties AS INTEGER
	select @enties = COUNT(*) from [vw_Credit_Card_Orders] where mw_to_jde is null 
			and jde_account_no is not null and receipt_required = 1
			AND Order_URN = @Urn

	DECLARE @pages AS INTEGER
	SET @Pages = CONVERT(INT, (@enties / @entriesPerPage)) + 1
	IF (@Pages < 1) SET @Pages = 1

	DECLARE  csrOrdersPage CURSOR FOR 
		select id from [ASOP_Credit_Card_Orders] where mw_to_jde is null 
			and jde_account_no is not null and receipt_required = 1
			AND Order_URN = @Urn

	DECLARE @id AS INT
	DECLARE @PageNumber AS INT
	DECLARE @rowCount AS INT

	SET @PageNumber = 1
	SET @rowCount = 0

	OPEN csrOrdersPage
	FETCH NEXT FROM csrOrdersPage INTO @id
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @rowCount = @rowCount + 1

		IF(@rowCount > @entriesPerPage)
		BEGIN
			SET @rowCount = 0
			SET @PageNumber = @PageNumber + 1	
		END
		UPDATE dbo.ASOP_Credit_Card_Orders SET Page_Number = @PageNumber, of_Page_Number = @Pages 
			WHERE id = @id

		FETCH NEXT FROM csrOrdersPage INTO @id

	END

	CLOSE csrOrdersPage
	DEALLOCATE csrOrdersPage

END
GO
