USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ActivityType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_ActivityType]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ActivityType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 22nd September 2011
-- Description:	Deletes payament types
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_ActivityType] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM ASOP_Activity_Types
    WHERE id = @id
END
GO
