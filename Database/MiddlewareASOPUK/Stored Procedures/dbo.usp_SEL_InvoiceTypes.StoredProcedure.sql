USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_InvoiceTypes]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_InvoiceTypes]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_InvoiceTypes]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 7th February 2011
-- Description:	Selects all from invoice types
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_InvoiceTypes] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ASOP_Invoice_Types
END
GO
