USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_PaymentType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SAV_PaymentType]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_PaymentType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,B.M.Patterson>
-- Create date: <Create Date,,01/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_PaymentType]
	-- Add the parameters for the stored procedure here
	@Payment_Type_ID AS int,
	@Name AS NVARCHAR(27),
	@Payment_Type AS NVARCHAR(4),
	@Payment_setting AS NVARCHAR(3),
	@Document_Type AS NVARCHAR(50),
	@Credit_Document_Type AS NVARCHAR(50),
	@Invoice_Type AS NVARCHAR(50)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
		UPDATE ASOP_Payment_Types SET  Name = @Name
		,Payment_Type = @Payment_Type
		,Payment_setting = @Payment_setting
		,Document_Type = @Document_Type
		,Credit_Document_Type = @Credit_Document_Type
		,Invoice_Type = @Invoice_Type
		WHERE Payment_Type_ID =@Payment_Type_ID
	
END
GO
