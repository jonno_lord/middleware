USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ZeroValueOrders]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_ZeroValueOrders]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ZeroValueOrders]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_DEL_ZeroValueOrders]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Orders
	WHERE Id IN ( 
	SELECT id FROM Orders
	WHERE Stage = 1
	GROUP BY id, insert_date, Title_Card
	HAVING SUM(ISNULL(Order_Insert_Breakdown, 0.00)) = 0 
	AND SUM(ISNULL(Order_Insert_Breakdown_cy, 0.00)) = 0)

END

GO
