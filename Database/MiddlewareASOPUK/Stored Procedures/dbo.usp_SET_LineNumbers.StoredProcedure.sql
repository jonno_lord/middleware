USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_LineNumbers]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SET_LineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_SET_LineNumbers]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SET_LineNumbers]
	-- Add the parameters for the stored procedure here
AS
BEGIN

	-- Generate Line numbers
	
	DECLARE csrLines CURSOR FOR
	SELECT id, Urn_Number FROM Orders WHERE Line_Number IS NULL
		ORDER BY Urn_Number, Sequence_Number 

	OPEN csrLines
	DECLARE @id AS INT
	DECLARE @UrnNumber AS INT

	FETCH NEXT FROM csrLines INTO @id, @UrnNumber
	WHILE @@FETCH_STATUS = 0
	BEGIN

		DECLARE @NextLineNumber AS INT
		
		-- calculate the next line number
		SELECT @NextLineNumber = CASE WHEN MAX(Line_Number) IS NULL THEN 1 ELSE MAX(Line_Number) + 1 END FROM Orders WHERE Urn_Number = @UrnNumber 
		UPDATE Orders SET Line_Number = @NextLineNumber WHERE id = @id

		PRINT @id
		FETCH NEXT FROM csrLines INTO @id, @UrnNumber
		
	END

	CLOSE csrLines
	DEALLOCATE csrLines


END
GO
