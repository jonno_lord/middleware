USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PaymentType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_PaymentType]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PaymentType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,B M Patterson>
-- Create date: <Create,,01/2/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_PaymentType]
	-- Add the parameters for the stored procedure here
	@Payment_Type_ID AS integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT [Payment_Type_ID]
		  ,[Name]
		  ,[Payment_Type]
		  ,[Payment_setting]
		  ,[Document_Type]
		  ,[Credit_Document_Type]
		  ,[Invoice_Type]
	FROM [MiddlewareASOPUK].[dbo].[ASOP_Payment_Types]
	WHERE [Payment_Type_ID] = @Payment_Type_ID
END
GO
