USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ASOPReasonCode]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_ASOPReasonCode]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ASOPReasonCode]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Elizabeth Hamlet
-- Create date: 14th February 2011
-- Description:	Selects publication to business units as well as joining to retrieve title card and business unit names and decriptions
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ASOPReasonCode] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CONVERT(VARCHAR(10),REASON_ID) + ' - ' + LONG_NAME AS ASOPREASONCODE, REASON_ID FROM ASOP_REASON_CODE
	ORDER BY REASON_ID
	
END
GO
