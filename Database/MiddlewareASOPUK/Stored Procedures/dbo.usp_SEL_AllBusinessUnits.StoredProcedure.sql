USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_AllBusinessUnits]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_AllBusinessUnits]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_AllBusinessUnits]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barry Chuckle
-- http://en.wikipedia.org/wiki/Chuckle_Brothers
-- Create date: 1st February 2011
-- Description:	Selects publication to business units as well as joining to retrieve title card and business unit names and decriptions
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_AllBusinessUnits] 
	@publication AS VARCHAR(4) = '',
	@titleCard AS VARCHAR(50) = '',
	@businessUnit AS VARCHAR(4) = '',
	@businessUnitDescription AS VARCHAR(50) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT 
		   jpbu.id, 
		   vjde.BusinessUnit AS BusinessUnit, 
		   vjde.[Description] AS BusinessUnitDescription
	FROM MiddlewareJDE.dbo.vw_JdeBusinessUnits AS vjde
		LEFT JOIN JDE_Publication_to_Business_unit AS jpbu ON jpbu.Business_Unit =  vjde.BusinessUnit
	  WHERE (jpbu.Publication = @publication OR jpbu.Publication LIKE @publication OR @publication = '')
		AND (jpbu.Business_Unit = @businessUnit  OR @businessUnit = '')
		AND (vjde.[Description] = @businessUnitDescription OR vjde.[Description] LIKE @businessUnitDescription OR @businessUnitDescription = '')
	ORDER BY vjde.Businessunit

END

GO
