USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ClashingTitleCards]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_ClashingTitleCards]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ClashingTitleCards]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 9th February 2011
-- Description:	Selects all clashing title cards between MiddlewareASOPUK and MiddlewareASOPDaltons
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ClashingTitleCards]
	-- Add the parameters for the stored procedure here

	@longName AS VARCHAR(27) = '',
	@shortName AS VARCHAR(4) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @longName = '%' + @longName + '%'
	SET @shortName = '%' + @shortName + '%'
	
    -- Insert statements for procedure here
	
	SELECT DISTINCT '(' + aUk.Short_name + ') ' + aUk.Long_name as Long_Name, '('+ aDl.Short_name +') ' + aDl.Long_name as Short_name FROM MiddlewareASOPUK.dbo.ASOP_Title_Card aUK
	LEFT OUTER JOIN MiddlewareASOPDaltons.dbo.ASOP_Title_Card aDl ON aUK.Short_name  = aDL.Short_name 
	WHERE aDl.Short_name IS NOT NULL 
	AND aUk.Long_name LIKE @longName AND aUk.Short_name LIKE @shortName
	AND 
	NOT (aUk.Discontinue_date IS NOT NULL OR aDl.Discontinue_date IS NOT NULL)

END
GO
