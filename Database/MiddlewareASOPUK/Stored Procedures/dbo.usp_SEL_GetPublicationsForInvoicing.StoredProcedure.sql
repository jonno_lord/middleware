USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_GetPublicationsForInvoicing]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_GetPublicationsForInvoicing]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_GetPublicationsForInvoicing]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_GetPublicationsForInvoicing]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 45 as id, 'IBM' as Code, 'Ian Beale Monthly' as Description
    UNION
    SELECT 65, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 262, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 24, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 25, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 26, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 27, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 28, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 29, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 30, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 31, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 32, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 33, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 34, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 35, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 36, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 37, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 38, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 39, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 40, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 41, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 42, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 43, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 44, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 46, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 47, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 48, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 49, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 12, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 10, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 11, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 1, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 2, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 3, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 4, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 5, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 6, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 7, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 8, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 9, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 75, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 76, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 77, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 78, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 79, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 80, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 81, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 82, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 83, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 84, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 85, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 86, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 87, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 88, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 89, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 90, 'FG', 'Farmers Guardian' 
    UNION
    SELECT 91, 'NAMBLA', 'National Man-bear pig association'
    UNION 
    SELECT 92, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 93, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 94, 'FG', 'Farmers Guardian' 
    UNION 
    SELECT 95, 'FG', 'Farmers Guardian' 
    

END
GO
