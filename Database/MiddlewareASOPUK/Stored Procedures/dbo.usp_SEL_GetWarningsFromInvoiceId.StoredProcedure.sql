USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_GetWarningsFromInvoiceId]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_GetWarningsFromInvoiceId]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_GetWarningsFromInvoiceId]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_GetWarningsFromInvoiceId]

	@InvoiceId as int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT '121564' as URN, 'Karl Pilkington' as CustomerName, 'Head like an orange' as Description
	UNION
	SELECT '45532', 'Ambrose', 'Comedy Genius'
	UNION
	SELECT '465762', 'Presly', 'Has Ian Beale Issues'
END
GO
