USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ValidSummaryCard]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_ValidSummaryCard]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ValidSummaryCard]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st January 2011
-- Description:	Updates Valid Summary Card
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_ValidSummaryCard] 
	-- Add the parameters for the stored procedure here
	@id int, 
	@objectCode AS VARCHAR(50),
	@subsidiaryCode AS VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE ASOP_Valid_Summary_Card SET JDE_Object_Code = @objectCode, JDE_Subsidiary_Code = @subsidiaryCode
	WHERE Valid_Summary_Card_Id = @id
END
GO
