USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F0911z1TransactionNumbersBKP]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_F0911z1TransactionNumbersBKP]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F0911z1TransactionNumbersBKP]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UPD_F0911z1TransactionNumbersBKP] 
	-- Add the parameters for the stored procedure here
AS

BEGIN
	--** Used for creating the transaction and line numbering details in the cursor
	DECLARE @VNEDTN AS INTEGER
	DECLARE @Transaction_Id AS INTEGER
	DECLARE @Line_Number AS INTEGER
	DECLARE @VNAN8 AS FLOAT
	DECLARE @VNVINV AS VARCHAR(30)
	DECLARE @VNCRCD AS VARCHAR(30)
	DECLARE @VNKCO AS VARCHAR(5)
	DECLARE @VNOLDVAT AS CHAR -- Added for CR32760 WO48106


	DECLARE @ID AS INTEGER

	--** Stores the previous batch details
	DECLARE @last_VNVINV AS VARCHAR(30)
	DECLARE @Last_VNAN8 AS FLOAT
	DECLARE @Last_VNCRCD AS  VARCHAR(30)
	DECLARE @Last_VNKCO AS  VARCHAR(5)
	DECLARE @Last_VNOLDVAT AS CHAR  -- Added for CR32760 WO48106

	--** Set up the last details so that
	--** they will break on the first occurence of entering the
	SET @Last_VNAN8 = 0
	SET @Last_VNCRCD = ''
	SET @Last_VNKCO = ''
	SET @Last_VNOLDVAT = '0'  -- Added for CR32760 WO48106
	--** Check to make sure the asop line number has been defined correctly
	IF NOT EXISTS(select Guid_Id from MiddlewareCommon.dbo.ASOPGuids WHERE System_Description = 'MW_ASOP_LINE_NO')
		RAISERROR ('Unable to find the next asop transaction line number', 16, 1)

	--** begin the transaction accross the GUIDs table... 
	SELECT @Transaction_Id = Guid_Id FROM MiddlewareCommon.dbo.ASOPGuids WHERE System_Description = 'MW_ASOP_LINE_NO'

	--** Create a cursor based from the temporary table loaded  (grouped
	--** by the key fields of batch number, bucket account and order currency)
	DECLARE csrF0911z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VNVINV, VNKCO,VNAN8, VNCRCD, [ID], VNOLDVAT FROM F0911z1 
		WHERE GeneralLedgerId IS NULL
		ORDER BY VNVINV, VNKCO,VNAN8, VNCRCD, [ID], VNOLDVAT
		
	--** Loop through this created cursor and update the transaction numbers
	OPEN csrF0911z1
	FETCH NEXT FROM csrF0911z1 INTO @VNVINV, @VNKCO,@VNAN8, @VNCRCD, @ID,@VNOLDVAT
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Increment the transaction number if the batch no, account or currency changed
		IF 	@VNVINV <> @Last_VNVINV OR
			@VNAN8 <> @Last_VNAN8 OR
			@VNCRCD <> @Last_VNCRCD OR
			@VNKCO <> @Last_VNKCO OR
			@VNOLDVAT <> @Last_VNOLDVAT  -- Added for CR32760 WO48106

			SET @Transaction_Id = @Transaction_Id + 1

		--** Update the table with the next value
		UPDATE F0911z1 SET VNEDTN = @Transaction_Id WHERE [ID] = @ID
		UPDATE F0911z1 SET VNDOC = @Transaction_Id WHERE [ID] = @ID

		--** store the previous batch details...
		SET @last_VNVINV = @VNVINV
		SET @last_VNAN8 = @VNAN8
		SET @last_VNCRCD = @VNCRCD
		SET @last_VNKCO = @VNKCO
		SET @last_VNOLDVAT = @VNOLDVAT  -- Added for CR32760 WO48106
		--** Get the next entry
	FETCH NEXT FROM csrF0911z1 INTO @VNVINV, @VNKCO,@VNAN8, @VNCRCD, @ID, @VNOLDVAT

	END

	CLOSE csrF0911z1
	DEALLOCATE csrF0911z1


	UPDATE MiddlewareCommon.dbo.ASOPGuids
		SET Guid_ID = @Transaction_Id
		WHERE System_Description = 'MW_ASOP_LINE_NO'


END
GO
