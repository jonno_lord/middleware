USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ActivityTypes]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_ActivityTypes]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ActivityTypes]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Brenda Patterson>
-- Create date: <Create Date,,02/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ActivityTypes]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT ASOP_Activity_Types.Id, ASOP_Activity_Types.ActivityID ,MiddlewareJDE.dbo.vw_Activities.Description, ASOP_Activity_Types.ReasonID ,ASOP_Reason_Code.Long_name
	FROM ASOP_Activity_Types
	
    INNER JOIN MiddlewareJDE.dbo.vw_Activities ON 
		ASOP_Activity_Types.ActivityID = MiddlewareJDE.dbo.vw_Activities.ActivityId
	INNER JOIN ASOP_Reason_Code ON ASOP_Activity_Types.ReasonID = ASOP_Reason_Code.Reason_ID
	
END
GO
