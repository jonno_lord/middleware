USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TaxCompany]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_TaxCompany]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TaxCompany]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th July 2011
-- Description:	gets tax company by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_TaxCompany] 
	-- Add the parameters for the stored procedure here
	@id int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Tax_Company
	WHERE Tax_Company_ID = @id
END
GO
