USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToJDEFormat]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_OrdersToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToJDEFormat]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_TRA_OrdersToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION

		EXECUTE dbo.usp_DEL_ZeroValueOrders 
	
		EXECUTE dbo.usp_INS_F47011  @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE dbo.usp_INS_F47012 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
        
        --EXECUTE MiddlewareCommon.[dbo].[usp_UPD_OrderPaymentTerms] @SystemName='MiddlewareASOPUK'
        --IF(@@ERROR <> 0) GOTO Failure
        
        EXECUTE MiddlewareCommon.[dbo].[usp_UPD_OrderTaxCode] @SystemName='MiddlewareASOPUK'
        IF(@@ERROR <> 0) GOTO Failure
        
		-- Mark all customers as ready for the next stage
		UPDATE Orders SET Stage = 2, MiddlewaretoMiddlewareIn = GETDATE() WHERE Stage = 1
		IF(@@ERROR <> 0) GOTO Failure

	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F47012 WHERE SZEDBT = @BatchNumber

END


GO
