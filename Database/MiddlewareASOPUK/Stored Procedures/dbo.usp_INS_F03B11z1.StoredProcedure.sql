USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03B11z1]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F03B11z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03B11z1]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_INS_F03B11z1] 
	@BatchNumber AS INT
AS
BEGIN

-- remove invalid and incomplete entries
DELETE FROM F03B11z1 WHERE Processed IS NULL

INSERT INTO F03b11z1
(VJEDTN,VJEDUS,VJEDBT,VJDCT,VJAN8,VJDIVJ,VJCO,VJCRRM,VJCRCD,VJTXA1,VJVINV,VJRMK,VJTORG,VJAG,VJACR,VJSTAM,VJMCU,VJODOC,VJSFX)
SELECT CONVERT(INT, 0) AS VJEDTN,
'CRCDASOP1' as VJEDUS,
	'CRCD' + REPLACE(CONVERT(VARCHAR(10), GETDATE(), 3), '/', '') AS VJEDBT,
	'G6' AS VJDCT,
	CONVERT(VARCHAR,acco.JDE_Account_No)+ '-' + '27900',
	dbo.fn_DateToOracleJulian(GETDATE()) as VJDIVJ,
	0 AS VJCO, --Added by Sathish For CR 30967
	CASE WHEN acco.Currency <> 'GBP' THEN 'F' ELSE 'D' END AS VJCRRM,
	CASE WHEN acco.Currency = '' THEN 'GBP' ELSE acco.Currency END AS VJCRCD,
	Tax_Rate_Code as VJTXA1,
	acco.Batch_Number AS VJVINV,
	acco.TransactionPOSID AS VJRMK,
	'MWCRCARD' AS VJTORG,
	CASE WHEN acco.Currency = 'GBP' THEN SUM(Output_Gross_Price)  ELSE 0 END AS VJAG,
	CASE WHEN acco.Currency <> 'GBP' THEN SUM(Output_Gross_Price) ELSE 0 END AS VJACR,
	CASE WHEN acco.Currency = 'GBP' THEN CASE WHEN SUM(output_gross_price) = SUM(output_net_price) THEN 0 ELSE SUM(Output_Vat_Amount) END ELSE 0 END AS VJSTAM,
	acco.Business_Unit as VJMCU,
	acco.Order_Urn AS VJODOC,
	acco.SFX
	FROM ASOP_Credit_Card_Orders acco
	INNER JOIN JDE_Publication_to_Business_Unit jpbu on (jpbu.Publication = acco.Publication_Code)
	WHERE mw_to_mw_out IS NULL and Jde_Account_No IS NOT NULL and Net_price <> 0 and Batch_Selected = 1
	GROUP BY Order_URN, acco.JDE_Account_No,acco.Batch_Number,acco.Currency,acco.Hub_Company,acco.Document_Number,acco.Old_VAT,acco.Business_Unit,acco.SFX,acco.Line_Number,
	acco.Publication,Object_Account, Subsidiary_Account, Issue_Date,Monthly_Weekly,Gross_price,Publication_Code,TransactionPOSID,Tax_Rate_Code,acco.Tax_Rate_Description
    ORDER BY Order_URN, acco.JDE_Account_No,acco.Batch_Number,acco.Currency,acco.Hub_Company,acco.Document_Number,acco.Old_VAT,acco.Business_Unit,acco.SFX,acco.Line_Number,
	acco.Publication,Object_Account, Subsidiary_Account, Issue_Date,Monthly_Weekly,Gross_price,Publication_Code,TransactionPOSID,Tax_Rate_Code,acco.Tax_Rate_Description
	
	UPDATE F03b11z1
	SET VJTXA1 = 'GBOUTSIDE'
	WHERE VJTXA1 = 'OUTSIDE'
	AND Processed IS NULL

	
	EXECUTE  [dbo].[usp_UPD_F03B11z1TransactionNumbers] 
	
	EXECUTE  [dbo].[USP_UPD_F03b11z1LineNumbers]
	
	-- now put the entries to load in the AR master table, this is used to
	-- send to JDE in the next step.
	INSERT INTO AccountsReceivable (MiddlewareToMiddlewareIn,F03B11z1id, stage)
		SELECT GETDATE(), id, 2 from f03B11z1
		WHERE Processed IS NULL

	UPDATE F03 SET AccountsReceivableId = ar.id
		FROM F03B11z1 F03
		INNER JOIN AccountsReceivable AR ON (F03.id = ar.F03B11z1id)
		WHERE Processed IS NULL

END

GO
