USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TransactionPOSID]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_TransactionPOSID]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TransactionPOSID]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_UPD_TransactionPOSID] 
	-- Add the parameters for the stored procedure here
AS

BEGIN

DECLARE @Order_URN VARCHAR(50)
DECLARE @Line_Number INT

DECLARE csrUpdateTransactionPOSID CURSOR FOR

	SELECT Order_URN, Line_Number FROM ASOP_Credit_Card_Orders
	WHERE MW_to_MW_Out IS NULL AND Batch_Selected = 1 
	ORDER BY Order_URN, Line_Number
	
OPEN csrUpdateTransactionPOSID
FETCH NEXT FROM csrUpdateTransactionPOSID INTO @Order_URN, @Line_Number
WHILE @@FETCH_STATUS = 0
	BEGIN

		UPDATE ASOP_Credit_Card_Orders
		SET TransactionPOSID = (SELECT MAX(TransactionPOSID) FROM TblCybsTransactionPOS tpos
		INNER JOIN TblTransactionGroup tg ON tpos.TransactionGroupID = tg.TransactionGroupID
		WHERE LTRIM(RTRIM(CONVERT(VARCHAR(50),tg.TransactionRef))) = @Order_URN)
		WHERE Order_URN = @Order_URN AND Line_Number = @Line_Number

		FETCH NEXT FROM csrUpdateTransactionPOSID INTO @Order_URN, @Line_Number
END

CLOSE csrUpdateTransactionPOSID
DEALLOCATE csrUpdateTransactionPOSID


END


GO
