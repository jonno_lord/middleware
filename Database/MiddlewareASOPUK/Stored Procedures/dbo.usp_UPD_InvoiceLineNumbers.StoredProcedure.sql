USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceLineNumbers]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoiceLineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceLineNumbers]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoiceLineNumbers]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE acco
	SET acco.SFX = qrySFX.SFX
	FROM ASOP_Credit_Card_Orders acco
	INNER JOIN JDE_Publication_to_Business_Unit jpbu on (jpbu.Publication = acco.Publication_Code)
	INNER JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY Order_URN ORDER BY Order_URN, Line_Number) as SFX,Order_URN,acco.JDE_Account_No,acco.Batch_Number,acco.Currency,
				acco.Hub_Company,acco.Document_Number,acco.Old_VAT,acco.Business_Unit,acco.Line_Number,acco.Publication,Object_Account, 
				Subsidiary_Account, Issue_Date,Monthly_Weekly,Gross_price,Publication_Code,TransactionPOSID,Tax_Rate_Code,acco.Tax_Rate_Description
				FROM ASOP_Credit_Card_Orders acco
				INNER JOIN JDE_Publication_to_Business_Unit jpbu on (jpbu.Publication = acco.Publication_Code)
				WHERE mw_to_mw_out IS NULL and Jde_Account_No IS NOT NULL and Net_price <> 0 and Batch_Selected = 1
				GROUP BY Order_URN, acco.JDE_Account_No,acco.Batch_Number,acco.Currency,acco.Hub_Company,acco.Document_Number,acco.Old_VAT,acco.Business_Unit,
				acco.Line_Number,acco.Publication,Object_Account, Subsidiary_Account, Issue_Date,Monthly_Weekly,Gross_price,Publication_Code,
				TransactionPOSID,Tax_Rate_Code,acco.Tax_Rate_Description) as qrySFX
	ON acco.Order_URN = qrySFX.Order_URN and
	acco.JDE_Account_No = qrySFX.JDE_Account_No and
	acco.Batch_Number = qrySFX.Batch_Number and 
	acco.Currency = qrySFX.Currency and 
	ISNULL(acco.Hub_Company,'') = ISNULL(qrySFX.Hub_Company,'') and 
	ISNULL(acco.Document_Number,'') = ISNULL(qrySFX.Document_Number,'') and 
	acco.Old_VAT = qrySFX.Old_VAT and 
	acco.Business_Unit = qrySFX.Business_Unit and
	acco.Line_Number = qrySFX.Line_Number and
	acco.Publication = qrySFX.Publication and
	acco.Object_Account = qrySFX.Object_account and 
	acco.Subsidiary_Account = qrySFX.Subsidiary_account and 
	acco.Issue_Date = qrySFX.Issue_date and
	jpbu.Monthly_Weekly = qrySFX.Monthly_Weekly and
	acco.Gross_price = qrySFX.Gross_price and
	acco.Publication_Code = qrySFX.Publication_Code and
	acco.TransactionPOSID = qrySFX.TransactionPOSID and
	acco.Tax_Rate_Code = qrySFX.Tax_Rate_Code and
	acco.Tax_Rate_Description = qrySFX.Tax_Rate_Description
	WHERE mw_to_mw_out IS NULL 
	AND acco.Jde_Account_No IS NOT NULL 
	AND Net_price <> 0 
	AND Batch_Selected = 1
	
	UPDATE acco
	SET SFX = REPLICATE('0', 3 - LEN(SFX)) + SFX
	FROM ASOP_Credit_Card_Orders acco
	WHERE mw_to_mw_out IS NULL 
	AND Jde_Account_No IS NOT NULL 
	AND Net_price <> 0 
	AND Batch_Selected = 1
	
		
END


		

GO
