USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_CustomerType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_CustomerType]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_CustomerType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st January 2011
-- Description:	Updates ASOP customer type group_code
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_CustomerType] 
	-- Add the parameters for the stored procedure here
	@id AS int,
	@groupCode VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE ASOP_Customer_Type SET Group_code = @groupCode
	WHERE Customer_Type_ID = @id
END
GO
