USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_BusinessUnit]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Elizabeth Hamlet
-- Create date: 14th February 2011
-- Description:	Selects publication to business units as well as joining to retrieve title card and business unit names and decriptions
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_BusinessUnit] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT BUSINESSUNIT + ' - ' + [DESCRIPTION] as BUSINESSUNITDESCRIPTION, BUSINESSUNIT FROM MIDDLEWAREJDE.DBO.VW_JDEBUSINESSUNITS
	WHERE ISNULL(MCPECC,'') <> 'N' AND ISNUMERIC(BUSINESSUNIT) = 1 
	ORDER BY BUSINESSUNIT
END
GO
