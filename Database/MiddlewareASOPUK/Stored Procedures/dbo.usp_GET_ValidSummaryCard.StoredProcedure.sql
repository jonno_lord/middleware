USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ValidSummaryCard]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_ValidSummaryCard]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ValidSummaryCard]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st January 2011
-- Description:	selects ASOP valid summary card and summary card details as specified by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ValidSummaryCard] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN Discontinue_Date IS NULL THEN 1 ELSE 0 END AS Valid,
	  sc.Long_Name as Summary_Card,
      sc.Short_name as Short_Code,
      vsc.JDE_Object_Code, vsc.Valid_Summary_Card_Id,
      vsc.JDE_Subsidiary_Code from ASOP_Valid_Summary_Card vsc
      LEFT JOIN ASOP_Summary_Card sc ON sc.Summary_id = vsc.Summary_id
      WHERE Valid_Summary_Card_Id = @id
END
GO
