USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F47012]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F47012]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- Verify the data is valid, and perform fixes

		--DELETE F12 
		--	FROM F47012 F12
		--INNER JOIN Orders ON F12.SZEDCT = Orders.Document_Type AND F12.SZDOCO = Orders.Urn_Number 
		--WHERE Orders.Stage = 1

		INSERT INTO F47012
		(OrdersId,SZAN8,SZDCTO,SZDOCO,SZDSC1,SZDSC2,SZEXR1,SZFUN2,SZFUP,SZLITM,SZLNID,SZLOCN,SZMCU,SZPTC,SZSBL,SZSERN,SZTXA1,SZUORG,SZUPRC,SZVR02,SZEDOC)
		
		SELECT DISTINCT
				ord.Id																														AS OrdersId,
				ord.Account_No																												AS SZAN8,
				LEFT(dbo.[fn_DocumentType](ord.account_no, ord.Order_Insert_Breakdown,ord.Order_Insert_Breakdown_CY, 
									 ISNULL(ord.Payment_Type_short, ''),ISNULL(ord.Payment_Setting_Short, '')), 2)							AS SZDCTO,
				dbo.fn_OracleDetailOrderNumber(ord.urn_number,ord.batch_number,ord.Title_Card_short)										AS SZDOCO,
				LEFT(ord.Size_Card, 30)																										AS SZDSC1,
				LEFT(dbo.[fn_TitleCardAndPageNumber](LTRIM(RTRIM(ord.Title_Card)), ISNULL(ord.Page_Number,'')), 30)							AS SZDSC2,
				qryCustomers.Q1EXR1																											AS SZEXR1, 
				dbo.[fn_AgencyDiscountPercentage](ord.Feature_Type, ord.Agency_Discount)													AS SZFUN2,
				dbo.[fn_ForeignCurrencyFUPAmount](ord.negotiation_currency, ord.Order_Insert_Breakdown_cy, ord.Feature_Type)				AS SZFUP,
				LEFT(dbo.[fn_FeatureTypeProductionCharge](ord.Feature_Type, ord.Summary_card, ord.Account_No,ord.Payment_Type_short), 40)	AS SZLITM,
				dbo.fn_OrderLineNumber(Line_Number)																							AS SZLNID,
				dbo.[fn_FormatedPublicationDate](ord.Title_Card_short, ord.insert_date, ord.Colour_Card)									AS SZLOCN,
				LEFT(dbo.[fn_BusinessUnit](ord.Title_Card_Short), 12)																		AS SZMCU,
				LEFT(Q1TRAR, 30)																											AS SZPTC,
				LEFT(dbo.[fn_FormatedSubledgerDate](ord.insert_date), 6)																	AS SZSBL,
				LEFT(dbo.[fn_AgencyDiscountFeature](ord.Feature_Type, ord.Class_Card), 40)													AS SZSERN,
				qryCustomers.Q1TXA1																											AS SZTXA1, 				
				LEFT(dbo.[fn_SZUORG](ord.Order_Insert_Breakdown, ord.Order_Insert_Breakdown_cy), 2)											AS SZUORG,
				dbo.[fn_InvoiceAmount](ord.negotiation_currency, ord.Order_Insert_Breakdown, ord.Feature_Type)								AS SZUPRC,
				LEFT(dbo.[fn_RemoveSpecialChars](ord.Text_Sample), 23)																		AS SZVR02,
				ord.urn_number																												AS SZEDOC
			FROM Orders Ord
				INNER JOIN (SELECT DISTINCT Jde_Account_Number, Q1TXA1, Q1TRAR, Q1EXR1, ShipTo FROM Customers WHERE Stage > 3) as qryCustomers ON 
					qryCustomers.Jde_Account_number = ord.Account_No
				WHERE ord.Stage = 1 			
				
		UPDATE F47
		SET SZTXA1 = 'GBOUTSIDE'
		FROM F47012 f47
		INNER JOIN Orders o ON f47.OrdersId = o.id
		WHERE o.Stage = 1 AND SZTXA1 = 'OUTSIDE'
		
		UPDATE F47
		SET SZPTC = 'Prepaid'
		FROM F47012 f47
		INNER JOIN Orders o ON f47.OrdersId = o.id
		WHERE o.Stage = 1 AND SZDCTO LIKE '%3'
	
	
END
GO
