USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Orders]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_Orders]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Orders]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_Orders]
	-- Middleware always passed a batchnumber to SP's
	@BatchNumber as INT = 0
AS
BEGIN


BEGIN TRANSACTION

	-- Insert into Orders extracted Prepaid, Immediate and publication invoicing lines

	INSERT INTO Orders 
		(Stage, SourceToMiddleware, Account_No,Advertisers_Reference,Agency_Discount,Client_Name,Contact_forenames,Contact_Surname,Contact_Title,Create_Time,customer_name,batch_number,negotiation_currency,payment_setting_short,Payment_Type_short,Salesperson_Forenames,Salesperson_Surname,urn_number,Class_card,Colour_Card,EDZone_Card,EdZone_Card_short,Feature_Type,insert_date,Document_Type,Order_Insert_Breakdown,Order_Insert_Breakdown_cy,Page_Number,Size_Card,Style_Card,Summary_Card,Summary_Card_short,Text_Sample,Title_Card,Title_Card_short,Sequence_Number)
		SELECT 1 as Stage, 
		 GETDATE() AS SourceToMiddleware,
		 Account_No, 
         Advertisers_Reference, 
         Agency_Discount, 
         Client_Name, 
         Contact_forenames, 
         Contact_Surname, 
         Contact_Title, 
         Create_Time,
         customer_name, 
         batch_number, 
         negotiation_currency, 
         payment_setting_short, 
         Payment_Type_short, 
         Salesperson_Forenames, 
         Salesperson_Surname,
         urn_number, 
         Class_card, 
         Colour_Card, 
         EDZone_Card, 
         EdZone_Card_short, 
         Feature_Type, 
         insert_date, 
         dbo.[fn_OrderDocumentType](Account_No, Order_Insert_Breakdown, Order_Insert_Breakdown_cy, Payment_Type_short, payment_setting_short) AS Document_Type,
         Order_Insert_Breakdown, 
         Order_Insert_Breakdown_cy, 
         Page_Number, 
         Size_Card, 
         Style_Card, 
         Summary_Card, 
         Summary_Card_short, 
         Text_Sample,
         Title_Card, 
         Title_Card_short,
         Sequence_No as Sequence_Number
      FROM ASOP_Invoice_Extraction 
			WHERE ISNULL(Passed_To_MW,0) = 0

	  EXECUTE dbo.usp_SET_LineNumbers
			
      UPDATE ASOP_Invoice_Extraction  Set Passed_To_MW = 1 WHERE ISNULL(Passed_To_MW,0) = 0
 

COMMIT TRANSACTION

END
GO
