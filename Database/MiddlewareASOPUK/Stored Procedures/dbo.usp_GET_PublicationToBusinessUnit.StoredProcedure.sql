USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PublicationToBusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_PublicationToBusinessUnit]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PublicationToBusinessUnit]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st February 2011
-- Description:	Gets a publication to business unit mapping specified by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_PublicationToBusinessUnit] 
	-- Add the parameters for the stored procedure here
	@id AS INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT DISTINCT ptbu.id, ptbu.Business_Unit, ptbu.Publication AS Title_Short_Name, atc.Long_name AS Title_Card, vbu.[Description] AS Business_Unit_Description, ptbu.Collection_Manager_Client, ptbu.Collection_Manager_Agent, ptbu.Monthly_Weekly, ptbu.Tax_Company_Code, ptbu.Sales_admin_name, ptbu.Sales_admin_tel, ptbu.Sales_admin_fax, ptbu.Sales_admin_email
	FROM JDE_Publication_to_Business_Unit AS ptbu
	INNER JOIN ASOP_Title_Card AS atc ON atc.Short_name = ptbu.Publication
	INNER JOIN MiddlewareJDE.dbo.vw_JdeBusinessUnits AS vbu ON ptbu.Business_Unit = vbu.BusinessUnit
	WHERE ptbu.ID = @id
END

GO
