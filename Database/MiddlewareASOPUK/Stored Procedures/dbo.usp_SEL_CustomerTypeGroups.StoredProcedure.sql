USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerTypeGroups]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_CustomerTypeGroups]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CustomerTypeGroups]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st January 2011
-- Description:	selects all asop customer type groups
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CustomerTypeGroups] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ASOP_Customer_Type_Group
END
GO
