USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_INS_F0101z2]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F0101z2]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F0101Z2 f
		INNER JOIN Customers c ON c.id = f.szedtn
		WHERE Stage = 1 AND Accepted = 1 AND ISNULL(JDE_Account_Number, 0) = 0

	INSERT INTO F0101Z2
	(CustomerId, SZAC05, SZAC08, SZADD1, SZADD2, SZADD3, SZADD4, SZADDZ, SZALKY, SZAR1, SZAR2, SZAT1, SZCM, SZCOUN, SZCTR,
	 SZCTY1, SZEDBT, SZEDLN, SZEDTN, SZEDUS, SZMLNM, SZPH1, SZPH2, SZTAX, SZTXCT, SZURRF, SZPHT1, SZPHT2, SZRMK, SZTNAC)
	SELECT 
		id																												AS CustomerId,
		LEFT(dbo.fn_AgencyCode(c.Payment_Setting),2)																	AS SZAC05,
		LEFT(dbo.fn_CustomerCategory(c.Describes_Agency, c.Describes_House_Customer),2)	                                AS SZAC08,
		LEFT(dbo.fn_RemoveSpecialChars(dbo.fn_Address1(c.Is_Company, c.Address1)), 40)									AS SZADD1,
		LEFT(dbo.fn_RemoveSpecialChars(dbo.fn_Address2(c.Is_Company, c.Address1, c.Address2)), 40)						AS SZADD2,
		LEFT(dbo.fn_Address3(c.Is_Company, c.Address2, c.Address3), 40)													AS SZADD3,
		LEFT(dbo.fn_Address4(c.Is_Company, c.Address3,c.Address4), 40)													AS SZADD4,
		LEFT(c.Postcode, 10)																							AS SZADDZ,
		LEFT(dbo.fn_LongAddressNumber(c.Urn_Number), 20)																AS SZALKY,			
		LEFT(dbo.fn_DefaultTelephoneCode(c.Telephone_Number),3)															AS SZAR1,
		LEFT(dbo.fn_DefaultFaxCode(c.Fax_Number), 3)																	AS SZAR2,																
		LEFT(dbo.fn_SearchType(), 1)																					AS SZAT1,
		LEFT(dbo.fn_CreditMessage(c.Payment_Setting), 1)																AS SZCM,
		LEFT(dbo.fn_County(c.County, c.Country_Code), 25)																AS SZCOUN,
		LEFT(c.Country_Code, 4)																							AS SZCTR,
		LEFT(c.Town, 25)																								AS SZCTY1,
		c.Batch_Number																									AS SZEDBT,
		LEFT(dbo.fn_LineNumber(), 1)																					AS SZEDLN,
		id																												AS SZEDTN,
		LEFT(dbo.fn_MWUserName(), 4)																					AS SZEDUS,
		--LEFT(dbo.fn_CompanyNameFormat(c.Is_Company, c.Company_Name), 40)												AS SZMLNM,
		LEFT(dbo.fn_RemoveSpecialChars(dbo.fn_CompanyNameFormat(c.Is_Company, c.Company_Name)),40)						AS SZMLNM,
		LEFT(dbo.fn_FormatedTelephoneNumber(c.Telephone_Number, c.Telephone_Prefix, c.Telephone_Std), 40)				AS SZPH1,
		LEFT(dbo.fn_FormatedFaxNumber(c.Fax_Number, c.Fax_Prefix, c.Fax_Std), 40)										AS SZPH2,
		LEFT(ISNULL(c.Vat_Reg_No,''), 30)																				AS SZTAX,
		LEFT(ISNULL(c.Tax_Exempt_Number,''), 20)																		AS SZTXCT,
		c.Urn_Number																									AS SZURRF,
		LEFT(dbo.fn_DefaultTelephoneCode(c.Telephone_Number),10)														AS SZPHT1,
		LEFT(dbo.fn_DefaultFaxCode(c.Fax_Number),10)																	AS SZPHT2,
		LEFT(c.Email_Address, 30)																						AS SZRMK,
		LEFT(dbo.fn_TransactionSetting(),2)																				AS SZTNAC
	FROM Customers c
	WHERE Stage = 1 AND Accepted = 1 AND ISNULL(JDE_Account_Number, 0) = 0
	
		
END

GO
