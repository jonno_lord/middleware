USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_PaymentType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_PaymentType]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_PaymentType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,B.M.Patterson>
-- Create date: <Create Date,,01/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_PaymentType]
	-- Add the parameters for the stored procedure here
	@Payment_Type_ID AS int,
	@Name AS VARCHAR(20),
	@Payment_Type AS int,
	@Payment_setting AS int,
	@Document_Type AS int,
	@Credit_Document_Type AS int,
	@Invoice_Type AS int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [MiddlewareASOPUK].[dbo].[ASOP_Payment_Types]
	SET  Name = @Name
		,Payment_Type = @Payment_Type
		,Payment_setting = @Payment_setting
		,Document_Type = @Document_Type
		,Credit_Document_Type = @Credit_Document_Type
		,Invoice_Type = @Invoice_Type
	WHERE Payment_Type_ID =@Payment_Type_ID
		
	
END
GO
