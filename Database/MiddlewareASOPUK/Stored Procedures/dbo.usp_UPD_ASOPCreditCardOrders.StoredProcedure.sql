USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ASOPCreditCardOrders]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_ASOPCreditCardOrders]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ASOPCreditCardOrders]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jonno Lord
-- Create date: 13/09/11
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_ASOPCreditCardOrders]

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE ACCO
	SET Document_Type = VNDCT, Document_Number = VNEDTN
	FROM ASOP_CREDIT_CARD_ORDERS ACCO
	INNER JOIN F0911Z1 F9 ON (Business_Unit = LTRIM(VNMCU) AND Object_Account = VNOBJ AND Order_URN = VNODOC 
								AND Currency = VNCRCD AND SFX = VNSFX AND ABS(Output_net_sterling) = ABS(VNAA))
	WHERE Document_Type IS NULL
	AND MW_to_MW_Out IS NULL
	AND Batch_Selected = 1
	
END


 

	

GO
