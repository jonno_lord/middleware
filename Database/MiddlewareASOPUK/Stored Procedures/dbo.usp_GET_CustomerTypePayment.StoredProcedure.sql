USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerTypePayment]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_CustomerTypePayment]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerTypePayment]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 7th February 2011
-- Description:	Gets values from asop customer type payment and payment types, by match on group_code, payment_type and payment_setting
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_CustomerTypePayment] 
	-- Add the parameters for the stored procedure here
	@GroupCode AS VARCHAR(50),
	@PaymentType AS VARCHAR(50),
	@PaymentSetting AS VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM 
      (SELECT apt.*, actg.Group_Code, actg.Group_Description 
      FROM asop_Payment_types apt, asop_Customer_Type_Group actg) AS apt
      LEFT OUTER JOIN asop_Customer_Type_payment actp ON actp.Group_Code = apt.Group_Code 
      AND actp.payment_type = apt.Payment_Type AND actp.Payment_Setting = apt.Payment_Setting
      WHERE apt.Group_Code = @GroupCode AND apt.Payment_Type = @PaymentType AND apt.Payment_setting = @PaymentSetting
END
GO
