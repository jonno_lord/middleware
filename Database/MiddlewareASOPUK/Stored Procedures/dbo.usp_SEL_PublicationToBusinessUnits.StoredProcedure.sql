USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_PublicationToBusinessUnits]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_PublicationToBusinessUnits]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_PublicationToBusinessUnits]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st February 2011
-- Description:	Selects publication to business units as well as joining to retrieve title card and business unit names and decriptions
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_PublicationToBusinessUnits] 
	-- Add the parameters for the stored procedure here
	
	@publication AS VARCHAR(4) = '',
	@titleCard AS VARCHAR(50) = '',
	@businessUnit AS VARCHAR(4) = '',
	@businessUnitDescription AS VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT jpbu.id, jpbu.Business_Unit, jpbu.Publication AS Title_Short_Name, atc.Long_name AS Title_Card, vjde.[Description] AS Business_Unit_Description
	FROM JDE_Publication_to_Business_Unit AS jpbu
	INNER JOIN ASOP_Title_Card AS atc ON atc.Short_name = jpbu.Publication
	INNER JOIN MiddlewareJDE.dbo.vw_JdeBusinessUnits AS vjde ON jpbu.Business_Unit =  vjde.BusinessUnit
	WHERE (jpbu.Publication = @publication OR jpbu.Publication LIKE @publication OR @publication = '')
	AND (atc.Long_Name = @titleCard OR atc.Long_name LIKE @titleCard  OR @titleCard = '')
	AND (jpbu.Business_Unit = @businessUnit  OR @businessUnit = '')
	AND (vjde.[Description] = @businessUnitDescription OR vjde.[Description] LIKE @businessUnitDescription OR @businessUnitDescription = '')
	ORDER BY Title_Short_Name

END
GO
