USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_TaxCompany]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_DEL_TaxCompany]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_TaxCompany]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th July 2011
-- Description:	deletes tax company by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_TaxCompany] 
	-- Add the parameters for the stored procedure here
	@taxCompanyid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Tax_Company
	WHERE Tax_Company_ID = @taxCompanyid
END
GO
