USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_TaxCompany]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SAV_TaxCompany]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_TaxCompany]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th July 2011
-- Description:	Inserts or Updates tax company table entry
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_TaxCompany] 
	-- Add the parameters for the stored procedure here
	@id int,
	@taxCompanyCode AS NVARCHAR(4),
	@name AS NVARCHAR(50),
	@vatRegNo AS NVARCHAR(50),
	@address1 AS NVARCHAR(50),
	@address2 AS NVARCHAR(50),
	@address3 AS NVARCHAR(50) = '',
	@town AS NVARCHAR(40),
	@postcode AS NVARCHAR(10),
	@county AS NVARCHAR(20) = '',
	@country AS NVARCHAR(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		IF EXISTS (SELECT * FROM Tax_Company WHERE Tax_Company_ID = @id)
	BEGIN
		UPDATE Tax_Company SET Tax_Company_Code = @taxCompanyCode, Entity_name = @name, Entity_VAT_Reg_no = @vatRegNo, Address_1 = @address1, Address_2 = @address2, Address_3 = @address3, Town = @town, Postcode = @postcode, County = @county, Country = @country
		WHERE Tax_Company_ID = @id
	END
	ELSE
	BEGIN
		INSERT INTO Tax_Company(Tax_Company_Code, Entity_name, Entity_VAT_Reg_no, Address_1, Address_2, Address_3, Town, Postcode, County, Country)
		VALUES (@taxCompanyCode, @name, @vatRegNo, @address1, @address2, @address3, @town, @postcode, @county, @country)
	END
END
GO
