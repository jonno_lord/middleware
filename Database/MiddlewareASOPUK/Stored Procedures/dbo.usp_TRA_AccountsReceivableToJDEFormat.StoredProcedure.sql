USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJDEFormat]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJDEFormat]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION
	
		DELETE FROM AccountsReceivable
		DELETE FROM GeneralLedger
		
		--Update the ACCO table with the TransactionPOSID
		EXECUTE dbo.usp_UPD_TransactionPOSID	
		IF(@@ERROR <> 0) GOTO Failure	
		
		EXECUTE dbo.usp_UPD_InvoiceLineNumbers
		IF(@@ERROR <> 0) GOTO Failure	

		--Translate the data into JDE Format(F0911Z1)
		EXECUTE dbo.usp_INS_F0911z1 @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		--Translate the data into JDE Format(F03B11Z1)
		EXECUTE dbo.usp_INS_F03B11z1 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
		
		--Update tax code from JDE to Oracle format		
		EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoiceTaxCode @SystemName='MiddlewareASOPUK'
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoicesPaymentTerms @SystemName='MiddlewareASOPUK'
		IF(@@ERROR <> 0) GOTO Failure
		
		--Update the Document_Type and Document_Number on the ACCO table
		EXECUTE dbo.usp_UPD_ASOPCreditCardOrders
		IF(@@ERROR <> 0) GOTO Failure

		-- Mark details as processed
		UPDATE ASOP_Credit_Card_Orders SET MW_to_MW_Out = GETDATE(), Batch_Selected = 0 
		WHERE Batch_Selected = 1 
		AND MW_to_MW_Out IS NULL 
		AND Jde_Account_No IS NOT NULL
						
		UPDATE F03b11z1 SET Processed = 1 WHERE ISNULL(Processed,0) = 0
		UPDATE F0911z1 SET Processed = 1 WHERE ISNULL(Processed,0) = 0

	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F0911z1 WHERE VNEDBT = @BatchNumber

END

GO
