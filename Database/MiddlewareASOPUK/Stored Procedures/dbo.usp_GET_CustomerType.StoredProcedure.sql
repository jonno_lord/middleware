USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_CustomerType]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st January 2011
-- Description:	retrieves Customer Types specified by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_CustomerType] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ASOP_Customer_Type
	WHERE Customer_Type_ID = @id
END
GO
