USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_GetInvoicesFromPublicationId]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_SEL_GetInvoicesFromPublicationId]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_GetInvoicesFromPublicationId]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_GetInvoicesFromPublicationId]
	
	@PublicationId as int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SELECT 34 as id, '10/01/2010' as IssueDate, 0 as HasWarnings
	UNION
	SELECT 87, '11/01/2010',  0
	UNION
	SELECT 12, '12/10/2010',  1
END
GO
