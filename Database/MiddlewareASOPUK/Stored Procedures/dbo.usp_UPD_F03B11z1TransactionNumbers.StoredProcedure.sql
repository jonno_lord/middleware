USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03B11z1TransactionNumbers]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_UPD_F03B11z1TransactionNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F03B11z1TransactionNumbers]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UPD_F03B11z1TransactionNumbers] 
	-- Add the parameters for the stored procedure here
AS

BEGIN

	UPDATE F3
	SET VJEDTN = VNEDTN, VJDOC = VNDOC
	FROM F0911Z1 F9
	INNER JOIN F03B11Z1 F3 ON (F3.VJODOC = F9.VNODOC)
	WHERE F9.PROCESSED IS NULL

END

GO
