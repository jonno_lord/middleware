USE [MiddlewareASOPUK]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ActivityType]    Script Date: 09/04/2018 16:11:47 ******/
DROP PROCEDURE [dbo].[usp_GET_ActivityType]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ActivityType]    Script Date: 09/04/2018 16:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Brenda Patterson>
-- Create date: <Create Date,,02/02/2011>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ActivityType]
	-- Add the parameters for the stored procedure here
	@Id AS Integer
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT MiddlewareASOPUK.dbo.ASOP_Activity_Types.ActivityID
			,MiddlewareJDE.dbo.vw_Activities.Description
			,MiddlewareASOPUK.dbo.ASOP_Activity_Types.ReasonID
			,MiddlewareASOPUK.dbo.ASOP_Reason_Code.Long_name
	FROM MiddlewareASOPUK.dbo.ASOP_Activity_Types
	
    INNER JOIN MiddlewareJDE.dbo.vw_Activities on 
		MiddlewareASOPUK.dbo.ASOP_Activity_Types.ActivityID = MiddlewareJDE.dbo.vw_Activities.ActivityId
	INNER JOIN MiddlewareASOPUK.dbo.ASOP_Reason_Code on
		MiddlewareASOPUK.dbo.ASOP_Activity_Types.ReasonID = MiddlewareASOPUK.dbo.ASOP_Reason_Code.Reason_ID
	
	WHERE MiddlewareASOPUK.dbo.ASOP_Activity_Types.Id = @Id
	
END
GO
