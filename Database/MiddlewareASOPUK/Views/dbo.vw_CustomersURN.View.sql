USE [MiddlewareASOPUK]
GO
/****** Object:  View [dbo].[vw_CustomersURN]    Script Date: 09/04/2018 16:12:31 ******/
DROP VIEW [dbo].[vw_CustomersURN]
GO
/****** Object:  View [dbo].[vw_CustomersURN]    Script Date: 09/04/2018 16:12:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_CustomersURN] AS
SELECT Urn_Number AS URN, id FROM Customers
GO
