USE [MiddlewareASOPUK]
GO
/****** Object:  View [dbo].[VW_CustomersFull]    Script Date: 09/04/2018 16:12:31 ******/
DROP VIEW [dbo].[VW_CustomersFull]
GO
/****** Object:  View [dbo].[VW_CustomersFull]    Script Date: 09/04/2018 16:12:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_CustomersFull] AS
SELECT id,Customer_Id,Stage,Accepted,Rejected,SourceToMiddleware,MiddlewareScrutiny,AcceptedInScrutiny,RejectedInScrutiny,MiddlewaretoMiddlewareIn,MiddlewareToJDE,RejectedFromJDE,JDEToMiddleware,MiddlewareToMiddlewareOut,MiddlewareToSource,NumberOfScrutinyRulesBroken,Company_Name,Is_Company,Reference_Required_Setting,Urn_Number,Tax_Exempt_Number,
	CONVERT(VARCHAR(1000), Address) as Address,
	CONVERT(VARCHAR(1000),Address1) as Address1,
	CONVERT(VARCHAR(1000),Address2) as Address2,
	CONVERT(VARCHAR(1000),Address3) as Address3,
	CONVERT(VARCHAR(1000),Address4) as Address4,Town,Postcode,Country,County,Country_Code,Email_Address,Describes_Agency,Describes_House_Customer,Customer_Type,Telephone_Number,Telephone_Prefix,Telephone_Std,Fax_Number,Fax_Prefix,Fax_Std,Gross_Price_Sterling,Payment_Type,Vat_Reg_No,Operator_Name,Payment_Setting,Summary_Card,TitleCard_Name,Title_Card,Trading_Style,Batch_Number,Negotiation_Gross_Price,Negotiation_Currency,First_Insert_Date,JDE_Account_Number,Cat_Code_28,Cat_Code_29,Cat_Code_30,Credit_Limit,Credit_Controller_Notes,Available_Credit,Payment_Terms,Account_Status_id,F55.Q1AC05,F55.Q1AC06,F55.Q1ACL,F55.Q1AMCR,F55.Q1AN81,F55.Q1CM,F55.Q1CMGR,F55.Q1EXR1,F55.Q1HOLD,F55.Q1LNGP,F55.Q1MLN1,F55.Q1TXA1,F55.Q1AC02,F55.Q1TRAR,F55.Q1PORQ FROM Customers cus
	INNER JOIN F550101 f55 ON f55.Q1URRF = cus.Urn_Number
GO
