USE [MiddlewareASOPUK]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Credit_Card_Orders'
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Credit_Card_Orders'
GO
/****** Object:  View [dbo].[vw_Credit_Card_Orders]    Script Date: 09/04/2018 16:12:31 ******/
DROP VIEW [dbo].[vw_Credit_Card_Orders]
GO
/****** Object:  View [dbo].[vw_Credit_Card_Orders]    Script Date: 09/04/2018 16:12:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Credit_Card_Orders]
AS
SELECT     acco.Credit_Card_Orders_Id, acco.Source_to_MW, acco.MW_to_MW_Conversion, acco.MW_to_MW_Out, ISNULL(gl.MiddlewareToJDE, acco.MW_to_JDE) 
                      AS Mw_to_Jde, acco.Order_Urn, acco.Line_Number, acco.Customer_Urn, acco.Customer_name, acco.Address, acco.Town, acco.County, acco.Postcode, acco.Country, 
                      acco.Country_Code, acco.Country_EC, acco.Placed_By, acco.Customer_Reference, acco.Receipt_Number, acco.Receipt_Date, acco.Publication, 
                      acco.Publication_Code, acco.Size, acco.Colour, acco.Summary, acco.Classification, acco.Order_Date, acco.Customer_Type, acco.VAT_Registration_No, 
                      acco.Charity_Number, acco.Payment_Method, acco.Currency, acco.Net_price, acco.Gross_price, acco.Net_price_sterling, acco.Gross_price_sterling, 
                      acco.Text_Sample, acco.Issue_date, acco.Subledger_Issue_date, acco.Original_Issue_Date, acco.Credit_Controller_name, acco.Credit_Controller_phone, 
                      acco.Credit_Controller_fax, acco.Credit_Controller_email, acco.Sent_To_JDE, acco.Batch_number, acco.JDE_Account_No, acco.Receipt_required, acco.Jersey, 
                      acco.Tax_Rate_Code, acco.Tax_Rate_Description, acco.Output_net_price, acco.Output_VAT_amount, acco.Output_gross_price, acco.Output_net_sterling, 
                      acco.Output_VAT_sterling, acco.Output_gross_sterling, acco.Business_Unit, acco.Object_account, acco.Subsidiary_account, acco.subledger, acco.Logo_Reference_ID,
                       acco.Produce_Copy, acco.Actual_Insert_Description, acco.Page_Number, acco.of_Page_Number, acco.Document_Type, acco.Document_Number, 
                      acco.Invalid_Tax_From_ASOP, acco.Hub_Company, acco.Old_VAT, acco.Batch_Selected
FROM         dbo.ASOP_Credit_Card_Orders AS acco LEFT OUTER JOIN
                      dbo.F0911z1 AS f9 ON f9.VNODOC = acco.Order_Urn AND f9.VNSFX = acco.Line_Number AND f9.VNVINV = acco.Batch_number LEFT OUTER JOIN
                      dbo.GeneralLedger AS gl ON gl.f0911z1id = f9.id

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[5] 2[36] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "acco"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f9"
            Begin Extent = 
               Top = 6
               Left = 289
               Bottom = 125
               Right = 469
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gl"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Credit_Card_Orders'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Credit_Card_Orders'
GO
