USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PurchaseOrderRequired]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_PurchaseOrderRequired]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PurchaseOrderRequired]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PurchaseOrderRequired](@PoRequired tinyint)
returns varchar(1)
as
begin
	DECLARE @ret AS VARCHAR(1)

	if @PoRequired = 1
		set @ret =  'Y'
	else
		set @ret = 'N'
	
	return @ret
end
GO
