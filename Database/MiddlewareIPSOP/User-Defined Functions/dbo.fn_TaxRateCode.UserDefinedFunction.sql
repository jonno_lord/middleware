USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxRateCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_TaxRateCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxRateCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_TaxRateCode]
(
	-- Add the parameters for the function here
	@CountryName AS VARCHAR(40), @VatNumber  AS VARCHAR(20), @TaxExemptNumber AS VARCHAR(30)
)
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @Rate AS VARCHAR(10)
	DECLARE @InEC AS BIT 
	
	SET @Rate = 'GBSTD'			-- Default vat code
	SET @InEC = 0

	-- check to see if country is in EC
	IF EXISTS(SELECT * FROM MiddlewareJDE.dbo.vw_ECVatGroups jevg 
					INNER JOIN CountryCodeMappings ccm ON (ccm.Country_Code = jevg.CountryCode)
					WHERE ccm.Country_Code = @CountryName)
		SET @InEC = 1

	IF(ISNULL(@VatNumber,'') <> '' AND @CountryName <> 'GB' AND @InEC = 1)
		SET @Rate = 'GBREVCHRGE'
	ELSE IF (ISNULL(@TaxExemptNumber,'') <> '') 
		SET @Rate = 'GBSTD'
	ELSE IF (@CountryName <> 'GB' AND @InEC = 0)
		SET @Rate = 'GBOUTSIDE'

	RETURN @Rate

END

GO
