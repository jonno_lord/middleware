USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Col]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_Col]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Col]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_Col](@ColValue as MONEY)
returns MONEY
as
begin

	RETURN @ColValue * 100
	
end
GO
