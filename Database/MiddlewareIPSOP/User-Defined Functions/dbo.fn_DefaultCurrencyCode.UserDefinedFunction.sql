USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultCurrencyCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DefaultCurrencyCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultCurrencyCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DefaultCurrencyCode]()
returns varchar(3)
as
begin
	return 'GBP'
end
GO
