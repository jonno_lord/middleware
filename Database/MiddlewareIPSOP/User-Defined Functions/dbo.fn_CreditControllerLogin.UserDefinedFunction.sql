USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditControllerLogin]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CreditControllerLogin]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditControllerLogin]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CreditControllerLogin](@describesAgency as bit, @titleCard as Varchar(4))
returns varchar(50)
as
begin
	
	
	DECLARE @ret AS VARCHAR(50)

	

	-- look up the credit team
	IF(@describesAgency <> 1)
	BEGIN

		SELECT @ret = Collection_Manager_Client FROM
			JDE_Publication_To_Business_Unit 
			WHERE Publication = @titleCard
	
	END
	ELSE
	BEGIN
	
		SELECT @ret = Collection_Manager_Agent FROM
			JDE_Publication_To_Business_Unit 
			WHERE Publication = @titleCard
	END

	IF (@ret IS NULL) SET @ret = ''

	RETURN @ret
	
END

GO
