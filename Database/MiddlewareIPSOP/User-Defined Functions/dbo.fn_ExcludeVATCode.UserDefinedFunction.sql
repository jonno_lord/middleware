USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ExcludeVATCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_ExcludeVATCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ExcludeVATCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ExcludeVATCode](@ExclVat as tinyint)
returns varchar(1)
as
begin
	
	DECLARE @ret AS VARCHAR(1)
	
	IF(LTRIM(@ExclVat) <> 0)
		SET @ret = 'V'
	ELSE
		SET @ret = ''
	
	RETURN @ret
	
END
GO
