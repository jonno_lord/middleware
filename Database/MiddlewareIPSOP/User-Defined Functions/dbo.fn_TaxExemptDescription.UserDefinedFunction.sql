USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptDescription]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_TaxExemptDescription]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptDescription]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_TaxExemptDescription](@TaxExemptCertificateNumber as varchar(20), @searchType as varchar(10))
returns varchar(10)
as
begin
	
	DECLARE @ret AS VARCHAR(10)
	IF ((@searchType = 'CE' OR @searchType = 'CS') AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) <> '')
		SET @ret = 'GBEXMEPT'

	IF ((@searchType = 'CE' OR @searchType = 'CS') AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) = '')
		SET @ret = 'GBSTD'

	IF (@searchType = 'CH' AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) <> '') 
		SET @ret = 'NLEXEMPT'

	IF (@searchType = 'CH' AND LTRIM(ISNULL(@TaxExemptCertificateNumber, '')) = '') 
		SET @ret = 'NLSTD'

	IF (@searchType = 'CI') 
		SET @ret = 'OUTSIDE'



	
	RETURN @ret
	
END
GO
