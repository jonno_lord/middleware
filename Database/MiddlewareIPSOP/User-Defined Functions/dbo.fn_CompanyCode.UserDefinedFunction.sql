USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CompanyCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_CompanyCode]
(
	-- Add the parameters for the function here
	@BusinessUnit AS VARCHAR(10)
)
RETURNS VARCHAR(5)
AS
BEGIN
	DECLARE @Company AS VARCHAR(5)

	SELECT @Company = Company from MiddlewareJDE.dbo.vw_JdeBusinessUnits WHERE BusinessUnit = @BusinessUnit

	RETURN @Company
END
GO
