USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerPaymentTerms]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CustomerPaymentTerms]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerPaymentTerms]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CustomerPaymentTerms]()
returns varchar(3)
as
begin

	DECLARE @Ret AS VARCHAR(3)
	SET @Ret = 'N30'


	RETURN @Ret


end
GO
