USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_LongAddressNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_LongAddressNumber](@CompanyTextID as varchar(20))
returns varchar(20)
as
begin
	
	declare @ret as VARCHAR(20)
	
	SELECT @ret = LTRIM(RTRIM(@CompanyTextID)) + 'B'
	
	RETURN @ret
	
END
GO
