USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DunningCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DunningCode]()
returns varchar(2)
as
begin

	DECLARE @ret AS VARCHAR(2)
	
	SET @ret = '16'
	RETURN @ret
	
END
GO
