USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CreditLimit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CreditLimit]()
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	
	SET @ret = '10000'
				  
	RETURN @ret
	
END
GO
