USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_QuantityOrder]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_QuantityOrder]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_QuantityOrder]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_QuantityOrder](@SubNo AS VARCHAR(15), @BatchNumber AS INT, @Status AS VARCHAR(15))
RETURNS FLOAT
AS 
BEGIN
	
	DECLARE @ret AS FLOAT
	DECLARE @Total AS FLOAT
	
	SELECT @Total = SUM(IPSOP_Invoice_Value) FROM Orders 
	WHERE Sub_No = @SubNo
	AND Batch_No = @BatchNumber
	AND Status = @Status
	AND Stage = 1
		
	IF(@Total < 0)
		SET @ret = -1
	ELSE
		SET @ret = 1

	RETURN @ret
	
END
GO
