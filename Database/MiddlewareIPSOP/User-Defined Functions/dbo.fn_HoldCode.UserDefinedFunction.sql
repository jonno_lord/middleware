USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HoldCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_HoldCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HoldCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_HoldCode]()
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	SET @ret = ''
	RETURN @ret
	
END
GO
