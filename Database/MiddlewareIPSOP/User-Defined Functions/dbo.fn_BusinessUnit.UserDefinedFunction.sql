USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnit]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_BusinessUnit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnit]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_BusinessUnit](@JdeBusinessUnit AS VARCHAR(12))
returns char(12)
as
begin

	SET @JdeBusinessUnit = LTRIM(RTRIM(ISNULL(@JdeBusinessUnit,'')))

	return @JdeBusinessUnit
end
GO
