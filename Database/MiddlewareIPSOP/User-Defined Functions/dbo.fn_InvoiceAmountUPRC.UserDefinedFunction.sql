USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmountUPRC]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_InvoiceAmountUPRC]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmountUPRC]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_InvoiceAmountUPRC](@SearchType AS VARCHAR(3), @CurrencyUnit as VARCHAR(5), @InvoiceAmount as MONEY)
returns money
as
begin

	DECLARE @ReturnedAmount as MONEY

	SELECT @ReturnedAmount = @InvoiceAmount * CONVERT(MONEY, (POWER(10, CVCDEC))) FROM MiddlewareJDE.dbo.F0013
		WHERE CVCRCD = @CurrencyUnit 

	IF((@SearchType = 'CE' OR @SearchType='CS') AND @CurrencyUnit <> 'GBP')
		SET @ReturnedAmount = 0

	IF(@SearchType = 'CH' AND @CurrencyUnit <> 'EUR')
		SET @ReturnedAmount = 0

	IF(@SearchType = 'CI' AND @CurrencyUnit <> 'USD')
		SET @ReturnedAmount = 0
	

	return @ReturnedAmount
end
GO
