USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode1]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CatCode1]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode1]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CatCode1](@BusinessUnit as Varchar(20))
returns varchar(1)
as
begin
		
	DECLARE @ret AS VARCHAR(1)

	BEGIN

		SELECT @ret = CreditControllerTeam from BusinessUnitMappings bum
			INNER JOIN MiddlewareJDE.dbo.vw_CreditControllers vcc
			ON (bum.Collection_Manager_Client = vcc.LogonName)
			WHERE  LTRIM(RTRIM(bum.Business_Unit)) =  LTRIM(RTRIM(@BusinessUnit))
	END

	-- if we could not find this default - put it to X to mark the failure
	-- for JDE to resolve by hand
	IF (@ret IS NULL) SET @ret = 'X'

	RETURN @ret
	
END
GO
