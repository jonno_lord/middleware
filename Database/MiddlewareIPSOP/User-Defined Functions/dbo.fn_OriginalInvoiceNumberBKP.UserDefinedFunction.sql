USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceNumberBKP]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_OriginalInvoiceNumberBKP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceNumberBKP]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OriginalInvoiceNumberBKP]
(
@DocumentType AS VARCHAR(2), 
@SubNo AS VARCHAR(15),
@InvoiceValue FLOAT,
@InvoiceDescription AS VARCHAR(40),
@Status AS VARCHAR(15),
@Price AS FLOAT
)
RETURNS FLOAT
AS
BEGIN

	DECLARE @ret AS FLOAT
	
	IF (@Status = 'Credit')
		BEGIN
			SELECT @ret = ord.JDE_Invoice_Number FROM Orders ord
			WHERE ord.Document_Type = REPLACE(@DocumentType,'Q','N')
			AND ord.Sub_No = @SubNo
			AND ord.Invoice_Description = @InvoiceDescription
			AND ord.IPSOP_Invoice_Value = @InvoiceValue * -1
			AND ord.Price = @Price
		END
		
	RETURN @ret	
	
END



GO
