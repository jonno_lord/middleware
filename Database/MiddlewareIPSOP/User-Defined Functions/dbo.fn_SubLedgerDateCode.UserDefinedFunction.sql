USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SubLedgerDateCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_SubLedgerDateCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SubLedgerDateCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[fn_SubLedgerDateCode](
	@issue_date as DATETIME
)
returns varchar(1)
as
begin

	DECLARE @SubLedgerDateCode as VARCHAR(1)

	SET @SubLedgerDateCode = ''
	IF (ISNULL(@issue_date, '1900-01-01 00:00:00') <> '1900-01-01 00:00:00')
		SET @SubLedgerDateCode = 'S'

	RETURN @SubLedgerDateCode

end
GO
