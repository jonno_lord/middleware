USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnitSecurity]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_BusinessUnitSecurity]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnitSecurity]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_BusinessUnitSecurity](@searchType as varchar(10))
returns varchar(12)
as
begin
	
	declare @ret as VARCHAR(12)
	SELECT @ret = CASE 
				  WHEN @searchType = 'CE' THEN '10199'
				  WHEN @searchType = 'CH' THEN '50199'
				  WHEN @searchType = 'CI' THEN '52399'
				  WHEN @searchType = 'CS' THEN '13209'
				  ELSE 'x'+ @searchType END
	
	RETURN @ret
	
END
GO
