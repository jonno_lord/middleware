USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmount]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_InvoiceAmount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_InvoiceAmount]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_InvoiceAmount](@InvoiceAmount AS MONEY, @Sub_No AS VARCHAR(15), @BatchNumber AS INT, @Status AS VARCHAR(15))
RETURNS MONEY
AS 
BEGIN

	DECLARE @ReturnedAmount AS FLOAT
	DECLARE @Total AS FLOAT
	
	SELECT @Total = SUM(IPSOP_Invoice_Value) FROM Orders
	WHERE Sub_No = @Sub_No
	AND Batch_No = @BatchNumber
	AND Status = @Status
	AND Stage = 1
	
	
	IF(@Total < 0)
		SET @ReturnedAmount = @InvoiceAmount * -1
	ELSE 
		SET @ReturnedAmount = @InvoiceAmount
		
	RETURN @ReturnedAmount

END
GO
