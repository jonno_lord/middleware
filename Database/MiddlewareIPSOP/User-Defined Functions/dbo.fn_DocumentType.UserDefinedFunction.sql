USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DocumentType]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DocumentType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DocumentType]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_DocumentType]
(
	-- Add the parameters for the function here
	@Type AS VARCHAR(30), @Status AS VARCHAR(30), @SubNo AS VARCHAR(15), @BatchNumber AS VARCHAR(10)
)
RETURNS VARCHAR(2)
AS
BEGIN

	DECLARE @DocumentType AS VARCHAR(2)
	DECLARE @Total AS FLOAT
	
	SELECT @Total = SUM(IPSOP_Invoice_Value) FROM Orders o
	WHERE o.Stage = 1 
	AND o.Sub_No = @SubNo 
	AND o.Batch_No = @BatchNumber
	AND o.Status = @Status
	
	SELECT @DocumentType =  CASE WHEN (@Status = 'Credit' OR @Total < 0) THEN 'Q1'
							WHEN @Type = 'In-House' AND (@Status = 'Credit' OR @Total < 0) THEN 'Q5'
							WHEN @Type = 'In-House' AND @Status <> 'Credit' THEN 'N5'
							WHEN @Type = 'Prepayment' AND (@Status = 'Credit' OR @Total < 0) THEN 'Q3'
							WHEN @Type = 'Prepayment' AND @Status <> 'Credit' THEN 'N3'
							ELSE 'N1'
							END
	RETURN @DocumentType


END
GO
