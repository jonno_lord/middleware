USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTerms]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_PaymentTerms]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTerms]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PaymentTerms](@SearchType AS VARCHAR(8), @PaymentTerms as VARCHAR(10))
returns char(10)
as
begin
	
	DECLARE @ret AS VARCHAR(10)
	SET @ret = @PaymentTerms
	IF(@SearchType = 'CH' AND @PaymentTerms <> 'R') SET @ret = 'N30'
	
	RETURN @ret
	
end
GO
