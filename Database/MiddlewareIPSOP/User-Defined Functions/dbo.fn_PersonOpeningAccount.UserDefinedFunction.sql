USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PersonOpeningAccount]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_PersonOpeningAccount]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PersonOpeningAccount]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PersonOpeningAccount]()
returns varchar(5)
as
begin
	return 'IPSOP'
end
GO
