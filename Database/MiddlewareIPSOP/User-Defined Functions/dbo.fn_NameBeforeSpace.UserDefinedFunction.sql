USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_NameBeforeSpace]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_NameBeforeSpace]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_NameBeforeSpace]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_NameBeforeSpace](@detail as varchar(4096))
returns varchar(4096)
as
begin

	declare @return as varchar(4096)
	set @detail = LTRIM(RTRIM(@detail))
	
	declare @startinx as int
	set @startinx = CHARINDEX(' ', @detail)
	
	set @return = @detail
	
	if(@startinx > 3)
	begin
		set @return = SUBSTRING(@detail, 1, @startinx)
		return @return
	end
	
	if (LEN(@return) > 20) set @return = LEFT(@return,20)

	return @return
	
END
GO
