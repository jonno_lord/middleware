USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_AgencyCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_AgencyCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_AgencyCode]()
returns varchar(2)
as
begin
	return 'BR'
end


GO
