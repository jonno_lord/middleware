USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditControllerInitials]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CreditControllerInitials]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditControllerInitials]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CreditControllerInitials](@BusinessUnit as Varchar(20))
returns varchar(10)
as
begin
	
	
	DECLARE @ret AS VARCHAR(10)
	
	BEGIN 

		SELECT @ret = Collection_Manager_Client from BusinessUnitMappings bum
			INNER JOIN MiddlewareJDE.dbo.vw_CreditControllers vcc
			ON (bum.Collection_Manager_Client = vcc.LogonName)
			WHERE  LTRIM(RTRIM(bum.Business_Unit)) =  LTRIM(RTRIM(@BusinessUnit))
	END

	-- if we could not find this default - put it to X to mark the failure
	-- for JDE to resolve by hand
	IF (@ret IS NULL) SET @ret = 'X'

	RETURN @ret
	
END

GO
