USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MWUserName]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_MWUserName]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MWUserName]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_MWUserName]()
returns char(8)
as
begin
	return 'IPSOP'
end
GO
