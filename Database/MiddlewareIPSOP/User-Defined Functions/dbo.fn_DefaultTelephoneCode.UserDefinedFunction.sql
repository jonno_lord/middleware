USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultTelephoneCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DefaultTelephoneCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultTelephoneCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DefaultTelephoneCode](@phone as varchar(100))
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	
	SET @ret = ''
	
	IF(ltrim(@phone) <> '') SET @ret = 'TEL'
				  
	RETURN @ret
	
END
GO
