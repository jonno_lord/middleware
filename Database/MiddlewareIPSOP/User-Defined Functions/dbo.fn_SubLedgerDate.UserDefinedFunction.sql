USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SubLedgerDate]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_SubLedgerDate]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SubLedgerDate]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  function [dbo].[fn_SubLedgerDate](
	@issue_date as DATETIME
)
returns varchar(8)
as
begin

	DECLARE @SubLedgerDate as VARCHAR(8)

	SET @SubLedgerDate = ''
	IF (ISNULL(@issue_date, '1900-01-01 00:00:00') <> '1900-01-01 00:00:00')
		--SET @SubLedgerDate = RTRIM(REPLACE(CONVERT(varchar(6), @issue_date, 112), '.', '') )
		SET @SubLedgerDate = CONVERT(CHAR(6), @issue_date, 112)

	RETURN @SubLedgerDate

end
GO
