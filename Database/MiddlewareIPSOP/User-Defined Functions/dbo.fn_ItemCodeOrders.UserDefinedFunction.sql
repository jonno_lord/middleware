USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ItemCodeOrders]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_ItemCodeOrders]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ItemCodeOrders]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_ItemCodeOrders](@JDEProductCode AS VARCHAR(25), @ItemCodeType AS VARCHAR(1), @AccountNo AS FLOAT)
RETURNS VARCHAR(26)
AS
BEGIN

	DECLARE @CatCode28 AS VARCHAR(10)
	DECLARE @ret AS VARCHAR(26)
	
	SELECT @CatCode28 = cat_code_28 FROM Customers
	WHERE Jde_Account_Number = @AccountNo
	
	IF (@CatCode28 = 'I')
		SET @ret = RTRIM(UPPER(@JDEProductCode)) + UPPER(@ItemCodeType) + 'IC'
	ELSE
		SET @ret = RTRIM(UPPER(@JDEProductCode)) + UPPER(@ItemCodeType)
		
	RETURN @ret
END
GO
