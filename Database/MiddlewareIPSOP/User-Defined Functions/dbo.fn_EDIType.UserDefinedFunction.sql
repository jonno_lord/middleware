USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EDIType]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_EDIType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EDIType]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_EDIType]()
returns varchar(3)
as
begin
	return '850'
end
GO
