USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningLetters]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DunningLetters]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningLetters]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DunningLetters]
()
RETURNS VARCHAR(2)
AS
BEGIN

	DECLARE @ret AS VARCHAR(2)
	SET @ret = 'TS'
	
	RETURN @ret

END
GO
