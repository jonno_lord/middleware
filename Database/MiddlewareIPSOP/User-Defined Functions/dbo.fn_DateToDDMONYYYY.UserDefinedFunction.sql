USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateToDDMONYYYY]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DateToDDMONYYYY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateToDDMONYYYY]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_DateToDDMONYYYY]
(
        @DATE DATETIME
) RETURNS int
AS
BEGIN
        RETURN REPLACE(CONVERT(VARCHAR,@Date,106),' ','-')
END
GO
