USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateCheckSPR5]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DateCheckSPR5]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DateCheckSPR5]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_DateCheckSPR5](@datecheck as int) RETURNS varchar(3) AS 
BEGIN 
	
	
	DECLARE @ret AS VARCHAR(3)
	
	SET @ret = ''
	
	IF(@datecheck = 0) SET @ret = 'SUB'

	return @ret
END
GO
