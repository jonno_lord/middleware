USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BatchNumber]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_BatchNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BatchNumber]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_BatchNumber]()
returns varchar(10)
as
begin
	DECLARE @ret AS VARCHAR(10)
	SET @ret = '9999999'

	Select @ret = CONVERT(varchar(10), GetDate(), 112) 
	return @ret

end
GO
