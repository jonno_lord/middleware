USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultBaseCurrency]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_DefaultBaseCurrency]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DefaultBaseCurrency]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_DefaultBaseCurrency]()
returns varchar(10)
as
begin
	
	declare @ret as VARCHAR(10)
	
	SET @ret = 'GBP'	
			  
	RETURN @ret
	
END
GO
