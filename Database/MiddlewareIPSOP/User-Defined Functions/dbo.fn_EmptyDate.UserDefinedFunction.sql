USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EmptyDate]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_EmptyDate]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EmptyDate]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_EmptyDate]()
returns int
as
begin
	return 0
end
GO
