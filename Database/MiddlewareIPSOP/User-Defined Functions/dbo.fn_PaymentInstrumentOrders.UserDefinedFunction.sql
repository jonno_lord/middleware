USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentInstrumentOrders]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_PaymentInstrumentOrders]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentInstrumentOrders]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_PaymentInstrumentOrders](@PaymentType as varchar(20))
returns varchar(10)
as
begin

	DECLARE @ret AS VARCHAR(1)
	
	SET @ret = NULL
	
	IF(@PaymentType = 'Direct Debit') SET @ret = 'D'

	IF(@PaymentType = 'Standing Order') SET @ret = 'S'
	
	IF(@PaymentType = 'Deferred Invoice') SET @ret = 'Q'
	
	RETURN @ret
	
END

GO
