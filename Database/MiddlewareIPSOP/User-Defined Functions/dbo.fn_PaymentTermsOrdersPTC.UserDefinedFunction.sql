USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsOrdersPTC]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_PaymentTermsOrdersPTC]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsOrdersPTC]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_PaymentTermsOrdersPTC](@JDEPaymentTermsCode as varchar(3), --U10/NULL
											 @JDEPaymentType as varchar(10), --Direct Debit/Invoice
											 @Type AS VARCHAR(10), --NULL
											 @Status AS VARCHAR(10), --Live/Not started
											 @JdeNumber AS VARCHAR(10)) --
RETURNS VARCHAR(10)
AS
BEGIN

	--DECLARE @ret as VARCHAR(10)
	--SET @ret = 'N30'
	
	--SET @JDEPaymentTermsCode = ISNULL(@JDEPaymentTermsCode, '')
	
	--IF (LTRIM(ISNULL(@JDEPaymentTermsCode, '')) <> '')
	--	SET @ret = @JDEPaymentTermsCode 
		
	--RETURN @ret
	
	DECLARE @ret as VARCHAR(10)
	DECLARE @TaxCode AS VARCHAR(10)
	SET @JDEPaymentTermsCode = ISNULL(@JDEPaymentTermsCode, '')
	
	SELECT @TaxCode = TXA1 FROM Customers WHERE Jde_Account_Number = @JdeNumber 
	
	-- If there is a payment, pre-append the document type to it
	IF (LTRIM(ISNULL(@JDEPaymentTermsCode, '')) <> '')
		SET @ret = @JDEPaymentTermsCode 
	
	IF (LTRIM(ISNULL(@JDEPaymentType, '')) = 'Invoice')
		SET @ret =  ''
		
	IF (@TaxCode <> 'GBSTD' AND @JDEPaymentType = 'Direct Debit' AND LEN(@JDEPaymentTermsCode) > 1)
		SET @ret = 'V' + SUBSTRING(@JDEPaymentTermsCode, 2, 20) 

	IF (@TaxCode <> 'GBSTD' AND @JDEPaymentType = 'Standing Order' AND LEN(@JDEPaymentTermsCode) > 1)
		SET @ret = 'T' + SUBSTRING(@JDEPaymentTermsCode, 2, 20) 

	IF (@TaxCode <> 'GBSTD' AND @JDEPaymentType = 'Deferred Invoice' AND LEN(@JDEPaymentTermsCode) > 1)
		SET @ret = 'X' + SUBSTRING(@JDEPaymentTermsCode, 2, 20) 	
	
	/* This doesn't look correct
	IF(@Status = 'Credit' AND @JDEPaymentType = 'Invoice')
		SET @ret = 'QCRD'
	*/
	
	IF(@JDEPaymentTermsCode = 'REN')
		SET @ret = 'REN'
	
	RETURN @ret
	
END
GO
