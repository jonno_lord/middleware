USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExplanationCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_TaxExplanationCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExplanationCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_TaxExplanationCode]
(
	-- Add the parameters for the function here
	@CountryName AS VARCHAR(40), @VatNumber  AS VARCHAR(20), @TaxExemptNumber AS VARCHAR(30)
)
RETURNS VARCHAR(1)
AS
BEGIN

	DECLARE @Code AS VARCHAR(1)
	DECLARE @InEC AS BIT 
	
	SET @Code = 'V'			-- Default vat code
	SET @InEC = 0

	-- check to see if country is in EC
	IF EXISTS(SELECT * FROM MiddlewareJDE.dbo.vw_ECVatGroups jevg 
					INNER JOIN CountryCodeMappings ccm ON (ccm.Country_Code = jevg.CountryCode)
					WHERE ccm.Country_Name = @CountryName)
		SET @InEC = 1
	
	IF(ISNULL(@VatNumber, '') <> '' AND @InEC = 1)
		SET @Code = 'E'
	
	IF(@CountryName <> 'GB' AND @InEC = 0)
		SET @Code = 'E'

	IF (ISNULL(@TaxExemptNumber,'') <> '')
		SET @Code = 'E'
	
	RETURN @Code
	
END
GO
