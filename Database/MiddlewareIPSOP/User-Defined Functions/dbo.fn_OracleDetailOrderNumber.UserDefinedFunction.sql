USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleDetailOrderNumber]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_OracleDetailOrderNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleDetailOrderNumber]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_OracleDetailOrderNumber]
(
 @Sub_No AS VARCHAR(8), @BatchNumber AS VARCHAR(10), @DocumentType AS VARCHAR(2)
)
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @Line_No AS INT
		
	SELECT @Line_No = MAX(o.Line_No) FROM Orders o
	WHERE o.Sub_No = @Sub_No
	AND o.Batch_No = @BatchNumber
	AND o.Document_Type = @DocumentType
	AND o.Stage = 1

    RETURN CONVERT(VARCHAR(10),@Sub_No) + '_' + CONVERT(VARCHAR(10),@Line_No)

END










GO
