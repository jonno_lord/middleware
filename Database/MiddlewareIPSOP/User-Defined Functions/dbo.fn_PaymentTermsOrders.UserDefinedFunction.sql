USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsOrders]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_PaymentTermsOrders]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentTermsOrders]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_PaymentTermsOrders](@JDEPaymentTermsCode as varchar(3),
											 @JDEPaymentType as varchar(10),
											 @Type AS VARCHAR(10),
											 @Status AS VARCHAR(10),
											 @JdeNumber AS VARCHAR(10))
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @ret as VARCHAR(10)
	DECLARE @DocumentType AS VARCHAR(2)
	DECLARE @TaxCode AS VARCHAR(10)
	
	SELECT @DocumentType = dbo.fn_DocumentType(@Type, @Status)
	
	SELECT @TaxCode = TXA1 FROM Customers WHERE Jde_Account_Number = @JdeNumber 
	
	-- If there is a payment, pre-append the document type to it
	IF (LTRIM(ISNULL(@JDEPaymentTermsCode, '')) <> '')
		SET @ret = LEFT(@DocumentType, 1) + @JDEPaymentTermsCode 
	
	IF (LTRIM(ISNULL(@JDEPaymentType, '')) = 'Invoice')
		SET @ret =  ''
		
	IF (@TaxCode <> 'GBSTD' AND @JDEPaymentType = 'Direct Debit' AND LEN(@JDEPaymentTermsCode) > 1)
		SET @ret = LEFT(@DocumentType, 1) + 'V' + SUBSTRING(@JDEPaymentTermsCode, 2, 20) 

	IF (@TaxCode <> 'GBSTD' AND @JDEPaymentType = 'Standing Order' AND LEN(@JDEPaymentTermsCode) > 1)
		SET @ret = LEFT(@DocumentType, 1) + 'T' + SUBSTRING(@JDEPaymentTermsCode, 2, 20) 

	IF (@TaxCode <> 'GBSTD' AND @JDEPaymentType = 'Deferred Invoice' AND LEN(@JDEPaymentTermsCode) > 1)
		SET @ret = LEFT(@DocumentType, 1) + 'X' + SUBSTRING(@JDEPaymentTermsCode, 2, 20) 	
	
	IF(@Status = 'Credit' AND @JDEPaymentType = 'Invoice')
		SET @ret = 'QCRD'
	
	IF(@JDEPaymentTermsCode = 'REN')
		SET @ret = 'REN'
	
	RETURN @ret
	
END
GO
