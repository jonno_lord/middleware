USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleOrderNumber]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_OracleOrderNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OracleOrderNumber]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[fn_OracleOrderNumber]
(
@Sub_No AS VARCHAR(15),
@Line_No AS INT
)
RETURNS VARCHAR(10)
AS
BEGIN
	
	RETURN CONVERT(VARCHAR(10),@Sub_No) + '_' + CONVERT(VARCHAR(10),@Line_No)

END
GO
