USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalLineNumberBKP]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_OriginalLineNumberBKP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalLineNumberBKP]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OriginalLineNumberBKP]
(
@DocumentType AS VARCHAR(2), 
@SubNo AS VARCHAR(15),
@InvoiceValue FLOAT,
@InvoiceDescription AS VARCHAR(40),
@LineNo AS INT,
@BatchNumber AS VARCHAR(10),
@Price AS FLOAT
)
RETURNS FLOAT
AS
BEGIN

	DECLARE @ret AS FLOAT
	DECLARE @Total AS FLOAT
			
	SELECT @Total = SUM(o.IPSOP_Invoice_Value) FROM Orders o
	WHERE o.Stage = 1
	AND o.Sub_No = @SubNo
	AND o.Batch_No = @BatchNumber
	
	IF(@Total < 0)
		BEGIN 
			IF(SUBSTRING(@DocumentType,1,1) = 'N')
			SET @DocumentType = REPLACE(@DocumentType,'N','Q')
		END
			
	IF(@Total >= 0)
		SET @ret = @LineNo * 1000
	ELSE
		SELECT @ret = ord.Line_No * 1000 FROM Orders ord
		WHERE ord.Document_Type = REPLACE(@DocumentType,'Q','N')
		AND ord.Sub_No = @SubNo
		AND ord.Invoice_Description = @InvoiceDescription
		AND ord.IPSOP_Invoice_Value = @InvoiceValue * -1
		AND ord.Price = @Price
	
	RETURN @ret	
	
END



GO
