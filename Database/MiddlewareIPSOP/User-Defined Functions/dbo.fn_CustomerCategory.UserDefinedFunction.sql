USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerCategory]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_CustomerCategory]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CustomerCategory]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CustomerCategory](@AgencyType as varchar(10), @CountryCode as Varchar(10))
returns varchar(2)
as
begin
	
	DECLARE @ret AS VARCHAR(2)
	SET @ret = 'XX'
	
	if(@AgencyType IS NULL) set @AgencyType = ''
	
	
	IF(@AgencyType = 'Agency' and @CountryCode = 'GB')
		SET @ret = 'GA'

	IF(@AgencyType <> 'Agency' and @CountryCode = 'GB')
		SET @ret = 'GD'

	IF(@AgencyType <> 'Agency' and @CountryCode <> 'GB')
		SET @ret = 'OC'
		
	IF(@AgencyType = 'Agency' and @CountryCode <> 'GB')
		SET @ret = 'OA'
	
	IF(@AgencyType = 'In-House')
		SET @ret = 'IC'

	RETURN @ret

END

GO
