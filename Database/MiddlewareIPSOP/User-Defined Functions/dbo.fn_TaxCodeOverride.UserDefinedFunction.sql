USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxCodeOverride]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_TaxCodeOverride]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxCodeOverride]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_TaxCodeOverride](@JdeAccountNumber as VARCHAR(10), @JdeBusinessUnit as varchar(12), @TaxCode as varchar(10))
returns char(12)
as
begin
	
	DECLARE @ret AS VARCHAR(10)
	SET @ret = @TaxCode

	DECLARE @CountryCode AS VARCHAR(20)
	SELECT @CountryCode = Country_Code FROM Customers WHERE JDE_Account_No = @JdeAccountNumber 
	
	IF EXISTS(SELECT * FROM TaxOverride  WHERE Country_Code = @CountryCode AND Business_Unit = @JdeBusinessUnit AND Tax_Code = @TaxCode AND Enabled = 1)
		SELECT @ret = Tax_Override_Code FROM TaxOverride WHERE Country_Code = @CountryCode AND Business_Unit = @JdeBusinessUnit AND Tax_Code = @TaxCode AND Enabled = 1
	
	RETURN @ret
	
end
GO
