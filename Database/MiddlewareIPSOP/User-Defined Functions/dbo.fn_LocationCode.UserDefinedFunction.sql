USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LocationCode]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_LocationCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LocationCode]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_LocationCode]
(
	-- Add the parameters for the function here
	@Issue_Date AS DATETIME, @Cover_Date AS VARCHAR(20)
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @LocationCode AS VARCHAR(20)

	SELECT @LocationCode  =''
	
	IF(LTRIM(ISNULL(@Cover_Date,'')) = '')
	BEGIN
		IF (ISNULL(@Issue_Date, '') <> '' AND  @Issue_Date <> '1900-01-01 00:00:00')
			SET @LocationCode  = RTRIM(CONVERT(VARCHAR(11), @Issue_Date, 106))
	END
	ELSE
		SET @LocationCode = @cover_date
	
	RETURN @LocationCode

END
GO
