USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProcessedFlag]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_ProcessedFlag]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProcessedFlag]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_ProcessedFlag]()
RETURNS VARCHAR(1)
AS
BEGIN
	RETURN 'N'

END
GO
