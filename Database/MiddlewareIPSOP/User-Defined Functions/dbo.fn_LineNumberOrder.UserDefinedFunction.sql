USE [MiddlewareIPSOP]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LineNumberOrder]    Script Date: 10/04/2018 10:44:57 ******/
DROP FUNCTION [dbo].[fn_LineNumberOrder]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LineNumberOrder]    Script Date: 10/04/2018 10:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_LineNumberOrder](@lineNumber as int)

RETURNS INT
AS
BEGIN

	RETURN @lineNumber * 1000

END
GO
