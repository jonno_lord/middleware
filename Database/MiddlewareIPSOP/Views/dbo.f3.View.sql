USE [MiddlewareIPSOP]
GO
/****** Object:  View [dbo].[f3]    Script Date: 10/04/2018 10:40:51 ******/
DROP VIEW [dbo].[f3]
GO
/****** Object:  View [dbo].[f3]    Script Date: 10/04/2018 10:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[f3] as
	SELECT f.*, c.Stage FROM F03012Z1 f
		inner join customers c ON (c.id = f.VOEDTN)
GO
