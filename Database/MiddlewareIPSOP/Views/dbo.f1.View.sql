USE [MiddlewareIPSOP]
GO
/****** Object:  View [dbo].[f1]    Script Date: 10/04/2018 10:40:51 ******/
DROP VIEW [dbo].[f1]
GO
/****** Object:  View [dbo].[f1]    Script Date: 10/04/2018 10:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[f1] as
	SELECT f.*, c.Stage FROM F0101Z2 f
		inner join customers c ON (c.id = f.SZEDTN)
GO
