USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenuePeriodSummary]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_RevenuePeriodSummary]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenuePeriodSummary]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,19/05/2011>
-- Description:	<Description,,Inserts selected subscription records into the Revenue_Period_Summary
                             -- table from table F0911>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_RevenuePeriodSummary]
	-- Add the parameters for the stored procedure here
	@Order_Number as varchar (50),
	@Document_Type as varchar(50), 
	@Account_Code_Short_Ref as varchar(50),
	@Business_Unit as varchar(50),
	@Company_Number as varchar(50), 
	@Document_Company as varchar(50),
	@Account_Number as int, 
	@GL_Date as datetime,
	@Period_Month as int,
	@Period_Year as int,
	@Order_Amount as money,
	@Line_Number as int,
	@Object as varchar(50),
	@Subsidiary as varchar(50),
	@Order_Detail_Number as varchar(50),
	@Duration as int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Revenue_Period_Summary (Order_Number,Document_Type,Account_Code_Short_Ref,Business_Unit,Company_Number,Document_Company,Order_Amount,Account_Number,GL_Date,Period_Month,Period_Year, Line_Number,[Object],Subsidiary,Order_Detail_Number,Duration)
	VALUES (CAST(@Order_Number as varchar),@Document_Type,@Account_Code_Short_Ref,@Business_Unit,@Company_Number,@Document_Company,@Order_Amount,@Account_Number,cast(@GL_Date as decimal),@Period_Month,@Period_Year,@Line_Number,@Object,@Subsidiary,@Order_Detail_Number,@Duration)  
END
GO
