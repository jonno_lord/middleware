USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_RevenueProcessPeriod]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_RevenueProcessPeriod]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_RevenueProcessPeriod]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_RevenueProcessPeriod]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int,
	@sysname as varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Revenue_Process_Period
	WHERE Period_Month = @month AND period_year = @year;
END
GO
