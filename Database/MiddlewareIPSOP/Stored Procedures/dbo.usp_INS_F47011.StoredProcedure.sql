USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F47011]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47011]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F47011]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	BEGIN TRANSACTION

	  DELETE F47 FROM F47011 F47 
		INNER JOIN Orders ON F47.SYDCTO = dbo.fn_DocumentType([Type], [Status], Sub_No, Batch_No) 
		AND F47.SYDOCO = Orders.Sub_No
		WHERE Orders.Stage = 1
	
	
	INSERT INTO F47011
		(SYAN8, SYCRCD, SYDCTO, SYDEL1, SYDEL2, SYDOCO, SYKCOO, SYMCU, SYPDDJ,SYOPDJ, SYRYIN, SYTPUR, SYTRDJ, SYVR01, SYTORG, SYEDOC)
		SELECT DISTINCT
		O.JDE_Number																						AS SYAN8,
		LEFT(dbo.fn_DefaultCurrencyCode(), 3)																AS SYCRCD,
		LEFT(dbo.fn_DocumentType(JDE_Payment_Type, [Status], o.Sub_No, o.Batch_No), 2)						AS SYDCTO,
		LEFT(o.Client_name, 20)																				AS SYDEL1,
		LEFT(o.Full_Name, 20)																				AS SYDEL2,
        dbo.fn_OracleOrderNumber(o.Sub_No,MAX(o.Line_No))													AS SYDOCO,
		dbo.fn_CompanyCode(O.Business_Unit_Code)															AS SYKCOO,
		LEFT(dbo.fn_BusinessUnit(O.Business_Unit_Code),12)													AS SYMCU,
		dbo.fn_DateToOracleJulian(o.End_Date)																AS SYPDDJ,
		dbo.fn_DateToOracleJulian(o.Start_Date)																AS SYOPDJ,	
		LEFT(dbo.fn_PaymentInstrumentOrders(O.JDE_Payment_Type), 2)											AS SYRYIN,
		dbo.fn_TransactionSetting()																			AS SYTPUR,
		dbo.fn_DateToOracleJulian(MIN(o.rn_Create_Date))													AS SYTRDJ,
		o.Customer_Ref																						AS SYVR01,
		LEFT(dbo.fn_TransactionOriginator(), 5)																AS SYTORG,
		o.Sub_No																							AS SYEDOC
	FROM Orders as O
		INNER JOIN
			(SELECT MAX(JDE_Number) as JDE_Account_No, Sub_No, dbo.fn_DocumentType(JDE_Payment_Type, [Status], Sub_No, Batch_No) as Document_Type, MIN(Line_No) as Detail_Line_No  
				FROM Orders o1 
				WHERE Stage = 1 
				GROUP BY  Sub_No, dbo.fn_DocumentType(JDE_Payment_Type, [Status], Sub_No, Batch_No)) 
		as qryAccount
				ON qryAccount.Sub_No = O.Sub_No AND
				qryAccount.Document_Type =dbo.fn_DocumentType(o.JDE_Payment_Type, o.[Status], o.Sub_No, Batch_No)
	INNER JOIN Customers c on o.JDE_Number = c.Jde_Account_Number
	WHERE o.Stage = 1 
	GROUP BY o.JDE_Number,o.[Type],o.[Status],o.Client_Name,o.Full_Name,o.Sub_No,o.Batch_No,o.JDE_Payment_Terms_Code,o.JDE_Payment_Type,o.JDE_Number,o.Business_Unit_Code,
				o.End_Date,o.Start_Date,o.Customer_Ref

	UPDATE F47 
	SET f47.OrdersId = Ord.id 
	FROM F47011 F47
	INNER JOIN Orders Ord ON (F47.SYDCTO = dbo.fn_DocumentType(JDE_Payment_Type, [Status], Sub_No, Batch_No)AND F47.SYEDOC = Ord.Sub_No)
	WHERE Stage = 1
	AND F47.OrdersId IS NULL
	
	COMMIT TRANSACTION

	
END

GO
