USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenueRecognition]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_RevenueRecognition]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenueRecognition]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_RevenueRecognition]
	-- Add the parameters for the stored procedure here
	@month as int,
	@year as int,
	@username as varchar(50),
	@Committed_Date as datetime
	
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert INTO Revenue_Recognition  (Period_Month,Period_Year,Run_Date,Run_By,Committed_Date)
	VALUES (@month, @year,GETDATE(),@username,@Committed_Date)
	
END
GO
