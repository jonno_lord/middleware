USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ScrutinyRules]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_ScrutinyRules]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ScrutinyRules]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 19th January 2011
-- Description:	Enables and disables scrutiny rules
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_ScrutinyRules] 
	-- Add the parameters for the stored procedure here
	@scrutinyRuleid int, 
	@scrutinyRuleDisabled AS BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE CustomerScrutinyRules SET Disabled =  @scrutinyRuleDisabled
	WHERE id = @scrutinyRuleid
END
GO
