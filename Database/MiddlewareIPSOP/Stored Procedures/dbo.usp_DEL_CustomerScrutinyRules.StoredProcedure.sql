USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CustomerScrutinyRules]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_CustomerScrutinyRules]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CustomerScrutinyRules]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 13th August 2010
-- Description:	Deletes all from the Customer Scrutiny Rule table
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_CustomerScrutinyRules] 
	-- Add the parameters for the stored procedure here
	@customerScrutinyRuleid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM CustomerScrutinyRules
    WHERE id = @customerScrutinyRuleid
END
GO
