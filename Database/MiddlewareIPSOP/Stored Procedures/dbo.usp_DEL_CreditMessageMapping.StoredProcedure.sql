USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CreditMessageMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_CreditMessageMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CreditMessageMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November
-- Description:	Deletes a credit message mapping based on id parameter
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_CreditMessageMapping] 
	-- Add the parameters for the stored procedure here
	@CMMappingid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM CreditMessageMappings
	WHERE id = @CMMappingid
END
GO
