USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911Z1table]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F0911Z1table]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911Z1table]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,19/05/2011>
-- Description:	<Description,,Inserts selected subscription records into the Revenue_Period_Summary
                             -- table from table F0911>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_F0911Z1table]
	-- Add the parameters for the stored procedure here
    
    @Revenue_Period_Summary_Id as int,
	@VNDOC as float,
	@VNEDCT as varchar(10), 
	@VNANI as varchar(50),
	@VNCO as varchar(50),
	@VNKCO as varchar(50),
	@VNAA as float,
	@VNMCU as varchar(50),
	@VNOBJ as varchar(50),
	@VNEDUS as varchar(10),
    @VNEXA  as varchar(30),
	@VNEXR as varchar(30),
	@VNAID as varchar(50), 
	@VNDGJ as decimal(18,0),
	@VNPN as float,
	@VNDGM as float,
	@VNDGD as float,
	@VNFY as float,
	@VNEDLN as float,
	@VNJELN as float,
	@VNSUB as varchar(50),
	@VNR1 as varchar(50),
	@VNEDBT as varchar(15),
	@VNVINV as varchar(25),
	@VNEDTN as varchar(25),
	@VNCRCD as varchar(5),
	@VNTORG as varchar(10),
	@VNCTRY as float
	
	
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO F0911Z1 (Revenue_Period_Summary_Id,VNDOC,VNEDCT,VNANI,VNMCU,VNCO,VNKCO,VNAA,VNAID,VNDGJ,VNPN,VNFY,VNJELN,VNOBJ,VNSUB,VNR1,VNEDLN,VNEDUS,VNEDBT,VNEXA,VNEXR,VNVINV,VNDGM,VNDGD,VNEDTN,VNCRCD,VNTORG,VNCTRY)
	VALUES (@Revenue_Period_Summary_Id,@VNDOC,@VNEDCT,@VNANI,@VNMCU,@VNCO,@VNKCO,@VNAA,@VNAID, CAST(CONVERT(datetime,@VNDGJ) as decimal),@VNPN,@VNFY,@VNJELN,@VNOBJ,@VNSUB,@VNR1,@VNEDLN,@VNEDUS,@VNEDBT,@VNEXA,@VNEXR,@VNVINV,@VNDGM,@VNDGD,@VNEDTN,@VNCRCD,@VNTORG,@VNCTRY)

END
GO
