USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012z1]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F03012z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012z1]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F03012z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F03012Z1 f
		INNER JOIN Customers c ON c.id = f.voedtn
		WHERE Stage = 1 AND Accepted = 1 and ISNULL(JDE_Account_Number,0) = 0		

	INSERT INTO F03012Z1 
	(CustomerId,VOACL,VOCRCD,VOEDBT,VOEDLN,VOEDTN,VOEDUS,VOPOPN,VOPORQ,VOSTMT,VOTNAC,VOTRAR,VOALKY,VOTXA1,VOBADT)
	SELECT
	Id AS Customer, 
	CONVERT(FLOAT, dbo.fn_CreditLimit())													AS VOACL,
	LEFT(dbo.fn_DefaultBaseCurrency(), 3)													AS VOCRCD,
	LEFT(@BatchNumber, 15)																	AS VOEDBT,
	LEFT(dbo.fn_LineNumber(),1)																AS VOEDLN,
	LEFT(CONVERT(VARCHAR(10), c.id), 22)													AS VOEDTN,
	LEFT(dbo.fn_UserName(), 10)																AS VOEDUS,
	LEFT(dbo.fn_PersonOpeningAccount(), 5)													AS VOPOPN,
	LEFT(dbo.fn_PurchaseOrderRequired(PO_Required), 1)										AS VOPORQ,
	LEFT(dbo.fn_PrintStatement(), 1)														AS VOSTMT,	
	LEFT(dbo.fn_ActionCode(), 2)															AS VOTNAC,
	LEFT(dbo.fn_CustomerPaymentTerms(), 3)													AS VOTRAR,
	LEFT(dbo.fn_LongAddressNumber(Company_Text_ID), 20)										AS VOALKY,
	LEFT(dbo.fn_TaxRateCode(c.Country, c.Vat_Number, c.Tax_Exempt_Cert_No),10)				AS VOTXA1,
	LEFT(dbo.fn_BillingType(),1)															AS VOBADT
	FROM Customers C
		WHERE Stage = 1 AND Accepted = 1 and ISNULL(JDE_Account_Number,0) = 0		
	
END
GO
