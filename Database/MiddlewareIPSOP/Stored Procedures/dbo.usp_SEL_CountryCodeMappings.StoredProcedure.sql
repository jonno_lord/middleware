USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CountryCodeMappings]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_CountryCodeMappings]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CountryCodeMappings]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 30th November
-- Description:	Selects all country code mappings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CountryCodeMappings] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM CountryCodeMappings
END
GO
