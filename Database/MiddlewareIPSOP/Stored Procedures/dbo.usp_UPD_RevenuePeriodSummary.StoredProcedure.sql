USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_RevenuePeriodSummary]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_RevenuePeriodSummary]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_RevenuePeriodSummary]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BMPatterson>
-- Create date: <Create Date,,13/06/2011>
-- Description:	<Description,,Update the Processed flag in the RevenuePeriodSummary table when each
                             -- invoice has been split and processed>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_RevenuePeriodSummary]
	-- Add the parameters for the stored procedure here
	@RevenuePeriodSummaryid as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Revenue_Period_Summary

    SET [Processed] = 1
	
	WHERE Revenue_Period_Summary_Id = @RevenuePeriodSummaryid
  
END
GO
