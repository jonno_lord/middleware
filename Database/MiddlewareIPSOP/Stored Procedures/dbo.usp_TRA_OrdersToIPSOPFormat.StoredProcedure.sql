USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToIPSOPFormat]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_OrdersToIPSOPFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToIPSOPFormat]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_OrdersToIPSOPFormat](@BatchNumber as INT) AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Rows AS INTEGER
	
	SELECT @Rows = COUNT(*) FROM Orders WHERE Stage = 4

	Update Ord SET
	Stage = 5,
	MiddlewareToMiddlewareOut = GETDATE(),
	JDE_Invoice_Number = CONVERT(INT, SDDOC),
	-- divide by 100 to get currency value (e.g. £1 stored as 100) 
	-- then divide by 10???? as this is in the existing middleware logic. Not clear.
	JDE_Invoice_Value = CASE WHEN SDCRCD = 'GBP' THEN SDAEXP / 100 ELSE SDFEA / 100 END,
	JDE_Invoice_Date = CONVERT(VARCHAR(10), dbo.Julian_to_Date(SDIVD), 102)
	FROM Orders Ord
		INNER JOIN F42119 ON F42119.OrdersId = Ord.id
		WHERE Stage = 4                


	SELECT @Rows 

END
GO
