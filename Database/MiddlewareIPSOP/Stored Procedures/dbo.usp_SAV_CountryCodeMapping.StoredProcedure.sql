USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CountryCodeMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SAV_CountryCodeMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CountryCodeMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 30th November 2010
-- Description:	Inserts or edits country code mappings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_CountryCodeMapping] 
	-- Add the parameters for the stored procedure here
	@CCMappingid AS INTEGER, 
	@CountryName AS VARCHAR(50),
	@CountryCode AS VARCHAR(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM CountryCodeMappings WHERE id = @CCMappingid) 
	BEGIN
		UPDATE CountryCodeMappings SET Country_Name = @CountryName, Country_Code = @CountryCode
		WHERE id = @CCMappingid
	END
	ELSE
	BEGIN
		INSERT INTO CountryCodeMappings(Country_Name, Country_Code)
		VALUES (@CountryName, @CountryCode)
	END
END
GO
