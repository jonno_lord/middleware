USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F0101z2]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F0101z2]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F0101Z2 f
		INNER JOIN Customers c ON c.id = f.szedtn
		WHERE Stage = 1 AND Accepted = 1 and ISNULL(Jde_Account_Number,0) = 0

	INSERT INTO F0101Z2
	(CustomerId,SZAC05,SZAC08,SZADD1,SZADD2,SZADD3,SZADD4,SZADDZ,SZALKY,SZAR1,SZAR2,SZAT1,SZCM,
	SZCOUN,SZCTR,SZCTY1,SZEDBT,SZEDTN,SZEDUS,SZMLNM,SZPH1,SZPH2,SZTAX,SZTNAC,SZTXCT,SZURRF,SZEDLN)		
	
	SELECT 
		id AS	CustomerId,
		LEFT(dbo.fn_AgencyCode(),2) AS											SZAC05,
		LEFT(dbo.fn_CountryCode(),3) AS									        SZAC08,
		LEFT('Accounts Payable',40)  AS											SZADD1,
		LEFT(Address_1,40)  AS													SZADD2,
		LEFT(Address_2, 40) AS													SZADD3,
		LEFT(Address_3, 40) AS													SZADD4,
		LEFT(Post_Code, 12) AS													SZADDZ,
		LEFT(dbo.fn_LongAddressNumber(Company_Text_ID), 20) AS					SZALKY,
		LEFT(dbo.fn_DefaultTelephoneCode(phone), 6)								SZAR1,
		LEFT(dbo.fn_DefaultFaxCode(Fax), 6)										SZAR2,
		LEFT('C', 3) as															SZAT1,
		LEFT(dbo.fn_CreditMessage(), 2) AS										SZCM,
		LEFT(dbo.fn_County(c.County, c.Country), 25) as							SZCOUN,
		LEFT(Country, 3) AS														SZCTR,
		LEFT(City, 25) AS														SZCTY1,
		LEFT(@BatchNumber, 15) AS												SZEDBT,
		LEFT(CONVERT(VARCHAR(10), id), 22) AS									SZEDTN,
		LEFT(dbo.fn_UserName(), 10) AS											SZEDUS,
		LEFT(Company_Name, 40) AS												SZMLNM,
		LEFT(Phone, 20) as														SZPH1,
		LEFT(Fax, 20) AS														SZPH2,
		LEFT(VAT_Number, 20) AS													SZTAX,
		LEFT(dbo.fn_TransactionSetting(), 20) AS								SZTNAC,
		LEFT(Tax_Exempt_Cert_No, 20) as											SZTXCT,
		LEFT(Company_Text_ID, 15)  as											SZURRF,
		dbo.fn_LineNumber() AS													SZEDLN
		FROM Customers c
		WHERE Stage = 1 AND Accepted = 1 and ISNULL(Jde_Account_Number,0) = 0

END


GO
