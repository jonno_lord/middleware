USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToJDEFormat]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_OrdersToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_OrdersToJDEFormat]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_OrdersToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION
	
		EXECUTE dbo.usp_INS_F47011  @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE dbo.usp_INS_F47012 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE MiddlewareCommon.[dbo].[usp_UPD_OrderPaymentTerms] @SystemName='MiddlewareIPSOP'
		IF(@@ERROR <> 0) GOTO Failure
		
		--EXECUTE MiddlewareCommon.[dbo].[usp_UPD_OrderTaxCode] @SystemName='MiddlewareIPSOP'
		--IF(@@ERROR <> 0) GOTO Failure

		-- Mark all customers as ready for the next stage
		UPDATE Orders SET Stage = 2, MiddlewaretoMiddlewareIn = GETDATE() WHERE Stage = 1
		IF(@@ERROR <> 0) GOTO Failure
		
		UPDATE o 
		SET o.Oracle_Document_Type = f.SZDCTO
		FROM Orders o 
		INNER JOIN F47012 f on o.id = f.OrdersId
		WHERE o.Stage = 2

	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F47012 WHERE SZEDBT = @BatchNumber

END
GO
