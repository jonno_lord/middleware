USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CreditMessageMappings]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_CreditMessageMappings]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_CreditMessageMappings]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November
-- Description:	Gets all credit message mappings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_CreditMessageMappings] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM CreditMessageMappings
END
GO
