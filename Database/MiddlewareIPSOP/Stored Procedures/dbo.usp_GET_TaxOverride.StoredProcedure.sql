USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TaxOverride]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_GET_TaxOverride]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TaxOverride]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:B.M.Patterson
-- Create date: 20th September 2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_TaxOverride]

	@TaxId AS INTEGER

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM TaxOverride 
	INNER JOIN MiddlewareJDE.dbo.vw_JdeBusinessUnits ON TaxOverride.Business_Unit =  MiddlewareJDE.dbo.vw_JdeBusinessUnits.BusinessUnit
	WHERE Tax_Override_Id = @TaxId 
END
GO
