USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJDEFormat]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToJDEFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJDEFormat]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TRA_CustomersToJDEFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	SET NOCOUNT ON
	
	BEGIN TRANSACTION

		EXECUTE dbo.usp_INS_F0101z2  @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
		
		EXECUTE dbo.usp_INS_F03012z1 @BatchNumber 
		IF(@@ERROR <> 0) GOTO Failure
       
        --EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerPaymentTerms @SystemName = 'MiddlewareIPSOP'
        --IF(@@ERROR <> 0) GOTO Failure
        
        --EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerTaxCode @SystemName = 'MiddlewareIPSOP'
        --IF(@@ERROR <> 0) GOTO Failure
        
		-- Mark all customers as ready for the next stage
		UPDATE C SET Stage = 2, MiddlewaretoMiddlewareIn = GETDATE() 
			FROM Customers C INNER JOIN F0101Z2 F ON (F.SZEDTN = C.ID)
			WHERE Stage = 1 
			
		-- Mark shared customers as ready to be mapped. *Merged Accounts
		UPDATE C SET Stage = 2
			FROM Customers C WHERE Stage = 1 AND Jde_Account_Number IS NOT NULL 

		IF(@@ERROR <> 0) GOTO Failure

	COMMIT TRANSACTION
	
	GOTO ENDFunction

Failure:
	ROLLBACK TRANSACTION

ENDFunction:
	-- return the amount of created rows for this batch
	SELECT COUNT(*) FROM F0101z2 WHERE SZEDBT = @BatchNumber

END
GO
