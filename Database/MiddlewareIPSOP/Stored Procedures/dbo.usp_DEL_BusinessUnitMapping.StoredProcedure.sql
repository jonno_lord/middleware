USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_BusinessUnitMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_BusinessUnitMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_BusinessUnitMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November
-- Description:	Deletes a specified Business Unit Mapping
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_BusinessUnitMapping]
	-- Add the parameters for the stored procedure here
	@BUMappingid AS INTEGER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM BusinessUnitMappings WHERE id = @BUMappingid
END
GO
