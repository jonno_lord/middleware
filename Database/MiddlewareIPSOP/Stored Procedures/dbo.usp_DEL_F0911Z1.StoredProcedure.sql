USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_F0911Z1]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_F0911Z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_F0911Z1]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BMPatterson>
-- Create date: <Create Date,,26/09/2011>
-- Description:	<Description,,Deletes invoices from the F0911Z1 tables based on date selected>
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_F0911Z1]
	-- Add the parameters for the stored procedure here
	@month as int,
	@firstyearpart as int,
	@secondyearpart as int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM F0911Z1
	WHERE VNPN = @month AND VNCTRY = @firstyearpart AND VNFY = @secondyearpart
	
	
END
GO
