USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_RevenuePeriodSummary]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_RevenuePeriodSummary]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_RevenuePeriodSummary]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,18/05/2011>
-- Description:	<Description,,Selects all the records from Revenue Recognition Summary and display >
-- =============================================

CREATE PROCEDURE [dbo].[usp_SEL_RevenuePeriodSummary]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
       [Order_Number]
      ,[Document_Type]
      ,[Account_Code_Short_Ref]
      ,[Business_Unit]
      ,[Company_Number]
      ,[Document_Company]
      ,ROUND(CONVERT(MONEY, SUM(Order_Amount) / 100),2) as Order_Amount
      ,[Account_Number]
      ,dbo.Julian_to_Date(cast (GL_Date as int)) as GL_Date
      ,[Period_Month]
      ,[Period_Year]
      ,[Line_Number]
      ,[Start_Date]
      ,[Duration]
      ,[Object]
      ,[Subsidiary]
      ,[Order_Detail_Number]

	FROM Revenue_Period_Summary
	GROUP BY 
	   [Order_Number]
      ,[Document_Type]
      ,[Account_Code_Short_Ref]
      ,[Business_Unit]
      ,[Company_Number]
      ,[Document_Company]
      ,[Account_Number]
      ,[GL_Date]
      ,[Period_Month]
      ,[Period_Year]
      ,[Line_Number]
      ,[Start_Date]
      ,[Duration]
      ,[Object]
      ,[Subsidiary]
      ,[Order_Detail_Number]

	
END
GO
