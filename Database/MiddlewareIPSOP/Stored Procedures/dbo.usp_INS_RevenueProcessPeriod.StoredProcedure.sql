USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenueProcessPeriod]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_RevenueProcessPeriod]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_RevenueProcessPeriod]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BMPatterson>
-- Create date: <Create Date,,23/05/2011>
-- Description:	<Description,,Inserts revenue_period_summary_id, month and year from F0911Z1, into Revenue_Process_Period>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_RevenueProcessPeriod]
	-- Add the parameters for the stored procedure here
	@Revenue_Process_Period_Id as int,
	@month as int,
	@year as int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO Revenue_Process_Period
    VALUES (@Revenue_Process_Period_Id,@month,@year)
	
END
GO
