USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BusinessUnits]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_BusinessUnits]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BusinessUnits]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November 2010
-- Description:	Gets all business unit mappings 
-- and business unit descriptions from MiddlewareIPSOP and  MiddlewareJDE
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_BusinessUnits]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM BusinessUnitMappings
	INNER JOIN MiddlewareJDE.dbo.vw_JdeBusinessUnits ON BusinessUnitMappings.Business_Unit =  MiddlewareJDE.dbo.vw_JdeBusinessUnits.BusinessUnit
END
GO
