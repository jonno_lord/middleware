USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_InvoiceData]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_InvoiceData]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_InvoiceData]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,13/05/2011>
-- Description:	<Description,,Selects all the Order_Numbers from the Revenue_Period_Summary
                 -- based on Month and Year>
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_InvoiceData]
	-- Add the parameters for the stored procedure here
	@month int,
	@year as int,
	@sysname as varchar(50)

	

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @sysname = 'IPSOP'
        -- Delete all previously split invoice entries from [F0911Z1] table in MiddlewareIPSOP
        -- Delete the annual supscription records from the Revenue_Period_Summary table that correspond
        -- to the Order_Number. 
		Begin
			declare @DeletedInvoiceIDs table (id int);
		     
			DELETE t1
			output deleted.Revenue_Period_Summary_Id into @DeletedInvoiceIDs  
			FROM [F0911Z1] as t1 
			INNER JOIN Revenue_Period_Summary as t2 ON Order_Number = t1.Revenue_Period_Summary_Id
			WHERE Period_Month = @month AND Period_Year = @year;
			
			DELETE t2
			FROM Revenue_Period_Summary as t2 
			JOIN @DeletedInvoiceIDs del
			on del.id = t2.Order_Number	
		end
	ELSE
	    -- Delete all previous invoice entries from JDE [F0911Z1] table.
		Begin
			DELETE t1 FROM [F0911Z1] t1
			INNER JOIN Revenue_Period_Summary ON Order_Number = t1.Revenue_Period_Summary_Id
			WHERE Period_Month = @month AND Period_Year = @year
		end

END
GO
