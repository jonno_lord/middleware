USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CustomerScrutinyRule]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SAV_CustomerScrutinyRule]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CustomerScrutinyRule]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 13th August 2010
-- Description:	Decides whether to insert or update based on whether id exists
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_CustomerScrutinyRule] 
	-- Add the parameters for the stored procedure here
	
	@customerScrutinyRuleid AS int,
	@customerScrutinyRuleDescription AS NVARCHAR(1024),
	@customerScrutinyRuleReturnsViolation AS BIT,
	@customerScrutinyRuleReturnsDuplicates AS BIT,
	@customerScrutinyRuleQuerySystemid AS int,
	@customerScrutinyRuleIsQueryBased AS BIT,
	@customerScrutinyRuleQuery AS NVARCHAR(MAX),
	@customerScrutinyRuleAssemblyName AS NVARCHAR(250),
	@customerScrutinyRuleClassName AS NVARCHAR(250),
	@customerScrutinyRuleDisabled AS BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM CustomerScrutinyRules WHERE id = @customerScrutinyRuleid)
	BEGIN
		UPDATE CustomerScrutinyRules SET Description = @customerScrutinyRuleDescription, ReturnsViolation = @customerScrutinyRuleReturnsViolation, ReturnsDuplicates = @customerScrutinyRuleReturnsDuplicates, QuerySystemId = @customerScrutinyRuleQuerySystemid, IsQueryBased = @customerScrutinyRuleIsQueryBased, Query = @customerScrutinyRuleQuery, AssemblyName = @customerScrutinyRuleAssemblyName, ClassName = @customerScrutinyRuleClassName, Disabled = @customerScrutinyRuleDisabled
		WHERE id = @customerScrutinyRuleid
	END
	ELSE
	BEGIN
		INSERT INTO CustomerScrutinyRules (Description, ReturnsViolation, ReturnsDuplicates, QuerySystemId, IsQueryBased, Query, AssemblyName, ClassName, Disabled)
		VALUES (@customerScrutinyRuleDescription, @customerScrutinyRuleReturnsViolation, @customerScrutinyRuleReturnsDuplicates, @customerScrutinyRuleQuerySystemid, @customerScrutinyRuleIsQueryBased, @customerScrutinyRuleQuery, @customerScrutinyRuleAssemblyName, @customerScrutinyRuleClassName, @customerScrutinyRuleDisabled)
	END
END
GO
