USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CountryCodeMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_CountryCodeMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_CountryCodeMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 30th November 2010
-- Description:	Deletes specified copuntry code mapping by id supplied
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_CountryCodeMapping]
	-- Add the parameters for the stored procedure here
	@CCMappingid int = 0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM CountryCodeMappings
	WHERE id = @CCMappingid
END
GO
