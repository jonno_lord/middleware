USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F47012]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F47012]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_INS_F47012]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- verify the data is valid, and perform fixes
	BEGIN TRANSACTION
	
		DELETE F12 FROM F47012 F12 
		    INNER JOIN Orders O ON F12.SZDCTO = dbo.fn_DocumentType(JDE_Payment_Type, [Status],Sub_No,Batch_No) 
			AND F12.SZDOCO = O.Sub_No AND F12.SZEDLN = dbo.fn_LineNumberOrder(O.Line_No)
			WHERE o.Stage = 1

		INSERT INTO F47012
			(OrdersId,SZAN8,SZDCTO,SZDOCO,SZDSC1,SZDSC2,SZEDBT,SZEXR1,SZTXA1,SZLITM,SZLNID,SZLOCN,SZMCU,
			SZOPDJ,SZPTC,SZSBL,SZSRP4,SZSRP5,SZUORG,SZUPRC,SZVR02,/*SZDOC,*/SZEDOC/*SZRLLN*/)
			SELECT DISTINCT
				O.id																													as OrdersId,
				O.JDE_Number																											as SZAN8,
				LEFT(dbo.fn_DocumentType(O.JDE_Payment_Type,[Status],Sub_No,Batch_No),2)												as SZDCTO,
			    dbo.fn_OracleDetailOrderNumber(O.Sub_No,o.Batch_No,o.Document_Type)														as SZDOCO,
				LEFT(O.Invoice_Description,30)																							as SZDSC1,
				LEFT(O.Business_Unit_Description, 30)																					as SZDSC2,
				O.Batch_No																												as SZEDBT,
				LEFT(dbo.fn_ExcludeVATCode(O.Excl_Vat),1)																				as SZEXR1,
				c.TXA1																													as SZTXA1,
				LEFT(dbo.fn_ItemCodeOrders(o.JDE_Item_Code, O.Item_Code_Type, O.JDE_Number), 26)										as SZLITM,
				dbo.fn_LineNumberOrder(O.Line_No)																						as SZLNID,
				dbo.fn_LocationCode(Issue_Date, Cover_Date)																				as SZLOCN,
				dbo.fn_BusinessUnit(O.Business_Unit_Code)																				as SZMCU,
				dbo.fn_DateToOracleJulian(o.[Start_date])																				as SZOPDJ,
				dbo.fn_PaymentTermsOrdersPTC(ISNULL(JDE_Payment_Terms_Code, ''), Jde_Payment_Type, O.[Type], [status], jde_number)		as SZPTC,
				dbo.fn_SubledgerDate(o.[Start_Date])																					as SZSBL,
				O.Duration																												as SZSRP4,
				LEFT(dbo.fn_DateCheckSPR5(O.Date_Check), 3)																				as SZSRP5,
				dbo.fn_QuantityOrder(o.Sub_No, o.Batch_No, o.[Status])																	as SZUORG,
				dbo.fn_InvoiceAmount(O.IPSOP_Invoice_Value, o.Sub_No,o.Batch_No,o.[Status])												as SZUPRC,
				O.Quantity																												as SZVR02,
				--dbo.fn_OriginalInvoiceNumber(o.Document_Type,o.Sub_No,o.IPSOP_Invoice_Value,
				--										o.Invoice_Description,o.Status,o.Price)											as SZDOC,
				o.Sub_No																												as SZEDOC
				--dbo.fn_OriginalLineNumber(o.Document_Type,o.Sub_No,o.IPSOP_Invoice_Value,
				--								o.Invoice_Description,o.Line_No,o.Batch_No,o.Price)										as SZRLLN				
				FROM Orders O 
			    INNER JOIN Customers C on O.JDE_Number = C.JDE_Account_Number
				WHERE O.Stage = 1
				
			UPDATE F47
			SET SZPTC = 'Prepaid'	
			FROM F47012 F47
			INNER JOIN Orders o on f47.OrdersId = o.id
			WHERE o.Stage = 1 AND SZDCTO LIKE '%3'
				
	COMMIT TRANSACTION

	
END


GO
