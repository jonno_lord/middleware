USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CreditMessageMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_GET_CreditMessageMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CreditMessageMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November 2010
-- Description:	Gets values for specifed Credit Message Mapping based on id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_CreditMessageMapping] 
	-- Add the parameters for the stored procedure here
	@CMMappingid AS int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM CreditMessageMappings
	WHERE id = @CMMappingid
END
GO
