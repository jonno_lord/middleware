USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BusinessUnitMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_GET_BusinessUnitMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BusinessUnitMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November
-- Description:	Gets all values of a specified business unit mapping by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_BusinessUnitMapping] 
	-- Add the parameters for the stored procedure here
	@BUMappingid int,
	@BusinessUnit AS INTEGER
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM BusinessUnitMappings
	INNER JOIN MiddlewareJDE.dbo.vw_JdeBusinessUnits ON BusinessUnitMappings.Business_Unit =  MiddlewareJDE.dbo.vw_JdeBusinessUnits.BusinessUnit
	WHERE ID = @BUMappingid AND Business_Unit = @BusinessUnit
END
GO
