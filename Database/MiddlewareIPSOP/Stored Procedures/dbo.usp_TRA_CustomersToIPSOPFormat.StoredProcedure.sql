USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToIPSOPFormat]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToIPSOPFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToIPSOPFormat]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TRA_CustomersToIPSOPFormat]
	-- Add the parameters for the stored procedure here
	@BatchNumber as INT = 0
AS
BEGIN

	SET NOCOUNT ON
	
	UPDATE CTD	
		SET Stage = 5, 
		MiddlewareToMiddlewareOut = GETDATE(),
		Company_Name = f55.Q1MLNM,
		Address_1 = f55.Q1ADD1,
		Address_2 = F55.Q1ADD2,
		Address_3 = f55.Q1ADD3,
		Address_4 = f55.Q1ADD4,
		city = f55.q1cty1,
		County = f55.q1coun,
		Post_Code = f55.q1addz,
		Phone = f55.q1ph1,
		Fax = f55.q1ph2,		
		JDE_Payment_Terms = pay.PaymentTerms,
		JDE_Credit_Limit = f55.q1acl,
		Tax_Exempt_Cert_No = f55.q1txct,
		JDE_Available_Credit = f55.q1amcr,
		Credit_Message_Code = f55.q1cm,
		Credit_Message_Description = jicm.Credit_Message_Description,
		q1hold = f55.q1hold,
		VAT_Number = q1Tax,
		country = ccm.Country_Name,
		Credit_Controller_Name= cco.Fullname,
		Credit_Controller_Phone = cco.Phone,
		txa1 = f55.q1txa1,
		PO_Required =CASE f55.Q1PORQ WHEN 'Y' THEN 1 ELSE 0 END,
		Cat_Code_28 = f55.Q1AC28
		FROM Customers ctd
			INNER JOIN F550101 f55 ON (f55.Q1URRF = ctd.Company_Text_Id) AND ctd.JDE_Account_Number = F55.Q1AN8
			INNER JOIN CountryCodeMappings ccm ON f55.Q1CTR = ccm.Country_Code
			LEFT OUTER JOIN MiddlewareJDE.dbo.vw_creditcontrollers cco ON f55.Q1AC02 = cco.initials
			LEFT OUTER JOIN MiddlewareJDE.dbo.vw_paymentterms pay ON f55.Q1TRAR = pay.code
			LEFT OUTER JOIN CreditMessageMappings jicm ON (F55.q1cm = jicm.Credit_Message_Code)
	WHERE ctd.Stage > 3


END

GO
