USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CreditMessageMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SAV_CreditMessageMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_CreditMessageMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November
-- Description:	Inserts or edits a Credit Message Mapping
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_CreditMessageMapping] 
	-- Add the parameters for the stored procedure here
	@CMMappingid AS INTEGER,
	@CreditMessageCode AS VARCHAR(10),
	@CreditMessageDescription AS VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM CreditMessageMappings WHERE id = @CMMappingid) 
	BEGIN
		UPDATE CreditMessageMappings SET Credit_Message_Code = @CreditMessageCode, Credit_Message_Description = @CreditMessageDescription
		WHERE ID = @CMMappingid
	END
	ELSE
	BEGIN
		INSERT INTO CreditMessageMappings (Credit_Message_Code, Credit_Message_Description)
		VALUES (@CreditMessageCode, @CreditMessageDescription)
	END
END
GO
