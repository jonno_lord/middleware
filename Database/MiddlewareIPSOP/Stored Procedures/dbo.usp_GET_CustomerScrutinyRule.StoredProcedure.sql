USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerScrutinyRule]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_GET_CustomerScrutinyRule]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerScrutinyRule]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 13th August 2010
-- Description:	Selects specified Customer Scrutiny Rule
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_CustomerScrutinyRule] 
	-- Add the parameters for the stored procedure here
	@customerScrutinyRuleid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM CustomerScrutinyRules
	INNER JOIN MiddlewareSystem.dbo.Systems ON Systems.id = QuerySystemId
	WHERE CustomerScrutinyRules.id = @customerScrutinyRuleid
END
GO
