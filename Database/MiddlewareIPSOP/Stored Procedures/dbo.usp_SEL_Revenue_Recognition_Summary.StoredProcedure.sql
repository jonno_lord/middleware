USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Revenue_Recognition_Summary]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_Revenue_Recognition_Summary]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Revenue_Recognition_Summary]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,18/05/2011>
-- Description:	<Description,,Selects all the records from Revenue Recognition Summary and display >
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_Revenue_Recognition_Summary]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Revenue_Period_Summary;
END
GO
