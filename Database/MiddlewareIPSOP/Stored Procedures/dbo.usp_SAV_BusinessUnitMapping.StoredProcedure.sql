USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_BusinessUnitMapping]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SAV_BusinessUnitMapping]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_BusinessUnitMapping]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th November 2010
-- Description:	Either Inserts or Edits a business unit mapping
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_BusinessUnitMapping] 
	-- Add the parameters for the stored procedure here

	@BUMappingid AS INT,
	@BusinessUnit AS NVARCHAR(4),
	@CollectionClient AS NVARCHAR(50),
	@CollectionAgent AS NVARCHAR(50),
	@AdminContactName AS NVARCHAR(50),
	@AdminContactNumber AS NVARCHAR(50) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM BusinessUnitMappings WHERE Business_Unit = @BusinessUnit AND id = @BUMappingid) 
	BEGIN
		UPDATE BusinessUnitMappings SET Business_Unit = @BusinessUnit, Collection_Manager_Client = @CollectionClient, Collection_Manager_Agent = @CollectionAgent, Admin_Contact_Name = @AdminContactName, Admin_Contact_Number = @AdminContactName
		WHERE ID = @BUMappingid
	END
	ELSE
	BEGIN
		INSERT INTO BusinessUnitMappings (Business_Unit, Collection_Manager_Client, Collection_Manager_Agent, Admin_Contact_Name, Admin_Contact_Number)
		VALUES (@BusinessUnit, @CollectionClient, @CollectionAgent, @AdminContactName, @AdminContactNumber)
	END
END
GO
