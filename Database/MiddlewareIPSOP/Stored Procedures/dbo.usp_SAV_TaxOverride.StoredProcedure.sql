USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_TaxOverride]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_SAV_TaxOverride]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_TaxOverride]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: B.M.Patterson
-- Create date: 20th September 2010
-- Description:	Decides whether to add or insert into the TaxOverride table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_TaxOverride] 
	-- Add the parameters for the stored procedure here
	@TaxId AS int,
	@CountryCode AS varchar(10),
	@BusinessUnit AS varchar(12),
	@TaxCode AS varchar(20),
	@TaxOverrideCode AS varchar(20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM TaxOverride WHERE Country_Code = @CountryCode AND Business_Unit = @BusinessUnit AND Tax_Code = @TaxCode) 
	BEGIN
		UPDATE TaxOverride SET Country_Code = @CountryCode, Business_Unit = @BusinessUnit, Tax_Code = @TaxCode, Tax_Override_Code = @TaxOverrideCode
		WHERE Country_Code= @CountryCode AND Business_Unit= @BusinessUnit AND Tax_Code= @TaxCode
	END
	ELSE
	BEGIN
		INSERT INTO TaxOverride (Country_Code, Business_Unit, Tax_Code, Tax_Override_Code, Enabled)
		VALUES (@CountryCode, @BusinessUnit, @TaxCode, @TaxOverrideCode, 1)
	END
END
GO
