USE [MiddlewareIPSOP]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_EnableScrutinyRules]    Script Date: 10/04/2018 10:41:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_EnableScrutinyRules]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_EnableScrutinyRules]    Script Date: 10/04/2018 10:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th January
-- Description:	Enables all customer scrutiny rules
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_EnableScrutinyRules] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE CustomerScrutinyRules SET Disabled = 0
END
GO
