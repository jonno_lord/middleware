USE [MiddlewareJDE]
GO
/****** Object:  View [dbo].[vw_ECVatGroups]    Script Date: 10/04/2018 10:46:00 ******/
DROP VIEW [dbo].[vw_ECVatGroups]
GO
/****** Object:  View [dbo].[vw_ECVatGroups]    Script Date: 10/04/2018 10:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vw_ECVatGroups] AS
select LTRIM(RTRIM(DRKY)) AS CountryCode, LTRIM(RTRIM(DRDL01))  as CountryName 
	FROM dbo.f0005 where DRSY = '55' AND DRRT = 'EC'
GO
