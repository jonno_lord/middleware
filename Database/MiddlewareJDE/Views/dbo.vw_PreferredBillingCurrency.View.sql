USE [MiddlewareJDE]
GO
/****** Object:  View [dbo].[vw_PreferredBillingCurrency]    Script Date: 10/04/2018 10:46:00 ******/
DROP VIEW [dbo].[vw_PreferredBillingCurrency]
GO
/****** Object:  View [dbo].[vw_PreferredBillingCurrency]    Script Date: 10/04/2018 10:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_PreferredBillingCurrency] as
select LTRIM(RTRIM(DRKY)) as CountryCode, 
	LTRIM(RTRIM(DRDL01)) as Country, LTRIM(RTRIM(DRDL02)) as Currency from f0005 where DRRT = 'PC'
GO
