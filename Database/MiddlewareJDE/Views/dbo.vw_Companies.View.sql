USE [MiddlewareJDE]
GO
/****** Object:  View [dbo].[vw_Companies]    Script Date: 10/04/2018 10:46:00 ******/
DROP VIEW [dbo].[vw_Companies]
GO
/****** Object:  View [dbo].[vw_Companies]    Script Date: 10/04/2018 10:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_Companies] AS
SELECT CCCO as CompanyCode, CCAN8 as AccountNumber, CCNAME as CompanyName, F0101.ABAC28 as CatCode28, F0101.ABAC29 as CatCode29, F0101.ABAC30 as CatCode30 FROM dbo.F0010 comp
	INNER JOIN 
	(SELECT DRDL01 as CompanyCode, DRKY as CompanyKey FROM F0005 WHERE  DRSY = '55' AND DRRT = 'TC' AND CHARINDEX('.', DRDL01) = 0)
		as  CompLu ON
		(CONVERT(INT, CompLu.CompanyKey) = comp.CCCO)
	INNER JOIN f0101
		ON CONVERT(INT, CompLu.CompanyKey) = F0101.ABAN8
GO
