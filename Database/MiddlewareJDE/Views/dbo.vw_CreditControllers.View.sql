USE [MiddlewareJDE]
GO
/****** Object:  View [dbo].[vw_CreditControllers]    Script Date: 10/04/2018 10:46:00 ******/
DROP VIEW [dbo].[vw_CreditControllers]
GO
/****** Object:  View [dbo].[vw_CreditControllers]    Script Date: 10/04/2018 10:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_CreditControllers] AS
SELECT  DISTINCT   f5.DRDL01 AS Initials, ISNULL(logon.FullName, '') AS FullName, ISNULL(logon.LogonName, '') AS LogonName, ISNULL(phone.Phone, '') AS Phone, ISNULL(Fax.Fax, '') 
                      AS Fax, ISNULL(Email.Email, '') AS Email, teams.CreditControllerTeam, '' as CreditControllerNotes
FROM         dbo.F0005 AS f5 
	LEFT OUTER JOIN
	(SELECT  DRKY AS LogonName, DRDL02 AS CreditControllerTeam FROM dbo.F0005
	WHERE (DRSY = '55') AND (DRRT = 'CC')) AS teams ON f5.DRKY = teams.LogonName 
	LEFT OUTER JOIN
                          (SELECT     DRKY AS LogonName, DRDL02 AS AN8, DRDL01 AS FullName
                            FROM          dbo.F0005
                            WHERE      (DRSY = '01') AND (DRRT = 'CM')) AS logon ON f5.DRKY = logon.LogonName LEFT OUTER JOIN
                          (SELECT     WPPH1 AS Phone, WPAN8
                            FROM          dbo.F0115
                            WHERE      (WPAR1 = 'Tel')) AS phone ON phone.WPAN8 = logon.AN8 LEFT OUTER JOIN
                          (SELECT     WPPH1 AS Fax, WPAN8
                            FROM          dbo.F0115
                            WHERE      (WPAR1 = 'Fax')) AS Fax ON Fax.WPAN8 = logon.AN8 LEFT OUTER JOIN
                          (SELECT     WWREM1 AS Email, WWAN8
                            FROM          dbo.F0111) AS Email ON Email.WWAN8 = logon.AN8
WHERE     (f5.DRSY = '55') AND (f5.DRRT = 'CC')



GO
