USE [MiddlewareJDE]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_F0101]    Script Date: 10/04/2018 10:46:53 ******/
DROP PROCEDURE [dbo].[usp_SEL_F0101]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_F0101]    Script Date: 10/04/2018 10:46:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 3rd December 2010
-- Description:	Selects all from the F0101 table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_F0101]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM F0101
END
GO
