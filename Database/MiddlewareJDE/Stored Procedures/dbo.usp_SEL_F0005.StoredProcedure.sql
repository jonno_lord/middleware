USE [MiddlewareJDE]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_F0005]    Script Date: 10/04/2018 10:46:53 ******/
DROP PROCEDURE [dbo].[usp_SEL_F0005]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_F0005]    Script Date: 10/04/2018 10:46:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 2nd December 2010
-- Description:	Selects all from F0005 table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_F0005] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM F0005
END
GO
