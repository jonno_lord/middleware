USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_MetricsCPU]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_MetricsCPU]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_MetricsCPU]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 8th October 2010
-- Description:	Selects metrics specifically for CPU utilisation based on date value
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_MetricsCPU] 
	-- Add the parameters for the stored procedure here
	@EntryStartDate AS DATETIME,
	@EntryEndDate AS DATETIME
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Metrics
	WHERE MetricTypeid = 2 AND 
	(EntryDate BETWEEN @EntryStartDate AND @EntryEndDate)
	
END
GO
