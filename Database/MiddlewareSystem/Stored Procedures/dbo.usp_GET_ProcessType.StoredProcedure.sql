USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ProcessType]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_ProcessType]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ProcessType]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Selects everything from the ProcessTypes table based on a specific id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ProcessType]
	-- Add the parameters for the stored procedure here
	@processTypeid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ProcessTypes WHERE id = @processTypeid
END
GO
