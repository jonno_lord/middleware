USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JobFailed]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_JobFailed]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JobFailed]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UPD_JobFailed](
	-- Add the parameters for the stored procedure here
	@JobId AS INTEGER
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE jobs SET status = 'FAILED' WHERE id = @JobId 
    UPDATE Processes SET status = 'FAILED' WHERE jobid = @JobId AND (status='PROCESSING' OR status='CREATING')
    
END
GO
