USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Batch]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_Batch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Batch]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 20th August 2010
-- Description:	Gets Batch based on id and inner joins with batch log table
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_Batch] 
	-- Add the parameters for the stored procedure here
	@batchid int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Batches.id as Id,  
		Batches.Status as Status,
		Batches.StartDate as Startdate,
		Batches.EndDate as EndDate,
		Batches.JobId as JobId,
		Jobs.ShortName as JobName,
		Systems.ShortName as SystemName,
		CONVERT (VARCHAR(10), DATEDIFF(SECOND, StartDate, EndDate)) AS DurationSeconds
		 
		
		FROM Batches
	INNER JOIN BatchLogs ON Batches.id = BatchLogs.Batchid
	INNER JOIN Jobs ON Batches.JobId = Jobs.id 
	INNER JOIN Systems ON Jobs.Systemid = Systems.id
	WHERE (Batches.id = @batchid)
END
GO
