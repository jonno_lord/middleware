USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BatchTimeOuts]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_BatchTimeOuts]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BatchTimeOuts]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		steve Parker
-- Create date: 2nd Feb 2011
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_BatchTimeOuts] 
	@BatchNumber AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
    -- Insert statements for procedure here
	SELECT * FROM BatchLogs where BatchId = @BatchNumber AND
		([Description] LIKE '%connection not established correctly%' OR
		[Description] LIKE '%CANNOTACQUIRECONNECTIONFROMCONNECTIONMANAGER%' OR
		[Description] LIKE '%Failed to acquire connection%' OR
		[Description] LIKE '%timeout period elapsed%')
		
END
GO
