USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Metrics]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_INS_Metrics]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Metrics]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<BPatterson>
-- Create date: <24/09/2010>
-- Description:	<Gets the disk space and time>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_Metrics] 
	-- Add the parameters for the stored procedure here
		@value as FLOAT,
		@Enumid as INT,
		@EntryDate AS DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@EntryDate IS NULL)
		SET @EntryDate = GETDATE()

	INSERT INTO Metrics(EntryDate, Value, MetricTypeId)
		SELECT @EntryDate, @value, id FROM MetricTypes
			WHERE EnumId = @Enumid 

END
GO
