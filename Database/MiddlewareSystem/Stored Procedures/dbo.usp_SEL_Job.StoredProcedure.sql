USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Job]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_Job]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Job]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author, Brenda Patterson>
-- Create date: <16/08/2010>
-- Description:	<Selects Batches based on the date selected by the user>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_Job]
	-- Add the parameters for the stored procedure here
	   @SystemName as varchar(50)= ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT Jobs.ShortName FROM Jobs
	INNER JOIN Systems
	on Jobs.Systemid = Systems.id
	WHERE Systems.ShortName = @SystemName or @SystemName = ''
		

	
END
GO
