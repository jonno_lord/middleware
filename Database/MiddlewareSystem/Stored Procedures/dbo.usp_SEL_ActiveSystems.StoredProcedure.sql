USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ActiveSystems]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_ActiveSystems]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ActiveSystems]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Selects all from the systems table with an inner join to
-- the Jobs table for finding a Jobs for a particular System.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ActiveSystems]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT Systems.* FROM Systems
		INNER JOIN Jobs ON Systems.id = Jobs.Systemid
	ORDER BY ShortName
		
END
GO
