USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_SystemConnectionName]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_SystemConnectionName]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_SystemConnectionName]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 13th August 2010
-- Description:	Returns the System details for the ID specified
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_SystemConnectionName]
	-- Add the parameters for the stored procedure here
	@systemid AS INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ConnectionName FROM systems WHERE id = @systemid 
END
GO
