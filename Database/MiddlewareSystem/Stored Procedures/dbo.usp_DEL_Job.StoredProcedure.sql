USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_Job]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_DEL_Job]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_Job]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 21st July 2010
-- Description:	deletes the specified job from the table
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_Job] 
	-- Add the parameters for the stored procedure here
	@jobid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DELETE FROM Processes
    WHERE jobid = @jobid
    
    DELETE FROM Schedules
    WHERE jobid = @jobid
    
	DELETE FROM Jobs
	WHERE id = @jobid
	
	
END
GO
