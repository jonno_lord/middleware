USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_BatchProcess]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_INS_BatchProcess]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_BatchProcess]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 2nd August 2010 - 13:07
-- Description:	Inserts a batch into the batch log
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_BatchProcess]
	-- Add the parameters for the stored procedure here
	@Processid AS INT,
	@Batchid AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO BatchProcesses(Processid, Batchid, StartDate) VALUES
							  (@Processid, @Batchid, GETDATE())
	SELECT @@IDENTITY 
	
END
GO
