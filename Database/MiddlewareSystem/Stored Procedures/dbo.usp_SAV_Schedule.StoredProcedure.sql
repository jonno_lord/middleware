USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_Schedule]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SAV_Schedule]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_Schedule]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 27th July 2010
-- Description:	Decides whether to edit or insert into the schedule table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_Schedule] 
	-- Add the parameters for the stored procedure here
	@scheduleid AS int,
	@scheduleJobid AS int,
	@scheduleDescription AS NVARCHAR(MAX),
	@scheduleStartDate AS DATETIME,
	@scheduleStartTime AS NVARCHAR(50),
	@scheduleEndTime AS NVARCHAR(50),
	@scheduleMonday AS BIT,
	@scheduleTuesday AS BIT,
	@scheduleWednesday AS BIT,
	@scheduleThursday AS BIT,
	@scheduleFriday AS BIT,
	@scheduleSaturday AS BIT,
	@scheduleSunday AS BIT,
	@scheduleFrequency AS INTEGER,
	@scheduleDisabled AS BIT,
	@scheduleEndDate AS DATETIME = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		IF EXISTS (SELECT * FROM Schedules WHERE id = @scheduleid)
	BEGIN
		UPDATE Schedules SET Jobid = @scheduleJobid, Description = @scheduleDescription, StartDate = @scheduleStartDate, StartTime = @scheduleStartTime, EndDate = @scheduleEndDate, EndTime = @scheduleEndTime, Monday = @scheduleMonday, Tuesday = @scheduleTuesday, Wednesday = @scheduleWednesday, Thursday = @scheduleThursday, Friday = @scheduleFriday, Saturday = @scheduleSaturday, Sunday = @scheduleSunday, Frequency = @scheduleFrequency, [Disabled] = @scheduleDisabled
		WHERE id = @scheduleid 
	END
	ELSE
	BEGIN
		INSERT INTO Schedules (Jobid,Description, StartDate, StartTime, EndDate, EndTime, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, Frequency, [Disabled])
		VALUES (@scheduleJobid, @scheduleDescription, @scheduleStartDate, @scheduleStartTime, @scheduleEndDate, @scheduleEndTime, @scheduleMonday, @scheduleTuesday, @scheduleWednesday,  @scheduleThursday,  @scheduleFriday, @scheduleSaturday, @scheduleSunday, @scheduleFrequency, @scheduleDisabled)
	END
END
GO
