USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ProcessType]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SAV_ProcessType]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ProcessType]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Decides whether to add or insert into Process Type table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_ProcessType] 
	-- Add the parameters for the stored procedure here
	@processTypeid int,
	@processType AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		IF EXISTS (SELECT * FROM ProcessTypes WHERE id = @processTypeid)
	BEGIN
		UPDATE ProcessTypes SET ProcessType = @processType
		WHERE id = @processTypeid 
	END
	ELSE
	BEGIN
		INSERT INTO ProcessTypes(ProcessType)
		VALUES (@processType)
	END
END
GO
