USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_LastBatches]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_LastBatches]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_LastBatches]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 31st August
-- Description:	Returns last 100 batches that failed
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_LastBatches]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * FROM 
	(
	SELECT TOP 10 batches.id, 
				  batches.Status,
				  jobs.ShortName AS ShortName,
				  systems.ShortName as SystemShortName
				  FROM Batches
		INNER JOIN Jobs on (Jobs.id = Batches.JobId)
		INNER JOIN Systems on (jobs.Systemid = systems.id)
		WHERE CONVERT(Date, Batches.StartDate) = CONVERT(DATE, GETDATE())
		ORDER BY id DESC
		) qryBatches
		ORDER BY id ASC
END
GO
