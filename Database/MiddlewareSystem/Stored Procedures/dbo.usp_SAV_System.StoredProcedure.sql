USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_System]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SAV_System]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_System]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Decides whether to insert or edit systems table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_System] 
	-- Add the parameters for the stored procedure here
	@systemid AS int, 
	@systemShortName AS NVARCHAR(50),
	@systemAcronym AS NVARCHAR(10),
	@systemVersion AS NVARCHAR(10),
	@systemDescription AS NVARCHAR(MAX),
	@systemDisabled AS BIT,
	@systemConnName AS NVARCHAR(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		IF EXISTS (SELECT * FROM Systems WHERE id = @systemid)
	BEGIN
		UPDATE Systems SET  ShortName = @systemShortName, Acronym = @systemAcronym, Version = @systemVersion, Description = @systemDescription, Disabled = @systemDisabled, ConnectionName = @systemConnName
		WHERE id = @systemid 
	END
	ELSE
	BEGIN
		INSERT INTO Systems(ShortName, Acronym, Version, Description, Disabled, ConnectionName)
		VALUES (@systemShortName, @systemAcronym, @systemVersion,@systemDescription, @systemDisabled, @systemConnName)
	END
END
GO
