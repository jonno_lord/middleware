USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_MiddlewareStatus]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_MiddlewareStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_MiddlewareStatus]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SEL_MiddlewareStatus] AS
BEGIN

	DECLARE @Status AS VARCHAR(12)

	-- there are failed jobs	
	IF EXISTS(SELECT * FROM jobs WHERE status = 'FAILED')
	BEGIN
		SET @Status = 'RED'
		SELECT @Status as MiddlewareStatus
		RETURN
	END

	-- jobs failed in the past hour
	IF EXISTS(select * FROM Batches WHERE status = 'FAILED' AND DATEDIFF(MINUTE,startdate, GETDATE()) < 60)
	BEGIN
		SET @Status = 'ORANGE'
		SELECT @Status as MiddlewareStatus
		RETURN
	END 

	-- jobs in the past half an hour are still processing (+30 minutes
	-- to complete a job.
	IF EXISTS(SELECT * FROM Batches WHERE DATEDIFF(MINUTE,startdate, GETDATE()) BETWEEN 15 AND 60 AND enddate IS NULL AND status <> 'COMPLETED') 
	BEGIN
		SET @Status = 'YELLOW'
		SELECT @Status as MiddlewareStatus
		RETURN
	END 

	-- jobs are processing 
	IF EXISTS(SELECT * FROM jobs WHERE status = 'PROCESSING')
	BEGIN
		SET @Status = 'GREEN'
		SELECT @Status as MiddlewareStatus
		RETURN
	END

	-- nothing is happening. no action.	
	SET @Status = 'WHITE'
	SELECT @Status as MiddlewareStatus
	RETURN
	
END
GO
