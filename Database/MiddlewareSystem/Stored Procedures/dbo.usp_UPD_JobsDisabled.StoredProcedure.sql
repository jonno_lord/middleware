USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JobsDisabled]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_JobsDisabled]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JobsDisabled]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 27th September 2010
-- Description:	Updates all Jobs to disabled
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_JobsDisabled] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Jobs SET Disabled = 1
	
END
GO
