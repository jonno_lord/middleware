USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_BatchProcess]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_BatchProcess]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_BatchProcess]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 2nd August 2010 - 13:07
-- Description:	Inserts a batch into the batch log
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_BatchProcess]
	-- Add the parameters for the stored procedure here
	@BatchProcessId AS INT,
	@Status AS VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE BatchProcesses SET Status = @Status WHERE id = @BatchProcessId 
    
    IF(@Status = 'COMPLETE' OR @Status = 'FAILED')
		UPDATE BatchProcesses SET EndDate = GETDATE() WHERE id =@BatchProcessId 
	
END
GO
