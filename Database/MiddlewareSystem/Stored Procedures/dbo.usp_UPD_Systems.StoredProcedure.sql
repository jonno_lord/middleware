USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_Systems]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_Systems]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_Systems]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 28th September 2010
-- Description:	Disables Schedules by update
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_Systems] 
	-- Add the parameters for the stored procedure here
	@systemid int, 
	@systemDisabled AS BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	BEGIN
		UPDATE Systems SET Disabled =  @systemDisabled
		WHERE id = @systemid
	END
	
END
GO
