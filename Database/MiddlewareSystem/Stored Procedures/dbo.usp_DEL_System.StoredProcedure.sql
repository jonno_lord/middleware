USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_System]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_DEL_System]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_System]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Deletes all from a specified id for the Systems table
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_System]
	-- Add the parameters for the stored procedure here
	@systemid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
	
	--PRINT 'Deleting Schedules'
    DELETE Schedules
		FROM Schedules
			INNER JOIN Jobs ON Jobs.id=Schedules.Jobid
    WHERE Jobs.Systemid = @systemid 


    -- Insert statements for procedure here
    --PRINT 'Deleting Processes'
    DELETE Processes
		FROM Processes 
			INNER JOIN Jobs ON Jobs.id=Processes.Jobid
    WHERE Jobs.Systemid = @systemid 

	--PRINT 'Deleting Jobs'    
    DELETE FROM Jobs WHERE Jobs.Systemid = @systemid 
    
    --PRINT 'Deleting Systems'    
	DELETE FROM Systems WHERE id = @systemid
END
GO
