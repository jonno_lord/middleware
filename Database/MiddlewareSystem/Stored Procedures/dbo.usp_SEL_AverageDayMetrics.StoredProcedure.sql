USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_AverageDayMetrics]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_AverageDayMetrics]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_AverageDayMetrics]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 3rd November 2010
-- Description:	gets an daily average of specified metric
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_AverageDayMetrics] 
	-- Add the parameters for the stored procedure here
	@StartDate AS DATE,
	@EndDate AS DATE,
	@MetricType AS INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate) as Dated
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND(EntryDate BETWEEN
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 00:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 23:59:59'))
	
	GROUP BY CONVERT(Date, EntryDate)
	
	ORDER BY Dated
	
END

GO
