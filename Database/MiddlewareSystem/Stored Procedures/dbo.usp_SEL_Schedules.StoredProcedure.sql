USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Schedules]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_Schedules]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Schedules]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SEL_Schedules] 
	-- Add the parameters for the stored procedure here
	
	@jobid AS INTEGER = 0,
	@systemid AS INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Schedules
	INNER JOIN Jobs ON Schedules.jobid = Jobs.id
	INNER JOIN Systems ON Jobs.Systemid = Systems.id
	WHERE Jobs.id = @jobid or @jobid = 0
	AND (Systems.id = @systemid or @systemid = 0)
END
GO
