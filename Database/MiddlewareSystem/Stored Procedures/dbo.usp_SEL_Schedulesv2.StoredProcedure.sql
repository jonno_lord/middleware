USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Schedulesv2]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_Schedulesv2]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Schedulesv2]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Selects all from the schedules table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_Schedulesv2] 
	-- Add the parameters for the stored procedure here
	
	@jobid AS INTEGER = 0,
	@systemid AS INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Schedules.id, jobid, Jobs.ShortName, Systems.id AS Systemid, Schedules.Description, StartDate, StartTime, EndDate, EndTime, Frequency,Schedules.Disabled,
	
	RTRIM(CASE WHEN Monday = 1  THEN 'Mo ' ELSE '' END 
	+ 	CASE WHEN Tuesday = 1  THEN 'Tu ' ELSE '' END
	+	CASE WHEN Wednesday = 1  THEN 'We ' ELSE '' END 
	+	CASE WHEN Thursday = 1  THEN 'Ths ' ELSE '' END 
	+	CASE WHEN Friday = 1  THEN 'Fr ' ELSE '' END
	+	CASE WHEN Saturday = 1  THEN 'Sa ' ELSE '' END
	+	CASE WHEN Sunday = 1  THEN 'Su ' ELSE '' END )
	
	AS [Days]
	
	FROM Schedules
	INNER JOIN Jobs ON Schedules.jobid = Jobs.id
	INNER JOIN Systems ON Jobs.Systemid = Systems.id
	WHERE Jobs.id = @jobid or @jobid = 0
	AND (Systems.id = @systemid or @systemid = 0)
END

GO
