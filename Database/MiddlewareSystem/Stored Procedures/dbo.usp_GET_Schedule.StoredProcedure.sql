USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Schedule]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_Schedule]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Schedule]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 27th July 2010
-- Description:	Selects all from schedules by specific id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_Schedule] 
	-- Add the parameters for the stored procedure here
	@scheduleid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Schedules.*, Jobs.Systemid FROM Schedules
		INNER JOIN Jobs ON (Jobs.id = Schedules.Jobid)
		WHERE Schedules.id = @scheduleid 
END
GO
