USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_BatchLog]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_INS_BatchLog]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_BatchLog]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 2nd August 2010 - 13:07
-- Description:	Inserts a batch into the batch log
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_BatchLog]
	-- Add the parameters for the stored procedure here
	@Jobid AS INT,
	@Processid AS INT,
	@Batchid AS INT,
	@LogLevel AS INT,
	@Description AS VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO BatchLogs(Jobid, Processid, Batchid, LogLevel, Description, EntryDate) VALUES
						 (@Jobid, @Processid, @Batchid, @LogLevel, @Description, GETDATE())
	SELECT @@IDENTITY 
	
END
GO
