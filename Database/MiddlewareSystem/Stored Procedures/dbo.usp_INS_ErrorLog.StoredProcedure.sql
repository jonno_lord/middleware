USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_ErrorLog]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_INS_ErrorLog]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_ErrorLog]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_ErrorLog]
	-- Add the parameters for the stored procedure here
	@message as nvarchar(MAX),
	@InnerException as nvarchar(MAX),
	@Source as nvarchar(1024),
	@StackTrace as nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO ErrorLogs(Message, InnerException, Source, StackTrace, EntryDate)
				VALUES
						(@message, @InnerException, @Source, @StackTrace, GETDATE())
						
END
GO
