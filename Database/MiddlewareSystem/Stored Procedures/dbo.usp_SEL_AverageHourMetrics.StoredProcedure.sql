USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_AverageHourMetrics]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_AverageHourMetrics]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_AverageHourMetrics]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 2nd November 2010
-- Description:	Gets metrics based on start and end date and returns an average for each hour. Takes a metric type as parameter
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_AverageHourMetrics] 
	-- Add the parameters for the stored procedure here
	@StartDate AS DATE,
	@EndDate AS DATE,
	@MetricType AS INTEGER

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate) as Dated, '00:00:00' as StartTime, '01:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 00:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 01:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '01:00:00' as StartTime, '02:00:00'  as EndTime
	FROM Metrics
	WHERE  (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 01:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 02:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '02:00:00' as StartTime, '03:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 02:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 03:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '03:00:00' as StartTime, '04:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 03:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 04:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '04:00:00' as StartTime, '05:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 04:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 05:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '05:00:00' as StartTime, '06:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 05:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 06:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '06:00:00' as StartTime, '07:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 06:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 07:00:00')
	GROUP BY CONVERT(Date, EntryDate)		
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '07:00:00' as StartTime, '08:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 07:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 08:00:00')
	GROUP BY CONVERT(Date, EntryDate)	
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '08:00:00' as StartTime, '09:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 08:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 09:00:00')
	GROUP BY CONVERT(Date, EntryDate)			
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '09:00:00' as StartTime, '10:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 09:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 10:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '10:00:00' as StartTime, '11:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 10:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 11:00:00')
	GROUP BY CONVERT(Date, EntryDate)		
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '11:00:00' as StartTime, '12:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 11:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 12:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '12:00:00' as StartTime, '13:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 12:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 13:00:00')
	GROUP BY CONVERT(Date, EntryDate)	
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '13:00:00' as StartTime, '14:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 13:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 14:00:00')
	GROUP BY CONVERT(Date, EntryDate)			
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '14:00:00' as StartTime, '15:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 14:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 15:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '15:00:00' as StartTime, '16:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 15:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 16:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '16:00:00' as StartTime, '17:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 16:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 17:00:00')
	GROUP BY CONVERT(Date, EntryDate)				
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '17:00:00' as StartTime, '18:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 17:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 18:00:00')
	GROUP BY CONVERT(Date, EntryDate)		
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '18:00:00' as StartTime, '19:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 18:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 19:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '19:00:00' as StartTime, '20:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 19:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 20:00:00')
	GROUP BY CONVERT(Date, EntryDate)	
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '20:00:00' as StartTime, '21:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 20:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 21:00:00')
	GROUP BY CONVERT(Date, EntryDate)		
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '21:00:00' as StartTime, '22:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 21:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 22:00:00')
	GROUP BY CONVERT(Date, EntryDate)					
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '22:00:00' as StartTime, '23:00:00'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 22:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 23:00:00')
	GROUP BY CONVERT(Date, EntryDate)
UNION	
SELECT AVG(Value) AS MetricValue, CONVERT(Date, EntryDate), '23:00:00' as StartTime, '23:59:59'  as EndTime
	FROM Metrics
	WHERE (MetricTypeid = @MetricType OR @MetricType = 0)
	AND EntryDate Between 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @StartDate) + ' 23:00:00')
	AND 
	CONVERT(DATETIME, CONVERT(VARCHAR(20), @EndDate) + ' 23:59:59')
	GROUP BY CONVERT(Date, EntryDate)
	
	ORDER BY Dated, StartTime			
END

GO
