USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_SystemNameFromSystemId]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_SystemNameFromSystemId]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_SystemNameFromSystemId]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SEL_SystemNameFromSystemId]
	-- Add the parameters for the stored procedure here
	@systemid AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id FROM dbo.SystemNames WHERE SystemId = @systemid
	
END
GO
