USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[UDP_Job]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[UDP_Job]
GO
/****** Object:  StoredProcedure [dbo].[UDP_Job]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 24th September 2010
-- Description:	Updates disabled field of Job
-- =============================================
CREATE PROCEDURE [dbo].[UDP_Job]
	-- Add the parameters for the stored procedure here
	@jobid AS INTEGER,
	@jobDisabled AS BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Jobs SET Disabled = @jobDisabled
	WHERE  id = @jobid
END
GO
