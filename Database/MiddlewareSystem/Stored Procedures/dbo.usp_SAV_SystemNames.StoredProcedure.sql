USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_SystemNames]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SAV_SystemNames]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_SystemNames]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 19th August 2010
-- Description:	If parameter is supplied then an update is made, if not an insert is made to the SystemNames table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_SystemNames] 
	-- Add the parameters for the stored procedure here
	@systemNameid int, 
	@systemid int,
	@systemName AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM SystemNames WHERE id = @systemNameid)
	BEGIN
		UPDATE SystemNames SET systemName = @systemName, systemid = @systemid
		WHERE id = @systemNameid 
	END
	ELSE
	BEGIN
		INSERT INTO SystemNames (SystemName ,systemid)
		VALUES (@systemName, @systemid)
	END
END
GO
