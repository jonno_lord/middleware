USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ErrorLogs]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_ErrorLogs]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_ErrorLogs]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 12th November 2010
-- Description:	Creates a copy of the error log 
--table and removes the logs from ErrorLogs which are less than 
--a month old and places them in it. Then truncates the original,
--removing old data, then moves the recent data back, dropping
--the new table
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_ErrorLogs]
	-- Add the parameters for the stored procedure here
	
@batchNumber AS INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
BEGIN TRY

	BEGIN TRANSACTION
	
    -- Insert statements for procedure here
	CREATE TABLE [dbo].[#TempErrorLogs](
	[id] [int] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[InnerException] [nvarchar](max) NOT NULL,
	[Source] [nvarchar](1024) NOT NULL,
	[StackTrace] [nvarchar](max) NOT NULL,
	[EntryDate] [datetime] NULL)
	
	INSERT INTO #TempErrorLogs (id, Message, InnerException, Source, StackTrace, EntryDate)  SELECT id, Message, InnerException, Source, StackTrace, EntryDate FROM ErrorLogs 
	WHERE DATEDIFF(MONTH, EntryDate, GETDATE()) < 1
	
	TRUNCATE TABLE ErrorLogs
	
	SET IDENTITY_INSERT ErrorLogs ON
	
	INSERT INTO ErrorLogs (id, Message, InnerException, Source, StackTrace, EntryDate)   SELECT id, Message, InnerException, Source, StackTrace, EntryDate FROM #TempErrorLogs
	
	SET IDENTITY_INSERT ErrorLogs OFF
	
	COMMIT TRANSACTION
	
END TRY

BEGIN CATCH
	
	DECLARE @ErrorMessage NVARCHAR(4000);
	SELECT @ErrorMessage = ERROR_MESSAGE();

	ROLLBACK TRANSACTION
	
	RAISERROR (@ErrorMessage, 16, 1);

END CATCH

	
END

GO
