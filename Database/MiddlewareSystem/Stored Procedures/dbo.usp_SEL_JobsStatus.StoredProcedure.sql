USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JobsStatus]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_JobsStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JobsStatus]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 21st July 2010
-- Description:	Selects all data from the Jobs table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_JobsStatus] 
	-- Add the parameters for the stored procedure here
	@status AS VARCHAR(50) = '',
	@systemid AS INTEGER = 0
	AS
	BEGIN
	

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Systems.ShortName as Systemname, Systems.Disabled, Jobs.id,Jobs.Systemid,Jobs.Sequence,Jobs.ShortName,Jobs.Description,Jobs.LastExecuted,Jobs.Disabled,Jobs.FailureNotificationSent, 
	CASE WHEN Systems.Disabled = 1 OR Jobs.Disabled = 1 THEN 'DISABLED' ELSE Status END AS Status FROM Jobs
		INNER JOIN Systems ON Jobs.Systemid = Systems.id
		WHERE (Systems.id = @systemid or @systemid = 0)
		AND (Status = @status or @status = '')
		ORDER BY Systems.ShortName, Jobs.ShortName
			
END
GO
