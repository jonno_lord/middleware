USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchNumber]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_BatchNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchNumber]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 14th March 2011
-- Description:	Finds maximum batch number  of a specified job
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_BatchNumber] 
	-- Add the parameters for the stored procedure here
	@jobid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT MAX(id) AS batchNo FROM Batches
	WHERE JobId = @jobid
END
GO
