USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Batches]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_Batches]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Batches]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author, Brenda Patterson>
-- Create date: <16/08/2010>
-- Description:	<Selects Batches based on the date selected by the user>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_Batches]
	-- Add the parameters for the stored procedure here
	   @Date as varchar(10),
	   @SystemId as int = 0,
	   @JobId as int = 0,
	   @BatchStatus as varchar(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT batches.Status, batches.id, Systems.ShortName as SystemName, jobs.ShortName AS Job, StartDate,EndDate,  CONVERT (VARCHAR(10), DATEDIFF(SECOND, StartDate, EndDate)) AS DurationSeconds FROM Batches
		INNER JOIN Jobs on (Jobs.id = Batches.JobId)
		INNER JOIN Systems on (jobs.Systemid = systems.id)
	WHERE CONVERT(CHAR(10),StartDate,120) = @Date
		AND (Jobs.id = @JobId OR @JobId = 0)
		AND (Systems.id = @SystemId OR @SystemId = 0)
		AND (Batches.Status = @Batchstatus or @BatchStatus = '')
		
	ORDER BY StartDate DESC
		
		

	
END
GO
