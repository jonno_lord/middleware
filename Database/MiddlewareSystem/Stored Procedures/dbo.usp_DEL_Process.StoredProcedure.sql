USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_Process]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_DEL_Process]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_Process]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Deletes specified process from the table
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_Process] 
	-- Add the parameters for the stored procedure here
	@processid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Processes
	WHERE id = @processid
END
GO
