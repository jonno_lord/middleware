USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_Job]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SAV_Job]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_Job]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 23rd July 2010
-- Description:	Decides whether to add or insert to the jobs table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_Job] 
	-- Add the parameters for the stored procedure here
	@jobid AS int,
	@jobSystemid AS int, 
	@jobSequence AS int,
	@jobShortName AS NVARCHAR(50),
	@jobDescription AS NVARCHAR(MAX),
	@jobStatus AS NVARCHAR(50),
	@jobLastExecuted AS DATETIME,
	@jobDisabled AS BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM Jobs WHERE id = @jobid)
	BEGIN
		UPDATE Jobs SET Systemid = @jobSystemid, Sequence = @jobSequence, ShortName = @jobShortName, Description = @jobDescription, Status = @jobStatus, LastExecuted = @jobLastExecuted, Disabled = @jobDisabled
		WHERE id = @jobid 
	END
	ELSE
	BEGIN
		INSERT INTO Jobs(Systemid, Sequence, ShortName, Description, Status, LastExecuted, Disabled)
		VALUES (@jobSystemid, @jobSequence, @jobShortName, @jobDescription, @jobStatus, @jobLastExecuted, @jobDisabled)
	END
END
GO
