USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BatchLogs]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_BatchLogs]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_BatchLogs]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 20th August 2010
-- Description:	Retreives all batch logs
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_BatchLogs] 
	-- Add the parameters for the stored procedure here
		@batchid AS INTEGER,
		@batchLogLevel AS INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT B.*, ISNULL(P.ClassName, '') as ClassName FROM BatchLogs as B
		LEFT JOIN Processes P on p.id = b.Processid 
	WHERE Batchid = @batchid
	AND (LogLevel < @batchLogLevel or @batchLogLevel = 0)
	
	ORDER BY EntryDate DESC 
END
GO
