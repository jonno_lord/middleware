USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JobsFailedBatches]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_JobsFailedBatches]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JobsFailedBatches]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 29th March 2011
-- Description:	Selects jobs which have had a failed batch for a given date
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_JobsFailedBatches] 
	-- Add the parameters for the stored procedure here
	@startDate AS DATETIME,
	@endDate AS DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT DISTINCT Batches.Jobid, Systems.ShortName AS Systemname, Systems.Disabled, Jobs.id, Jobs.Systemid,Jobs.Sequence,Jobs.ShortName,Jobs.Description,Jobs.LastExecuted,Jobs.Disabled AS JobDisabled,Jobs.FailureNotificationSent, Jobs.Status FROM Batches
    INNER JOIN Jobs ON Jobs.id = Batches.Jobid
    INNER JOIN Systems ON Jobs.Systemid = Systems.id
    WHERE (Batches.Status = 'FAILED' ) 
    AND (CAST(FLOOR(CAST(Batches.StartDate AS FLOAT))AS DATETIME) = @startDate)
    AND (CAST(FLOOR(CAST(Batches.EndDate AS FLOAT))AS DATETIME) = @startDate)
	ORDER BY Jobs.ShortName
END
GO
