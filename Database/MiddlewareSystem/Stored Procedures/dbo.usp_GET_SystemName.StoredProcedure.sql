USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_SystemName]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_SystemName]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_SystemName]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 19th August 2010
-- Description:	Gets a System Name based on id parameter
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_SystemName] 
	-- Add the parameters for the stored procedure here
	@systemNameid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM SystemNames
	WHERE SystemNames.id = @systemNameid
	
END
GO
