USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_BatchLogs]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_BatchLogs]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_BatchLogs]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 12th November 2010
-- Description:	deletes all non critical batch logs when they are two weeks old and critical batch logs when they are a month old
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_BatchLogs] 
	-- Add the parameters for the stored procedure here
	@batchNumber AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	BEGIN TRY
    
    BEGIN TRANSACTION
   

    CREATE TABLE [dbo].[#TempBatchLogs](
	[id] [int] NOT NULL,
	[Batchid] [int] NOT NULL,
	[Jobid] [int] NOT NULL,
	[Processid] [int] NOT NULL,
	[LogLevel] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[EntryDate] [datetime] NULL)
	
	INSERT INTO #TempBatchLogs (id, Batchid, Jobid, Processid, LogLevel, Description, EntryDate) SELECT id,Batchid, Jobid, Processid, LogLevel, Description, EntryDate FROM BatchLogs
	WHERE (LogLevel = 10 OR LogLevel = 20)
	AND DATEDIFF(MONTH, EntryDate, GETDATE()) < 1
	
	INSERT INTO #TempBatchLogs(id, BatchId, Jobid, Processid, LogLevel, Description, EntryDate) SELECT id, Batchid, Jobid, Processid, LogLevel, Description, EntryDate FROM BatchLogs
	WHERE LogLevel = 300
	AND DATEDIFF(DAY, EntryDate, GETDATE()) < 14
	
	TRUNCATE TABLE BatchLogs
	
	SET IDENTITY_INSERT BatchLogs ON
	
	INSERT INTO BatchLogs (id, batchId, Jobid, Processid, LogLevel, Description, EntryDate) SELECT id, Batchid, Jobid, Processid, LogLevel, Description, EntryDate FROM #TempBatchLogs
	
	SET IDENTITY_INSERT BatchLogs OFF

	COMMIT TRANSACTION
	
END TRY

BEGIN CATCH

	DECLARE @ErrorMessage NVARCHAR(4000);
	SELECT @ErrorMessage = ERROR_MESSAGE();

	ROLLBACK TRANSACTION
	
	RAISERROR (@ErrorMessage, 16, 1);
	
END CATCH
	
END

GO
