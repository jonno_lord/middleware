USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_LastFailedBatches]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_LastFailedBatches]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_LastFailedBatches]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 31st August
-- Description:	Returns last 100 batches that failed
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_LastFailedBatches]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 10 batches.id, 
				  jobs.ShortName AS ShortName,
				  systems.ShortName as SystemShortName
				  FROM Batches
		INNER JOIN Jobs on (Jobs.id = Batches.JobId)
		INNER JOIN Systems on (jobs.Systemid = systems.id)
	WHERE batches.Status = 'FAILED' AND 
		CONVERT(Date, EndDate) = CONVERT(Date, GETDATE())
		ORDER BY id ASC	
END
GO
