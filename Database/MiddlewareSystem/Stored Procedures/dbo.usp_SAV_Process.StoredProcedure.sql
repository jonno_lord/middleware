USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_Process]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SAV_Process]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_Process]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Decides whether to add or insert into Processes table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_Process] 
	-- Add the parameters for the stored procedure here
	@processid AS int,
	@processJobid AS int,
	@processSequence AS int,
	@processAssemblyName AS NVARCHAR(512),
	@processClassName AS NVARCHAR(512),
	@processShortName AS NVARCHAR(50),
	@processTypeid AS int,
	@processDescription AS NVARCHAR(MAX),
	@processStatus AS NVARCHAR(50),
	@processLastExecuted AS DATETIME,
	@processDisabled AS BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM Processes WHERE id = @processid)
	BEGIN
		UPDATE Processes SET Jobid = @processJobid, Sequence = @processSequence, AssemblyName = @processAssemblyName, ClassName = @processClassName, ShortName = @processShortName, ProcessTypeid = @processTypeid, Description = @processDescription, Status = @processStatus, LastExecuted = @processLastExecuted, Disabled = @processDisabled
		WHERE id = @processid 
	END
	ELSE
	BEGIN
		INSERT INTO Processes(Jobid, Sequence, AssemblyName, ClassName, ShortName, ProcessTypeid, Description, Status, LastExecuted, Disabled)
		VALUES (@processJobid, @processSequence, @processAssemblyName, @processClassName, @processShortName, @processTypeid, @processDescription, @processStatus, @processLastExecuted, @processDisabled)
	END
END
GO
