USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JobsByDate]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_JobsByDate]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_JobsByDate]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Brenda Patterson>
-- Create date: <Create Date,,16/03/2011>
-- Description:	<Description,,This stored procedure will select all Jobs from the Job table where the
                              --date is equal to the date parameter passed in>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_JobsByDate] 
	-- Add the parameters for the stored procedure here
	@UserDate AS Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, ShortName, Status FROM Jobs
	WHERE CONVERT(Char(10),LastExecuted,120) = @UserDate
END
GO
