USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_Process]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_Process]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_Process]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 24th Septmber 2010
-- Description:	Updates whether a Process is disabled or not
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_Process] 
	-- Add the parameters for the stored procedure here
	@processesid int,
	@processesDisabled AS BIT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN
		UPDATE Processes SET Disabled =  @processesDisabled
		WHERE id = @processesid 
	END
	
END
GO
