USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchDuration]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_BatchDuration]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchDuration]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 23rd March 2011
-- Description:	gets duration of each batch as well as duration and start and end of periods of inactivity
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_BatchDuration] 
	-- Add the parameters for the stored procedure here
	
	@jobid AS INT,
	@startDate AS DATETIME,
	@endDate AS DATETIME,
	
	@jobidBatch AS INT = 0,
	@startDateBatch  AS DATETIME = NULL,
	@endDateBatch  AS DATETIME = NULL,
	@duration AS INT = 0,
	@status AS NVARCHAR(50) = '',
	@batchNo AS INT = 0,
	@jobName AS NVARCHAR(50) = '',
	@batchCount AS INT = 0,
	@currentBatchCount AS INT = 0,
	
	@inactiveDuration AS INT = 0,
	@inactiveEndDate AS DATETIME = NULL,
	@durationEndDate AS DATETIME = NULL,
	@inactiveStartDate AS DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT @jobName = ShortName FROM Jobs WHERE id = @jobid
    
    SELECT @batchCount = COUNT(Batches.id) FROM Batches
    INNER JOIN Jobs on jobs.id = Batches.JobId
    WHERE Batches.JobId = @jobid AND CAST(FLOOR(CAST(StartDate AS FLOAT))AS DATETIME) = @startDate AND CAST(FLOOR(CAST(EndDate AS FLOAT))AS DATETIME) = @EndDate
    
    DECLARE batchCursor SCROLL CURSOR FOR
    SELECT DATEDIFF(SECOND, StartDate, EndDate) AS Duration, StartDate, EndDate, [Batches].id, Batches.[Status], Jobs.ShortName, Jobid
    FROM Batches
    INNER JOIN Jobs on jobs.id = Batches.JobId
    WHERE Batches.JobId = @jobid AND CAST(FLOOR(CAST(StartDate AS FLOAT))AS DATETIME) = @startDate AND CAST(FLOOR(CAST(EndDate AS FLOAT))AS DATETIME) = @EndDate
    ORDER BY StartDate ASC
    
    OPEN batchCursor
    
    --create a temporary table to store periods of time where batch isn't run
    
    CREATE TABLE #inactiveBatchDuration(
	Duration INT,
	StartDate DATETIME,
	EndDate DATETIME,
	Status VARCHAR(50),
	JobName VARCHAR(50),
	Jobid INT)
    
    --if no batches have run for supplied date
    IF NOT EXISTS(SELECT DATEDIFF(SECOND, StartDate, EndDate) AS Duration, StartDate, EndDate, [Batches].id, Batches.[Status], Jobs.ShortName, Jobid
    FROM Batches
    INNER JOIN Jobs on jobs.id = Batches.JobId
    WHERE Batches.JobId = @jobid AND CAST(FLOOR(CAST(StartDate AS FLOAT))AS DATETIME) = @startDate AND CAST(FLOOR(CAST(EndDate AS FLOAT))AS DATETIME) = @EndDate)
    
    BEGIN		
		INSERT INTO #inactiveBatchDuration (Duration, StartDate, EndDate, [Status], JobName, Jobid) VALUES(86399, CAST(CAST(FLOOR(CAST(@StartDate AS FLOAT))AS DATETIME) + ' 00:00:00' AS DATETIME), CAST(CAST(FLOOR(CAST(@endDate AS FLOAT))AS DATETIME) + ' 23:59:59' AS DATETIME), NULL, @jobName, @jobid)
    END
    
    --get first batch ran
    FETCH FIRST FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
	SET @currentBatchCount = @currentBatchCount + 1
	
	--calculate duration between beginning of day (00:00:00 and first batch ran
	SET @inactiveDuration = DATEDIFF(SECOND, CAST(FLOOR(CAST(@startDateBatch AS FLOAT))AS DATETIME) + ' 00:00:00', @startDateBatch)
	
	--if batch not ran at midnight create an inactive entry to temporary table
	IF @startDateBatch <> CAST(FLOOR(CAST(@startDateBatch AS FLOAT))AS DATETIME) + ' 00:00:00'
	BEGIN
		--set start date as a second less than start of first batch
		SET @inactiveEndDate = DATEADD(SECOND, -1, @startDateBatch)
		
		INSERT INTO #inactiveBatchDuration (Duration, StartDate, EndDate, [Status], JobName, Jobid) VALUES(@inactiveDuration, CAST(CAST(FLOOR(CAST(@StartDateBatch AS FLOAT))AS DATETIME) + ' 00:00:00' AS DATETIME), @inactiveEndDate, NULL, @jobName, @jobidBatch)
    END
    -- Insert for first batch gap between second batch
    
    
    IF @@FETCH_STATUS = 0 AND @startDateBatch <> CAST(FLOOR(CAST(@startDateBatch AS FLOAT))AS DATETIME) + ' 00:00:00'
    BEGIN
    		--set inactive start date to 1 second later than end of current batch
		SET @inactiveStartDate = DATEADD(SECOND, 1, @endDateBatch)
		
		FETCH NEXT FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
		
		--deals with jobs of less than a second
		IF DATEDIFF(SECOND, @startDateBatch, DATEADD(SECOND, -1, @inactiveStartDate)) > 1
		BEGIN
		--find the duration end between first and next batch
		SET @durationEndDate = DATEADD(SECOND, -1, @startDateBatch)
		
		FETCH FIRST FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
		
		--calculate duration between end of last batch and start of current row's batch
		SET @inactiveDuration = DATEDIFF(SECOND, @startDateBatch, @durationEndDate)
		
		INSERT INTO #inactiveBatchDuration (Duration, StartDate, EndDate, [Status], JobName, Jobid) VALUES(@inactiveDuration, @inactiveStartDate, @durationEndDate, NULL, @jobName, @jobidBatch)			
		
		END
		ELSE
			BEGIN
				--sets cursor back to beginning to not insert for gap of less than 1 second
				FETCH FIRST FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
			END	
	END
	
    --checks if there are more rows (batches) to fetch
     WHILE @@FETCH_STATUS = 0
     BEGIN
		--sets to end date of previous batch
		SET @durationEndDate = DATEADD(SECOND, -1, @endDateBatch)
		
		FETCH NEXT FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
		SET @currentBatchCount = @currentBatchCount + 1
		
			--only calculate and insert if not the last row
			IF (@@FETCH_STATUS = 0) AND (@currentBatchCount <> @batchCount) 
			BEGIN
				--set inactive start date to 1 second later than end of current batch
				SET @inactiveStartDate = DATEADD(SECOND, 1, @endDateBatch)
				
				--check start of next batch to get end of inactivity period
				FETCH NEXT FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
				
				--set inactive end time to a second less than this next batches start
				SET @inactiveEndDate = DATEADD(SECOND, -1, @startDateBatch)
				
				--go back to previous row (batch)
				FETCH PRIOR FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
				--insert to temporary table an inactive period
				
				--calculate duration between end of last batch and start of current row's batch
				SET @inactiveDuration = DATEDIFF(SECOND, @inactiveStartDate, @inactiveEndDate)
				
				INSERT INTO #inactiveBatchDuration (Duration, StartDate, EndDate, [Status], JobName, Jobid) VALUES(@inactiveDuration, @inactiveStartDate, @inactiveEndDate, NULL, @jobName, @jobidBatch)
			END		
	 END
	 
	-- fetch last batch
	FETCH LAST FROM batchCursor INTO @duration, @startDateBatch, @endDateBatch, @batchNo, @status, @jobName, @jobidBatch
	
	-- calculate duration between end of last batch and end of day 23:59:59
	SET @inactiveDuration = DATEDIFF(SECOND, @endDateBatch, CAST(FLOOR(CAST(@endDateBatch AS FLOAT))AS DATETIME) + ' 23:59:59')
	
	IF @endDateBatch <> CAST(FLOOR(CAST(@endDateBatch AS FLOAT))AS DATETIME) + ' 23:59:59'
	BEGIN
		-- calculate start of inactive period from end of last batch
		SET @inactiveStartDate = DATEADD(SECOND, 1, @endDateBatch)
		
		--insert inactive entry for end of day
		INSERT INTO #inactiveBatchDuration (Duration, StartDate, EndDate, [Status], JobName, Jobid) VALUES(@inactiveDuration, @inactiveStartDate, CAST(CAST(FLOOR(CAST(@endDateBatch AS FLOAT))AS DATETIME) + ' 23:59:59' AS DATETIME), NULL, @jobName, @jobidBatch)
	END
	
    CLOSE batchCursor

	DEALLOCATE batchCursor
	
	--select all batches and time between batches and order by date
	SELECT * FROM (
	SELECT DATEDIFF(SECOND, StartDate, EndDate) AS Duration, StartDate, EndDate, [Batches].id AS Batchid, dbo.fn_ProperCase(Batches.[Status]) as Status, Jobs.ShortName AS JobName, Jobid
    FROM Batches
    INNER JOIN Jobs on jobs.id = Batches.JobId
    WHERE Batches.JobId = @jobid AND CAST(FLOOR(CAST(StartDate AS FLOAT))AS DATETIME) = @startDate AND CAST(FLOOR(CAST(EndDate AS FLOAT))AS DATETIME) = @EndDate
	UNION
    SELECT Duration, StartDate, EndDate, NULL AS Batchid, ISNULL('Nonactive', Status) as Status, JobName, Jobid FROM #inactiveBatchDuration
   
    ) AS QRYUNION ORDER BY StartDate 
END

GO
