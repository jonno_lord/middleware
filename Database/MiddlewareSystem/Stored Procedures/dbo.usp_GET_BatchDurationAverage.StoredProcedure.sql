USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchDurationAverage]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_BatchDurationAverage]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchDurationAverage]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 21st March 2011
-- Description:	Calculates Average Duration of Batches
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_BatchDurationAverage]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AVG(DATEDIFF(SECOND, StartDate, EndDate)) AS Duration FROM Batches
END
GO
