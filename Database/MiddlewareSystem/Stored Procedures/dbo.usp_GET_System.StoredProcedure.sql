USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_System]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_System]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_System]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Gets a specific system
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_System] 
	-- Add the parameters for the stored procedure here
	@systemid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Systems WHERE id = @systemid
END
GO
