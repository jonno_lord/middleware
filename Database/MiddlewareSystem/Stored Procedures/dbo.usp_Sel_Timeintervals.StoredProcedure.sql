USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_Sel_Timeintervals]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_Sel_Timeintervals]
GO
/****** Object:  StoredProcedure [dbo].[usp_Sel_Timeintervals]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,BPatterson>
-- Create date: <Create Date,,25/03/2011>
-- Description:	<Description,,Get the time intervals for the Job Duration graph's header repeater>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sel_Timeintervals] 
	-- Add the parameters for the stored procedure here

	
	
AS
BEGIN
	--Declare @DaySec int = 86399
	--Declare @DayMin int = 1440
	Declare @Sec int

	CREATE Table #tempTimeIntervals(
	HeaderTime nvarchar(50),
	Pixels int)
	
	set @Sec = 0

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Start a loop that will keep incrementing @Sec + 60 until @Sec = 86399
	--Each time the value of @Min should be displayed under the appropriate minute header.
	--Time  Pixels  
	--0.00  0
	--0.01  60  
	--0.02  120
 
	
		--While the number of seconds is less that the complete number of seconds in a day
		While (@Sec < 86340)
		Begin

			--increment the number of seconds which is equivalent to the number if pixels
			Set @Sec = @Sec + 60
		
		   --Display the data under the appropriate headers
		   --Select CONVERT(VARCHAR(12), DATEADD(SECOND, @Sec,  CONVERT(DATETIME, '1900-01-01 00:00:00')), 108) as HeaderTime, @Sec as Pixels
		Insert Into #tempTimeIntervals
		Values (CONVERT(VARCHAR(12), DATEADD(SECOND, @Sec,  CONVERT(DATETIME, '1900-01-01 00:00:00')), 108), @Sec)
		
		End	
		
		Select * From #tempTimeIntervals
		
		
       


			
			
			
		

		


		   

	
	
	

	

	
END
GO
