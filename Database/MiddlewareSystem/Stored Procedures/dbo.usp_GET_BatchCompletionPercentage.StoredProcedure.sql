USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchCompletionPercentage]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_BatchCompletionPercentage]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_BatchCompletionPercentage]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_BatchCompletionPercentage]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT COUNT(id) AS Completed FROM Batches 
		WHERE Status = 'Completed'
		AND EndDate >= Convert(datetime, Convert(varchar, GetDate(), 101))
	SELECT COUNT(id) AS Total FROM Batches 
	 WHERE EndDate >= Convert(datetime, Convert(varchar, GetDate(), 101))
	 
	
END
GO
