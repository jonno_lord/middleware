USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Processes]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_Processes]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_Processes]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th July 2010
-- Description:	Selects all the data from the Processes table
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_Processes]
	-- Add the parameters for the stored procedure here
	@status AS VARCHAR(50) = '',
	@jobid AS INTEGER = 0,
	@processTypeid AS INTEGER = 0,
	@systemid AS INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Jobs.ShortName As JobName, Processes.AssemblyName, Processes.ClassName, Processes.Description, Processes.Disabled, Processes.Jobid, Processes.LastExecuted, Processes.ProcessTypeid, Processes.ShortName, Processes.Status, Processes.id, ProcessTypes.id, ProcessTypes.ProcessType FROM Processes
		INNER JOIN ProcessTypes ON Processes.ProcessTypeid = ProcessTypes.id
		INNER JOIN Jobs ON Processes.jobid = Jobs.id
		WHERE (Jobs.id = @jobid or @jobid = 0)
		AND (Processes.ProcessTypeid = @processTypeid or @processTypeid = 0)
		AND (Processes.Status = @status or @status = '')
		ORDER BY Processes.Sequence 

END
GO
