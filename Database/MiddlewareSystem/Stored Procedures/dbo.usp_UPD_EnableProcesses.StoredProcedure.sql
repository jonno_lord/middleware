USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_EnableProcesses]    Script Date: 10/04/2018 10:52:40 ******/
DROP PROCEDURE [dbo].[usp_UPD_EnableProcesses]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_EnableProcesses]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 26th January 2011
-- Description:	Enables all Processes
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_EnableProcesses] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Processes SET Disabled = 0
END
GO
