USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ErrorLogs]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_ErrorLogs]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ErrorLogs]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brenda Patterson
-- Create date: 6th August 2010
-- Description:	Selects Error Logs for a particular date
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ErrorLogs]
	-- Add the parameters for the stored procedure here
	@Date AS DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT EntryDate, Message, StackTrace FROM ErrorLogs
	WHERE CONVERT(DATE, EntryDate) = CONVERT(DATE, @Date)
	ORDER BY EntryDate DESC
END
GO
