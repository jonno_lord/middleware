USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_SystemName]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_DEL_SystemName]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_SystemName]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 19th August 2010
-- Description:	Deletes all from the System Names table
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_SystemName] 
	-- Add the parameters for the stored procedure here
	@systemNameid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	DELETE FROM SystemNames
    WHERE id = @systemNameid
END
GO
