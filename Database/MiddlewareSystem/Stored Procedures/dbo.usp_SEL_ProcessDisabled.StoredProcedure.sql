USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ProcessDisabled]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_ProcessDisabled]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ProcessDisabled]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st December 2010
-- Description:	Selects all processes that are not disabled
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ProcessDisabled] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT Processes.Disabled, Processes.AssemblyName, Processes.ClassName FROM Processes
		inner join Jobs on Processes.Jobid = Jobs.id
		inner join Systems on Jobs.Systemid = Systems.id
	WHERE Processes.Disabled = 0 and Jobs.Disabled = 0 and Systems.Disabled = 0

END
GO
