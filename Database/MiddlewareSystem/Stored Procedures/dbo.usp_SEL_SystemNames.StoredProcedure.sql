USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_SystemNames]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_SEL_SystemNames]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_SystemNames]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 19th August 2010
-- Description:	Selects all from the SystemNames table with an innter join to Systems
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_SystemNames] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM SystemNames
	INNER JOIN Systems ON SystemNames.SystemId = Systems.id
	
END
GO
