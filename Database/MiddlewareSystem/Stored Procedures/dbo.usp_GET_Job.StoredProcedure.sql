USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Job]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_Job]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_Job]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: July 22nd 2010
-- Description:	Obtains data in a specific row based on a query string, for editing through a form
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_Job]
	-- Add the parameters for the stored procedure here
	@jobid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Jobs WHERE id = @jobid
END
GO
