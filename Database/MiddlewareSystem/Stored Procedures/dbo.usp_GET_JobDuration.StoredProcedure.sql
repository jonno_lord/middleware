USE [MiddlewareSystem]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_JobDuration]    Script Date: 10/04/2018 10:52:41 ******/
DROP PROCEDURE [dbo].[usp_GET_JobDuration]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_JobDuration]    Script Date: 10/04/2018 10:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 15th November 2010
-- Description:	gets the duration of each job
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_JobDuration] 
	-- Add the parameters for the stored procedure here
	
	@batchNumber AS INT,
	@jobid AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DATEDIFF(SECOND, StartDate, EndDate) AS Value FROM Batches
	WHERE JobId = @jobid
END
GO
