USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TranslatedCompanyNumber]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_TranslatedCompanyNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_TranslatedCompanyNumber]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jonno Lord	
-- Create date: 24th October 2011
-- Description:	Updates this and that
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_TranslatedCompanyNumber]

AS
BEGIN

--Update company number on F03B11
UPDATE F3 
SET F3.VJCO = MCCO, F3.VJKCO = MCCO
FROM MiddlewareJDE.dbo.F0006 F6 
INNER JOIN F03B11Z1 F3 ON F6.MCMCU = F3.VJMCU
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE AR.stage = 2

--Update currency mode on F03B11
UPDATE F3 
SET F3.VJCRRM = CASE WHEN CCCRCD <> VJCRCD THEN 'F' ELSE 'D' END   
FROM MiddlewareJDE.dbo.F0010 F00  
INNER JOIN F03B11Z1 F3  ON F00.CCCO = F3.VJCO
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE AR.stage = 2

--Update company number and currency mode on F0911
UPDATE F9
SET VNKCO = VJCO, VNCO = VJCO, VNCRRM = VJCRRM, VNLT = CASE VJCRRM WHEN  'D' THEN 'AA' WHEN 'F' THEN 'CA' END
FROM F03B11Z1 F3 
INNER JOIN F0911Z1 F9 ON F3.VJEDTN = F9.VNEDTN AND F3.VJAN8 = F9.VNAN8 AND F3.VJEDBT = F9.VNEDBT
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE AR.stage = 2


--Set domestic amounts to 0 for foreign transactions
UPDATE F3
SET VJAG = 0.0, VJAAP = 0.0, VJATXA = 0.0, VJSTAM = 0.0
FROM F03B11Z1 F3
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE VJCRRM = 'F' and VJEDUS = 'MW_EFORCE'
AND AR.stage = 2

--Set foreign amounts to 0 for domestic transactions
UPDATE F3
SET VJACR = 0.0,VJFAP = 0.0,VJCTXA = 0.0,VJCTAM = 0.0
FROM F03B11Z1 F3
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE VJCRRM = 'D' and VJEDUS = 'MW_EFORCE'
AND AR.stage = 2

--Set GL foreign distribution amount to 0 for domestic transactions
UPDATE F9
SET  VNAA = 0, VNSTAM = 0
FROM F03B11Z1 F3 
INNER JOIN F0911Z1 F9 ON F3.VJEDTN = F9.VNEDTN AND F3.VJAN8 = F9.VNAN8 AND F3.VJEDBT = F9.VNEDBT
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE F3.VJCRRM = 'F' and F3.VJEDUS = 'MW_EFORCE'
AND AR.stage = 2

UPDATE F9
SET  VNACR = 0, VNCTAM = 0
FROM F03B11Z1 F3 
INNER JOIN F0911Z1 F9 ON F3.VJEDTN = F9.VNEDTN AND F3.VJAN8 = F9.VNAN8 AND F3.VJEDBT = F9.VNEDBT
INNER JOIN AccountsReceivable AR on f3.Id = AR.F03b11z1id
WHERE F3.VJCRRM = 'D' and F3.VJEDUS = 'MW_EFORCE'
AND AR.stage = 2

END




GO
