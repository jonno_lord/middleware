USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PersonId]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_GET_PersonId]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_PersonId]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_PersonId]
	-- Add the parameters for the stored procedure here
	@codId AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @retValue AS BIT

    -- Insert statements for procedure here
	IF EXISTS 
	(SELECT * FROM Customers WHERE CodId = @codId)
	BEGIN
		SET @retValue = 1
	END
	ELSE
	BEGIN 
		SET @retValue = 0
	END

	RETURN @retValue
END


GO
