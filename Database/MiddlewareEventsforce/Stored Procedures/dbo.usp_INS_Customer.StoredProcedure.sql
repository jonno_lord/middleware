USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Customer]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_Customer]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Customer]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_Customer]
(
    @codID AS varchar(50),
    @firstname AS varchar(50),
    @lastname AS varchar(50),
    @salutation AS varchar(50),
    @addressLine1 AS varchar(50),
    @addressLine2 AS varchar(50),
    @addressLine3 AS varchar(50),
	@eventName as varchar(250),
	@jobTitle as varchar(50),
	@eventStartDate as varchar(50),
	@businessUnit as varchar(10),
	@paymentMethod as varchar(50),
    @postcode AS varchar(50),
    @country AS varchar(50),
    @company AS varchar(100),
    @email AS varchar(500),
    @telephone AS varchar(50),
    @town AS varchar(250),
	@purchaseStatus AS varchar(20),
	@grossPrice as decimal(18,2),
	@currencyCode as varchar(10),
	@county as varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM Customers WHERE CodId = @codID)
	BEGIN

		DECLARE @paymentTerms AS VARCHAR(3)
		DECLARE @creditLimit AS DECIMAL(10,2)
		DECLARE @creditController AS VARCHAR(3)
		
		SET @paymentTerms = 'N30'
		SET @creditLimit = 25000.00
		SET @creditController = 'ECC' 

		INSERT INTO Customers(CodId,Country,Town,Telephone_number,email_address,Postcode,Company_Name,Address_1,
		Address_2,Address_3,Event_Name,Title,Event_Start_Date,Business_Unit,Payment_Method,First_Name,Last_Name,Purchase_Status,
		Gross_Price_Sterling,Payment_Terms,Negotiation_gross_price,Credit_Limit,Credit_Controller_Initials,Credit_Controller_Login_Name,Negotiation_Currency,County)

		SELECT @codId,@country,@town,@telephone,@email,@postcode,@company,@addressLine1,@addressLine2,
		@addressLine3,@eventName,@jobTitle,@eventStartDate,@businessUnit,@paymentMethod,@firstname,@lastname,
		@purchaseStatus,@grossPrice,@paymentTerms,@grossPrice,@creditLimit,@creditController,@creditController,@currencyCode,@county

	END

	--Set Batch Number
	DECLARE @Batch AS INT
	SELECT @Batch = CustomerBatchNumber FROM dbo.EventsForce_Guids

	UPDATE dbo.Customers
	SET Batch_Number = @Batch
	WHERE SourceToMiddleware IS NULL

	UPDATE dbo.Customers
	SET Batch_Number = @Batch
	WHERE SourceToMiddleware IS NULL

	UPDATE dbo.EventsForce_Guids
	SET CustomerBatchNumber += 1

	SELECT @Batch as BatchNumber

	-- Country Codes
	UPDATE ctd 
	SET ctd.Country_Code = ec.countryCode 
	FROM dbo.Customers ctd 
	INNER JOIN dbo.EventsForce_Country ec 
	ON ctd.Country = ec.Country 
	AND ctd.urn_number IS NULL AND ctd.SourceToMiddleware IS NULL

	-- URN
	EXEC dbo.prc_GenerateURN @Batch

	UPDATE dbo.Customers
	SET  Stage = 1, SourceToMiddleware = GETDATE()
	WHERE Stage IS NULL

END

GO
