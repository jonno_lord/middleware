USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CustomersOut]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_CustomersOut]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CustomersOut]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_CustomersOut]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	-- Update Customer Details
	
	UPDATE CTD
	SET Address_1 = f55.Q1ADD1,
	Address_2 = F55.Q1ADD2,
	Address_3 = f55.Q1ADD3,
	Address_4 = f55.Q1ADD4,
	town = f55.q1cty1,
	County = f55.q1coun,
	PostCode = f55.q1addz,
	Country = f55.q1ctr,
	Tax_Exempt_Number = f55.q1txct,
	q1hold = f55.q1hold,
	VAT_REg_NO = q1Tax
	FROM Customers ctd
	INNER JOIN F550101 f55 ON (f55.Q1URRF = ctd.URN_Number)
	WHERE (ctd.has_Errors IS NULL or ctd.has_Errors =0) 
	AND ctd.Stage = 5
	

	-- Insert Into Outgoing Customers

	INSERT INTO ##EF_Outgoing_Customers
			   ([CodId]
			   ,[ExternalCustomerAccount])


		SELECT DISTINCT
		ctd.CodID as CodID,
		ctd.JDE_Account_Number As ExternalCustomerAccount
		FROM Customers ctd
		INNER JOIN F550101 f55 on f55.Q1URRF = ctd.URN_NUMBER
		WHERE ctd.Stage = 5
		
		-- Update Customers
		UPDATE ctm
		SET MiddlewareToMiddlewareOut  = GETDATE()
		FROM Customers ctm 
		INNER JOIN F550101 f55 ON (f55.Q1URRF = ctm.URN_Number)
		WHERE ctm.Stage = 5

END





GO
