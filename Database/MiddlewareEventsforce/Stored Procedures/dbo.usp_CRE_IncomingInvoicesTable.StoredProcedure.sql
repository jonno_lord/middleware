USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_IncomingInvoicesTable]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_CRE_IncomingInvoicesTable]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_IncomingInvoicesTable]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CRE_IncomingInvoicesTable]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..##EF_Incoming_Invoices') IS NOT NULL 
		DROP TABLE ##EF_Incoming_Invoices



	CREATE TABLE ##EF_Incoming_Invoices
	(
	[InvoiceNumber] [varchar](50) NULL,
	[OriginalInvoiceNumber] [varchar](50) NULL,
	[CodId] [varchar](50) NOT NULL,
	[Externalcustomeraccount] [varchar](50) NULL,
	[BookingReference] [varchar](50) NULL,
	[PurchaseCreated] [datetime] NOT NULL,
	[EventName] [varchar](500) NULL,
	[EventStartDate] [varchar](50) NULL,
	[EventEndDate] [varchar](50) NULL,
	[CurrencyCode] [varchar](50) NULL,
	[TotalPrice] [varchar](50) NULL,
	[VAT] [varchar](50) NULL,
	[PaymentMethod] [varchar](50) NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[addressLine1] [varchar](50) NULL,
	[addressLine2] [varchar](50) NULL,
	[addressLine3] [varchar](50) NULL,
	[city] [varchar] (50) NULL,
	[postcode] [varchar](50) NULL,
	[country] [varchar](50) NULL,
	[company] [varchar](100) NULL,
	[Email] [varchar](500) NULL,
	[Duedays] [varchar](50) NULL,
	[CreditController] [varchar](250) NULL,
	[VATRegistrationNumber] [varchar](50) NULL,
	[BusinessUnit] [varchar](250) NULL,
	[Venue] [varchar](250) NULL,
	[NumberOfAttendees] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[Description] [varchar](150) NULL,
	[Quantity] [varchar](50) NULL,
	[UnitPrice] [varchar](50) NULL,
	[Amount] [varchar](50) NULL,
	[extItemCode] [varchar](50) NULL,
	[extItemCode2] [varchar](50) NULL,
	[Batch_Number] [int] NULL,
	[Item_Number] [int] NULL,
	[TransactionReference] [varchar](50) NULL
) ON [PRIMARY]

END




GO
