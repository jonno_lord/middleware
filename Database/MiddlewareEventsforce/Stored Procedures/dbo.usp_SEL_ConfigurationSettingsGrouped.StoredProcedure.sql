USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ConfigurationSettingsGrouped]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_ConfigurationSettingsGrouped]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ConfigurationSettingsGrouped]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ConfigurationSettingsGrouped] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT keyCategory, keyName FROM ConfigurationSettings
END



GO
