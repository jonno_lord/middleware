USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_PaymentsToEventsForceFormat]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_PaymentsToEventsForceFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_PaymentsToEventsForceFormat]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TRA_PaymentsToEventsForceFormat]
	(@BatchNumber as int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO F5503B14
	(
	EFPYID , EFRC5 , EFDOC , EFDCT , EFSFX , EFCKNU , EFAN8 , EFDMTJ , EFEXR , EFPAAP, EFCRCD, EFURDT ,
	EFURAB,EFUSER , EFPID , EFUPMJ ,EFUPMT
	)
	SELECT JF55.EFPYID , JF55.EFRC5 , JF55.EFDOC , JF55.EFDCT , JF55.EFSFX , JF55.EFCKNU , JF55.EFAN8 , JF55.EFDMTJ ,
	 JF55.EFEXR , JF55.EFPAAP, JF55.EFCRCD, JF55.EFURDT ,
	JF55.EFURAB,JF55.EFUSER , JF55.EFPID , JF55.EFUPMJ ,JF55.EFUPMT FROM JDE_F5503B14 JF55 
	LEFT JOIN F5503B14 F55 
	ON JF55.EFDOC = F55.EFDOC AND JF55.EFPYID = F55.EFPYID AND JF55.EFRC5 = F55.EFRC5
	WHERE F55.SENT_TO_MW IS NULL


END



GO
