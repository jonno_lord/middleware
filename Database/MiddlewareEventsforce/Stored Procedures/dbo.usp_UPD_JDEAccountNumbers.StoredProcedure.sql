USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JDEAccountNumbers]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_JDEAccountNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JDEAccountNumbers]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_JDEAccountNumbers]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE i
	SET i.JDE_Account_No = c.JDE_Account_Number
	FROM Invoices i
	INNER JOIN Customers c ON i.JDE_Account_No = c.JDENumberCopy
	INNER JOIN MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch ccob ON (i.Batch_Number = ccob.Batch_number)
	WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' 
	and ccob.mw_to_mw_NCC_out IS NULL and Jde_Account_No IS NOT NULL
	and net_price <> 0 and c.JDE_Account_Number IS NOT NULL
		
END



GO
