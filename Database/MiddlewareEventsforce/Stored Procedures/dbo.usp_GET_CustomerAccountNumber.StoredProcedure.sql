USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerAccountNumber]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_GET_CustomerAccountNumber]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_CustomerAccountNumber]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_CustomerAccountNumber]
	-- Add the parameters for the stored procedure here
	@codId AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @retValue AS INT

    -- Insert statements for procedure here
	IF EXISTS 
	(SELECT * FROM Customers WHERE CodId = @codId)
	BEGIN
		SELECT @retValue = ISNULL(CONVERT(INT,JDE_Account_Number),0) FROM Customers WHERE CodId = @codId
	END
	ELSE
	BEGIN 
		SET @retValue = 0
	END

	RETURN @retValue
END


GO
