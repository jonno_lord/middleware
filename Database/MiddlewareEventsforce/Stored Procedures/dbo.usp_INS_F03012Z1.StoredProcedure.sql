USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012Z1]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F03012Z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F03012Z1]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F03012Z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F03012Z1 f
		INNER JOIN Customers c ON c.id = f.VOEDTN
		WHERE c.Stage = 2

	INSERT INTO dbo.F03012Z1
	(CustomerId,VOACL,VOBADT,VOCRCD,VOEDBT,VOEDLN,VOEDTN,VOEDUS,VOSTMT,VOTNAC,VOTRAR,VOALKY,VOTXA1)
	SELECT	Id															AS CustomerId,
			dbo.fn_CreditLimit()										AS VOACL,
			LEFT(dbo.fn_BillingType(),1)								AS VOBADT,
			c.Negotiation_Currency										AS VOCRCD,
			LEFT(@BatchNumber, 15)										AS VOEDBT,
			LEFT(dbo.fn_LineNumber(),1)									AS VOEDLN,
			c.id														AS VOEDTN,									
			LEFT(dbo.fn_MiddlewareUser(), 9)							AS VOEDUS,
			LEFT(dbo.fn_PrintStatement(),1)								AS VOSTMT,
			LEFT(dbo.fn_TransactionSetting(), 1)						AS VOTNAC,
			LEFT(dbo.fn_PaymentTerms(), 1)								AS VOTRAR,
			dbo.fn_LongAddressNumber(c.URN_number)						AS VOALKY,
			LEFT(dbo.fn_TaxExemptCodeDesc(c.Tax_exempt_number), 5)		AS VOTXA1
	FROM Customers c	 
	WHERE Stage = 1		
END




GO
