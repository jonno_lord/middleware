USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_IncomingCustomersTable]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_CRE_IncomingCustomersTable]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_IncomingCustomersTable]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CRE_IncomingCustomersTable]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..##EF_Incoming_Customers') IS NOT NULL 
		DROP TABLE ##EF_Incoming_Customers 



	CREATE TABLE ##EF_Incoming_Customers
	(
	[Sent_to_MW] [datetime] NULL, 
	[XML_Filename] [nvarchar](50) NULL, 
	[XCRstamp] [datetime] NULL, 
	[XMOstamp] [datetime] NULL, 
	[XCRuserID] [nvarchar](50) NULL, 
	[XMOUserID] [nvarchar](50) NULL, 
	[InvoiceNumber] [nvarchar](50) NULL, 
	[purchaseID] [nvarchar](50) NULL, 
	[OriginalInvoiceNumber] [nvarchar](50) NULL, 
	[Externalpersonid] [nvarchar](50) NULL, 
	[CodId] [nvarchar](50) NULL, 
	[Externalcustomerid] [nvarchar](50) NULL, 
	[Externalcustomeraccount] [nvarchar](50) NULL, 
	[ExportStatus] [nvarchar](50) NULL, 
	[ExportTimeStamp] [datetime] NULL, 
	[BookingReference] [nvarchar](50) NULL, 
	[ExternalPurchaseReference] [nvarchar](50) NULL, 
	[PurchaseType] [nvarchar](50) NULL, 
	[PurchaseStatus] [nvarchar](50) NULL, 
	[PurchaseCreated] [datetime] NULL, 
	[PurchaseLastChanged] [datetime] NULL, 
	[EventName] [nvarchar](500) NULL, 
	[Eventid] [nvarchar](50) NULL, 
	[EventStartDate] [nvarchar](50) NULL, 
	[EventEndDate] [nvarchar](50) NULL, 
	[CurrencyCode] [nvarchar](50) NULL, 
	[TotalPrice] [decimal](18, 2) NULL, 
	[VAT] [decimal](18, 2) NULL, 
	[PaymentMethod] [nvarchar](50) NULL, 
	[Firstname] [nvarchar](50) NULL, 
	[Lastname] [nvarchar](50) NULL, 
	[Salutation] [nvarchar](50) NULL, 
	[JobTitle] [nvarchar](50) NULL, 
	[addressLine1] [nvarchar](50) NULL, 
	[addressLine2] [nvarchar](50) NULL, 
	[addressLine3] [nvarchar](50) NULL, 
	[city] [nvarchar](50) NULL, 
	[postcode] [nvarchar](50) NULL, 
	[country] [nvarchar](50) NULL, 
	[CountryCode] [nvarchar](50) NULL, 
	[company] [nvarchar](100) NULL, 
	[Email] [nvarchar](500) NULL, 
	[Telephone] [nvarchar](50) NULL, 
	[BookedBy] [nvarchar](50) NULL, 
	[CostCentre] [nvarchar](250) NULL, 
	[ChequeNumber] [nvarchar](250) NULL, 
	[town] [nvarchar](250) NULL, 
	[Duedays] [nvarchar](50) NULL, 
	[CreditController] [nvarchar](250) NULL, 
	[VATRegistrationNumber] [nvarchar](50) NULL, 
	[TaxExemptCertificate] [nvarchar](50) NULL,
	 [BusinessUnit] [nvarchar](250) NULL, 
	[Venue] [nvarchar](250) NULL, 
	[CustomerPOnumber] [nvarchar](50) NULL, 
	[NumberOfAttendees] [nvarchar](50) NULL, 
	[ObjInvoiceItems] [nvarchar](50) NULL, 
	[LnkInvoiceRun] [nvarchar](50) NULL, 
	[Type] [nvarchar](50) NULL, 
	[Description] [nvarchar](50) NULL, 
	[Amount] [decimal](18, 2) NULL, 
	[extItemCode] [nvarchar](50) NULL, 
	[extItemCode2] [nvarchar](50) NULL
	) ON [PRIMARY]
END


GO
