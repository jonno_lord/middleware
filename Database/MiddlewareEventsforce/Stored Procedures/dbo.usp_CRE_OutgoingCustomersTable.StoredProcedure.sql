USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_OutgoingCustomersTable]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_CRE_OutgoingCustomersTable]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_OutgoingCustomersTable]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CRE_OutgoingCustomersTable]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..##EF_Outgoing_Customers') IS NOT NULL 
		DROP TABLE ##EF_Outgoing_Customers

	CREATE TABLE ##EF_Outgoing_Customers(
		[Sent_to_EventsForce] [datetime] NULL,
		[XML_Filename] [varchar](50) NULL,
		[XCRstamp] [datetime] NULL,
		[XMOStamp] [datetime] NULL,
		[XCRuserID] [varchar](50) NULL,
		[XMOUserID] [varchar](50) NULL,
		[ExternalPersonId] [varchar](50) NULL,
		[CodId] [varchar](50) NOT NULL,
		[ExternalCustomerID] [varchar](50) NULL,
		[ExternalCustomerAccount] [float] NULL,
		[FirstName] [varchar](50) NULL,
		[LastName] [varchar](50) NULL,
		[addressLine1] [varchar](50) NULL,
		[addressLine2] [varchar](50) NULL,
		[addressLine3] [varchar](50) NULL,
		[country] [varchar](50) NULL,
		[company] [varchar](100) NULL,
		[jobTitle] [varchar](50) NULL,
		[salutation] [varchar](50) NULL,
		[email] [varchar](500) NULL,
		[telephone] [varchar](50) NULL,
		[postcode] [varchar](50) NULL,
		[creditStatus] [varchar](50) NULL,
		[creditLimit] [varchar](50) NULL,
		[availableCredit] [decimal](19, 2) NULL,
		[paymentTerms] [varchar](50) NULL,
		[VATRegistrationNumber] [varchar](50) NULL,
		[taxExemptCertificate] [varchar](50) NULL
	) ON [PRIMARY]
	
	
END



GO
