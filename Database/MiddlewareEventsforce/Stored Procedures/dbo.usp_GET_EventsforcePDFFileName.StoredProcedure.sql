USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_EventsforcePDFFileName]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_GET_EventsforcePDFFileName]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_EventsforcePDFFileName]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonno Lord
-- Create date: 2nd November 2015
-- Description: Eventsforce PDF filename for MoretonSmith
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_EventsforcePDFFileName]
	-- Add the parameters for the stored procedure here
	@InvoiceNumber AS VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @FileName AS VARCHAR(50)

	SELECT @FileName = CONVERT(VARCHAR,JDE_Account_No) + '_' + c.ShipTo + '_' + Document_Number + '_' + Document_Number + '_' + CONVERT(VARCHAR(10), Invoice_Date, 112)
	FROM Invoices i 
    INNER JOIN Customers c ON i.JDE_Account_No = c.JDE_Account_Number 
    WHERE Document_Number = @InvoiceNumber

	RETURN @FileName
	
END


GO
