USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JDEBUandObject]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_JDEBUandObject]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_JDEBUandObject]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_JDEBUandObject]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Invoices
	WHERE (Business_Unit = 0 OR Object_account = 0)
	AND MW_to_MW_Out IS NULL

	UPDATE i
	SET i.Business_Unit = bul.Oracle, 
	i.Object_account = CASE WHEN RTRIM(i.Object_account) = '30210' THEN '42040210' WHEN RTRIM(i.Object_account) = '30360' THEN '42380254' ELSE i.Object_account END,
	Subsidiary_account = 0
	FROM Invoices i
	INNER JOIN dbo.EF_Invoice_Orders_Batch ccob ON (i.Batch_Number = ccob.Batch_number)
	INNER JOIN MiddlewareJDE.dbo.BusinessUnitLookup bul on i.Business_Unit = bul.JDE
	WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' 
	AND ccob.mw_to_mw_NCC_out IS NULL and Jde_Account_No IS NOT NULL
	AND net_price <> 0 
	
	UPDATE i
	SET i.Object_account = CASE WHEN RTRIM(i.Object_account) = '30210' THEN '42040210' WHEN RTRIM(i.Object_account) = '30360' THEN '42380254' ELSE i.Object_account END,
	Subsidiary_account = 0
	FROM Invoices i
	INNER JOIN dbo.EF_Invoice_Orders_Batch ccob ON (i.Batch_Number = ccob.Batch_number)
	LEFT JOIN MiddlewareJDE.dbo.BusinessUnitLookup bul on i.Business_Unit = bul.JDE
	WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' 
	AND ccob.mw_to_mw_NCC_out IS NULL and Jde_Account_No IS NOT NULL
	AND net_price <> 0 
		
END



		



GO
