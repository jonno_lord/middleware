USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ConfigurationSettings]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_ConfigurationSettings]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ConfigurationSettings]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Deletes configuration settings by keycategory and keyname match (deleting for each environment)
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_ConfigurationSettings]
	-- Add the parameters for the stored procedure here
	@category AS NVARCHAR(50), 
	@name AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM ConfigurationSettings
	WHERE keyCategory = @category AND keyName = @name
END



GO
