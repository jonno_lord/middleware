USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F5503B14]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_F5503B14]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_F5503B14]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Batch submitted through debugger: SQLQuery106.sql|7|0|C:\Users\jpresly\AppData\Local\Temp\~vsF94A.sql
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_F5503B14]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE F5503B14
SET Sent_to_MW = GETDATE()
WHERE Sent_To_MW IS NULL

END




GO
