USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F0101z2]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0101z2]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F0101z2]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	-- remove previously created accounts from F0101z2
	DELETE f FROM F0101Z2 f
		INNER JOIN Customers c ON c.id = f.szedtn
		WHERE c.Stage = 2

	INSERT INTO dbo.F0101Z2
	(CustomerId,SZAC05,SZAC08,SZADD1,SZADD2,SZADD3,SZADD4,SZADDZ,SZALKY,SZAR1,SZAR2,SZAT1,SZCOUN,
	SZCTR,SZCTY1,SZEDBT,SZEDLN,SZEDTN,SZEDUS,SZMLNM,SZPH1,SZRMK,SZTAX,SZTNAC,SZURAB,SZURRF)
	SELECT	
			c.id																	AS CustomerId,
			LEFT(dbo.fn_CustomerType(),2)											AS SZAC05,
			LEFT(dbo.fn_IsoCountry(), 3)								            AS SZAC08,
			LEFT(c.Address_1, 40)													AS SZADD1,
			LEFT(c.Address_2, 40)													AS SZADD2,
			LEFT(c.Address_3, 40)													AS SZADD3,
			LEFT(c.Address_4, 40)													AS SZADD4,
			LEFT(c.Postcode, 12)													AS SZADDZ,
			dbo.fn_LongAddressNumber(c.URN_number)									AS SZALKY,
			LEFT(dbo.fn_TelephoneType(c.Telephone_number), 3)						AS SZAR1,
			LEFT(dbo.fn_FaxType(), 3)												AS SZAR2, 
			LEFT(dbo.fn_SearchType(), 2)											AS SZAT1,
			LEFT(c.County,25)														AS SZCOUN,
			LEFT(c.Country_Code,3)													AS SZCTR,
			LEFT(c.Town,25)															AS SZCTY1,
		    @BatchNumber															AS SZEDBT,
			LEFT(dbo.fn_LineNumber(), 1)											AS SZEDLN,
			c.id																	AS SZEDTN,
			LEFT(dbo.fn_MiddlewareUser(), 9)										AS SZEDUS,
			LEFT(dbo.fn_CompanyName(c.Company_Name,c.First_Name,c.Last_Name), 40)	AS SZMLNM,		
			LEFT(c.Telephone_number,20)												AS SZPH1,
			LEFT(c.Email_address,240)												AS SZRMK,
			c.VAT_Reg_No															AS SZTAX,
			LEFT(dbo.fn_TransactionSetting(), 1)									AS SZTNAC,
			c.JDE_Account_Number													AS SZURAB,
			c.URN_number															AS SZURRF
	FROM Customers c	 
	WHERE Stage = 1		
END




GO
