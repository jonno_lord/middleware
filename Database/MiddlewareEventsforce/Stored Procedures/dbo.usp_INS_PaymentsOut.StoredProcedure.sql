USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_PaymentsOut]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_PaymentsOut]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_PaymentsOut]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_PaymentsOut]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	INSERT INTO ##EF_Outgoing_Payments
           ([EFPYID]
			,[EFCKNU]
			,[Amount]
			,[Comment]
			,[Currency]
			,[RelatedInvoiceNumber]
			,[RelatedPurchaseID]
			,[paymentMethod])



	SELECT DISTINCT 
		LTRIM(RTRIM(CONVERT(INT,EFPYID))) as EFPYID,
		EFCKNU,
		((f55.EFPAAP)/100) as Amount,
		LEFT(ISNULL(LTRIM(RTRIM(f55.EFEXR)),''),50) As Comment,
		LTRIM(RTRIM(f55.EFCRCD)) as Currency,
		LTRIM(RTRIM(f55.EFDOC)) as RelatedInvoiceNumber, 
		LTRIM(RTRIM(Inv.PurchaseID)) as RelatedPurchaseID,
		LTRIM(RTRIM(Inv.Payment_Method)) as paymentMethod
		FROM F5503B14  f55 
		JOIN MiddlewareEventsForce.DBO.Invoices Inv 
		ON F55.EFDOC = Inv.Document_number and F55.SENT_TO_MW IS NULL 
END



GO
