USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[prc_GenerateURN]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[prc_GenerateURN]
GO
/****** Object:  StoredProcedure [dbo].[prc_GenerateURN]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_GenerateURN] (@Batch_No AS INTEGER) AS

Declare @Email AS VARCHAR(50)
Declare @Next_URN_Number AS INTEGER


--** Find out all the customers that we need to allocate receipt numbers for in the
--** batch we are about to process..
declare csrORDURN cursor for
	select Email_Address from Customers where Batch_Number= @Batch_No
		Group By Email_Address

Open csrORDURN

--** Retrieve the first customer we are about to assign a receipt number to
Fetch Next FROM csrORDURN INTO @Email
WHILE @@Fetch_Status = 0
BEGIN

	IF EXISTS (SELECT Email_Address from Customers where Batch_Number < @Batch_No and Email_Address = @Email)
		UPDATE  Customers SET URN_Number = (SELECT URN_NUMBER FROM Customers where Batch_Number < @Batch_No and Email_Address = @Email)
		WHERE 	Email_Address= @Email AND URN_Number IS NULL
			AND Batch_Number = @Batch_No


	--** Get the next receipt number
	INSERT INTO Last_URN_Number([Description]) VALUES (CONVERT(VARCHAR(101), GETDATE()) + ' URN Request')
	SELECT @Next_URN_Number = @@IDENTITY

	--** Update the Receipt number for this customer to hold the next free value...
	UPDATE Customers SET URN_Number = @Next_URN_Number 
		WHERE 	Email_Address= @Email AND URN_Number IS NULL
			AND Batch_Number = @Batch_No

	Fetch Next FROM csrORDURN INTO @Email

END

--** Close and release resources
close csrORDURN
deallocate csrORDURN






GO
