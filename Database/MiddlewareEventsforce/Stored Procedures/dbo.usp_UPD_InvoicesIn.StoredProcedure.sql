USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoicesIn]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoicesIn]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoicesIn]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery106.sql|7|0|C:\Users\jpresly\AppData\Local\Temp\~vsF94A.sql
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoicesIn]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @InvoiceNumber AS VARCHAR(50) 
	DECLARE @OriginalInvoiceNumber AS VARCHAR(50)
	DECLARE @Batch_Number AS INT
	DECLARE @PreviousInvoice AS VARCHAR(50)
	
	DECLARE @Net_Price AS DECIMAL(19,2)
	DECLARE @Net_Price_Sterling AS DECIMAL(19,2)
	DECLARE @Gross_Price AS DECIMAL(19,2)
	DECLARE @Temp_Gross_Price AS DECIMAL(19,2)
	DECLARE @Gross_Price_Sterling AS DECIMAL(19,2)  

	DECLARE @Total_Net_Price AS DECIMAL(19,2)
	DECLARE @Total_Gross_Price AS DECIMAL(19,2)
	DECLARE @Total_Temp_Gross_Price AS DECIMAL(19,2)

	DECLARE @VatAmount AS DECIMAL(19,2)
	DECLARE @Description AS VARCHAR(50)
	DECLARE @Basket_Amount AS DECIMAL(19,2)
	DECLARE @Basket_Net_Price AS DECIMAL(19,2)
	DECLARE @Basket_Gross_Price AS DECIMAL(19,2)

	DECLARE @ItemNumber AS INT
	DECLARE @EFOverrideAmount AS DECIMAL(19,2)
	DECLARE @itemCount AS INT
	DECLARE @GrandTotal AS DECIMAL(19,2)
	DECLARE @TotalOverrideAmount AS DECIMAL(19,2)
	DECLARE @TotalGTAmount AS DECIMAL(19,2)

	SET @PreviousInvoice = ''
	SET @itemCount = 0
	SET @GrandTotal = 0
	SET @EFOverrideAmount = 0

	DECLARE csrInvoiceRecord CURSOR FOR 

		SELECT InvoiceNumber,Batch_Number,Item_Number
		FROM ##EF_Incoming_Invoices
		WHERE PaymentMethod IN ('Invoice','Bank Transfer','Cheque')
		GROUP BY InvoiceNumber,Batch_Number,Item_Number
		ORDER BY InvoiceNumber  

	OPEN csrInvoiceRecord

		FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber,@Batch_Number,@ItemNumber

		WHILE @@Fetch_Status = 0  
		BEGIN

			SET @Net_Price = 0
			SET @Total_Net_Price = 0
			SET @Temp_Gross_Price = 0
			SET @Total_Temp_Gross_Price = 0
			SET @VatAmount = 0

		IF (@PreviousInvoice <> @InvoiceNumber ) -- check whether the invoice has over ride type	
		BEGIN 
			SET @PreviousInvoice = @InvoiceNumber 

			SET @EFOverrideAmount = 0
			SET @GrandTotal = 0
		
		  SELECT @EFOverrideAmount = CONVERT(decimal(19,2),CONVERT(money,ISNULL(amount,0))) 
		  FROM ##EF_Incoming_Invoices 
		  WHERE [Type] = 'Override' 
		  AND InvoiceNumber = @InvoiceNumber 
	  
		  SELECT @GrandTotal = CONVERT(decimal(19,2),CONVERT(money,ISNULL(amount,0))) 
		  FROM ##EF_Incoming_Invoices 
		  WHERE [Type] = 'GrandTotal' 
		  AND InvoiceNumber = @InvoiceNumber 

		  SET @itemCount = (SELECT MAX(Item_Number) FROM ##EF_Incoming_Invoices 
		  WHERE InvoiceNumber = @InvoiceNumber)
	  
		  SET @TotalOverrideAmount = 0
		  SET @TotalGTAmount = 0

		END

		IF (@EFOverrideAmount IS NOT NULL AND @EFOverrideAmount != 0 ) ----prorated override calculation
		BEGIN 

		  IF(@itemCount - @ItemNumber > 0)
		  BEGIN
		  ----Calculate prorated net price i.e Override amount
		  SELECT @Net_Price = SUM(CONVERT(DECIMAL(19,2),CONVERT(MONEY,ISNULL(Amount,0)) ))
		  FROM ##EF_Incoming_Invoices 
		  WHERE PaymentMethod IN ('Invoice','Bank Transfer','Cheque')
		  AND (([Type] = 'Item') OR ([Type] in ('Discount','Surcharge') AND [Description] not like '%vat%'))  --Modified for WO49126
		  AND InvoiceNumber = @InvoiceNumber 
		  AND Batch_Number = @Batch_Number 
		  AND Item_number = @ItemNumber
		  GROUP BY InvoiceNumber,Item_number

		  SELECT @Total_Temp_Gross_Price = SUM(CONVERT(DECIMAL(19,2),CONVERT(MONEY,ISNULL(Amount,0)) ))
		  FROM ##EF_Incoming_Invoices 
		  WHERE PaymentMethod IN ('Invoice','Bank Transfer','Cheque')
		  AND (([Type] = 'Item') OR ([Type] = 'Discount') OR ([Type] = 'Surcharge')) 
		  AND InvoiceNumber = @InvoiceNumber 
		  AND Batch_Number = @Batch_Number 
		  GROUP BY InvoiceNumber
	
			IF( @Total_Temp_Gross_Price = 0 )
			BEGIN
				SET @Total_Temp_Gross_Price = 1
			END
	
		  SET @Net_Price = (@Net_Price * @GrandTotal/@Total_Temp_Gross_Price)
	
		  UPDATE MiddlewareEventsForce.dbo.invoices
		  SET Net_Price = @Net_Price,Net_PRice_Sterling = @Net_Price
		  WHERE Document_Number = @InvoiceNumber 
		  AND Batch_Number = @Batch_Number 
		  AND Line_number = @ItemNumber

		  ----Calculate prorated Gross price i.e Grand total
		  SELECT @Temp_Gross_Price = SUM(CONVERT(DECIMAL(19,2),CONVERT(MONEY,ISNULL(Amount,0))))
		  FROM ##EF_Incoming_Invoices 
		  WHERE PaymentMethod IN ('Invoice','Bank Transfer','Cheque')
		  AND (([Type] = 'Item') OR ([Type] = 'Discount') OR ([Type] = 'Surcharge' )) 
		  AND InvoiceNumber = @InvoiceNumber 
		  AND Batch_Number = @Batch_Number 
		  AND Item_number = @ItemNumber
		  GROUP BY InvoiceNumber,Item_number

		  SET @Temp_Gross_Price  = (@Temp_Gross_Price * @GrandTotal/@Total_Temp_Gross_Price)
	
		  UPDATE MiddlewareEventsForce.dbo.invoices
		  SET Gross_Price = @Temp_Gross_Price,Gross_Price_Sterling = @Temp_Gross_Price
		  WHERE Document_Number = @InvoiceNumber 
		  AND Batch_Number = @Batch_Number 
		  AND Line_number = @ItemNumber
	
		  SET  @TotalOverrideAmount = @TotalOverrideAmount + @Net_Price
		  SET  @TotalGTAmount = @TotalGTAmount + @Temp_Gross_Price
	
		  END --- if end

		  ELSE --If this is last item in the Invoice Subtact first items price from XML total price
		  BEGIN
	   
		   SET @Net_Price = @EFOverrideAmount - @TotalOverrideAmount 
		   SET @Temp_Gross_Price = @GrandTotal- @TotalGTAmount

		   UPDATE MiddlewareEventsForce.dbo.invoices
		   SET Net_Price = @Net_Price,Net_Price_Sterling = @Net_Price
		   WHERE Document_Number = @InvoiceNumber 
		   AND Batch_Number = @Batch_Number 
		   AND Line_number = @ItemNumber

		   UPDATE MiddlewareEventsForce.dbo.invoices
		   SET Gross_Price = @Temp_Gross_Price,Gross_Price_Sterling = @Temp_Gross_Price
 		   WHERE Document_Number = @InvoiceNumber 
		   AND Batch_Number = @Batch_Number 
		   AND Line_number = @ItemNumber

		  END ---else 
		END --if 

		ELSE ---Non override invoice calculation
		BEGIN 
	
			SELECT @Net_Price = SUM(CONVERT(DECIMAL(19,2),CONVERT(MONEY,ISNULL(Amount,0))))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod IN ('Invoice','Bank Transfer','Cheque')
			AND (([Type] = 'Item') OR ([Type] IN ('Discount','Surcharge') AND [Description] NOT LIKE '%vat%'))  --Modified for WO49126
			and InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			AND Item_Number = @ItemNumber
			GROUP BY InvoiceNumber,Item_Number

			SELECT @Temp_Gross_Price = SUM(CONVERT(decimal(19,2),CONVERT(money,ISNULL(amount,0)) ))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod in  ('Invoice','Bank Transfer','Cheque')
			AND [Type] IN ('Surcharge','Discount') 
			AND [Description] LIKE '%vat%' --Modified for WO49126 
			AND InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number  
			AND Item_Number = @ItemNumber
			GROUP BY InvoiceNumber,Item_Number
			
			SET @Total_Gross_Price = @Net_Price + ISNULL(@Temp_Gross_Price,0)
			SET @PreviousInvoice = @InvoiceNumber 

			UPDATE MiddlewareEventsForce.dbo.invoices
			SET Net_Price = @Net_Price,Net_Price_Sterling = @Net_Price, Gross_Price = @Total_Gross_Price,Gross_Price_Sterling = @Total_Gross_Price			
			WHERE Document_Number = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			AND Line_Number = @ItemNumber

			END  -- end else stament for Non override invoice

			FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber,@Batch_Number,@ItemNumber
	
		END  --while loop

	CLOSE csrInvoiceRecord
	DEALLOCATE csrInvoiceRecord

-- PART 2 @JP

DECLARE @Tax_Rate_Code AS char(50) 
DECLARE @Tax_Rate_Description as char(10)

DECLARE csrInvoiceRecord CURSOR FOR 

	SELECT InvoiceNumber,Batch_Number                                
	FROM ##EF_Incoming_Invoices
	WHERE PaymentMethod in  ('Invoice','Bank Transfer','Cheque')                
	GROUP BY InvoiceNumber,Batch_Number

OPEN csrInvoiceRecord 
FETCH NEXT FROM csrInvoiceRecord
INTO @InvoiceNumber,@Batch_Number

WHILE @@Fetch_Status = 0
 BEGIN           
	SELECT @Tax_Rate_Code = Substring(Rtrim([Description]), 
		       	Charindex('(', Rtrim([Description])) + 1, Charindex(')', Rtrim([Description])) 
                                        - Charindex('(', Rtrim([Description])) - 1)                           
	FROM ##EF_Incoming_Invoices              
	WHERE [Type] IN ('surcharge','discount') 
	AND [Description] LIKE '%VAT%' 
	AND PaymentMethod <> 'Credit Card'
	AND Batch_Number = @Batch_Number 
	AND InvoiceNumber = @InvoiceNumber 

		--Added by Offshore team on 8th Dec,2008 for CR32760 WO48106
	SELECT @Tax_Rate_Description = '+' + substring(UnitPrice, 0,charindex('.', UnitPrice, 0)) + '%'
	FROM ##EF_Incoming_Invoices 
	WHERE [Description] LIKE 'VAT (%' 
	AND PaymentMethod <> 'Credit Card'
	AND [Type] IN ('surcharge','discount')
	AND Batch_Number = @Batch_Number 
	AND InvoiceNumber = @InvoiceNumber 
               
	UPDATE MiddlewareEventsForce.DBO.Invoices
	SET  Tax_Rate_Code = SUBSTRING(@Tax_Rate_Code,1,10), Tax_Rate_Description=@Tax_Rate_Description		
	WHERE Document_Number = @InvoiceNumber 
	AND Batch_Number = @Batch_Number 

	SET @Tax_Rate_Code = NULL
	SET @Tax_Rate_Description = NULL

	FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber,@Batch_Number
END 

CLOSE csrInvoiceRecord 
DEALLOCATE csrInvoiceRecord

-- PART 3 @JP
UPDATE Invoices
SET Document_Type = 'EJ'
WHERE Gross_price < 0 
AND MW_to_MW_Conversion IS NULL 
AND Document_Type = 'EI'

UPDATE Invoices
SET MW_To_MW_Conversion = GETDATE(), Invoice_Date = GETDATE() 
WHERE MW_To_MW_Conversion IS NULL 

END



GO
