USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ConfigurationSettings]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_GET_ConfigurationSettings]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ConfigurationSettings]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ConfigurationSettings]
	-- Add the parameters for the stored procedure here
	@EnvironmentName as VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [ConfigurationSettings] WHERE EnvironmentName=@EnvironmentName
	
END





GO
