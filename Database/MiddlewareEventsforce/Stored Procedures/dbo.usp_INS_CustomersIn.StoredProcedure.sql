USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CustomersIn]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_CustomersIn]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CustomersIn]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_CustomersIn]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


		INSERT INTO dbo.Customers
				   ([Stage]
				   ,[Accepted]
				   ,[Rejected]
				   ,[SourceToMiddleware]
				   ,[MiddlewareScrutiny]
				   ,[AcceptedInScrutiny]
				   ,[RejectedInScrutiny]
				   ,[MiddlewaretoMiddlewareIn]
				   ,[MiddlewareToJDE]
				   ,[RejectedFromJDE]
				   ,[JDEToMiddleware]
				   ,[MiddlewareToMiddlewareOut]
				   ,[MiddlewareToSource]
				   ,[NumberOfScrutinyRulesBroken]
				   ,[CodID]
				   ,[has_Errors]
				   ,[has_Warnings]
				   ,[Last_Affected]
				   ,[Create_Mapping_Entry]
				   ,[Account_Mapped_An8]
				   ,[Is_Company]
				   ,[Country]
				   ,[Tax_exempt_number]
				   ,[Town]
				   ,[County]
				   ,[Telephone_number]
				   ,[Email_address]
				   ,[URN_number]
				   ,[MW_Batch_File_Name]
				   ,[Batch_Number]
				   ,[Country_Code]
				   ,[Postcode]
				   ,[Negotiation_gross_price]
				   ,[Company_Name]
				   ,[Operator_Name]
				   ,[VAT_Reg_No]
				   ,[Negotiation_Currency]
				   ,[Address_1]
				   ,[Address_2]
				   ,[Address_3]
				   ,[Address_4]
				   ,[Payment_Terms]
				   ,[JDE_Account_Number]
				   ,[Gross_Price_Sterling]
				   ,[Credit_Limit]
				   ,[Credit_Controller_Login_Name]
				   ,[Credit_Controller_Initials]
				   ,[Available_Credit]
				   ,[Q1AC05]
				   ,[Q1AC06]
				   ,[Q1AN81]
				   ,[Q1AMCR]
				   ,[Q1CM]
				   ,[Q1CMGR]
				   ,[Q1EXR1]
				   ,[Q1HOLD]
				   ,[Q1LNGP]
				   ,[Q1MLN1]
				   ,[Q1TXA1]
				   ,[Q1PORQ]
				   ,[Q1TRAR]
				   ,[Q1AC02]
				   ,[Event_Name]
				   ,[Title]
				   ,[Event_Start_Date]
				   ,[Purchase_Status]
				   ,[Business_Unit]
				   ,[Payment_Method]
				   ,[First_Name]
				   ,[Last_Name]
				   ,[RelatedPurchaseID])


		SELECT 

		null as Stage,
		null as Accepted,
		null as Rejected,
		null as SourceToMiddleware,
		null as MiddlewareScrutiny,
		null as AcceptedInScrutiny,
		null as RejectedInScrutiny,
		null as MiddlewaretoMiddlewareIn,
		null as MiddlewareToJDE,
		null as RejectedFromJDE,
		null as JDEToMiddleware,
		null as MiddlewareToMiddlewareOut,
		null as MiddlewareToSource,
		null as NumberOfScrutinyRulesBroken,
		tci.CodID as CodID,
		null as has_Errors,
		null as has_Warnings,
		null as Last_Affected,
		null as Create_Mapping_Entry,
		null as Account_Mapped_An8,
		0 as Is_Company,
		tci.Country as Country, 
		TaxExemptCertificate as Tax_Exempt_Number,
		tci.town, 
		null as County,
		telephone as Telephone_Number,
		email as Email_Address,
		null as URN_number,
		XML_Filename as MW_Batch_File_Name,
		null as Batch_Number,
		Countrycode  as  Country_code,
		tci.postcode,
		convert(decimal(18,2),CAST(Amount as money)) as Negotiation_Gross_Price , 
		company as Company_Name,
		bookedBy as Operator_Name,
		VATregistrationNumber as VAT_Reg_No,

		CurrencyCode as Negotiation_Currency,
		addressLine1 Address_1,
		addressLine2 as Address_2,
		addressLine3 as Address_3,

		null as Address_4,
		'N30'  as Payment_Terms , 
		null as JDE_Account_Number, 
		convert(decimal(18,2),CAST(Amount as money)) as Gross_Price_Sterling,
		25000.00 as Credit_Limit , 

		CASE when patindex('%(%',creditController ) > 1 then substring(creditController,1,patindex('%(%',creditController )-1) 
		else substring( creditController , patindex( '%)%', creditController )+1 ,Len(creditController ) - patindex( '%)%', creditController )) 
		end as Credit_Controller_Login_Name,CASE when patindex( '%(%', creditController ) > 0 then substring( creditController , patindex( '%(%', creditController )+1,patindex( '%)%', creditController )-patindex( '%(%', creditController )-1) else NULL end as Credit_Controller_Initials, 
		null as Available_Credit,		
		null as Q1AC05,
		null as Q1AC06,
		null as Q1AN81,
		null as Q1AMCR,
		null as Q1CM,
		null as Q1CMGR,
		null as Q1EXR1,
		null as Q1HOLD,
		null as Q1LNGP,
		null as Q1MLN1,
		null as Q1TXA1,
		null as Q1PORQ,
		null as Q1TRAR,
		null as Q1AC02,
		EventName as Event_Name,
		Salutation as title,
		CONVERT(DATETIME, EventStartDate,103),
		PurchaseStatus as Purchase_Status,
		BusinessUnit as Business_Unit,
		PaymentMethod as Payment_Method,
		Firstname as First_name,
		Lastname as Last_name,
		null as RelatedPurchaseID

		FROM ##EF_Incoming_Customers tci
		LEFT OUTER JOIN dbo.Customers ctd ON ctd.CodID = tci.CodID COLLATE SQL_Latin1_General_CP1_CI_AS
		WHERE Sent_to_MW IS NULL and ctd.CodID IS NULL		-- Batch Numbers
		
		Declare @Batch as int

		SELECT @Batch = CustomerBatchNumber FROM dbo.EventsForce_Guids

		UPDATE dbo.Customers
		SET Batch_Number = @Batch
		WHERE SourceToMiddleware IS NULL

		UPDATE dbo.EventsForce_Guids
		SET CustomerBatchNumber += 1

		SELECT @Batch as BatchNumber

		-- Country Codes
		UPDATE ctd 
		SET ctd.Country_Code = ec.countryCode 


		FROM dbo.Customers ctd 
		INNER JOIN dbo.EventsForce_Country ec 
		ON ctd.Country_Code = ec.CountryID 
		AND ctd.urn_number IS NULL AND ctd.SourceToMiddleware IS NULL

		-- URN
		EXEC dbo.prc_GenerateURN @Batch
		
		UPDATE dbo.Customers
		SET  SourceToMiddleware = GETDATE()
		WHERE SourceToMiddleware IS NULL
		
		-- Set Stage
		UPDATE dbo.Customers
		SET  Stage = 1
		WHERE Stage IS NULL

END


GO
