USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ConfigurationSetting]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_DEL_ConfigurationSetting]
GO
/****** Object:  StoredProcedure [dbo].[usp_DEL_ConfigurationSetting]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Deletes configuration Settings by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_DEL_ConfigurationSetting] 
	-- Add the parameters for the stored procedure here
	@id AS INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM ConfigurationSettings
	WHERE id = @id
END



GO
