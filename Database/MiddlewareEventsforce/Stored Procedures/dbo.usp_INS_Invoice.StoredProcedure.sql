USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Invoice]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_Invoice]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_Invoice]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_Invoice]
(
    @invoiceNumber as int,
	@originalInvoiceNumber as int,
	@codId as int,
	@bookingReference as varchar(20),
	@eventName as varchar(50),
	@eventStartDate as varchar(20),
	@eventEndDate as varchar(20),
	@currencyCode as varchar(5),
	@grossPrice as varchar(20),
	@vatAmount as varchar(20),
	@paymentMethod as varchar(20),
	@firstName as varchar(50),
	@lastName as varchar(50),
	@addressLine1 as varchar(50),
	@addressLine2 as varchar(50),
	@addressLine3 as varchar(50),
	@city as varchar(30),
	@postCode as varchar(20),
	@country as varchar(20),
	@companyName as varchar(50),
	@email as varchar(50),
	@vatRegistrationNumber as varchar(20),
	@businessUnit as varchar(10),
	@venue as varchar(50),
	@numberOfAttendees as varchar(5),
	@type as varchar(20),
	@description as varchar(100),
	@quantity as int,
	@unitPrice as varchar(20),
	@amount as varchar(20),
	@purchaseCreated as datetime,
	@itemNumber as int,
	@accountNumber as varchar(10),
	@transactionReference as varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @paymentTerms AS VARCHAR(3)
	DECLARE @creditController AS VARCHAR(3)
	DECLARE @extItemCode AS VARCHAR(10)
	DECLARE @extItemCode2 AS VARCHAR(2)
	
	SET @paymentTerms = 'N30'
	SET @creditController = 'ECC' 
	SET @extItemCode = '42040210'
	SET @extItemCode2 = '0'

	INSERT INTO ##EF_Incoming_Invoices(InvoiceNumber,OriginalInvoiceNumber,CodId,ExternalCustomerAccount,BookingReference,EventName,EventStartDate,EventEndDate,CurrencyCode,
	TotalPrice,VAT,PaymentMethod,FirstName,LastName,AddressLine1,AddressLine2,AddressLine3,City,PostCode,Country,Company,Email,VATRegistrationNumber,BusinessUnit,Venue,NumberOfAttendees,
	[Type],[Description],Quantity,UnitPrice,Amount,ExtItemCode,ExtItemCode2,CreditController,PurchaseCreated,DueDays,Item_Number,TransactionReference)

	VALUES (@invoiceNumber,@originalInvoiceNumber,@codId,@accountNumber,@bookingReference,@eventName,@eventStartDate,@eventEndDate,@currencyCode,@grossPrice,@vatAmount,@paymentMethod,
	@firstName,@lastName,@addressLine1,@addressLine2,@addressLine3,@city,@postCode,@country,@companyName,@email,@vatRegistrationNumber,@businessUnit,@venue,@numberOfAttendees,
	@type,@description,@quantity,@unitPrice,@amount,@extItemCode,@extItemCode2,@creditController,@purchaseCreated,@paymentTerms,@itemNumber,@transactionReference)

END

GO
