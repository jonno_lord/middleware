USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_InvoicesIn]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_InvoicesIn]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_InvoicesIn]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_InvoicesIn]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [MiddlewareEventsForce].[dbo].[Invoices]
			   ([Customer_name]
			    ,[Address]
				,[Town]
				,[Postcode]
				,[Country]
			    ,[Source_to_MW]
				,[Invoice_date]
				,[Order_Date]
				,[VAT_Registration_No]
				,[Payment_Method]
				,[Currency]
				,[JDE_Account_No]
				,[Business_Unit]
				,[Object_account]
				,[Subsidiary_account]
				,[Document_Type]
				,[Document_Number]
				,[Original_Invoice_Number]
				,[Event_Name]
			    ,[Event_Start_Date]
			    ,[Event_End_Date]
				,[CodID]
			    ,[Batch_number]
			    ,[Line_Number]
			    ,[Booking_Reference])

    -- Insert statements for procedure here
	SELECT DISTINCT 
	SUBSTRING(ISNULL(FirstName,'') + ' ' + ISNULL(Lastname,''),1,50) As Customer_name,			
	SUBSTRING(ISNULL(Addressline1,'') + ISNULL(' ,'+ Addressline2 ,' ')  + ISNULL(', ' + Addressline3,' '),1,100) As [Address],	
	City as town,
	efi.PostCode as postcode,
	efi.Country,	
	GETDATE() as Source_to_MW,
	PurchaseCreated as Invoice_Date,					 
	PurchaseCreated As Order_Date,
	VATRegistrationNumber As Vat_Registration_No,		
	PaymentMethod As Payment_Method,					 
	CurrencyCode As Currency,
	CAST(ExternalCustomerAccount as int) As JDE_Account_No,
	BusinessUnit As Business_Unit,	
	extItemCode As Object_account,
	extItemCode2 As Subsidiary_account,
	CASE WHEN ([type] ='item' and (substring(Amount,1,1 ) = '-')) 
	THEN 'EJ' 
	ELSE 'EI' END AS Document_Type,
	InvoiceNumber AS Document_Number,				 
	OriginalInvoiceNumber As Original_Invoice_Number,
	EventName As Event_Name,
	SUBSTRING(EventStartDate,9,2) + '/' + SUBSTRING(EventStartDate,6,2) + '/' + SUBSTRING(EventStartDate,1,4) As Event_Start_Date,		
	SUBSTRING(EventEndDate,9,2) + '/' + SUBSTRING(EventEndDate,6,2) + '/' + SUBSTRING(EventEndDate,1,4) As Event_End_Date,		
	efi.CodID As CodID,
	efi.Batch_Number as Batch_Number , 
	Item_number as Line_Number,
	BookingReference as Booking_Reference
	FROM ##EF_Incoming_Invoices efi 
	INNER JOIN Customers c on efi.externalCustomerAccount = c.JDE_Account_Number
	WHERE PaymentMethod in ('Invoice','Bank Transfer','Cheque') and [type] = 'item'
END



GO
