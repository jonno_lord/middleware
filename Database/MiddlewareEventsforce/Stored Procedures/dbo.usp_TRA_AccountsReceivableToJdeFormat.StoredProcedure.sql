USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJdeFormat]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJdeFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_AccountsReceivableToJdeFormat]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TRA_AccountsReceivableToJdeFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Set F0911z1, F03b11z1
	
	 BEGIN TRANSACTION
			
			EXECUTE dbo.usp_UPD_JDEAccountNumbers
			IF(@@ERROR <> 0) GOTO Failure
			
			EXECUTE usp_UPD_JDEBUandObject
			IF(@@ERROR <> 0) GOTO Failure

			EXECUTE dbo.usp_UPD_InvoiceLineNumbers
			IF(@@ERROR <> 0) GOTO Failure

            EXECUTE dbo.usp_INS_F03B11z1 @BatchNumber
            IF(@@ERROR <> 0) GOTO Failure
            
            EXECUTE dbo.usp_INS_F0911z1 @BatchNumber  
            IF(@@ERROR <> 0) GOTO Failure
            
            EXECUTE dbo.usp_UPD_TranslatedCompanyNumber  
            IF(@@ERROR <> 0) GOTO Failure
            
            EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoiceTaxCode @SystemName='MiddlewareEventsforce'
            IF(@@ERROR <> 0) GOTO Failure
            
            EXECUTE MiddlewareCommon.dbo.usp_UPD_InvoicesPaymentTerms @SystemName='MiddlewareEventsforce'
            IF(@@ERROR <> 0) GOTO Failure
                        
            UPDATE F03b11z1 SET Processed = 1 WHERE ISNULL(Processed,0) = 0
			UPDATE F0911z1 SET Processed = 1 WHERE ISNULL(Processed,0) = 0
            
			-- Update Invoices
				
				UPDATE acco 
				SET Mw_to_MW_Out = GETDATE()
				FROM Invoices acco
				INNER JOIN MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch accob ON (acco.Batch_Number = accob.Batch_number)
				WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' 
				and accob.mw_to_mw_NCC_out IS NULL 
			
			-- Update Batch
			
				UPDATE MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch  
				SET Mw_to_MW_NCC_Out = GETDATE(), Batch_Selected_NCC = 0
				WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' and MW_to_Mw_NCC_Out IS NULL

			IF(@@ERROR <> 0) GOTO Failure
			
      COMMIT TRANSACTION
      
      GOTO ENDFunction

Failure:
      ROLLBACK TRANSACTION

ENDFunction:


END




GO
