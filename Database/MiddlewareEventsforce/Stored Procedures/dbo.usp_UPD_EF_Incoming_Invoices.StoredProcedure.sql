USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_EF_Incoming_Invoices]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_EF_Incoming_Invoices]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_EF_Incoming_Invoices]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_EF_Incoming_Invoices]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM ##EF_Incoming_Invoices
	WHERE type = 'item' AND amount = '0.00'
	 AND [Description] <> 'Booking Cancellation' --Modified for WO52677
		  AND PaymentMethod IN ('Invoice','Bank Transfer','Cheque', 'Credit Card')
      
	INSERT INTO EF_Invoice_Orders_Batch VALUES ('MW_EVENTSFORCE', getdate(), NULL,NULL,NULL,1,1,NULL,NULL,NULL,'WAITING','WAITING')
	
	UPDATE ##EF_Incoming_Invoices
	SET Batch_Number = 	@@identity 

END



GO
