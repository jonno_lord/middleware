USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ConfigurationSettings]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_SEL_ConfigurationSettings]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_ConfigurationSettings]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects all from Configuration Settings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_ConfigurationSettings] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ConfigurationSettings
END



GO
