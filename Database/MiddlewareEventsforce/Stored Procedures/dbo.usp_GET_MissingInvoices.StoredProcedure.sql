USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_MissingInvoices]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_GET_MissingInvoices]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_MissingInvoices]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_MissingInvoices]
	-- Add the parameters for the stored procedure here
		@startFrom as VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--startFrom: 168494

    -- Insert statements for procedure here
	SELECT Document_Number FROM Invoices
	WHERE Document_Number > @startFrom
	AND Document_Number IS NOT NULL
	--UNION
	--SELECT Original_Invoice_Number FROM Invoices
	--WHERE Original_Invoice_Number > @startFrom
	--AND Original_Invoice_Number IS NOT NULL
	UNION
	SELECT Invoice_Number FROM Credit_Card_Invoices
	WHERE Invoice_Number > @startFrom
	AND Invoice_Number IS NOT NULL
	--UNION
	--SELECT ISNULL(Original_Invoice_Number,'') FROM Credit_Card_Invoices
	--WHERE Original_Invoice_Number > @startFrom
	ORDER BY Document_Number
		
END






GO
