USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceLineNumbers]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoiceLineNumbers]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceLineNumbers]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 31st March 2011
-- Description:	Selects distinct key catergory and name pairs from ConfigurationSettings
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoiceLineNumbers]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE i
	SET i.SFX = qrySFX.SFX
	FROM Invoices i
	LEFT JOIN Customers c on i.JDE_Account_No = c.JDE_Account_Number
	INNER JOIN MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch ccob ON (i.Batch_Number = ccob.Batch_number)
	INNER JOIN (SELECT ROW_NUMBER() OVER(PARTITION BY Document_Number ORDER BY Document_Number) as SFX, i.JDE_Account_No, i.Batch_Number, 
				Currency, Tax_Rate_CODE,i.Document_Number,i.Document_Type,i.Tax_Rate_Description,i.Invoice_Date,i.Business_Unit, 
				Object_Account, Subsidiary_Account,i.Customer_Name,i.Event_Start_Date,i.Booking_Reference,i.Event_Name,Original_Invoice_Number,
				c.ShipTo,'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), i.Event_Start_Date, 3), '/', ''),7,2)+ 
				SUBSTRING(REPLACE(CONVERT(VARCHAR(10), i.Event_Start_Date, 3), '/', ''),3,2) as StartDate,i.Payment_Method FROM Invoices i
				LEFT JOIN Customers c ON i.JDE_Account_No = c.JDE_Account_Number
				INNER JOIN MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch ccob ON (i.Batch_Number = ccob.Batch_number)
				WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' 
				and ccob.mw_to_mw_NCC_out IS NULL and Jde_Account_No IS NOT NULL
				and net_price <> 0
				GROUP BY i.JDE_Account_No, i.Batch_Number, Currency, Tax_Rate_CODE,i.Document_Number,i.Document_Type,
				i.Tax_Rate_Description,i.Invoice_Date,i.Business_Unit, Object_Account, Subsidiary_Account,i.Customer_Name,
				i.Event_Start_Date,i.Booking_Reference,i.Event_Name,Original_Invoice_Number,c.ShipTo, 
				'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), i.Event_Start_Date, 3), '/', ''),7,2)+ SUBSTRING(REPLACE(CONVERT(VARCHAR(10), i.Event_Start_Date, 3), '/', ''),3,2),
				i.Payment_Method) as qrySFX
	ON i.JDE_Account_No = qrySFX.JDE_Account_No and
	i.Batch_number = qrySFX.Batch_number and 
	i.Currency = qrySFX.Currency and 
	i.Tax_Rate_Code = qrySFX.Tax_Rate_Code and 
	i.Document_Number = qrySFX.Document_Number and 
	i.Document_Type = qrySFX.Document_Type and 
	i.Tax_rate_Description = qrySFX.Tax_rate_Description and 
	i.Invoice_date = qrySFX.Invoice_date and 
	i.Business_Unit = qrySFX.Business_Unit and
	i.Object_account= qrySFX.Object_account and 
	i.Subsidiary_account = qrySFX.Subsidiary_account and
	i.Customer_name = qrySFX.Customer_name and
	i.Event_Start_Date = qrySFX.Event_Start_Date and
	i.Booking_Reference = qrySFX.Booking_Reference and
	i.Event_Name = qrySFX.Event_Name and
	i.Original_Invoice_Number = qrySFX.Original_Invoice_Number and
	c.ShipTo = qrySFX.ShipTo and
	i.Customer_name = qrySFX.Customer_name and
	'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), i.Event_Start_Date, 3), '/', ''),7,2)+ 
				SUBSTRING(REPLACE(CONVERT(VARCHAR(10), i.Event_Start_Date, 3), '/', ''),3,2) = qrySFX.StartDate and 
	i.Payment_Method = qrySFX.Payment_Method
	WHERE Batch_Selected_NCC = 1 
	AND Generating_System = 'MW_EVENTSFORCE' 
	AND ccob.mw_to_mw_NCC_out IS NULL 
	AND i.JDE_Account_No IS NOT NULL
	AND net_price <> 0
	
	UPDATE i
	SET SFX = REPLICATE('0', 3 - LEN(SFX)) + SFX
	FROM Invoices i
	LEFT JOIN Customers c on i.JDE_Account_No = c.JDE_Account_Number
	INNER JOIN MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch ccob ON (i.Batch_Number = ccob.Batch_number)
	WHERE Batch_Selected_NCC = 1 
	AND Generating_System = 'MW_EVENTSFORCE' 
	AND ccob.mw_to_mw_NCC_out IS NULL 
	AND i.JDE_Account_No IS NOT NULL
	AND net_price <> 0
		
END


		


GO
