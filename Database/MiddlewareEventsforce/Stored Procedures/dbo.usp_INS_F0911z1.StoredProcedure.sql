USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_F0911z1]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_F0911z1]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_INS_F0911z1]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

-- Prime F0911z1
	
	TRUNCATE TABLE Staging_F0911z1

	INSERT INTO Staging_F0911z1
	(VNEDTN,VNEDLN,VNEDUS,VNEDBT,VNDCT,VNDOC,VNDGJ,VNOBJ,VNSBL,VNAA,VNEXA,VNEXR,VNACR,VNAN8,/*VNTXITM,VNEXR1,
	VNDLNID,*/VNTORG,PaymentMethod,VNEDCT,VNSFX,VNSTAM,VNCTAM,VNDICJ,VNDSYJ)

	SELECT  
		CONVERT(VARCHAR(20),pcco.Document_Number)																						AS VNEDTN,
		0																																AS VNEDLN, 
		'MW_EFORCE'																														AS VNEDUS,
		'MW_EF' +  CONVERT(VARCHAR(20),pcco.Batch_Number)																				AS VNEDBT,
		pcco.Document_Type																												AS VNDCT,
		CONVERT(VARCHAR(20),pcco.Document_Number)																						AS VNDOC, 
		dbo.fn_DateToOracleJulian(GETDATE())																							AS VNDGJ,
		Object_Account																													AS VNOBJ,
		'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), pcco.Event_Start_Date, 3), '/', ''),7,2) + 
				SUBSTRING(REPLACE(CONVERT(VARCHAR(10), pcco.Event_Start_Date, 3), '/', ''),3,2)											AS VNSBL,
		SUM(NET_PRICE) * -1																												AS VNAA,
		SUBSTRING(Customer_Name,1,30)																									AS VNEXA,
		SUBSTRING(pcco.Event_Name,1,200)																								AS VNEXR, 
		SUM(pcco.NET_PRICE) * -1																										AS VNACR,
		CONVERT(VARCHAR,pcco.Jde_Account_no)+ '-' + c.ShipTo																			AS VNAN8,	
		--CASE WHEN Document_Type = 'EJ' THEN Original_Invoice_Number ELSE NULL END														AS VNTXITM,
		--CASE WHEN Document_Type = 'EJ' THEN 'EI' ELSE NULL END																			AS VNEXR1,
		--CASE WHEN Document_Type = 'EJ' THEN dbo.fn_OriginalInvoiceLineNumber(Original_Invoice_Number,SUM(Gross_Price)) ELSE NULL END	AS VNDLNID,
		'MW_EFORCE'																														AS VNTORG,
		pcco.Payment_Method																												AS PaymentMethod,
		pcco.Document_Type																												AS VNEDCT,
		pcco.SFX																														AS VNSFX,
		SUM(pcco.Gross_price * -1) - SUM(pcco.Net_Price * -1)																			AS VNSTAM,
		SUM(pcco.Gross_price * -1) - SUM(pcco.Net_Price * -1)																			AS VNCTAM,
		dbo.fn_DateToOracleJulian(CONVERT(DATETIME,pcco.event_start_date,103))															AS VNDICJ,
		dbo.fn_DateToOracleJulian(CONVERT(DATETIME,pcco.event_end_date,103))															AS VNDSYJ
		FROM Invoices pcco
		INNER JOIN Customers c ON pcco.JDE_account_no = c.JDE_Account_Number
		INNER JOIN MiddlewareEventsForce.dbo.EF_Invoice_Orders_Batch ccob ON (pcco.Batch_Number = ccob.Batch_number)
		WHERE Batch_Selected_NCC = 1 and Generating_System = 'MW_EVENTSFORCE' 
		and ccob.mw_to_mw_NCC_out IS NULL and Jde_Account_No IS NOT NULL
		and net_price <> 0 and pcco.MW_to_MW_Out IS NULL
		GROUP BY pcco.JDE_Account_No, Currency,pcco.Document_Number,pcco.Document_Type,pcco.Customer_Name, pcco.Batch_Number,pcco.Business_Unit, Object_Account, 
		Subsidiary_Account, pcco.Event_Name,'20' + SUBSTRING(REPLACE(CONVERT(VARCHAR(10), pcco.Event_Start_Date, 3), '/', ''),7,2) + 
		SUBSTRING(REPLACE(CONVERT(VARCHAR(10), pcco.Event_Start_Date, 3), '/', ''),3,2),Original_Invoice_Number,c.ShipTo,pcco.Payment_Method,pcco.SFX, 
		pcco.Event_start_date, pcco.Event_End_Date
		

	-- Update VNEDLN
	
	--** Used for creating the transaction and line numbering details in the cursor
	DECLARE @VNEDTN AS INTEGER
	DECLARE @VNSFX AS VARCHAR(3)
	DECLARE @Line_Number AS INTEGER
	DECLARE @ID AS INTEGER

	--** Stores the previous batch details
	DECLARE @last_VNEDTN AS VARCHAR(10)
	DECLARE @last_VNSFX AS VARCHAR(3)

	--** Set up the last details so that
	--** they will break on the first occurence of entering the
	--** loop below...
	SET @last_VNEDTN = ''
	SET @last_VNSFX = ''

	--** begin the line number
	SET @Line_Number = 0

	--** Create a cursor based from the temporary table loaded  (grouped
	--** by the key fields of batch number, bucket account and order currency)
	DECLARE csrF0911z1 CURSOR LOCAL FORWARD_ONLY FOR 
		SELECT VNEDTN, VNSFX, [ID] FROM staging_F0911z1 
		ORDER BY VNEDTN

	--** Loop through this created cursor and update the transaction numbers
	OPEN csrF0911z1
	FETCH NEXT FROM csrF0911z1 INTO @VNEDTN, @VNSFX, @ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Increment the transaction number if the batch no, account or currency changed
		IF 	@last_VNEDTN <> @VNEDTN 
			SET @Line_Number = 1
		
		ELSE IF @last_VNEDTN = @VNEDTN AND @last_VNSFX <> @VNSFX
			SET @Line_Number = 1

		ELSE 		
			SET @Line_Number = @Line_Number + 1

		--** Update the table with the next value
		UPDATE staging_F0911z1 SET VNEDLN = @Line_Number WHERE [ID] = @ID

		--** store the previous batch details...
		SET @last_VNEDTN = @VNEDTN
		SET @last_VNSFX = @VNSFX

		--** Get the next entry
		FETCH NEXT FROM csrF0911z1 INTO @VNEDTN, @VNSFX, @ID
	END

	CLOSE csrF0911z1
	DEALLOCATE csrF0911z1


		INSERT INTO F0911z1
	(VNEDUS,VNEDTY,VNEDSQ,VNEDTN,VNEDCT,VNEDLN,VNEDTS,VNEDFT,VNEDDT,VNEDER,VNEDDL,VNEDSP,VNEDTC,VNEDTR,VNEDBT,VNEDGL,VNEDAN,VNKCO,VNDCT,VNDOC,VNDGJ,VNJELN,
	VNEXTL,VNPOST,VNICU,VNICUT,VNDICJ,VNDSYJ,VNTICU,VNCO,VNANI,VNAM,VNAID,VNMCU,VNOBJ,VNSUB,VNSBL,VNSBLT,VNLT,VNPN,VNCTRY,VNFY,VNFQ,VNCRCD,VNCRR,VNHCRR,VNHDGJ,VNAA,
	VNU,VNUM,VNGLC,VNRE,VNEXA,VNEXR,VNR1,VNR2,VNR3,VNSFX,VNODOC,VNODCT,VNOSFX,VNPKCO,VNOKCO,VNPDCT,VNAN8,VNCN,VNDKJ,VNDKC,VNASID,VNBRE,VNRCND,VNSUMM,VNPRGE,VNTNN,VNALT1,
	VNALT2,VNALT3,VNALT4,VNALT5,VNALT6,VNALT7,VNALT8,VNALT9,VNALT0,VNALTT,VNALTU,VNALTV,VNALTW,VNALTX,VNALTZ,VNDLNA,VNCFF1,VNCFF2,VNASM,VNBC,VNVINV,VNIVD,VNWR01,VNPO,VNPSFX,VNDCTO,VNLNID,VNWY,
	VNWN,VNFNLP,VNOPSQ,VNJBCD,VNJBST,VNHMCU,VNDOI,VNALID,VNALTY,VNDSVJ,VNTORG,VNREG#,VNPYID,VNUSER,VNPID,VNJOBN,VNUPMJ,VNUPMT,VNCRRM,VNACR,VNDGM,VNDGD,VNDGY,VNDG#,VNDICM,VNDICD,VNDICY,VNDIC#,VNDSYM,
	VNDSYD,VNDSYY,VNDSY#,VNDKM,VNDKD,VNDKY,VNDK#,VNDSVM,VNDSVD,VNDSVY,VNDSV#,VNHDGM,VNHDGD,VNHDGY,VNHDG#,VNDKCM,VNDKCD,VNDKCY,VNDKC#,VNIVDM,VNIVDD,VNIVDY,VNIVD#,PaymentMethod,VNTXITM,
	VNEXR1,VNDLNID,VNSTAM,VNCTAM
	)

		SELECT DISTINCT VNEDUS,VNEDTY,VNEDSQ,VNEDTN,VNEDCT,VNEDLN,VNEDTS,VNEDFT,VNEDDT,VNEDER,VNEDDL,VNEDSP,VNEDTC,VNEDTR,VNEDBT,VNEDGL,VNEDAN,VNKCO,VNDCT,VNDOC,VNDGJ,VNJELN,
	VNEXTL,VNPOST,VNICU,VNICUT,VNDICJ,VNDSYJ,VNTICU,VNCO,VNANI,VNAM,VNAID,VNMCU,VNOBJ,VNSUB,VNSBL,VNSBLT,VNLT,VNPN,VNCTRY,VNFY,VNFQ,VNCRCD,VNCRR,VNHCRR,VNHDGJ,VNAA,
	VNU,VNUM,VNGLC,VNRE,VNEXA,VNEXR,VNR1,VNR2,VNR3,VNSFX,VNODOC,VNODCT,VNOSFX,VNPKCO,VNOKCO,VNPDCT,VNAN8,VNCN,VNDKJ,VNDKC,VNASID,VNBRE,VNRCND,VNSUMM,VNPRGE,VNTNN,VNALT1,
	VNALT2,VNALT3,VNALT4,VNALT5,VNALT6,VNALT7,VNALT8,VNALT9,VNALT0,VNALTT,VNALTU,VNALTV,VNALTW,VNALTX,VNALTZ,VNDLNA,VNCFF1,VNCFF2,VNASM,VNBC,VNVINV,VNIVD,VNWR01,VNPO,VNPSFX,VNDCTO,VNLNID,VNWY,
	VNWN,VNFNLP,VNOPSQ,VNJBCD,VNJBST,VNHMCU,VNDOI,VNALID,VNALTY,VNDSVJ,VNTORG,VNREG#,VNPYID,VNUSER,VNPID,VNJOBN,VNUPMJ,VNUPMT,VNCRRM,VNACR,VNDGM,VNDGD,VNDGY,VNDG#,VNDICM,VNDICD,VNDICY,VNDIC#,VNDSYM,
	VNDSYD,VNDSYY,VNDSY#,VNDKM,VNDKD,VNDKY,VNDK#,VNDSVM,VNDSVD,VNDSVY,VNDSV#,VNHDGM,VNHDGD,VNHDGY,VNHDG#,VNDKCM,VNDKCD,VNDKCY,VNDKC#,VNIVDM,VNIVDD,VNIVDY,VNIVD#,PaymentMethod,VNTXITM,
	VNEXR1,VNDLNID,VNSTAM,VNCTAM
	from Staging_f0911z1 where paymentMethod in ('Invoice','Bank Transfer','Cheque')
	
  
	INSERT INTO GeneralLedger (f0911z1id, stage)
	SELECT f09.Id AS f0911z1id, 2 AS stage FROM f0911z1 f09
	INNER JOIN Staging_F0911z1 sf09 on F09.VNEDTN = sf09.VNEDTN AND F09.VNEDCT = SF09.VNEDCT AND F09.VNEDLN = sf09.VNEDLN
	WHERE f09.Processed IS NULL
		
	UPDATE F09 
	SET GeneralLedgerId = gl.id 
	FROM F0911z1 f09
	INNER JOIN GeneralLedger gl on gl.F0911z1id = f09.id
	WHERE gl.Stage = 2
	
END





GO
