USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_OutgoingPayments]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_CRE_OutgoingPayments]
GO
/****** Object:  StoredProcedure [dbo].[usp_CRE_OutgoingPayments]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CRE_OutgoingPayments]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..##EF_Outgoing_Payments') IS NOT NULL 
	DROP TABLE ##EF_Outgoing_Payments

	CREATE TABLE ##EF_Outgoing_Payments(
		[Sent_to_EventsForce] [datetime] NULL,
		[EFPYID] [varchar](50) NULL,
		[EFCKNU] [varchar](50) NULL,
		[Amount] [varchar](50) NULL,
		[Comment] [varchar](50) NULL,
		[Currency] [varchar](50) NULL,
		[RelatedInvoiceNumber] [varchar](50) NULL,
		[RelatedPurchaseID] [varchar](50) NULL,
		[TransactionReference] [varchar](50) NULL,
		[paymentMethod] [varchar](50) NULL
	) ON [PRIMARY]
END



GO
