USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceStatus]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_InvoiceStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_InvoiceStatus]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery106.sql|7|0|C:\Users\jpresly\AppData\Local\Temp\~vsF94A.sql
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_InvoiceStatus]

(@BatchNumber AS VARCHAR(10))

AS
BEGIN

UPDATE inv
SET inv.MW_to_JDE = acc.MiddlewareToJDE
FROM Invoices inv
  JOIN [F03B11Z1] f03  ON  inv.Document_number = f03.VJDOC 
  JOIN  AccountsReceivable acc ON acc.Id = f03.AccountsReceivableId 
   
  WHERE acc.MiddlewareToJDE is not null


END



GO
