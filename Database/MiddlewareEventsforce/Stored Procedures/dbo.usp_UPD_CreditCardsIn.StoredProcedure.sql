USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_CreditCardsIn]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_UPD_CreditCardsIn]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_CreditCardsIn]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_CreditCardsIn]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @InvoiceNumber AS VARCHAR(50) 
	DECLARE @OriginalInvoiceNumber AS VARCHAR(50)
	DECLARE @XML_Filename AS VARCHAR(50)
	DECLARE @Batch_Number AS INT
	DECLARE @PreviousInvoice AS VARCHAR(50)
		
	DECLARE @Net_Price AS DECIMAL(19,2)
	DECLARE @Net_Price_Sterling AS DECIMAL(19,2)
	DECLARE @Gross_Price AS DECIMAL(19,2)
	DECLARE @Temp_Gross_Price AS DECIMAL(19,2)
	DECLARE @Gross_Price_Sterling AS DECIMAL(19,2)  

	DECLARE @Total_Net_Price AS DECIMAL(19,2)
	DECLARE @Total_Gross_Price AS DECIMAL(19,2)
	DECLARE @Total_Temp_Gross_Price AS DECIMAL(19,2)

	DECLARE @VatAmount AS DECIMAL(19,2)
	DECLARE @Description AS VARCHAR(50)
	DECLARE @Basket_Amount AS DECIMAL(19,2)
	DECLARE @Basket_Net_Price AS DECIMAL(19,2)
	DECLARE @Basket_Gross_Price AS DECIMAL(19,2)

	DECLARE @ItemNumber AS INT
	DECLARE @EFOverrideAmount AS DECIMAL(19,2)
	DECLARE @itemCount AS INT
	DECLARE @GrandTotal AS DECIMAL(19,2)
	DECLARE @TotalOverrideAmount AS DECIMAL(19,2)
	DECLARE @TotalGTAmount AS DECIMAL(19,2)

	SET @PreviousInvoice = ''
	SET @itemCount = 0
	SET @GrandTotal = 0
	SET @EFOverrideAmount = 0

	DECLARE csrInvoiceRecord CURSOR FOR 

	SELECT InvoiceNumber, Batch_Number, Item_Number
	FROM ##EF_Incoming_Invoices
	WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
	GROUP BY InvoiceNumber, Batch_Number, Item_Number
	ORDER BY InvoiceNumber  

	OPEN csrInvoiceRecord

		FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber, @Batch_Number, @ItemNumber

		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SET @Net_Price = 0
			SET @Total_Net_Price = 0
			SET @Temp_Gross_Price = 0
			SET @Total_Temp_Gross_Price = 0
			SET @VatAmount = 0

		IF (@PreviousInvoice <> @InvoiceNumber ) -- check whether the invoice has over ride type	
		BEGIN 

			SET @PreviousInvoice = @InvoiceNumber 

			SET @EFOverrideAmount = 0
			SET @GrandTotal = 0
			
			SELECT @EFOverrideAmount = CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(Amount,0)) ) 
			FROM ##EF_Incoming_Invoices 
			WHERE [Type] = 'Override' 
			AND InvoiceNumber = @InvoiceNumber 

			SELECT @GrandTotal = CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(Amount,0)) ) 
			FROM ##EF_Incoming_Invoices 
			WHERE type='GrandTotal' 
			AND InvoiceNumber = @InvoiceNumber 

			SET @itemCount = (SELECT MAX(Item_Number) FROM ##EF_Incoming_Invoices WHERE InvoiceNumber = @InvoiceNumber)
			SET @TotalOverrideAmount = 0
			SET @TotalGTAmount = 0

		END

		IF (@EFOverrideAmount IS NOT NULL AND @EFOverrideAmount <> 0 ) ----prorated override calculation
		BEGIN 

			IF(@itemCount - @ItemNumber > 0)
			BEGIN
			----Calculate prorated net price i.e Override amount
			SELECT @Net_Price = SUM(CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(Amount,0))))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
			AND (([Type] = 'Item') OR ([Type] IN ('Discount','Surcharge') AND [Description] NOT LIKE '%vat%'))  --Modified for WO49126
			AND InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			AND Item_number = @ItemNumber
			group by InvoiceNumber, Item_number

			SELECT @Total_Temp_Gross_Price = SUM(CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(Amount,0))))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
			AND (([Type] = 'Item') OR ([Type] = 'Discount') OR ([Type] = 'Surcharge')) 
			AND InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			GROUP BY InvoiceNumber
		
			IF( @Total_Temp_Gross_Price = 0 )
			BEGIN
				SET @Total_Temp_Gross_Price = 1
			END
		
			SET @Net_Price  = (@Net_Price * @GrandTotal / @Total_Temp_Gross_Price)
		
			UPDATE Credit_Card_invoices
			SET Net_Price = @Net_Price, Net_Price_Sterling = @Net_Price
			WHERE Invoice_Number = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			AND Line_number = @ItemNumber

			----Calculate prorated Gross price i.e Grand total
			SELECT @Temp_Gross_Price = SUM(CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(Amount,0)) ))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
			AND (([Type] = 'Item') OR ([Type] = 'Discount') OR ([Type] = 'Surcharge' )) 
			AND InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			AND Item_number = @ItemNumber
			group by InvoiceNumber, Item_number

			SET @Temp_Gross_Price = (@Temp_Gross_Price * @GrandTotal / @Total_Temp_Gross_Price )
		
			UPDATE Credit_Card_invoices
			SET Gross_Price = @Temp_Gross_Price, Gross_Price_Sterling = @Temp_Gross_Price
			WHERE Invoice_Number = @InvoiceNumber 
			AND MW_Batch_File_Name = @XML_Filename 
			AND Batch_Number = @Batch_Number
			AND Line_number = @ItemNumber
		
			SET  @TotalOverrideAmount = @TotalOverrideAmount + @Net_Price
			SET  @TotalGTAmount = @TotalGTAmount + @Temp_Gross_Price
		
		  END --- if end

		  ELSE --If this is last item in the Invoice Subtact first items price from XML total price
		  BEGIN 
		   
		   SET @Net_Price = @EFOverrideAmount - @TotalOverrideAmount 
		   SET @Temp_Gross_Price = @GrandTotal- @TotalGTAmount

		   UPDATE Credit_Card_invoices
		   SET Net_Price = @Net_Price, Net_Price_Sterling = @Net_Price
		   WHERE Invoice_Number = @InvoiceNumber 
		   AND Batch_Number = @Batch_Number 
		   AND Line_number = @ItemNumber

		   UPDATE Credit_Card_invoices
		   SET Gross_Price = @Temp_Gross_Price, Gross_Price_Sterling = @Temp_Gross_Price
 		   WHERE Invoice_Number = @InvoiceNumber 
		   AND Batch_Number = @Batch_Number 
		   AND Line_number = @ItemNumber

		  END ---else 
		END --if 

		ELSE ---Non override invoice calculation
		BEGIN 
		
			SELECT @Net_Price = SUM(CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(Amount,0))))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
			AND (([Type] = 'Item') OR ([Type] IN ('Discount','Surcharge') AND [Description] NOT LIKE '%vat%'))  --Modified for WO49126
			AND InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number 
			AND Item_Number = @ItemNumber
			GROUP BY InvoiceNumber, Item_Number

			SELECT @Temp_Gross_Price = SUM(CONVERT(DECIMAL(19,2), CONVERT(MONEY,ISNULL(amount,0)) ))
			FROM ##EF_Incoming_Invoices 
			WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
			AND [Type] IN ('Surcharge','Discount') 
			AND [Description] LIKE '%vat%'  --Modified for WO49126
			AND InvoiceNumber = @InvoiceNumber 
			AND Batch_Number = @Batch_Number  
			AND Item_Number = @ItemNumber
			GROUP BY InvoiceNumber, Item_Number
				
			SET @Total_Gross_Price = @Net_Price + ISNULL(@Temp_Gross_Price,0)
			SET @PreviousInvoice = @InvoiceNumber 

			UPDATE Credit_Card_invoices
			SET Net_Price = @Net_Price,Net_Price_Sterling = @Net_Price, Gross_Price = @Total_Gross_Price, Gross_Price_Sterling = @Total_Gross_Price			
			WHERE Invoice_Number = @InvoiceNumber
			AND Batch_Number = @Batch_Number 
			AND Line_Number = @ItemNumber

			END  -- end else stament for Non override invoice

			FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber, @Batch_Number, @ItemNumber
		
		END  --while loop

CLOSE csrInvoiceRecord
DEALLOCATE csrInvoiceRecord

--- PART 2 @JP
DECLARE @Tax_Rate_Code AS VARCHAR(50)
DECLARE @Tax_Rate_Description AS VARCHAR(10)
DECLARE @Old_VAT AS CHAR

DECLARE csrInvoiceRecord CURSOR FOR 

SELECT InvoiceNumber
FROM ##EF_Incoming_Invoices
WHERE PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
GROUP BY InvoiceNumber
ORDER BY InvoiceNumber  

OPEN csrInvoiceRecord

	FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber

	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SELECT @Tax_Rate_Code = SUBSTRING(RTRIM([Description]), CHARINDEX('(', RTRIM([description])) + 1, CHARINDEX(')', RTRIM([Description])) - CHARINDEX('(', RTRIM([Description])) -1) 
		FROM ##EF_Incoming_Invoices 
		WHERE [Description] LIKE 'VAT (%' 
		AND PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
		and InvoiceNumber = @InvoiceNumber

		--Added by Offshore team on 3rd Dec,2008
		SELECT @Tax_Rate_Description = '+' + substring(UnitPrice, 0,charindex('.', UnitPrice, 0)) + '%'
		FROM ##EF_Incoming_Invoices 
		WHERE [Description] LIKE 'VAT (%' 
		AND PaymentMethod NOT IN ('Invoice','Bank Transfer','Cheque')
		AND InvoiceNumber = @InvoiceNumber

		--Code added by Offshore for CR32760 WO48106		
		IF @Tax_rate_Description = '+15%'  --Modified for WO52357
		BEGIN 
			SET @Old_VAT = '1'
		END
		ELSE
		BEGIN
			SET @Old_VAT = '0'
		END 
		
		-- Update the Row
		UPDATE Credit_Card_Invoices
        SET Tax_Rate_Code = SUBSTRING(@Tax_Rate_Code, 1, 10), Tax_Rate_Description = @Tax_Rate_Description, Old_VAT = @Old_VAT
		WHERE Invoice_Number = @InvoiceNumber
	
		SET @Tax_Rate_Code = NULL
		SET @Tax_Rate_Description = NULL
		SET @Old_VAT = NULL

		FETCH NEXT FROM csrInvoiceRecord INTO @InvoiceNumber 
	END

CLOSE csrInvoiceRecord
DEALLOCATE csrInvoiceRecord

-- PART 3 @JP

	UPDATE Credit_Card_Invoices
	SET MW_To_MW_COnversion = GETDATE(), Invoice_Date = GETDATE()
	WHERE MW_To_MW_Conversion IS NULL 

END



GO
