USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ConfigurationSettingGrouped]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[USP_GET_ConfigurationSettingGrouped]
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ConfigurationSettingGrouped]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 4th April 2011
-- Description:	Selects configuration settings across multiple environments (DEV, UAT, Production)
-- =============================================
CREATE PROCEDURE [dbo].[USP_GET_ConfigurationSettingGrouped] 
	-- Add the parameters for the stored procedure here
	@category AS NVARCHAR(50),
	@name AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ConfigurationSettings
	WHERE keyCategory = @category AND keyName = @name
END



GO
