USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJdeFormat]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToJdeFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToJdeFormat]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TRA_CustomersToJdeFormat]
(@BatchNumber AS VARCHAR(10))
AS
BEGIN

	
SET NOCOUNT ON
      
      BEGIN TRANSACTION

		EXECUTE dbo.usp_INS_F0101z2 @BatchNumber
		IF(@@ERROR <> 0) GOTO Failure
    
		EXECUTE dbo.usp_INS_F03012z1 @BatchNumber            
		IF(@@ERROR <> 0) GOTO Failure
		
		--EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerPaymentTerms @SystemName = 'MiddlewareEventsForce'
		--IF(@@ERROR <> 0) GOTO Failure
		
		--EXECUTE MiddlewareCommon.dbo.usp_UPD_CustomerTaxCode @SystemName = 'MiddlewareEventsForce'
		--IF(@@ERROR <> 0) GOTO Failure
		
		-- Mark all customers as ready for the next stage
		UPDATE C SET 
			Stage = 2, 
			MiddlewaretoMiddlewareIn = GETDATE(),
			Last_Affected = GETDATE()
			FROM Customers C INNER JOIN F0101Z2 F ON (F.SZEDTN = C.ID)
			WHERE Stage = 1 

		IF(@@ERROR <> 0) GOTO Failure

      COMMIT TRANSACTION
      
      GOTO ENDFunction

Failure:
      ROLLBACK TRANSACTION

ENDFunction:


END





GO
