USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_LatestInvoice]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_GET_LatestInvoice]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_LatestInvoice]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_LatestInvoice]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @retValue AS INT

	SELECT @retValue = MAX(document_number) FROM 
	(
	SELECT MAX(CONVERT(INT,Document_Number)) AS Document_Number FROM Invoices
	--UNION
	--SELECT MAX(CONVERT(INT,Original_Invoice_Number)) FROM Invoices
	UNION
	SELECT MAX(CONVERT(INT,Invoice_Number)) FROM Credit_Card_Invoices
	--UNION
	--SELECT MAX(CONVERT(INT,ISNULL(Original_Invoice_Number,0))) FROM Credit_Card_Invoices
	) AS sQuery

	RETURN @retValue
END






GO
