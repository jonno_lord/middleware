USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToEventsForceFormat]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_TRA_CustomersToEventsForceFormat]
GO
/****** Object:  StoredProcedure [dbo].[usp_TRA_CustomersToEventsForceFormat]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TRA_CustomersToEventsForceFormat]
	-- Add the parameters for the stored procedure here
	(@BatchNumber as int = 0)
AS
BEGIN

	SET NOCOUNT ON
	
	UPDATE Customers SET 
		Stage = 5, 
		MiddlewareToMiddlewareOut = GETDATE(),
		JDE_Account_Number = F55.Q1AN8,
		Last_Affected = GETDATE() 
		
		FROM F550101 F55
		INNER JOIN Customers C ON F55.Q1URRF = C.URN_Number
		WHERE Stage > 3

END




GO
