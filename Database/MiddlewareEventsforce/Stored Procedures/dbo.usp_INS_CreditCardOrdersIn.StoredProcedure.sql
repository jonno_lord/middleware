USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditCardOrdersIn]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_CreditCardOrdersIn]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_CreditCardOrdersIn]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_CreditCardOrdersIn]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO [MiddlewareEventsForce].[dbo].[Credit_Card_Invoices]
		   ([Customer_name]
		    ,[Address]
			,[Town]
			,[Postcode]
			,[Country]
		    ,[Source_to_MW]
			,[Invoice_date]
			,[Order_Date]
			,[VAT_Registration_No]
			,[Payment_Method]
			,[Currency]
			,[JDE_Account_No]
			,[Business_Unit]
			,[Object_account]
			,[Subsidiary_account]
			,[Document_Type]
			,[Invoice_Number] 
			,[Original_Invoice_Number]
			,[Event_Name]
		    ,[Event_Start_Date]
			,[Event_End_Date]
			,[CodID]
		    ,[Batch_number]
		    ,[Line_Number]
		    ,[Booking_Reference]
		    ,[Hub_Company]
		    ,[Document_Number]
			,[TransactionPOSId])
		   

   					
	SELECT DISTINCT 
	SUBSTRING(ISNULL(FirstName,'') + ' ' + ISNULL(Lastname,''),1,50) As Customer_Name,			
	SUBSTRING( ISNULL(Addressline1,'') + ISNULL(' ,'+ Addressline2 ,' ')  + ISNULL(', ' + Addressline3,' '),1,100) As Address,	
	inv.City as town,
	inv.PostCode as postcode,
	inv.Country,
	GETDATE() as Source_to_MW,
	PurchaseCreated as Invoice_Date,
	PurchaseCreated As Order_Date,
	VATRegistrationNumber As Vat_Registration_No,		
	PaymentMethod As Payment_Method,					 
	CurrencyCode As Currency,
	CAST(ExternalCustomerAccount as int) As JDE_Account_No,
	BusinessUnit As Business_Unit,	
	extItemCode As Object_account,
	extItemCode2 As Subsidiary_account,
	'EK' as Document_Type,
	InvoiceNumber AS Invoice_Number,				 
	OriginalInvoiceNumber As Original_Invoice_Number,
	EventName as Event_Name,
	SUBSTRING(EventStartDate,9,2) + '/' + SUBSTRING(EventStartDate,6,2) + '/' + SUBSTRING(EventStartDate,1,4) As Event_Start_Date,		
	SUBSTRING(EventEndDate,9,2) + '/' + SUBSTRING(EventEndDate,6,2) + '/' + SUBSTRING(EventEndDate,1,4) As Event_End_Date,		
	inv.CodID As CodID,
	inv.Batch_Number as Batch_Number,
	Item_Number as Line_Number,
	BookingReference as Booking_Reference,
	jbu.Company as Hub_Company,
	InvoiceNumber as Document_Number,
	TransactionReference as TransactionPOSId
	FROM ##EF_Incoming_Invoices inv
	LEFT JOIN MiddlewareEventsForce.dbo.JDE_Business_Units jbu ON jbu.business_unit = ISNULL(inv.businessunit,'')
	--LEFT JOIN MiddlewareEventsForce.dbo.Credit_Card_Invoices cc ON inv.InvoiceNumber = ISNULL(cc.Invoice_Number,'')
	WHERE PaymentMethod = 'Credit Card' AND [type] = 'item'
	
END



GO
