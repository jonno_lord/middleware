USE [MiddlewareEventsForce]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_IncomingInvoices]    Script Date: 10/04/2018 10:39:34 ******/
DROP PROCEDURE [dbo].[usp_INS_IncomingInvoices]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_IncomingInvoices]    Script Date: 10/04/2018 10:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_IncomingInvoices]
(
	@XML_Filename AS varchar(50),
	@invoiceNumber AS varchar(50),
    @originalInvoiceNumber AS varchar(50),
    @externalpersonid AS varchar(50),
    @codID AS varchar(50),
    @externalcustomerid AS varchar(50),
    @externalcustomeraccount AS varchar(50),
    @exportStatus AS varchar(50),
    @exportTimeStamp AS datetime,
    @bookingReference AS varchar(50),
    @externalPurchaseReference AS varchar(50),
    @purchaseType AS varchar(50),
    @purchaseStatus AS varchar(50),
    @purchaseCreated AS datetime,
    @purchaseLastChanged AS datetime,
    @eventName AS  varchar(500),
    @eventid AS  varchar(50),
    @eventStartDate AS varchar(50),
    @eventEndDate AS varchar(50),
    @currencyCode AS varchar(50),
    @totalPrice AS decimal(18, 2),
    @vat AS decimal(18, 2),
    @paymentMethod AS varchar(50),
    @firstname AS varchar(50),
    @lastname AS varchar(50),
    @salutation AS varchar(50),
    @jobTitle AS varchar(50),
    @addressLine1 AS varchar(50),
    @addressLine2 AS varchar(50),
    @addressLine3 AS varchar(50),
    @city AS varchar(50),
    @postcode AS varchar(50),
    @country AS varchar(50),
    @countryCode AS varchar(50),
    @company AS varchar(100),
    @email AS varchar(500),
    @telephone AS varchar(50),
    @bookedBy AS varchar(50),
    @costCentre AS varchar(250),
    @chequeNumber AS varchar(250),
    @town AS varchar(250),
    @duedays AS varchar(50),
    @creditController AS varchar(250),
    @vatRegistrationNumber AS varchar(50),
    @taxExemptCertificate AS varchar(50),
    @businessUnit AS varchar(250),
    @venue AS varchar(250),
	@customerPOnumber AS varchar(50),
	
	@NumberOfAttendees AS varchar(50),

	@Type AS varchar(50),
	@Description AS varchar(150),
	@Quantity AS varchar(50),
	@UnitPrice AS varchar(50),
	@Amount AS varchar(50),
	@extItemCode AS varchar(50),
	@extItemCode2 AS varchar(50),
	@PurchaseID AS varchar(50),
	@lnkInvoiceRun AS varchar(50),
	@Item_Number as int

)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO ##EF_Incoming_Invoices
           ([XML_Filename]
           ,[InvoiceNumber]
           ,[OriginalInvoiceNumber]
           ,[Externalpersonid]
           ,[CodId]
           ,[Externalcustomerid]
           ,[Externalcustomeraccount]
           ,[ExportStatus]
           ,[ExportTimeStamp]
           ,[BookingReference]
           ,[ExternalPurchaseReference]
           ,[PurchaseType]
           ,[PurchaseStatus]
           ,[PurchaseCreated]
           ,[PurchaseLastChanged]
           ,[EventName]
           ,[Eventid]
           ,[EventStartDate]
           ,[EventEndDate]
           ,[CurrencyCode]
           ,[TotalPrice]
           ,[VAT]
           ,[PaymentMethod]
           ,[Firstname]
           ,[Lastname]
           ,[Salutation]
           ,[JobTitle]
           ,[addressLine1]
           ,[addressLine2]
           ,[addressLine3]
           ,[city]
           ,[postcode]
           ,[country]
           ,[CountryCode]
           ,[company]
           ,[Email]
           ,[Telephone]
           ,[BookedBy]
           ,[CostCentre]
           ,[ChequeNumber]
           ,[town]
           ,[Duedays]
           ,[CreditController]
           ,[VATRegistrationNumber]
           ,[TaxExemptCertificate]
           ,[BusinessUnit]
           ,[Venue]
           ,[CustomerPOnumber]
           	,[NumberOfAttendees]
			
			,[Type]
			,[Description]
			,[Quantity]
			,[UnitPrice]
			,[Amount]
			,[extItemCode]
			,[extItemCode2]
			,[PurchaseID]
			,[lnkInvoiceRun]
			,[Item_Number])

     VALUES
      (
			@XML_Filename,
			@invoiceNumber,
			@originalInvoiceNumber,
			@externalpersonid,
			@codID,
			@externalcustomerid,
			@externalcustomeraccount,
			@exportStatus,
			@exportTimeStamp,
			@bookingReference,
			@externalPurchaseReference,
			@purchaseType,
			@purchaseStatus,
			@purchaseCreated,
			@purchaseLastChanged,
			@eventName,
			@eventid,
			@eventStartDate,
			@eventEndDate,
			@currencyCode,
			@totalPrice,
			@vat,
			@paymentMethod,
			@firstname,
			@lastname,
			@salutation,
			@jobTitle,
			@addressLine1,
			@addressLine2,
			@addressLine3,
			@city,
			@postcode,
			@country,
			@countryCode,
			@company,
			@email,
			@telephone,
			@bookedBy,
			@costCentre,
			@chequeNumber,
			@town,
			@duedays,
			@creditController,
			@vatRegistrationNumber,
			@taxExemptCertificate,
			@businessUnit,
			@venue,
			@customerPOnumber,
			
				@NumberOfAttendees,
			
				@Type,
				@Description,
				@Quantity,
				@UnitPrice,
				@Amount,
				@extItemCode,
				@extItemCode2,
				@PurchaseID,
					@lnkInvoiceRun,
				@Item_Number
		)

END




GO
