USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_LongAddressNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LongAddressNumber]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_LongAddressNumber](@Urn as varchar(15))
returns varchar(16)
as
begin
	
	declare @ret as VARCHAR(16)
	
	SET @ret = @Urn + 'N'
					  
	RETURN @ret
	
END




GO
