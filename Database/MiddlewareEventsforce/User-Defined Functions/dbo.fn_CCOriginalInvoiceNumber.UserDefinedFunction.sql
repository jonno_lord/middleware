USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CCOriginalInvoiceNumber]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_CCOriginalInvoiceNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CCOriginalInvoiceNumber]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_CCOriginalInvoiceNumber](@DocumentNumber AS VARCHAR(10), @GrossPrice AS DECIMAL(19,2))
RETURNS VARCHAR(10)
AS
BEGIN
	
	DECLARE @ret AS VARCHAR(10)
	
	IF(@GrossPrice < 0)
	BEGIN
		SELECT @ret = Original_Invoice_Number FROM Invoices
		WHERE Document_Number = @DocumentNumber 
	END
	
	RETURN @ret
	
END







GO
