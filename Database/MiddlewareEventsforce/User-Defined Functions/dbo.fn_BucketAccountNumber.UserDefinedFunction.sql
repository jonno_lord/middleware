USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BucketAccountNumber]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_BucketAccountNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BucketAccountNumber]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_BucketAccountNumber]
(
	@Currency AS VARCHAR(50)
)
RETURNS FLOAT
AS
BEGIN
	
	DECLARE @Ret AS FLOAT
	
	SELECT @Ret = CASE WHEN @Currency = 'USD' THEN 2999961
					WHEN @Currency = 'EUR' THEN 2999962
					ELSE 2999960 END
			
	RETURN @Ret
	
END






GO
