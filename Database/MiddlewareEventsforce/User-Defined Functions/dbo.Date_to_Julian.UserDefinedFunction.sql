USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[Date_to_Julian]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[Date_to_Julian]
GO
/****** Object:  UserDefinedFunction [dbo].[Date_to_Julian]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[Date_to_Julian] (@GregDate AS DATETIME)  
RETURNS VARCHAR(20) AS  
BEGIN 

	DECLARE @Day AS INT
	DECLARE @Year AS INT
	DECLARE @Century as BIT

	SET @Century =0
	SET @Year = YEAR(@GregDate)


	SET @Day = Datediff("y", CONVERT(DateTime, CONVERT(VARCHAR(4), @Year) +'/01/01'), @GregDate) + 1

	IF @Year > 2000 SET @Century =1

	RETURN  CONVERT(VARCHAR(20), CONVERT(Varchar(1), @Century) + RIGHT(Convert(Varchar(4), @Year), 2) +
	REPLICATE('0', 3 - LEN(RTRIM(LTRIM(CONVERT(VARCHAR(3), @Day))))) +
	CONVERT(VARCHAR(3), @Day))

END






GO
