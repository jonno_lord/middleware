USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SearchType]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_SearchType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SearchType]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_SearchType]()
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = 'CN'
					  
	RETURN @ret
	
END





GO
