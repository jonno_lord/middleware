USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_DunningCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningCode]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_DunningCode]
(
	@Country_Code AS varchar(3)
)
returns float
as
begin
	
	DECLARE @Dunning as float
	
	IF @Country_Code = 'GB'	 
		SELECT @Dunning =  RFPLY FROM [MiddlewareJDE].[dbo].[F03B25] WHERE RTRIM(RFPLYN) = 'ES' AND RFPLY <> 0
	ELSE
		SELECT @Dunning = RFPLY FROM [MiddlewareJDE].[dbo].[F03B25] WHERE RTRIM(RFPLYN) = 'EF' AND RFPLY <> 0
	
	RETURN @Dunning
	
END





GO
