USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TelephoneType]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_TelephoneType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TelephoneType]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_TelephoneType]
(
	@Telephone_number AS varchar (50)
)
returns varchar(3)
as
begin
	
	declare @ret as VARCHAR(3)
	
	SET @ret = 'TEL'
	
	IF ISNULL(@Telephone_number, '') = '' OR @Telephone_number = ''
		SET @ret = ''
					  
	RETURN @ret
	
END





GO
