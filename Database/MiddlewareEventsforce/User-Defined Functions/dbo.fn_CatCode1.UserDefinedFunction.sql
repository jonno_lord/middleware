USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode1]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_CatCode1]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CatCode1]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_CatCode1]
(
	@Credit_Controller_Initials as nvarchar(50)
)
returns varchar(1)
as
begin
	
	declare @ret as VARCHAR(1)
	
	SET @ret = 'E'
	
	IF @Credit_Controller_Initials = 'LGA'
		SET @ret = 'H'
				  
	RETURN @ret
	
END




GO
