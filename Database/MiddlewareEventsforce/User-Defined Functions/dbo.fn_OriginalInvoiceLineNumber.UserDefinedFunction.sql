USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceLineNumber]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_OriginalInvoiceLineNumber]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_OriginalInvoiceLineNumber]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_OriginalInvoiceLineNumber]
(
	@OriginalInvoice AS VARCHAR(10),
	@Gross AS DECIMAL(19,2)
)

RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @ret AS VARCHAR(10)
	
	BEGIN
		SELECT @ret = SFX FROM Invoices
		WHERE Document_Number = @OriginalInvoice
		AND ABS(Gross_Price) = ABS(@Gross)
	END
	
	RETURN @ret
	
END


GO
