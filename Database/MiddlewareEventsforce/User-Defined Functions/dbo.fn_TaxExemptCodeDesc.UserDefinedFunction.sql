USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCodeDesc]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_TaxExemptCodeDesc]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCodeDesc]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_TaxExemptCodeDesc]
(
	@TaxExemptNumber AS varchar(50)
)
returns varchar(5)
as
begin
	
	declare @ret as VARCHAR(5)
	
	SET @ret = 'GBSTD'
	
	IF @TaxExemptNumber != '' AND @TaxExemptNumber IS NOT NULL
		SET @ret = 'GBEXEMPT'
				  
	RETURN @ret
	
END





GO
