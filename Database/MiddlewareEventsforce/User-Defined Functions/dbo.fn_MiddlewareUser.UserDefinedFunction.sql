USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MiddlewareUser]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_MiddlewareUser]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MiddlewareUser]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_MiddlewareUser]()
returns varchar(9)
as
begin
	
	declare @ret as VARCHAR(9)
	
	SET @ret = 'MW_EFORCE'
	
				  
	RETURN @ret
	
END





GO
