USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyName]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_CompanyName]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CompanyName]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fn_CompanyName]
(
	@CompanyName AS NVARCHAR(100),
	@FirstName AS NVARCHAR(50),
	@LastName AS NVARCHAR(50)
)
RETURNS NVARCHAR(100)
AS
BEGIN
	
	DECLARE @Ret AS VARCHAR(100)
	SET @Ret = ''
	
	IF (ISNULL(@CompanyName,'') = '') 
		SET @Ret = @FirstName + ' ' + @LastName
	ELSE
		SET @Ret = @CompanyName
				  
	RETURN @Ret
	
END





GO
