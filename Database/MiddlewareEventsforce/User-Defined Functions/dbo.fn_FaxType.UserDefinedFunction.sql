USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FaxType]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_FaxType]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_FaxType]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_FaxType]()
returns varchar(3)
as
begin
	
	declare @ret as VARCHAR(3)
	
	SET @ret = 'FAX'
					  
	RETURN @ret
	
END





GO
