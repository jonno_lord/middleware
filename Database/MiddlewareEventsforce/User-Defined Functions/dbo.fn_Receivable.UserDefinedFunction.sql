USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Receivable]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_Receivable]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Receivable]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_Receivable]()
returns varchar(1)
as
begin
	
	declare @ret as VARCHAR(1)
	
	SET @ret = 'Y'
					  
	RETURN @ret
	
END





GO
