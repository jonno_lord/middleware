USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCode]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_TaxExemptCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TaxExemptCode]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_TaxExemptCode]
(
	@TaxExemptNumber AS varchar(50)
)
returns varchar(1)
as
begin
	
	declare @ret as VARCHAR(1)
	
	SET @ret = 'V'
	
	IF @TaxExemptNumber != '' AND @TaxExemptNumber IS NOT NULL
		SET @ret = 'E'
				  
	RETURN @ret
	
END





GO
