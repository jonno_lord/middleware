USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_CreditLimit]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CreditLimit]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_CreditLimit]()
returns varchar(5)
as
begin
	
	declare @ret as VARCHAR(5)
	
	SET @ret = '25000'
	
				  
	RETURN @ret
	
END





GO
