USE [MiddlewareEventsForce]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningLetters]    Script Date: 10/04/2018 10:40:09 ******/
DROP FUNCTION [dbo].[fn_DunningLetters]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DunningLetters]    Script Date: 10/04/2018 10:40:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fn_DunningLetters]
(
	@Country_code as varchar(2)
)
returns varchar(2)
as
begin
	
	declare @ret as VARCHAR(2)
	
	SET @ret = 'EF'
	
	IF @Country_code = 'GB'
		SET @ret = 'ES'
	
				  
	RETURN @ret
	
END





GO
