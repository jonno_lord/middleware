USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_FileDetails]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_UPD_FileDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_FileDetails]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 25th January 2011
-- Description:	Checks to see if a file has been processed
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_FileDetails] 
	-- Add the parameters for the stored procedure here
@FileDetailid AS INT, @Status AS VARCHAR(20), @FTPDate AS DATETIME = NULL, @MovedFileDate AS DATETIME = NULL, @FileSize AS INT = NULL
AS
BEGIN

	UPDATE FileDetails SET Status = @Status WHERE id = @FileDetailid

	IF(@FTPDate IS NOT NULL)
		UPDATE FileDetails SET FTPDate = @FtpDate WHERE id = @FileDetailid

	IF(@MovedFileDate IS NOT NULL)
		UPDATE FileDetails SET MovedFileDate = @MovedFileDate WHERE id = @FileDetailid
	
	IF(@FileSize IS NOT NULL)
		UPDATE FileDetails SET FileSize = @FileSize WHERE id = @FileDetailid
		
	IF(@FTPDate IS NULL AND @MovedFileDate IS NULL)
		UPDATE FileDetails SET FileName = 'No files found', FilePath = '' WHERE id = @FileDetailid
	
END

GO
