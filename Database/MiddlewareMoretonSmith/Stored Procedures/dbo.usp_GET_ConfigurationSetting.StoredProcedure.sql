USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ConfigurationSetting]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_GET_ConfigurationSetting]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_ConfigurationSetting]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Selects specific Configuration Setting by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_ConfigurationSetting] 
	-- Add the parameters for the stored procedure here
	@id AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM ConfigurationSettings
	WHERE id = @id
END

GO
