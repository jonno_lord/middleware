USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ConfigurationSettings]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_SAV_ConfigurationSettings]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAV_ConfigurationSettings]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elizabeth Hamlet
-- Create date: 1st April 2011
-- Description:	Inserts or updates Configuration Settings
-- =============================================
CREATE PROCEDURE [dbo].[usp_SAV_ConfigurationSettings] 
	-- Add the parameters for the stored procedure here
	@id AS INT,
	@keyCategory AS NVARCHAR(50),
	@keyName AS NVARCHAR(50),
	@keyValue AS NVARCHAR(2048),
	@keyDataType AS NVARCHAR(50),
	@EnvironmentName AS NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT * FROM ConfigurationSettings WHERE id = @id)
	BEGIN
		UPDATE ConfigurationSettings SET keyCategory = @keyCategory, keyName = @keyName, keyValue = @keyValue, keyDataType = @keyDataType, EnvironmentName = @EnvironmentName
		WHERE id = @id 
	END
	ELSE
	BEGIN
		INSERT INTO ConfigurationSettings(keyCategory, keyName, keyValue, keyDataType, EnvironmentName)
		VALUES (@keyCategory, @keyName, @keyValue, @keyDataType, @EnvironmentName)
	END
END

GO
