USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TodaysSummaryReports]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_GET_TodaysSummaryReports]
GO
/****** Object:  StoredProcedure [dbo].[usp_GET_TodaysSummaryReports]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GET_TodaysSummaryReports]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [Summary]
	FROM [MiddlewareMoretonSmith].[dbo].[SummaryReports]
  
	WHERE RunDate > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
END

GO
