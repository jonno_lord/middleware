USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_FileDetailHistory]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_INS_FileDetailHistory]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_FileDetailHistory]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 25th January 2011
-- Description:	Checks to see if a file has been processed
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_FileDetailHistory] 
	-- Add the parameters for the stored procedure here
@FileDetailid AS INT, @Description as varchar(1024), @isError as bit
AS
BEGIN

	SET NOCOUNT ON
    -- Insert statements for procedure here
	INSERT INTO FileDetailHistory(FileDetailId, Description, isError)
		VALUES(@FileDetailid, @Description, @isError)
	
END

GO
