USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_FileDetailsSize]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_UPD_FileDetailsSize]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPD_FileDetailsSize]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 25th January 2011
-- Description:	Checks to see if a file has been processed
-- =============================================
CREATE PROCEDURE [dbo].[usp_UPD_FileDetailsSize] 
	-- Add the parameters for the stored procedure here
@FileDetailid AS INT, @FileSize AS INT = NULL
AS
BEGIN

	UPDATE FileDetails SET FileSize = @FileSize WHERE id = @FileDetailid
	
END

GO
