USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_FileSentviaFTP]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_INS_FileSentviaFTP]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_FileSentviaFTP]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 25th January 2011
-- Description:	Checks to see if a file has been processed
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_FileSentviaFTP] 
	-- Add the parameters for the stored procedure here
@FileName as VARCHAR(255), @FilePath as VARCHAR(1024),
@CategoryId AS INT, @FileSize AS INT, @FileDate AS DATETIME
AS
BEGIN

	SET NOCOUNT ON
    -- Insert statements for procedure here
	INSERT INTO FileDetails([CategoryId], [FilePath], [FileName], FileSize, FileDate,
		FTPDate)
		VALUES (@CategoryId, @FilePath, @FileName, @FileSize, @FileDate, GETDATE())
	
	SELECT @@IDENTITY 
	
END

GO
