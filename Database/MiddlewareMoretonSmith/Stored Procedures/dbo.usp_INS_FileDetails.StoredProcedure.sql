USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_FileDetails]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_INS_FileDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_FileDetails]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 25th January 2011
-- Description:	Checks to see if a file has been processed
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_FileDetails] 
	-- Add the parameters for the stored procedure here
@FileName as VARCHAR(255), @FilePath as VARCHAR(1024),
@CategoryId AS INT, @FileSize AS INT, @FileDate AS DATETIME, @BatchNumber AS INT, @Status AS VARCHAR(50) = 'WAITING'
AS
BEGIN

	SET NOCOUNT ON
    -- Insert statements for procedure here
	INSERT INTO FileDetails([CategoryId], [FilePath], [FileName], FileSize, FileDate, Status, BatchNumber, BatchDate)
		VALUES (@CategoryId, @FilePath, @FileName, @FileSize, @FileDate, @Status, @BatchNumber, GETDATE())
	
	SELECT @@IDENTITY AS ID
	
END

GO
