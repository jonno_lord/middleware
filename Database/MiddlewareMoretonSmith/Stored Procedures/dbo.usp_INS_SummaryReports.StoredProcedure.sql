USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_SummaryReports]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_INS_SummaryReports]
GO
/****** Object:  StoredProcedure [dbo].[usp_INS_SummaryReports]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_INS_SummaryReports]
(
	@JobName AS VARCHAR(50),	
	@Summary AS VARCHAR(MAX)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.SummaryReports ( JobName, RunDate, Summary )
	
	VALUES
	
	(@JobName, GETDATE(), @Summary )
	
END

GO
