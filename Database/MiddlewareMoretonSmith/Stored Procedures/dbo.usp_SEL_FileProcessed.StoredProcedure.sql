USE [MiddlewareMoretonSmith]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_FileProcessed]    Script Date: 10/04/2018 10:48:16 ******/
DROP PROCEDURE [dbo].[usp_SEL_FileProcessed]
GO
/****** Object:  StoredProcedure [dbo].[usp_SEL_FileProcessed]    Script Date: 10/04/2018 10:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Parker
-- Create date: 25th January 2011
-- Description:	Checks to see if a file has been processed
-- =============================================
CREATE PROCEDURE [dbo].[usp_SEL_FileProcessed] 
	-- Add the parameters for the stored procedure here
@FileName as VARCHAR(1024)
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * FROM ProcessedMSIFiles WHERE [FileName] = @FileName AND Processed = 1
	
END

GO
